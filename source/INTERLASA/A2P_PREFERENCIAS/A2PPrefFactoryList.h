//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.com S.A. de C.V.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================
REGISTER_PMINTERFACE(A2PPrefActionComponent, kA2PPrefActionComponentImpl)

//Dialogo Preferencias
REGISTER_PMINTERFACE(SelDlgTabDialogCreator, kA2PPrefTabDialogCreatorImpl)
REGISTER_PMINTERFACE(SelDlgDialogObserver, kA2PPrefDialogObserverImpl)
REGISTER_PMINTERFACE(PreferenciasControlador, kA2PPrefDialogControllerImpl)
//Dialogo Guardar Preferencia
REGISTER_PMINTERFACE(SelDlgGuardarDialogCreator, kA2PPrefSelDlgGuardarPrefCreatorImpl)

//Dialogo nuevo Password
REGISTER_PMINTERFACE(SelDlgNewPassDialogCreator, kA2PPrefSelDlgNewPassCreatorImpl)
REGISTER_PMINTERFACE(A2PNewPassDialogController, kA2PPrefSelDlgNewPassControllerImpl)
//REGISTER_PMINTERFACE(SelDlgBotonNewPassObserver, kA2PPrefSelBotonNewPassObserverImpl)

//Dialogo Password
REGISTER_PMINTERFACE(A2PPrefKeyLicenseDlgCreator, kA2PPrefKeyLicenseDlgCreatorImpl)
REGISTER_PMINTERFACE(A2PPrefKeyLicenseDlgController, kA2PPrefKeyLicenseDlgControllerImpl)

/////////////////////Registro las pestaÒa///////////////////////////////////////
REGISTER_PMINTERFACE(CreatorPestanaDocumento, kA2PPrefDocPanelCreatorImpl)
REGISTER_PMINTERFACE(CreatorPestanaCapas, kA2PPrefCapasPanelCreatorImpl)
REGISTER_PMINTERFACE(CreadorPestanaCImagen, kA2PPrefCImagPanelCreatorImpl)
REGISTER_PMINTERFACE(CreadorPestanaImport, kA2PPrefImportPanelCreatorImpl)
REGISTER_PMINTERFACE(CreadorPestanaOrdenCoord, kA2PPrefOrdenCoordenadasPanelCreatorImpl)
REGISTER_PMINTERFACE(CreadorPestanaEtiqueta, kA2PPrefEtiquetPanelCreatorImpl)
REGISTER_PMINTERFACE(CreadorPestanaOrden, kA2PPrefOrdenPanelCreatorImpl)
////////////////////Registro de todos los observadores/////////////////////////
//REGISTER_PMINTERFACE(SelDlgBotonAplyObserver, kA2PPrefSelBotonAplyObserverImpl)

REGISTER_PMINTERFACE(SelDlgBotonSeleccionObserver, kA2PPrefSelBotonSeleccionarObserverImpl)
REGISTER_PMINTERFACE(SelDlgBotonAtrasVPrevObserver, kA2PPrefSelBotonAtrasVPrevObserverImpl)
REGISTER_PMINTERFACE(SelDlgBotonAdelanteVPrevObserver, kA2PPrefSelBotonAdelVPrevObserverImpl)

REGISTER_PMINTERFACE(A2PPrefUserUIPwdDialogController, kA2PPrefUserUIPwdDialogControllerImpl)
REGISTER_PMINTERFACE(A2PPrefUserUIPwdDialogObserver, kA2PPrefUserUIPwdDialogObserverImpl)

#ifdef MACINTOSH
REGISTER_PMINTERFACE(A2PPrefNonBrokenPasswordEventHandler, kA2PPrefNonBrokenPasswordEventHandlerImpl) 
#endif

REGISTER_PMINTERFACE(SelDlgChekBoxObserver, kA2PPrefSelDlgCheckBoxObserverImpl)
REGISTER_PMINTERFACE(SelRadioButtomTLBRObserver, kA2PPrefRadioButtomTLBRObserverImpl)
//REGISTER_PMINTERFACE(SelDlgIconObserver, kA2PPrefIconObserverImpl)
//Interface para utilidades del RegEdit
#ifdef WINDOWS
REGISTER_PMINTERFACE(RegEditUtilities, kA2PPrefRegEditUtilitiesImpl)
#endif
//Interface para accesar y guardar preferencias
REGISTER_PMINTERFACE(A2PPreferenciasFunciones, kA2PPrefPreferenciasFuncionesImpl)

REGISTER_PMINTERFACE(A2PContratoDialogController, kA2PPrefDialogContratoControllerImpl)
REGISTER_PMINTERFACE(A2PDialogContratoObserver, kA2PPrefDialogContratoObserverImpl)
//  Code generated by DollyXs code generator

//  Code generated by DollyXs code generator
