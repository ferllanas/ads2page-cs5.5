/*
//	File:	A2P20CreaPestañaOrdenCoord.cpp
//
//	Creador de la interfaz de la pestaña documento
//
*/

#include "VCPlugInHeaders.h"

// Implementation includes:
#include "CPanelCreator.h"

// Project includes:
#include "A2PPrefID.h"

/** A2P20CreaPestañaDocumento
	implementa IPanelCreator basedo sobre la implementacion parcial CPanelCreator.
	Eliminamos CPanelCreator's GetPanelRsrcID().
	@author Fernando llanas
*/
class CreatorPestanaOrdenCoord : public CPanelCreator
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CreatorPestanaOrdenCoord(IPMUnknown *boss) : CPanelCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CreatorPestanaOrdenCoord() {}

		/** 
			Returns the local resource ID of the ordered panels.
		*/
		virtual RsrcID GetPanelRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(CreatorPestanaOrdenCoord, kA2PPrefOrdenCoordenadasPanelCreatorImpl)

/* GetPanelRsrcID
*/
RsrcID CreatorPestanaOrdenCoord::GetPanelRsrcID() const
{
	return kA2PPrefOrdenCoorPanelCreatorResourceID;
}


