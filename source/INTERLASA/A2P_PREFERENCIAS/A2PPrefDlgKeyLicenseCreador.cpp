//nuevo

#include "VCPlugInHeaders.h"

// Interface includes:
#include "LocaleSetting.h"
#include "IDialogMgr.h"
#include "IApplication.h"
#include "ISession.h"

// Implementation includes:
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"

// Project includes:
#include "A2PPrefID.h"


/** SelDlgDialogCreator
	provides management and control over the dialog. 
  
	SelDlgDialogCreator implements IDialogCreator based on
	the partial implementation CDialogCreator. 
	@author Rodney Cook
*/
class A2PPrefKeyLicenseDlgCreator : public CDialogCreator
{
	public:
		/**
			Constructor.

			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		A2PPrefKeyLicenseDlgCreator(IPMUnknown *boss) : CDialogCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~A2PPrefKeyLicenseDlgCreator() {}

		/** 
			Creates a dialog from the resource that is cached by 
			the dialog manager until invoked.
		*/
		virtual IDialog* CreateDialog();

		/** 
			Returns the resource ID of the resource containing an ordered list of panel IDs.

			@param classIDs specifies resource ID containing an ordered list of panel IDs.
		*/
		virtual void GetOrderedPanelIDs(K2Vector<ClassID>& classIDs);

		/** 
			Returns an ordered list of class IDs of selectable dialogs
			that are to be installed in this dialog.
		*/
		virtual RsrcID GetOrderedPanelsRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(A2PPrefKeyLicenseDlgCreator, kA2PPrefKeyLicenseDlgCreatorImpl)


/* 
	CreateDialog
*/
IDialog* A2PPrefKeyLicenseDlgCreator::CreateDialog()
{
	IDialog* dialog = nil;
	
	// Use a do-while(false) so we can break on bad pointers:
	do
	{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());

		InterfacePtr<IDialogMgr> dialogMgr(app, UseDefaultIID());
		if (dialogMgr == nil)
		{
			ASSERT_FAIL("A2PPrefKeyLicenseDlgCreator::CreateDialog: dialogMgr invalid");
			break;
		}

		// We need to load the plug-in's resource:
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec(
			nLocale,					// Defined in PMLocaleIDs.h. 
			kA2PPrefPluginID,		// Defined in A2PPrefID.h. 
			kViewRsrcType,			// This is the kViewRsrcType.
			A2PPrefKeyLicenseDlgResourceID,	// Defined in A2PPrefID.h.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above,
		// and the type of dialog being created.	
		// The dialog manager caches the dialog for us.
		dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);

		// Make the initial focus be in the selection list.
		dialog->SetDialogFocusingAlgorithm(IDialog::kNoAutoFocus);

	} while (false); // Only do once.

	return dialog;
}


/* 
	GetOrderedPanelIDs
*/
void A2PPrefKeyLicenseDlgCreator::GetOrderedPanelIDs(K2Vector<ClassID>& classIDs)
{
	ResourceEnabler en;
	CDialogCreator::GetOrderedPanelIDs(classIDs);
}


/* 
	GetOrderedPanelsRsrcID
*/
RsrcID A2PPrefKeyLicenseDlgCreator::GetOrderedPanelsRsrcID() const
{
	return kA2PPrefNewPassResourceID;
}

// End, SelDlgDialogCreator.cpp


