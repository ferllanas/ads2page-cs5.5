/*
//	File:	SelRadioButtomTLBRObserver.cpp
//
//	Date:	11-May-2001
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#include "VCPlugInHeaders.h"

// Interface includes
#include "ISubject.h"
#include "IControlView.h"
#include "ITriStateControlData.h"
#include "ITextValue.h"
#include "IPanelControlData.h"
#include "IWidgetParent.h"
// Implem includes

#include "CObserver.h"
#include "A2PPrefID.h"
#include "CAlert.h"

/**
	Implements IObserver. The SelRadioButtomTLBRObserver class is derived from CObserver, and overrides the
	AutoAttach(), AutoDetach(), and Update() methods.
	This class provides the capability to observe the widgets on the panel and respond appropriately
	to changes in their state.
	
	@author Ian Paterson

*/
class SelRadioButtomTLBRObserver : public CObserver
{
public:
	/**
		Constructor for SelRadioButtomTLBRObserver class
	*/		
	SelRadioButtomTLBRObserver(IPMUnknown *boss);
	/**
		Destructor for SelRadioButtomTLBRObserver class - 
		performs cleanup 
	*/
	~SelRadioButtomTLBRObserver();
	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	*/		
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when the widget is hidden.
	*/		
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	void handleWidgetHit(InterfacePtr<IControlView>& controlView, const ClassID& theChange, const WidgetID&  widgetID) ;
	void initialiseState();
	void attachWidget(InterfacePtr<IPanelControlData>&  panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
	void detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
	void resetInputStates();
	void setIntEditBoxState(const WidgetID&  widgetID, int32 prop);
	void setTriState(const WidgetID&  widgetID, ITriStateControlData::TriState state);
	void HabilDeshaWitgetsDependientes(const WidgetID& widgetID,bool16 ChecBoxSeleccionado);
	void MostrarOcultar(const WidgetID& widgetID,bool16 ChecBoxSeleccionado);

	bool16 isSelectedTriState(const WidgetID&  widgetID);
	const PMIID fObserverIID;

};

CREATE_PMINTERFACE(SelRadioButtomTLBRObserver, kA2PPrefRadioButtomTLBRObserverImpl)

/*
Constructor
*/
SelRadioButtomTLBRObserver::SelRadioButtomTLBRObserver(IPMUnknown* boss)
: CObserver(boss), fObserverIID(IID_IOBSERVER)
{
	
}

/*
Constructor
*/
SelRadioButtomTLBRObserver::~SelRadioButtomTLBRObserver()
{
	
}


/*
Constructor
*/
void SelRadioButtomTLBRObserver::AutoAttach()
{
	do {
		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			//CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			//CAlert::InformationAlert("No se encontro el panel");
			break;										
		}	
		const PMIID radioProtocol = IID_ITRISTATECONTROLDATA;
		this->attachWidget(panel, kA2PPrefRadioButtonEspecialWidgetID,radioProtocol);
		this->attachWidget(panel, kA2PPrefRadioButtonEspecialOrdCWidgetID,radioProtocol);
		this->attachWidget(panel, kA2PPrefRadioButtonTLBRWidgetID    ,radioProtocol);
		this->attachWidget(panel, kA2PPrefRadioButtonXYWidgetID      ,radioProtocol);
		this->attachWidget(panel, kA2PPrefRadioButtonTLBRSepWidgetID   ,radioProtocol);
	} while(kFalse);
}

/*
Constructor
*/
void SelRadioButtomTLBRObserver::AutoDetach()
{
	do {
	
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			//CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}

		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			//CAlert::InformationAlert("No se encontro el panel677");
			break;										
		}	
		

		const PMIID radioProtocol = IID_ITRISTATECONTROLDATA;
		this->detachWidget(panel, kA2PPrefRadioButtonEspecialWidgetID,radioProtocol);
		this->detachWidget(panel, kA2PPrefRadioButtonEspecialOrdCWidgetID,radioProtocol);
		this->detachWidget(panel, kA2PPrefRadioButtonTLBRWidgetID,radioProtocol);
		this->detachWidget(panel, kA2PPrefRadioButtonXYWidgetID,radioProtocol);
		this->detachWidget(panel, kA2PPrefRadioButtonTLBRSepWidgetID,radioProtocol);

	} while(kFalse);
}



/* attachWidget
*/
void SelRadioButtomTLBRObserver::attachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData);

//	TRACE("TblAttWidgetObserver::AttachWidget(widgetID=0x%x, interfaceID=0x%x\n", widgetID, interfaceID);
	do
	{
		if(panelControlData==nil) 
		{
			//CAlert::InformationAlert("No se encontro el panel");
			break;
		}

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView);
		if (controlView == nil) 
		{
			//CAlert::InformationAlert("No se encontro el Controlview");
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject);
		if (subject == nil) 
		{ 
			//CAlert::InformationAlert("No se encontro el suject attachWidget");
			break;
		}
		subject->AttachObserver(this, interfaceID, fObserverIID);
	}
	while (kFalse);
}

/* detachWidget
*/
void SelRadioButtomTLBRObserver::detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData);
	do
	{
		if(panelControlData == nil) {
			break;
		}
		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView);
		if (controlView == nil) {
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject);
		if (subject == nil) 
		{
			break;
		}
		subject->DetachObserver(this, interfaceID, fObserverIID);
	}
	while (kFalse);
}


/*
Constructor
*/
void SelRadioButtomTLBRObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	do 
	{
		
		ASSERT(theSubject);
		if(theSubject==nil)
		{
			break;
		}

		InterfacePtr<IControlView>  icontrolView(theSubject, UseDefaultIID());
		ASSERT(icontrolView);
		if(icontrolView==nil) 
		{
			CAlert::InformationAlert("No se encontro el controlview update");
			break;
		}

		WidgetID thisID = icontrolView->GetWidgetID();
		
		if(thisID != kInvalidWidgetID) 
		{
			this->handleWidgetHit(icontrolView, theChange, thisID);
		}
	} while(kFalse);
}

/*
Constructor
*/
void SelRadioButtomTLBRObserver::handleWidgetHit(InterfacePtr<IControlView> & controlView, 
										   const ClassID & theChange, 
										   const WidgetID&  widgetID) 
{
	do {

		ASSERT(widgetID != kInvalidWidgetID);
		ASSERT(controlView);
		if(controlView==nil) {
			break;
		}


		
			switch(widgetID.Get()) 
			{
				case kA2PPrefRadioButtonEspecialWidgetID:
				{
					if(theChange == kTrueStateMessage)
					{
						//CAlert::InformationAlert("Seleccion");
						HabilDeshaWitgetsDependientes(kA2PPrefBoxCarEspecialLecturaEID,kTrue);
					}
					else
					{
						//Deshabilita caja de caracter especial
						//CAlert::InformationAlert("deseleccion");
						HabilDeshaWitgetsDependientes(kA2PPrefBoxCarEspecialLecturaEID,kFalse);	
					}
				}
				break;

				case kA2PPrefRadioButtonEspecialOrdCWidgetID:
				{
					if(theChange == kTrueStateMessage)
					{
						//CAlert::InformationAlert("Seleccion");
						HabilDeshaWitgetsDependientes(kA2PPrefBoxCarEspecialLecturaOrdCID,kTrue);
					}
					else
					{
						//Deshabilita caja de caracter especial
						//CAlert::InformationAlert("deseleccion");
						HabilDeshaWitgetsDependientes(kA2PPrefBoxCarEspecialLecturaOrdCID,kFalse);	
					}
				}
				break;

				case kA2PPrefRadioButtonTLBRWidgetID:
				{
					
					//setTriState(kA2PPrefRadioButtonTLBRWidgetID      ,ITriStateControlData::kSelected);
					this->setTriState(kA2PPrefRadioButtonXYWidgetID      ,ITriStateControlData::kUnselected);
					this->setTriState(kA2PPrefRadioButtonTLBRSepWidgetID   ,ITriStateControlData::kUnselected);

					if(theChange == kTrueStateMessage)//si se encuentra seleccionado
					{
						if(isSelectedTriState(kA2PPrefRadioButtonTLBRWidgetID))
						{
							//CAlert::InformationAlert("Seleccion");
							this->MostrarOcultar(kA2PPrefComboBoxXYWidgetID,kFalse);
							this->MostrarOcultar(kA2PPrefComboBoxTLBRWidgetID,kTrue);
							this->HabilDeshaWitgetsDependientes(kA2PPrefComboBoxXYWidgetID,kTrue);
							this->HabilDeshaWitgetsDependientes(kA2PPrefComboBoxTLBRWidgetID,kTrue);
						}
					}
					
				}
				break;

				case kA2PPrefRadioButtonXYWidgetID:
				{
					//setTriState(kA2PPrefRadioButtonXYWidgetID    ,ITriStateControlData::kSelected);
					setTriState(kA2PPrefRadioButtonTLBRWidgetID    ,ITriStateControlData::kUnselected);
					setTriState(kA2PPrefRadioButtonTLBRSepWidgetID   ,ITriStateControlData::kUnselected);

					if(theChange == kTrueStateMessage)//si se encuentra seleccionado
					{
						if(isSelectedTriState(kA2PPrefRadioButtonXYWidgetID))
						{
							this->MostrarOcultar(kA2PPrefComboBoxXYWidgetID,kTrue);
							this->MostrarOcultar(kA2PPrefComboBoxTLBRWidgetID,kFalse);
							HabilDeshaWitgetsDependientes(kA2PPrefComboBoxXYWidgetID,kTrue);
							HabilDeshaWitgetsDependientes(kA2PPrefComboBoxTLBRWidgetID,kTrue);
						}
					}
					
				}
				break;

				case kA2PPrefRadioButtonTLBRSepWidgetID:
				{
					//setTriState(kA2PPrefRadioButtonTLBRSepWidgetID    ,ITriStateControlData::kSelected);
					setTriState(kA2PPrefRadioButtonTLBRWidgetID    ,ITriStateControlData::kUnselected);
					setTriState(kA2PPrefRadioButtonXYWidgetID      ,ITriStateControlData::kUnselected);
					if(theChange == kTrueStateMessage)//si se encuentra seleccionado
					{
						if(isSelectedTriState(kA2PPrefRadioButtonTLBRSepWidgetID))
						{
							//CAlert::InformationAlert("Seleccion");
							HabilDeshaWitgetsDependientes(kA2PPrefComboBoxXYWidgetID,kFalse);
							HabilDeshaWitgetsDependientes(kA2PPrefComboBoxTLBRWidgetID,kFalse);
						}
					}
				}
				break;	
			}
	} while(kFalse);
}

/* resetInputStates
*/
void SelRadioButtomTLBRObserver::resetInputStates()
{
	
}


/*
Constructor
*/
void SelRadioButtomTLBRObserver::initialiseState()
{
	
}


void SelRadioButtomTLBRObserver::setTriState(const WidgetID&  widgetID, ITriStateControlData::TriState state)
{
	do {
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("myParent setTriState");
			break;
		}
		//encuentra la interfaz IPanelControlData a partir del parent
		InterfacePtr<IPanelControlData>	iPanelControlData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(iPanelControlData==nil)
		{
			CAlert::InformationAlert("IPanelControlData setTriState");
			break;
		}

		IControlView * iControlView  = iPanelControlData->FindWidget(widgetID);
		ASSERT(iControlView);
		if(iControlView==nil) 
		{
			break;
		}

		InterfacePtr<ITriStateControlData> itristatecontroldata(iControlView, UseDefaultIID());
		ASSERT(itristatecontroldata);
		if(itristatecontroldata==nil) {
			break;
		}
		itristatecontroldata->SetState(state);
	} while(kFalse);	

}




void SelRadioButtomTLBRObserver::setIntEditBoxState(const WidgetID&  widgetID, int32 prop)
{
	do {
		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert(" myParent setIntEditBoxState");
			break;
		}
		//encuentra la interfaz IPanelControlData a partir del parent
		InterfacePtr<IPanelControlData>	iPanelControlData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(iPanelControlData==nil)
		{
			CAlert::InformationAlert("iPanelControlData setIntEditBoxState");
			break;
		}

		IControlView * iControlView  = iPanelControlData->FindWidget(widgetID);
		ASSERT(iControlView);
		if(iControlView==nil) {
			CAlert::InformationAlert("iControlView setIntEditBoxState");
			break;
		}

		InterfacePtr<ITextValue> adjustedValue(iControlView, UseDefaultIID());
		ASSERT(adjustedValue);
		if(adjustedValue==nil) 
		{
			CAlert::InformationAlert("adjustedValue setIntEditBoxState");
			break;
		}
		adjustedValue->SetTextFromValue(PMReal(prop), kTrue/*invalidate*/, kFalse/*don't notify*/);

	} while(kFalse);	

}



/*
Constructor
*/
bool16 SelRadioButtomTLBRObserver::isSelectedTriState(const WidgetID&  widgetID)
{
	bool16  retval=kFalse;
	do {
		
			//encuentra el la interfaz IWidgetParent del checkbox seleccionado
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("IWidgetParent isSelectedTriState");
			break;
		}
		//encuentra la interfaz IPanelControlData a partir del parent
		InterfacePtr<IPanelControlData>	iPanelControlData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(iPanelControlData==nil)
		{
			CAlert::InformationAlert("iPanelControlData isSelectedTriState");
			break;
		}

		IControlView * iControlView  = iPanelControlData->FindWidget(widgetID);
		ASSERT(iControlView);
		if(iControlView==nil) 
		{
			CAlert::InformationAlert("Controlview isSelectedTriState3");
			break;
		}

		InterfacePtr<ITriStateControlData> iTriStateControlData(iControlView, UseDefaultIID());
		ASSERT(iTriStateControlData);
		if(iTriStateControlData==nil) 
		{
			CAlert::InformationAlert("ITriStateControlData isSelectedTriState");
			break;
		}

		retval = iTriStateControlData->IsSelected();

	} while(kFalse);

	return retval;

}

void SelRadioButtomTLBRObserver::HabilDeshaWitgetsDependientes(const WidgetID& widgetID,bool16 ChecBoxSeleccionado)
{
	do
	{	
		//encuentra el la interfaz IWidgetParent del checkbox seleccionado
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("myParent HabilDeshaWitgetsDependientes");
			break;
		}
		
		InterfacePtr<IPanelControlData>	iPanelControlData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(iPanelControlData==nil)
		{
			CAlert::InformationAlert("iPanelControlData HabilDeshaWitgetsDependientes");
			break;
		}
		

		IControlView * iControlView  = iPanelControlData->FindWidget(widgetID);
		ASSERT(iControlView);
		if(iControlView==nil) 
		{
			CAlert::InformationAlert("Controlview HabilDeshaWitgetsDependientes5");
			break;
		}
		iControlView->Enable(ChecBoxSeleccionado);
	}while(false);
}


void SelRadioButtomTLBRObserver::MostrarOcultar(const WidgetID& widgetID,bool16 ChecBoxSeleccionado)
{
	do
	{
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("IWidgetParent MostrarOcultar");
			break;
		}

		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("IPanelControlData MostrarOcultar");
			break;										
		}	
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		ComboBox( panel->FindWidget(widgetID), UseDefaultIID() );
		if(ComboBox==nil)
		{
			CAlert::InformationAlert("IControlView MostrarOcultar");
			break;
		}	
		
		if(ChecBoxSeleccionado)
			ComboBox->Show(kTrue);//Abilita el boton Aceptar
		else
			ComboBox->Hide();//Abilita el textbox del nombre de la nueva preferencia
	}while(false);
}