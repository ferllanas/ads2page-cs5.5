/*
//	File:	A2P20CreaPestañaImportar.cpp
//
//	Creador de la interfaz de la pestaña Importar
//
*/

#include "VCPlugInHeaders.h"

// Implementation includes:
#include "CPanelCreator.h"

// Project includes:
#include "A2PPrefID.h"

/** A2P20CreaPestañaImportar
	implementa IPanelCreator basedo sobre la implementacion parcial CPanelCreator.
	Eliminamos CPanelCreator's GetPanelRsrcID().
	@author Fernando llanas
*/

class CreatorPestanaImportar : public CPanelCreator
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CreatorPestanaImportar(IPMUnknown *boss) : CPanelCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CreatorPestanaImportar() {}

		/** 
			Returns the local resource ID of the ordered panels.
		*/
		virtual RsrcID GetPanelRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(CreatorPestanaImportar, kA2PPrefImportPanelCreatorImpl)

/* GetPanelRsrcID
*/
RsrcID CreatorPestanaImportar::GetPanelRsrcID() const
{
	return kA2PPrefImportPanelCreatorResourceID;
}

// End, YinPanelCreator.cpp
