/*
//	File:	A2P20BotonSeleccionObserver.cpp
//
//	Author: Fernando Llanas
//
//	Date:	11-Jun-2003
//
//	Este archivo es utilizado para ejecutar el observer del boton Seleccion en Vista Previa
//	.este archivo contiene las instrucciones que se ejecutaran a la hora de dar click sobre el 
//	boton antes mensionado.
//
*/
#include "VCPlugInHeaders.h"
#include "FileUtils.h"

// Interface includes:
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"


// Implementation includes:
#include "CDialogObserver.h"
// Project:

#include "SDKUtilities.h"
#include "A2PPrefID.h"
#include "CAlert.h"
#include "IOpenFileDialog.h"
#include "IOpenFileCmdData.h"
#include "IOpenManager.h"
#include "OpenPlaceID.h"
#include "SysFileList.h"
#include "ErrorUtils.h"
#include "LayoutID.h"
#include "IK2ServiceRegistry.h"
//#include "SeparaLinea.h"
#include "CDialogController.h"
#include "PMString.h"

#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"

#include "A2PPrefID.h"

#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
#endif
#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"
#endif
const SysOSType kSysOSType_CSVFileType = 'TEXT';
const PMString  kPMString_CSVFileFamily("Texto");
const PMString kPMString_CSVFileExtension("txt");

const SysOSType kSysOSType_CSVFileTipo = 'EXPO';
const PMString  kPMString_CSVFileFamily1("EXPORT");
const PMString kPMString_CSVFileExtension1("export");


// File control characters
const uchar kuchar_Separator = ',';
const uchar kuchar_CR = 0x0d;
const uchar kuchar_LF = 0x0a;

// Filename/path parsing characters
const char kchar_WinPathSeparator = '\\';
const char kchar_ExtensionSeparator = '.';
const char kchar_TableNameSeparator = '_';

/** SelDlgIconObserver
	Allows dynamic processing of icon button widget state changes, in this case
	the tab dialog's info button. 

	Implements IObserver based on the partial implementation CObserver. 

	@author Lee Huang
*/
class SelDlgBotonSeleccionObserver : public CObserver
{
	public:	
		/**
			Constructor.

			@param boss interface ptr from boss object on which interface is aggregated.
		*/
		SelDlgBotonSeleccionObserver(IPMUnknown *boss) : CObserver(boss) {}

		virtual void AutoAttach();

        virtual void AutoDetach();		
		/**
			Called by the host when the observed object changes. In this case: 
			the tab dialog's info button is clicked.

			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);

	private:
		/**
			Funcion que llama al Dialogo abrir archivo, lee el archivo caracter por caracter
			separandolo por lineas, y manda llamar la funcion Separacad, ademas pone el las
			cajas de texto invisibles sobre el panel o pestaÒa Importar, la cantidad de 
			avisos y la linea(aviso) que se ha leido en este caso sera el numero 1.
		*/
		void DoOpen();

		/**
			Cuenta el numero de lineas del archivo Expor, este numero de lineas equivale al numero de avisos
			
			  @param Direccion, Path del archivo para vista previa
		*/
		PMString NumLineas(PMString Direccion);

		/**
			Esta funcion separa la linea que se envia(CRecibida) entabuladores tambien aÒade 
			el numero de campo asi como el brinco de linea, y regresa la cadena formada y enumerada.
			
			  @param CRecibida, cadena de caracteres(aviso)
		*/
		PMString SeparaCad(PMString CRecibida,int32 CaracterSeparacionEnCampos);

		/**
		*/
		int32 CaracterDeSeparaciondeCampos();

		/**
		*/
		bool16 LeerEnCheckBox(IPanelControlData* panelControlData, const WidgetID& widgetID);
	

		PMString LeerEnWidgetBox(IPanelControlData* panelControlData, const WidgetID& widgetID);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(SelDlgBotonSeleccionObserver, kA2PPrefSelBotonSeleccionarObserverImpl)


/*	Update
*/
void SelDlgBotonSeleccionObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID& protocol, 
	void* changedBy
)
{
	InterfacePtr<IControlView> view(theSubject, IID_ICONTROLVIEW);

	if (view != nil)
	{
		// Get the button ID from the view
		WidgetID theSelectedWidget = view->GetWidgetID();

		if (theSelectedWidget == kA2PPrefBotonSelecVisPreviaID && theChange == kTrueStateMessage)
		{
			//Llamado a la funcion para poner leer archivo para vista previa
			DoOpen();
		}
    }
}




/* AutoAttach */
/**
	AL INICIAR LA FORMA SE CARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
	SOBRE EL BOTON APLICAR
*/

void SelDlgBotonSeleccionObserver::AutoAttach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_IBOOLEANCONTROLDATA);
	}
}

/* AutoDetach */
/**
	AL TERMINAR LA FORMA SE DESCARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR
*/

void SelDlgBotonSeleccionObserver::AutoDetach()
{
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_IBOOLEANCONTROLDATA);
	}
}


void SelDlgBotonSeleccionObserver::DoOpen()
{	//PMString r;

do
{
	
	if(CaracterDeSeparaciondeCampos()==-1)
	{//sale de funcion pues no se encuentra ningun caracter de separacion para los campos
		break;
	}
	
	IDFile file;
	bool16 success = kFalse;
	SysFileList filesToOpen;
	PMString NomArc;
	FILE *stream;

	PMString	DireccionCString="";
	CString NumLineNew;
	CString ArchivoCString;
	CString ArchivoCString2;
	CString NumLineaLeidaString;
	int PosIni;
	int PosFin;
	PlatformChar Caracter;
	char *NumUltimaLineaString;
	int NumCaracter=0;

	int Linea;
	int ANSCCICaranter;
	PMString NumUltimaLinea;
	PMString numerolinea;

	PMString DireccionPMString; //Direccion del archivo a leer
	PMString NumLineaLeida;//Numero de linea a leer
	PMString CadenaAEmprimir;//Cadena que se mostrara en vista previa
	PMString PMSCadena;
	PMString NumUltimaLineaPMString;
	
	ArchivoCString2="";
	NumLineNew="";
	ArchivoCString="";
	Caracter=' ';

	PMSCadena="";
	NumUltimaLineaPMString="";
	NumUltimaLineaString="";
	NumLineaLeidaString="";

	//Archivo="";
	NumLineaLeida="";
	CadenaAEmprimir="";
	NumUltimaLinea="";

//	Archivo.Clear();		
	NumLineaLeida.Clear();
	CadenaAEmprimir.Clear();
	PMSCadena.Clear();
	//NumUltimaLineaInt=0;
	
	
		//codigo para llamar el Dilogo para abrir un documento
		
	//llama a la interfaz IOpenFileDialog a partir del jefe kOpenFileDialogBoss
	InterfacePtr<IOpenFileDialog> iOpenDlg(	static_cast<IOpenFileDialog*>(CreateObject(	kOpenFileDialogBoss,IOpenFileDialog::kDefaultIID)));
				
	// si se creo la interfaz
	if (iOpenDlg != nil)
	{
		//adiciona los tipos de archivos a abrir
		iOpenDlg->AddType(kSysOSType_CSVFileType);
		iOpenDlg->AddExtension(&kPMString_CSVFileFamily, &kPMString_CSVFileExtension);
		iOpenDlg->AddType(kSysOSType_CSVFileTipo);
		iOpenDlg->AddExtension(&kPMString_CSVFileFamily1, &kPMString_CSVFileExtension1);
		
		//al selecciunar un archivo este se guardara en la variable file
		if (iOpenDlg->DoDialog(nil, filesToOpen, kFalse) == kTrue)
		{
			file = *filesToOpen.GetNthFile(0);
			success = kTrue;
		}
	}
		

		/*PARA ABRIR EL TEXTO DE UN FICHERO PERO ESTE METODO NO FUNCIONA MANDA UN ERROR*/		
		if(success==kTrue)
		{	
									
			DireccionPMString=FileUtils::SysFileToPMString(file);
			//DireccionPMString=InterlasaUtilities::MacToUnix(DireccionPMString);
			if( (stream  = FileUtils::OpenFile(DireccionPMString.GrabCString(),"r"))==NULL)
			{	
				CAlert::ErrorAlert( DireccionPMString );
				CAlert::ErrorAlert("1 ");
				//break;
			}
			else
			{
				fclose(stream);
				NumUltimaLinea=NumLineas(DireccionPMString);		
				if( (stream  = FileUtils::OpenFile(DireccionPMString.GrabCString(),"r"))==NULL)
					{	//CAlert::ErrorAlert("2 ");
						CAlert::ErrorAlert(DireccionPMString.GrabCString() );
						CAlert::ErrorAlert( kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
						//break;
					}
					else
					{		
						
						PosIni=0;	//Posicion inicial de una linea
						PosFin=0;	//Posicion final de una linea
						Linea=1;	//Numero de linea se inicializa en uno
						while(Linea<(2) && !feof(stream))//Hacer mientras el numero de linea sea menor a la ultima linea del archivo y menor a la linea que sigue de leer
						{	PosIni=PosFin;
							do
							{		
								Caracter.SetFromUnicode(fgetc(stream));
								NumCaracter++;
								ANSCCICaranter=Caracter.GetValue();
							}while(ANSCCICaranter!=10 && ANSCCICaranter!=13 && !feof(stream));//Hacer mientras que no sea fin de linea(10) retorno de carro(13) o fin de archivo(EOF)

							if(!feof(stream))
							{	PosFin=NumCaracter;	//Posicion final es igual al numero del ultimo caracter leido
								Linea++;	//siguiente linea
							}
						}
						

						fclose(stream);//Cierre de archivo

						if( (stream  = FileUtils::OpenFile(DireccionPMString.GrabCString(),"r"))==NULL)
						{	
							CAlert::ErrorAlert("3 ");
							CAlert::ErrorAlert( kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
							//	break;
						}
						else
						{
							
							char *Cadena=new char[PosFin];
							fread(Cadena,1,PosFin,stream);
							fclose(stream);
							//cuando cierro el archivo me manda un error en la ultima linea que se debe leer			
							
							//Copia cadena a archivo, Cadena contiene 
							PMSCadena=Cadena;
										
															
							//Obtengo la cadena a imprimir llamando a la funcion SeparaCad
							CadenaAEmprimir=SeparaCad(PMSCadena,CaracterDeSeparaciondeCampos());
				
							//Creo una interfaz llamada Myparen la cual sera la ventana 
							//que se encuentra abierta
							InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
							//Obtengo la inerfaz del panel que se encuentra abierto
							InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
							//Busqueda del Widget pde la caja  de vista previa
							InterfacePtr<IControlView>		editBoxView( panel->FindWidget(kA2PPrefBoxVistaPreviaID), UseDefaultIID() );
							//crea una interfax para controlar los dsatos del widget correspondiente
							InterfacePtr<ITextControlData>	selectedChar( editBoxView, UseDefaultIID());
							//inserto dentro del widget la nueva cadena
							selectedChar->SetString(CadenaAEmprimir);
							
							
							//Linea que se leyo
							InterfacePtr<IControlView>		editBoxView1( panel->FindWidget(kA2PPrefBoxNumeroLineaLeidaWidgetID), UseDefaultIID() );
							InterfacePtr<ITextControlData>	selectedChar1( editBoxView1, UseDefaultIID());
							selectedChar1->SetString("1", kTrue, kFalse);
							
							DireccionPMString=FileUtils::SysFileToPMString(file);
							DireccionCString=DireccionPMString.GrabCString();
							//Archivo que se leyo
							InterfacePtr<IControlView>		editBoxView2( panel->FindWidget(kA2PPrefBoxDireccionArchivoLeidoWidgetID), UseDefaultIID() );
							InterfacePtr<ITextControlData>	selectedChar2( editBoxView2, UseDefaultIID());
							selectedChar2->SetString(DireccionCString, kTrue, kFalse);
							//Numero de lineas del archivo
							InterfacePtr<IControlView>		editBoxView3( panel->FindWidget(kA2PPrefBoxNumUltimaLineEnArcWidgetID), UseDefaultIID() );
							InterfacePtr<ITextControlData>	selectedChar3( editBoxView3, UseDefaultIID());
							selectedChar3->SetString(NumUltimaLinea, kTrue, kFalse);
							delete Cadena;
							
							
						}
						
						
					}
				
			}
		}
}while(false);
}


PMString SelDlgBotonSeleccionObserver::NumLineas(PMString Direccion)
{	
	FILE *stream;
	
	PlatformChar Caracter;
	PMString CSLinea="";
	CSLinea.Clear();
	
	Caracter=' ';
	
	//Direccion=InterlasaUtilities::MacToUnix(Direccion);
	PMString CSDireccion=Direccion.GrabCString();

	
	int Linea;
	int ANSCCICaranter;
	Linea=0;
//do
//{
	if( (stream  = FileUtils::OpenFile(CSDireccion,"r"))==NULL)
		{	
			CAlert::ErrorAlert( kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
			//break;
		}
	else
		{		
	
			while(!feof(stream))
			{	
				do
				{		
					Caracter.SetFromUnicode(fgetc(stream));
					ANSCCICaranter=Caracter.GetValue();
				}while(ANSCCICaranter!=10 && ANSCCICaranter!=13 && !feof(stream));
				Linea++;
			}
			fclose(stream);
		}
	Linea--;
	CSLinea.AppendNumber(Linea);
//}while(false);
return(CSLinea);
}

/*
	Esta funcion separa la linea que se envia(CRecibida) entabuladores tambien aÒade 
	el numero de campo asi como el brinco de linea
*/
PMString SelDlgBotonSeleccionObserver::SeparaCad(PMString CRecibida,int32 CaracterSeparacionEnCampos)
{
		PMString CadenaAEnviar;	//Cadena que retorna
	PMString CEncontrada;	//Cadena del campo encontrada
	CadenaAEnviar.Clear();	//limpia cadenas
	CadenaAEnviar.Clear();	
	int PosTab=0;			//posicion del primer caracter tabulador
	int numcampo=1;			//numero de campo que se a leido
	int Tamano=0;			//tamaÒo de la cadena que se recibio
	int salir=0;			//bandera que indica si se ha leido el ultimo caracter de la cadena
	
	do
	{
		//se obtien la posicion del tabulador
		PosTab=CRecibida.IndexOfWChar(CaracterSeparacionEnCampos);
		//Si la posicion del tabulador fue -1 inidica que no se ha encontrado un tabulador entonces se encontro un fin 
		//de cadena se enciende la bandera salir del ciclo
		if(PosTab==-1)
		{
			PosTab=CRecibida.NumUTF16TextChars();//la posicion del tabulador sera el tamaÒo de la cadena
			salir=1;
		}

		//Cadena encontrada(campo) se toma desde el inicio de la cadena recibida hasta la posicion del 1∞ tabulador
		CEncontrada.Insert(CRecibida,0,PosTab);
		//se bora este campo de la cadena recibida
		CRecibida.Remove(0,PosTab+1);
		//se agrega el numero de campo que le corresponde  ala cadena a enviar
		CadenaAEnviar.AppendNumber(numcampo);
		CadenaAEnviar.Append(".- ");
		//se agrega la cadena encontrada a cadena a enviar
		CadenaAEnviar.Append(CEncontrada);
		//se obtiene  el tamaÒo de la cadena a enviar para insertar en la ultima posicion 
		//el salto de linea
		Tamano=CadenaAEnviar.NumUTF16TextChars();
		CadenaAEnviar.InsertW(13,Tamano);
		CadenaAEnviar.InsertW(10,Tamano+1);
		//se incrementa el numero de campo
		numcampo++;
		//se limpia la cadena encontrada
		CEncontrada.Clear();
	}while(salir!=1);
	return(CadenaAEnviar);//regresa cadena a enviar, cadena con numero de campo y su contenido
}

int32 SelDlgBotonSeleccionObserver::CaracterDeSeparaciondeCampos()
{

	int32 retval;
	UTF32TextChar temp;
	InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
	//Obtengo la inerfaz del panel que se encuentra abierto
	InterfacePtr<IPanelControlData>	panelControlImportar((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
	//Busqueda del Widget pde la caja  de vista previa

	

	if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonTabWidgetID))	
		retval=9;
	else
		if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonComaWidgetID))
			retval=44;
		else
			if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonPyComaWidgetID))
				retval=59;
			else
				if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonDosPuntosWidgetID))
					retval=58;
				else
				{
					if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonEspecialWidgetID))
					{
						PMString a;
						a=LeerEnWidgetBox(panelControlImportar,kA2PPrefBoxCarEspecialLecturaEID);
						if(a.NumUTF16TextChars()>0)
						{
							temp=a.GetWChar(0);
							retval=temp.GetValue();
						}
						else
						{
							CAlert::WarningAlert(kA2PPrefMensajeTeclearCarEspecialStringKey);
							retval=-1;
						}
					}
					else
					{
						CAlert::WarningAlert(kA2PPrefMensajeSelecTipoSepCamposStringKey);
						retval=-1;
					}
				}
	return(retval);
}

bool16 SelDlgBotonSeleccionObserver::LeerEnCheckBox(IPanelControlData* panelControlData, const WidgetID& widgetID)
{	
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IControlView>		editCheckBoxt(panelControlData->FindWidget(widgetID), UseDefaultIID() );
		if(editCheckBoxt==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		InterfacePtr<ITriStateControlData>	CheckBox( editCheckBoxt, IID_ITRISTATECONTROLDATA);
		if(CheckBox==nil)
		{
			CAlert::InformationAlert("No se encontro el estado del check box");
			break;
		}
		if(CheckBox->IsSelected())
			retval=kTrue;
		else
			retval=kFalse;
	}while(false);
	return retval;
}

/*
	FUNCION PARA LEER TEXTO EN UNA CAJA DE TEXTO 
*/
PMString SelDlgBotonSeleccionObserver::LeerEnWidgetBox(IPanelControlData* panelControlData, const WidgetID& widgetID)
{
	PMString retval="";
	do 
	{	
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		editBoxView( panelControlData->FindWidget(widgetID), UseDefaultIID() );
		if(editBoxView==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
		InterfacePtr<ITextControlData>	ControlDETexto( editBoxView, UseDefaultIID());
		if(ControlDETexto==nil)
		{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}
		//Extraigo el texto del Widget
		retval=ControlDETexto->GetString();
	}while(false);
	return retval;
}
