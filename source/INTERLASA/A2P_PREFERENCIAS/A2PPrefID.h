//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.com S.A. de C.V.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __A2PPrefID_h__
#define __A2PPrefID_h__

#include "SDKDef.h"

// Company:
#define kA2PPrefCompanyKey	"Interlasa"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kA2PPrefCompanyValue	"Interlasa"	// Company name displayed externally.

// Plug-in:
#define kA2PPrefPluginName	"Ads2Page"			// Name of this plug-in.
#define kA2PPrefPrefixNumber	0x73100 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kA2PPrefVersion		"v4.7.6M CS5"						// Version of this plug-in (for the About Box).
#define kA2PPrefAuthor		"Interlasa.com S.A. de C.V."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kA2PPrefPrefixNumber above to modify the prefix.)
#define kA2PPrefPrefix		RezLong(kA2PPrefPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kA2PPrefStringPrefix	SDK_DEF_STRINGIZE(kA2PPrefPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kA2PPrefMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kA2PPrefMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kA2PPrefPluginID, kA2PPrefPrefix + 0)


// ClassIDs:
DECLARE_PMID(kClassIDSpace, kA2PPrefActionComponentBoss,      kA2PPrefPrefix + 0)
DECLARE_PMID(kClassIDSpace, kA2PPrefStringRegisterBoss,       kA2PPrefPrefix + 1)
DECLARE_PMID(kClassIDSpace, kA2PPrefMenuRegisterBoss, 	      kA2PPrefPrefix + 2)
DECLARE_PMID(kClassIDSpace, kA2PPrefActionRegisterBoss,       kA2PPrefPrefix + 3)
DECLARE_PMID(kClassIDSpace, kA2PPrefDialogBoss, 		      kA2PPrefPrefix + 4)
DECLARE_PMID(kClassIDSpace, kA2PPrefTabDialogBoss, 		      kA2PPrefPrefix + 5)
DECLARE_PMID(kClassIDSpace, kA2PPrefImportarDialogBoss,       kA2PPrefPrefix + 6)

DECLARE_PMID(kClassIDSpace, kA2PPrefDocPanelBoss, 		       kA2PPrefPrefix +14)
DECLARE_PMID(kClassIDSpace, kA2PPrefCapasPanelBoss, 	       kA2PPrefPrefix + 13)
DECLARE_PMID(kClassIDSpace, kA2PrefCImagPanelBoss, 			   kA2PPrefPrefix + 12)
DECLARE_PMID(kClassIDSpace, kA2PPrefOrdenCoordenadasPanelBoss, kA2PPrefPrefix + 10)
DECLARE_PMID(kClassIDSpace, kA2PPrefImportPanelBoss,           kA2PPrefPrefix + 11)
DECLARE_PMID(kClassIDSpace, kA2PPrefEtiquetPanelBoss,		   kA2PPrefPrefix + 9)
DECLARE_PMID(kClassIDSpace, kA2PPrefOrdenPanelBoss, 		   kA2PPrefPrefix + 8)

DECLARE_PMID(kClassIDSpace, kA2PPrefUserUIPwdDialogBoss,	         kA2PPrefPrefix + 15)
//DECLARE_PMID(kClassIDSpace, kA2PPrefBotonNewPassSuiteWidgetBoss, 	 kA2PPrefPrefix + 16)
DECLARE_PMID(kClassIDSpace, kA2PPrefBotonSeleccionarSuiteWidgetBoss, kA2PPrefPrefix + 17)
DECLARE_PMID(kClassIDSpace, kA2PPrefBotonAtrasVPrevSuiteWidgetBoss,  kA2PPrefPrefix + 18)
DECLARE_PMID(kClassIDSpace, kA2PPrefBotonAdelVPrevSuiteWidgetBoss, 	 kA2PPrefPrefix + 19)
DECLARE_PMID(kClassIDSpace, kA2PPrefNonBrokenPasswordEditBoxWidgetBoss,kA2PPrefPrefix + 20)
//DECLARE_PMID(kClassIDSpace, kA2PPrefBotonGuardarPrefSuiteWidgetBoss, kA2PPrefPrefix + 21)
//DECLARE_PMID(kClassIDSpace, kA2PPrefBotonBorrarPrefSuiteWidgetBoss,  kA2PPrefPrefix + 22)
DECLARE_PMID(kClassIDSpace, kA2PPrefGuardarPrefDialogBoss, 			   kA2PPrefPrefix + 23)
DECLARE_PMID(kClassIDSpace, kA2PPrefBotonOkGuardarPrefSuiteWidgetBoss, kA2PPrefPrefix + 24)
DECLARE_PMID(kClassIDSpace, kA2PPrefNewPassDialogBoss,				   kA2PPrefPrefix + 25)
DECLARE_PMID(kClassIDSpace, kA2PPrefCheckBoxSuiteWidgetBoss,		   kA2PPrefPrefix + 26)
DECLARE_PMID(kClassIDSpace, kA2PPrefDynPnPanelManagerBoss,			   kA2PPrefPrefix + 27)
DECLARE_PMID(kClassIDSpace, kA2PPrefDynPnPanelWidgetBoss,			   kA2PPrefPrefix + 28)
DECLARE_PMID(kClassIDSpace, kA2PPrefKeyLicenseDialogBoss,			   kA2PPrefPrefix + 29)
DECLARE_PMID(kClassIDSpace, kA2PPrefRadioButtonTLBRSuiteWidgetBoss,	   kA2PPrefPrefix + 30)

DECLARE_PMID(kClassIDSpace, kA2PPrefRegEditUtilitiesBoss,			   kA2PPrefPrefix + 31)
DECLARE_PMID(kClassIDSpace, kA2PPreferenciasFuncionesBoss,			   kA2PPrefPrefix + 32)
DECLARE_PMID(kClassIDSpace, kA2PPrefDialogSegurityDialogBoss,		   kA2PPrefPrefix + 33)
DECLARE_PMID(kClassIDSpace, kA2PPrefDialogContradoDialogBoss,		   kA2PPrefPrefix + 34)
//DECLARE_PMID(kClassIDSpace, kA2PPrefPwdDlgEnterPasswordWidgetBoss,   kA2PPrefPrefix + 35)


// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPTBLMUTWIDGETOBSERVER,		kA2PPrefPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPDynPnPANELMANAGER,			kA2PPrefPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace,IID_IA2PPDynPnPERSISTUIDDATA,		kA2PPrefPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace,IID_IA2PPRegEditUtilities,			kA2PPrefPrefix + 3)
DECLARE_PMID(kInterfaceIDSpace,IID_IA2PPreferenciasFunciones,	    kA2PPrefPrefix + 4)


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kA2PPrefActionComponentImpl,				kA2PPrefPrefix + 0)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDialogControllerImpl, 				kA2PPrefPrefix + 1)
//DECLARE_PMID(kImplementationIDSpace, kA2PPrefDialogControllerImpl, 		    kA2PPrefPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefTabDialogObserverImpl, 			kA2PPrefPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDialogObserverImpl,				kA2PPrefPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDialogCreatorImpl, 				kA2PPrefPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDocPanelCreatorImpl, 				kA2PPrefPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefCapasPanelCreatorImpl, 			kA2PPrefPrefix + 7)
//DECLARE_PMID(kImplementationIDSpace, kA2PPrefIconObserverImpl, 				kA2PPrefPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefTabDialogCreatorImpl, 				kA2PPrefPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefImportarCreatorImpl, 				kA2PPrefPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefImportarControllerImpl, 			kA2PPrefPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefCImagPanelCreatorImpl, 			kA2PPrefPrefix + 12)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefImportPanelCreatorImpl, 			kA2PPrefPrefix + 13)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefOrdenCoordenadasPanelCreatorImpl, 	kA2PPrefPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefEtiquetPanelCreatorImpl,			kA2PPrefPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefOrdenPanelCreatorImpl, 			kA2PPrefPrefix + 16)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefNonBrokenPasswordEventHandlerImpl, kA2PPrefPrefix + 17)
//DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelBotonNewPassObserverImpl,		kA2PPrefPrefix + 18)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelBotonSeleccionarObserverImpl, 	kA2PPrefPrefix + 19)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelBotonAtrasVPrevObserverImpl,	kA2PPrefPrefix + 20)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelBotonAdelVPrevObserverImpl, 	kA2PPrefPrefix + 21)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefUserUIPwdDialogControllerImpl, 	kA2PPrefPrefix + 22)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefUserUIPwdDialogObserverImpl, 		kA2PPrefPrefix + 23)
//DECLARE_PMID(kImplementationIDSpace, kA2PPrefUniMedSelectionObserverImpl,		kA2PPrefPrefix + 24)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelDlgGuardarPrefCreatorImpl, 		kA2PPrefPrefix + 25)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelDlgGuardarPrefControllerImpl,	kA2PPrefPrefix + 26)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelBotonOkGuardarPrefObserverImpl,	kA2PPrefPrefix + 27)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelDlgNewPassCreatorImpl, 			kA2PPrefPrefix + 28)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelDlgNewPassControllerImpl, 		kA2PPrefPrefix + 29)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefSelDlgCheckBoxObserverImpl, 		kA2PPrefPrefix + 30)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDynPnPanelManagerImpl,				kA2PPrefPrefix + 31)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDynPnPersistUIDRefDataImpl,		kA2PPrefPrefix + 32)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefKeyLicenseDlgCreatorImpl, 			kA2PPrefPrefix + 33)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefKeyLicenseDlgControllerImpl, 		kA2PPrefPrefix + 34)
DECLARE_PMID(kImplementationIDSpace, kA2PPrefRadioButtomTLBRObserverImpl, 		kA2PPrefPrefix + 35) 

DECLARE_PMID(kImplementationIDSpace, kA2PPrefRegEditUtilitiesImpl,				kA2PPrefPrefix + 36)     
DECLARE_PMID(kImplementationIDSpace, kA2PPrefPreferenciasFuncionesImpl, 		kA2PPrefPrefix + 37)  
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDialogSegurityControllerImpl, 		kA2PPrefPrefix + 38)  
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDialogSegurityObserverImpl, 		kA2PPrefPrefix + 39)  
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDialogContratoControllerImpl, 		kA2PPrefPrefix + 40) 
DECLARE_PMID(kImplementationIDSpace, kA2PPrefDialogContratoObserverImpl, 		kA2PPrefPrefix + 41) 


// ActionIDs:
DECLARE_PMID(kActionIDSpace, kA2PPrefAboutActionID,                kA2PPrefPrefix + 0)
DECLARE_PMID(kActionIDSpace, kA2PPrefDialogActionID,               kA2PPrefPrefix + 1)
DECLARE_PMID(kActionIDSpace, kA2PPrefTabDialogActionID,            kA2PPrefPrefix + 2)
DECLARE_PMID(kActionIDSpace, kA2PPrefSelPanelImportDialogActionID, kA2PPrefPrefix + 3)
DECLARE_PMID(kActionIDSpace, kA2PPrefDynPnMenuSeparatorActionID,   kA2PPrefPrefix + 4)
DECLARE_PMID(kActionIDSpace, kA2PPrefDynPnFirstPanelActionID,	   kA2PPrefPrefix + 5)


// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBotonOKNewPrefWidgetID,      kA2PPrefPrefix + 1)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextNomNewPrefWidgetID,      kA2PPrefPrefix + 2)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefNewPassWidgetID,             kA2PPrefPrefix + 3)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextPassAntWidgetID,         kA2PPrefPrefix + 4)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextNewAntWidgetID,          kA2PPrefPrefix + 5)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextRepNewAntWidgetID,       kA2PPrefPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefButtonCreatNewPreWidgetID,   kA2PPrefPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefButtonAplyNewPreWidgetID,    kA2PPrefPrefix + 8)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefSelDlgDialogWidgetID,        kA2PPrefPrefix + 9)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefIconSuiteWidgetID,           kA2PPrefPrefix + 10)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefDocPanelWidgetID,            kA2PPrefPrefix + 11)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefDocPanelStaticTextWidgetID,  kA2PPrefPrefix + 12)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCapasPanelWidgetID,          kA2PPrefPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCapasPanelStaticTextWidgetID,kA2PPrefPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefSelDlgTabDialogWidgetID,     kA2PPrefPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefSelPanelImportqarWidgetID,   kA2PPrefPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefSelDlgGuardarPrefWidgetID,   kA2PPrefPrefix + 17)
/************************/
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCImagPanelWidgetID,               kA2PPrefPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCImagPanelStaticTextWidgetID,     kA2PPrefPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefImportPanelWidgetID,              kA2PPrefPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefImportPanelStaticTextWidgetID,    kA2PPrefPrefix + 21)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefOrdenCoorPanelWidgetID,           kA2PPrefPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefOrdenCoorPanelStaticTextWidgetID, kA2PPrefPrefix + 23)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefEtiquetPanelWidgetID,             kA2PPrefPrefix + 24)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefEtiquetPanelStaticTextWidgetID,   kA2PPrefPrefix + 25)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefOrdenPanelWidgetID,               kA2PPrefPrefix + 26)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefOrdenPanelStaticTextWidgetID,     kA2PPrefPrefix + 27)
/****************ID de frame Pagina en Pestana Documento***********/
DECLARE_PMID(kWidgetIDSpace, kA2PPrefFramePagInDocID, kA2PPrefPrefix + 28)
/************************/
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxAnchoPagID,                kA2PPrefPrefix + 29)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxAltoPagID,                 kA2PPrefPrefix + 30)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumColID,                  kA2PPrefPrefix + 31)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxMedianilID,                kA2PPrefPrefix + 32)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxMargSupID,                 kA2PPrefPrefix + 33)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxMargInfID,                 kA2PPrefPrefix + 34)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxMargExtID,                 kA2PPrefPrefix + 35)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxMargIntID,                 kA2PPrefPrefix + 36)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefApplyButtonWidgetID,          kA2PPrefPrefix + 37)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCambiaPassButtonWidgetID,     kA2PPrefPrefix + 38)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefGuardarPrefButtonWidgetID,    kA2PPrefPrefix + 39)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBorrarPrefButtonWidgetID,     kA2PPrefPrefix + 40)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCancelPrefButtonWidgetID,     kA2PPrefPrefix +41)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefAceptarNewPassButtonWidgetID, kA2PPrefPrefix + 42)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCancelarNewPassButtonWidgetID,kA2PPrefPrefix + 43)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID,          kA2PPrefPrefix + 44)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID1,         kA2PPrefPrefix + 45)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID2,         kA2PPrefPrefix + 46)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID3,         kA2PPrefPrefix + 47)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID4,         kA2PPrefPrefix + 48)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID5,         kA2PPrefPrefix + 49)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID6,         kA2PPrefPrefix + 50)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID7,         kA2PPrefPrefix + 51)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID8,         kA2PPrefPrefix + 52)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID9,         kA2PPrefPrefix + 53)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID10,        kA2PPrefPrefix + 54)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID11,        kA2PPrefPrefix + 55)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextInvalidWidgetID12,        kA2PPrefPrefix + 56)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxUniMedWidgetID,       kA2PPrefPrefix + 57)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxUniMedNudgeWidgetID,  kA2PPrefPrefix + 58)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxSelecPreferID,        kA2PPrefPrefix + 59)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaEditID,              kA2PPrefPrefix + 60)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaEditVisID,           kA2PPrefPrefix + 61)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaEditBlqID,           kA2PPrefPrefix + 62)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaImagID,              kA2PPrefPrefix + 63)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaImagVisID,           kA2PPrefPrefix + 64)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaImagBlqID,           kA2PPrefPrefix + 65)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaEConID,              kA2PPrefPrefix + 66)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaEConVisID,           kA2PPrefPrefix + 67)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaEConBlqID,           kA2PPrefPrefix + 68)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaESinID,              kA2PPrefPrefix + 69)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaESinVisID,           kA2PPrefPrefix + 70)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCapaESinBlqID,           kA2PPrefPrefix + 71)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxColImagConID,         kA2PPrefPrefix + 72)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxColMaImagConID,       kA2PPrefPrefix + 73)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxAjusteID,             kA2PPrefPrefix + 74)
//DECLARE_PMID(kWidgetIDSpace, kBotonAdelanteID,                   kA2PPrefPrefix + 43)
//DECLARE_PMID(kWidgetIDSpace, kBotonSeleccionarID,                kA2PPrefPrefix + 44)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxColImagSinID,         kA2PPrefPrefix + 75)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxColMaImagSinID,       kA2PPrefPrefix + 76)
//DECLARE_PMID(kWidgetIDSpace, kA2PPrefMensajeUniMedID,            kA2PPrefPrefix + 77)
/****************VISTA PREVIA**************************************/
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxVistaPreviaID,                kA2PPrefPrefix + 78)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumeroLineaLeidaWidgetID,     kA2PPrefPrefix + 79)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxDireccionArchivoLeidoWidgetID,kA2PPrefPrefix + 80)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumUltimaLineEnArcWidgetID,   kA2PPrefPrefix + 81)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBotonAtrasVisPreviaID,           kA2PPrefPrefix + 82)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBotonAdelanteVisPreviaID,        kA2PPrefPrefix + 83)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBotonSelecVisPreviaID,           kA2PPrefPrefix + 84)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBotonAplyCamposPreviaID,         kA2PPrefPrefix + 85)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLectura1ID,           kA2PPrefPrefix + 86)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLectura2ID,           kA2PPrefPrefix + 87)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLectura3ID,           kA2PPrefPrefix + 88)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLectura4ID,           kA2PPrefPrefix + 89)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLectura5ID,           kA2PPrefPrefix + 90)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLectura6ID,           kA2PPrefPrefix + 91)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLectura7ID,           kA2PPrefPrefix + 92)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLectura8ID,           kA2PPrefPrefix + 93)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLectura9ID,           kA2PPrefPrefix + 94)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLecturaAID,           kA2PPrefPrefix + 95)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLecturaBID,           kA2PPrefPrefix + 96)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLecturaCID,           kA2PPrefPrefix + 97)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLecturaDID,           kA2PPrefPrefix + 98)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCampoLecturaEID,           kA2PPrefPrefix + 99)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLectura1ID,           kA2PPrefPrefix + 100)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLectura2ID,           kA2PPrefPrefix + 101)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLectura3ID,           kA2PPrefPrefix + 102)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLectura4ID,           kA2PPrefPrefix + 103)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLectura5ID,           kA2PPrefPrefix + 104)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLectura6ID,           kA2PPrefPrefix + 105)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLectura7ID,           kA2PPrefPrefix + 106)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLectura8ID,           kA2PPrefPrefix + 107)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLectura9ID,           kA2PPrefPrefix + 108)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLecturaAID,           kA2PPrefPrefix + 109)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLecturaBID,           kA2PPrefPrefix + 110)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLecturaCID,           kA2PPrefPrefix + 111)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLecturaDID,           kA2PPrefPrefix + 112)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCampoLecturaEID,           kA2PPrefPrefix + 113)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonTabWidgetID,          kA2PPrefPrefix + 114)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonComaWidgetID,         kA2PPrefPrefix + 115)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonPyComaWidgetID,       kA2PPrefPrefix + 116)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonDosPuntosWidgetID,    kA2PPrefPrefix + 117)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonEspecialWidgetID,     kA2PPrefPrefix + 118)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTblMutRowHeaderWidgetID,         kA2PPrefPrefix + 119)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonTLBRWidgetID,         kA2PPrefPrefix + 120)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonXYWidgetID,           kA2PPrefPrefix + 121)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonTLBRSepWidgetID,      kA2PPrefPrefix + 122)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxTLBRWidgetID,            kA2PPrefPrefix + 123)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxXYWidgetID,              kA2PPrefPrefix + 124)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxUniTopWidgetID,          kA2PPrefPrefix + 125)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxUniLeftWidgetID,         kA2PPrefPrefix + 126)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxUniBottomWidgetID,       kA2PPrefPrefix + 127)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxUniRightWidgetID,        kA2PPrefPrefix + 128)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxCarEspecialLecturaEID,        kA2PPrefPrefix + 129)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxCadUnidadesLecturaEID,        kA2PPrefPrefix + 130)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTblMutRow1HeaderWidgetID,        kA2PPrefPrefix + 131)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonTabOrdCWidgetID,      kA2PPrefPrefix + 132)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonComaOrdCWidgetID,     kA2PPrefPrefix + 133)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonPyComaOrdCWidgetID,   kA2PPrefPrefix + 134)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonDosPuntosOrdCWidgetID,kA2PPrefPrefix + 135)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefRadioButtonEspecialOrdCWidgetID, kA2PPrefPrefix + 136)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxCarEspecialLecturaOrdCID,     kA2PPrefPrefix + 137)
/**********************ETIQUETAS*************************************/
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCreaEtiqConID,       kA2PPrefPrefix + 138)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxColEtiqConID,     kA2PPrefPrefix + 139)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxColTextEtiqConID, kA2PPrefPrefix + 140)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxTamMarcEtiqConID,      kA2PPrefPrefix + 141)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxCreaEtiqSinID,       kA2PPrefPrefix + 142)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxColEtiqSinID,     kA2PPrefPrefix + 143)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefComboBoxColTextEtiqSinID, kA2PPrefPrefix + 144)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxTamMarcEtiqSinID,      kA2PPrefPrefix + 145)
/********************ORDEN DE CAMPOS EN ETIQUETAS********************/
/***********CON PUBLICIDAD*****************************************/
DECLARE_PMID(kWidgetIDSpace, kA2PPrefMensaje1OrdenEtiqID,   kA2PPrefPrefix + 146)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiCon1ID, kA2PPrefPrefix + 147)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiCon2ID, kA2PPrefPrefix + 148)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiCon3ID, kA2PPrefPrefix + 149)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiCon4ID, kA2PPrefPrefix + 150)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiCon5ID, kA2PPrefPrefix + 151)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiCon6ID, kA2PPrefPrefix + 152)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiCon7ID, kA2PPrefPrefix + 153)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiCon8ID, kA2PPrefPrefix + 154)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiCon9ID, kA2PPrefPrefix + 155)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiConAID, kA2PPrefPrefix + 156)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiConBID, kA2PPrefPrefix + 157)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiConCID, kA2PPrefPrefix + 158)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiConDID, kA2PPrefPrefix + 159)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiConEID, kA2PPrefPrefix + 160)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiCon1ID, kA2PPrefPrefix + 161)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiCon2ID, kA2PPrefPrefix + 162)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiCon3ID, kA2PPrefPrefix + 163)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiCon4ID, kA2PPrefPrefix + 164)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiCon5ID, kA2PPrefPrefix + 165)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiCon6ID, kA2PPrefPrefix + 166)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiCon7ID, kA2PPrefPrefix + 167)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiCon8ID, kA2PPrefPrefix + 168)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiCon9ID, kA2PPrefPrefix + 169)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiConAID, kA2PPrefPrefix + 170)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiConBID, kA2PPrefPrefix + 171)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiConCID, kA2PPrefPrefix + 172)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiConDID, kA2PPrefPrefix + 173)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiConEID, kA2PPrefPrefix + 174)
/***********SIN PUBLICIDAD*****************************************/
DECLARE_PMID(kWidgetIDSpace, kA2PPrefMensaje2OrdenEtiqID,   			kA2PPrefPrefix + 175)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSin1ID, 			kA2PPrefPrefix + 176)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSin2ID, 			kA2PPrefPrefix + 177)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSin3ID, 			kA2PPrefPrefix + 178)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSin4ID, 			kA2PPrefPrefix + 179)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSin5ID, 			kA2PPrefPrefix + 180)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSin6ID, 			kA2PPrefPrefix + 181)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSin7ID, 			kA2PPrefPrefix + 182)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSin8ID, 			kA2PPrefPrefix + 183)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSin9ID, 			kA2PPrefPrefix + 184)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSinAID, 			kA2PPrefPrefix + 185)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSinBID, 			kA2PPrefPrefix + 189)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSinCID, 			kA2PPrefPrefix + 190)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSinDID, 			kA2PPrefPrefix + 191)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNumCamOrdEtiSinEID, 			kA2PPrefPrefix + 192)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSin1ID, 			kA2PPrefPrefix + 193)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSin2ID, 			kA2PPrefPrefix + 194)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSin3ID, 			kA2PPrefPrefix + 195)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSin4ID, 			kA2PPrefPrefix + 196)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSin5ID, 			kA2PPrefPrefix + 197)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSin6ID, 			kA2PPrefPrefix + 198)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSin7ID, 			kA2PPrefPrefix + 199)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSin8ID, 			kA2PPrefPrefix + 200)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSin9ID, 			kA2PPrefPrefix + 201)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSinAID,				kA2PPrefPrefix + 202)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSinBID, 			kA2PPrefPrefix + 203)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSinCID, 			kA2PPrefPrefix + 204)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSinDID,			 	kA2PPrefPrefix + 205)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxNomCamOrdEtiSinEID, 			kA2PPrefPrefix + 206)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefKeyLicenseDlgWidgetID,				kA2PPrefPrefix + 207) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefCkBoxMostrarDlgIssueWidgetID,		kA2PPrefPrefix + 208) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefLogoWidgetID,	 					kA2PPrefPrefix + 209) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefNameEditionTextEditWidgetID,	 	kA2PPrefPrefix + 211) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxDespVerWidgetID,	 			kA2PPrefPrefix + 213) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBoxDespHorWidgetID,				kA2PPrefPrefix + 214) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefA2PSecurityDialogWidgetID,			kA2PPrefPrefix + 215) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextKeyA2PWidgetID,	 			kA2PPrefPrefix + 216) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefTextKeyIDWidgetID,	 				kA2PPrefPrefix + 217) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefUserUIPwdDialogWidgetID,	 	kA2PPrefPrefix + 218) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefUserUIUsernameWidgetID,	 	kA2PPrefPrefix + 219) 
DECLARE_PMID(kWidgetIDSpace, kA2PPrefParagraphStyleNameToFolioWidgetID,	 kA2PPrefPrefix + 220)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefBorrarEtiqSDespuesUpdateWidgetID,	 kA2PPrefPrefix + 221)
DECLARE_PMID(kWidgetIDSpace, kA2PPrefUsarPlantilleroWidgetID,	 kA2PPrefPrefix + 222)
//Script Element IDs
//Events
DECLARE_PMID(kScriptInfoIDSpace, kA2PPrefEventScriptElement, kA2PPrefPrefix + 1)
// Properties
DECLARE_PMID(kScriptInfoIDSpace, kA2PPrefPropertyScriptElement, kA2PPrefPrefix + 2)

// Service IDs
DECLARE_PMID(kServiceIDSpace, kA2PPrefSelDlgService, kA2PPrefPrefix + 0)

// "About Plug-ins" sub-menu:
#define kA2PPrefAboutMenuKey		  kA2PPrefStringPrefix "kA2PPrefAboutMenuKey"
#define kA2PPrefAboutMenuPath		  kSDKDefStandardAboutMenuPath kA2PPrefCompanyKey
#define kA2PAboutMenuPath			  kSDKDefStandardAboutMenuPath kA2PPrefCompanyKey
#define kA2PPrefTabDialogMenuItemKey  kA2PPrefStringPrefix "kA2PPrefTabDialogMenuItemKey"

// "Plug-ins" sub-menu:
#define kA2PPrefPluginsMenuKey 		kA2PPrefStringPrefix "kA2PPrefPluginsMenuKey"
#define kA2PPrefPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kA2PPrefCompanyKey kSDKDefDelimitMenuPath kA2PPrefPluginsMenuKey

// Other StringKeys:
#define kA2PPrefAboutBoxStringKey		 kA2PPrefStringPrefix   "kA2PPrefAboutBoxStringKey"
#define kA2PPrefPanelTitleKey			 kA2PPrefStringPrefix   "kA2PPrefPanelTitleKey"
#define kA2PPrefStaticTextKey			 kA2PPrefStringPrefix   "kA2PPrefStaticTextKey"
#define kA2PPrefInternalPopupMenuNameKey kA2PPrefStringPrefix	"kA2PPrefInternalPopupMenuNameKey"
#define kA2PPrefTargetMenuPath           kA2PPrefInternalPopupMenuNameKey

// Menu item positions:

#define	kA2PPrefSeparator1MenuItemPosition		10.0
#define kA2PPrefAboutThisMenuItemPosition		11.0



#define kA2PPrefDialogTitleKey kA2PPrefStringPrefix "kA2PPrefDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kA2PPrefDialogMenuItemKey kA2PPrefStringPrefix "kA2PPrefDialogMenuItemKey"

#define kA2PPrefTituloDialogPrefStringKey		kA2PPrefStringPrefix "kA2PPrefTituloDialogPrefStringKey"
#define kA2PPrefBotonCancelarStringKey		kA2PPrefStringPrefix "kA2PPrefBotonCancelarStringKey"
#define kA2PPrefBotonAplicarStringKey		kA2PPrefStringPrefix "kA2PPrefBotonAplicarStringKey"
#define kA2PPrefPestanaDocumentoStringKey		kA2PPrefStringPrefix "kA2PPrefPestanaDocumentoStringKey"
#define kA2PPrefAnchoDocumentoStringKey		kA2PPrefStringPrefix "kA2PPrefAnchoDocumentoStringKey"
#define kA2PPrefAltoPaginaStringKey		kA2PPrefStringPrefix "kA2PPrefAltoPaginaStringKey"
#define kA2PPrefNumColumnasStringKey		kA2PPrefStringPrefix "kA2PPrefNumColumnasStringKey"
#define kA2PPrefMedianilStringKey		kA2PPrefStringPrefix "kA2PPrefMedianilStringKey"
#define kA2PPrefMargenSuperiorStringKey		kA2PPrefStringPrefix "kA2PPrefMargenSuperiorStringKey"
#define kA2PPrefMargenInferiorStringKey		kA2PPrefStringPrefix "kA2PPrefMargenInferiorStringKey"
#define kA2PPrefMargenIzquierdoStringKey		kA2PPrefStringPrefix "kA2PPrefMargenIzquierdoStringKey"
#define kA2PPrefMargenDerechoStringKey		kA2PPrefStringPrefix "kA2PPrefMargenDerechoStringKey"
#define kA2PPrefLabelPreferencesStringKey		kA2PPrefStringPrefix "kA2PPrefLabelPreferencesStringKey"
#define kA2PPrefBotonCambiarPrefStringKey		kA2PPrefStringPrefix "kA2PPrefBotonCambiarPrefStringKey"
#define kA2PPrefBotonCrearPrefStringKey		kA2PPrefStringPrefix "kA2PPrefBotonCrearPrefStringKey"
#define kA2PPrefBotonGuardarPrefStringKey		kA2PPrefStringPrefix "kA2PPrefBotonGuardarPrefStringKey"
#define kA2PPrefBotonBorrarPrefStringKey		kA2PPrefStringPrefix "kA2PPrefBotonBorrarPrefStringKey"
#define kA2PPrefBotonCancelarPrefStringKey		kA2PPrefStringPrefix "kA2PPrefBotonCancelarPrefStringKey"
#define kA2PPrefCambiarContrasenaStringKey		kA2PPrefStringPrefix "kA2PPrefCambiarContrasenaStringKey"
#define kA2PPrefTituloPestanaCapasStringKey		kA2PPrefStringPrefix "kA2PPrefTituloPestanaCapasStringKey"
#define kA2PPrefLabelPestanaCapasStringKey		kA2PPrefStringPrefix "kA2PPrefLabelPestanaCapasStringKey"
#define kA2PPrefCreaCapaEditorialStringKey		kA2PPrefStringPrefix "kA2PPrefCreaCapaEditorialStringKey"
#define kA2PPrefCreaCapaVisibleStringKey		kA2PPrefStringPrefix "kA2PPrefCreaCapaVisibleStringKey"
#define kA2PPrefCreaCapaBloqStringKey		kA2PPrefStringPrefix "kA2PPrefCreaCapaBloqStringKey"
#define kA2PPrefCreaCapaImagenStringKey		kA2PPrefStringPrefix "kA2PPrefCreaCapaImagenStringKey"
#define kA2PPrefCreaCapaEtiquetaConStringKey		kA2PPrefStringPrefix "kA2PPrefCreaCapaEtiquetaConStringKey"
#define kA2PPrefCreaCapaEtiquetaSinStringKey		kA2PPrefStringPrefix "kA2PPrefCreaCapaEtiquetaSinStringKey"
#define kA2PPrefTituloPestanaCajaImagenStringKey	kA2PPrefStringPrefix "kA2PPrefTituloPestanaCajaImagenStringKey"
#define kA2PPrefSinPublicidadStringKey				kA2PPrefStringPrefix "kA2PPrefSinPublicidadStringKey"
#define kA2PPrefConPublicidadStringKey				kA2PPrefStringPrefix "kA2PPrefConPublicidadStringKey"
#define kA2PPrefColordeFondoStringKey				kA2PPrefStringPrefix "kA2PPrefColordeFondoStringKey"
#define kA2PPrefColordeMarcoStringKey				kA2PPrefStringPrefix "kA2PPrefColordeMarcoStringKey"
#define kA2PPrefColordeTextoStringKey				kA2PPrefStringPrefix "kA2PPrefColordeTextoStringKey"

#define kA2PPrefAjusteImagenStringKey				kA2PPrefStringPrefix "kA2PPrefAjusteImagenStringKey"
#define kA2PPrefAjusteContenidoAFrameStringKey		kA2PPrefStringPrefix "kA2PPrefAjusteContenidoAFrameStringKey"
#define kA2PPrefAjusteCentrarStringKey				kA2PPrefStringPrefix "kA2PPrefAjusteCentrarStringKey"
#define kA2PPrefAjusteProporcionalmenteStringKey	kA2PPrefStringPrefix "kA2PPrefAjusteProporcionalmenteStringKey"
#define kA2PPrefNingunoStringKey					kA2PPrefStringPrefix "kA2PPrefNingunoStringKey"

#define kA2PPrefTituloPestanaImportStringKey		kA2PPrefStringPrefix "kA2PPrefTituloPestanaImportStringKey"
#define kA2PPrefLabelPathStringKey					kA2PPrefStringPrefix "kA2PPrefLabelPathStringKey"
#define kA2PPrefBotonSeleccionarStringKey			kA2PPrefStringPrefix "kA2PPrefBotonSeleccionarStringKey"
#define kA2PPrefLabelOrdenStringKey			kA2PPrefStringPrefix "kA2PPrefLabelOrdenStringKey"
#define kA2PPrefTabuladorStringKey			kA2PPrefStringPrefix "kA2PPrefTabuladorStringKey"
#define kA2PPrefComaStringKey			kA2PPrefStringPrefix "kA2PPrefComaStringKey"
#define kA2PPrefPyComaStringKey			kA2PPrefStringPrefix "kA2PPrefPyComaStringKey"
#define kA2PPrefDosPuntosStringKey			kA2PPrefStringPrefix "kA2PPrefDosPuntosStringKey"
#define kA2PPrefCaracterEspecialStringKey			kA2PPrefStringPrefix "kA2PPrefCaracterEspecialStringKey"
#define kA2PPrefSeparacionparaCamposStringKey			kA2PPrefStringPrefix "kA2PPrefSeparacionparaCamposStringKey"
#define kA2PPrefTituloPestanaOrdCoorStringKey		kA2PPrefStringPrefix "kA2PPrefTituloPestanaOrdCoorStringKey"
#define kA2PPrefLabelOrdenPlectCoordStringKey		kA2PPrefStringPrefix "kA2PPrefLabelOrdenPlectCoordStringKey"
#define kA2PPrefLabelSepDeCoordStringKey		kA2PPrefStringPrefix "kA2PPrefLabelSepDeCoordStringKey"
#define kA2PPrefTituloPestanaEtiquetasStringKey		kA2PPrefStringPrefix "kA2PPrefTituloPestanaEtiquetasStringKey"
#define kA2PPrefLabelOrdenPlectCoordStringKey		kA2PPrefStringPrefix "kA2PPrefLabelOrdenPlectCoordStringKey"
#define kA2PPrefTituloPestanaOrdenEtiqStringKey		kA2PPrefStringPrefix "kA2PPrefTituloPestanaOrdenEtiqStringKey"
#define kA2PPrefLabelOrdenEnEtiqCondStringKey		kA2PPrefStringPrefix "kA2PPrefLabelOrdenEnEtiqCondStringKey"
#define kA2PPrefLabelOrdenEnEtiqSindStringKey		kA2PPrefStringPrefix "kA2PPrefLabelOrdenEnEtiqSindStringKey"
#define kA2PPrefLabelPassActualStringKey		kA2PPrefStringPrefix "kA2PPrefLabelPassActualStringKey"
#define kA2PPrefLabelNewPassStringKey		kA2PPrefStringPrefix "kA2PPrefLabelNewPassStringKey"
#define kA2PPrefLabelRepitNewPassStringKey		kA2PPrefStringPrefix "kA2PPrefLabelRepitNewPassStringKey"
#define kA2PPrefLabelCrearEtiquetaStringKey		kA2PPrefStringPrefix "kA2PPrefLabelCrearEtiquetaStringKey"


//errores
#define kA2PPrefMensajeSerepitecampoStringKey		kA2PPrefStringPrefix "kA2PPrefMensajeSerepitecampoStringKey"
#define kA2PPrefMensajeSelecOrdenSepCoordStringKey			kA2PPrefStringPrefix "kA2PPrefMensajeSelecOrdenSepCoordStringKey"
#define kA2PPrefMensajeSelecTipoSepCoordStringKey			kA2PPrefStringPrefix "kA2PPrefMensajeSelecTipoSepCoordStringKey"
#define kA2PPrefMensajeSelecTipoSepCamposStringKey			kA2PPrefStringPrefix "kA2PPrefMensajeSelecTipoSepCamposStringKey"
#define kA2PPrefMensajeTeclearCarEspecialStringKey			kA2PPrefStringPrefix "kA2PPrefMensajeTeclearCarEspecialStringKey"
#define kA2PPrefMensajeNoPudoAbrirArchivoStringKey			kA2PPrefStringPrefix "kA2PPrefMensajeNoPudoAbrirArchivoStringKey"
#define kA2PPrefMensajeDebeSelecPrefStringKey			kA2PPrefStringPrefix "kA2PPrefMensajeDebeSelecPrefStringKey"
#define kA2PPrefMensajeSeguroDePrefStringKey			kA2PPrefStringPrefix "kA2PPrefMensajeSeguroDePrefStringKey"
#define kA2PPrefMensajeErrorDuranteBorradoStringKey			kA2PPrefStringPrefix "kA2PPrefMensajeErrorDuranteBorradoStringKey"
#define kA2PPrefMensajePrefBorradaStringKey			kA2PPrefStringPrefix "kA2PPrefMensajePrefBorradaStringKey"
#define kA2PPrefErrorLecturaVerArchDefaultStringKey			kA2PPrefStringPrefix "kA2PPrefErrorLecturaVerArchDefaultStringKey"
#define kA2PPrefSeguroDeReempPassStringKey			kA2PPrefStringPrefix "kA2PPrefSeguroDeReempPassStringKey"
#define kA2PPrefReeniciarPreferStringKey			kA2PPrefStringPrefix "kA2PPrefReeniciarPreferStringKey"
#define kA2PPrefPasswordIncorrectoStringKey			kA2PPrefStringPrefix "kA2PPrefPasswordIncorrectoStringKey"
#define kA2PPrefPasswordNoCoincidenStringKey			kA2PPrefStringPrefix "kA2PPrefPasswordNoCoincidenStringKey"

#define kA2PPrefNoSeEncontroLlaveStringKey			kA2PPrefStringPrefix "kA2PPrefNoSeEncontroLlaveStringKey"
#define kA2PPrefLlaveIncorrectaStringKey			kA2PPrefStringPrefix "kA2PPrefLlaveIncorrectaStringKey"


#define kA2PPrefVerificarNomNuevaPrefStringKey			kA2PPrefStringPrefix "kA2PPrefVerificarNomNuevaPrefStringKey"
#define kA2PPrefEscribeAquiNomPrefStringKey			kA2PPrefStringPrefix "kA2PPrefEscribeAquiNomPrefStringKey"

#define kA2PPrefArchivoExisteDeseaReemStringKey			kA2PPrefStringPrefix "kA2PPrefArchivoExisteDeseaReemStringKey"

#define kA2PPrefCampoNombreStringKey				kA2PPrefStringPrefix "kA2PPrefCampoNombreStringKey"
#define kA2PPrefCampoCoordenadasStringKey			kA2PPrefStringPrefix "kA2PPrefCampoCoordenadasStringKey"
#define kA2PPrefCampoPathStringKey					kA2PPrefStringPrefix "kA2PPrefCampoPathStringKey"
#define kA2PPrefPageOSectionStringKey				kA2PPrefStringPrefix "kA2PPrefPageOSectionStringKey"


#define kA2PPrefComunicDistribuidorStringKey		kA2PPrefStringPrefix "kA2PPrefComunicDistribuidorStringKey"
#define kA2PPrefCopiadeA2PStringKey				kA2PPrefStringPrefix "kA2PPrefCopiadeA2PStringKey"





//#define kA2PPrefAboutBoxStringKey		kA2PPrefStringPrefix "kA2PPrefAboutBoxStringKey"
#define kA2PPrefSelDlgDialogTitleKey			kA2PPrefStringPrefix "kA2PPrefSelDlgDialogTitleKey"
#define kA2PPrefDocPanelTitleKey				kA2PPrefStringPrefix "kA2PPrefDocPanelTitleKey"
#define kA2PPrefDocPanelStaticTextKey		    kA2PPrefStringPrefix "kA2PPrefDocPanelStaticTextKey"
#define kA2PPrefCapasPanelTitleKey				kA2PPrefStringPrefix "kA2PPrefCapasPanelTitleKey"
#define kA2PPrefCapasPanelStaticTextKey		kA2PPrefStringPrefix "kA2PPrefCapasPanelStaticTextKey"
/*******************************/
#define kA2PPrefCImagPanelTitleKey				kA2PPrefStringPrefix "kA2PPrefCImagPanelTitleKey"
#define kA2PPrefCImagPanelStaticTextKey		kA2PPrefStringPrefix "kA2PPrefCImagPanelStaticTextKey"
#define kA2PPrefImportPanelTitleKey				kA2PPrefStringPrefix "kA2PPrefImportPanelTitleKey"
#define kA2PPrefImportPanelStaticTextKey		kA2PPrefStringPrefix "kA2PPrefImportPanelStaticTextKey"

#define kA2PPrefOrdenCoorPanelTitleKey				kA2PPrefStringPrefix "kA2PPrefOrdenCoorPanelTitleKey"
#define kA2PPrefOrdenCoorPanelStaticTextKey		kA2PPrefStringPrefix "kA2PPrefOrdenCoorPanelStaticTextKey"


#define kA2PPrefEtiquetPanelTitleKey				kA2PPrefStringPrefix "kA2PPrefEtiquetPanelTitleKey"
#define kA2PPrefEtiquetPanelStaticTextKey		kA2PPrefStringPrefix "kA2PPrefEtiquetPanelStaticTextKey"
#define kA2PPrefOrdenPanelTitleKey				kA2PPrefStringPrefix "kA2PPrefOrdenPanelTitleKey"
#define kA2PPrefOrdenPanelStaticTextKey		kA2PPrefStringPrefix "kA2PPrefOrdenPanelStaticTextKey"

#define kA2PPrefBscPnlStaticTextKey				kA2PPrefStringPrefix	"kA2PPrefBscPnlStaticTextKey"

#define kA2PPrefDelClrDialogTextEditBoxKey		kA2PPrefStringPrefix "kA2PPrefDelClrDialogTextEditBoxKey"
#define kA2PPrefWinNewPwdPanelTitleKey		kA2PPrefStringPrefix "kA2PPrefWinNewPwdPanelTitleKey"
#define kA2PPrefUnidadesEnCoorLabelKey		kA2PPrefStringPrefix "kA2PPrefUnidadesEnCoorLabelKey"



#define kA2PPrefNameEditionStaticStringKey		kA2PPrefStringPrefix "kA2PPrefNameEditionStaticStringKey"
#define kA2PPrefClickOnCornerTipKey		kA2PPrefStringPrefix "kA2PPrefClickOnCornerTipKey"
#define kA2PPrefDesHorStaticStringKey		kA2PPrefStringPrefix "kA2PPrefDesHorStaticStringKey"
#define kA2PPrefDesVerStaticStringKey		kA2PPrefStringPrefix "kA2PPrefDesVerStaticStringKey"
#define kA2PPrefEtiquetaDeEjemploStaticStringKey		kA2PPrefStringPrefix "kA2PPrefEtiquetaDeEjemploStaticStringKey"
#define kA2PPrefLabelMostrarDlgIssueStringKey		kA2PPrefStringPrefix "kA2PPrefLabelMostrarDlgIssueStringKey"
#define kA2PPrefTextoContratoStringKey		kA2PPrefStringPrefix "kA2PPrefTextoContratoStringKey"
#define kA2PPrefLabelContratoStringKey		kA2PPrefStringPrefix "kA2PPrefLabelContratoStringKey"
#define kA2PPrefWarningPlgDemoStringKey		kA2PPrefStringPrefix "kA2PPrefWarningPlgDemoStringKey"

#define kA2PPrefAcceptContratoStringKey		kA2PPrefStringPrefix "kA2PPrefAcceptContratoStringKey"
#define kA2PPrefNoAcceptContratoStringKey		kA2PPrefStringPrefix "kA2PPrefNoAcceptContratoStringKey"
#define kA2PPrefNoExistenPreferenciasStringKey		kA2PPrefStringPrefix "kA2PPrefNoExistenPreferenciasStringKey"

//Changed on A2P v3.0
//#define kA2PPrefFontStaticStringKey		kA2PPrefStringPrefix "kA2PPrefFontStaticStringKey"
//#define kA2PPrefSizeFontStaticStringKey		kA2PPrefStringPrefix "kA2PPrefSizeFontStaticStringKey"
#define kA2PPrefParagraphStyleNameStringKey		kA2PPrefStringPrefix "kA2PPrefParagraphStyleNameStringKey"
#define kA2PPrefBorrarEtiqSDespuesUpdateStringKey		kA2PPrefStringPrefix "kA2PPrefBorrarEtiqSDespuesUpdateStringKey"
#define kA2PPrefInvalidKeyAccessStringKey		kA2PPrefStringPrefix "kA2PPrefInvalidKeyAccessStringKey"

#define kA2PPrefNoSeEncontroLlaveStringKey		kA2PPrefStringPrefix "kA2PPrefNoSeEncontroLlaveStringKey"
#define kA2PPrefLlaveIncorrectaStringKey		kA2PPrefStringPrefix "kA2PPrefLlaveIncorrectaStringKey"
#define kA2PPrefUsarPlantilleroStringKey		kA2PPrefStringPrefix "kA2PPrefUsarPlantilleroStringKey"
//#define kA2PImpInstallPnPlantilleroStringKey		kA2PPrefStringPrefix "kA2PImpInstallPnPlantilleroStringKey"


// "Plug-ins" sub-menu item position for dialog:
#define kA2PPrefDialogMenuItemPosition	12.0
#define kA2PPrefTabDialogMenuItemPosition	13.0


/********************************/
#define kA2PPrefPanelOrderingResourceID	700
#define kA2PPrefDialogResourceID			710
#define kA2PPrefTabDialogResourceID		711
#define kA2PPrefPanelImportarResourceID		712
#define kA2PPrefGuardarPrefResourceID	713
#define kA2PPrefNewPassResourceID		714
#define A2PPrefKeyLicenseDlgResourceID			715


#define kA2PPrefDocPanelResourceID				720
#define kA2PPrefDocPanelCreatorResourceID		730
#define kA2PPrefCapasPanelResourceID			740
#define kA2PPrefCapasPanelCreatorResourceID	750
/*******MIO**********/
#define kA2PPrefCImagPanelResourceID				755
#define kA2PPrefCImagPanelCreatorResourceID		760
#define kA2PPrefImportPanelResourceID				765
#define kA2PPrefImportPanelCreatorResourceID		770
#define kA2PPrefOrdenCoorPanelResourceID			775
#define kA2PPrefOrdenCoorPanelCreatorResourceID	780
#define kA2PPrefEtiquetPanelResourceID				785
#define kA2PPrefEtiquetPanelCreatorResourceID		790
#define kA2PPrefOrdenPanelResourceID				795
#define kA2PPrefOrdenPanelCreatorResourceID		800
#define kA2PPrefLogoResourceID		            1560  //recurso del logotipo
#define kA2PPrefSecurityDialogResourceID		2000
#define kA2PPrefContratoDialogResourceID		2100

#define kA2PPrefUserUIPwdDialogResourceID		2300
#define kA2PPrefDefPluginDependencyResourceID		2500


// Initial data format version numbers
#define kA2PPrefFirstMajorFormatNumber  RezLong(1)
#define kA2PPrefFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kA2PPrefCurrentMajorFormatNumber kA2PPrefFirstMajorFormatNumber
#define kA2PPrefCurrentMinorFormatNumber kA2PPrefFirstMinorFormatNumber

/*// ClassIDs:
 DECLARE_PMID(kClassIDSpace, kA2PPrefActionComponentBoss, kA2PPrefPrefix + 0)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 3)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 4)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 5)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 6)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 7)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 8)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 9)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 10)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 11)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 12)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 13)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 14)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 15)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 16)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 17)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 18)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 19)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 20)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 21)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 22)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 23)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 24)
 //DECLARE_PMID(kClassIDSpace, kA2PPrefBoss, kA2PPrefPrefix + 25)
 
 
 // InterfaceIDs:
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 0)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 1)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 2)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 3)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 4)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 5)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 6)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 7)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 8)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 9)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 10)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 11)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 12)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 13)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 14)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 15)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 16)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 17)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 18)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 19)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 20)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 21)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 22)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 23)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 24)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PPREFINTERFACE, kA2PPrefPrefix + 25)
 
 
 // ImplementationIDs:
 DECLARE_PMID(kImplementationIDSpace, kA2PPrefActionComponentImpl, kA2PPrefPrefix + 0 )
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 1)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 2)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 3)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 4)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 5)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 6)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 7)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 8)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 9)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 10)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 11)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 12)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 13)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 14)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 15)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 16)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 17)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 18)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 19)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 20)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 21)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 22)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 23)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 24)
 //DECLARE_PMID(kImplementationIDSpace, kA2PPrefImpl, kA2PPrefPrefix + 25)
 
 
 // ActionIDs:
 DECLARE_PMID(kActionIDSpace, kA2PPrefAboutActionID, kA2PPrefPrefix + 0)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 5)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 6)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 7)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 8)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 9)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 10)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 11)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 12)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 13)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 14)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 15)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 16)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 17)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 18)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 19)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 20)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 21)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 22)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 23)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 24)
 //DECLARE_PMID(kActionIDSpace, kA2PPrefActionID, kA2PPrefPrefix + 25)
 
 
 // WidgetIDs:
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 2)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 3)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 4)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 5)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 6)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 7)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 8)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 9)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 10)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 11)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 12)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 13)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 14)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 15)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 16)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 17)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 18)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 19)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 20)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 21)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 22)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 23)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 24)
 //DECLARE_PMID(kWidgetIDSpace, kA2PPrefWidgetID, kA2PPrefPrefix + 25)
 
 
 // "About Plug-ins" sub-menu:
 #define kA2PPrefAboutMenuKey			kA2PPrefStringPrefix "kA2PPrefAboutMenuKey"
 #define kA2PPrefAboutMenuPath		kSDKDefStandardAboutMenuPath kA2PPrefCompanyKey
 
 // "Plug-ins" sub-menu:
 #define kA2PPrefPluginsMenuKey 		kA2PPrefStringPrefix "kA2PPrefPluginsMenuKey"
 #define kA2PPrefPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kA2PPrefCompanyKey kSDKDefDelimitMenuPath kA2PPrefPluginsMenuKey
 
 // Menu item keys:
 
 // Other StringKeys:
 #define kA2PPrefAboutBoxStringKey	kA2PPrefStringPrefix "kA2PPrefAboutBoxStringKey"
 #define kA2PPrefTargetMenuPath kA2PPrefPluginsMenuPath
 
 // Menu item positions:
 
 
 // Initial data format version numbers
 #define kA2PPrefFirstMajorFormatNumber  RezLong(1)
 #define kA2PPrefFirstMinorFormatNumber  RezLong(0)
 
 // Data format version numbers for the PluginVersion resource 
 #define kA2PPrefCurrentMajorFormatNumber kA2PPrefFirstMajorFormatNumber
 #define kA2PPrefCurrentMinorFormatNumber kA2PPrefFirstMinorFormatNumber
 */
#endif // __A2PPrefID_h__

//  Code generated by DollyXs code generator
