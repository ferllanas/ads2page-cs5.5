//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/basicselectabledialog/N2P2SelDlgDialogObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:

#include "ITextControlData.h"
#include "IPanelControlData.h"
#include "IWidgetParent.h"
#include "IListBoxController.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "ISubject.h"
#include "ICoreFilename.h"
#include "IDialogController.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"

#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialogCreator.h"
#include "IDialog.h"
#include "IDropDownListController.h"
#include "IDropDownListController.h"
// Implementation includes:
#include "CAlert.h"
#include "CSelectableDialogObserver.h"
#include "SDKFileHelper.h"
//#include "PlatformFileSystemIterator.h"

#ifdef WINDOWS
	#include "..\Interlasa_common\PlatformFileSystemIterator.h"
#endif
#ifdef MACINTOSH
	#include "PlatformFileSystemIterator.h"
#endif

// Project includes:
#include "IA2PPreferenciasFunciones.h"
#include "A2PPrefActionsOnDLGPreferenciasUtils.h"
#include "FileTreeUtils.h"

#include "A2PPrefID.h"

/** This implementation subclasses CSelectableDialogObserver, not CObserver, 
	to help provide the dialog switching mechanism.  Note that 
	each of the methods in this class first call the method in the parent class, 
	CSelectableDialogObserver. (e.g. N2P2SelDlgDialogObserver::Update calls 
	CSelectableDialogObserver::Update)
	
	In addition, this implementation allows dynamic processing of the dialog's 
	info button. 
  	
	For the tabs to switch correctly, it is necessary to listen to kSelectedTabChangedMessage
	and react accordingly.  It is illustrated in the Update() method, and it is actually from
	production code of TabBasicSelectableDialogObserver class, since the header is private, we provide
	the implementation here for developer who might need it.  The previous version of this sample
	used an icon observer to dynamically process widget's state change, but it is impractical
	for plug-in that has many widgets to have one observer for each widget, thus we provide this
	dialog observer.

	@ingroup basicselectabledialog
	@author Lee Huang
*/
class N2P2SelDlgDialogObserver : public CSelectableDialogObserver
{
public: 
	/**	Constructor.
		@param boss interface ptr from boss object on which interface is aggregated.
	*/
	N2P2SelDlgDialogObserver(IPMUnknown* boss);

	/** Destructor. 
	 */
	virtual ~N2P2SelDlgDialogObserver() {}

	/**	Called by the host when the observed object changes. In this case: 
		the tab dialog's info button is clicked.  
		
		Since this class inherits CSelectableDialogObserver, it only handles the default list box widget, which
		is not used in the tab style. for tab style, we need to handle the kSelectedTabChangedMessage

		@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
		@param theSubject points to the ISubject interface for the subject that has changed.
		@param protocol specifies the ID of the changed interface on the subject boss.
		@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
	*/
	virtual void Update(const ClassID& theChange, 
						ISubject* theSubject, 
						const PMIID& protocol, 
						void* changedBy);

	/** Called by the application to allow the observer to attach to the 
		subjects to be observed. In this case the tab dialog's info button widget.
	*/
	virtual void AutoAttach();

	/** Called by the application to allow the observer to detach from the 
		subjects being observed. 
	*/
	virtual void AutoDetach();
	
private:

	void doSavePreferences();
	
	void doDeletePreferences();
	
	void doGuardarNuevaPreferencia();	
	
	void doCancelarNuevaPreferencia();
	
	void doNuevaPreferencia();
	
	void doAplicarPreferenciaSelectedToDlgPref();
	
	
	void UpdateComBoDePreferencias(PMString NomPref);
	
	
	void doOpenDlgCambiarPassword();
	
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(N2P2SelDlgDialogObserver, kA2PPrefDialogObserverImpl)


/* Constructor
*/
N2P2SelDlgDialogObserver::N2P2SelDlgDialogObserver(IPMUnknown* boss)
	: CSelectableDialogObserver(boss)
{
}



/* AutoAttach
*/
void N2P2SelDlgDialogObserver::AutoAttach()
{
	// Call base class AutoAttach() function so that default behavior
	// will still occur (selectable dialog listbox, OK and Cancel buttons, etc.).
	CSelectableDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("BscSlDlgDialogObserver::AutoAttach() panelControlData invalid");
			break;
		}

		// Now attach to BasicSelectableDialog's info button widget.
		AttachToWidget(kA2PPrefIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		AttachToWidget(kA2PPrefBotonAplyCamposPreviaID, IID_IBOOLEANCONTROLDATA, panelControlData);
		AttachToWidget(kA2PPrefBorrarPrefButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		
		AttachToWidget(kA2PPrefGuardarPrefButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		AttachToWidget(kA2PPrefCancelPrefButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		AttachToWidget(kA2PPrefButtonCreatNewPreWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		AttachToWidget(kA2PPrefButtonAplyNewPreWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		AttachToWidget(kA2PPrefCambiaPassButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		
		//AttachToWidget(kA2PPrefComboBoxSelecPreferID, IID_ISTRINGLISTCONTROLDATA, panelControlData); //OBSERVACION3 
		//ESTO ESTABA COMENTADP
		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/*	AutoDetach
*/
void N2P2SelDlgDialogObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur 
	// (selectable dialog listbox, OK and Cancel buttons, etc.).
	CSelectableDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("BscSlDlgDialogObserver::AutoDetach() panelControlData invalid");
			break;
		}

		// Now detach from BasicSelectableDialog's info button widget.
		DetachFromWidget(kA2PPrefIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		DetachFromWidget(kA2PPrefBotonAplyCamposPreviaID, IID_IBOOLEANCONTROLDATA, panelControlData);
		DetachFromWidget(kA2PPrefBorrarPrefButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		
		DetachFromWidget(kA2PPrefGuardarPrefButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		DetachFromWidget(kA2PPrefCancelPrefButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		DetachFromWidget(kA2PPrefButtonCreatNewPreWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		DetachFromWidget(kA2PPrefButtonAplyNewPreWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		DetachFromWidget(kA2PPrefCambiaPassButtonWidgetID, IID_IBOOLEANCONTROLDATA, panelControlData);
		//DetachFromWidget(kA2PPrefComboBoxSelecPreferID, IID_ISTRINGLISTCONTROLDATA, panelControlData);//OBSERVACION3 
		//ANTES COMENTADO
		// Detach from other widgets you handle dynamically here.

	} while (false);
}


/* Update
*/
void N2P2SelDlgDialogObserver::Update(const ClassID& theChange, 
									   ISubject* theSubject, 
									   const PMIID& protocol, 
									   void* changedBy)
{
	// Call base class Update() function so that default behavior
	// will still occur (selectable dialog listbox, OK and Cancel buttons, etc.).
	CSelectableDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	InterfacePtr<IControlView> view(theSubject, UseDefaultIID());
	if (view != nil)
	{
		do
		{
			
			InterfacePtr<ISelectableDialogSwitcher> panelSwitcher(this, IID_ISELECTABLEDIALOGSWITCHER);

			if (view->GetWidgetID() == panelSwitcher->GetSelectionListWidgetID() && theChange == kSelectedTabChangedMessage)
			{
				// Handle tab switching
				InterfacePtr<IPanelControlData> iPanelControlData(this, UseDefaultIID());
				InterfacePtr<IListBoxController> listCntl(iPanelControlData->FindWidget(panelSwitcher->GetSelectionListWidgetID()), IID_ILISTBOXCONTROLLER);
				panelSwitcher->SwitchDialogPanel(listCntl->GetSelected());
		
			}
			                              //"Apply"
			if (view->GetWidgetID() == kA2PPrefBotonAplyCamposPreviaID && theChange == kTrueStateMessage)
			{
				this->doSavePreferences();
				break;
			}
			
			if (view->GetWidgetID() == kA2PPrefBorrarPrefButtonWidgetID && theChange == kTrueStateMessage)
			{
				this->doDeletePreferences();
				break;
			}
			//"Save Prefs."
			if (view->GetWidgetID() == kA2PPrefGuardarPrefButtonWidgetID && theChange == kTrueStateMessage)
			{
				this->doGuardarNuevaPreferencia();
				break;	
			}
			
			if (view->GetWidgetID() == kA2PPrefCancelPrefButtonWidgetID && theChange == kTrueStateMessage)
			{
				this->doCancelarNuevaPreferencia();	
				break;
			}
			                            //BOTON SALVAR PREFERENCIAS
			if (view->GetWidgetID() == kA2PPrefButtonCreatNewPreWidgetID && theChange == kTrueStateMessage)
			{
				this->doNuevaPreferencia();
				break;
			}                          //"Switch Prefs."
			if (view->GetWidgetID() == kA2PPrefButtonAplyNewPreWidgetID && theChange == kTrueStateMessage)
			{
				// Bring up the About box.
				
				this->doAplicarPreferenciaSelectedToDlgPref();
			}
			if (view->GetWidgetID() == kA2PPrefCambiaPassButtonWidgetID && theChange == kTrueStateMessage)
			{
				// Bring up the About box.
				this->doOpenDlgCambiarPassword();
			}
			
		}while(false);
		
	}
}


//METODO QUE GUARDA LAS PREFERENCIAS
void N2P2SelDlgDialogObserver::doSavePreferences()
{
	do
	{
	
		Preferencias Pref;
		
		InterfacePtr<IControlView>	DlgPrefView(this, UseDefaultIID()) ;
		if(DlgPrefView==nil)
		{
			CAlert::InformationAlert("No se encontro el DlgPrefView");
			break;
		}
					
		InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
			(
				kA2PPreferenciasFuncionesBoss,	// Object boss/class
				IA2PPreferenciasFunctions::kDefaultIID
			)));
		
		if(A2PPrefeFuntions==nil)
		{
			break;
		}
		
		if((A2PPrefActionsOnDLGPreferenciasUtils::LlenarPreferenciasDeDialogoPref(DlgPrefView, Pref))==kFalse)
		{
			break;
		}
		
		//A2PPrefeFuntions->ConvertirUnidadesdePreferenciasAPuntos(Pref);
		A2PPrefeFuntions->EscribeDatosOnDocument("Default",Pref);
		
		PMString NomPref=Pref.Nombrepreferencias.GrabCString();
		if(Pref.Nombrepreferencias.NumUTF16TextChars()>0)

		A2PPrefeFuntions->EscribeDatosOnDocument(NomPref,Pref);
		
		A2PPrefActionsOnDLGPreferenciasUtils::CopiaCamposAImportarParaOrdenCamposEtiq(DlgPrefView, Pref);
		
		A2PPrefActionsOnDLGPreferenciasUtils::ValidaNumeracionDeCampos(DlgPrefView,Pref);
		
	}while(false);
}

void N2P2SelDlgDialogObserver::doDeletePreferences()
{
	PMString fFilter("\\*.pfa");
	do
	{
		InterfacePtr<IControlView>	DlgPrefView(this, UseDefaultIID()) ;
		if(DlgPrefView==nil)
		{
			CAlert::InformationAlert("No se encontro el DlgPrefView");
			break;
		}
		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{CAlert::InformationAlert("no encontro un parent");
			break;
		}
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("No encontro un  panel");
			break;
		}

		//obtengo una interfaz del la caja de vista previa   combo del nombre de las prefere
		InterfacePtr<IControlView>editBoxView( panel->FindWidget(kA2PPrefComboBoxSelecPreferID), UseDefaultIID() );
		if(editBoxView==nil)
		{
			CAlert::InformationAlert("no encontro el editBoxView UNIDADES DE MEDICION");
			break;
		}

			
		//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
		InterfacePtr<ITextControlData>	TextoDeDireccion( editBoxView, UseDefaultIID());
		if(TextoDeDireccion==nil)
		{
			CAlert::InformationAlert("No encontro un texto de direccion");
			break;
		}

		//Extraigo el texto del Widget
		PMString Archivo = TextoDeDireccion->GetString();
		//CAlert::InformationAlert("Archivo");
		if(Archivo.NumUTF16TextChars()==0)
		{
			CAlert::ErrorAlert(kA2PPrefMensajeDebeSelecPrefStringKey);
			//CAlert::ErrorAlert("Debe seleccionar una preferencia..");
			break;
		}
			
		PMString PathPMS=FileTreeUtils::CrearFolderPreferencias();	
		PathPMS.Append(Archivo);
		PathPMS.Append(".pfa");
		
		PMString Path=PathPMS.GrabCString();
		const int ActionAgreedValue=1;
		int32 result=CAlert::ModalAlert(kA2PPrefMensajeSeguroDePrefStringKey,
			kYesString, 
			kNoString,
			kNullString,
			ActionAgreedValue,CAlert::eQuestionIcon);
		if(ActionAgreedValue==result)
		{
			
			InterfacePtr<ICoreFilename> cfn((ICoreFilename*)::CreateObject(kCoreFilenameBoss,IID_ICOREFILENAME));
			IDFile file=SDKUtilities::PMStringToSysFile(&PathPMS);
			if(cfn->Initialize(&file)==0)
			{
				if(cfn->Delete()==0)
					CAlert::InformationAlert(kA2PPrefMensajePrefBorradaStringKey);
				else
					CAlert::InformationAlert(kA2PPrefMensajeErrorDuranteBorradoStringKey);			
			}
				
			
			
			editBoxView->Show(kTrue);

			InterfacePtr<IDialogController> Dialog(DlgPrefView,IID_IDIALOGCONTROLLER) ;
			if(Dialog==nil)
			{
				CAlert::InformationAlert("No se encontro el dialogo");
				break;
			}
			InterfacePtr<IStringListControlData> dropListData(Dialog->QueryListControlDataInterface(kA2PPrefComboBoxSelecPreferID));
			if (dropListData == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			///borrado de la lista al inicializar el combo

			dropListData->Clear(kFalse, kFalse);//lIMPIA LA LISTA ACTUAL //OBSERVACION2
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////COMENTARIO ES MUY EXTRA�O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//this->UpdateComBoDePreferencias("");
			
			PathPMS=FileTreeUtils::CrearFolderPreferencias();

			if(PathPMS.IsEmpty() == kTrue)
			{
				break;
			}
			SDKFileHelper rootFileHelper(PathPMS);
			IDFile rootSysFile = rootFileHelper.GetIDFile();
			PlatformFileSystemIterator iter;
			if(!iter.IsDirectory(rootSysFile))
			{
				break;
			}

		//	#ifdef WINDOWS
				// Windows dir iteration a little diff to Mac
		//		rootSysFile.Append(fFilter);
		//	#endif
						iter.SetStartingPath(rootSysFile);
					
					IDFile sysFile;
					bool16 hasNext= iter.FindFirstFile(sysFile,fFilter);
					while(hasNext)
					{
						SDKFileHelper fileHelper(sysFile);
						PMString truncP = FileTreeUtils::TruncatePath(fileHelper.GetPath());
						if(FileTreeUtils::validPath(truncP) && truncP.Contains(".pfa") )
						{
							truncP=FileTreeUtils::TruncateExtencion(truncP);
							dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
						}
						hasNext= iter.FindNextFile(sysFile);
					}
			int32 posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
			if(posicion!=-1)
				dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
			posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
			if(posicion!=-1)
				dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			
			///Verifico la cantidad dce elementos del combo box
			int32 CantElemEnLista=dropListData->Length();
			if(CantElemEnLista>0)
			{
				Archivo=dropListData->GetString(0);///obtengo el nombre de la preferencia que se encuentra en primer lugar del Combo
				//////poner la primera preferencia en el combo (Como seleccionada)////////
				//////y aplicar preferencias/////////
				PMString FilePreference=FileTreeUtils::CrearFolderPreferencias();
				FilePreference.Append(Archivo);
				FilePreference.Append(".pfa");
			
				//CAlert::InformationAlert(FilePreference);
				A2PPrefActionsOnDLGPreferenciasUtils::LlenaDlgPreferenciasXNombrePref(DlgPrefView,FilePreference);
			}
			Dialog->SetInitialized(kTrue);
		}
			
	}while(false);
}


//METODO PARA GUARDAR NUEVAS PREFERENCIAS..........

void N2P2SelDlgDialogObserver::doGuardarNuevaPreferencia()
{
	//FILE *Arch;
	Preferencias Pref;
	do
	{
		
		InterfacePtr<IControlView>	DlgPrefView(this, UseDefaultIID()) ;
		if(DlgPrefView==nil)
		{
			CAlert::InformationAlert("No se encontro el DlgPrefView");
			break;
		}
		
		
		InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
			(
				kA2PPreferenciasFuncionesBoss,	// Object boss/class
				IA2PPreferenciasFunctions::kDefaultIID
			)));

		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}

		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;
		}
							
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		editBoxView( panel->FindWidget(kA2PPrefTextNomNewPrefWidgetID), UseDefaultIID() );
		if(editBoxView==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
		InterfacePtr<ITextControlData>	Nombre_Preferencia( editBoxView, UseDefaultIID());
		if(Nombre_Preferencia==nil)
		{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}
		//Extraigo el texto del Widget		
		PMString NomNuevaPrefPMS = Nombre_Preferencia->GetString();
		//NombrePref=NomNuevaPrefPMS.GrabCString();
		if(NomNuevaPrefPMS=="Escribe aqui el nombre de la nueva preferencia" || NomNuevaPrefPMS=="Write down the new preference name")
		{
			CAlert::InformationAlert(kA2PPrefVerificarNomNuevaPrefStringKey);
			break;
		}
		
		if(NomNuevaPrefPMS.NumUTF16TextChars()<1)
		{
			CAlert::InformationAlert(kA2PPrefVerificarNomNuevaPrefStringKey);
			break;
		}

			
		PMString PathPMS=FileTreeUtils::CrearFolderPreferencias();
		PathPMS.Append(NomNuevaPrefPMS);
		PathPMS.Append(".pfa");

		//la adicion de los complementos unicamente se puede utilizar 
		//por medio de un + en una variable del tipo PMString  en una 
		//variable char * o CString no se puede realizar esta operacion
		PMString NombreDePreferenciaCS=PathPMS.GrabCString();//Transformacion de PMString a CString
					
		if(SDKUtilities::FileExistsForRead(NombreDePreferenciaCS)==kSuccess)//if ((Arch=fopen(NombreDePreferenciaCS.GrabCString(),"r"))!=NULL)//Si El archivo ya existe
		{	//El archivo ya existe
			const int ActionAgreedValue=1;//Variable para la ccion que se realiza en el siguiente 
			//DialogModal como pregunta
			int32 result=CAlert::ModalAlert(kA2PPrefArchivoExisteDeseaReemStringKey,
			kYesString, //Para un Si
			kNoString,	//Para un no
			kNullString,//para una entrada nulla o un click en la x de salir del dialogo
			ActionAgreedValue,CAlert::eQuestionIcon);//el icono que paparecera en el dialogo
			if(ActionAgreedValue==result)//si se oprimio aceptar
			{//se oprimio el boton aceptar
				
				//fclose(Arch);//Cierra el archivo
				
				/////////////////////////////////////////////////////////////
				//////extraer todos los datos de nuestras preferencias///////
				/////////////////////////////////////////////////////////////
				if((A2PPrefActionsOnDLGPreferenciasUtils::LlenarPreferenciasDeDialogoPref(DlgPrefView, Pref))==kFalse)
				{
					break;
				}
				
				//Se leen las preferencias de los widgets

				if(A2PPrefActionsOnDLGPreferenciasUtils::ValidaNumeracionDeCampos(DlgPrefView,Pref)!=kDefaultWidgetId)//Valido los numero de los campos
				{
					break;
				}
				
				//A2PPrefeFuntions->ConvertirUnidadesdePreferenciasAPuntos(Pref);
				A2PPrefeFuntions->EscribeDatosOnDocument(NomNuevaPrefPMS,Pref);//Llamada a la funcion en donde Se escriben los datos o preferencias en el archivo
				
				UpdateComBoDePreferencias(Nombre_Preferencia->GetString());//Cambia el contenido del Combo de preferencias puesto que existe una nueva preferencia
				this->doCancelarNuevaPreferencia();
			
			}
			else////////NO DESEA GURDAR LA PREFERENCIA QUE YA EXISTE
			{
			
				this->doCancelarNuevaPreferencia();
			
			}///if else
		}///if arch
		else//Cando el archivo no existe
		{
			
			InterfacePtr<IDialogController> Dialog(this, IID_IDIALOGCONTROLLER);
			if(Dialog==nil)
			{
				CAlert::InformationAlert("No se encontro el dialogo");
				break;
			}
			                                                                                 
			InterfacePtr<IStringListControlData> dropListData(Dialog->QueryListControlDataInterface(kA2PPrefComboBoxSelecPreferID));
			if (dropListData == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}

			dropListData->AddString(NomNuevaPrefPMS, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
			
			
		InterfacePtr<IControlView>CVComboBoxSelecPrefer(panel->FindWidget(kA2PPrefComboBoxSelecPreferID), UseDefaultIID() );
			if(CVComboBoxSelecPrefer==nil)
			{
				CAlert::InformationAlert("No se encontro el editbox");
				break;
			}
								
			InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( CVComboBoxSelecPrefer, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				CAlert::InformationAlert("No se encontro el texto de direccion");
				break;
			}

			IDDLDrComboBoxSelecPrefer->Select(dropListData->Length()-1);
			//////////////////////////////////////////////////////////////
			////////CREAR UNA NUEVA PREFERENCIA/////////////////////////////
			if((A2PPrefActionsOnDLGPreferenciasUtils::LlenarPreferenciasDeDialogoPref(DlgPrefView, Pref))==kFalse)
			{
				break;
			}
				
			//Se leen las preferencias de los widgets
			
			if(A2PPrefActionsOnDLGPreferenciasUtils::ValidaNumeracionDeCampos(DlgPrefView,Pref)!=kDefaultWidgetId)//Valido los numero de los campos
			{
				break;
			}
			
			//A2PPrefeFuntions->ConvertirUnidadesdePreferenciasAPuntos(Pref);
			
			A2PPrefeFuntions->EscribeDatosOnDocument(NomNuevaPrefPMS, Pref);//grabar nuevo documento de preferencias
			
			UpdateComBoDePreferencias(Nombre_Preferencia->GetString());//Cambia el contenido del Combo box puesto que ya existe una nueva preferencia
			this->doCancelarNuevaPreferencia();
		}			
	}while(false);//do

}


void N2P2SelDlgDialogObserver::UpdateComBoDePreferencias(PMString NomPref)
{
	PMString fFilter("\\*.pfa");
	PMString Archivo;
	PMString PathPMS;
	CString Path="";
	PMString Arc;
	int posicion;
		do
		{
			InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
			if(myParent==nil)
			{
				CAlert::InformationAlert("No se3 encontro el parent");
				//break;
			}
			//Obtengo la inerfaz del panel que se encuentra abierto
			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panel==nil)
			{
				CAlert::InformationAlert("No se encontro el panel");
				//break;
			}

			InterfacePtr<IWidgetParent>	 dialog(panel, UseDefaultIID());				
			if(myParent==nil)
			{
				CAlert::InformationAlert("No encontro el parent del dialogo");
				break;
			}
			InterfacePtr<IDialogController> Dialog(this, IID_IDIALOGCONTROLLER);
			if(Dialog==nil)
			{
				CAlert::InformationAlert("No se encontro el dialogo");
				break;
			}
			InterfacePtr<IStringListControlData> dropListData(Dialog->QueryListControlDataInterface(kA2PPrefComboBoxSelecPreferID));
			if (dropListData == nil)
			{
				ASSERT_FAIL("No pudo Obtener IStringListControlData*");
				break;
			}
			///borrado de la lista al inicializar el combo
			dropListData->Clear(kFalse, kFalse);//lIMPIA LA LISTA ACTUAL
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			//////COMENTARIO ES MUY EXTRA�O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
			////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
	/*		InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
				(
					kA2PPrefRegEditUtilitiesBoss,	// Object boss/class
					IRegEditUtilities::kDefaultIID
				)));*/
				 
			PathPMS=FileTreeUtils::CrearFolderPreferencias();

			if(PathPMS.IsEmpty() == kTrue)
			{
				break;
			}
			SDKFileHelper rootFileHelper(PathPMS);
			IDFile rootSysFile = rootFileHelper.GetIDFile();
			PlatformFileSystemIterator iter;
			if(!iter.IsDirectory(rootSysFile))
			{
				break;
			}

		//	#ifdef WINDOWS
				// Windows dir iteration a little diff to Mac
		//		rootSysFile.Append(fFilter);
		//	#endif
						iter.SetStartingPath(rootSysFile);
					
					IDFile sysFile;
					bool16 hasNext= iter.FindFirstFile(sysFile,fFilter);
					while(hasNext)
					{
						SDKFileHelper fileHelper(sysFile);
						PMString truncP = FileTreeUtils::TruncatePath(fileHelper.GetPath());
						if(FileTreeUtils::validPath(truncP) && truncP.Contains(".pfa") )
						{
							truncP=FileTreeUtils::TruncateExtencion(truncP);
							dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
						}
						hasNext= iter.FindNextFile(sysFile);
					}

			posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
			if(posicion!=-1)
				dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
			posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
			if(posicion!=-1)
				dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			posicion=dropListData->GetIndex(NomPref);//busco la ultima preferencia con que se trabajo

			Dialog->SetInitialized(kTrue);	
			//para posicionar la ultima preferencia
			InterfacePtr<IControlView>		CVComboBoxSelecPrefer(panel->FindWidget(kA2PPrefComboBoxSelecPreferID), UseDefaultIID() );
			if(CVComboBoxSelecPrefer==nil)
			{
				CAlert::InformationAlert("No se encontro el editbox");
				break;
			}
								
			InterfacePtr<IDropDownListController>	IDDLDrComboBoxSelecPrefer( CVComboBoxSelecPrefer, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLDrComboBoxSelecPrefer==nil)
			{
				CAlert::InformationAlert("No se encontro el texto de direccion");
				break;
			}
			
			if(posicion >= 0)
				IDDLDrComboBoxSelecPrefer->Select(posicion);
			else
				IDDLDrComboBoxSelecPrefer->Select(0);

		}while(false);
}


/**
*/
void N2P2SelDlgDialogObserver::doCancelarNuevaPreferencia()
{
	// Obtiene los servicios registrados, incluyendo nuestro servicio del dialogo selectable: 
	// esto es con el fin de que existan servicio registrados
	do
	{		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}
		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;										
		}	
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		editBoxView( panel->FindWidget(kA2PPrefTextNomNewPrefWidgetID), UseDefaultIID() );
		if(editBoxView==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}												
		editBoxView->Hide();//Abilita el textbox del nombre de la nueva preferencia
		IControlView*	Boton_Crear= panel->FindWidget(kA2PPrefButtonCreatNewPreWidgetID);
		if(Boton_Crear==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		Boton_Crear->Enable(kTrue);
		//Abilita el textbox del nombre de la nueva preferencia
		IControlView*	Boton_borrar= panel->FindWidget(kA2PPrefBorrarPrefButtonWidgetID);
		if(Boton_borrar==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		Boton_borrar->Show(kTrue);//Abilita el boton Aceptar
		//
		IControlView*	Boton_Cancelar= panel->FindWidget(kA2PPrefCancelPrefButtonWidgetID);
		if(Boton_Cancelar==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		Boton_Cancelar->Hide();//Abilita el boton Cancelar
		//"Save Prefs."
		IControlView*	Boton_Guardar= panel->FindWidget(kA2PPrefGuardarPrefButtonWidgetID);
		if(Boton_Guardar==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		Boton_Guardar->Disable(kTrue);//Abilita el textbox del nombre de la nueva preferencia
		//////desabilita la caja del nombre de la nueva preferencia					
		editBoxView->Hide();//Abilita el textbox del nombre de la nueva preferencia
	}while(false);	
}


void N2P2SelDlgDialogObserver::doNuevaPreferencia()
{
	do
	{	///Se obtiene el parent de la ventana
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}

		//Obtengo la inerfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;
		}
								
		//obtengo una interfaz del la caja del nombre de la nueva preferencia
		IControlView*	editBoxView= panel->FindWidget(kA2PPrefTextNomNewPrefWidgetID);
		if(editBoxView==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
					
		//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
		InterfacePtr<ITextControlData>	TextoDeDireccion( editBoxView, UseDefaultIID());
		if(TextoDeDireccion==nil)
		{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}

		//Extraigo el texto del Widget
		TextoDeDireccion->SetString(kA2PPrefEscribeAquiNomPrefStringKey);
		editBoxView->Show(kTrue);//Abilita el textbox del nombre de la nueva preferencia
		//////inhabitar botones cancelar y aceptar 
		IControlView*	Boton_Crear= panel->FindWidget(kA2PPrefButtonCreatNewPreWidgetID);
		if(Boton_Crear==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		Boton_Crear->Disable(kTrue);//Abilita el textbox del nombre de la nueva preferencia
		//
		IControlView*	Boton_Borrar= panel->FindWidget(kA2PPrefBorrarPrefButtonWidgetID);
		if(Boton_Borrar==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		Boton_Borrar->Hide();//Abilita el textbox del nombre de la nueva preferencia
		IControlView*	Boton_Cancelar= panel->FindWidget(kA2PPrefCancelPrefButtonWidgetID);
		if(Boton_Cancelar==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		Boton_Cancelar->Show(kTrue);//Abilita el textbox del nombre de la nueva preferencia
		//"Save Prefs."
		IControlView*	Boton_Guardar= panel->FindWidget(kA2PPrefGuardarPrefButtonWidgetID);
		if(Boton_Guardar==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		Boton_Guardar->Enable(kTrue);//Abilita el textbox del nombre de la nueva preferencia
	}while(false);
}


void N2P2SelDlgDialogObserver::doAplicarPreferenciaSelectedToDlgPref()
{
	do
	{
		
		InterfacePtr<IControlView>		DlgPrefView(this, UseDefaultIID());				
		if(DlgPrefView==nil)
		{
			CAlert::InformationAlert("No se3 encontro el DlgPrefView");
			break;
		}
		
		
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}
		
		
		//Obtengo la interfaz del panel que se encuentra abierto
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;
		}
		
		
		//obtengo una interfaz del la caja de vista previa  //UNIDADES DE MEDICION
		//InterfacePtr<IControlView>	editBoxView( panel->FindWidget(kA2PPrefComboBoxSelecPreferID), UseDefaultIID() );//ORIGINAL
		IControlView* editBoxView= panel->FindWidget(kA2PPrefComboBoxSelecPreferID);
		if(editBoxView==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		
		
		//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
		InterfacePtr<ITextControlData>	TextoDeDireccion( editBoxView, UseDefaultIID());
		if(TextoDeDireccion==nil)
		{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}
		
		
		PMString NombreSeletedPreference=TextoDeDireccion->GetString();
		//CAlert::InformationAlert("CADENA PREFERENCIA    "+NombreSeletedPreference);
		
		
		PMString FilePreference=FileTreeUtils::CrearFolderPreferencias();
		
		
		FilePreference.Append(NombreSeletedPreference);
		
		
		FilePreference.Append(".pfa");
			
		
		//CAlert::InformationAlert(FilePreference);
		A2PPrefActionsOnDLGPreferenciasUtils::LlenaDlgPreferenciasXNombrePref(DlgPrefView,FilePreference);
		/*if(!);
			CAlert::InformationAlert("Salio por algun motivo  " + FilePreference);
		*/	
	}while(false);
}

void N2P2SelDlgDialogObserver::doOpenDlgCambiarPassword()
{
	
	bool16 retval=kFalse;
	do
		{
			//Obtengo la interfaz IK2ServiceRegistry de la actual sesion
			InterfacePtr<IK2ServiceRegistry> sRegistry(GetExecutionContextSession(), UseDefaultIID());
			if (sRegistry == nil)
			{	
				//*si no se encontro ningun  servicio registrado sale de la funcion y realiza nada
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: sRegistry invalid");
				break;
			}
			//obtengo un classID del jefe que se creo previamente en el archivo SelDlg.fr
			ClassID	classID=kA2PPrefNewPassDialogBoss;

			//busco una interfaz IK2ServiceProvider a partir del registro de Servicio el cual busca la implementacion y su calssID
			InterfacePtr<IK2ServiceProvider> sdService(sRegistry->QueryServiceProviderByClassID(kSelectableDialogServiceImpl, classID));
			if (sdService == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: sdService invalid");
				break;
			}

			// This is an interface to our service, which will end up calling our 
			// plug-in's dialog creator implementation:
			//*Esto es un interfaz a nuestro servicio, que terminar� encima de 
			//la llamada a la implementacion del creador del di�logo de nuestro plug-in: 
			InterfacePtr<IDialogCreator> dialogCreator(sdService, IID_IDIALOGCREATOR);
			if (dialogCreator == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogCreator invalid");
				break;
			}
			
			// This is our CreateDialog, which loads the dialog from our resource:
			//Este es nuestro creador de dialogos, que cargara el dialogo de nuestro recurso
			IDialog* dialog = dialogCreator->CreateDialog();
			if (dialog == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: could not create dialog");
				break;
			}

			// The panel data will be the selectable panel of the dialog:
			//los datos del panel sea el panel seleccionable del di�logo
			InterfacePtr<IPanelControlData> panelData(dialog, UseDefaultIID());
			if (panelData == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
				break;
			}
			//Busqueda del panel:
			WidgetID	widgetID = kA2PPrefNewPassWidgetID;
			
			
			IControlView* dialogView = panelData->FindWidget(widgetID);
			if (dialogView == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogView invalid");
				break;
			}
			
			// The dialog switcher controls how the panels are switched between:
			//los controles del switcher del di�logo c�mo los paneles se cambian en medio: 
			InterfacePtr<ISelectableDialogSwitcher> 
				dialogSwitcher(dialogView, IID_ISELECTABLEDIALOGSWITCHER);
			if (dialogSwitcher == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogSwitcher invalid");
				break;
			}
		
			dialogSwitcher->SetDialogServiceID(kA2PPrefSelDlgService);

			//Abre el dialogo
			dialog->Open(); 
			retval=kTrue;
		}while(false);	

}
// End, N2P2SelDlgDialogObserver.cpp