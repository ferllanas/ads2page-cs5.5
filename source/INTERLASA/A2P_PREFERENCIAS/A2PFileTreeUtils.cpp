//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/FileTreeUtils.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interface includes
//#include "IImportFileCmdData.h"
#include "IDocument.h"
#include "IPlaceGun.h"
#include "IPanelControlData.h"

// Other API includes
#include "K2SmartPtr.h"
#include "IPalettePanelUtils.h"
#include "TextChar.h"
#include "SDKUtilities.h"
#include "SDKFileHelper.h"
#include "FileUtils.h"
// Project includes
#include "FileTreeUtils.h"
#include "SysFileList.h"
//a�adimos
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h> 
#include <sys/stat.h> 
#include <time.h> 
#include "CmdUtils.h"
#include "SysFileList.h"
#include "IWidgetParent.h"
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "CAlert.h"
#ifdef WINDOWS
#define PLATFORM_PATH_DELIMITER kTextChar_ReverseSolidus
#include "..\Interlasa_common\InterlasaRegEditUtilities.h" //SE LO AGREGUE
#endif
#ifdef MACINTOSH
#define PLATFORM_PATH_DELIMITER kTextChar_Colon
#endif


/* TruncatePath
*/
PMString FileTreeUtils::TruncatePath(const PMString& fromthis)
{
	if(fromthis.IsEmpty() == kTrue) return PMString("Empty!");

	PMString retval = fromthis;
	int32 lastpos = (-1);
	for (int32 i = 0 ; i < fromthis.CharCount();i++)
	{
		bool16 predicate = (fromthis[i] == PLATFORM_PATH_DELIMITER);
		if (predicate)
		{
			lastpos = i;
		}
	}

	if(lastpos >= 0)
	{
		// Suppose we have ../X/Y.gif
		// Then, lastpos would be 4, and we'd want 5 chars from 5 onwards
		int32 countChars = fromthis.CharCount() - (lastpos+1);
		int32 startIndex = lastpos+1;
		int32 endIndex = (startIndex + countChars);
		if((endIndex > startIndex) && (endIndex <= fromthis.CharCount()))
		{
			K2::scoped_ptr<PMString> ptrRightStr(fromthis.Substring(startIndex, countChars));
			if(ptrRightStr)
			{
				retval = *ptrRightStr;
			}
		}
	} 
	return retval;
}

/* TruncatePath
*/
PMString FileTreeUtils::TruncateExtencion(const PMString& fromthis)
{
	if(fromthis.IsEmpty() == kTrue) return PMString("Empty!");

	PMString retval = fromthis;
	int32 lastpos = (-1);
	lastpos=fromthis.IndexOfString("."); 

	if(lastpos >= 0)
	{
		// Suppose we have ../X/Y.gif
		// Then, lastpos would be 4, and we'd want 5 chars from 5 onwards
		int32 countChars = lastpos;
		int32 startIndex = 0;
		int32 endIndex = (startIndex + countChars);
		if((endIndex > startIndex) && (endIndex <= fromthis.CharCount()))
		{
			K2::scoped_ptr<PMString> ptrRightStr(fromthis.Substring(startIndex, countChars));
			if(ptrRightStr)
			{
				retval = *ptrRightStr;
			}
		}
	} 
	return retval;
}


/* ImportImageAndLoadPlaceGun

UIDRef FileTreeUtils::ImportImageAndLoadPlaceGun(
	const UIDRef& docUIDRef, const PMString& fromPath)
{
	// Precondition: active document
	// Precondition: file exists
	// 
	UIDRef retval = UIDRef::gNull;
	do
	{
		SDKFileHelper fileHelper(fromPath);
		bool16 fileExists = fileHelper.IsExisting();
		ASSERT(fileExists);
		if(!fileExists)
		{
			break;
		}
		InterfacePtr<IDocument> 
			iDocument(docUIDRef, UseDefaultIID());
		ASSERT(iDocument);
		if(!iDocument)
		{
			break;
		}
	
		InterfacePtr<ICommand> 
			importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
		ASSERT(importCmd);
		if(!importCmd)
		{
			break;
		}	
		IDataBase* db = docUIDRef.GetDataBase();
		ASSERT(db);
		if(!db)
		{
			break;
		}
		InterfacePtr<IImportFileCmdData> 
			importFileCmdData(importCmd, IID_IIMPORTFILECMDDATA); // no DefaultIID for this
		ASSERT(importFileCmdData);
		if(!importFileCmdData)
		{
			break;
		}
		importFileCmdData->Set(db, fileHelper.GetIDFile(), kMinimalUI);
		ErrorCode err = CmdUtils::ProcessCommand(importCmd);
		ASSERT(err == kSuccess);
		if(err != kSuccess)
		{
			break;
		}	
		// Get the contents of the place gun as our return value
		InterfacePtr<IPlaceGun> 
			placeGun(db, db->GetRootUID(), UseDefaultIID());
		ASSERT(placeGun);
		if(!placeGun)
		{
			break;
		}
		UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
		retval = placedItem; 
	} while(kFalse);

	return retval;
}
*/

/* GetWidgetOnPanel
*/
IControlView* FileTreeUtils::GetWidgetOnPanel(
	const WidgetID& panelWidgetID, const WidgetID& widgetID)
{
	IControlView* controlView=nil;
	do
	{
		InterfacePtr<IPanelControlData> 
			panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(panelWidgetID));	
		// Don't assert, fail silently, the tree view panel may be closed.
		if(panelData == nil)
		{
			break;
		}
		controlView = panelData->FindWidget(widgetID);
		ASSERT(controlView);
		if(controlView == nil)
		{
			break;
		}
	} while(0);
	
	return controlView;
}

bool16 FileTreeUtils::validPath(const PMString& p)
{
	const PMString thisDir(".");
	const PMString parentDir("..");
	return p != thisDir && p != parentDir;
}

//PMString DesignerPreferenciasFunctions::CrearFolderPreferencias()
PMString FileTreeUtils::CrearFolderPreferencias()
{  //CAlert::InformationAlert("Entro FileTreeUtils::CrearFolderPreferencias()");
	PMString Archivo = "";//ORIGINAL
	PMString SeparatorPath="";
    
	#if defined(MACINTOSH)
	SDKUtilities::GetApplicationFolder(Archivo); //ORIGINAL
	SeparatorPath=":";
	#elif defined(WINDOWS)
	//Archivo="C:\\Windows";
     InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
			(	kA2PPrefRegEditUtilitiesBoss,	// Object boss/class
				IRegEditUtilities::kDefaultIID
			)));
Archivo=ConsultaRegEdit->QueryKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), TEXT("SystemRoot")); 
//CAlert::InformationAlert(Archivo+ "  N1");
   	SeparatorPath="\\";
	#endif,
	 //Archivo.Append(":asalretni:");//ORIGINAL
	Archivo.Append(SeparatorPath + "asalretni" + SeparatorPath);
	//CAlert::InformationAlert(Archivo+ " N2" );//WINDOWS\asalretni
  	
	IDFile fileFolder;
	FileUtils::PMStringToIDFile(Archivo,fileFolder);
	//CAlert::InformationAlert("despues del IDFile fileFolder");

	if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
	 {//Fue creado  
	  //CAlert::InformationAlert("entro a condicion CreateFolderIfNeeded(fileFolder)<0");
	  Archivo.Append("Software");
      //CAlert::InformationAlert(Archivo + "N3");

	  FileUtils::PMStringToIDFile(Archivo,fileFolder);
	   if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
		{//fue creado
		 Archivo.Append("ADS2PAGE30");
		 SDKUtilities::AppendPathSeparator(Archivo);
		 FileUtils::PMStringToIDFile(Archivo,fileFolder);
		  if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
		 	{//fue creado
			 Archivo.Append("Preferencias");
             SDKUtilities::AppendPathSeparator(Archivo);
			 FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				 {//fue Creado
				  //Archivo.Append("C2PPref.pfa");
				 }
				else
				 {//Archivo.Append("C2PPref.pfa");
				 }
				}
				else
				{ Archivo.Append("Preferencias");
		          SDKUtilities::AppendPathSeparator(Archivo);
				  FileUtils::PMStringToIDFile(Archivo,fileFolder);
				  if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
					 //Archivo.Append("C2PPref.pfa");
					}
					else
					{//Archivo.Append("C2PPref.pfa");
					}
				}

			}
			else
			{
				Archivo.Append("ADS2PAGE30");
				SDKUtilities::AppendPathSeparator(Archivo);
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias");
					SDKUtilities::AppendPathSeparator(Archivo);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{	Archivo.Append("Preferencias");
				    SDKUtilities::AppendPathSeparator(Archivo);
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
					 //Archivo.Append("C2PPref.pfa");
					}
					else
					{//Archivo.Append("C2PPref.pfa");
					}
				}
			}
		}
		else//ya habia sido creado
		{  //CAlert::InformationAlert(" ya habia sido creado");
			Archivo.Append("Software");
            SDKUtilities::AppendPathSeparator(Archivo);
			//CAlert::InformationAlert(Archivo+ "NT1");
			FileUtils::PMStringToIDFile(Archivo,fileFolder);
			if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
			{//fue creado
				Archivo.Append("ADS2PAGE30");
				SDKUtilities::AppendPathSeparator(Archivo);
			    //CAlert::InformationAlert(Archivo+ "NT2");
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias");
					SDKUtilities::AppendPathSeparator(Archivo);
			        //CAlert::InformationAlert(Archivo+ "NT3");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias");
					SDKUtilities::AppendPathSeparator(Archivo);
			        //CAlert::InformationAlert(Archivo+ "NT4");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
			else
			{
				Archivo.Append("ADS2PAGE30");
				SDKUtilities::AppendPathSeparator(Archivo);
			    //CAlert::InformationAlert(Archivo+ "NT5");
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias");
					SDKUtilities::AppendPathSeparator(Archivo);
			        //CAlert::InformationAlert(Archivo+ "NT5");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias");
					SDKUtilities::AppendPathSeparator(Archivo);
			        //CAlert::InformationAlert(Archivo+ "NT6");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
		}
	return(Archivo);
}

//	end, File: FileTreeUtils.cpp
