/*
//	File:	A2P20ControlladorDlgPreferencias.cpp
//
//	Este archivo ferifica el nuevo passwor y lo guarda en el documento pwd.pw
//
*/

#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"
// Interface includes:
// none.

// General includes:
#include "CDialogController.h"

// Project includes:
#include "FileUtils.h"
#include "PMString.h"
#include <stdio.h>
#include <string.h>
//#include <io.h>
//#include "fstream.h"
//#include <iomanip.h>
#include "CAlert.h"
//#include "SelDlgActionComponent.cpp"
//////////////////////////
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialog.h"
#include "IDialogCreator.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "ISession.h"
#include "CAlert.h"
#include "SysFileList.h"
#include "IOpenFileDialog.h"
#include "ITextControlData.h"
// General includes:
#include "SDKUtilities.h"
#include "CActionComponent.h"

#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"
#endif
#include "A2PPrefID.h"
//#include  "A2PPrefEncrypt.h"//NO UTILIZADO
//#include  "InterlasaRegEditUtilities.h"

////////////////////////////////


/** A2PNewPassDialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Rodney Cook
*/
class A2PNewPassDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		A2PNewPassDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~A2PNewPassDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext *myContext);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext *myContext);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId);


		private:
			int32 result;
		
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(A2PNewPassDialogController, kA2PPrefSelDlgNewPassControllerImpl)
/* InitializeDialogFields
*/
void A2PNewPassDialogController::InitializeDialogFields(IActiveContext *myContext) 
{	/***************AL ABRIR LA FORMA DE LA LISTA*********/
	SetTextControlData(kA2PPrefTextPassAntWidgetID," ");
	SetTextControlData(kA2PPrefTextNewAntWidgetID," ");
	SetTextControlData(kA2PPrefTextRepNewAntWidgetID," ");	
	SelectDialogWidget(kA2PPrefTextPassAntWidgetID);
}

/* ValidateDialogFields
*/
/************ACOMODA O PONE LAS PREFERENCIAS SOBRE LAS CAJAS ETC....********/
WidgetID A2PNewPassDialogController::ValidateDialogFields(IActiveContext *myContext) 
{
	WidgetID retval=kNoInvalidWidgets;
	PMString PassPMS="";	//Password Anterior
	PMString Password1PMS="";	//Nuevo Password
	PMString Password2PMS="";	//Pasword nuevo repetido
	PMString PwdRegEdit="";	//PASWORD REGEDIT
		

	/*InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
			(
				kA2PPrefRegEditUtilitiesBoss,	// Object boss/class
				IRegEditUtilities::kDefaultIID
			)));

	if(PassPMS.NumUTF16TextChars()==0)
		PassPMS="#";

	if(ConsultaRegEdit->ExistKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\RAM2HPAC\\Drive32\\Driv\\A2P20CSPwd"), TEXT("A2Ppwd"))==kTrue)
	{
		//existe llave
		
		PwdRegEdit.Append(ConsultaRegEdit->QueryKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\RAM2HPAC\\Drive32\\Driv\\A2P20CSPwd"), TEXT("A2Ppwd")));
		if(PassPMS==PwdRegEdit)
		{
			if(Password1PMS==Password2PMS)
			{
				if(Password1PMS.NumUTF16TextChars()==0)
					Password1PMS="#";
				ConsultaRegEdit->CreateKey(TEXT("SOFTWARE\\Microsoft\\RAM2HPAC\\Drive32\\Driv\\A2P20CSPwd"), TEXT("A2Ppwd"), Password1PMS.GrabTString());//se crea la copia del password
			}
			else
			{
				CAlert::InformationAlert(kA2PPrefPasswordNoCoincidenStringKey);
				SetTextControlData(kA2PPrefTextNewAntWidgetID," ");
				SetTextControlData(kA2PPrefTextRepNewAntWidgetID," ");	
				retval=kA2PPrefTextNewAntWidgetID;
			}
			
		}
		else
		{
			CAlert::InformationAlert(kA2PPrefPasswordIncorrectoStringKey);
			//limpia widgets
			SetTextControlData(kA2PPrefTextPassAntWidgetID," ");
			SetTextControlData(kA2PPrefTextNewAntWidgetID," ");
			SetTextControlData(kA2PPrefTextRepNewAntWidgetID," ");	
			retval=kA2PPrefTextPassAntWidgetID;
		}
	}
	else
	{//no existe llave
		if(PassPMS.NumUTF16TextChars()==0)
			PassPMS="#";
		ConsultaRegEdit->CreateKey(TEXT("SOFTWARE\\Microsoft\\RAM2HPAC\\Drive32\\Driv\\A2P20CSPwd"), TEXT("A2Ppwd"), PassPMS.GrabTString());//se crea la copia del password
	}*/
	FILE *ArcPass;	
	
	CString	CadenaLeida="";
	PMString PassDeDocumento="";
		
	const int ActionAgreedValue=1;

	result=CAlert::ModalAlert(kA2PPrefSeguroDeReempPassStringKey,
	kYesString, 
	kNoString,
	kNullString,
	ActionAgreedValue,CAlert::eQuestionIcon);
	if(ActionAgreedValue==result)
	{
							
							
		//////////////////////////////////////////////////////////////////
		/////////////ACEPTAR NUEVO PASSWORD///////////////////////////////
		//////////////////////////////////////////////////////////////////
							
		//obtine el password anterior del dialogo
		PassPMS =GetTextControlData(kA2PPrefTextPassAntWidgetID);
		
		//obtine el password nuevo del dialogo
		Password1PMS =GetTextControlData(kA2PPrefTextNewAntWidgetID);
		
		//obtine el password nuevo2 del dialogo
		Password2PMS =GetTextControlData(kA2PPrefTextRepNewAntWidgetID);
		
		
		
		PMString PathPwd="";
		SDKUtilities::GetApplicationFolder(PathPwd);
		PathPwd.Append(":PAse2WgurPtyD.segurity");
		//PathPwd=InterlasaUtilities::MacToUnix(PathPwd);
		if((ArcPass=FileUtils::OpenFile(PathPwd.GrabCString(),"r"))==NULL)
		{
			CAlert::InformationAlert(kA2PPrefReeniciarPreferStringKey);
			//return kA2PPrefTextNewAntWidgetID;
		}
		else
		{///LECTURA DEL ARCHIVO
		
			//Lectura del archivo Password
			char c;
			int n=0;
			 do
			 {
    			c = fgetc (ArcPass);
     			if (c != EOF) 
     			{
     				n++;
     				PassDeDocumento.Append(c);
     			}
     			 
  			 } while (c != EOF);
  			 
		
			fclose(ArcPass);//cierra el archivo
			//PassDeDocumento=Descrypt(PassDeDocumento);//Desencripta el password
			
			if(PassPMS==PassDeDocumento)//comparacion del pasword del documento con el password tecleado
			{
				//si son iguales
				if(Password1PMS==Password2PMS)//compara los dos nuevos passwords 
				{	
					//si son iguales
					//PathPwd=InterlasaUtilities::MacToUnix(PathPwd);
					if((ArcPass=FileUtils::OpenFile(PathPwd.GrabCString(),"w"))==NULL)//abre el archivo password para rescribir el password
					{
		
						CAlert::InformationAlert(kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
						//return kA2PPrefTextNewAntWidgetID;
					}
					else
					{//si se pudo abrir el archivo de password	

						//Encripta el nuevo password
						//CString CadeAEncript=Password1PMS.GrabCString();
						//CadeAEncript=encrypt(CadeAEncript);
							
						//imprime el nuevo passwor en el documento password
						//fprintf(ArcPass,"Password: %s",CadeAEncript);
						fprintf(ArcPass,"%s",Password1PMS.GrabCString());
						//cierra el archivo
						fclose(ArcPass);
						
						//limpia los widgets
						SetTextControlData(kA2PPrefTextPassAntWidgetID," ");
						SetTextControlData(kA2PPrefTextNewAntWidgetID," ");
						SetTextControlData(kA2PPrefTextRepNewAntWidgetID," ");	
						retval=kDefaultWidgetId;
					}
				}
				else
				{//si no son iguales los paswords nuevos
					//limpia widgets
					CAlert::InformationAlert(kA2PPrefPasswordNoCoincidenStringKey);
					SetTextControlData(kA2PPrefTextNewAntWidgetID," ");
					SetTextControlData(kA2PPrefTextRepNewAntWidgetID," ");	
					retval=kA2PPrefTextNewAntWidgetID;
				}

			}
			else
			{//si el password no tecleado es diferente al paswword del documento
				CAlert::InformationAlert(kA2PPrefPasswordIncorrectoStringKey);
				//limpia widgets
				SetTextControlData(kA2PPrefTextPassAntWidgetID," ");
				SetTextControlData(kA2PPrefTextNewAntWidgetID," ");
				SetTextControlData(kA2PPrefTextRepNewAntWidgetID," ");	
				retval=kA2PPrefTextPassAntWidgetID;
			}
			
		}
	}
	else
	{
		//CAlert::InformationAlert("se salio");
	}
	return retval;
}

/* ApplyDialogFields
*/
void A2PNewPassDialogController::ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId) 
{	
	SystemBeep();  
}

// End, A2PNewPassDialogController.cpp.
