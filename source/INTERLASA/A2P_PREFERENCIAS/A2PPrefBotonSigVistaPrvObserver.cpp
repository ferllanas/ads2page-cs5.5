/*
//	File:	A2P20BotonAdelanteVPrevObserver.cpp
//
//	Author: Fernando Llanas
//
//	Date:	11-Jun-2003
//
//	Este archivo es utilizado para ejecutar el observer del boton Adelante en vista previa 
//	.este archivo contiene las instrucciones que se ejecutaran a la hora de dar click sobre el 
//	boton antes mensionado.
//
*/
#include "VCPlugInHeaders.h"

#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
// Interface includes:
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"

// Implementation includes:
#include "CDialogObserver.h"
//#include "EstructuraPreferencias.h"
// Project:
#include "SDKUtilities.h"
#include "CAlert.h"

#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "ITriStateControlData.h"
#include "FileUtils.h"

#include "A2PPrefID.h"
#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
#endif
#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"
#endif

class SelDlgBotonAdelanteVPrevObserver : public CObserver
{
	public:	
		/**
			Constructor.

			@param boss interface ptr from boss object on which interface is aggregated.
		*/
		SelDlgBotonAdelanteVPrevObserver(IPMUnknown *boss) : CObserver(boss) {}
		
		/**
			Called by the host when the observed object changes. In this case: 
			the tab dialog's info button is clicked.

			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
		
		/**
			Esta funcion separa la linea que se envia(CRecibida) entabuladores tambien aÒade 
			el numero de campo asi como el brinco de linea, y regresa la cadena formada y enumerada.
		*/

		PMString SeparaCad(PMString CRecibida,int32 CaracterSeparacionEnCampos);

		/**
		*/
		int32 CaracterDeSeparaciondeCampos();

		/**
		*/
		bool16 LeerEnCheckBox(IPanelControlData* panelControlData, const WidgetID& widgetID);
	

		PMString LeerEnWidgetBox(IPanelControlData* panelControlData, const WidgetID& widgetID);
		/** 
			Called by the application to allow the observer to attach to the 
			subjects to be observed. In this case the tab dialog's info button widget.
		*/
		virtual void AutoAttach();

        
		/** 
			Called by the application to allow the observer to detach from the 
			subjects being observed. 
		*/
		 virtual void AutoDetach();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(SelDlgBotonAdelanteVPrevObserver, kA2PPrefSelBotonAdelVPrevObserverImpl)



/*	
	Update, la llamada a esta funcion ocurre cuando se oprime el boton adelante (>>) que se encuentra
	sobre la pestaÒa importar.
*/
void SelDlgBotonAdelanteVPrevObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID& protocol, 
	void* changedBy
)
{

	do
	{

		if(CaracterDeSeparaciondeCampos()==-1)
			{//sale de funcion pues no se encuentra ningun caracter de separacion para los campos
				break;
			}
		FILE *stream;				//variable archivo
		PMString NumLineNew;			//numero de linea siguente a leer
		PMString ArchivoCString;		//cadena que contiene el path del archivo que se esta leyendo
		int PosIni;					//posicion inicial de la cadena del campo dentro de la cadena
		int PosFin;					//posicion final de la cadena del campo sobre la cadena del aviso
		PlatformChar Caracter;				//caracter que se esta leyendo
		int NumCaracter=0;			//numero del caracter en que se encuentra leyendo de la cadena del aviso
		int NumLineaALeer;			//numero de la linea que se debe leer, siguente linea o aviso a leer
		int Linea;					//numero de linea actual
		int ANSCCICaranter;			//codigo ascci del caracter que se lee de la cadena del aviso				//

		//int a,b;					//punteros que se utilizan en la conversion de una cadena a un entero.
		
		PMString Archivo;			//Direccion del archivo a leer
		PMString NumLineaLeida;		//Numero de linea a leer
		PMString CadenaAEmprimir;	//Cadena que se mostrara en vista previa
		PMString PMSCadena;
		PMString NumUltimaLineaPMString;
		
		//Inicializa variables
			NumLineNew="";
			ArchivoCString="";
			Caracter=' ';

			PMSCadena="";
			NumUltimaLineaPMString="";
			Archivo="";
			NumLineaLeida="";
			CadenaAEmprimir="";

			Archivo.Clear();		
			NumLineaLeida.Clear();
			CadenaAEmprimir.Clear();
			PMSCadena.Clear();
			

		InterfacePtr<IControlView> view(theSubject, IID_ICONTROLVIEW);

		if (view != nil)
		{
			// Get the button ID from the view
			WidgetID theSelectedWidget = view->GetWidgetID();

			if (theSelectedWidget == kA2PPrefBotonAdelanteVisPreviaID && theChange == kTrueStateMessage)
			{

				// Get the button ID from the view
				theSelectedWidget = view->GetWidgetID();

				if (theSelectedWidget == kA2PPrefBotonAdelanteVisPreviaID && theChange == kTrueStateMessage)
				{	

					//Creo una interfaz llamada Myparen la cual sera la ventana 
							//que se encuentra abierta
					InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				

					//Obtengo la inerfaz del panel que se encuentra abierto
					InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;

					//obtengo una interfaz del la caja de vista previa
					InterfacePtr<IControlView>		editBoxView( panel->FindWidget(kA2PPrefBoxDireccionArchivoLeidoWidgetID), UseDefaultIID() );

					//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
					InterfacePtr<ITextControlData>	TextoDeDireccion( editBoxView, UseDefaultIID());

					//Extraigo el texto del Widget
					Archivo=TextoDeDireccion->GetString();
							
					//Lecctura del numero de linea que se leyo
					InterfacePtr<IControlView>		editBoxView1( panel->FindWidget(kA2PPrefBoxNumeroLineaLeidaWidgetID), UseDefaultIID() );
					
					//obtengo una interfaz ITextControlData a partir del control de vista de la caja de texto
					//para obtener su contenido
					InterfacePtr<ITextControlData>	numerolinea( editBoxView1, UseDefaultIID());

					//obtencion del contenido del
					NumLineaLeida=numerolinea->GetString();

					//obtengo una interfaz ITextControlData a partir del control de vista de la caja de texto
					//para obtener su contenido
					InterfacePtr<IControlView>		editBoxView2( panel->FindWidget(kA2PPrefBoxNumUltimaLineEnArcWidgetID), UseDefaultIID() );

					//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
					InterfacePtr<ITextControlData>	UltimaLinea( editBoxView2, UseDefaultIID());

					//Extraigo el texto del Widget
					NumUltimaLineaPMString=UltimaLinea->GetString();
					
					/***************************************************************/
					//Archivo=InterlasaUtilities::MacToUnix(Archivo);

					ArchivoCString=Archivo.GrabCString();
					/****************************************************************/
					/*se revisara siempre y cuando el numero de la linea a leer sea menor a
						el numero de la ultima linea del archivo*/
					if(NumLineaLeida.GetAsNumber()<NumUltimaLineaPMString.GetAsNumber()-1)
					{
						//se revisa si se puede leer el archivo si no s epuede leer se
						//no se entra a revisar las lineas
												if( (stream  = FileUtils::OpenFile(ArchivoCString,"r"))==NULL)
							{	
								CAlert::ErrorAlert(kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
							}
							else//cuando si se pudo leer el archivo
							{		
								NumLineaALeer=NumLineaLeida.GetAsNumber()+1;//se le asigna a NumLineNew el contenido de NumLineLeida
																	//se tiene que hacer la convercion de PMString a CString para el manejo de las
																	//cadenas con las funciones basicas de c++

								
								PosIni=0;	//Posicion inicial de una linea
								PosFin=0;	//Posicion final de una linea
								Linea=1;	//Numero de linea se inicializa en uno
								while(Linea<(NumLineaALeer+2) && Linea<=NumUltimaLineaPMString.GetAsNumber())//Hacer mientras el numero de linea sea menor a la ultima linea del archivo y menor a la linea que sigue de leer
								{	PosIni=PosFin;
									do
									{		
										Caracter.SetFromUnicode(getc(stream));
										NumCaracter++;
										ANSCCICaranter=Caracter.GetValue();
									}while(ANSCCICaranter!=10 && ANSCCICaranter!=13 && !feof(stream));//Hacer mientras que no sea fin de linea(10) retorno de carro(13) o fin de archivo(EOF)

									if(!feof(stream))
										{	PosFin=NumCaracter;	//Posicion final es igual al numero del ultimo caracter leido
											Linea++;	//siguiente linea
										}
								}
								
								/*si se encontro un fin de archivo se indica que no existe mas lineas por leer
									entonces el numero de linea se decrementa*/
								if(feof(stream))
									Linea--;
								/*se cierra el archivo*/
								fclose(stream);
								
								/*Se lee denuevo el archivo*/
								
								if( (stream  = FileUtils::OpenFile(ArchivoCString,"r"))==NULL)
									{	
										CAlert::ErrorAlert(kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
									//	break;
									}
								else
									{
									
										char *Cadena=new char[PosFin];
										fread(Cadena,1,PosFin,stream);
										//cuando cierro el archivo me manda un error en la ultima linea que se debe leer
										fclose(stream);
										
										//Copia cadena a archivo
										PMSCadena=Cadena;
										
										//Remueve del archivo las lineas que no se desean dejando 
										PMSCadena.Remove(0,PosIni);
										
										//convierte el numero de linea a caracteres
										NumLineNew.AppendNumber(Linea-1);
										
										//Se pone el nuevo numero de linea que se ha leido en el widget correspondiente
										numerolinea->SetString(NumLineNew);
										
										PMString Arc2;
										
										//PosFin Obtiene el tamaÒo de la linea
										PosFin=PosFin-PosIni;
										
										//inserto en Arc2 la linea deseada
										Arc2.Insert(PMSCadena,PosIni,PosFin);
										
										//Obtengo la cadena a imprimir llamando a la funcion SeparaCad
										CadenaAEmprimir=SeparaCad(Arc2,CaracterDeSeparaciondeCampos());
										
										//Busqueda del Widget pde la caja  de vista previa
										InterfacePtr<IControlView>		editBoxView3( panel->FindWidget(kA2PPrefBoxVistaPreviaID), UseDefaultIID() );
										//crea una interfax para controlar los dsatos del widget correspondiente
										InterfacePtr<ITextControlData>	selectedChar3( editBoxView3, UseDefaultIID());
										//inserto dentro del widget la nueva cadena
										selectedChar3->SetString(CadenaAEmprimir);
										delete Cadena;
										
										fclose(stream);
									}
								
							}
					}
				}
				
			}
		}
	}while(false);
}




/* AutoAttach */
/************AL INICIAR LA FORMA SE CARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 			SOBRE EL BOTON APLICAR************************************************************************/

void SelDlgBotonAdelanteVPrevObserver::AutoAttach()
{//	CAlert::WarningAlert("AutoAttach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_IBOOLEANCONTROLDATA);
	}
}

/* AutoDetach */
/************AL TERMINAR LA FORMA SE DESCARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR************************************************************************/

void SelDlgBotonAdelanteVPrevObserver::AutoDetach()
{//	CAlert::WarningAlert("AutoDetach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_IBOOLEANCONTROLDATA);
	}
}

/**
	Esta funcion separa la linea que se envia(CRecibida) entabuladores tambien aÒade 
	el numero de campo asi como el brinco de linea
*/
PMString SelDlgBotonAdelanteVPrevObserver::SeparaCad(PMString CRecibida,int32 CaracterSeparacionEnCampos)
{
		PMString CadenaAEnviar;	//Cadena que retorna
	PMString CEncontrada;	//Cadena del campo encontrada
	CadenaAEnviar.Clear();	//limpia cadenas
	CadenaAEnviar.Clear();	
	int PosTab=0;			//posicion del primer caracter tabulador
	int numcampo=1;			//numero de campo que se a leido
	int Tamano=0;			//tamaÒo de la cadena que se recibio
	int salir=0;			//bandera que indica si se ha leido el ultimo caracter de la cadena
	
	do
	{
		//se obtien la posicion del tabulador
		PosTab=CRecibida.IndexOfWChar(CaracterSeparacionEnCampos);
		//Si la posicion del tabulador fue -1 inidica que no se ha encontrado un tabulador entonces se encontro un fin 
		//de cadena se enciende la bandera salir del ciclo
		if(PosTab==-1)
		{
			PosTab=CRecibida.NumUTF16TextChars();//la posicion del tabulador sera el tamaÒo de la cadena
			salir=1;
		}

		//Cadena encontrada(campo) se toma desde el inicio de la cadena recibida hasta la posicion del 1∞ tabulador
		CEncontrada.Insert(CRecibida,0,PosTab);
		//se bora este campo de la cadena recibida
		CRecibida.Remove(0,PosTab+1);
		//se agrega el numero de campo que le corresponde  ala cadena a enviar
		CadenaAEnviar.AppendNumber(numcampo);
		CadenaAEnviar.Append(".- ");
		//se agrega la cadena encontrada a cadena a enviar
		CadenaAEnviar.Append(CEncontrada);
		//se obtiene  el tamaÒo de la cadena a enviar para insertar en la ultima posicion 
		//el salto de linea
		Tamano=CadenaAEnviar.NumUTF16TextChars();
		CadenaAEnviar.InsertW(13,Tamano);
		CadenaAEnviar.InsertW(10,Tamano+1);
		//se incrementa el numero de campo
		numcampo++;
		//se limpia la cadena encontrada
		CEncontrada.Clear();
	}while(salir!=1);
	return(CadenaAEnviar);//regresa cadena a enviar, cadena con numero de campo y su contenido
}

int32 SelDlgBotonAdelanteVPrevObserver::CaracterDeSeparaciondeCampos()
{

	int32 retval;
	UTF32TextChar temp;
	InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
	//Obtengo la inerfaz del panel que se encuentra abierto
	InterfacePtr<IPanelControlData>	panelControlImportar((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
	//Busqueda del Widget pde la caja  de vista previa

	

	if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonTabWidgetID))	
		retval=9;
	else
		if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonComaWidgetID))
			retval=44;
		else
			if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonPyComaWidgetID))
				retval=59;
			else
				if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonDosPuntosWidgetID))
					retval=58;
				else
				{
					if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonEspecialWidgetID))
					{
						PMString a;
						a=LeerEnWidgetBox(panelControlImportar,kA2PPrefBoxCarEspecialLecturaEID);
						if(a.NumUTF16TextChars()>0)
						{
							temp=a.GetWChar(0);
							retval=temp.GetValue();
						}
						else
						{
							CAlert::WarningAlert(kA2PPrefMensajeTeclearCarEspecialStringKey);
							retval=-1;
						}
					}
					else
					{
						CAlert::WarningAlert(kA2PPrefMensajeSelecTipoSepCamposStringKey);
						retval=-1;
					}
				}
	return(retval);
}

bool16 SelDlgBotonAdelanteVPrevObserver::LeerEnCheckBox(IPanelControlData* panelControlData, const WidgetID& widgetID)
{	
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IControlView>		editCheckBoxt(panelControlData->FindWidget(widgetID), UseDefaultIID() );
		if(editCheckBoxt==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		InterfacePtr<ITriStateControlData>	CheckBox( editCheckBoxt, IID_ITRISTATECONTROLDATA);
		if(CheckBox==nil)
		{
			CAlert::InformationAlert("No se encontro el estado del check box");
			break;
		}
		if(CheckBox->IsSelected())
			retval=kTrue;
		else
			retval=kFalse;
	}while(false);
	return retval;
}

/*
	FUNCION PARA LEER TEXTO EN UNA CAJA DE TEXTO 
*/
PMString SelDlgBotonAdelanteVPrevObserver::LeerEnWidgetBox(IPanelControlData* panelControlData, const WidgetID& widgetID)
{
	PMString retval="";
	do 
	{	
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		editBoxView( panelControlData->FindWidget(widgetID), UseDefaultIID() );
		if(editBoxView==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
		InterfacePtr<ITextControlData>	ControlDETexto( editBoxView, UseDefaultIID());
		if(ControlDETexto==nil)
		{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}
		//Extraigo el texto del Widget
		retval=ControlDETexto->GetString();
	}while(false);
	return retval;
}