//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/FileTreeUtils.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __A2PPrefActionsOnDLGPreferenciasUtils_H_DEFINED__
#define __A2PPrefActionsOnDLGPreferenciasUtils_H_DEFINED__

#include "A2PPreferencias.h"

class IDocument;
class IControlView;
class IDialogController;

/** A collection of miscellaneous utility methods used by this plug-in.
@ingroup paneltreeview
*/
class A2PPrefActionsOnDLGPreferenciasUtils
{

public:
	
	
	static bool16 LlenaDlgPreferenciasXNombrePref(IControlView *DlgView,PMString NombrePreferencia);
	/**
			Obtiene los campos tecleados de cada widget y llena con estos la estructura preferencias.
			
			  @param Pref, Estructura preferencias.
	*/
	static bool16 LlenarPreferenciasDeDialogoPref(IControlView *DlgView, Preferencias &Pref);

	/**
			Funcion que compara los numeros de las preferencias de lectura de campos del archivo 
			de los avisos(Pesta�a Importar), y el orden de los campos en las etiquetas, verificando
			que no se encuentren repetidos algunos de estos numeros.Esta funcion manda llamar a las funciones
			ComparaNumero, ComparaNumeroEnEtiCon, ComparaNumeroEnEtiSin y la funcion openDialogForUpdate

			  @param Pref, Estructura de tipo Preferencias.
	*/
	static WidgetID ValidaNumeracionDeCampos(IControlView *DlgPrefView,Preferencias &Pref);
	
	
	static bool16 CopiaCamposAImportarParaOrdenCamposEtiq(IControlView *DlgView, Preferencias &Pref);
	
	//static int32 llenarComboPrefDeDlgPref(IControlView *DlgPrefView, Preferencias &Preferencia);
	static int llenarComboPrefDeDlgPref(IControlView *DlgPrefView, Preferencias &Preferencia);
	
	static bool16 HabilDeshaWitgetsDependientes(IControlView *DlgPrefView,const WidgetID& widgetID,bool16 ChecBoxSeleccionado);

	
	static bool16 ColocaEnComboColor(IControlView *DlgPrefView,const WidgetID& widgetid,PMString Color);
	
	static void ColocaEnComboxy(IControlView *DlgPrefView,const WidgetID& widgetid,PMString Secuencia);
	
    static void ColocaEnComboTLBR(IControlView *DlgPrefView, const WidgetID& widgetid,PMString Secuencia);
    
    static void ColocaEnComboUnidades(IControlView *DlgPrefView,const WidgetID& widgetid,PMString Unidad);
	
private:

	/**
			Funcion que compara una determinada cadena con todos las demas posiciones de campos de etiquetas Sin Publicidad.
			  @param Pref, Estructura de tipo Preferencias.
			  @param CadenaAChecar, Cadena que contien el numero a comparar, con los demas numeros que se tienen en la estructura.
			  @param numero, indica con que numero no se debe comparar.
		*/
	static bool16 ComparaNumeroEnEtiSin(PMString CadenaAChecar,Preferencias &Pref,int32 numero);
	
	/**
			Funcion que compara una determinada cadena con todos las demas posiciones de campos de etiquetas conn Publicidad.
			  @param Pref, Estructura de tipo Preferencias.
			  @param CadenaAChecar, Cadena que contien el numero a comparar, con los demas numeros que se tienen en la estructura.
			  @param numero, indica con que numero no se debe comparar.
		*/
	static bool16 ComparaNumeroEnEtiCon(PMString CadenaAChecar,Preferencias &Pref,int32 numero);
	
	/**
			Funcion que compara una determinada cadena con todos las demas posiciones de campos.
			  @param Pref, Estructura de tipo Preferencias.
			  @param CadenaAChecar, Cadena que contien el numero a comparar, con los demas numeros que se tienen en la estructura.
			  @param numero, indica con que numero no se debe comparar.
		*/
	static bool16 ComparaNumero(int32 CadenaAChecar,Preferencias &Pref,int32 numero);

	

};

#endif // __A2PPrefActionsOnDLGPreferenciasUtils_H_DEFINED__

