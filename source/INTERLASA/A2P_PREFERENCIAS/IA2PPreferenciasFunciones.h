//nuevo
#ifndef _IA2PPreferenciasFunciones_
#define _IA2PPreferenciasFunciones_

// Interface includes:
#include "IPMUnknown.h"

#include "A2PPrefID.h"
#include "A2PPreferencias.h"

    //IA2PPreferenciasFunctions es la misma que en la interface de importacion
class IA2PPreferenciasFunctions : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IA2PPreferenciasFunciones };

	/**
		funcion que abre el archivo de preferencias default y llena la estructura de preferencias
		@Preferencia estructura que contiene las preferencias del archivo Defaul.pfa.
	*/
	//virtual bool16 GuardarPreferencias(PMString NomPref,Preferencias &Preferencia)=0;

	/**
		funcion que abre el archivo de preferencias default y llena la estructura de preferencias
		@Preferencia estructura que contiene las preferencias del archivo Defaul.pfa.
	*/
	
	virtual bool16 GetDefaultPreferencesConnection(Preferencias &Pref,bool16 RefreshPreferencias)=0;

	//virtual bool16 leerDocDePreferencia(CString Path,Preferencias &Pref)=0;//ORIGINAL NUEVO
    virtual bool16 leerDocDePreferencia(PMString Path,Preferencias &Pref)=0;

	virtual bool16 EscribeDatosOnDocument(PMString NomPref,Preferencias &Pref)=0;

    virtual bool16 OpenDialogPassword()=0;
    
    //virtual PMString ProporcionaFechaArchivo(CString Archivo)=0;//ORIGINAL NUEVO
      virtual PMString ProporcionaFechaArchivo(PMString Archivo)=0;
    
     virtual bool16 VerificaSeguridadPirateria()=0;
     
     virtual bool16 openDialogForSecurity()=0;
     
     //virtual bool16 ConvertirUnidadesdePreferenciasAPuntos(Preferencias &Pref)=0;

	//virtual bool16 ConvertirPuntosAUnidadesdePreferencias(Preferencias &Pref)=0;
    
private:

	int TipoPropiedad(CString LineaLeida,Preferencias &Pref);
      // int TipoPropiedad(PMString LineaLeida,Preferencias &Pref);

	void SeparaCampos(int tipo,CString Cadena,Preferencias &Pref);
	
	

};

#endif // _IN2P10InputOutputsFILES_
