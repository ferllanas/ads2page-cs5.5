/*
//	File:	A2P20CreaPestañaEtiquetas.cpp
//
//	Creador de la interfaz de la pestaña etiquetas
//
*/

#include "VCPlugInHeaders.h"

// Implementation includes:
#include "CPanelCreator.h"

// Project includes:
#include "A2PPrefID.h"



/** A2P20CreaPestañaEtiquetas
	implementa IPanelCreator basedo sobre la implementacion parcial CPanelCreator.
	Eliminamos CPanelCreator's GetPanelRsrcID().
	@author Fernando llanas
*/
class CreatorPestanaEtiquetas : public CPanelCreator
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CreatorPestanaEtiquetas(IPMUnknown *boss) : CPanelCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CreatorPestanaEtiquetas() {}

		/** 
			Returns the local resource ID of the ordered panels.
		*/
		virtual RsrcID GetPanelRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(CreatorPestanaEtiquetas, kA2PPrefEtiquetPanelCreatorImpl)

/* GetPanelRsrcID
*/
RsrcID CreatorPestanaEtiquetas::GetPanelRsrcID() const
{
	return kA2PPrefEtiquetPanelCreatorResourceID;
}

// End, YinPanelCreator.cpp
