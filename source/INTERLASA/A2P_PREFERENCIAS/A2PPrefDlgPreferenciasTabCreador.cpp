/*
//	File:	SelDlgTabDialogCreator.cpp
//
//	Date:	11-Jun-2003
//
//	Interlas
//	
	
*/
#include "VCPlugInHeaders.h"

// Interface includes:
#include "LocaleSetting.h"
#include "IDialogMgr.h"
#include "IApplication.h"
#include "ISession.h"

// Implementation includes:
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"

// Project includes:
#include "A2PPrefID.h"

/** 
	SelDlgTabDialogCreator
	provee manegadores y controles sobre el dialogo
	provides management and control over the dialog. 

	
	SelDlgTabDialogCreator 
	Implementa El IDialogCreator basado sobre
	el implementacion parcial de CDialogCreator

	implements IDialogCreator based on
	the partial implementation CDialogCreator. 

	@author Rodney Cook
*/

class SelDlgTabDialogCreator : public CDialogCreator
{
	public:
		/**
			Constructor.

			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		SelDlgTabDialogCreator(IPMUnknown* boss) : CDialogCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~SelDlgTabDialogCreator() {}

		/** 
			Creates a dialog from the resource that is cached by 
			the dialog manager until invoked.
		*/
		virtual IDialog* CreateDialog();

		/** 
			Returns the resource ID of the resource containing an
			ordered list of panel IDs.

			@param classIDs an ordered list of class IDs of selectable dialogs that are to be installed in this dialog
		*/
		virtual void GetOrderedPanelIDs(K2Vector<ClassID>& classIDs);

		/** 
			Returns an ordered list of class IDs of selectable dialogs
			that are to be installed in this dialog.
		*/
		virtual RsrcID GetOrderedPanelsRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(SelDlgTabDialogCreator, kA2PPrefTabDialogCreatorImpl)

/* CreateDialog
*/
IDialog* SelDlgTabDialogCreator::CreateDialog()
{
	IDialog* dialog = nil;

	// Use a do-while(false) so we can break on bad pointers:
	do
	{
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		InterfacePtr<IDialogMgr> dialogMgr(app, UseDefaultIID());
		if (dialogMgr == nil)
		{
			ASSERT_FAIL("SelDlgTabDialogCreator::CreateDialog: dialogMgr invalid");
			break;
		}

		// We need to load the plug-ins resource:
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,								// Locale index from PMLocaleIDs.h. 
			kA2PPrefPluginID,		// Our Plug-in ID from SelectableDialog.h. 
			kViewRsrcType,						// This is the kViewRsrcType.
			kA2PPrefTabDialogResourceID,	// Resource ID for our dialog.
			kTrue									// Initially visible.
		);

		// CreateDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).	
		// The dialog manager caches the dialog for us:
		dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);

		// We want initial focus to be in the selection list.
		dialog->SetDialogFocusingAlgorithm(IDialog::kNoAutoFocus);
	} while (false); // Only do once.

	return dialog;
}

/*	GetOrderedPanelIDs
*/
void SelDlgTabDialogCreator::GetOrderedPanelIDs(K2Vector<ClassID>& classIDs)
{
	ResourceEnabler rsrcEnable;
	CDialogCreator::GetOrderedPanelIDs(classIDs);
}

/* GetOrderedPanelsRsrcID
*/
RsrcID SelDlgTabDialogCreator::GetOrderedPanelsRsrcID() const
{
	return kA2PPrefPanelOrderingResourceID;
}

// End, SelDlgTabDialogCreator.cpp
