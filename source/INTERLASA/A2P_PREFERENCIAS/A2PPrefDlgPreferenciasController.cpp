/*
//	File:	A2P20ControlladorDlgPreferencias.cpp
//
//	Este arcivo como su nombre lo indica es quien esta pendiente del control del Dialogo
//	Preferencias y observa cuando se le da click al boton aceptar de este dialogo, al
//	dar click al boton aceptar este archivo hace una validacion de los campos en de cada pesta�a
//	pero se refleja mas en las pesta�as Importar(validacion de campos) y en la pesta�a Orden en
//	etiquetas, si todos los campos de los widgets se encuentran en su rango y sin repeticion, 
//	se debe guardar las preferencias tecleadas en los archivos de preferencia(Deafault.pfa y PrefActual.pfa)
//	. En caso de que no sean aceptados algun campo el dialogo no se cerrara si no que pondra el setfocus sobre 
//	el widget que tien su campo erroneo.
//	
*/


#define _WIN32_WINNT 0x0400

//#include <windows.h>
#include <stdio.h>

#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"
#include "CDialogController.h"
#include "A2PPrefID.h"
#include "IDataBase.h"
#include "PMString.h"
#include "A2PPreferencias.h"
#include <stdio.h>
#include <string.h>

// Interface includes:
#include "IDocument.h"
#include "IStringListControlData.h"
#include "IMasterSpreadList.h"
#include "IMasterSpread.h"
#include "IDropDownListController.h"
#include "IPageSetupPrefs.h"
#include "IMasterSpreadCmdData.h"
#include "IPanelControlData.h"
#include "IUsedFontList.h"
#include "IFontMgr.h"
#include "IFontGroup.h"
///////////////
#include "SystemUtils.h"
#include "UIDList.h"
#include "CAlert.h"										// For ErrorAlert()
#include "CmdUtils.h"									// For ProcessCommand()
#include "ErrorUtils.h"									// For PMGetGlobalErrorCode()
#include "ILayoutUtils.h"								// For QueryFrontLayoutData()...
#include "PreferenceUtils.h"							// For QueryPreferences()
#include "SpreadID.h"
#include "SysFileList.h"
#include "IWidgetParent.h"
#include "IDialogCreator.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "ISelectableDialogSwitcher.h"
#include "IDialog.h"
#include "PMString.h"
#include "SDKFileHelper.h"
#include "FileTreeUtils.h"
#include "IA2PPreferenciasFunciones.h"
#include "A2PPrefActionsOnDLGPreferenciasUtils.h"
#include <string.h>
//#include "PlatformFileSystemIterator.h"

#ifdef MACINTOSH
	#include "PlatformFileSystemIterator.h"
#endif
#ifdef WINDOWS
	#include "..\Interlasa_common\PlatformFileSystemIterator.h"
#endif



class PreferenciasControlador: public CDialogController
{
	Preferencias Pref;
	public:

		/**
			Constructor
		*/
		PreferenciasControlador(IPMUnknown* boss); //: CDialogController(boss) {}
		/**
			Destructor
		*/
		virtual ~PreferenciasControlador();// {}

		/**
			Inicializa Fields, pone las preferencias deacuerdo a la ultima preferencia modificada.
		*/
		virtual void InitializeDialogFields(IActiveContext *myContext);

		/**
			Validacion de los Widgets
		*/
		virtual WidgetID ValidateDialogFields(IActiveContext *myContext);

		/**
			Aplica Widgets, en dao caso que se acepten los campos tecleados en los
			widgets.
		*/
		virtual void ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId);
		
	private:

		

		UIDList*		fmpDropList;		// List of Master Page UIDs
		UID				fmpBasedOnUID;		// Default UID of MP
		PMString		fmpName;			// Name of the default MP
		PMString		fmpPrefix;			// Prefix of the default MP
		int32			fmpNumPages;		// Number of pages in the default MP
		bool16			fmpValidated;		// Whether the member vars are validated	
		PMString		fFilter;

};



CREATE_PMINTERFACE(PreferenciasControlador, kA2PPrefDialogControllerImpl)

//CONSTRUCTOR 
PreferenciasControlador::PreferenciasControlador(IPMUnknown* boss)
	: CDialogController(boss),
	fmpDropList(nil), fmpBasedOnUID(kInvalidUID),
	fmpName(""), fmpPrefix(""), fmpNumPages(1), fmpValidated(kFalse)
{
}

//DESTRUCTOR
PreferenciasControlador::~PreferenciasControlador() 
{
	if (fmpDropList)
	{
		delete fmpDropList;
		fmpDropList = nil;
	}
}
//CHECAR LOS METODOS PARA ABRIR Y AL GUARDAR LOS DOCUMENTOS 
void PreferenciasControlador::InitializeDialogFields(IActiveContext *myContext) 
{	
	//CAlert::InformationAlert("entro InitializeDialogFields en PreferenciasControlador inicia dialogo");
	do
	{	InterfacePtr<IControlView> DlgPrefView(this, UseDefaultIID());
		if(DlgPrefView==nil)
		{
			CAlert::ErrorAlert("No existe");
			break;
		}
		
		PMString PathPreferencia = FileTreeUtils::CrearFolderPreferencias();
        //CAlert::InformationAlert("despues de CrearFolderPreferencias en PreferenciasControlador inicia dialogo ");
		PathPreferencia.Append("Default.pfa");
		//CAlert::InformationAlert(PathPreferencia);
		A2PPrefActionsOnDLGPreferenciasUtils::LlenaDlgPreferenciasXNombrePref(DlgPrefView,PathPreferencia);
		//CAlert::InformationAlert("despues de LlenaDlgPreferenciasXNombrePref InitializeDialogFields");
	}while(false);
		
}

/**
	Funcion que llama ValidaNumeracion de campos, ValidaNumeraciondecampos
	regresa un widgetID en el cual sel setfocus es puesto automatuicamente
*/
WidgetID PreferenciasControlador::ValidateDialogFields(IActiveContext *myContext) 
{
	WidgetID result;
	do
	{
		//CAlert::InformationAlert("ValidateDialogFields1");
		InterfacePtr<IControlView> DlgPrefView(this, UseDefaultIID());
		if(DlgPrefView==nil)
		{
			CAlert::ErrorAlert("No existe");
			break;
		}
		//CAlert::InformationAlert("ValidateDialogFields2");
		if(A2PPrefActionsOnDLGPreferenciasUtils::LlenarPreferenciasDeDialogoPref(DlgPrefView, Pref))
		{
			result=A2PPrefActionsOnDLGPreferenciasUtils::ValidaNumeracionDeCampos(DlgPrefView, Pref);
		}
		
		//CAlert::InformationAlert("ValidateDialogFields3");
		Pref.Nombrepreferencias=GetTextControlData(kA2PPrefComboBoxSelecPreferID);
		
	}while(false);
	
	return result;	
}

/**
	Funcion que se ejecuta al no regresar ningunn widget la funcion validafields
*/
void PreferenciasControlador::ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId) 
{ 	
	//CAlert::InformationAlert("ApplyDialogFields 1");
	InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
			(
				kA2PPreferenciasFuncionesBoss,	// Object boss/class
				IA2PPreferenciasFunctions::kDefaultIID
			)));
	//obtiene campos y guarda en estructura pref
	//getTextFromWidgets(Pref);
	//si el nombre de preferencias es mayor a 0 
	
	if(Pref.Nombrepreferencias.NumUTF16TextChars()>0)
	{	
		//Se crea o se reescribe el archivo con este nombre
		//CAlert::InformationAlert("ApplyDialogFields 2");
		A2PPrefeFuntions->EscribeDatosOnDocument(Pref.Nombrepreferencias, Pref);/////graba en preferencias default
	}
	A2PPrefeFuntions->EscribeDatosOnDocument("Default", Pref);///graba preferencias default
	//CAlert::InformationAlert("ApplyDialogFields 3");
	A2PPrefeFuntions->GetDefaultPreferencesConnection(Pref,kTrue);
    //CAlert::InformationAlert("ApplyDialogFields 4");
	SystemBeep();  
}




