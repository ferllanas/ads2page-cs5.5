#include "VCPlugInHeaders.h"

#include "CAlert.h"
#include "IDialogMgr.h"
#include "CDialogCreator.h"
#include "CDialogController.h"

#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"

#include "IApplication.h"

// General includes:
#include "SDKUtilities.h"
#include "FileUtils.h"

#include "A2PPrefID.h"
//#include "A2PPrefEncrypt.h"
#if defined(MACINTOSH)
	#include "InterlasaUtilities.h"
#endif

#if defined(WINDOWS)
	#include "..\Interlasa_common\InterlasaRegEditUtilities.h"
	#include "..\Interlasa_common\InterlasaUtilities.h"
#endif
//#include <stdlib.h>

//#include <stdio.h>
//#include <string.h>
//#include <io.h>
//#include "fstream.h"
//#include <iomanip.h>

/** 
*/
class A2PPrefKeyLicenseDlgController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		A2PPrefKeyLicenseDlgController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~A2PPrefKeyLicenseDlgController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext *myContext);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext *myContext);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId);

		private:
		/**
			Funcion que abre el dialogo de preferencias
		*/
		void AbrirDlgPassword();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(A2PPrefKeyLicenseDlgController, kA2PPrefKeyLicenseDlgControllerImpl)
/* InitializeDialogFields
*/
void A2PPrefKeyLicenseDlgController::InitializeDialogFields(IActiveContext *myContext) 
{	/***************AL ABRIR LA FORMA DE LA LISTA*********/
	//struct tm *fh;
	time_t segundos;
	time(&segundos);
	
	//srand(segundos);
	PMString NumeroRandom;
	NumeroRandom.AppendNumber(segundos);
	
	SetTextControlData(kA2PPrefTextKeyA2PWidgetID,NumeroRandom);
	SetTextControlData(kA2PPrefTextKeyIDWidgetID," ");
	SelectDialogWidget(kA2PPrefTextKeyIDWidgetID);
}

/* ValidateDialogFields
*/
/************ACOMODA O PONE LAS PREFERENCIAS SOBRE LAS CAJAS ETC....********/
WidgetID A2PPrefKeyLicenseDlgController::ValidateDialogFields(IActiveContext *myContext) 
{


	WidgetID result = kNoInvalidWidgets;
	FILE *ArcPass;

	
	PMString StringRandom=GetTextControlData(kA2PPrefTextKeyA2PWidgetID);
	PMReal NumRandom=StringRandom.GetAsDouble();
	
	NumRandom=NumRandom/5555555;
	StringRandom="";
	
	StringRandom.AppendNumber(NumRandom,7,kFalse,kFalse);
	
	int32 index=StringRandom.IndexOfString(".");
	PMString StrEntero=((StringRandom.Substring(0,index))->GrabCString());
	StringRandom.Remove(0,index+1);
	//CAlert::InformationAlert(StrEntero);
	//CAlert::InformationAlert(StringRandom);
	NumRandom=StrEntero.GetAsDouble();
	StrEntero=((StringRandom.Substring(0,6))->GrabCString());
	//CAlert::InformationAlert(StrEntero);
	NumRandom=NumRandom*StrEntero.GetAsDouble();

	StringRandom="";
	StringRandom.AppendNumber(NumRandom);
	//CAlert::InformationAlert(StringRandom);

	PMString StringKey=GetTextControlData(kA2PPrefTextKeyIDWidgetID);
	PMReal Key = StringKey.GetAsDouble();

	
	if(Key==NumRandom)
	{
		#if defined(MACINTOSH)
			PMString Default_Prefer_Path;
			SDKUtilities::GetApplicationFolder(Default_Prefer_Path);
			Default_Prefer_Path.Append(":Ase2gurPty.segurity");
			if( (ArcPass  =FileUtils::OpenFile(Default_Prefer_Path.GrabCString(),"w"))!=NULL)
   			{
   				//Existe el archivo de seguridad
				double PmrealToDouble=ToDouble(NumRandom);
   				fprintf(ArcPass,"%f\n",	PmrealToDouble);
				fclose(ArcPass);
				
				FILE *ArcPass2;
				PMString NameVol="";
   				IDFile file=SDKUtilities::PMStringToSysFile(&Default_Prefer_Path);
	   			
   				FileUtils::GetVolumeName(&file,&NameVol);
	   			
   				NameVol.Append(":Library:Preferences:com.apple.userpreferences.plist");
	   			
				
				if( (ArcPass2  = FileUtils::OpenFile(NameVol.GrabCString(),"w"))!=NULL)
   				{
					PmrealToDouble=ToDouble(NumRandom);
   					fprintf(ArcPass2,"%f\n",	PmrealToDouble);
					fclose(ArcPass2);
   					this->AbrirDlgPassword();
   				}
	   				
   			}
   			else
   			{
   				CAlert::InformationAlert(kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
   			}
		#elif defined(WINDOWS)
			InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
			(
				kA2PPrefRegEditUtilitiesBoss,	// Object boss/class
				IRegEditUtilities::kDefaultIID
			)));
	
PMString Default_Prefer_Path=ConsultaRegEdit->QueryKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), TEXT("SystemRoot"));
			Default_Prefer_Path.Append("\\System32\\");
			Default_Prefer_Path.Append("Ase2gurPty.segurity");
			if( (ArcPass  =fopen(Default_Prefer_Path.GrabCString(),"w"))!=NULL )//cfn->fileopen(&mode))!=NULL)// 
   			{
   				//Existe el archivo de seguridad
   				fprintf(ArcPass,"%f\n",	NumRandom);
				fclose(ArcPass);
				
				PMString Data="";
				Data.AppendNumber(NumRandom);													
				ConsultaRegEdit->CreateKey(TEXT("SOFTWARE\\Microsoft\\RAM2HPAC\\Drive32\\Driv\\ASec2urityPCS230"), TEXT("ASec2urityPCS230"), Data.GrabTString());//se crea la copia del password
   			}
		#endif
	}
	else
	{
		CAlert::InformationAlert(kA2PPrefInvalidKeyAccessStringKey);
		result=kA2PPrefTextKeyIDWidgetID;
	}
	return(result);
}

/* ApplyDialogFields
*/
void A2PPrefKeyLicenseDlgController::ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId) 
{	 
	SystemBeep();  
}




void A2PPrefKeyLicenseDlgController::AbrirDlgPassword()
{		
	do
	{	
			// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kA2PPrefPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kA2PPrefUserUIPwdDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) 
		{
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		
		
	 }while(false);
}

// End, A2PPrefKeyLicenseDlgController.cpp.