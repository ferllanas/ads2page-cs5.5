/*
//	File:	A2P20BotonSeleccionObserver.cpp
//	
//	Programa que observa cuando tu cambias el estado de un checkbox
//	al hacer este cambio, se verifica en cual checkbox se ralizo el cambio 
//	uesto que cada check box tien ena funcion determina por ejemplo habilitar o deshabilitar
//	otros widgets.
*/
#include "VCPlugInHeaders.h"

// Interface includes
#include "ISubject.h"
#include "IControlView.h"
#include "ITriStateControlData.h"
#include "ITextValue.h"
#include "IPanelControlData.h"
// Implem includes

#include "CObserver.h"
#include "A2PPrefID.h"
#include "CAlert.h"
#include "IWidgetParent.h"
//#include "TblMutationHelper.h"

/**
	Implements IObserver. The SelDlgChekBoxObserver class is derived from CObserver, and overrides the
	AutoAttach(), AutoDetach(), and Update() methods.
	This class provides the capability to observe the widgets on the panel and respond appropriately
	to changes in their state.
	
	@author Ian Paterson

*/
class SelDlgChekBoxObserver : public CObserver
{
public:
	/**
		Constructor for SelDlgChekBoxObserver class
	*/		
	SelDlgChekBoxObserver(IPMUnknown *boss);
	/**
		Destructor for SelDlgChekBoxObserver class - 
		performs cleanup 
	*/
	~SelDlgChekBoxObserver();
	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	*/		
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when the widget is hidden.
	*/		
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 
		@param theChange this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject this provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol the protocol along which the change occurred.
		@param changedBy this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/
	virtual void Update(const ClassID& theChange, ISubject* theSubject, const PMIID &protocol, void* changedBy);

private:
	
	void attachWidget(InterfacePtr<IPanelControlData>&  panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);
	void detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID);

	/**
		Habilita o deshabilita un determinado Widget dependiendo el estado del checkbox.
		
		  @param widgetID, Widget al que se aplicara la accion.
		  @param checkbox, indica el estado del checkbox y por lo tanto la accion que se debe ejecutar 
			al widget.
	*/
	void HabilDeshaWitgetsDependientes(const WidgetID& widgetID,bool16 ChecBoxSeleccionado);
	
	/**
		Determina el estado de seleccion de un checkbox determinado por widgetID.
		
		  @param widgetID, checbox a preguntar.
	*/
	bool16 isSelectedTriState(const WidgetID&  widgetID);
	const PMIID fObserverIID;

};

CREATE_PMINTERFACE(SelDlgChekBoxObserver, kA2PPrefSelDlgCheckBoxObserverImpl)

/*
Constructor
*/
SelDlgChekBoxObserver::SelDlgChekBoxObserver(IPMUnknown* boss)
: CObserver(boss), fObserverIID(IID_IOBSERVER)
{
	
}

/*
Constructor
*/
SelDlgChekBoxObserver::~SelDlgChekBoxObserver()
{
	
}

/*
Constructor
*/
void SelDlgChekBoxObserver::AutoAttach()
{
	
	do {
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}
		
		InterfacePtr<IPanelControlData>	panelControlData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelControlData==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;
		}
		const PMIID radioProtocol = IID_ITRISTATECONTROLDATA;
		this->attachWidget(panelControlData, kA2PPrefCkBoxCapaEditID, radioProtocol);
		this->attachWidget(panelControlData, kA2PPrefCkBoxCapaImagID, radioProtocol);
		this->attachWidget(panelControlData, kA2PPrefCkBoxCapaEConID, radioProtocol);
		this->attachWidget(panelControlData, kA2PPrefCkBoxCapaESinID, radioProtocol);
		this->attachWidget(panelControlData, kA2PPrefCkBoxCreaEtiqConID, radioProtocol);
		this->attachWidget(panelControlData, kA2PPrefCkBoxCreaEtiqSinID, radioProtocol);
		this->attachWidget(panelControlData, kA2PPrefCkBoxMostrarDlgIssueWidgetID, radioProtocol);
		this->attachWidget(panelControlData, kA2PPrefUsarPlantilleroWidgetID, radioProtocol);
		
	//	initialiseState();
	} while(kFalse);
}

/*
Constructor
*/
void SelDlgChekBoxObserver::AutoDetach()
{
	do {
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}
		
		InterfacePtr<IPanelControlData>	panelControlData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panelControlData==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;
		}
								
		const PMIID radioProtocol = IID_ITRISTATECONTROLDATA;
		this->detachWidget(panelControlData, kA2PPrefCkBoxCapaEditID,radioProtocol);
		this->detachWidget(panelControlData, kA2PPrefCkBoxCapaImagID,radioProtocol);
		this->detachWidget(panelControlData, kA2PPrefCkBoxCapaEConID,radioProtocol);
		this->detachWidget(panelControlData, kA2PPrefCkBoxCapaESinID,radioProtocol);
		this->detachWidget(panelControlData, kA2PPrefCkBoxCreaEtiqConID,radioProtocol);
		this->detachWidget(panelControlData, kA2PPrefCkBoxCreaEtiqSinID,radioProtocol);
		this->detachWidget(panelControlData, kA2PPrefCkBoxMostrarDlgIssueWidgetID,radioProtocol);
		this->detachWidget(panelControlData, kA2PPrefUsarPlantilleroWidgetID, radioProtocol);
		
	} while(kFalse);
}



/* attachWidget
*/
void SelDlgChekBoxObserver::attachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{

	ASSERT(panelControlData);

//	TRACE("TblAttWidgetObserver::AttachWidget(widgetID=0x%x, interfaceID=0x%x\n", widgetID, interfaceID);
	do
	{
		if(panelControlData==nil) {
			break;
		}

		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView);
		if (controlView == nil) {
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject);
		if (subject == nil) { 
			break;
		}
		subject->AttachObserver(this, interfaceID, fObserverIID);
	}
	while (kFalse);
}

/* detachWidget
*/
void SelDlgChekBoxObserver::detachWidget(InterfacePtr<IPanelControlData>& panelControlData, const WidgetID& widgetID, const PMIID& interfaceID)
{
	ASSERT(panelControlData);
	do
	{
		if(panelControlData == nil) 
		{
			break;
		}
		IControlView* controlView = panelControlData->FindWidget(widgetID);
		ASSERT(controlView);
		if (controlView == nil) 
		{
			break;
		}

		InterfacePtr<ISubject> subject(controlView, UseDefaultIID());
		ASSERT(subject);
		if (subject == nil) {
			break;
		}
		subject->DetachObserver(this, interfaceID, fObserverIID);
	}
	while (kFalse);
}


/*
Constructor
*/
void SelDlgChekBoxObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	bool16 ChecBoxSeleccionado;
	do 
	{
		ASSERT(theSubject);
		if(theSubject==nil)
		{
			break;
		}

		InterfacePtr<IControlView>  icontrolView(theSubject, UseDefaultIID());
		ASSERT(icontrolView);
		if(icontrolView==nil) 
		{
			break;
		}

		WidgetID thisID = icontrolView->GetWidgetID();
		
		///Pregunta por checkbox que cambio de estado
		if(thisID == kA2PPrefCkBoxCapaEditID ) 
		{
			ChecBoxSeleccionado=isSelectedTriState(kA2PPrefCkBoxCapaEditID);
			HabilDeshaWitgetsDependientes(kA2PPrefCkBoxCapaEditVisID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefCkBoxCapaEditBlqID,ChecBoxSeleccionado);
		}

		if(thisID == kA2PPrefCkBoxCapaImagID ) 
		{
			ChecBoxSeleccionado=isSelectedTriState(kA2PPrefCkBoxCapaImagID);
			HabilDeshaWitgetsDependientes(kA2PPrefCkBoxCapaImagVisID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefCkBoxCapaImagBlqID,ChecBoxSeleccionado);
		}

		if(thisID == kA2PPrefCkBoxCapaEConID ) 
		{
			ChecBoxSeleccionado=isSelectedTriState(kA2PPrefCkBoxCapaEConID);
			HabilDeshaWitgetsDependientes(kA2PPrefCkBoxCapaEConVisID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefCkBoxCapaEConBlqID,ChecBoxSeleccionado);
		}

		if(thisID == kA2PPrefCkBoxCapaESinID ) 
		{
			ChecBoxSeleccionado=isSelectedTriState(kA2PPrefCkBoxCapaESinID);
			HabilDeshaWitgetsDependientes(kA2PPrefCkBoxCapaESinVisID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefCkBoxCapaESinBlqID,ChecBoxSeleccionado);
		}
		
		if(thisID == kA2PPrefUsarPlantilleroWidgetID)
		{
			ChecBoxSeleccionado=isSelectedTriState(kA2PPrefUsarPlantilleroWidgetID);
			if(ChecBoxSeleccionado)
			{
				HabilDeshaWitgetsDependientes(kA2PPrefCkBoxMostrarDlgIssueWidgetID,!ChecBoxSeleccionado);
				HabilDeshaWitgetsDependientes(kA2PPrefParagraphStyleNameToFolioWidgetID,!ChecBoxSeleccionado);
				HabilDeshaWitgetsDependientes(kA2PPrefNameEditionTextEditWidgetID,ChecBoxSeleccionado);
				HabilDeshaWitgetsDependientes(kA2PPrefBoxDespHorWidgetID,!ChecBoxSeleccionado);
				HabilDeshaWitgetsDependientes(kA2PPrefBoxDespVerWidgetID,!ChecBoxSeleccionado);
			}
			else
			{
				HabilDeshaWitgetsDependientes(kA2PPrefCkBoxMostrarDlgIssueWidgetID,!ChecBoxSeleccionado);
				
				ChecBoxSeleccionado=isSelectedTriState(kA2PPrefCkBoxMostrarDlgIssueWidgetID);
				
				HabilDeshaWitgetsDependientes(kA2PPrefParagraphStyleNameToFolioWidgetID,ChecBoxSeleccionado);
				HabilDeshaWitgetsDependientes(kA2PPrefNameEditionTextEditWidgetID,ChecBoxSeleccionado);
				HabilDeshaWitgetsDependientes(kA2PPrefBoxDespHorWidgetID,ChecBoxSeleccionado);
				HabilDeshaWitgetsDependientes(kA2PPrefBoxDespVerWidgetID,ChecBoxSeleccionado);
			}
			
			break;
		}
		
		
		if(thisID == kA2PPrefCkBoxMostrarDlgIssueWidgetID ) 
		{
			
			ChecBoxSeleccionado=isSelectedTriState(kA2PPrefCkBoxMostrarDlgIssueWidgetID);
			//Changed to A2P v3.0
			//HabilDeshaWitgetsDependientes(kA2PPrefComboBoxFontWidgetID,ChecBoxSeleccionado);
			//HabilDeshaWitgetsDependientes(kA2PPrefBoxSizeFontWidgetID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefParagraphStyleNameToFolioWidgetID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefNameEditionTextEditWidgetID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxDespHorWidgetID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxDespVerWidgetID,ChecBoxSeleccionado);
			break;
		}

		if(thisID == kA2PPrefCkBoxCreaEtiqConID ) 
		{
			ChecBoxSeleccionado=isSelectedTriState(kA2PPrefCkBoxCreaEtiqConID);
			HabilDeshaWitgetsDependientes(kA2PPrefComboBoxColEtiqConID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefComboBoxColTextEtiqConID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxTamMarcEtiqConID,ChecBoxSeleccionado);

		/*	HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiCon1ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiCon2ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiCon3ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiCon4ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiCon5ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiCon6ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiCon7ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiCon8ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiCon9ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiConAID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiConBID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiConCID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiConDID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiConEID,ChecBoxSeleccionado);

			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiCon1ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiCon2ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiCon3ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiCon4ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiCon5ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiCon6ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiCon7ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiCon8ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiCon9ID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiConAID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiConBID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiConCID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiConDID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiConEID,ChecBoxSeleccionado);
			*/
		}

		if(thisID == kA2PPrefCkBoxCreaEtiqSinID ) 
		{
			ChecBoxSeleccionado=isSelectedTriState(kA2PPrefCkBoxCreaEtiqSinID);
			HabilDeshaWitgetsDependientes(kA2PPrefComboBoxColEtiqSinID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefComboBoxColTextEtiqSinID,ChecBoxSeleccionado);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxTamMarcEtiqSinID,ChecBoxSeleccionado);


		/*	HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSin1ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSin2ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSin3ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSin4ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSin5ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSin6ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSin7ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSin8ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSin9ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSinAID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSinBID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSinCID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSinDID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNumCamOrdEtiSinEID,kTrue);

			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSin1ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSin2ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSin3ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSin4ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSin5ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSin6ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSin7ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSin8ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSin9ID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSinAID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSinBID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSinCID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSinDID,kTrue);
			HabilDeshaWitgetsDependientes(kA2PPrefBoxNomCamOrdEtiSinEID,kTrue);
			*/
		}
	} while(kFalse);
}

/**
*/
bool16 SelDlgChekBoxObserver::isSelectedTriState(const WidgetID&  widgetID)
{
	bool16  retval=kFalse;
	do 
	{
		//encuentra el parent del widget seleccionado
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}
		//encuentra la interfaz IPanelControlData a partir del checkbox seleccionado
		InterfacePtr<IPanelControlData>	iPanelControlData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(iPanelControlData==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;
		}
		//obten la interfaz IControlView buscando el Widget o el checkbox seleccionado
		IControlView * iControlView  = iPanelControlData->FindWidget(widgetID);
		ASSERT(iControlView);
		if(iControlView==nil) {
			break;
		}
		//obtiene la interfaz ITriStateControlData para determinar el estado de seleccion del checkbox
		InterfacePtr<ITriStateControlData> iTriStateControlData(iControlView, UseDefaultIID());
		ASSERT(iTriStateControlData);
		if(iTriStateControlData==nil) {
			break;
		}
		retval = iTriStateControlData->IsSelected();
	} while(kFalse);
	return retval;
}
/**
*/
void SelDlgChekBoxObserver::HabilDeshaWitgetsDependientes(const WidgetID& widgetID,bool16 ChecBoxSeleccionado)
{
	do
	{	
		//encuentra el la interfaz IWidgetParent del checkbox seleccionado
		InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}
		//encuentra la interfaz IPanelControlData a partir del parent
		InterfacePtr<IPanelControlData>	iPanelControlData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(iPanelControlData==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;
		}
		//obtengo el contro de vista del widget determinado p�ra habilitarlo o deshabilitarlo segun sea el caso
		IControlView * iControlView  = iPanelControlData->FindWidget(widgetID);
		ASSERT(iControlView);
		if(iControlView==nil) 
		{
			break;
		}
		iControlView->Enable(ChecBoxSeleccionado);
	}while(false);
}






