/*
//	File:	A2P20CreaPestañaCapas.cpp
//
//	Creador de la interfaz de la pestaña Capas
//
*/

#include "VCPlugInHeaders.h"

// Implementation includes:
#include "CPanelCreator.h"

// Project includes:
#include "A2PPrefID.h"


/** A2P20CreaPestañaCapas
	implementa IPanelCreator basedo sobre la implementacion parcial CPanelCreator.
	Eliminamos CPanelCreator's GetPanelRsrcID().
	@author Fernando llanas
*/
class CreatorPestanaCapas : public CPanelCreator
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CreatorPestanaCapas(IPMUnknown *boss) : CPanelCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CreatorPestanaCapas() {}

		/** 
			Returns the local resource ID of the ordered panels.
		*/
		virtual RsrcID GetPanelRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(CreatorPestanaCapas, kA2PPrefCapasPanelCreatorImpl)

/* GetPanelRsrcID
*/
RsrcID CreatorPestanaCapas::GetPanelRsrcID() const
{
	return kA2PPrefCapasPanelCreatorResourceID;
}

// End, YinPanelCreator.cpp
