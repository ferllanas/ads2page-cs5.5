/*
//
*/

#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"
// Interface includes:
// none.

// General includes:
#include "CDialogController.h"

// Project includes:

#include "A2PPrefID.h"
#include "PMString.h"
#include <stdio.h>
#include <string.h>
//#include <io.h>
//#include "fstream.h"
//#include <iomanip.h>
#include "CAlert.h"
//#include "SelDlgActionComponent.cpp"
//////////////////////////
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialog.h"
#include "IDialogCreator.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "ISession.h"
#include "CAlert.h"
#include "SysFileList.h"
#include "IOpenFileDialog.h"
#include "ITextControlData.h"
#include <stdlib.h>
// General includes:
#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "FileUtils.h"
//#include <types.h> 
//#include <stat.h> 
#include <stdio.h>
#include <string.h>
//#include <io.h>
#include <stdlib.h>
#include <time.h>

//#include "InterlasaRegEditUtilities.h"
//#include "A2PPrefEncrypt.h"//NO UTILIZADO
//#include "InterlasaRegEditUtilities.h"


/** 
*/
class A2PContratoDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		A2PContratoDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~A2PContratoDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext *myContext);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext *myContext);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId);

		virtual void UserCancelled();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(A2PContratoDialogController, kA2PPrefDialogContratoControllerImpl)
/* InitializeDialogFields
*/
void A2PContratoDialogController::InitializeDialogFields(IActiveContext *myContext) 
{	/***************AL ABRIR LA FORMA DE LA LISTA*********/

	SetTextControlData(kA2PPrefTextKeyIDWidgetID,"OK");
}

/* ValidateDialogFields
*/
/************ACOMODA O PONE LAS PREFERENCIAS SOBRE LAS CAJAS ETC....********/
WidgetID A2PContratoDialogController::ValidateDialogFields(IActiveContext *myContext) 
{
	WidgetID result = kNoInvalidWidgets;
	return(result);
}

/* ApplyDialogFields
*/
void A2PContratoDialogController::ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId) 
{	 
	SystemBeep();  
}



void A2PContratoDialogController::UserCancelled()
{
	SetTextControlData(kA2PPrefTextKeyIDWidgetID,"kasaka");
}


// End, A2PContratoDialogController.cpp.