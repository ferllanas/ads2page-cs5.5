/*
//	File:	A2P20CreaPestañaOrden.cpp
//
//	Creador de la interfaz de la pestaña Orden en Etiquetas
//
*/

#include "VCPlugInHeaders.h"

// Implementation includes:
#include "CPanelCreator.h"

// Project includes:
#include "A2PPrefID.h"

/** A2P20CreaPestañaOrden
	implementa IPanelCreator basedo sobre la implementacion parcial CPanelCreator.
	Eliminamos CPanelCreator's GetPanelRsrcID().
	@author Fernando llanas
*/
class CreatorPestanaOrden : public CPanelCreator
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CreatorPestanaOrden(IPMUnknown *boss) : CPanelCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CreatorPestanaOrden() {}

		/** 
			Returns the local resource ID of the ordered panels.
		*/
		virtual RsrcID GetPanelRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(CreatorPestanaOrden, kA2PPrefOrdenPanelCreatorImpl)

/* GetPanelRsrcID
*/
RsrcID CreatorPestanaOrden::GetPanelRsrcID() const
{
	return kA2PPrefOrdenPanelCreatorResourceID;
}
