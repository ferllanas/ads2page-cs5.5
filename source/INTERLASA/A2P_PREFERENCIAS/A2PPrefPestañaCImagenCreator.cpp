/*
//	
//	File:	A2P20CreaPestañaCImagen.cpp
//
//	Creador de la interfaz de la pestaña Imagen
//
//
*/

#include "VCPlugInHeaders.h"

// Implementation includes:
#include "CPanelCreator.h"

// Project includes:
#include "A2PPrefID.h"

/** A2P20CreaPestañaCImagen
	implementa IPanelCreator basedo sobre la implementacion parcial CPanelCreator.
	Eliminamos CPanelCreator's GetPanelRsrcID().
	@author Fernando llanas
*/
class CreatorPestanaCImagen : public CPanelCreator
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		CreatorPestanaCImagen(IPMUnknown *boss) : CPanelCreator(boss) {}

		/** 
			Destructor.
		*/
		virtual ~CreatorPestanaCImagen() {}

		/** 
			Returns the local resource ID of the ordered panels.
		*/
		virtual RsrcID GetPanelRsrcID() const;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(CreatorPestanaCImagen, kA2PPrefCImagPanelCreatorImpl)

/* GetPanelRsrcID
*/
RsrcID CreatorPestanaCImagen::GetPanelRsrcID() const
{
	return kA2PPrefCImagPanelCreatorResourceID;
}

// End, YinPanelCreator.cpp
