
#include "VCPluginHeaders.h"

#include "IDialogController.h"
#include "ISelectableDialogSwitcher.h"
#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "IDropDownListController.h"
#include "IStringListControlData.h"
#include "IControlView.h"
#include "CAlert.h"
#include "SDKFileHelper.h"
#include "FileTreeUtils.h"
#include "FileUtils.h"
#include "A2PPrefActionsOnDLGPreferenciasUtils.h"
#include "IA2PPreferenciasFunciones.h"
#include "A2PPrefID.h"

#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
	#include "PlatformFileSystemIterator.h"
#endif
#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"
	#include "..\Interlasa_common\PlatformFileSystemIterator.h"
#endif

bool16 A2PPrefActionsOnDLGPreferenciasUtils::LlenaDlgPreferenciasXNombrePref(IControlView *DlgPrefView,PMString NombrePreferencia)
{  //CAlert::InformationAlert("entro LlenaDlgPreferenciasXNombrePref");

	bool16 retval=kFalse;
	Preferencias Pref;
	FILE *Archivo;
	//int PosPrefenComboBox;//posicion en el combo de preferencias del ultimo archivo modificado
    int PosPrefenComboBox=0;
	do
	{InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
			(
				kA2PPreferenciasFuncionesBoss,	// Object boss/class
				IA2PPreferenciasFunctions::kDefaultIID
			)));
			
		if(A2PPrefeFuntions==nil)
		{
			CAlert::InformationAlert("A2PPrefActionsOnDLGPreferenciasUtils::LlenaDlgPreferenciasXNombrePref A2PPrefeFuntions");
			retval=kFalse;
			break;
		}
		//CAlert::InformationAlert("1");

		if(DlgPrefView==nil)
		{
			CAlert::InformationAlert("A2PPrefActionsOnDLGPreferenciasUtils::LlenaDlgPreferenciasXNombrePref DlgPrefView");
			retval=kFalse;
			break;
		}
		//CAlert::InformationAlert("2");
		
		InterfacePtr<IDialogController> DlgPrefController(DlgPrefView,IID_IDIALOGCONTROLLER);
		if(DlgPrefController==nil)
		{
			CAlert::InformationAlert("A2PPrefActionsOnDLGPreferenciasUtils::LlenaDlgPreferenciasXNombrePref DlgPrefController");
			retval=kFalse;
			break;
		}
		
		//CAlert::InformationAlert("3");
		
		//NombrePreferencia=InterlasaUtilities::MacToUnix(NombrePreferencia);
		if((Archivo=FileUtils::OpenFile(NombrePreferencia.GrabCString(),"r"))!=NULL)
		{
			//CAlert::InformationAlert("OpenFile 11111");
			fclose(Archivo);
		
			
			//lectura del Archivo preferencias Defaul(por lo tanto las ultimas preferencias que se modificaron)
			if(A2PPrefeFuntions->leerDocDePreferencia(NombrePreferencia.GrabCString(),Pref))
			{	//CAlert::InformationAlert("NombrePreferencia== "+NombrePreferencia);	
				//Pref.Nombrepreferencias = DlgPrefController->GetTextControlData(kA2PPrefComboBoxSelecPreferID);
               // CAlert::InformationAlert("Pref== "+Pref.Nombrepreferencias); //si lo obtiene
			   //A2PPrefeFuntions->ConvertirPuntosAUnidadesdePreferencias(Pref);		
				
				//llenado del Combo preferencias y obtencion de la posicion del la preferencia q
				//que fue modificada la ultima vez que se entro a preferencias				
				PosPrefenComboBox = llenarComboPrefDeDlgPref(DlgPrefView, Pref); //AQUI MERO
                		
				//Coloca preferencias sobre pestaÒa documentos
				DlgPrefController->SetTextValue(kA2PPrefBoxAnchoPagID,Pref.AnchoPagina);//ancho
				DlgPrefController->SetTextValue(kA2PPrefBoxAltoPagID,Pref.AltoPag);
				DlgPrefController->SetTextValue(kA2PPrefBoxMargExtID,Pref.MargenExterior);
				DlgPrefController->SetTextValue(kA2PPrefBoxMargInfID,Pref.MargenInferior);
				DlgPrefController->SetTextValue(kA2PPrefBoxMargIntID,Pref.MargenInterior);
				DlgPrefController->SetTextValue(kA2PPrefBoxMargSupID,Pref.MargenSuperior);
				DlgPrefController->SetTextValue(kA2PPrefBoxMedianilID,Pref.Medianil);
				//Elimina los espacios de esta cadena
				//coloca numero de columnas
				DlgPrefController->SetTextValue(kA2PPrefBoxNumColID,Pref.NumColumas);
		       //CAlert::InformationAlert("COLUMNAS: "+Pref.NumColumas);  //OK 

				//Coloca la preferencia que se modifico por ultima vez en el combo preferencias
				InterfacePtr<IWidgetParent>	myParent(DlgPrefView, UseDefaultIID());				
				if(myParent==nil)
				{   retval=kFalse;
					CAlert::InformationAlert("Salio por algun motivo  myParent");
					break;
				}
				
				InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
				if(panel==nil)
				{	retval=kFalse;
					CAlert::InformationAlert("Salio por algun motivo  panel");
					break;
				}
		InterfacePtr<IControlView>CVComboBoxSelecPrefer(panel->FindWidget(kA2PPrefComboBoxSelecPreferID), UseDefaultIID() );
				if(CVComboBoxSelecPrefer==nil)
				{	retval=kFalse;
					CAlert::InformationAlert("Salio por algun motivo  CVComboBoxSelecPrefer");
					break;
				}
								
				InterfacePtr<IDropDownListController>IDDLDrComboBoxSelecPrefer( CVComboBoxSelecPrefer, IID_IDROPDOWNLISTCONTROLLER);
				if(IDDLDrComboBoxSelecPrefer==nil)
				{
					retval=kFalse;
					CAlert::InformationAlert("Salio por algun motivo  IDDLDrComboBoxSelecPrefer");
					break;
				}
				IDDLDrComboBoxSelecPrefer->Select(PosPrefenComboBox);//aqui es donde se supone que lo coloca ENTERO
                //IDDLDrComboBoxSelecPrefer->Select(1);
				//CAlert::InformationAlert("7"); 
				DlgPrefController->SetTriStateControlData(kA2PPrefUsarPlantilleroWidgetID,Pref.UtilizaPlantilleroPref);
				//Preferencias de pestaÒa capas
				//CAlert::InformationAlert("8"); 
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaEditID,Pref.CapaEditorial);
				if(Pref.CapaEditorial==1)
				{
					HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefCkBoxCapaEditVisID,kTrue);
					HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefCkBoxCapaEditBlqID,kTrue);
				}
				else
				{
					HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefCkBoxCapaEditVisID,kFalse);
					HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefCkBoxCapaEditBlqID,kFalse);
				}
                //CAlert::InformationAlert("9"); 

				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaEditVisID,Pref.CapaEditorialVisible);
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaEditBlqID,Pref.CapaEditorialBlocked);
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaImagID,Pref.CapaImagen);
				//CAlert::InformationAlert("10"); 
				if(Pref.CapaImagen==1)
				{
					HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefCkBoxCapaImagVisID,kTrue);
					HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefCkBoxCapaImagBlqID,kTrue);
				}
				else
				{
					HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefCkBoxCapaImagVisID, kFalse);
					HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefCkBoxCapaImagBlqID, kFalse);
				}
		
				//CAlert::InformationAlert("11"); 
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaImagVisID,Pref.CapaImagenVisible);
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaImagBlqID,Pref.CapaImagenBlocked);
		
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaEConID,Pref.CapaEtiquetaConPublicidad);
				if(Pref.CapaEtiquetaConPublicidad==1)
				{
					HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefCkBoxCapaEConVisID,kTrue);
					HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefCkBoxCapaEConBlqID,kTrue);
				}
				else
				{
					HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefCkBoxCapaEConVisID, kFalse);
					HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefCkBoxCapaEConBlqID, kFalse);
				}
		        //CAlert::InformationAlert("12"); 
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaEConVisID,Pref.CapaEtiquetaConPublicidadVisible);
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaEConBlqID,Pref.CapaEtiquetaConPublicidadBlocked);
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaESinID,Pref.CapaEtiquetaSinPublicidad);

				if(Pref.CapaEtiquetaSinPublicidad==1)
				{
					HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefCkBoxCapaESinVisID,kTrue);
					HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefCkBoxCapaESinBlqID,kTrue);
				}
				else
				{
					HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefCkBoxCapaESinVisID,kFalse);
					HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefCkBoxCapaESinBlqID,kFalse);
				}
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaESinVisID,Pref.CapaEtiquetaSinPublicidadVisible);
				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCapaESinBlqID,Pref.CapaEtiquetaSinPublicidadBlocked);
	            //CAlert::InformationAlert("13");  

				//Llenado de preferencias en pestaÒa Imagen
			
				ColocaEnComboColor(DlgPrefView, kA2PPrefComboBoxColImagConID, Pref.ColorCajaImagenConPublicidad);
				ColocaEnComboColor(DlgPrefView, kA2PPrefComboBoxColMaImagConID, Pref.ColorMargenCajaImagenConPublicidad);
				
				
				InterfacePtr<IControlView>		CVComboBoxAjuste(panel->FindWidget(kA2PPrefComboBoxAjusteID), UseDefaultIID() );
				if(CVComboBoxAjuste==nil)
				{
					CAlert::InformationAlert("No se encontro el editbox");
					retval=kFalse;
					break;
				}
				//CAlert::InformationAlert("14");  			
				InterfacePtr<IDropDownListController>	DDLCrComboBoxAjuste( CVComboBoxAjuste, IID_IDROPDOWNLISTCONTROLLER);
				if(DDLCrComboBoxAjuste==nil)
				{
					CAlert::InformationAlert("No se encontro el texto de direccion");
					retval=kFalse;
					break;
				}
			
			 //CAlert::InformationAlert("15");  	
				
					if(Pref.FitCajaImagenConPublicidad.Contains(kA2PPrefAjusteContenidoAFrameStringKey))
					{
						// CAlert::InformationAlert("16");  	
						DDLCrComboBoxAjuste->Select(0);
						
					}
					if(Pref.FitCajaImagenConPublicidad.Contains(kA2PPrefAjusteCentrarStringKey))
					{
						 //CAlert::InformationAlert("17---");  	
						DDLCrComboBoxAjuste->Select(1);
						
					}
					if(Pref.FitCajaImagenConPublicidad.Contains(kA2PPrefAjusteProporcionalmenteStringKey))
					{
						//CAlert::InformationAlert("18");  	
						DDLCrComboBoxAjuste->Select(2);
						
					}
					if(Pref.FitCajaImagenConPublicidad.Contains(kA2PPrefNingunoStringKey))
					{	
						 //CAlert::InformationAlert("19");  	
						DDLCrComboBoxAjuste->Select(3);
						
					}
									
                //CAlert::InformationAlert("20");  	
				ColocaEnComboColor(DlgPrefView, kA2PPrefComboBoxColImagSinID,Pref.ColorCajaImagenSinPublicidad);
				ColocaEnComboColor(DlgPrefView, kA2PPrefComboBoxColMaImagSinID,Pref.ColorMargenCajaImagenSinPublicidad);
						
				//Llenado de preferencias de la pestaÒa importar
		
				switch(Pref.SepEnCampos.GetAsNumber())
				{
					 //CAlert::InformationAlert("21");  	
					case 9://tabulador
						DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonTabWidgetID,kTrue);
						HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefBoxCarEspecialLecturaEID,kFalse);
						break;
					case 44://coma
						DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonComaWidgetID,kTrue);
						HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefBoxCarEspecialLecturaEID,kFalse);
						break;
					case 59://punto y coma 
						DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonPyComaWidgetID,kTrue);
						HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefBoxCarEspecialLecturaEID,kFalse);
						break;
					case 58://dos pundos
						DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonDosPuntosWidgetID,kTrue);
						HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefBoxCarEspecialLecturaEID,kFalse);
						break;
					default:
						DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonEspecialWidgetID,kTrue);
						PMString a;
						a.AppendW(Pref.SepEnCampos.GetAsNumber());
						DlgPrefController->SetTextControlData(kA2PPrefBoxCarEspecialLecturaEID,a);	
						//CAlert::InformationAlert("coloco en SEPARACION EN CAMPOS");
						break;

				}
			
				//CAlert::InformationAlert("22");  	
				PMString TempString="";
				TempString.AppendNumber(Pref.PosCampos1);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLectura1ID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLectura1ID,Pref.NomCampos1);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos2);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLectura2ID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLectura2ID,Pref.NomCampos2);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos3);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLectura3ID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLectura3ID,Pref.NomCampos3);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos4);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLectura4ID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLectura4ID,Pref.NomCampos4);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos5);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLectura5ID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLectura5ID,Pref.NomCampos5);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos6);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLectura6ID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLectura6ID,Pref.NomCampos6);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos7);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLectura7ID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLectura7ID,Pref.NomCampos7);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos8);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLectura8ID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLectura8ID,Pref.NomCampos8);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos9);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLectura9ID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLectura9ID,Pref.NomCampos9);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos10);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLecturaAID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLecturaAID,Pref.NomCampos10);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos11);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLecturaBID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLecturaBID,Pref.NomCampos11);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos12);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLecturaCID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLecturaCID,Pref.NomCampos12);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos13);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLecturaDID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLecturaDID,Pref.NomCampos13);
				TempString="";
				TempString.AppendNumber(Pref.PosCampos14);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNumCampoLecturaEID,TempString);
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCampoLecturaEID,Pref.NomCampos14);

                 //CAlert::InformationAlert("23");  	

				//Llenado de preferencias de la pestaÒa orden de coordenadas
				//CAlert::InformationAlert(Pref.SepEnCoordenadas+ " SepEnCoordenadas 1");
				Pref.SepEnCoordenadas.Remove(0,1);
				PMString CaracterSeparacion=Pref.SepEnCoordenadas;
               	int32 indexCar=CaracterSeparacion.LastIndexOfWChar(94);
				

				CaracterSeparacion.Remove(indexCar,CaracterSeparacion.NumUTF16TextChars());
				//CAlert::InformationAlert(CaracterSeparacion +"27");
                                                   //caja donde se supone que esta el Unidades en Coor.:
				DlgPrefController->SetTextControlData(kA2PPrefBoxCadUnidadesLecturaEID,CaracterSeparacion);	

		       //CAlert::InformationAlert("28"); 
				//CAlert::InformationAlert(Pref.SepEnCoordenadas);
				if(indexCar==0)
				{  Pref.SepEnCoordenadas.Remove(0,	1);
				  // CAlert::InformationAlert("29"); 
				}

				else
				{Pref.SepEnCoordenadas.Remove(0,	(Pref.SepEnCoordenadas.NumUTF16TextChars()-indexCar)-1);
				 //CAlert::InformationAlert("30"); 
				}
				//CAlert::InformationAlert(Pref.SepEnCoordenadas);
                 	
				switch(Pref.SepEnCoordenadas.GetAsNumber())
				{   	
					case 9://tabulador
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonEspecialOrdCWidgetID,kFalse);
					HabilDeshaWitgetsDependientes(DlgPrefView, kA2PPrefBoxCarEspecialLecturaEID,kFalse);
					break;

					case 44://coma
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonEspecialOrdCWidgetID,kFalse);
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonComaOrdCWidgetID,kTrue);
					break;
					case 59://punto y coma 
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonEspecialOrdCWidgetID,kFalse);
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonPyComaOrdCWidgetID,kTrue);
					break;
					case 58://dos pundos
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonEspecialOrdCWidgetID,kFalse);
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonDosPuntosOrdCWidgetID,kTrue);
					break;
					default:
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonEspecialOrdCWidgetID,kTrue);
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonEspecialOrdCWidgetID,kTrue);
					PMString a;
					a.AppendW(Pref.SepEnCoordenadas.GetAsNumber());
					DlgPrefController->SetTextControlData(kA2PPrefBoxCarEspecialLecturaOrdCID,a);		
					break;

				}

			
				  //CAlert::InformationAlert("31");  	
				InterfacePtr<IControlView>		CVComboBoxTLBR(panel->FindWidget(kA2PPrefComboBoxTLBRWidgetID), UseDefaultIID() );
				if(CVComboBoxTLBR==nil)
				{
					retval=kFalse;
					CAlert::InformationAlert("Salio por algun motivo  CVComboBoxTLBR");
					break;
				}
				
				InterfacePtr<IControlView>		CVComboBoxXY(panel->FindWidget(kA2PPrefComboBoxXYWidgetID), UseDefaultIID() );
				if(CVComboBoxXY==nil)
				{
					retval=kFalse;
					CAlert::InformationAlert("Salio por algun motivo  CVComboBoxXY");
					break;
				}


				switch(Pref.TipoLecturaCoor.GetAsNumber())
				{
					case 1:
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonTLBRWidgetID,kTrue);
					ColocaEnComboTLBR(DlgPrefView, kA2PPrefComboBoxTLBRWidgetID,Pref.OrdendeLectura);
					CVComboBoxTLBR->Show(kTrue);
					CVComboBoxTLBR->Enable(kTrue);
					CVComboBoxXY->Hide();
					break;

					case 2:
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonXYWidgetID,kTrue);
					ColocaEnComboxy(DlgPrefView, kA2PPrefComboBoxXYWidgetID,Pref.OrdendeLectura);		
					CVComboBoxXY->Show(kTrue);
					CVComboBoxXY->Enable(kTrue);
					CVComboBoxTLBR->Hide();
					break;

					case 3:
					DlgPrefController->SetTriStateControlData(kA2PPrefRadioButtonTLBRSepWidgetID,kTrue);
					CVComboBoxTLBR->Disable();
					CVComboBoxXY->Disable();
					break;
				}
			



				ColocaEnComboUnidades(DlgPrefView,kA2PPrefComboBoxUniTopWidgetID, Pref.UnidadesEnTop);
				ColocaEnComboUnidades(DlgPrefView,kA2PPrefComboBoxUniLeftWidgetID, Pref.UnidadesEnLeft);
				ColocaEnComboUnidades(DlgPrefView,kA2PPrefComboBoxUniBottomWidgetID, Pref.UnidadesEnButtom);
				ColocaEnComboUnidades(DlgPrefView,kA2PPrefComboBoxUniRightWidgetID, Pref.UnidadesEnRight);
		

				//Llenado de preferencias de la pestaÒa etiquetas
		
				ColocaEnComboColor(DlgPrefView, kA2PPrefComboBoxColEtiqConID,Pref.ColorFondoEtiquetaConPublicidad);
				ColocaEnComboColor(DlgPrefView, kA2PPrefComboBoxColTextEtiqConID,Pref.ColorTextEtiquetaConPublicidad);
				ColocaEnComboColor(DlgPrefView, kA2PPrefComboBoxColEtiqSinID,Pref.ColorFondoEtiquetaSinPublicidad);
				ColocaEnComboColor(DlgPrefView, kA2PPrefComboBoxColTextEtiqSinID,Pref.ColorTextEtiquetaSinPublicidad);
				DlgPrefController->SetTextControlData(kA2PPrefBoxTamMarcEtiqConID ,Pref.TamMargenEtiquetaConPublicidad);
				DlgPrefController->SetTextControlData(kA2PPrefBoxTamMarcEtiqSinID,Pref.TamMargenEtiquetaSinPublicidad);

				DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxMostrarDlgIssueWidgetID,Pref.AbrirDialogoIssue);	

		
				Pref.NombreEditorial.Remove(0,1);
				DlgPrefController->SetTextControlData(kA2PPrefNameEditionTextEditWidgetID ,Pref.NombreEditorial);
				
				DlgPrefController->SetTextValue( kA2PPrefBoxDespHorWidgetID,Pref.DesplazamientoVerIssue);
				DlgPrefController->SetTextValue( kA2PPrefBoxDespVerWidgetID ,Pref.DesplazamientoVerIssue);
		
				//Changed A2P v3.0
				Pref.NombreEstiloIssue.StripWhiteSpace();
				DlgPrefController->SetTextControlData( kA2PPrefParagraphStyleNameToFolioWidgetID ,Pref.NombreEstiloIssue);
			

		
				//HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefComboBoxFontWidgetID,Pref.AbrirDialogoIssue);
				//HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefBoxSizeFontWidgetID,Pref.AbrirDialogoIssue);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefParagraphStyleNameToFolioWidgetID,Pref.AbrirDialogoIssue);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefNameEditionTextEditWidgetID,Pref.AbrirDialogoIssue);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefBoxDespVerWidgetID,Pref.AbrirDialogoIssue);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefBoxDespHorWidgetID,Pref.AbrirDialogoIssue);
		

				//////////////////////////////////////////////////////////////
				//llenado de preferencias de la pestaÒa Orden en etiquetas
		
			DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCreaEtiqConID,Pref.CreaEtiquetaConPublicidad);	
			if(Pref.CreaEtiquetaConPublicidad==1)
			{
			
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefBoxTamMarcEtiqConID,kTrue);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefComboBoxColTextEtiqConID,kTrue);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefComboBoxColEtiqConID,kTrue);

			
			}
			else
			{
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefBoxTamMarcEtiqConID,kFalse);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefComboBoxColTextEtiqConID,kFalse);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefComboBoxColEtiqConID,kFalse);

		
			}
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiCon1ID,Pref.PosCamposEtiCon1);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon1ID,Pref.NomCamposEtiCon1);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiCon2ID,Pref.PosCamposEtiCon2);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon2ID,Pref.NomCamposEtiCon2);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiCon3ID,Pref.PosCamposEtiCon3);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon3ID,Pref.NomCamposEtiCon3);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiCon4ID,Pref.PosCamposEtiCon4);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon4ID,Pref.NomCamposEtiCon4);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiCon5ID,Pref.PosCamposEtiCon5);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon5ID,Pref.NomCamposEtiCon5);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiCon6ID,Pref.PosCamposEtiCon6);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon6ID,Pref.NomCamposEtiCon6);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiCon7ID,Pref.PosCamposEtiCon7);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon7ID,Pref.NomCamposEtiCon7);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiCon8ID,Pref.PosCamposEtiCon8);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon8ID,Pref.NomCamposEtiCon8);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiCon9ID,Pref.PosCamposEtiCon9);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon9ID,Pref.NomCamposEtiCon9);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiConAID,Pref.PosCamposEtiCon10);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConAID,Pref.NomCamposEtiCon10);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiConBID,Pref.PosCamposEtiCon11);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConBID,Pref.NomCamposEtiCon11);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiConCID,Pref.PosCamposEtiCon12);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConCID,Pref.NomCamposEtiCon12);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiConDID,Pref.PosCamposEtiCon13);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConDID,Pref.NomCamposEtiCon13);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiConEID,Pref.PosCamposEtiCon14);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConEID,Pref.NomCamposEtiCon14);

			DlgPrefController->SetTriStateControlData(kA2PPrefCkBoxCreaEtiqSinID,Pref.CreaEtiquetaSinPublicidad);
			DlgPrefController->SetTriStateControlData(kA2PPrefBorrarEtiqSDespuesUpdateWidgetID,Pref.BorrarEtiquetaDespuesDeActualizar);
		
			if(Pref.CreaEtiquetaSinPublicidad==1)
			{
			
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefBoxTamMarcEtiqSinID,kTrue);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefComboBoxColTextEtiqSinID,kTrue);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefComboBoxColEtiqSinID,kTrue);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefBorrarEtiqSDespuesUpdateWidgetID,kTrue);
				
		
			}
			else
			{
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefBoxTamMarcEtiqSinID,kFalse);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefComboBoxColTextEtiqSinID,kFalse);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefComboBoxColEtiqSinID,kFalse);
				HabilDeshaWitgetsDependientes(DlgPrefView,kA2PPrefBorrarEtiqSDespuesUpdateWidgetID,kFalse);
		
			}
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSin1ID,Pref.PosCamposEtiSin1);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin1ID,Pref.NomCamposEtiSin1);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSin2ID,Pref.PosCamposEtiSin2);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin2ID,Pref.NomCamposEtiSin2);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSin3ID,Pref.PosCamposEtiSin3);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin3ID,Pref.NomCamposEtiSin3);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSin4ID,Pref.PosCamposEtiSin4);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin4ID,Pref.NomCamposEtiSin4);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSin5ID,Pref.PosCamposEtiSin5);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin5ID,Pref.NomCamposEtiSin5);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSin6ID,Pref.PosCamposEtiSin6);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin6ID,Pref.NomCamposEtiSin6);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSin7ID,Pref.PosCamposEtiSin7);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin7ID,Pref.NomCamposEtiSin7);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSin8ID,Pref.PosCamposEtiSin8);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin8ID,Pref.NomCamposEtiSin8);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSin9ID,Pref.PosCamposEtiSin9);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin9ID,Pref.NomCamposEtiSin9);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSinAID,Pref.PosCamposEtiSin10);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinAID,Pref.NomCamposEtiSin10);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSinBID,Pref.PosCamposEtiSin11);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinBID,Pref.NomCamposEtiSin11);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSinCID,Pref.PosCamposEtiSin12);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinCID,Pref.NomCamposEtiSin12);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSinDID,Pref.PosCamposEtiSin13);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinDID,Pref.NomCamposEtiSin13);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNumCamOrdEtiSinEID,Pref.PosCamposEtiSin14);
			DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinEID,Pref.NomCamposEtiSin14);
			
			retval=kTrue;
		}
		else
		{
			CAlert::ErrorAlert(kA2PPrefNoExistenPreferenciasStringKey);
			retval=kFalse;
		}
	}
	else
	{
		CAlert::ErrorAlert(kA2PPrefNoExistenPreferenciasStringKey);
		//CAlert::ErrorAlert("No se han creado preferencias XXXX");
		retval=kFalse;
	}
	}while(false);
	return(retval);
}


/******************Obtiene el texto de cada uno de los objetos de preferencia**********/
bool16 A2PPrefActionsOnDLGPreferenciasUtils::LlenarPreferenciasDeDialogoPref(IControlView *DlgPrefView, Preferencias &Pref)
{
	bool16 retval=kTrue;
	do
	{
		if(DlgPrefView==nil)
		{
			retval=kFalse;
			break;
		}
		
		InterfacePtr<IDialogController> DlgPrefController(DlgPrefView,IID_IDIALOGCONTROLLER);
		if(DlgPrefController==nil)
		{
			retval=kFalse;
			break;
		}
	
		/***************OBTENCION DE DATOS DE LAS CAJAS DE TEXTO***********************/
		/*GetTextControlData(WidgetID):
			Esta funcion es creada por InDesign SDK la cual devuelve 
			el texto(como PMString), de un objeto determinado (WidgetID).
			En este caso es almacenado en la estructura preferencias.
		*/

		Pref.AnchoPagina = DlgPrefController->GetTextValue(kA2PPrefBoxAnchoPagID);
		Pref.AltoPag = DlgPrefController->GetTextValue(kA2PPrefBoxAltoPagID);
		Pref.MargenExterior = DlgPrefController->GetTextValue(kA2PPrefBoxMargExtID);
		Pref.MargenInferior = DlgPrefController->GetTextValue(kA2PPrefBoxMargInfID);
		Pref.MargenInterior = DlgPrefController->GetTextValue(kA2PPrefBoxMargIntID);
		Pref.MargenSuperior = DlgPrefController->GetTextValue(kA2PPrefBoxMargSupID);
		Pref.Medianil = DlgPrefController->GetTextValue(kA2PPrefBoxMedianilID);
        
		PMString COLUMNAS=DlgPrefController->GetTextControlData(kA2PPrefBoxNumColID);
		//CAlert::InformationAlert("OBTENER: " +COLUMNAS );
		Pref.NumColumas = DlgPrefController->GetTextControlData(kA2PPrefBoxNumColID).GetAsNumber();//columnas
		
        PMString UNIDADES_MEDICION=DlgPrefController->GetTextControlData(kA2PPrefComboBoxUniMedWidgetID);
       // CAlert::InformationAlert("OBTENER: " +UNIDADES_MEDICION);
		Pref.UnidadesMedicion =   DlgPrefController->GetTextControlData(kA2PPrefComboBoxUniMedWidgetID);

		Pref.Nombrepreferencias = DlgPrefController->GetTextControlData(kA2PPrefComboBoxSelecPreferID);//??
		//CAlert::InformationAlert("NO LO SE " +Pref.Nombrepreferencias);
		 
		/*********************************************************************************/
	
		Pref.UtilizaPlantilleroPref = DlgPrefController->GetTriStateControlData(kA2PPrefUsarPlantilleroWidgetID);
		
		Pref.CapaEditorial=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaEditID);
		Pref.CapaEditorialVisible=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaEditVisID);
		Pref.CapaEditorialBlocked=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaEditBlqID);
	
		Pref.CapaImagen=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaImagID);
		Pref.CapaImagenVisible=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaImagVisID);
		Pref.CapaImagenBlocked=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaImagBlqID);
	
		Pref.CapaEtiquetaConPublicidad=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaEConID);
		Pref.CapaEtiquetaConPublicidadVisible=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaEConVisID);
		Pref.CapaEtiquetaConPublicidadBlocked=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaEConBlqID);

		Pref.CapaEtiquetaSinPublicidad=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaESinID);
		Pref.CapaEtiquetaSinPublicidadVisible=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaESinVisID);
		Pref.CapaEtiquetaSinPublicidadBlocked=DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCapaESinBlqID);
		/***********************************************************************************/
		Pref.ColorCajaImagenConPublicidad=DlgPrefController->GetTextControlData(kA2PPrefComboBoxColImagConID);
		Pref.ColorMargenCajaImagenConPublicidad=DlgPrefController->GetTextControlData(kA2PPrefComboBoxColMaImagConID);
		Pref.FitCajaImagenConPublicidad=DlgPrefController->GetTextControlData(kA2PPrefComboBoxAjusteID);

		Pref.ColorCajaImagenSinPublicidad=DlgPrefController->GetTextControlData(kA2PPrefComboBoxColImagSinID);
		Pref.ColorMargenCajaImagenSinPublicidad=DlgPrefController->GetTextControlData(kA2PPrefComboBoxColMaImagSinID);
	
		/**************************************************************************************/

		Pref.PosCampos1 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLectura1ID).GetAsNumber();
		Pref.NomCampos1 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLectura1ID);
		Pref.PosCampos2 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLectura2ID).GetAsNumber();
		Pref.NomCampos2 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLectura2ID);
		Pref.PosCampos3 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLectura3ID).GetAsNumber();
		Pref.NomCampos3 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLectura3ID);
		Pref.PosCampos4 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLectura4ID).GetAsNumber();
		Pref.NomCampos4 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLectura4ID);
		Pref.PosCampos5 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLectura5ID).GetAsNumber();
		Pref.NomCampos5 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLectura5ID);
		Pref.PosCampos6 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLectura6ID).GetAsNumber();
		Pref.NomCampos6 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLectura6ID);
		Pref.PosCampos7 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLectura7ID).GetAsNumber();
		Pref.NomCampos7 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLectura7ID);
		Pref.PosCampos8 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLectura8ID).GetAsNumber();
		Pref.NomCampos8 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLectura8ID);
		Pref.PosCampos9 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLectura9ID).GetAsNumber();
		Pref.NomCampos9 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLectura9ID);
		Pref.PosCampos10 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLecturaAID).GetAsNumber();
		Pref.NomCampos10 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLecturaAID);
		Pref.PosCampos11 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLecturaBID).GetAsNumber();
		Pref.NomCampos11 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLecturaBID);
		Pref.PosCampos12 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLecturaCID).GetAsNumber();
		Pref.NomCampos12 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLecturaCID);
		Pref.PosCampos13 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLecturaDID).GetAsNumber();
		Pref.NomCampos13 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLecturaDID);
		Pref.PosCampos14 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCampoLecturaEID).GetAsNumber();
		Pref.NomCampos14 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCampoLecturaEID);

		//CAlert::InformationAlert("CAMPOS A: "+ Pref.SepEnCampos); //AQUI NO HAY NADA
		Pref.SepEnCampos.Clear();
        //CAlert::InformationAlert("CAMPOS B: "+ Pref.SepEnCampos);//AQUI NO HAY NADA

		if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonTabWidgetID))	
			Pref.SepEnCampos.AppendNumber(9);
		else
			if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonComaWidgetID))
				Pref.SepEnCampos.AppendNumber(44);
			else
				if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonPyComaWidgetID))
					Pref.SepEnCampos.AppendNumber(59);
				else
					if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonDosPuntosWidgetID))
						Pref.SepEnCampos.AppendNumber(58);
					else
						if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonEspecialWidgetID))
						{
							PMString a;
							a=DlgPrefController->GetTextControlData(kA2PPrefBoxCarEspecialLecturaEID);
							UTF32TextChar temp=a.GetWChar(0);
							Pref.SepEnCampos.AppendNumber(temp.GetValue());
                             
							CAlert::InformationAlert(" SE OBTIENE SepEnCampos: "+Pref.SepEnCampos );

						}
		/***************************************************************************/
		//obtencion de preferencias de la Orden de Coordenadas
		Pref.TipoLecturaCoor.Clear();
		if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonTLBRWidgetID))
		{
			Pref.TipoLecturaCoor.AppendNumber(1);
			Pref.OrdendeLectura=DlgPrefController->GetTextControlData(kA2PPrefComboBoxTLBRWidgetID);
		}
		else
			if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonXYWidgetID))
			{
				Pref.TipoLecturaCoor.AppendNumber(2);
				Pref.OrdendeLectura=DlgPrefController->GetTextControlData(kA2PPrefComboBoxXYWidgetID);
			}
			else
				if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonTLBRSepWidgetID))
				{
					Pref.TipoLecturaCoor.AppendNumber(3);
				}

	

		Pref.SepEnCoordenadas.Clear();
		Pref.SepEnCoordenadas=DlgPrefController->GetTextControlData(kA2PPrefBoxCadUnidadesLecturaEID);
		Pref.SepEnCoordenadas.AppendW(94);
		//CAlert::InformationAlert("se obtiene COORDENADADS: "+Pref.SepEnCoordenadas);

		if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonTabOrdCWidgetID))	
			Pref.SepEnCoordenadas.AppendNumber(94);
		else
			if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonComaOrdCWidgetID))
				Pref.SepEnCoordenadas.AppendNumber(44);
			else
				if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonPyComaOrdCWidgetID))
					Pref.SepEnCoordenadas.AppendNumber(59);
				else
					if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonDosPuntosOrdCWidgetID))
						Pref.SepEnCoordenadas.AppendNumber(58);
					else
						if(DlgPrefController->GetTriStateControlData(kA2PPrefRadioButtonEspecialOrdCWidgetID))
						{
							PMString a;
							a=DlgPrefController->GetTextControlData(kA2PPrefBoxCarEspecialLecturaOrdCID);
							UTF32TextChar temp=a.GetWChar(0);
							Pref.SepEnCoordenadas.AppendNumber(temp.GetValue());
						}

	

		Pref.UnidadesEnTop = DlgPrefController->GetTextControlData(kA2PPrefComboBoxUniTopWidgetID);		
		Pref.UnidadesEnLeft = DlgPrefController->GetTextControlData(kA2PPrefComboBoxUniLeftWidgetID);		
		Pref.UnidadesEnButtom = DlgPrefController->GetTextControlData(kA2PPrefComboBoxUniBottomWidgetID);		
		Pref.UnidadesEnRight = DlgPrefController->GetTextControlData(kA2PPrefComboBoxUniRightWidgetID);		
			

		/***************************************************************************/
		//obtencion de preferencias de la pestaÒa Etiquetas
		//Pref.CapaEtiquetaConPublicidadBlocked = GetTriStateControlData(kA2PPrefCkBoxCreaEtiqConID);
		Pref.ColorFondoEtiquetaConPublicidad = DlgPrefController->GetTextControlData(kA2PPrefComboBoxColEtiqConID);
		//CAlert::InformationAlert("NO LO SE" +Pref.ColorFondoEtiquetaConPublicidad);
		Pref.ColorTextEtiquetaConPublicidad = DlgPrefController->GetTextControlData(kA2PPrefComboBoxColTextEtiqConID);
		Pref.TamMargenEtiquetaConPublicidad = DlgPrefController->GetTextControlData(kA2PPrefBoxTamMarcEtiqConID);

		//Pref.CapaEtiquetaSinPublicidadBlocked = GetTriStateControlData(kA2PPrefCkBoxCreaEtiqSinID);
		Pref.ColorFondoEtiquetaSinPublicidad = DlgPrefController->GetTextControlData(kA2PPrefComboBoxColEtiqSinID);
		Pref.ColorTextEtiquetaSinPublicidad = DlgPrefController->GetTextControlData(kA2PPrefComboBoxColTextEtiqSinID);
		Pref.TamMargenEtiquetaSinPublicidad = DlgPrefController->GetTextControlData(kA2PPrefBoxTamMarcEtiqSinID);


		Pref.AbrirDialogoIssue = DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxMostrarDlgIssueWidgetID);	
	
		//Changed to A2P v3.0
		//Pref.FuenteDeIssue = GetTextControlData(kA2PPrefComboBoxFontWidgetID);
		//Pref.TamFuenteIssue = GetTextControlData(kA2PPrefBoxSizeFontWidgetID);
		Pref.NombreEstiloIssue = DlgPrefController->GetTextControlData(kA2PPrefParagraphStyleNameToFolioWidgetID);
		Pref.NombreEditorial  = DlgPrefController->GetTextControlData(kA2PPrefNameEditionTextEditWidgetID);
		Pref.DesplazamientoHorIssue = DlgPrefController->GetTextValue(kA2PPrefBoxDespHorWidgetID);
		Pref.DesplazamientoVerIssue = DlgPrefController->GetTextValue(kA2PPrefBoxDespVerWidgetID);
		/*****************************************************************************/
		//obtencion de preferencias de la pestaÒa Orden en etiquetas
		Pref.CreaEtiquetaConPublicidad = DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCreaEtiqConID);	
		Pref.PosCamposEtiCon1 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiCon1ID);
		Pref.NomCamposEtiCon1 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiCon1ID);
		Pref.PosCamposEtiCon2 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiCon2ID);
		Pref.NomCamposEtiCon2 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiCon2ID);
		Pref.PosCamposEtiCon3 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiCon3ID);
		Pref.NomCamposEtiCon3 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiCon3ID);
		Pref.PosCamposEtiCon4 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiCon4ID);
		Pref.NomCamposEtiCon4 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiCon4ID);
		Pref.PosCamposEtiCon5 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiCon5ID);
		Pref.NomCamposEtiCon5 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiCon5ID);
		Pref.PosCamposEtiCon6 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiCon6ID);
		Pref.NomCamposEtiCon6 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiCon6ID);
		Pref.PosCamposEtiCon7 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiCon7ID);
		Pref.NomCamposEtiCon7 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiCon7ID);
		Pref.PosCamposEtiCon8 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiCon8ID);
		Pref.NomCamposEtiCon8 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiCon8ID);
		Pref.PosCamposEtiCon9 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiCon9ID);
		Pref.NomCamposEtiCon9 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiCon9ID);
		Pref.PosCamposEtiCon10 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiConAID);
		Pref.NomCamposEtiCon10 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiConAID);
		Pref.PosCamposEtiCon11 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiConBID);
		Pref.NomCamposEtiCon11 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiConBID);
		Pref.PosCamposEtiCon12 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiConCID);
		Pref.NomCamposEtiCon12 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiConCID);
		Pref.PosCamposEtiCon13 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiConDID);
		Pref.NomCamposEtiCon13 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiConDID);
		Pref.PosCamposEtiCon14 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiConEID);
		Pref.NomCamposEtiCon14 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiConEID);

		Pref.CreaEtiquetaSinPublicidad = DlgPrefController->GetTriStateControlData(kA2PPrefCkBoxCreaEtiqSinID);
		Pref.BorrarEtiquetaDespuesDeActualizar = DlgPrefController->GetTriStateControlData(kA2PPrefBorrarEtiqSDespuesUpdateWidgetID);
		Pref.PosCamposEtiSin1 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSin1ID);
		Pref.NomCamposEtiSin1 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSin1ID);
		Pref.PosCamposEtiSin2 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSin2ID);
		Pref.NomCamposEtiSin2 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSin2ID);
		Pref.PosCamposEtiSin3 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSin3ID);
		Pref.NomCamposEtiSin3 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSin3ID);
		Pref.PosCamposEtiSin4 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSin4ID);
		Pref.NomCamposEtiSin4 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSin4ID);
		Pref.PosCamposEtiSin5 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSin5ID);
		Pref.NomCamposEtiSin5 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSin5ID);
		Pref.PosCamposEtiSin6 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSin6ID);
		Pref.NomCamposEtiSin6 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSin6ID);
		Pref.PosCamposEtiSin7 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSin7ID);
		Pref.NomCamposEtiSin7 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSin7ID);
		Pref.PosCamposEtiSin8 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSin8ID);
		Pref.NomCamposEtiSin8 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSin8ID);
		Pref.PosCamposEtiSin9 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSin9ID);
		Pref.NomCamposEtiSin9 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSin9ID);
		Pref.PosCamposEtiSin10 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSinAID);
		Pref.NomCamposEtiSin10 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSinAID);
		Pref.PosCamposEtiSin11 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSinBID);
		Pref.NomCamposEtiSin11 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSinBID);
		Pref.PosCamposEtiSin12 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSinCID);
		Pref.NomCamposEtiSin12 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSinCID);
		Pref.PosCamposEtiSin13 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSinDID);
		Pref.NomCamposEtiSin13 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSinDID);
		Pref.PosCamposEtiSin14 = DlgPrefController->GetTextControlData(kA2PPrefBoxNumCamOrdEtiSinEID);
		Pref.NomCamposEtiSin14 = DlgPrefController->GetTextControlData(kA2PPrefBoxNomCamOrdEtiSinEID);
	}while(false);
	return(retval);
}

bool16 A2PPrefActionsOnDLGPreferenciasUtils::CopiaCamposAImportarParaOrdenCamposEtiq(IControlView *DlgPrefView, Preferencias &Pref)
{
	bool16 retval=kTrue;
	do
	{
		if(DlgPrefView==nil)
		{
			retval=kFalse;
			break;
		}
		
		InterfacePtr<IDialogController> DlgPrefController(DlgPrefView,IID_IDIALOGCONTROLLER);
		if(DlgPrefController==nil)
		{
			retval=kFalse;
			break;
		}
	
		    DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon1ID,Pref.NomCampos1);
		  //DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon1ID,Pref.NomCampos1);//OBSERVACION
				/////////////////////////////////////////////////////////
																	
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon2ID,Pref.NomCampos2);
				/////////////////////////////////////////////////////////
														
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon3ID,Pref.NomCampos3);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon4ID,Pref.NomCampos4);
				/////////////////////////////////////////////////////////
												
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon5ID,Pref.NomCampos5);
				/////////////////////////////////////////////////////////
																	
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon6ID,Pref.NomCampos6);
				////////////////////////////////////////////////////////					
												
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon7ID,Pref.NomCampos7);
				/////////////////////////////////////////////////////////
																		
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon8ID,Pref.NomCampos8);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiCon9ID,Pref.NomCampos9);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConAID,Pref.NomCampos10);
				/////////////////////////////////////////////////////////
								
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConBID,Pref.NomCampos11);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConCID,Pref.NomCampos12);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConDID,Pref.NomCampos13);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiConEID,Pref.NomCampos14);
				/////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin1ID,Pref.NomCampos1);
				/////////////////////////////////////////////////////////
				
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin2ID,Pref.NomCampos2);
				/////////////////////////////////////////////////////////
				
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin3ID,Pref.NomCampos3);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin4ID,Pref.NomCampos4);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin5ID,Pref.NomCampos5);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin6ID,Pref.NomCampos6);
				/////////////////////////////////////////////////////////					
												
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin7ID,Pref.NomCampos7);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin8ID,Pref.NomCampos8);
				/////////////////////////////////////////////////////////
									
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSin9ID,Pref.NomCampos9);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinAID,Pref.NomCampos10);
				/////////////////////////////////////////////////////////
								
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinBID,Pref.NomCampos11);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinCID,Pref.NomCampos12);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinDID,Pref.NomCampos13);
				/////////////////////////////////////////////////////////
										
				DlgPrefController->SetTextControlData(kA2PPrefBoxNomCamOrdEtiSinEID,Pref.NomCampos14);
	}while(false);
	return(retval);
}

WidgetID A2PPrefActionsOnDLGPreferenciasUtils::ValidaNumeracionDeCampos(IControlView *DlgPrefView, Preferencias &Pref)
{
	WidgetID retval=kDefaultWidgetId;
	do
	{
		if(DlgPrefView==nil)
		{
			retval=kFalse;
			break;
		}
		
		InterfacePtr<IDialogController> DlgPrefController(DlgPrefView,IID_IDIALOGCONTROLLER);
		if(DlgPrefController==nil)
		{
			retval=kFalse;
			break;
		}
		
		InterfacePtr<ISelectableDialogSwitcher>	Selectswitcher(DlgPrefView, IID_ISELECTABLEDIALOGSWITCHER);
		if(Selectswitcher==nil)
		{
			CAlert::InformationAlert("No se encontro el Selectswitcher");
			break;
		}


		if(ComparaNumero(Pref.PosCampos1,Pref,1)==kTrue)//si regresa verdadero indica que este campo se encuentra repetido
		{ 
			if(Pref.PosCampos1!=0)//si el numero repetido no es 0
			{
				//mensaje de alerta
				
				//busqueda de el widget y posicion del texfocus
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLectura1ID);
				retval=kA2PPrefBoxNumCampoLectura1ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}

		if(ComparaNumero(Pref.PosCampos2,Pref,2)==kTrue)
		{ 
			if(Pref.PosCampos2!=0)
			{				
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLectura2ID);
				retval=kA2PPrefBoxNumCampoLectura2ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos3,Pref,3)==kTrue)
		{ 
			if(Pref.PosCampos3!=0)
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLectura3ID);
				retval=kA2PPrefBoxNumCampoLectura3ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos4,Pref,4)==kTrue)
		{ 
			if(Pref.PosCampos4!=0)
			{				
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLectura4ID);
				retval=kA2PPrefBoxNumCampoLectura4ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos5,Pref,5)==kTrue)
		{ 
			if(Pref.PosCampos5!=0)
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLectura5ID);
				retval=kA2PPrefBoxNumCampoLectura5ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos6,Pref,6)==kTrue)
		{ 
			if(Pref.PosCampos6!=0)
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLectura6ID);
				retval=kA2PPrefBoxNumCampoLectura6ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos7,Pref,7)==kTrue)
		{ 
			if(Pref.PosCampos7!=0)
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLectura7ID);
				retval=kA2PPrefBoxNumCampoLectura7ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos8,Pref,8)==kTrue)
		{ 
			if(Pref.PosCampos8!=0)
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLectura8ID);
				retval=kA2PPrefBoxNumCampoLectura8ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos9,Pref,9)==kTrue)
		{ 
			if(Pref.PosCampos9!=0)
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLectura9ID);
				retval=kA2PPrefBoxNumCampoLectura9ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos10,Pref,10)==kTrue)
		{ 
			if(Pref.PosCampos10!=0)
			{
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				retval=kA2PPrefImportPanelWidgetID;
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLecturaAID);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos11,Pref,11)==kTrue)
		{ 
			if(Pref.PosCampos11!=0)
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLecturaBID);
				retval=kA2PPrefBoxNumCampoLecturaBID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos12,Pref,12)==kTrue)
		{ 
			if(Pref.PosCampos12!=0)
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLecturaCID);
				retval=kA2PPrefBoxNumCampoLecturaCID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos13,Pref,13)==kTrue)
		{ 
			if(Pref.PosCampos13!=0)
			{
				
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLecturaEID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLecturaDID);
				retval=kA2PPrefBoxNumCampoLecturaDID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumero(Pref.PosCampos14,Pref,14)==kTrue)
		{ 
			if(Pref.PosCampos14!=0)
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefImportPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCampoLecturaEID);
				retval=kA2PPrefBoxNumCampoLecturaEID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		//////////////////////////////////////////////////////////////////////////////////////////

		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon1,Pref,1)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon1!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiCon1ID);
				retval=kA2PPrefBoxNumCamOrdEtiCon1ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon2,Pref,2)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon2!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiCon2ID);
				retval=kA2PPrefBoxNumCamOrdEtiCon2ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}

		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon3,Pref,3)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon3!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiCon3ID);
				retval=kA2PPrefBoxNumCamOrdEtiCon3ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon4,Pref,4)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon4!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiCon4ID);
				retval=kA2PPrefBoxNumCamOrdEtiCon4ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon5,Pref,5)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon5!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiCon5ID);
				retval=kA2PPrefBoxNumCamOrdEtiCon5ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon6,Pref,6)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon6!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiCon6ID);
				retval=kA2PPrefBoxNumCamOrdEtiCon6ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon7,Pref,7)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon7!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiCon7ID);
				retval=kA2PPrefBoxNumCamOrdEtiCon7ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon8,Pref,8)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon8!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiCon8ID);
				retval=kA2PPrefBoxNumCamOrdEtiCon8ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon9,Pref,9)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon9!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiCon9ID);
				retval=kA2PPrefBoxNumCamOrdEtiCon9ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon10,Pref,10)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon10!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiConAID);
				retval=kA2PPrefBoxNumCamOrdEtiConAID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon11,Pref,11)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon11!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiConBID);
				retval=kA2PPrefBoxNumCamOrdEtiConBID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon12,Pref,12)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon12!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiConCID);
				retval=kA2PPrefBoxNumCamOrdEtiConCID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon13,Pref,13)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon13!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiConDID);
				retval=kA2PPrefBoxNumCamOrdEtiConDID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiCon(Pref.PosCamposEtiCon14,Pref,14)==kTrue)
		{ 
			if(Pref.PosCamposEtiCon14!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiConEID);
				retval=kA2PPrefBoxNumCamOrdEtiConEID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		////////////////////////////////////////////////////////////////////////////////////////
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin1,Pref,1)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin1!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSin1ID);
				retval=kA2PPrefBoxNumCamOrdEtiSin1ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin2,Pref,2)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin2!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSin2ID);
				retval=kA2PPrefBoxNumCamOrdEtiSin2ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin3,Pref,3)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin3!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSin3ID);
				retval=kA2PPrefBoxNumCamOrdEtiSin3ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin4,Pref,4)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin4!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSin4ID);
				retval=kA2PPrefBoxNumCamOrdEtiSin4ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin5,Pref,5)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin5!="0")
			{
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSin5ID);
				retval=kA2PPrefBoxNumCamOrdEtiSin5ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
				
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin6,Pref,6)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin6!="0")
			{
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSin6ID);
				retval=kA2PPrefBoxNumCamOrdEtiSin6ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin7,Pref,7)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin7!="0")
			{
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSin7ID);
				retval=kA2PPrefBoxNumCamOrdEtiSin7ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
				
			}
			break;
		}
		
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin8,Pref,8)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin8!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSin8ID);
				retval=kA2PPrefBoxNumCamOrdEtiSin8ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin9,Pref,9)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin9!="0")
			{
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSin9ID);
				retval=kA2PPrefBoxNumCamOrdEtiSin9ID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
				
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin10,Pref,10)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin10!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSinAID);
				retval=kA2PPrefBoxNumCamOrdEtiSinAID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin11,Pref,11)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin11!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSinBID);
				retval=kA2PPrefBoxNumCamOrdEtiSinBID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin12,Pref,12)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin12!="0")
			{
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSinCID);
				retval=kA2PPrefBoxNumCamOrdEtiSinCID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin13,Pref,13)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin13!="0")
			{
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				retval=kA2PPrefOrdenPanelWidgetID;
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSinDID);
			}
			break;
		}
		if(ComparaNumeroEnEtiSin(Pref.PosCamposEtiSin14,Pref,14)==kTrue)
		{ 
			if(Pref.PosCamposEtiSin14!="0")
			{
				
				Selectswitcher->SwitchDialogPanelByID(kA2PPrefOrdenPanelWidgetID);
				DlgPrefController->SelectDialogWidget(kA2PPrefBoxNumCamOrdEtiSinEID);
				retval=kA2PPrefBoxNumCamOrdEtiSinEID;
				CAlert::WarningAlert(kA2PPrefMensajeSerepitecampoStringKey);

			}
			break;
		}
	
	}while(false);
	
	return(retval);
}



bool16 A2PPrefActionsOnDLGPreferenciasUtils::ComparaNumero(int32 CadenaAChecar,Preferencias &Pref,int32 numero)
{
	bool16 error;
	error=kFalse;

	//Compara la cadena(el numero de campo) con los demas campos
	//siempre y cuando que el numero de campo no se compare con el mismo
	if(CadenaAChecar==Pref.PosCampos1 && numero!=1)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos2 && numero!=2)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos3 && numero!=3)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos4 && numero!=4)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos5 && numero!=5)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos6 && numero!=6)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos7 && numero!=7)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos8 && numero!=8)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos9 && numero!=9)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos10 && numero!=10)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos11 && numero!=11)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos12 && numero!=12)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos13 && numero!=13)
	{
		error=kTrue;
	}
	if(CadenaAChecar==Pref.PosCampos14 && numero!=14)
	{
		error=kTrue;
	}
return(error);
}


bool16 A2PPrefActionsOnDLGPreferenciasUtils::ComparaNumeroEnEtiCon(PMString CadenaAChecar,Preferencias &Pref,int32 numero)
{
	bool16 error;
	error=kFalse;
	//Compara la cadena(el numero de campo) con los demas campos
	//siempre y cuando que el numero de campo no se compare con el mismo
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon1)==kTrue && numero!=1)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon2)==kTrue && numero!=2)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon3)==kTrue && numero!=3)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon4)==kTrue && numero!=4)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon5)==kTrue && numero!=5)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon6)==kTrue && numero!=6)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon7)==kTrue && numero!=7)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon8)==kTrue && numero!=8)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon9)==kTrue && numero!=9)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon10)==kTrue && numero!=10)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon11)==kTrue && numero!=11)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon12)==kTrue && numero!=12)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon13)==kTrue && numero!=13)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiCon14)==kTrue && numero!=14)
	{
		error=kTrue;
	}
	return(error);
}

bool16 A2PPrefActionsOnDLGPreferenciasUtils::ComparaNumeroEnEtiSin(PMString CadenaAChecar,Preferencias &Pref,int32 numero)
{
	bool16 error;
	error=kFalse;
	//Compara la cadena(el numero de campo) con los demas campos
	//siempre y cuando que el numero de campo no se compare con el mismo
	
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin1)==kTrue && numero!=1)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin2)==kTrue && numero!=2)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin3)==kTrue && numero!=3)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin4)==kTrue && numero!=4)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin5)==kTrue && numero!=5)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin6)==kTrue && numero!=6)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin7)==kTrue && numero!=7)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin8)==kTrue && numero!=8)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin9)==kTrue && numero!=9)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin10)==kTrue && numero!=10)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin11)==kTrue && numero!=11)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin12)==kTrue && numero!=12)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin13)==kTrue && numero!=13)
	{
		error=kTrue;
	}
	if(CadenaAChecar.IsEqual(Pref.PosCamposEtiSin14)==kTrue && numero!=14)
	{
		error=kTrue;
	}
return(error);
}

//CHECAR METODO
//int32 A2PPrefActionsOnDLGPreferenciasUtils::llenarComboPrefDeDlgPref(IControlView *DlgPrefView, Preferencias &Pref)
int A2PPrefActionsOnDLGPreferenciasUtils::llenarComboPrefDeDlgPref(IControlView *DlgPrefView, Preferencias &Pref)
{  	PMString fFilter("\\*.*");

	//int posicion;//posicion en el combo del archivo de preferencias actual
   int32 posicion=0;
	do
	{ if(DlgPrefView==nil)
	  {CAlert::InformationAlert("DlgPrefView==nil en llenar");posicion=-1;break;}

       InterfacePtr<IDialogController> DlgPrefController(DlgPrefView,IID_IDIALOGCONTROLLER);//ORIGINAL
	 if(DlgPrefController==nil)
		{posicion=-1;break;	}
		// Get a pointer to the droplist for this dialog and clear it - it may have old values in it
		InterfacePtr<IStringListControlData> dropListData(DlgPrefController->QueryListControlDataInterface(kA2PPrefComboBoxSelecPreferID));
		if (dropListData == nil)
		{
			CAlert::InformationAlert("No pudo Obtener IStringListControlData*");
			posicion=-1;
			break;
		}
		///borrado de la lista al inicializar el combo

		dropListData->Clear(kFalse, kFalse);//lIMPIA LA LISTA ACTUAL  OBSERVACION4 ESTE DEBE DE IR
		//CAlert::InformationAlert("OBSERVACION4");

	/*	InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
			(
				kA2PPrefRegEditUtilitiesBoss,	// Object boss/class
				IRegEditUtilities::kDefaultIID
			)));
		if(ConsultaRegEdit==nil)
		{
			break;
		}*/

		PMString rootPath=FileTreeUtils::CrearFolderPreferencias();
		//checar lo de abajo
//ConsultaRegEdit->QueryKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), TEXT("SystemRoot"));
		if(rootPath.IsEmpty() == kTrue)
		{CAlert::InformationAlert("cadena vacia");break;}
		
		SDKFileHelper rootFileHelper(rootPath);
		IDFile rootSysFile = rootFileHelper.GetIDFile();
		PlatformFileSystemIterator iter;
		
		if(!iter.IsDirectory(rootSysFile))
	  	{	break;}

		
	/*	#ifdef WINDOWS
			// Windows dir iteration a little diff to Mac
			rootSysFile.Append(fFilter);
		#endif
    */

		iter.SetStartingPath(rootSysFile);
		
		IDFile sysFile;
		bool16 hasNext= iter.FindFirstFile(sysFile,fFilter);
		while(hasNext)
		{
			SDKFileHelper fileHelper(sysFile);
			PMString truncP = FileTreeUtils::TruncatePath(fileHelper.GetPath());
			if(FileTreeUtils::validPath(truncP) && truncP.Contains(".pfa") )
			{
				truncP=FileTreeUtils::TruncateExtencion(truncP);
				dropListData->AddString(truncP, IStringListControlData::kEnd, kFalse, kFalse);
				//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
			}
			hasNext= iter.FindNextFile(sysFile);
		}
       // CAlert::InformationAlert("PREFERENCIA  MARIBEL1 == "+ Pref.Nombrepreferencias);
		//Pref.Nombrepreferencias.Remove(0,1);//remuevo el espacio en blanco del principio del nombre de la preferencia 
		                                      //NO REMOVER LA PREFERENCIA YA VIENE BIEN
		posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
		if(posicion!=-1)
			dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
		posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
		if(posicion!=-1)
			dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion	
		//A—ADI
		//PMString Nombrepreferencias;
		//Preferencias Pref;
        //Pref.Nombrepreferencias = DlgPrefController->GetTextControlData(kA2PPrefComboBoxSelecPreferID);
		posicion=dropListData->GetIndex(Pref.Nombrepreferencias);//busco la ultima preferencia con que se trabajo
       //posicion=dropListData->GetIndex("Fer1");
	  //CAlert::InformationAlert("PREFERENCIA MARIBEL2== "+Pref.Nombrepreferencias);
	  DlgPrefController->SetInitialized(kTrue);

/*	WIN32_FIND_DATA FindFileData;//Busqueda del archivo 
	WIN32_FIND_DATA FindFileDataAnterior;//Busqueda del archivo anterior
	HANDLE hFind;
	char *Arc1;	//Path o nombre del archivo
	char *Arc2;
	PMString Arc;
	

	this->SetInitialized(kFalse);

	InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
			(
				kA2PPrefRegEditUtilitiesBoss,	// Object boss/class
				IRegEditUtilities::kDefaultIID
			)));
	
	PMString Default_Prefer_Path=ConsultaRegEdit->QueryKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), TEXT("SystemRoot"));
	Default_Prefer_Path.Append(":asalretni:Software:Ads2Page 2.0 CS:Preferencias:*.pfa");


	if (fmpDropList)
	{	
		delete fmpDropList;
		fmpDropList = nil;
	}

	fmpBasedOnUID = kInvalidUID;
	fmpName.Clear();
	fmpPrefix.Clear();
	fmpNumPages = 1;
	fmpValidated = kFalse;

	do				// false loop
	{
		// Get a pointer to the droplist for this dialog and clear it - it may have old values in it
		InterfacePtr<IStringListControlData> dropListData(this->QueryListControlDataInterface(kA2PPrefComboBoxSelecPreferID));
		if (dropListData == nil)
		{
			ASSERT_FAIL("No pudo Obtener IStringListControlData*");
			break;
		}
		///borrado de la lista al inicializar el combo

		dropListData->Clear(kFalse, kFalse);//lIMPIA LA LISTA ACTUAL
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//////COMENTARIO ES MUY EXTRA—O QUE EN ESTE ARCHIVO .CPP SI ME ACEPTE LA LA LIBRERIA WINDOWS.H//////////////////////////
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  Arc2=(char *)FindFileDataAnterior.cFileName;

		  hFind = FindFirstFile(Default_Prefer_Path.GrabTString(), &FindFileData);
  			Arc1=(char *)FindFileData.cFileName;//
			Arc1=strtok(Arc1,".");
			while(strcmp(Arc1,Arc2)!=0) //pregunta si la preferencia anterior se llama del mismo modo que la actual
			{		
				 FindFileDataAnterior=FindFileData;//copia preferencia anterior encontrada 
				 Arc2=(char *)FindFileDataAnterior.cFileName;//Copia nombre del archivo de la preferencia encontrada a Arc2
				 Arc2=strtok(Arc2,".");
				 Arc=Arc2;//COPIA EL NOMBRE DE LA PREFERENCIA ANTERIOR ENCONTRADA A LA VARIABLE Arc
				 dropListData->AddString(Arc, IStringListControlData::kEnd, kFalse, kFalse);//ADISIONA A LISTA EL NUEVO ARCHIVO DE PREFERECIA ENCONTRADO
				 FindNextFile(hFind,&FindFileData);//BUSQUEDA DEL NUEVO ARCHIVO
				 Arc1=(char *)FindFileData.cFileName;//COPIA A CAD 2 EL NOMBRE DEL ARCHIVO ENCONTRADO
				 Arc1=strtok(Arc1,".");
			}
			FindClose(hFind);//FINALIZA BUSQUEDA

			Pref.Nombrepreferencias.Remove(0,1);//remuevo el espacio en blanco del principio del nombre de la preferencia
			posicion=dropListData->GetIndex("Default");//busco la pocision del archivo default en la lista
			if(posicion!=-1)
				dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion
			posicion=dropListData->GetIndex("Predeterminado");//busco la pocision del archivo default en la lista
			if(posicion!=-1)
				dropListData->RemoveString(posicion);///quito el archivo defaul aputado por posicion		
			posicion=dropListData->GetIndex(Pref.Nombrepreferencias);//busco la ultima preferencia con que se trabajo
		this->SetInitialized(kTrue);
	} while (0);	// false loop*/
	}while(false);
return(posicion);////retorna la posicion del laultima preferencia con que se trabajo
}


bool16 A2PPrefActionsOnDLGPreferenciasUtils::HabilDeshaWitgetsDependientes(IControlView *DlgPrefView,const WidgetID& widgetID,bool16 ChecBoxSeleccionado)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IWidgetParent>		myParent(DlgPrefView, UseDefaultIID());				
		if(myParent==nil)
		{
			CAlert::InformationAlert("No se3 encontro el parent");
			break;
		}
		InterfacePtr<IPanelControlData>	iPanelControlData((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(iPanelControlData==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;
		}

		IControlView * iControlView  = iPanelControlData->FindWidget(widgetID);
		ASSERT(iControlView);
		if(iControlView==nil) 
		{
			break;
		}
		//iControlView->Enable(ChecBoxSeleccionado);
		if(ChecBoxSeleccionado)
			iControlView->Enable(kTrue);
		else
			iControlView->Disable(kTrue);
		
		retval=kTrue;
	}while(false);
	return retval;
}


bool16 A2PPrefActionsOnDLGPreferenciasUtils::ColocaEnComboColor(IControlView *DlgPrefView,const WidgetID& widgetid,PMString Color)
{
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IWidgetParent>		myParent(DlgPrefView, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			CAlert::InformationAlert("No se encontro el panel");
			break;
		}
		InterfacePtr<IControlView>		CVComboBoxColImagCon(panel->FindWidget(widgetid), UseDefaultIID() );
		if(CVComboBoxColImagCon==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
								
		InterfacePtr<IDropDownListController>	IDDLCrComBoxColImagCon( CVComboBoxColImagCon, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLCrComBoxColImagCon==nil)
		{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}

		
		do
		{
			if(Color.Contains("Negro")||Color.Contains("Black"))
			{
				IDDLCrComBoxColImagCon->Select(0);
				break;
			}
			if(Color.Contains("Blanco")||Color.Contains("White"))
			{	
				IDDLCrComBoxColImagCon->Select(1);
				break;
			}
			if(Color.Contains("Rojo")||Color.Contains("Red"))
			{
				IDDLCrComBoxColImagCon->Select(2);
				break;
			}
			if(Color.Contains("Azul")||Color.Contains("Blue"))
			{	
				IDDLCrComBoxColImagCon->Select(3);
				break;
			}
			if(Color.Contains("Amarillo")||Color.Contains("Yellow"))
			{	
				IDDLCrComBoxColImagCon->Select(4);
				break;
			}
			if(Color.Contains("Ninguno")||Color.Contains("None"))
			{		
				IDDLCrComBoxColImagCon->Select(5);
				break;
			}
			if(Color.Contains("Registro")||Color.Contains("Register"))
			{		
				IDDLCrComBoxColImagCon->Select(6);
				break;
			}
		}while(false);	
		retval=kTrue;
	}while(false);
	return retval;
}

void A2PPrefActionsOnDLGPreferenciasUtils::ColocaEnComboxy(IControlView *DlgPrefView,const WidgetID& widgetid,PMString Secuencia)
{
	do
	{
		InterfacePtr<IWidgetParent>		myParent(DlgPrefView, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			break;
		}
		InterfacePtr<IControlView>		CVComboBoxColImagCon(panel->FindWidget(widgetid), UseDefaultIID() );
		if(CVComboBoxColImagCon==nil)
		{
			break;
		}
								
		InterfacePtr<IDropDownListController>	IDDLCrComBoxColImagCon( CVComboBoxColImagCon, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLCrComBoxColImagCon==nil)
		{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}

		
		do
		{
			if(Secuencia.Contains("(X1,X2) (Y1,Y2)"))
			{
				IDDLCrComBoxColImagCon->Select(0);
				break;
			}
			if(Secuencia.Contains("(X1,X2) (Y2,Y1)"))
			{	
				IDDLCrComBoxColImagCon->Select(1);
				break;
			}
			if(Secuencia.Contains("(X1,Y1) (X2,Y2)"))
			{
				IDDLCrComBoxColImagCon->Select(2);
				break;
			}
			if(Secuencia.Contains("(X1,Y1) (Y2,X2)"))
			{	
				IDDLCrComBoxColImagCon->Select(3);
				break;
			}
			if(Secuencia.Contains("(X1,Y2) (X2,Y1)"))
			{	
				IDDLCrComBoxColImagCon->Select(4);
				break;
			}
			if(Secuencia.Contains("(X1,Y2) (Y1,X2)"))
			{		
				IDDLCrComBoxColImagCon->Select(5);
				break;
			}
			if(Secuencia.Contains("(X2,X1) (Y1,Y2)"))
			{		
				IDDLCrComBoxColImagCon->Select(6);
				break;
			}
			if(Secuencia.Contains("(X2,X1) (Y2,Y1)"))
			{		
				IDDLCrComBoxColImagCon->Select(7);
				break;
			}
			if(Secuencia.Contains("(X2,Y1) (X1,Y2)"))
			{		
				IDDLCrComBoxColImagCon->Select(8);
				break;
			}
			if(Secuencia.Contains("(X2,Y1) (Y2,X1)"))
			{		
				IDDLCrComBoxColImagCon->Select(9);
				break;
			}
			if(Secuencia.Contains("(X2,Y2) (X1,Y1)"))
			{		
				IDDLCrComBoxColImagCon->Select(10);
				break;
			}
			if(Secuencia.Contains("(X2,Y2) (Y1,X1)"))
			{		
				IDDLCrComBoxColImagCon->Select(11);
				break;
			}
			if(Secuencia.Contains("(Y1,X1) (X2,Y2)"))
			{		
				IDDLCrComBoxColImagCon->Select(12);
				break;
			}
			if(Secuencia.Contains("(Y1,X1) (Y2,X2)"))
			{		
				IDDLCrComBoxColImagCon->Select(13);
				break;
			}
			if(Secuencia.Contains("(Y1,Y2) (X1,X2)"))
			{		
				IDDLCrComBoxColImagCon->Select(14);
				break;
			}
			if(Secuencia.Contains("(Y1,Y2) (X2,X1)"))
			{		
				IDDLCrComBoxColImagCon->Select(15);
				break;
			}
			if(Secuencia.Contains("(Y1,X2) (Y2,X1)"))
			{		
				IDDLCrComBoxColImagCon->Select(16);
				break;
			}
			if(Secuencia.Contains("(Y1,X2) (X1,Y2)"))
			{		
				IDDLCrComBoxColImagCon->Select(17);
				break;
			}
			if(Secuencia.Contains("(Y2,X1) (Y1,X2)"))
			{		
				IDDLCrComBoxColImagCon->Select(18);
				break;
			}
			if(Secuencia.Contains("(Y2,X1) (X2,Y1)"))
			{		
				IDDLCrComBoxColImagCon->Select(19);
				break;
			}
			if(Secuencia.Contains("(Y2,Y1) (X1,X2)"))
			{		
				IDDLCrComBoxColImagCon->Select(20);
				break;
			}
			if(Secuencia.Contains("(Y2,Y1) (X2,X1)"))
			{		
				IDDLCrComBoxColImagCon->Select(21);
				break;
			}
			if(Secuencia.Contains("(Y2,X2) (Y1,X1)"))
			{		
				IDDLCrComBoxColImagCon->Select(22);
				break;
			}
			if(Secuencia.Contains("(Y2,X2) (X1,Y1)"))
			{		
				IDDLCrComBoxColImagCon->Select(23);
				break;
			}
		}while(false);	
	}while(false);
}

void A2PPrefActionsOnDLGPreferenciasUtils::ColocaEnComboTLBR(IControlView *DlgPrefView, const WidgetID& widgetid,PMString Secuencia)
{
		do
	{
		InterfacePtr<IWidgetParent>		myParent(DlgPrefView, UseDefaultIID());				
		if(myParent==nil)
		{
			break;
		}
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			break;
		}
		InterfacePtr<IControlView>		CVComboBoxColImagCon(panel->FindWidget(widgetid), UseDefaultIID() );
		if(CVComboBoxColImagCon==nil)
		{
			break;
		}
								
		InterfacePtr<IDropDownListController>	IDDLCrComBoxColImagCon( CVComboBoxColImagCon, IID_IDROPDOWNLISTCONTROLLER);
		if(IDDLCrComBoxColImagCon==nil)
		{
			break;
		}

		do
		{
			if(Secuencia.Contains("T,L,B,R"))
			{
				IDDLCrComBoxColImagCon->Select(0);
				break;
			}
			if(Secuencia.Contains("T,L,R,B"))
			{	
				IDDLCrComBoxColImagCon->Select(1);
				break;
			}
			if(Secuencia.Contains("T,R,L,B"))
			{
				IDDLCrComBoxColImagCon->Select(2);
				break;
			}
			if(Secuencia.Contains("T,R,B,L"))
			{	
				IDDLCrComBoxColImagCon->Select(3);
				break;
			}
			if(Secuencia.Contains("T,B,L,R"))
			{	
				IDDLCrComBoxColImagCon->Select(4);
				break;
			}
			if(Secuencia.Contains("T,B,R,L"))
			{		
				IDDLCrComBoxColImagCon->Select(5);
				break;
			}
			if(Secuencia.Contains("B,L,T,R"))
			{		
				IDDLCrComBoxColImagCon->Select(6);
				break;
			}
			if(Secuencia.Contains("B,L,R,T"))
			{		
				IDDLCrComBoxColImagCon->Select(7);
				break;
			}
			if(Secuencia.Contains("B,R,L,T"))
			{		
				IDDLCrComBoxColImagCon->Select(8);
				break;
			}
			if(Secuencia.Contains("B,R,T,L"))
			{		
				IDDLCrComBoxColImagCon->Select(9);
				break;
			}
			if(Secuencia.Contains("B,T,R,L"))
			{		
				IDDLCrComBoxColImagCon->Select(10);
				break;
			}
			if(Secuencia.Contains("B,T,L,R"))
			{		
				IDDLCrComBoxColImagCon->Select(11);
				break;
			}
			if(Secuencia.Contains("L,T,B,R"))
			{		
				IDDLCrComBoxColImagCon->Select(12);
				break;
			}
			if(Secuencia.Contains("L,T,R,B"))
			{		
				IDDLCrComBoxColImagCon->Select(13);
				break;
			}
			if(Secuencia.Contains("L,B,T,R"))
			{		
				IDDLCrComBoxColImagCon->Select(14);
				break;
			}
			if(Secuencia.Contains("L,B,R,T"))
			{		
				IDDLCrComBoxColImagCon->Select(15);
				break;
			}
			if(Secuencia.Contains("L,R,B,T"))
			{		
				IDDLCrComBoxColImagCon->Select(16);
				break;
			}
			if(Secuencia.Contains("L,R,T,B"))
			{		
				IDDLCrComBoxColImagCon->Select(17);
				break;
			}
			if(Secuencia.Contains("R,T,B,L"))
			{		
				IDDLCrComBoxColImagCon->Select(18);
				break;
			}
			if(Secuencia.Contains("R,T,L,B"))
			{		
				IDDLCrComBoxColImagCon->Select(19);
				break;
			}
			if(Secuencia.Contains("R,L,T,B"))
			{		
				IDDLCrComBoxColImagCon->Select(20);
				break;
			}
			if(Secuencia.Contains("R,L,B,T"))
			{		
				IDDLCrComBoxColImagCon->Select(21);
				break;
			}
			if(Secuencia.Contains("R,B,L,T"))
			{		
				IDDLCrComBoxColImagCon->Select(22);
				break;
			}
			if(Secuencia.Contains("R,B,T,L"))
			{		
				IDDLCrComBoxColImagCon->Select(23);
				break;
			}
		}while(false);	
	}while(false);

}



void A2PPrefActionsOnDLGPreferenciasUtils::ColocaEnComboUnidades(IControlView *DlgPrefView,const WidgetID& widgetid,PMString Unidad)
{
	do
		{
			InterfacePtr<IWidgetParent>		myParent(DlgPrefView, UseDefaultIID());				
			if(myParent==nil)
			{
				break;
			}
			InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
			if(panel==nil)
			{
				break;
			}

			InterfacePtr<IControlView>		CVComboBoxColImagCon(panel->FindWidget(widgetid), UseDefaultIID() );
			if(CVComboBoxColImagCon==nil)
			{
				break;
			}
									
			InterfacePtr<IDropDownListController>	IDDLCrComBoxColImagCon( CVComboBoxColImagCon, IID_IDROPDOWNLISTCONTROLLER);
			if(IDDLCrComBoxColImagCon==nil)
			{
				break;
			}

						
			
			do
			{	
				if(Unidad.Contains("CentÌmetros") || Unidad.Contains("Centimeters"))
				{
					IDDLCrComBoxColImagCon->Select(0);
					break;
				}
				if(Unidad.Contains("CÌceros"))
				{	
					IDDLCrComBoxColImagCon->Select(1);
					break;
				}
				if(Unidad.Contains("MilÌmetros") || Unidad.Contains("Milimeters"))
				{
					IDDLCrComBoxColImagCon->Select(2);
					break;
				}
				if(Unidad.Contains("Picas"))
				{	
					IDDLCrComBoxColImagCon->Select(3);
					break;
				}
				if(Unidad.Contains("Pulganas") || Unidad.Contains("Inches"))
				{	
					IDDLCrComBoxColImagCon->Select(4);
					break;
				}
				if(Unidad.Contains("Puntos") || Unidad.Contains("Points"))
				{		
					IDDLCrComBoxColImagCon->Select(5);
					break;
				}
			}while(false);	
		}while(false);
}