/**
//
//	Declaracion de la estructura de preferencias
//
//
*/
#ifndef __Preferencias_h__
#define __Preferencias_h__

#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"
//#include "CDialogController.h"
//#include "A2PPrefID.h"
//#include "IDataBase.h"
#include "PMString.h"
#include <stdio.h>
#include <string.h>



 struct Preferencias
	{
	public:		
	
			bool UtilizaPlantilleroPref;
		/*********Preferencias de Documento***********/
	  	  //PMString ColorFondoEtiquetaConPublicidad;//Color Fondo de Caja Imagen Con Publicidad:
			PMString Nombrepreferencias;//Nombre de Preferencia:
			PMString UnidadesEnPrefA2P;
			PMString PaginasOpuestas;
			PMReal Medianil;
			PMReal AnchoPagina;//Ancho de Pagina:
			PMReal AltoPag; //Alto de Pagina:
			PMReal MargenSuperior;
			PMReal MargenInferior;
			PMReal MargenExterior; //Margen Exterior:
			PMReal MargenInterior;
			int32 NumColumas;  //CHECAR ESTE
			PMString UnidadesMedicion;
		/*Preferencias de capas*/
			/**************************
			bool CapaUpdate;
			bool CapaUpdateBlocked;
			bool CapaUpdateVisible;
		**************************/

			bool CapaEditorial;
			bool CapaEditorialBlocked;
			bool CapaEditorialVisible;

			bool CapaImagen;
			bool CapaImagenBlocked;
			bool CapaImagenVisible;

			bool CapaEtiquetaConPublicidad;
			bool CapaEtiquetaConPublicidadBlocked;
			bool CapaEtiquetaConPublicidadVisible;

			bool CapaEtiquetaSinPublicidad;
			bool CapaEtiquetaSinPublicidadBlocked;
			bool CapaEtiquetaSinPublicidadVisible;

			/*Propiedades de Etiqueta con Publicidad*/
			bool CreaEtiquetaConPublicidad;  
			PMString ColorTextEtiquetaConPublicidad ;
			PMString ColorFondoEtiquetaConPublicidad;//Color Fondo de Caja Imagen Con Publicidad:
			PMString TamMargenEtiquetaConPublicidad ;

			PMString PosCamposEtiCon1;
			PMString NomCamposEtiCon1;
			PMString PosCamposEtiCon2;
			PMString NomCamposEtiCon2;
			PMString PosCamposEtiCon3;
			PMString NomCamposEtiCon3;
			PMString PosCamposEtiCon4;
			PMString NomCamposEtiCon4;
			PMString PosCamposEtiCon5;
			PMString NomCamposEtiCon5;
			PMString PosCamposEtiCon6;
			PMString NomCamposEtiCon6;
			PMString PosCamposEtiCon7;
			PMString NomCamposEtiCon7;
			PMString PosCamposEtiCon8;
			PMString NomCamposEtiCon8;
			PMString PosCamposEtiCon9;
			PMString NomCamposEtiCon9;
			PMString PosCamposEtiCon10;
			PMString NomCamposEtiCon10;
			PMString PosCamposEtiCon11;
			PMString NomCamposEtiCon11;
			PMString PosCamposEtiCon12;
			PMString NomCamposEtiCon12;
			PMString PosCamposEtiCon13;
			PMString NomCamposEtiCon13;
			PMString PosCamposEtiCon14;
			PMString NomCamposEtiCon14;
			


			//CamposEtiquetaConPublicidad(14) As Campos

			/*Propiedades de Etiqueta sin Publicidad*/
			bool CreaEtiquetaSinPublicidad;
			bool BorrarEtiquetaDespuesDeActualizar;
			PMString ColorTextEtiquetaSinPublicidad;
			PMString ColorFondoEtiquetaSinPublicidad;
			PMString TamMargenEtiquetaSinPublicidad;

			PMString PosCamposEtiSin1;
			PMString NomCamposEtiSin1;
			PMString PosCamposEtiSin2;
			PMString NomCamposEtiSin2;
			PMString PosCamposEtiSin3;
			PMString NomCamposEtiSin3;
			PMString PosCamposEtiSin4;
			PMString NomCamposEtiSin4;
			PMString PosCamposEtiSin5;
			PMString NomCamposEtiSin5;
			PMString PosCamposEtiSin6;
			PMString NomCamposEtiSin6;
			PMString PosCamposEtiSin7;
			PMString NomCamposEtiSin7;
			PMString PosCamposEtiSin8;
			PMString NomCamposEtiSin8;
			PMString PosCamposEtiSin9;
			PMString NomCamposEtiSin9;
			PMString PosCamposEtiSin10;
			PMString NomCamposEtiSin10;
			PMString PosCamposEtiSin11;
			PMString NomCamposEtiSin11;
			PMString PosCamposEtiSin12;
			PMString NomCamposEtiSin12;
			PMString PosCamposEtiSin13;
			PMString NomCamposEtiSin13;
			PMString PosCamposEtiSin14;
			PMString NomCamposEtiSin14;
			PMString SepEnCampos;//guardara el codigo ascii de la separacion para los campos CHECAR ESTE

			/*orden de coordenadas*/
			PMString TipoLecturaCoor;
			PMString OrdendeLectura;
			PMString SepEnCoordenadas;//CHECAR ESTE 


			PMString UnidadesEnTop;
			PMString UnidadesEnLeft;
			PMString UnidadesEnButtom;
			PMString UnidadesEnRight;
	
			/*Propiedades Caja Imagen con Publicidad*/
			PMString ColorCajaImagenConPublicidad;
			PMString ColorMargenCajaImagenConPublicidad;
			PMString FitCajaImagenConPublicidad;

			
			/*Propiedades caja imagen sin publicidad*/
			PMString ColorCajaImagenSinPublicidad;
			PMString ColorMargenCajaImagenSinPublicidad;
			PMString FitCajaImagenSinPublicidad;
			
			/* Para mostrar dialogo de Etiqueta Editorial Fecha*/
			bool AbrirDialogoIssue;
			
			//changed to A2P v3.0
			PMString NombreEstiloIssue;
			//PMString FuenteDeIssue;
			//PMString TamFuenteIssue;
			
			PMString NombreEditorial;
			PMReal DesplazamientoHorIssue;
			PMReal DesplazamientoVerIssue;

			/*'Forma de leer archivo de entrada*/
			//FormaLecturaArchivo(16) As Campos
			int32 PosCampos1;
			PMString NomCampos1;
			int32 PosCampos2;
			PMString NomCampos2;
			int32 PosCampos3;
			PMString NomCampos3;
			int32 PosCampos4;
			PMString NomCampos4;
			int32 PosCampos5;
			PMString NomCampos5;
			int32 PosCampos6;
			PMString NomCampos6;
			int32 PosCampos7;
			PMString NomCampos7;
			int32 PosCampos8;
			PMString NomCampos8;
			int32 PosCampos9;
			PMString NomCampos9;
			int32 PosCampos10;
			PMString NomCampos10;
			int32 PosCampos11;
			PMString NomCampos11;
			int32 PosCampos12;
			PMString NomCampos12;
			int32 PosCampos13;
			PMString NomCampos13;
			int32 PosCampos14;
			PMString NomCampos14;
		};
	 //Preferencias Pref;
#endif // __SelDlgID_h__