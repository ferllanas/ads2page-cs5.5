//NUEVO
#include "VCPlugInHeaders.h"
#include "HelperInterface.h"
#include "SDKUtilities.h"
#include "CAlert.h"
#include"IApplication.h"
//#include<iomanip>
//#include <winreg.h>
#include "IUnitOfMeasureSettings.h"
#include "IMeasurementSystem.h"
#include "IUnitOfMeasure.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "ISelectableDialogSwitcher.h"
#include "ICoreFilename.h"
#include "IDocument.h"
#include "ILayoutUIUtils.h"
//#include "LayoutUIUtils.h"
#include "ILayoutUIUtils.h"
#include "IDialogCreator.h"
#include "IDialog.h"
#include "IDialogMgr.h"
#include "IDialogController.h"
#include "LocaleSetting.h"
#include "CoreResTypes.h"
// Interface includes:
#include "IDialogMgr.h"
#include "IApplication.h"
#include "ISession.h"
// Implementation includes:
#include "CDialogCreator.h"
#include "ResourceEnabler.h"
#include "RsrcSpec.h"
#include "CoreResTypes.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "SDKUtilities.h"
#include "FileUtils.h"
#include "FileTreeUtils.h"
#include <sys/types.h> 
#include <sys/stat.h> 
#ifdef MACINTOSH 
	#include <folders.h>
	#include <files.h>
	#include <sys/time.h>
#elif defined(WINDOWS)
	#include "..\Interlasa_common\InterlasaRegEditUtilities.h"
#endif

// To MAC  DONGLE
#include <stdio.h>
#include <stdlib.h>
/*#if !defined (_SP_LINUX_)
	#include "flags.h"
#endif
#if defined (_SP_MACX_)
#include <unistd.h>
#endif

#include "upromeps.h"
#include "upromepsdesign.h"

#define MAX_STRING_LENGTH  30 


#if !defined (_SP_LINUX_) && !defined (_SP_MACX_)
#define sleep(A) _sleep(A*1000)
#endif
*/

#include "A2PPrefID.h"
#include "A2PPreferencias.h"
//#include "InterlasaRegEditUtilities.h"
#include "IA2PPreferenciasFunciones.h"
#include "PreferenceUtils.h"

#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
	#include "A2PImpStructAvisos.h"
#endif

#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"
	#include "..\A2PImportacion\A2PImpStructAvisos.h"
#endif


#include "A2PPreferencias.h"

//#include "InterUltraProKyID.h"
//#include "IInterUltraObjectProKy.h"
//#include "IInterUltraProKy.h"





Preferencias PreferenciasEstaticas;
/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	@author Juan Fernando Llanas Rdz
*/
class A2PPreferenciasFunciones : public CPMUnknown<IA2PPreferenciasFunctions>
{
public:
	Preferencias PrefGlobal;
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	A2PPreferenciasFunciones (IPMUnknown *boss);
	
	/**
			Funcion que lee un documento de preferencias(*.pfa) dado por la direccion(Path)
			y llena la estructura de preferencias (Preferencia).
			
			  @param Path, Direccion donde se encuentra el archivo de preferencias.
			  @param Pref, Estructura preferencias.
		
	ABRE EL ARCHIVO DE PREFERENCIAS DEFAULT, lee linea por linea para busca el encabezado de cada 
	una de ellas dependiendo, el encabezado indica la preferencia en donde va ser almacenado el dato
	que sigue del encabezado ejemplo:
		Alto de pagina: 60 cm. 
	este campo sera guardado en Pref.altopag="60".
	*/
	
	bool16 GetDefaultPreferencesConnection(Preferencias &Pref,bool16 RefreshPreferencias);
	
	//bool16 leerDocDePreferencia(CString Path,Preferencias &Pref); //ORIGINAL NUEVO
	bool16 leerDocDePreferencia(PMString Path,Preferencias &Pref); //ANTERIOR

	/**
			Funcion que crea o rescribe un documento de preferencias (*.pfa) con un nombre dado por NomPref
			
			  @param NomPref, Nombre de las preferencias que fueron modificadas.
		*/
	bool16 EscribeDatosOnDocument(PMString NomPref,Preferencias &Pref);
	
	
	bool16 OpenDialogPassword();
	
	//PMString ProporcionaFechaArchivo(CString Archivo);//ORIGINAL NUEVO
      PMString ProporcionaFechaArchivo(PMString Archivo);

	bool16 openDialogForSecurity();
	
	//bool16 ConvertirUnidadesdePreferenciasAPuntos(Preferencias &Pref);

	//bool16 ConvertirPuntosAUnidadesdePreferencias(Preferencias &Pref);


private:

	/**
			Esta Funcion compara el encabezado de la linea que se esta leyendo uy regresa un entero
				//Tipo de propiedad: 1 propiedad normal, la lectura es en  la misma linea
				//					 2 Propiedad de posicio de los campos y nombre de los campos en el archivo a importar.
				//					 3 Propiedad de posicio de los campos y nombre de los campos que deben aparecer en etiqueta con publicidad.
				//					 4 Propiedad de posicio de los campos y nombre de los campos que deben aparecer en etiqueta sin publicidad
		*/
	int TipoPropiedad(CString LineaLeida,Preferencias &Pref);
//int TipoPropiedad(PMString LineaLeida,Preferencias &Pref);

	/**
			comparacion de los encabezados de cada linea y dependiendo este encabezado se guardala
			la preferencia dada.
	*/
	void SeparaCampos(int tipo,CString Cadena,Preferencias &Pref);
	
	bool16 VerificaSeguridadPirateria();
	
	PMReal ConversionUnidadesAPuntos(PMReal Cantidad);

	PMReal ConversionPuntosAUnidades(PMReal Cantidad);
	
	bool16 openDialogForContrato();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(A2PPreferenciasFunciones, kA2PPrefPreferenciasFuncionesImpl)


/* HelloWorld Constructor
*/
A2PPreferenciasFunciones::A2PPreferenciasFunciones(IPMUnknown* boss) 
: CPMUnknown<IA2PPreferenciasFunctions>(boss)
{
}



bool16 A2PPreferenciasFunciones::GetDefaultPreferencesConnection(Preferencias &Pref,bool16 RefreshPreferencias)
{
	PMString PathPwd="";
	do
	{
	   PathPwd=FileTreeUtils::CrearFolderPreferencias();

		PathPwd.Append("Default.pfa");
						
		if(RefreshPreferencias==kTrue)
		{
			if(!this->leerDocDePreferencia(PathPwd.GrabCString(), Pref))
			//if(!this->leerDocDePreferencia(PathPwd.GetPlatformString(), Pref))
			  return kFalse;
			
			PreferenciasEstaticas=Pref;
		}
		else
		{
			if(PreferenciasEstaticas.Nombrepreferencias.NumUTF16TextChars()<=0)
			{
				//CAlert::InformationAlert("AAA");
				if(!this->leerDocDePreferencia(PathPwd.GrabCString(), Pref))
					return kFalse;
			}
			else
			{
				Pref = PreferenciasEstaticas;
				/*//CAlert::InformationAlert("BBB");
				//Connection=PreferenciasEstaticas;
				Pref.UtilizaPlantilleroPref = PreferenciasEstaticas.UtilizaPlantilleroPref;
			//*********Preferencias de Documento***********
				Pref.Nombrepreferencias=PreferenciasEstaticas.Nombrepreferencias;
				Pref.UnidadesEnPrefA2P=PreferenciasEstaticas.UnidadesEnPrefA2P;
				Pref.PaginasOpuestas=PreferenciasEstaticas.PaginasOpuestas;
				Pref.Medianil=PreferenciasEstaticas.Medianil;
				Pref.AnchoPagina=PreferenciasEstaticas.AnchoPagina;
				Pref.AltoPag=PreferenciasEstaticas.AltoPag;
				Pref.MargenSuperior=PreferenciasEstaticas.MargenSuperior;
				Pref.MargenInferior=PreferenciasEstaticas.MargenInferior;
				Pref.MargenExterior=PreferenciasEstaticas.MargenExterior;
				Pref.MargenInterior=PreferenciasEstaticas.MargenInterior;
				Pref.NumColumas=PreferenciasEstaticas.NumColumas;
				Pref.UnidadesMedicion=PreferenciasEstaticas.UnidadesMedicion;
			//*Preferencias de capas*
			//**************************
			//	bool CapaUpdate;
			//	bool CapaUpdateBlocked;
			//	bool CapaUpdateVisible;
			//*************************

				Pref.CapaEditorial=PreferenciasEstaticas.CapaEditorial;
				Pref.CapaEditorialBlocked=PreferenciasEstaticas.CapaEditorialBlocked;
				Pref.CapaEditorialVisible=PreferenciasEstaticas.CapaEditorialVisible;

				Pref.CapaImagen=PreferenciasEstaticas.CapaImagen;
				Pref.CapaImagenBlocked=PreferenciasEstaticas.CapaImagenBlocked;;
				Pref.CapaImagenVisible=PreferenciasEstaticas.CapaImagenVisible;

				Pref.CapaEtiquetaConPublicidad=PreferenciasEstaticas.CapaEtiquetaConPublicidad;
				Pref.CapaEtiquetaConPublicidadBlocked=PreferenciasEstaticas.CapaEtiquetaConPublicidadBlocked;
				Pref.CapaEtiquetaConPublicidadVisible=PreferenciasEstaticas.CapaEtiquetaConPublicidadVisible;

				Pref.CapaEtiquetaSinPublicidad=PreferenciasEstaticas.CapaEtiquetaSinPublicidad;
				Pref.CapaEtiquetaSinPublicidadBlocked=PreferenciasEstaticas.CapaEtiquetaSinPublicidadBlocked;
				Pref.CapaEtiquetaSinPublicidadVisible=PreferenciasEstaticas.CapaEtiquetaSinPublicidadVisible;

			//*Propiedades de Etiqueta con Publicidad
				Pref.CreaEtiquetaConPublicidad=PreferenciasEstaticas.CreaEtiquetaConPublicidad;  
				Pref.ColorTextEtiquetaConPublicidad=PreferenciasEstaticas.ColorTextEtiquetaConPublicidad ;
				Pref.ColorFondoEtiquetaConPublicidad=PreferenciasEstaticas.ColorFondoEtiquetaConPublicidad;
				Pref.TamMargenEtiquetaConPublicidad=PreferenciasEstaticas.TamMargenEtiquetaConPublicidad ;

				Pref.PosCamposEtiCon1=PreferenciasEstaticas.PosCamposEtiCon1;
				Pref.NomCamposEtiCon1=PreferenciasEstaticas.NomCamposEtiCon1;
				Pref.PosCamposEtiCon2=PreferenciasEstaticas.PosCamposEtiCon2;
				Pref.NomCamposEtiCon2=PreferenciasEstaticas.NomCamposEtiCon2;
				Pref.PosCamposEtiCon3=PreferenciasEstaticas.PosCamposEtiCon3;
				Pref.NomCamposEtiCon3=PreferenciasEstaticas.NomCamposEtiCon3;
				Pref.PosCamposEtiCon4=PreferenciasEstaticas.PosCamposEtiCon4;
				Pref.NomCamposEtiCon4=PreferenciasEstaticas.NomCamposEtiCon4;
				Pref.PosCamposEtiCon5=PreferenciasEstaticas.PosCamposEtiCon5;
				Pref.NomCamposEtiCon5=PreferenciasEstaticas.NomCamposEtiCon5;
				Pref.PosCamposEtiCon6=PreferenciasEstaticas.PosCamposEtiCon6;
				Pref.NomCamposEtiCon6=PreferenciasEstaticas.NomCamposEtiCon6;
				Pref.PosCamposEtiCon7=PreferenciasEstaticas.PosCamposEtiCon7;
				Pref.NomCamposEtiCon7=PreferenciasEstaticas.NomCamposEtiCon7;
				Pref.PosCamposEtiCon8=PreferenciasEstaticas.PosCamposEtiCon8;
				Pref.NomCamposEtiCon8=PreferenciasEstaticas.NomCamposEtiCon8;
				Pref.PosCamposEtiCon9=PreferenciasEstaticas.PosCamposEtiCon9;
				Pref.NomCamposEtiCon9=PreferenciasEstaticas.NomCamposEtiCon9;
				Pref.PosCamposEtiCon10=PreferenciasEstaticas.PosCamposEtiCon10;
				Pref.NomCamposEtiCon10=PreferenciasEstaticas.NomCamposEtiCon10;
				Pref.PosCamposEtiCon11=PreferenciasEstaticas.PosCamposEtiCon11;
				Pref.NomCamposEtiCon11=PreferenciasEstaticas.NomCamposEtiCon11;
				Pref.PosCamposEtiCon12=PreferenciasEstaticas.PosCamposEtiCon12;
				Pref.NomCamposEtiCon12=PreferenciasEstaticas.NomCamposEtiCon12;
				Pref.PosCamposEtiCon13=PreferenciasEstaticas.PosCamposEtiCon13;
				Pref.NomCamposEtiCon13=PreferenciasEstaticas.NomCamposEtiCon13;
				Pref.PosCamposEtiCon14=PreferenciasEstaticas.PosCamposEtiCon14;
				Pref.NomCamposEtiCon14=PreferenciasEstaticas.NomCamposEtiCon14;
			


			//CamposEtiquetaConPublicidad(14) As Campos

			//*Propiedades de Etiqueta sin Publicidad
				Pref.CreaEtiquetaSinPublicidad=PreferenciasEstaticas.CreaEtiquetaSinPublicidad;
				Pref.BorrarEtiquetaDespuesDeActualizar=PreferenciasEstaticas.BorrarEtiquetaDespuesDeActualizar;
				Pref.ColorTextEtiquetaSinPublicidad=PreferenciasEstaticas.ColorTextEtiquetaSinPublicidad;
				Pref.ColorFondoEtiquetaSinPublicidad=PreferenciasEstaticas.ColorFondoEtiquetaSinPublicidad;
				Pref.TamMargenEtiquetaSinPublicidad=PreferenciasEstaticas.TamMargenEtiquetaSinPublicidad;

				Pref.PosCamposEtiSin1=PreferenciasEstaticas.PosCamposEtiSin1;
				Pref.NomCamposEtiSin1=PreferenciasEstaticas.NomCamposEtiSin1;
				Pref.PosCamposEtiSin2=PreferenciasEstaticas.PosCamposEtiSin2;
				Pref.NomCamposEtiSin2=PreferenciasEstaticas.NomCamposEtiSin2;
				Pref.PosCamposEtiSin3=PreferenciasEstaticas.PosCamposEtiSin3;
				Pref.NomCamposEtiSin3=PreferenciasEstaticas.NomCamposEtiSin3;
				Pref.PosCamposEtiSin4=PreferenciasEstaticas.PosCamposEtiSin4;
				Pref.NomCamposEtiSin4=PreferenciasEstaticas.NomCamposEtiSin4;
				Pref.PosCamposEtiSin5=PreferenciasEstaticas.PosCamposEtiSin5;
				Pref.NomCamposEtiSin5=PreferenciasEstaticas.NomCamposEtiSin5;
				Pref.PosCamposEtiSin6=PreferenciasEstaticas.PosCamposEtiSin6;
				Pref.NomCamposEtiSin6=PreferenciasEstaticas.NomCamposEtiSin6;
				Pref.PosCamposEtiSin7=PreferenciasEstaticas.PosCamposEtiSin7;
				Pref.NomCamposEtiSin7=PreferenciasEstaticas.NomCamposEtiSin7;
				Pref.PosCamposEtiSin8=PreferenciasEstaticas.PosCamposEtiSin8;
				Pref.NomCamposEtiSin8=PreferenciasEstaticas.NomCamposEtiSin8;
				Pref.PosCamposEtiSin9=PreferenciasEstaticas.PosCamposEtiSin9;
				Pref.NomCamposEtiSin9=PreferenciasEstaticas.NomCamposEtiSin9;
				Pref.PosCamposEtiSin10=PreferenciasEstaticas.PosCamposEtiSin10;
				Pref.NomCamposEtiSin10=PreferenciasEstaticas.NomCamposEtiSin10;
				Pref.PosCamposEtiSin11=PreferenciasEstaticas.PosCamposEtiSin11;
				Pref.NomCamposEtiSin11=PreferenciasEstaticas.NomCamposEtiSin11;
				Pref.PosCamposEtiSin12=PreferenciasEstaticas.PosCamposEtiSin12;
				Pref.NomCamposEtiSin12=PreferenciasEstaticas.NomCamposEtiSin12;
				Pref.PosCamposEtiSin13=PreferenciasEstaticas.PosCamposEtiSin13;
				Pref.NomCamposEtiSin13=PreferenciasEstaticas.NomCamposEtiSin13;
				Pref.PosCamposEtiSin14=PreferenciasEstaticas.PosCamposEtiSin14;
				Pref.NomCamposEtiSin14=PreferenciasEstaticas.NomCamposEtiSin14;
				Pref.SepEnCampos=PreferenciasEstaticas.SepEnCampos;//guardara el codigo anscii de la separacion para los campos

			///*orden de coordenadas
				Pref.TipoLecturaCoor=PreferenciasEstaticas.TipoLecturaCoor;
				Pref.OrdendeLectura=PreferenciasEstaticas.OrdendeLectura;
				Pref.SepEnCoordenadas=PreferenciasEstaticas.SepEnCoordenadas;


				Pref.UnidadesEnTop=PreferenciasEstaticas.UnidadesEnTop;
				Pref.UnidadesEnLeft=PreferenciasEstaticas.UnidadesEnLeft;
				Pref.UnidadesEnButtom=PreferenciasEstaticas.UnidadesEnButtom;
				Pref.UnidadesEnRight=PreferenciasEstaticas.UnidadesEnRight;
	
			///*Propiedades Caja Imagen con Publicidad
				Pref.ColorCajaImagenConPublicidad=PreferenciasEstaticas.ColorCajaImagenConPublicidad;
				Pref.ColorMargenCajaImagenConPublicidad=PreferenciasEstaticas.ColorMargenCajaImagenConPublicidad;
				Pref.FitCajaImagenConPublicidad=PreferenciasEstaticas.FitCajaImagenConPublicidad;

			
			///*Propiedades caja imagen sin publicidad
				Pref.ColorCajaImagenSinPublicidad=PreferenciasEstaticas.ColorCajaImagenSinPublicidad;
				Pref.ColorMargenCajaImagenSinPublicidad=PreferenciasEstaticas.ColorMargenCajaImagenSinPublicidad;
				Pref.FitCajaImagenSinPublicidad=PreferenciasEstaticas.FitCajaImagenSinPublicidad;
			
			///* Para mostrar dialogo de Etiqueta Editorial Fecha
				Pref.AbrirDialogoIssue=Pref.AbrirDialogoIssue;
			
			//changed to A2P v3.0
				Pref.NombreEstiloIssue=PreferenciasEstaticas.NombreEstiloIssue;
			//PMString FuenteDeIssue;
			//PMString TamFuenteIssue;
			
				Pref.NombreEditorial=PreferenciasEstaticas.NombreEditorial;
				Pref.DesplazamientoHorIssue=PreferenciasEstaticas.DesplazamientoHorIssue;
				Pref.DesplazamientoVerIssue=PreferenciasEstaticas.DesplazamientoVerIssue;

			///*'Forma de leer archivo de entrada
			//FormaLecturaArchivo(16) As Campos
				Pref.PosCampos1=PreferenciasEstaticas.PosCampos1;
				Pref.NomCampos1=PreferenciasEstaticas.NomCampos1;
				Pref.PosCampos2=PreferenciasEstaticas.PosCampos2;
				Pref.NomCampos2=PreferenciasEstaticas.NomCampos2;
				Pref.PosCampos3=PreferenciasEstaticas.PosCampos3;
				Pref.NomCampos3=PreferenciasEstaticas.NomCampos3;
				Pref.PosCampos4=PreferenciasEstaticas.PosCampos4;
				Pref.NomCampos4=PreferenciasEstaticas.NomCampos4;
				Pref.PosCampos5=PreferenciasEstaticas.PosCampos5;
				Pref.NomCampos5=PreferenciasEstaticas.NomCampos5;
				Pref.PosCampos6=PreferenciasEstaticas.PosCampos6;
				Pref.NomCampos6=PreferenciasEstaticas.NomCampos6;
				Pref.PosCampos7=PreferenciasEstaticas.PosCampos7;
				Pref.NomCampos7=PreferenciasEstaticas.NomCampos7;
				Pref.PosCampos8=PreferenciasEstaticas.PosCampos8;
				Pref.NomCampos8=PreferenciasEstaticas.NomCampos8;
				Pref.PosCampos9=PreferenciasEstaticas.PosCampos9;
				Pref.NomCampos9=PreferenciasEstaticas.NomCampos9;
				Pref.PosCampos10=PreferenciasEstaticas.PosCampos10;
				Pref.NomCampos10=PreferenciasEstaticas.NomCampos10;
				Pref.PosCampos11=PreferenciasEstaticas.PosCampos11;
				Pref.NomCampos11=PreferenciasEstaticas.NomCampos11;
				Pref.PosCampos12=PreferenciasEstaticas.PosCampos12;
				Pref.NomCampos12=PreferenciasEstaticas.NomCampos12;
				Pref.PosCampos13=PreferenciasEstaticas.PosCampos13;
				Pref.NomCampos13=PreferenciasEstaticas.NomCampos13;
				Pref.PosCampos14=PreferenciasEstaticas.PosCampos14;
				Pref.NomCampos14=PreferenciasEstaticas.NomCampos14;
				*/
			}
		}
		
		
	}while(false);
	return kTrue;
}

/**
	funcion que abre el archivo de preferencias default y llena la estructura de preferencias
	@Preferencia estructura que contiene las preferencias del archivo Defaul.pfa.
*/
//bool16 A2PPreferenciasFunciones::leerDocDePreferencia(CString Path,Preferencias &Pref)
bool16 A2PPreferenciasFunciones::leerDocDePreferencia(PMString Path,Preferencias &Pref)//ANTERIOR
{
	bool16 retval=kFalse;
	
	bool16 entrada;			//esta bandera es utilizada para indicar que existen caracteres en el archivo de preferencias			//	
	FILE *ArchivoPref;			//Archivo a leer
	char Linea[1000];			//Conjunto de caracteres de la linea que se ha leido del archivo
	int32 TipoProp;			//Tipo de propiedad: 1 propiedad normal, la lectura es en  la misma linea
							//2 Propiedad de posicio de los campos y nombre de los campos en el archivo a importar.
							//3 Propiedad de posicio de los campos y nombre de los campos que deben aparecer en etiqueta con publicidad.
							//4 Propiedad de posicio de los campos y nombre de los campos que deben aparecer en etiqueta sin publicidad
	//inicializa variables
	entrada=kFalse;
	
	PMString PathPMstring=Path;
	//PathPMstring=InterlasaUtilities::MacToUnix(PathPMstring);
	//CAlert::InformationAlert(PathPMstring);
	
	#if defined(MACINTOSH)
		if((ArchivoPref=FileUtils::OpenFile(PathPMstring.GrabCString(),"r"))==NULL)
	#elif defined(WINDOWS)
		if((ArchivoPref=fopen(PathPMstring.GrabCString(),"r"))==NULL)
	#endif
	
	{	
		retval=kFalse;
	}
	else
	{	
		while(fgets(Linea,80,ArchivoPref)!=NULL)//lee siguiente linea del archivo preferencias hasta llegar al final del archivo
		{	
			entrada=kTrue;//se enciontro caracteres
			TipoProp=TipoPropiedad(Linea,Pref);//busca el tipo de propiedad de la linea leida
			switch(TipoProp)
			{	
				case 1://si el tipo de propiedad es 1 indica que la preferencia ya ha sido guardada en la
						//funcion Tipo Propiedad
					break;
				case 2://si el tipo de propiedad es 2 indica que el tipo de propiedad es
						//orden de campos en el archivo importar
					//CAlert::InformationAlert("tipo w2");
					fgets(Linea,80,ArchivoPref);//la sigueinete linea son mensajes(Posicampo	NomCampo)
					fgets(Linea,80,ArchivoPref);//lectura del primer campo
					do
					{
						SeparaCampos(TipoProp,Linea,Pref);//Separacion del encabezado, Posicion del campo y Nombre de campo
						fgets(Linea,80,ArchivoPref);//lee la siguente linea del archivo preferencias
					}while(strncmp(";",Linea,1)!=0);//lee las siguientes lineas hasta encontrar ;
					
					break;
				case 3:
					//CAlert::InformationAlert("tipo w3");
					fgets(Linea,80,ArchivoPref);//la sigueinete linea son mensajes(Posicampo	NomCampo)
					fgets(Linea,80,ArchivoPref);//lectura del primer campo
					do
					{
						SeparaCampos(TipoProp,Linea,Pref);//Separacion del encabezado, Posicion del campo y Nombre de campo
						fgets(Linea,80,ArchivoPref);//lee la siguente linea del archivo preferencias
					}while(strncmp(";:",Linea,1)!=0);//lee las siguientes lineas hasta encontrar ;:
					
					break;
				case 4:
					//CAlert::InformationAlert("tipo w4");
					fgets(Linea,80,ArchivoPref);//la sigueinete linea son mensajes(Posicampo	NomCampo)
					fgets(Linea,80,ArchivoPref);//lectura del primer campo
					do
					{
						SeparaCampos(TipoProp,Linea,Pref);//Separacion del encabezado, Posicion del campo y Nombre de campo
						fgets(Linea,80,ArchivoPref);//lee la siguente linea del archivo preferencias
					}while(strncmp(";",Linea,1)!=0);//lee las siguientes lineas hasta encontrar ;:
					
					break;
				case 0:
					break;
			}
		}
		if(entrada==kFalse)
		{
			CAlert::ErrorAlert(kA2PPrefErrorLecturaVerArchDefaultStringKey); 
		}
		retval=kTrue;
		fclose(ArchivoPref);	
		
		
	}
	
	PreferenciasEstaticas=Pref;
	
	//CAlert::InformationAlert("EXPERIMENTO22  "+PreferenciasEstaticas.Nombrepreferencias);
	
	return retval;
}
//escrible  AQUI GUARDA LOS DATOS DE LAS PREFERENCIAS ACCEDEMOS, LEEMOS Y GUARDAMOS
bool16 A2PPreferenciasFunciones::EscribeDatosOnDocument(PMString NomPref, Preferencias &Pref)
{  //CAlert::InformationAlert("entro a EscribeDatosOnDocument");
	bool16 retval=kTrue;

	FILE *stream;
	//NOTA:LA LINEA PMString CadenaTemporal SE ENCONTRABA COMENTADA ORIGINAL
	//PMString CadenaTemporal;// Variable para almacenar temporalmente el texto del objeto como PMStrting 
	//CString CopiaCadenaTemporal;//Varable para almacenar Temporalmente la cadena de la conversion de PMString a CString
	PMString CopiaCadenaTemporal;
    CopiaCadenaTemporal="";//Inicializa Copia de la cadena Nota: si no se inicializa esta cadena no podra almacenar la cadena y se creara un error.
	
	
	PMString Default_Prefer_Path=FileTreeUtils::CrearFolderPreferencias();
	Default_Prefer_Path.Append(NomPref);
	Default_Prefer_Path.Append(".pfa");
	//CAlert::InformationAlert(Default_Prefer_Path+" G1");
	IDFile sisfile;
	
	//sisfile=SDKUtilities::PMStringToSysFile(&Default_Prefer_Path);
		
	 InterfacePtr<ICoreFilename> cfn((ICoreFilename*)::CreateObject(kCoreFilenameBoss, IID_ICOREFILENAME)); 
	 		
	 cfn->Initialize(&Default_Prefer_Path);
	
	PMString mode="w";
	
	//Default_Prefer_Path=InterlasaUtilities::MacToUnix(Default_Prefer_Path);
	CopiaCadenaTemporal=Default_Prefer_Path.GrabCString();	
	//CAlert::InformationAlert(CopiaCadenaTemporal+ " G2");
	#if defined(MACINTOSH)
		if( (stream  =FileUtils::OpenFile(CopiaCadenaTemporal,"w"))!=NULL)
	#elif defined(WINDOWS)
		//if( (stream  =fopen(CopiaCadenaTemporal,"w"))!=NULL)//ORIGINAL
	      if( (stream  =fopen(CopiaCadenaTemporal.GrabCString(),"w"))!=NULL)
	#endif
	   //cfn->fileopen(&mode))!=NULL)// 
   {
	//***Pereferencias de Documentos*******
	float PmrealTofloat= ToFloat(Pref.AnchoPagina);
	fprintf(stream,"Ancho de Pagina: %f\n",	PmrealTofloat);

	
	//Obtengo la cadena del objeto caja
	PmrealTofloat= ToFloat(Pref.AltoPag);
	fprintf(stream,"Alto de Pagina: %f\n",	PmrealTofloat);
	
	fprintf(stream,"Numero_de_Columnas: %d\n",	Pref.NumColumas);

	PmrealTofloat= ToFloat(Pref.Medianil);
	fprintf(stream,"Medianil: %f\n",	PmrealTofloat);

	PmrealTofloat= ToFloat(Pref.MargenExterior);
	fprintf(stream,"Margen Exterior: %f\n",	PmrealTofloat);

	PmrealTofloat= ToFloat(Pref.MargenInferior);
	fprintf(stream,"Margen Inferior: %f\n",	PmrealTofloat);

	PmrealTofloat= ToFloat(Pref.MargenInterior);
	fprintf(stream,"Margen Interior: %f\n",	PmrealTofloat);
	
	PmrealTofloat= ToFloat(Pref.MargenSuperior);
	fprintf(stream,"Margen Superior: %f\n",PmrealTofloat);
	
	CopiaCadenaTemporal=Pref.UnidadesMedicion.GrabCString();
	fprintf(stream,"Unidades de Medicion: %s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.Nombrepreferencias.GrabCString();
  //CopiaCadenaTemporal=Pref.ColorCajaImagenConPublicidad.GrabCString();
  //fprintf(stream,"Nombre de Preferencia: %s\n\n",	CopiaCadenaTemporal.GrabCString());
    fprintf(stream,"Nombre de Preferencia: %s\n",	CopiaCadenaTemporal.GrabCString());
//**************Preferencias de Capas*************************

	//Preferencia.CapaEditorial=GetTriStateControlData(kA2PPrefCkBoxCapaEditID);
	
	fprintf(stream,"UtilizaPlantilleroPref: %d\n",	Pref.UtilizaPlantilleroPref);
	
	fprintf(stream,"Crear Capa Editorial: %d\n",	Pref.CapaEditorial);
	fprintf(stream,"Capa Editorial Visible: %d\n",	Pref.CapaEditorialVisible);
	fprintf(stream,"Capa Editorial Bloqueada: %d\n",Pref.CapaEditorialBlocked);

	fprintf(stream,"Crear Capa Imagen: %d\n",	Pref.CapaImagen);
	fprintf(stream,"Capa Imagen Visible: %d\n",	Pref.CapaImagenVisible);
	fprintf(stream,"Capa Imagen Bloqueada: %d\n",	Pref.CapaImagenBlocked);
	
	fprintf(stream,"Crear Capa Etiqueta con Publicidad: %d\n",	Pref.CapaEtiquetaConPublicidad);
	fprintf(stream,"Capa Etiqueta con Publicidad Visible: %d\n",	Pref.CapaEtiquetaConPublicidadVisible);
	fprintf(stream,"Capa Etiqueta con Publicidad Bloqueada: %d\n",	Pref.CapaEtiquetaConPublicidadBlocked);
	
	fprintf(stream,"Crear Capa Etiqueta sin Publicidad: %d\n",	Pref.CapaEtiquetaSinPublicidad);
	fprintf(stream,"Capa Etiqueta sin Publicidad Visible: %d\n",	Pref.CapaEtiquetaSinPublicidadVisible);
	fprintf(stream,"Capa Etiqueta sin Publicidad Bloqueada: %d\n\n",	Pref.CapaEtiquetaSinPublicidadBlocked);

//***************Preferencias de Caja Imagen*****************************
	
	CopiaCadenaTemporal=Pref.ColorCajaImagenConPublicidad.GrabCString();
	fprintf(stream,"Color Fondo de Caja Imagen Con Publicidad: %s\n",	CopiaCadenaTemporal.GrabCString());
	//CAlert::InformationAlert("EXPERIMENTO== "+ Pref.ColorCajaImagenConPublicidad);
	
	CopiaCadenaTemporal=Pref.ColorMargenCajaImagenConPublicidad.GrabCString();
	fprintf(stream,"Color Margen de Caja Imagen Con Publicidad: %s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.FitCajaImagenConPublicidad.GrabCString();
	fprintf(stream,"Ajuste de Imagen en Caja: %s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.ColorCajaImagenSinPublicidad.GrabCString();
	fprintf(stream,"Color Fondo de Caja Imagen Sin Publicidad: %s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.ColorMargenCajaImagenSinPublicidad.GrabCString();
	fprintf(stream,"Color Margen de Caja Imagen Sin Publicidad: %s\n\n",CopiaCadenaTemporal.GrabCString());

//**************************Preferencias Importar***************************************

	CopiaCadenaTemporal=Pref.SepEnCampos.GrabCString();
	fprintf(stream,"Separacion de Campos: %s	\n",CopiaCadenaTemporal.GrabCString());

	fprintf(stream,"Orden de lectura de campos: \n");
	fprintf(stream,"Num NomCampo 	 \n");

    //se supone que este es el Campo1:1	Name
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());
	

	fprintf(stream,"Campo1:%d	",	Pref.PosCampos1);
	CopiaCadenaTemporal=Pref.NomCampos1.GrabCString();
    fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

	fprintf(stream,"%s\n",          CopiaCadenaTemporal.GrabCString());
	fprintf(stream,"Campo2:%d	",	Pref.PosCampos2);
	CopiaCadenaTemporal=            Pref.NomCampos2.GrabCString();
	fprintf(stream,"%s\n",          CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Campo3:%d	",	Pref.PosCampos3);
	CopiaCadenaTemporal=Pref.NomCampos3.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Campo4:%d	",	Pref.PosCampos4);
	
	CopiaCadenaTemporal=Pref.NomCampos4.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Campo5:%d	",	Pref.PosCampos5);
	
	CopiaCadenaTemporal=Pref.NomCampos5.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Campo6:%d	",	Pref.PosCampos6);
	
	CopiaCadenaTemporal=Pref.NomCampos6.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());


	fprintf(stream,"Campo7:%d	",	Pref.PosCampos7);
	
	CopiaCadenaTemporal=Pref.NomCampos7.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Campo8:%d	",	Pref.PosCampos8);
	
	CopiaCadenaTemporal=Pref.NomCampos8.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Campo9:%d	",	Pref.PosCampos9);
	CopiaCadenaTemporal=Pref.NomCampos9.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Campo10:%d	",	Pref.PosCampos10);
	CopiaCadenaTemporal=Pref.NomCampos10.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

   	
	fprintf(stream,"Campo11:%d	",	Pref.PosCampos11);
	CopiaCadenaTemporal=Pref.NomCampos11.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());


	fprintf(stream,"Campo12:%d	",	Pref.PosCampos12);
	CopiaCadenaTemporal=Pref.NomCampos12.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Campo13:%d	",	Pref.PosCampos13);
	CopiaCadenaTemporal=Pref.NomCampos13.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Campo14:%d	",	Pref.PosCampos14);
	
	CopiaCadenaTemporal=Pref.NomCampos14.GrabCString();
	fprintf(stream,"%s\n",CopiaCadenaTemporal.GrabCString());
	fprintf(stream,"; \n");

//***************Orden de Coordenadas *******************************
		//NO LOS VUELVE A GUARDAR
		//CAlert::InformationAlert("2");
        CopiaCadenaTemporal=Pref.SepEnCoordenadas.GrabCString();
		fprintf(stream,"Separacion de Coordenadas: %s\n", CopiaCadenaTemporal.GrabCString());
		
		CopiaCadenaTemporal=Pref.TipoLecturaCoor.GrabCString();
		fprintf(stream,"Tipo de Lectura en Coordenadas: %s\n",CopiaCadenaTemporal.GrabCString());

	
		CopiaCadenaTemporal=Pref.OrdendeLectura.GrabCString();
		fprintf(stream,"Orden Lectura de Coordenadas: %s\n",CopiaCadenaTemporal.GrabCString());

	
		CopiaCadenaTemporal=Pref.UnidadesEnTop.GrabCString();
		fprintf(stream,"Unidades en Top: %s\n",CopiaCadenaTemporal.GrabCString());
		
	
		CopiaCadenaTemporal=Pref.UnidadesEnLeft.GrabCString();
		fprintf(stream,"Unidades en Left: %s\n",CopiaCadenaTemporal.GrabCString());

		CopiaCadenaTemporal=Pref.UnidadesEnButtom.GrabCString();
		fprintf(stream,"Unidades en Buttom: %s\n",CopiaCadenaTemporal.GrabCString());

		CopiaCadenaTemporal=Pref.UnidadesEnRight.GrabCString();
		fprintf(stream,"Unidades en Right: %s\n",CopiaCadenaTemporal.GrabCString());

//***************Etiquetas*******************************
	
	fprintf(stream,"Crear Etiqueta Con Publicidad: %d\n",	Pref.CreaEtiquetaConPublicidad);

	CopiaCadenaTemporal=Pref.ColorFondoEtiquetaConPublicidad.GrabCString();
	fprintf(stream,"Color de Fondo en Etiqueta Con Publicidad: %s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.ColorTextEtiquetaConPublicidad.GrabCString();
	fprintf(stream,"Color de Texto en Etiqueta Con Publicidad: %s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.TamMargenEtiquetaConPublicidad.GrabCString();
	fprintf(stream,"Tamaño de Margen en Etiqueta Con Publicidad: %s\n",	CopiaCadenaTemporal.GrabCString());

	
	fprintf(stream,"Crear Etiqueta Sin Publicidad: %d\n",	Pref.CreaEtiquetaSinPublicidad);
	
	fprintf(stream,"BorarEtiquetaSDespuesDeActualizarImagen: %d\n",	Pref.BorrarEtiquetaDespuesDeActualizar);
	
	
	CopiaCadenaTemporal=Pref.ColorFondoEtiquetaSinPublicidad.GrabCString();
	fprintf(stream,"Color de Fondo en Etiqueta Sin Publicidad: %s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.ColorTextEtiquetaSinPublicidad.GrabCString();
	fprintf(stream,"Color de Texto en Etiqueta Sin Publicidad: %s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.TamMargenEtiquetaSinPublicidad.GrabCString();
	fprintf(stream,"Tamaño de Margen en Etiqueta Sin Publicidad: %s\n\n",	CopiaCadenaTemporal.GrabCString());
	
	/***/
		fprintf(stream,"Abrir Dialogo Editorial: %d\n",	Pref.AbrirDialogoIssue);

	//Changed to A2P v3.0
	//	CopiaCadenaTemporal=Pref.FuenteDeIssue.GrabCString();
	//	fprintf(stream,"Fuente de Issue: %s\n",	CopiaCadenaTemporal);

	//	CopiaCadenaTemporal=Pref.TamFuenteIssue.GrabCString();
	//	fprintf(stream,"Tamaño Fuente de Issue: %s\n",	CopiaCadenaTemporal);
		CopiaCadenaTemporal=Pref.NombreEstiloIssue.GrabCString();
		fprintf(stream,"Nombre_Estilo: %s\n",	CopiaCadenaTemporal.GrabCString());
		
		CopiaCadenaTemporal=Pref.NombreEditorial.GrabCString();
		fprintf(stream,"Nombre Issue: %s\n",	CopiaCadenaTemporal.GrabCString());
		
		
		PmrealTofloat= ToFloat(Pref.DesplazamientoHorIssue);
		fprintf(stream,"Desplazamiento Hor. Issue: %f\n",	PmrealTofloat);
		PmrealTofloat= ToFloat(Pref.DesplazamientoVerIssue);
		fprintf(stream,"Desplazamiento Ver. Issue: %f\n",	PmrealTofloat);
		
//**************Campos en Etiqueta********************
//****Campos en Etiqueta Con Publicidad******
	fprintf(stream,"Campos en Etiqueta con Publicidad\n");
	fprintf(stream,"Num NomCampo\n");

	CopiaCadenaTemporal=Pref.PosCamposEtiCon1.GrabCString();
	fprintf(stream,"Campo1:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon1.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon2.GrabCString();
	fprintf(stream,"Campo2:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon2.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon3.GrabCString();
	fprintf(stream,"Campo3:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon3.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon4.GrabCString();
	fprintf(stream,"Campo4:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon4.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon5.GrabCString();
	fprintf(stream,"Campo5:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon5.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon6.GrabCString();
	fprintf(stream,"Campo6:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon6.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon7.GrabCString();
	fprintf(stream,"Campo7:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon7.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon8.GrabCString();
	fprintf(stream,"Campo8:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon8.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon9.GrabCString();
	fprintf(stream,"Campo9:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon9.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon10.GrabCString();
	fprintf(stream,"Campo10:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon10.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon11.GrabCString();
	fprintf(stream,"Campo11:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon11.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon12.GrabCString();
	fprintf(stream,"Campo12:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon12.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon13.GrabCString();
	fprintf(stream,"Campo13:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon13.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiCon14.GrabCString();
	fprintf(stream,"Campo14:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiCon14.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());
	fprintf(stream,"; \n");
	
	//****Campos en Etiqueta Sin publicidad***
	fprintf(stream,"Campos en Etiqueta sin Publicidad\n");
	fprintf(stream,"Num NomCampo\n");

	CopiaCadenaTemporal=Pref.PosCamposEtiSin1.GrabCString();
	fprintf(stream,"Campo1:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin1.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin2.GrabCString();
	fprintf(stream,"Campo2:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin2.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin3.GrabCString();
	fprintf(stream,"Campo3:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin3.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin4.GrabCString();
	fprintf(stream,"Campo4:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin4.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin5.GrabCString();
	fprintf(stream,"Campo5:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin5.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin6.GrabCString();
	fprintf(stream,"Campo6:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin6.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin7.GrabCString();
	fprintf(stream,"Campo7:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin7.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin8.GrabCString();
	fprintf(stream,"Campo8:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin8.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin9.GrabCString();
	fprintf(stream,"Campo9:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin9.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin10.GrabCString();
	fprintf(stream,"Campo10:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin10.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin11.GrabCString();
	fprintf(stream,"Campo11:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin11.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin12.GrabCString();
	fprintf(stream,"Campo12:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin12.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin13.GrabCString();
	fprintf(stream,"Campo13:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin13.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.PosCamposEtiSin14.GrabCString();
	fprintf(stream,"Campo14:%s	",	CopiaCadenaTemporal.GrabCString());

	CopiaCadenaTemporal=Pref.NomCamposEtiSin14.GrabCString();
	fprintf(stream,"%s\n",	CopiaCadenaTemporal.GrabCString());
	fprintf(stream,";");
	fclose(stream);

   }
   else
   {
	   CAlert::InformationAlert(kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
   }
	
	return retval;
}

/**
*/
int A2PPreferenciasFunciones::TipoPropiedad(CString LineaLeida,Preferencias &Pref)
//int A2PPreferenciasFunciones::TipoPropiedad(PMString LineaLeida,Preferencias &Pref)

{
	//int Tamcadena=strlen(LineaLeida);
	char *Elemento;
	
	
	PMString PMCadena(LineaLeida);
	PMCadena.Remove(PMCadena.NumUTF16TextChars()-1,1);
		
		if(PMCadena.Contains("Ancho de Pagina:") )
		{
			PMCadena.Remove(0,16);
			Pref.AnchoPagina = PMCadena.GetAsDouble();	
			return(1);
		}
		
		if(PMCadena.Contains("Alto de Pagina:") )
		{
			PMCadena.Remove(0,15);
			Pref.AltoPag = PMCadena.GetAsDouble();
			return(1);	
		}
	
		if(PMCadena.Contains("Medianil:") )
		{
			PMCadena.Remove(0,9);
			Pref.Medianil = PMCadena.GetAsDouble();	
			return(1);
		}
		
		if(PMCadena.Contains("Margen Exterior:") )
		{
			PMCadena.Remove(0,16);
			Pref.MargenExterior = PMCadena.GetAsDouble();	
			return(1);
		}
	
	
		if(PMCadena.Contains("Margen Interior:") )
		{
			PMCadena.Remove(0,16);
			Pref.MargenInterior = PMCadena.GetAsDouble();	
			return(1);
		}
	
		if(PMCadena.Contains("Margen Superior:") )
		{
			PMCadena.Remove(0,16);
			Pref.MargenSuperior = PMCadena.GetAsDouble();
			return(1);	
		}
	
		if(PMCadena.Contains("Margen Inferior:") )
		{
			PMCadena.Remove(0,16);
			Pref.MargenInferior = PMCadena.GetAsDouble();	
			return(1);
		}
	
		if(PMCadena.Contains("Numero_de_Columnas:") )
		{	
			//CAlert::InformationAlert("Numero_de_Columnas: " +PMCadena );
			PMCadena.Remove(0,19);
			//CAlert::InformationAlert(PMCadena + ",");
			Pref.NumColumas = PMCadena.GetAsNumber();	
			//CAlert::InformationAlert("jhhj");
			return(1);
		}
		

	if(strncmp("Separacion de Campos:",LineaLeida,21)==0)
	{	//CAlert::InformationAlert("SEPARACION DE CAMPOS?: "+PMCadena);
	   		Elemento=strtok(LineaLeida,":\n");
			while(Elemento!=NULL)
			{	Elemento=strtok(NULL,":\n");
				Pref.SepEnCampos=Elemento;
				return(1);
			}
	  
	}
		
   if(strncmp("Separacion de Coordenadas:",LineaLeida,26)==0)
	{	//CAlert::InformationAlert("SEPARACION DE COORDNADAS?: "+PMCadena);	
      
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.SepEnCoordenadas=Elemento;
			return(1);
		}

		
	}

	if(strncmp("Unidades de Medicion:",LineaLeida,21)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.UnidadesMedicion=Elemento;
			//CAlert::InformationAlert("MAR "+Pref.UnidadesMedicion);
			return(1);
		}
		
	}
/*if(strncmp("Nombre de Preferencia:",LineaLeida,22)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.Nombrepreferencias=Elemento;
		//CAlert::InformationAlert("Pref.Nombrepreferencias= "+Pref.Nombrepreferencias);//null
			return(1);
		}	
	}*/

if(strncmp("Nombre de Preferencia:",LineaLeida,22)==0)
		{ PMString copia=LineaLeida;
		  copia.Remove(22,copia.NumUTF16TextChars());
		  //CAlert::InformationAlert("MAR-MAR1 " +copia);
		   if(copia=="Nombre de Preferencia:")
			   {  copia=LineaLeida;
				  copia.Remove(0,23);
				  copia=InterlasaUtilities::RemoveCharactersReturn(copia);
				  Pref.Nombrepreferencias=copia;
               //CAlert::InformationAlert("MAR-MAR2 "+Pref.Nombrepreferencias);  
			   return(1);
		      }
       }

	if(strncmp("Tipo de Lectura en Coordenadas:",LineaLeida,31)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.TipoLecturaCoor=Elemento;
			return(1);
		}
		
	}

	
	if(strncmp("Orden Lectura de Coordenadas:",LineaLeida,29)==0)
	{			
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.OrdendeLectura=Elemento;
			return(1);
		}
		
	}
	
	

	
	if(strncmp("Unidades en Top:",LineaLeida,16)==0)
	{			
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.UnidadesEnTop=Elemento;
			return(1);
		}
		
	}

	if(strncmp("Unidades en Left:",LineaLeida,17)==0)
	{			
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.UnidadesEnLeft=Elemento;
			return(1);
		}
		
	}

	if(strncmp("Unidades en Buttom:",LineaLeida,19)==0)
	{			
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.UnidadesEnButtom=Elemento;
			return(1);
		}
		
	}

	if(strncmp("Unidades en Right:",LineaLeida,18)==0)
	{			
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.UnidadesEnRight=Elemento;
			return(1);
		}
		
	}
	
	
	
	if(strncmp("UtilizaPlantilleroPref:",LineaLeida,23)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
			{
				Pref.UtilizaPlantilleroPref=1;
			//	CAlert::InformationAlert("Es uno");
			}
			else
			{
				Pref.UtilizaPlantilleroPref=0;
			//	CAlert::InformationAlert("Es cero");
			}
			return(1);
		}
		
	}
	
	if(strncmp("Crear Capa Editorial:",LineaLeida,21)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
			{
				Pref.CapaEditorial=1;
			//	CAlert::InformationAlert("Es uno");
			}
			else
			{
				Pref.CapaEditorial=0;
			//	CAlert::InformationAlert("Es cero");
			}
			return(1);
		}
		
	}
	if(strncmp("Capa Editorial Visible:",LineaLeida,23)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaEditorialVisible=1;
			else
				Pref.CapaEditorialVisible=0;
			return(1);
		}
	}
	if(strncmp("Capa Editorial Bloqueada:",LineaLeida,25)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaEditorialBlocked=1;
			else
				Pref.CapaEditorialBlocked=0;
			return(1);
		}
	}
	if(strncmp("Crear Capa Imagen:",LineaLeida,18)==0)
	{	
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaImagen=1;
			else
				Pref.CapaImagen=0;
			return(1);
		}
	}
	if(strncmp("Capa Imagen Visible:",LineaLeida,20)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaImagenVisible=1;
			else
				Pref.CapaImagenVisible=0;

			return(1);
		}
	}
	if(strncmp("Capa Imagen Bloqueada:",LineaLeida,22)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
			{
				Pref.CapaImagenBlocked=1;
//				CAlert::InformationAlert("Es uno");
			}
			else
			{
				Pref.CapaImagenBlocked=0;
//				CAlert::InformationAlert("Es cero");
			}

			return(1);
		}
	}
	if(strncmp("Crear Capa Etiqueta con Publicidad:",LineaLeida,35)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaEtiquetaConPublicidad=1;
			else
				Pref.CapaEtiquetaConPublicidad=0;
			return(1);
		}
	}
	if(strncmp("Capa Etiqueta con Publicidad Visible:",LineaLeida,37)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaEtiquetaConPublicidadVisible=1;
			else
				Pref.CapaEtiquetaConPublicidadVisible=0;
			return(1);
		}
	}
	if(strncmp("Capa Etiqueta con Publicidad Bloqueada:",LineaLeida,39)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaEtiquetaConPublicidadBlocked=1;
			else
				Pref.CapaEtiquetaConPublicidadBlocked=0;
			return(1);
		}
	}
	
			/**/
	if(strncmp("Abrir Dialogo Editorial:",LineaLeida,24)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.AbrirDialogoIssue=1;
			else
				Pref.AbrirDialogoIssue=0;

			return(1);
		}
		
	}

	//if(strncmp("Fuente de Issue:",LineaLeida,16)==0)
	//{
	//	Elemento=strtok(LineaLeida,":\n");
	//	while(Elemento!=NULL)
	//	{
	//		Elemento=strtok(NULL,":\n");
	//		Pref.FuenteDeIssue=Elemento;
	//		return(1);
	//	}
	//}
	//if(strncmp("Tamaño Fuente de Issue:",LineaLeida,23)==0)
	//{
	//	Elemento=strtok(LineaLeida,":\n");
	//	while(Elemento!=NULL)
	//	{
	//		Elemento=strtok(NULL,":\n");
	//		Pref.TamFuenteIssue=Elemento;
	//		return(1);
	//	}
	//}
	
	if(strncmp("Nombre_Estilo:",LineaLeida,14)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.NombreEstiloIssue=Elemento;
			Pref.NombreEstiloIssue.Remove(0,1);
			return(1);
		}
		
	}

	if(strncmp("Nombre Issue:",LineaLeida,13)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.NombreEditorial=Elemento;
			return(1);
		}
		
	}

	if(PMCadena.Contains("Desplazamiento Hor. Issue:") )
		{
			PMCadena.Remove(0,26);
			Pref.DesplazamientoHorIssue = PMCadena.GetAsDouble();	
			return(1);
		}
		
	
	if(PMCadena.Contains("Desplazamiento Ver. Issue:") )
		{
			PMCadena.Remove(0,26);
			Pref.DesplazamientoVerIssue = PMCadena.GetAsDouble();	
			return(1);
		}
		
	

	if(strncmp("Crear Capa Etiqueta sin Publicidad:",LineaLeida,35)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaEtiquetaSinPublicidad=1;
			else
				Pref.CapaEtiquetaSinPublicidad=0;
			return(1);
		}
	}
	if(strncmp("Capa Etiqueta sin Publicidad Visible:",LineaLeida,37)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaEtiquetaSinPublicidadVisible=1;
			else
				Pref.CapaEtiquetaSinPublicidadVisible=0;
			return(1);
		}
	}
	if(strncmp("Capa Etiqueta sin Publicidad Bloqueada:",LineaLeida,39)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CapaEtiquetaSinPublicidadBlocked=1;
			else
				Pref.CapaEtiquetaSinPublicidadBlocked=0;
			return(1);
		}
	}
	if(strncmp("Color Fondo de Caja Imagen Con Publicidad:",LineaLeida,42)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.ColorCajaImagenConPublicidad=Elemento;
			return(1);
		}
	}
	if(strncmp("Color Margen de Caja Imagen Con Publicidad:",LineaLeida,43)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.ColorMargenCajaImagenConPublicidad=Elemento;
			return(1);
		}
	}
	if(strncmp("Ajuste de Imagen en Caja:",LineaLeida,25)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.FitCajaImagenConPublicidad=Elemento;
			return(1);
		}
	}
	if(strncmp("Color Fondo de Caja Imagen Sin Publicidad:",LineaLeida,42)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.ColorCajaImagenSinPublicidad=Elemento;
			return(1);
		}
	}
	if(strncmp("Color Margen de Caja Imagen Sin Publicidad:",LineaLeida,43)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.ColorMargenCajaImagenSinPublicidad=Elemento;
			return(1);
		}
	}
	if(strncmp("Orden de lectura de campos:",LineaLeida,27)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			return(2);//se deben escanear las siguientes cadenas para guardar la forma de lectura
		}
		
	}
	if(strncmp("Crear Etiqueta Con Publicidad:",LineaLeida,30)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CreaEtiquetaConPublicidad=1;
			else
				Pref.CreaEtiquetaConPublicidad=0;

			return(1);
		}
		
	}
	if(strncmp("Color de Fondo en Etiqueta Con Publicidad:",LineaLeida,42)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.ColorFondoEtiquetaConPublicidad=Elemento;
			//CAlert::InformationAlert("ColorFondoEtiquetaConPublicidad ="+ColorFondoEtiquetaConPublicidad);
			return(1);
		}
		
	}
	if(strncmp("Color de Texto en Etiqueta Con Publicidad:",LineaLeida,42)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.ColorTextEtiquetaConPublicidad=Elemento;
			return(1);
		}
		
	}
	if(strncmp("Tamaño de Margen en Etiqueta Con Publicidad:",LineaLeida,44)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.TamMargenEtiquetaConPublicidad=Elemento;
			return(1);
		}
		
	}
	
	if(strncmp("Crear Etiqueta Sin Publicidad:",LineaLeida,30)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.CreaEtiquetaSinPublicidad=1;
			else
				Pref.CreaEtiquetaSinPublicidad=0;
			return(1);
		}
		return(33);
	}
			 
	if(strncmp("BorarEtiquetaSDespuesDeActualizarImagen:",LineaLeida,40)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			if(strncmp(" 1",Elemento,2)==0)
				Pref.BorrarEtiquetaDespuesDeActualizar=1;
			else
				Pref.BorrarEtiquetaDespuesDeActualizar=0;
			return(1);
		}
		return(33);
	}
	
	
	
	if(strncmp("Color de Fondo en Etiqueta Sin Publicidad:",LineaLeida,42)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.ColorFondoEtiquetaSinPublicidad=Elemento;
			return(1);
		}
		return(34);
	}
	if(strncmp("Color de Texto en Etiqueta Sin Publicidad:",LineaLeida,42)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.ColorTextEtiquetaSinPublicidad=Elemento;
			return(1);
		}
		return(35);
	}
	if(strncmp("Tamaño de Margen en Etiqueta Sin Publicidad:",LineaLeida,44)==0)
	{
		Elemento=strtok(LineaLeida,":\n");
		while(Elemento!=NULL)
		{
			Elemento=strtok(NULL,":\n");
			Pref.TamMargenEtiquetaSinPublicidad=Elemento;
			return(1);
		}
		return(36);
	}
	
	if(strncmp("Campos en Etiqueta con Publicidad",LineaLeida,33)==0)
	{
		return(3);
	}
	
	if(strncmp("Campos en Etiqueta sin Publicidad",LineaLeida,33)==0)
	{
		return(4);
	}
	return(0);
}



void A2PPreferenciasFunciones::SeparaCampos(int tipo,CString Cadena,Preferencias &Pref)
{
	
	PMString PMCadena(Cadena);
	PMCadena.Remove(PMCadena.NumUTF16TextChars()-1,1);
	
	switch(tipo)
	{	
	case 2://Para los campos orden de lectura de los campos en archivo Export
		
		
		if(PMCadena.Contains("Campo1:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos1 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos1=PMCadena;
			
		}
		
		
		if(PMCadena.Contains("Campo2:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos2 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			
			Pref.NomCampos2=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo3:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos3 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos3=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo4:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos4 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos4=PMCadena;
			
			
			
		}
		
		if(PMCadena.Contains("Campo5:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos5 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos5=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo6:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos6 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos6 = PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo7:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos7 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos7 = PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo8:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos8 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos8 = PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo9:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos9 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos9 = PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo10:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos10 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos10 = PMCadena;
			
		}
		
		
		if(PMCadena.Contains("Campo11:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos11 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos11 = PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo12:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos12 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos12 = PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo13:") )
		{
			
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos13 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos13 = PMCadena;
			
		}
		
		
		if(PMCadena.Contains("Campo14:") )
		{
			
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			Pref.PosCampos14 = PMCadena.GetAsNumber();
			
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCampos14 = PMCadena;
			
		}
		
	break;

	case 3:///Campos en etiqueta con publicidad
		
		if(PMCadena.Contains("Campo1:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon1="";
			Pref.PosCamposEtiCon1.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon1=PMCadena;
			
		}
		
		
		if(PMCadena.Contains("Campo2:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon2="";
			Pref.PosCamposEtiCon2.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon2=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo3:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon3="";
			Pref.PosCamposEtiCon3.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon3=PMCadena;
			
		}
		if(PMCadena.Contains("Campo4:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon4="";
			Pref.PosCamposEtiCon4.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon4=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo5:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon5="";
			Pref.PosCamposEtiCon5.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon5=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo6:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon6="";
			Pref.PosCamposEtiCon6.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon6=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo7:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon7="";
			Pref.PosCamposEtiCon7.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon7=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo8:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon8="";
			Pref.PosCamposEtiCon8.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon8=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo9:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon9="";
			Pref.PosCamposEtiCon9.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon9=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo10:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon10="";
			Pref.PosCamposEtiCon10.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon10=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo11:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon11="";
			Pref.PosCamposEtiCon11.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon11=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo12:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon12="";
			Pref.PosCamposEtiCon12.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon12=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo13:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon13="";
			Pref.PosCamposEtiCon13.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			
			Pref.NomCamposEtiCon13=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo14:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiCon14="";
			Pref.PosCamposEtiCon14.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiCon14=PMCadena;
			
		}
		
		break;
	case 4:///Campos en etiqueta sin publicidad
	
	
		if(PMCadena.Contains("Campo1:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin1="";
			Pref.PosCamposEtiSin1.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin1=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo2:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin2="";
			Pref.PosCamposEtiSin2.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin2=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo3:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin3="";
			Pref.PosCamposEtiSin3.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin3=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo4:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin4="";
			Pref.PosCamposEtiSin4.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin4=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo5:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin5="";
			Pref.PosCamposEtiSin5.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin5=PMCadena;
			
		}
		if(PMCadena.Contains("Campo6:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin6="";
			Pref.PosCamposEtiSin6.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin6=PMCadena;
			
		}
		if(PMCadena.Contains("Campo7:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin7="";
			Pref.PosCamposEtiSin7.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin7=PMCadena;
			
		}
		if(PMCadena.Contains("Campo8:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin8="";
			Pref.PosCamposEtiSin8.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin8=PMCadena;
			
		}
		if(PMCadena.Contains("Campo9:") )
		{
			
			PMCadena.Remove(0,7);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin9="";
			Pref.PosCamposEtiSin9.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin9=PMCadena;
			
		}
		
		if(PMCadena.Contains("Campo10:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin10="";
			Pref.PosCamposEtiSin10.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin10=PMCadena;
			
		}
		if(PMCadena.Contains("Campo11:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin11="";
			Pref.PosCamposEtiSin11.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin11=PMCadena;
			
		}
		if(PMCadena.Contains("Campo12:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin12="";
			Pref.PosCamposEtiSin12.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin12=PMCadena;
			
		}
		if(PMCadena.Contains("Campo13:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin13="";
			Pref.PosCamposEtiSin13.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin13=PMCadena;
			
		}
		if(PMCadena.Contains("Campo14:") )
		{
			
			PMCadena.Remove(0,8);
			
			int32 index = PMCadena.IndexOfWChar(9);
			
			int32 pos = PMCadena.GetAsNumber();
			Pref.PosCamposEtiSin14="";
			Pref.PosCamposEtiSin14.AppendNumber(pos);
	
			PMCadena.Remove(0,index+1);
			PMCadena.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
			Pref.NomCamposEtiSin14=PMCadena;
			
		}
	
		break;
	}

}

bool16 A2PPreferenciasFunciones::OpenDialogPassword()
{
	bool16 retval=kFalse;

/*	do
	{
		//Obtengo la interfaz IK2ServiceRegistry de la actual sesion
		InterfacePtr<IK2ServiceRegistry> sRegistry(gSession, UseDefaultIID());
		if (sRegistry == nil)
		{	
			//*si no se encontro ningun  servicio registrado sale de la funcion y realiza nada
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: sRegistry invalid");
			break;
		}
		//obtengo un classID del jefe que se creo previamente en el archivo SelDlg.fr
		ClassID	classID=kA2PPrefKeyLicenseDialogBoss;
		//busco una interfaz IK2ServiceProvider a partir del registro de Servicio el cual busca la implementacion y su calssID
		InterfacePtr<IK2ServiceProvider> 
		sdService(sRegistry->QueryServiceProviderByClassID(kSelectableDialogServiceImpl, classID));
		if (sdService == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: sdService invalid");
			break;
		}

		// This is an interface to our service, which will end up calling our 
		// plug-in's dialog creator implementation:
		//*Esto es un interfaz a nuestro servicio, que terminará encima de 
		//la llamada a la implementacion del creador del diálogo de nuestro plug-in: 
		InterfacePtr<IDialogCreator> dialogCreator(sdService, IID_IDIALOGCREATOR);
		if (dialogCreator == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogCreator invalid");
			break;
		}
			
		// This is our CreateDialog, which loads the dialog from our resource:
		//Este es nuestro creador de dialogos, que cargara el dialogo de nuestro recurso
		IDialog* dialog = dialogCreator->CreateDialog();
		if (dialog == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: could not create dialog");
			break;
		}

		// The panel data will be the selectable panel of the dialog:
		//los datos del panel sea el panel seleccionable del diálogo
		InterfacePtr<IPanelControlData> panelData(dialog, UseDefaultIID());
		if (panelData == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		//Busqueda del panel:
		WidgetID	widgetID = kA2PPrefKeyLicenseDlgWidgetID;
		
		
		IControlView* dialogView = panelData->FindWidget(widgetID);
		if (dialogView == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogView invalid");
			break;
		}
			
		// The dialog switcher controls how the panels are switched between:
		//los controles del switcher del diálogo cómo los paneles se cambian en medio: 
		InterfacePtr<ISelectableDialogSwitcher> 
		dialogSwitcher(dialogView, IID_ISELECTABLEDIALOGSWITCHER);
		if (dialogSwitcher == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogSwitcher invalid");
			break;
		}
		
		dialogSwitcher->SetDialogServiceID(kA2PPrefSelDlgService);

		//Abre el dialogo
		dialog->Open(); 
		retval=kTrue;
	 }while(false);	*/
	
	//Abrir Dialogo de Password
	do
	{
		
			// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(
			nLocale,					// Locale index from PMLocaleIDs.h. 
			kA2PPrefPluginID,			// Our Plug-in ID  
			kViewRsrcType,				// This is the kViewRsrcType.
			kA2PPrefUserUIPwdDialogResourceID,	// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) 
		{
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		retval=kTrue;
		
	 }while(false);
	return retval;
}


//PMString A2PPreferenciasFunciones::ProporcionaFechaArchivo(CString Archivo)  //ORIGINAL NUEVO
 PMString A2PPreferenciasFunciones::ProporcionaFechaArchivo(PMString Archivo)
{
	PMString DateFileStr="";
	IDFile savedFile = SDKUtilities::PMStringToSysFile(&Archivo);
	
	uint32 modDate;
	if (FileUtils::GetModificationDate(savedFile, &modDate))
	{
		DateFileStr="";
		DateFileStr.AppendNumber(modDate);
	}
	
	/*CAlert::InformationAlert("CHINGADO");
	
	PMString TimeFileStr="";
	uint64 size=0;
	uint64 time=0;

	char t[ 100 ] = "";
	struct stat b;
	if (!stat(Archivo.GrabCString(), &b)) 
	{
		strftime(t, 100, "%d/%m/%Y %H:%M:%S", localtime( &b.st_mtime));
		DateFileStr.Append(t, 100);
		//printf("\nLast modified date and time = %s\n", t);
	}*/
	/*PMString FilePath(Archivo);
	InterfacePtr<ICoreFilename> cfn((ICoreFilename*)::CreateObject(kCoreFilenameBoss,IID_ICOREFILENAME));
	IDFile file=SDKUtilities::PMStringToSysFile(&FilePath);
	if(cfn->Initialize(&file)==0)
	{
		
		IDataLink *link=nil;
		cfn->GetFileInfo(&size,&time,link);
	
		GlobalTime  GlTimeFile(time);
		
		int32 yyyy=0;
		int32 mmm=0;
		int32 ddd=0;
		int32 hh=0;
		int32 mm=0;
		int32 ss=0;
					
		GlTimeFile.GetTime(&yyyy , &mmm, &ddd, &hh, &mm, &ss);
		DateFileStr="";
		DateFileStr.AppendNumber(yyyy);
		DateFileStr.Append("/");
		DateFileStr.AppendNumber(mmm);
		DateFileStr.Append("/");
		DateFileStr.AppendNumber(ddd);
		DateFileStr.Append("/");
		DateFileStr.AppendNumber(hh);
		DateFileStr.Append("/");
		DateFileStr.AppendNumber(mm);
		DateFileStr.Append("/");
		DateFileStr.AppendNumber(ss);
		DateFileStr.Append("/");
		
	}
	*/
	return(DateFileStr);

}




/****************************************************************/
////////////	 PARA ALLWORKING	/////////////////////////////
/**************************************************************/
bool16 A2PPreferenciasFunciones::VerificaSeguridadPirateria()
{  //CAlert::InformationAlert("entro VerificaSeguridadPirateria()");

#if defined(MACINTOSH)
	bool16 retval=kFalse;
	FILE *stream;
	FILE *stream2;
	do
	{
		PMString Default_Prefer_Path;
		SDKUtilities::GetApplicationFolder(Default_Prefer_Path);
		Default_Prefer_Path.Append(":Ase2gurPty.segurity");
		

		
		if( (stream  =FileUtils::OpenFile(Default_Prefer_Path.GrabCString(),"r"))!=NULL)
   		{
   			PMString NameVol="";
   			IDFile file=SDKUtilities::PMStringToSysFile(&Default_Prefer_Path);
   			FileUtils::GetVolumeName(&file,&NameVol);
   			NameVol.Append(":Library:Preferences:com.apple.userpreferences.plist");
   			fclose(stream);
			//

			#if defined(MACINTOSH)
				if((stream2  =FileUtils::OpenFile(NameVol.GrabCString(),"r"))!=NULL)
			#elif defined(WINDOWS)
				Default_Prefer_Path=InterlasaUtilities::MacToUnix(Default_Prefer_Path);
				if((stream2  =fopen(NameVol.GrabCString(),"r"))!=NULL)
			#endif
   			
   			{
   				//Existe el archivo de seguridad
   				retval=kTrue;
   				fclose(stream2);
   			}
   			else
   			{
   				//no existe el archivo de seguridad
   				if(A2PPreferenciasFunciones::openDialogForContrato())
   					A2PPreferenciasFunciones::openDialogForSecurity();
   			}
   			
   		}
   		else
   		{
   			//no existe el archivo de seguridad
   			if(A2PPreferenciasFunciones::openDialogForContrato())
   				A2PPreferenciasFunciones::openDialogForSecurity();
   		}
	}while(false);
	return(retval);
#elif defined(WINDOWS)
	bool16 retval=kFalse;
FILE *stream;
	do
	{  //CAlert::InformationAlert("entro VerificaSeguridadPirateria() para windows");

		InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
			(
				kA2PPrefRegEditUtilitiesBoss,	// Object boss/class
				IRegEditUtilities::kDefaultIID
			)));
	   
		 if(ConsultaRegEdit==nil)
		    {CAlert::InformationAlert("ConsultaRegEdit==nil en proyecto preferencias");break;}
        		
		PMString Default_Prefer_Path=ConsultaRegEdit->QueryKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), TEXT("SystemRoot"));
		Default_Prefer_Path.Append("\\System32\\");
        
		//CAlert::InformationAlert("ConsultaRegEdit en VerificarPirateria  "+Default_Prefer_Path);

	Default_Prefer_Path.Append("Ase2gurPty.segurity");//Para AllWorking
		if( (stream  =fopen(Default_Prefer_Path.GrabCString(),"r"))!=NULL )//cfn->fileopen(&mode))!=NULL)// 
 {if( ConsultaRegEdit->ExistKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\RAM2HPAC\\Drive32\\Driv\\ASec2urityPCS230"), TEXT("ASec2urityPCS230")))
   			{
   				//Existe el archivo de seguridad
				//CAlert::InformationAlert("Existe el archivo de seguridad");
   				retval=kTrue;
   			}
   			else
   			{
   			    //CAlert::InformationAlert("Existe el archivo de seguridad de lo contrario");
				if(this->openDialogForContrato())
   					this->openDialogForSecurity();
   			}

   		}
   		else
   		{ //CAlert::InformationAlert("Existe el archivo de seguridad de lo contrario de lo contrario");
			if(this->openDialogForContrato())
   			  this->openDialogForSecurity();
   		}
	}while(false);
	return(retval);
#endif
}


/****************************************************************/
////////////	 PARA DEMO	/////////////////////////////
/***************************************************************/
/*bool16 A2PPreferenciasFunciones::VerificaSeguridadPirateria()
{

bool16 retval=kFalse;
FILE *stream;
FILE *stream2;
FILE *ArcPass;
	do
	{
	
		PMString Default_Prefer_Path;
		SDKUtilities::GetApplicationFolder(Default_Prefer_Path);
		Default_Prefer_Path.Append(":Ase2gurPtyDemo30.segurity");
		if( (stream  =fopen(Default_Prefer_Path.GrabCString(),"r"))!=NULL)//cfn->fileopen(&mode))!=NULL)// 
   		{
   			PMString NameVol="";
   			IDFile file=SDKUtilities::PMStringToSysFile(&Default_Prefer_Path);
   			FileUtils::GetVolumeName(&file,&NameVol);
   			NameVol.Append(":Library:Preferences:com.apple.userpreferencesDemo.plist");
   			fclose(stream);
   			if((stream2  =fopen(NameVol.GrabCString(),"r"))!=NULL)
   			{
   				//Existe el archivo de seguridad
   				retval=kTrue;
   				fclose(stream2);
   			}
   			else
   			{
   				//no existe el archivo de seguridad
   				PMString WarningDemo=kA2PPrefWarningPlgDemoStringKey;
				WarningDemo.Translate();
				WarningDemo.SetTranslatable(kFalse);
				CAlert::WarningAlert(WarningDemo);
   				if(A2PPreferenciasFunciones::openDialogForContrato())
   				{
   				
   					PMString Default_Prefer_Path;
					SDKUtilities::GetApplicationFolder(Default_Prefer_Path);
					Default_Prefer_Path.Append(":Ase2gurPtyDemo30.segurity");
					if( (ArcPass  =fopen(Default_Prefer_Path.GrabCString(),"w"))!=NULL)//cfn->fileopen(&mode))!=NULL)// 
   					{
   						//Existe el archivo de seguridad
   						fprintf(ArcPass,"%s\n",	"Demo");
						fclose(ArcPass);
			
						FILE *ArcPass2;
						PMString NameVol="";
   						IDFile file=SDKUtilities::PMStringToSysFile(&Default_Prefer_Path);
   			
   						FileUtils::GetVolumeName(&file,&NameVol);
   			
   						NameVol.Append(":Library:Preferences:com.apple.userpreferencesDemo.plist");
   			
   						if( (ArcPass2  = fopen(NameVol.GrabCString(),"w"))!=NULL)//cfn->fileopen(&mode))!=NULL)// 
   						{
   							fprintf(ArcPass2,"%s\n",	"Demo");
							fclose(ArcPass2);
   						}
   					}
   					else
   					{
   						CAlert::InformationAlert(kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
   					}	
   				}
   			}		
   		}
   		else
   		{
   			//no existe el archivo de seguridad
   			PMString WarningDemo=kA2PPrefWarningPlgDemoStringKey;
			WarningDemo.Translate();
			WarningDemo.SetTranslatable(kFalse);
			CAlert::WarningAlert(WarningDemo);
   			if(A2PPreferenciasFunciones::openDialogForContrato())
   			{		
   				PMString Default_Prefer_Path;
				SDKUtilities::GetApplicationFolder(Default_Prefer_Path);
				Default_Prefer_Path.Append(":Ase2gurPtyDemo30.segurity");
				if((ArcPass  =fopen(Default_Prefer_Path.GrabCString(),"w"))!=NULL)//cfn->fileopen(&mode))!=NULL)// 
   				{
   					//Existe el archivo de seguridad
   					fprintf(ArcPass,"%f\n",	"Demo");
					fclose(ArcPass);
					FILE *ArcPass2;
					PMString NameVol="";
   					IDFile file=SDKUtilities::PMStringToSysFile(&Default_Prefer_Path);
   					FileUtils::GetVolumeName(&file,&NameVol);
   			
   					NameVol.Append(":Library:Preferences:com.apple.userpreferencesDemo.plist");
   			
   					if( (ArcPass2  = fopen(NameVol.GrabCString(),"w"))!=NULL)//cfn->fileopen(&mode))!=NULL)// 
   					{
   						fprintf(ArcPass2,"%f\n",	"Demo");
						fclose(ArcPass2);
   					}
   				}
   				else
   				{
   					CAlert::InformationAlert(kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
   				}
   			}
   		}
	}while(false);
	return(retval);
}*/


/****************************************************************/
////////////	ALLWORKING UTILIZANDO DONGLE   ///////////////////
/**************************************************************
bool16  A2PPreferenciasFunciones::VerificaSeguridadPirateria() 
{



	
	bool16 retval=kFalse;
	SP_UPRO_APIPACKET     ApiPack;               
  	SPP_UPRO_APIPACKET    ApiPacket = &ApiPack;
  	unsigned int          devID = 0x7571;
  	SP_DWORD 			Value=0;
  	SP_DWORD			toolKitCellAddress=0x20008;
  	SP_STATUS             status;
  	
  	do
  	{
  		status = SFNTsntlInitialize( ApiPacket );
  		if( status != SP_ERR_SUCCESS && status!=SP_ERR_PACKET_ALREADY_INITIALIZED)
  		{
   			CAlert::InformationAlert(kA2PPrefNoSeEncontroLlaveStringKey);
    		break;
  		}
  		
  		  //* Get the license to run the application 
  		status = SFNTsntlGetLicense( ApiPacket,
                                 devID,
                                 DESIGNID, //* DesignID specified in upromepsdesign.h
                                 0);  
                                 
     	if( status != SP_ERR_SUCCESS)
 		{
   			CAlert::InformationAlert(kA2PPrefLlaveIncorrectaStringKey );
   			break;
 		} 
 		
 		status= SFNTsntlReadValue(ApiPacket, toolKitCellAddress, &Value);
 		
 		if( status != SP_ERR_SUCCESS)
 		{
   			CAlert::InformationAlert(kA2PPrefLlaveIncorrectaStringKey );
   			break;
 		} 
 		
 		if(Value==471296)
 		{
 			retval=kTrue;
 			//CAlert::InformationAlert("OK" );
 		}
 		else
 		{
 			CAlert::InformationAlert(kA2PPrefLlaveIncorrectaStringKey );
 		}
  	}while(false);
  return(retval);

}

/****************************************************************/
////////////	ALLWORKING UTILIZANDO DONGLE RED  ///////////////////
/*************************************************************
bool16  A2PPreferenciasFunciones::VerificaSeguridadPirateria() 
{
	bool16 retval=kFalse;
	do
  	{
		InterfacePtr<IInterUltraProKy> UltraProCheck(static_cast<IInterUltraProKy*>(CreateObject(
		kInterUltraProKyBoss,
		IInterUltraProKy::kDefaultIID)));
		
		PMString ResultString="";
		if(UltraProCheck!=nil)
		{	
			retval= UltraProCheck->CheckDongleN2PPlugins(ResultString);//RedValueN2PPlugin(ResultString);// 
			if(retval==kFalse)
			{
				CAlert::InformationAlert(ResultString);
				break;
			}
			else
			{
				retval=kTrue;
			}
		}
 		else
 		{
 			CAlert::InformationAlert(kA2PPrefLlaveIncorrectaStringKey );
 		}
		InterfacePtr<IInterUltraObjectProKy> UltraProCheck(static_cast<IInterUltraObjectProKy*>(CreateObject(
		kInterUltraObjetProKyBoss,
		IInterUltraObjectProKy::kDefaultIID)));
		
		PMString ResultString="";
		if(UltraProCheck!=nil)
		{	
			retval= UltraProCheck->RedValueA2PPlugin(ResultString);//RedValueN2PPlugin(ResultString);// 
			if(retval==kFalse)
			{
				CAlert::InformationAlert(ResultString);
				break;
			}
			else
			{
				retval=kTrue;
			}
		}
 		else
 		{
 			CAlert::InformationAlert(kA2PPrefLlaveIncorrectaStringKey );
 		}
  	}while(false);
  return(kTrue);

}*/



bool16 A2PPreferenciasFunciones::openDialogForContrato()
{
	bool16 retval=kFalse;
	do
		{
			// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) 
		{	
			CAlert::InformationAlert("openDialogForContrato application");
			break;
		}
		
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) 
		{
			CAlert::InformationAlert("openDialogForContrato dialogMgr");
			break;
		}
		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpec
		(	nLocale,					// Locale index from PMLocaleIDs.h.
			kA2PPrefPluginID,			// Our Plug-in ID 
			kViewRsrcType,				// This is the kViewRsrcType.
			kA2PPrefContratoDialogResourceID,// Resource ID for our dialog.
			kTrue						// Initially visible.
		);

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpec, IDialog::kMovableModal);
		
		IControlView *CVDialog=dialog->GetDialogPanel();
			
		InterfacePtr<IDialogController> dialogController(CVDialog,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			CAlert::InformationAlert("openDialogForContrato dialogController");
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		
		ASSERT(dialog);///Developer/SDKs/MacOSX10.4u.sdk
		if (dialog == nil) 
		{
			CAlert::InformationAlert("openDialogForContrato dialog");
			break;
		}

		InterfacePtr<IPanelControlData> panelData(dialog, UseDefaultIID());
		if (panelData == nil)
		{
			CAlert::InformationAlert("openDialogForContrato panelData");
			ASSERT_FAIL("C2PWidgetObserver::OpenDialogFecha panelData invalid");
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		dialog->WaitForDialog();
		PMString Option=dialogController->GetTextControlData(kA2PPrefTextKeyIDWidgetID);
		if(Option!="kasaka" || !Option.Contains("kasaka"))
			retval=kTrue;
		}while(false);	
	return(retval);
}

bool16 A2PPreferenciasFunciones::openDialogForSecurity()
{
	bool16 retval=kFalse;
	do
		{
			//Obtengo la interfaz IK2ServiceRegistry de la actual sesion
			InterfacePtr<IK2ServiceRegistry> sRegistry(GetExecutionContextSession(), UseDefaultIID());
			if (sRegistry == nil)
			{	
				//*si no se encontro ningun  servicio registrado sale de la funcion y realiza nada
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: sRegistry invalid");
				break;
			}
			//obtengo un classID del jefe que se creo previamente en el archivo SelDlg.fr
			ClassID	classID=kA2PPrefKeyLicenseDialogBoss;

			//busco una interfaz IK2ServiceProvider a partir del registro de Servicio el cual busca la implementacion y su calssID
			InterfacePtr<IK2ServiceProvider> 
			sdService(sRegistry->QueryServiceProviderByClassID(kSelectableDialogServiceImpl, classID));
			if (sdService == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: sdService invalid");
				break;
			}

			// This is an interface to our service, which will end up calling our 
			// plug-in's dialog creator implementation:
			//*Esto es un interfaz a nuestro servicio, que terminará encima de 
			//la llamada a la implementacion del creador del diálogo de nuestro plug-in: 
			InterfacePtr<IDialogCreator> dialogCreator(sdService, IID_IDIALOGCREATOR);
			if (dialogCreator == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogCreator invalid");
				break;
			}
			
			// This is our CreateDialog, which loads the dialog from our resource:
			//Este es nuestro creador de dialogos, que cargara el dialogo de nuestro recurso
			IDialog* dialog = dialogCreator->CreateDialog();
			if (dialog == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: could not create dialog");
				break;
			}

			// The panel data will be the selectable panel of the dialog:
			//los datos del panel sea el panel seleccionable del diálogo
			InterfacePtr<IPanelControlData> panelData(dialog, UseDefaultIID());
			if (panelData == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
				break;
			}
			//Busqueda del panel:
			WidgetID	widgetID = kA2PPrefKeyLicenseDlgWidgetID;
			
			
			IControlView* dialogView = panelData->FindWidget(widgetID);
			if (dialogView == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogView invalid");
				break;
			}
			
			// The dialog switcher controls how the panels are switched between:
			//los controles del switcher del diálogo cómo los paneles se cambian en medio: 
			InterfacePtr<ISelectableDialogSwitcher> 
				dialogSwitcher(dialogView, IID_ISELECTABLEDIALOGSWITCHER);
			if (dialogSwitcher == nil)
			{
				ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogSwitcher invalid");
				break;
			}
		
			dialogSwitcher->SetDialogServiceID(kA2PPrefSelDlgService);

			//Abre el dialogo
			dialog->Open(); 
			retval=kTrue;
		}while(false);	
	return(retval);
}

/*bool16 A2PPreferenciasFunciones::ConvertirUnidadesdePreferenciasAPuntos(Preferencias &Pref)
{
	do
	{
		PMReal tempPMReal=0.0;
		tempPMReal=this->ConversionUnidadesAPuntos(Pref.AltoPag.GetAsDouble());
		Pref.AltoPag.Clear();
		Pref.AltoPag.AppendNumber(tempPMReal);
		
		tempPMReal=this->ConversionUnidadesAPuntos(Pref.AnchoPagina.GetAsDouble());
		Pref.AnchoPagina.Clear();
		Pref.AnchoPagina.AppendNumber(tempPMReal);

		

		tempPMReal=this->ConversionUnidadesAPuntos(Pref.MargenExterior.GetAsDouble());
		Pref.MargenExterior.Clear();
		Pref.MargenExterior.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionUnidadesAPuntos(Pref.MargenInferior.GetAsDouble());
		Pref.MargenInferior.Clear();
		Pref.MargenInferior.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionUnidadesAPuntos(Pref.MargenInterior.GetAsDouble());
		Pref.MargenInterior.Clear();
		Pref.MargenInterior.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionUnidadesAPuntos(Pref.MargenSuperior.GetAsDouble());
		Pref.MargenSuperior.Clear();
		Pref.MargenSuperior.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionUnidadesAPuntos(Pref.Medianil.GetAsDouble());
		Pref.Medianil.Clear();
		Pref.Medianil.AppendNumber(tempPMReal);
		
		PMReal tempPMReal=this->ConversionUnidadesAPuntos(Pref.DesplazamientoHorIssue.GetAsDouble());
		Pref.DesplazamientoHorIssue.Clear();
		Pref.DesplazamientoHorIssue.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionUnidadesAPuntos(Pref.DesplazamientoVerIssue.GetAsDouble());
		Pref.DesplazamientoVerIssue.Clear();
		Pref.DesplazamientoVerIssue.AppendNumber(tempPMReal);

	}while(false);
	return(kTrue);
}
*/
PMReal A2PPreferenciasFunciones::ConversionUnidadesAPuntos(PMReal Cantidad)
{	
	do
	{
		//IDocument* document = ::GetFrontDocument();
		//obtiene la interfaz IUnitOfMeasureSettings de las preferencias del documento (kGetFrontmostPrefs)
		InterfacePtr<IUnitOfMeasureSettings>	iUOMPref((IUnitOfMeasureSettings*)
			::QueryPreferences(IID_IUNITOFMEASURESETTINGS, GetExecutionContextSession()));
		if (iUOMPref == nil)
		{
			break;
		}
		// Get the measurement system interface from the session boss
		InterfacePtr<IMeasurementSystem>	iMeasureSystem(GetExecutionContextSession(), UseDefaultIID());
		if (iMeasureSystem == nil)
		{
			break;
		}
		// Get the XUnitOfMeasure index
		int16 unitIndex = iUOMPref->GetXUnitOfMeasureIndex();
		// Use the index to get the unit of measurement
		InterfacePtr<IUnitOfMeasure> iUOM(iMeasureSystem->QueryUnitOfMeasure(unitIndex));
		if (iUOM == nil)
		{
			break;
		}
		// Converting 1 unit of this unit of measurement to points
		PMReal pointValue = iUOM->UnitsToPoints(Cantidad);
		return(pointValue);
	}while(false);
	return(0);
}


PMReal A2PPreferenciasFunciones::ConversionPuntosAUnidades(PMReal Cantidad)
{	
	do
	{
		//IDocument* document = ::GetFrontDocument();
		//obtiene la interfaz IUnitOfMeasureSettings de las preferencias del documento (kGetFrontmostPrefs)
		InterfacePtr<IUnitOfMeasureSettings>	iUOMPref((IUnitOfMeasureSettings*)
			::QueryPreferences(IID_IUNITOFMEASURESETTINGS, GetExecutionContextSession()));
		if (iUOMPref == nil)
		{
			break;
		}
		// Get the measurement system interface from the session boss
		InterfacePtr<IMeasurementSystem>	iMeasureSystem(GetExecutionContextSession(), UseDefaultIID());
		if (iMeasureSystem == nil)
		{
			break;
		}
		// Get the XUnitOfMeasure index
		int16 unitIndex = iUOMPref->GetXUnitOfMeasureIndex();
		// Use the index to get the unit of measurement
		InterfacePtr<IUnitOfMeasure> iUOM(iMeasureSystem->QueryUnitOfMeasure(unitIndex));
		if (iUOM == nil)
		{
			break;
		}
		// Converting 1 unit of this unit of measurement to points
		PMReal pointValue = iUOM->PointsToUnits(Cantidad);
		return(pointValue);
	}while(false);
	return(0);
}


/*bool16 A2PPreferenciasFunciones::ConvertirPuntosAUnidadesdePreferencias(Preferencias &Pref)
{
	do
	{
		PMReal tempPMReal=0.0;
		tempPMReal=this->ConversionPuntosAUnidades(Pref.AltoPag.GetAsDouble());
		Pref.AltoPag.Clear();
		Pref.AltoPag.AppendNumber(tempPMReal);
		
		tempPMReal=this->ConversionPuntosAUnidades(Pref.AnchoPagina.GetAsDouble());
		Pref.AnchoPagina.Clear();
		Pref.AnchoPagina.AppendNumber(tempPMReal);

		

		tempPMReal=this->ConversionPuntosAUnidades(Pref.MargenExterior.GetAsDouble());
		Pref.MargenExterior.Clear();
		Pref.MargenExterior.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionPuntosAUnidades(Pref.MargenInferior.GetAsDouble());
		Pref.MargenInferior.Clear();
		Pref.MargenInferior.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionPuntosAUnidades(Pref.MargenInterior.GetAsDouble());
		Pref.MargenInterior.Clear();
		Pref.MargenInterior.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionPuntosAUnidades(Pref.MargenSuperior.GetAsDouble());
		Pref.MargenSuperior.Clear();
		Pref.MargenSuperior.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionPuntosAUnidades(Pref.Medianil.GetAsDouble());
		Pref.Medianil.Clear();
		Pref.Medianil.AppendNumber(tempPMReal);
		
		PMReal tempPMReal=Pref.DesplazamientoHorIssuethis->ConversionPuntosAUnidades();
		Pref.DesplazamientoHorIssue.Clear();
		Pref.DesplazamientoHorIssue.AppendNumber(tempPMReal);

		tempPMReal=this->ConversionPuntosAUnidades(Pref.DesplazamientoVerIssue.GetAsDouble());
		Pref.DesplazamientoVerIssue.Clear();
		Pref.DesplazamientoVerIssue.AppendNumber(tempPMReal);
	}while(false);
	return(kTrue);
}*/