/*
//
*/

#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"
// Interface includes:
// none.

// General includes:
#include "CDialogController.h"

// Project includes:

#include "A2PPrefID.h"
#include "PMString.h"
#include <stdio.h>
#include <string.h>
//#include <io.h>
//#include "fstream.h"
//#include <iomanip.h>
#include "CAlert.h"
//#include "SelDlgActionComponent.cpp"
//////////////////////////
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialog.h"
#include "IDialogCreator.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "ISession.h"
#include "CAlert.h"
#include "SysFileList.h"
#include "IOpenFileDialog.h"
#include "ITextControlData.h"
#include <stdlib.h>
// General includes:
#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "FileUtils.h"

#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
#endif
#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"
#endif
//#include "InterlasaRegEditUtilities.h"
//#include "A2PPrefEncrypt.h"//NO UTILIZADO




/** 
*/
class A2PPrefUserUIPwdDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		A2PPrefUserUIPwdDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~A2PPrefUserUIPwdDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext *myContext);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext *myContext);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId);

		private:
		/**
			Funcion que abre el dialogo de preferencias
		*/
		void Abrir_Preferencias();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(A2PPrefUserUIPwdDialogController, kA2PPrefUserUIPwdDialogControllerImpl)
/* InitializeDialogFields
*/
void A2PPrefUserUIPwdDialogController::InitializeDialogFields(IActiveContext *myContext) 
{	/***************AL ABRIR LA FORMA DE LA LISTA*********/
	
	SetTextControlData(kA2PPrefUserUIUsernameWidgetID,"");
	SelectDialogWidget(kA2PPrefUserUIUsernameWidgetID);
}

/* ValidateDialogFields
*/
/************ACOMODA O PONE LAS PREFERENCIAS SOBRE LAS CAJAS ETC....********/
WidgetID A2PPrefUserUIPwdDialogController::ValidateDialogFields(IActiveContext *myContext) 
{
	WidgetID result = kNoInvalidWidgets;
	FILE *ArcPass;
	PMString Default_Prefer_Path;
	SDKUtilities::GetApplicationFolder(Default_Prefer_Path);
	Default_Prefer_Path.Append(":PAse2WgurPtyD.segurity");
	
	PMString password="";
	PMString passwordArch="";
	password = GetTextControlData(kA2PPrefUserUIUsernameWidgetID);
	
	//Default_Prefer_Path=InterlasaUtilities::MacToUnix(Default_Prefer_Path);
		if( (ArcPass  = FileUtils::OpenFile(Default_Prefer_Path.GrabCString(),"r"))!=NULL)
		{	
			char c;
			
			int n=0;
			
			 do
			 {
    			c = fgetc (ArcPass);
     			if (c != EOF) 
     			{
     				n++;
     				passwordArch.Append(c);
     			}
     			 
  			 } while (c != EOF);
      		
  
			if(passwordArch==password )
			{
				this->Abrir_Preferencias();
			}
			else
			{
				CAlert::ErrorAlert(kA2PPrefPasswordIncorrectoStringKey);
				result = kA2PPrefUserUIUsernameWidgetID;
			}
			fclose(ArcPass);
		}
		else
		{
			//no exsite para lectura primera vez que se crea el password
			//Default_Prefer_Path=InterlasaUtilities::MacToUnix(Default_Prefer_Path);
			if( (ArcPass  = FileUtils::OpenFile(Default_Prefer_Path.GrabCString(),"w"))!=NULL)
			{
				fprintf(ArcPass,"%s",password.GrabCString());
				fclose(ArcPass);
				this->Abrir_Preferencias();
			}
			else
			{//no se pudo crear para escritura
			
			}
		
		}
	
/*	//	PARA WINDOWS
	WidgetID result = kNoInvalidWidgets;

	//std::ofstream salida;
	PMString Password;

	

	InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
			(
				kRegEditUtilitiesBoss,	// Object boss/class
				IRegEditUtilities::kDefaultIID
			)));

	if(ConsultaRegEdit->ExistKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\RAM2HPAC\\Drive32\\Driv\\A2P20CSPwd"), TEXT("A2Ppwd"))==kTrue)
	{
		//existe llave
		Password=this->GetTextControlData(kA2PPrefUserUIUsernameWidgetID);//se obtiene el password tecleado sobre el dialogo
		if(Password.ObsoleteLength()==0)
			Password="#";
		PMString PwdRegEdit="";
		PwdRegEdit.Append(ConsultaRegEdit->QueryKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\RAM2HPAC\\Drive32\\Driv\\A2P20CSPwd"), TEXT("A2Ppwd")));
		if(Password==PwdRegEdit)
		{
			this->Abrir_Preferencias();//se abre el Dialogo preferencias
		}
		else
		{
			CAlert::InformationAlert(kPasswordIncorrectoStringKey);
			result=kA2PPrefUserUIUsernameWidgetID;
		}
	}
	else
	{//no existe llave
		Password=this->GetTextControlData(kA2PPrefUserUIUsernameWidgetID);//se obtiene el password tecleado sobre el dialogo
		if(Password.ObsoleteLength()==0)
			Password="#";
		ConsultaRegEdit->CreateKey(TEXT("SOFTWARE\\Microsoft\\RAM2HPAC\\Drive32\\Driv\\A2P20CSPwd"), TEXT("A2Ppwd"), Password.GrabTString());//se crea la copia del password
		//CAlert::InformationAlert("1");
		this->Abrir_Preferencias();//se abre el Dialogo preferencias
		//CAlert::InformationAlert("2");
	}*/
	
	return(result);
}

/* ApplyDialogFields
*/
void A2PPrefUserUIPwdDialogController::ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId) 
{	 
	SystemBeep();  
}




void A2PPrefUserUIPwdDialogController::Abrir_Preferencias()
{		
		do
	{
		// Obtiene los servicios registrados, incluyendo nuestro servicio del dialogo selectable: 
		// esto es con el fin de que existan servicio registrados
		
		InterfacePtr<IK2ServiceRegistry> sRegistry(GetExecutionContextSession(), UseDefaultIID());
		if (sRegistry == nil)
		{	//si no se encontro ningun  servicio registrado sale de la funcion y realiza nada
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: sRegistry invalid");
			break;
		}

		
		//	El servicio es proveido basandose sobre el ID del Servicio y en el jefe pasado 
		//creo una implementacion del typo classID
		//if (isTabStyle)
		//{	//Se le asigna a la clase de identificador el ID del jefe que se refiere a nuestra pantalla
		//de pestaÒas
		ClassID	classID= kA2PPrefTabDialogBoss;
		//}
				

		//InterfacePtr es un constructor que devuelve un servicio
		InterfacePtr<IK2ServiceProvider> 
		sdService(sRegistry->QueryServiceProviderByClassID(kSelectableDialogServiceImpl, classID));
						
		if (sdService == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: sdService invalid");
			break;
		}

		// This is an interface to our service, which will end up calling our 
		// plug-in's dialog creator implementation:
		//Esto es un interfaz a nuestro servicio, que terminar· encima de 
		//la llamada a la implementacion del creador del di·logo de nuestro plug-in: 
		InterfacePtr<IDialogCreator> dialogCreator(sdService, IID_IDIALOGCREATOR);
		if (dialogCreator == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogCreator invalid");
			break;
		}
						
		// This is our CreateDialog, which loads the dialog from our resource:
		//Este es nuestro creador de dialogos, que cargara el dialogo de nuestro recurso
		IDialog* dialog = dialogCreator->CreateDialog();
		if (dialog == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: could not create dialog");
			break;
		}
		// The panel data will be the selectable panel of the dialog:
		//los datos del panel sea el panel seleccionable del di·logo
		InterfacePtr<IPanelControlData> panelData(dialog, UseDefaultIID());
		if (panelData == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
		
		// Find the panel:
		//Busqueda del panel:
		WidgetID	widgetID;
		//if (isTabStyle)
		//{
		widgetID = kA2PPrefSelDlgTabDialogWidgetID;
		//}
		
		IControlView* dialogView = panelData->FindWidget(widgetID);
		if (dialogView == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogView invalid");
			break;
		}
						
		
		// The dialog switcher controls how the panels are switched between:
		//los controles del switcher del di·logo cÛmo los paneles se cambian en medio: 
		InterfacePtr<ISelectableDialogSwitcher> 
		dialogSwitcher(dialogView, IID_ISELECTABLEDIALOGSWITCHER);
		if (dialogSwitcher == nil)
		{
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: dialogSwitcher invalid");
			break;
		}
		
		dialogSwitcher->SetDialogServiceID(kA2PPrefSelDlgService);

		// Sets default panel.
		
		//Coloca en la pestaÒa por default
		dialogSwitcher->SwitchDialogPanelByID(kA2PPrefDocPanelWidgetID);
		
		// Open the dialog.
		//Abre el dialogo
		
		dialog->Open(); 
		
	}while(false);
}

// End, A2PPrefUserUIPwdDialogController.cpp.