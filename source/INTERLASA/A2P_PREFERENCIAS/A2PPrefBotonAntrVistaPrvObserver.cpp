/*
//	File:	A2P20BotonAdelanteVPrevObserver.h
//
//	Author: Fernando Llanas
//
//	Date:	11-Jun-2003
//
//	Este archivo contiene las instrucciones a realizar al momento de dar click sobre el boton
//	Atras en vista previa.
//	
//
*/
#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISubject.h"
#include "ISelectableDialogSwitcher.h"
#include "FileUtils.h"
// Implementation includes:
#include "CDialogObserver.h"
//#include "EstructuraPreferencias.h"
// Project:
#include "SDKUtilities.h"
#include "CAlert.h"
#include "PMString.h"

#include "IWidgetParent.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "ITriStateControlData.h"

#include "A2PPrefID.h"

#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
#endif
#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"
#endif
/**
*/
class SelDlgBotonAtrasVPrevObserver : public CObserver
{
	public:	
		/**
			Constructor.

			@param boss interface ptr from boss object on which interface is aggregated.
		*/
		SelDlgBotonAtrasVPrevObserver(IPMUnknown *boss) : CObserver(boss) {}		

		/**
			Called by the host when the observed object changes. In this case: 
			the tab dialog's info button is clicked.

			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);

		PMString SeparaCad(PMString CRecibida,int32 CaracterSeparacionEnCampos);

		/**
		*/
		int32 CaracterDeSeparaciondeCampos();

		/**
		*/
		bool16 LeerEnCheckBox(IPanelControlData* panelControlData, const WidgetID& widgetID);
	

		PMString LeerEnWidgetBox(IPanelControlData* panelControlData, const WidgetID& widgetID);

		/** 
			Called by the application to allow the observer to attach to the 
			subjects to be observed. In this case the tab dialog's info button widget.
		*/
		virtual void AutoAttach();

		/** 
			Called by the application to allow the observer to detach from the 
			subjects being observed. 
		*/
		virtual void AutoDetach();		
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(SelDlgBotonAtrasVPrevObserver, kA2PPrefSelBotonAtrasVPrevObserverImpl)



/*
	Update
*/
void SelDlgBotonAtrasVPrevObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID& protocol, 
	void* changedBy
)
{

	do
	{
	
		//CAlert::InformationAlert("11");
		if(CaracterDeSeparaciondeCampos()==-1)
		{//sale de funcion pues no se encuentra ningun caracter de separacion para los campos
			break;
		}
		//CAlert::InformationAlert("12");
		FILE *stream;
		//	PMString Archivo;
		PMString Archivo;
		PMString NumLineaLeida;
		PMString PMSCadena;
		PMString CadenaAEmprimir;

		PMString NumLineNew;
		CString ArchivoCString;
		int PosIni;
		int PosFin;
		PlatformChar Caracter;
		int NumCaracter;
		int NumLineaALeer;
		int Linea;
		int ANSCCICaranter;
		//int a,b;
		
		
		Archivo="";
		NumLineaLeida="";
		NumLineNew="";
		ArchivoCString="";

		InterfacePtr<IControlView> view(theSubject, IID_ICONTROLVIEW);

		if (view != nil)
		{
			// Get the button ID from the view
			WidgetID theSelectedWidget = view->GetWidgetID();

			if (theSelectedWidget == kA2PPrefBotonAtrasVisPreviaID && theChange == kTrueStateMessage)
			{
				//Creo una interfaz llamada Myparen la cual sera la ventana 
						//que se encuentra abierta
				//CAlert::InformationAlert("13");
				InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
				//CAlert::InformationAlert("14");
				//Obtengo la inerfaz del panel que se encuentra abierto
				InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
				//obtengo una interfaz del la caja de vista previa
				//CAlert::InformationAlert("15");
				InterfacePtr<IControlView>		editBoxView( panel->FindWidget(kA2PPrefBoxDireccionArchivoLeidoWidgetID), UseDefaultIID() );
				//CAlert::InformationAlert("16");
				//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
				InterfacePtr<ITextControlData>	selectedChar( editBoxView, UseDefaultIID());
				//Extraigo el texto del Widget
				//CAlert::InformationAlert("17");
				Archivo=selectedChar->GetString();
				//CAlert::InformationAlert("18");
				//Lecctura del numero de linea que se leyo
				InterfacePtr<IControlView>		editBoxView1( panel->FindWidget(kA2PPrefBoxNumeroLineaLeidaWidgetID), UseDefaultIID() );
				//CAlert::InformationAlert("19");
				InterfacePtr<ITextControlData>	numerolinea( editBoxView1, UseDefaultIID());
				//CAlert::InformationAlert("20");
				NumLineaLeida=numerolinea->GetString();

				//CAlert::InformationAlert("22");
				//Lectura del archivo que se leyo
				InterfacePtr<IControlView>		editBoxView2( panel->FindWidget(kA2PPrefBoxDireccionArchivoLeidoWidgetID), UseDefaultIID() );
				//CAlert::InformationAlert("23");
				InterfacePtr<ITextControlData>	selectedChar2( editBoxView2, UseDefaultIID());
				//CAlert::InformationAlert("4");
				Archivo=selectedChar2->GetString();
				//CAlert::InformationAlert("25");
				NumLineaALeer=NumLineaLeida.GetAsNumber();
				//CAlert::InformationAlert("1");
				//Se leera la linea de atras siempre y cuando no sea la linea # 1
				if(NumLineaALeer>2)
				{	
					if(NumLineaALeer>3)
							 	NumLineaALeer=NumLineaALeer-3;	
							else
								NumLineaALeer=NumLineaALeer-1;	
					//CAlert::InformationAlert("2");
					//Archivo.GetCString(ArchivoCString,80);
					//CAlert::InformationAlert("3");
					//Archivo=InterlasaUtilities::MacToUnix(Archivo);
					if((stream  = FileUtils::OpenFile(Archivo.GrabCString(),"r"))==NULL)
						{	
							CAlert::InformationAlert(kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
						}
						else
						{	//CAlert::InformationAlert("4");
							PosIni=0;
							PosFin=0;
							Linea=1;
							NumCaracter=0;
							
							
							while(Linea<NumLineaALeer)//mientras que el numero de linea que se esta leyendo sea menor al numero de linea
							{							//que se leyo por ultima vez en importar
								PosIni=PosFin;			//
								do
								{		
									Caracter.SetFromUnicode(fgetc(stream));//se obtiene el caracter del archivo
									NumCaracter++;			//se incrementa el numero de caracter
									ANSCCICaranter=Caracter.GetValue();	//se adquiere el codigo ascii del caracter
								}while(ANSCCICaranter!=10 && ANSCCICaranter!=13 && !feof(stream));//mientras  el caraceter sea diferente a un salto de linea o fin de archivo
								PosFin=NumCaracter;//Pos sicion final de cadena igual al numero del ultimo caracter leido
								Linea++;//nueva linea a leer o nuevo aviso
							}
							//CAlert::InformationAlert("5");
							//cierre del archivo
							fclose(stream);
							
							//Obtencion del nombre del archivo en plataforma CString
							//Archivo.GetCString(ArchivoCString,80);

							if( (stream  = FileUtils::OpenFile(Archivo.GrabCString(),"r"))==NULL)
								{	
									CAlert::InformationAlert(kA2PPrefMensajeNoPudoAbrirArchivoStringKey);
								}
							else
								{
									
									char *Cadena=new char[PosFin];
										
									fread(Cadena,1,PosFin,stream);
									//cuando cierro el archivo me manda un error en la ultima linea que se debe leer
									fclose(stream);

									//Copia cadena a archivo
									PMSCadena=Cadena;
										
									//Remueve del archivo las lineas que no se desean dejando 
									PMSCadena.Remove(0,PosIni);
										
									//convierte el numero de linea a caracteres
									if(Linea>1)
									  Linea=Linea+1;
									NumLineNew.AppendNumber(Linea);
										
									//Se pone el nuevo numero de linea que se ha leido en el widget correspondiente
									numerolinea->SetString(NumLineNew);
										
									PMString Arc2;
										
									//PosFin Obtiene el tamaÒo de la linea
									PosFin=PosFin-PosIni;
										
									//inserto en Arc2 la linea deseada
									Arc2.Insert(PMSCadena,PosIni,PosFin);
										
									//Obtengo la cadena a imprimir llamando a la funcion SeparaCad
									CadenaAEmprimir=SeparaCad(Arc2,CaracterDeSeparaciondeCampos());
										
									//Busqueda del Widget pde la caja  de vista previa
									InterfacePtr<IControlView>		editBoxView3( panel->FindWidget(kA2PPrefBoxVistaPreviaID), UseDefaultIID() );
									//crea una interfax para controlar los dsatos del widget correspondiente
									InterfacePtr<ITextControlData>	selectedChar3( editBoxView3, UseDefaultIID());
									//inserto dentro del widget la nueva cadena
									selectedChar3->SetString(CadenaAEmprimir);
									delete Cadena;
									
									fclose(stream);
								}
							
						}
				}							
			}
		}
	}while(false);
}




/* AutoAttach */
/************AL INICIAR LA FORMA SE CARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR************************************************************************/

void SelDlgBotonAtrasVPrevObserver::AutoAttach()
{//	CAlert::WarningAlert("AutoAttach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_IBOOLEANCONTROLDATA);
	}
}

/* AutoDetach */
/************AL TERMINAR LA FORMA SE DESCARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR************************************************************************/

void SelDlgBotonAtrasVPrevObserver::AutoDetach()
{//	CAlert::WarningAlert("AutoDetach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->DetachObserver(this,IID_IBOOLEANCONTROLDATA);
	}
}


/*
	Esta funcion separa la linea que se envia(CRecibida) entabuladores tambien aÒade 
	el numero de campo asi como el brinco de linea
*/
PMString SelDlgBotonAtrasVPrevObserver::SeparaCad(PMString CRecibida,int32 CaracterSeparacionEnCampos)
{
		PMString CadenaAEnviar;	//Cadena que retorna
	PMString CEncontrada;	//Cadena del campo encontrada
	CadenaAEnviar.Clear();	//limpia cadenas
	CadenaAEnviar.Clear();	
	int PosTab=0;			//posicion del primer caracter tabulador
	int numcampo=1;			//numero de campo que se a leido
	int Tamano=0;			//tamaÒo de la cadena que se recibio
	int salir=0;			//bandera que indica si se ha leido el ultimo caracter de la cadena
	
	do
	{
		//se obtien la posicion del tabulador
		PosTab=CRecibida.IndexOfWChar(CaracterSeparacionEnCampos);
		//Si la posicion del tabulador fue -1 inidica que no se ha encontrado un tabulador entonces se encontro un fin 
		//de cadena se enciende la bandera salir del ciclo
		if(PosTab==-1)
		{
			PosTab=CRecibida.NumUTF16TextChars();//la posicion del tabulador sera el tamaÒo de la cadena
			salir=1;
		}

		//Cadena encontrada(campo) se toma desde el inicio de la cadena recibida hasta la posicion del 1∞ tabulador
		CEncontrada.Insert(CRecibida,0,PosTab);
		//se bora este campo de la cadena recibida
		CRecibida.Remove(0,PosTab+1);
		//se agrega el numero de campo que le corresponde  ala cadena a enviar
		CadenaAEnviar.AppendNumber(numcampo);
		CadenaAEnviar.Append(".- ");
		//se agrega la cadena encontrada a cadena a enviar
		CadenaAEnviar.Append(CEncontrada);
		//se obtiene  el tamaÒo de la cadena a enviar para insertar en la ultima posicion 
		//el salto de linea
		Tamano=CadenaAEnviar.NumUTF16TextChars();
		CadenaAEnviar.InsertW(13,Tamano);
		CadenaAEnviar.InsertW(10,Tamano+1);
		//se incrementa el numero de campo
		numcampo++;
		//se limpia la cadena encontrada
		CEncontrada.Clear();
	}while(salir!=1);
	return(CadenaAEnviar);//regresa cadena a enviar, cadena con numero de campo y su contenido
}

int32 SelDlgBotonAtrasVPrevObserver::CaracterDeSeparaciondeCampos()
{

	int32 retval;
	UTF32TextChar temp;
	InterfacePtr<IWidgetParent>		myParent(this, UseDefaultIID());				
	//Obtengo la inerfaz del panel que se encuentra abierto
	InterfacePtr<IPanelControlData>	panelControlImportar((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
	//Busqueda del Widget pde la caja  de vista previa

	

	if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonTabWidgetID))	
		retval=9;
	else
		if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonComaWidgetID))
			retval=44;
		else
			if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonPyComaWidgetID))
				retval=59;
			else
				if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonDosPuntosWidgetID))
					retval=58;
				else
				{
					if(LeerEnCheckBox(panelControlImportar,kA2PPrefRadioButtonEspecialWidgetID))
					{
						PMString a;
						a=LeerEnWidgetBox(panelControlImportar,kA2PPrefBoxCarEspecialLecturaEID);
						if(a.NumUTF16TextChars()>0)
						{
							temp=a.GetWChar(0);
							retval=temp.GetValue();
						}
						else
						{
							CAlert::WarningAlert(kA2PPrefMensajeTeclearCarEspecialStringKey);
							retval=-1;
						}
					}
					else
					{
						CAlert::WarningAlert(kA2PPrefMensajeSelecTipoSepCamposStringKey);
						retval=-1;
					}
				}
	return(retval);
}

bool16 SelDlgBotonAtrasVPrevObserver::LeerEnCheckBox(IPanelControlData* panelControlData, const WidgetID& widgetID)
{	
	bool16 retval=kFalse;
	do
	{
		InterfacePtr<IControlView>		editCheckBoxt(panelControlData->FindWidget(widgetID), UseDefaultIID() );
		if(editCheckBoxt==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		InterfacePtr<ITriStateControlData>	CheckBox( editCheckBoxt, IID_ITRISTATECONTROLDATA);
		if(CheckBox==nil)
		{
			CAlert::InformationAlert("No se encontro el estado del check box");
			break;
		}
		if(CheckBox->IsSelected())
			retval=kTrue;
		else
			retval=kFalse;
	}while(false);
	return retval;
}

/*
	FUNCION PARA LEER TEXTO EN UNA CAJA DE TEXTO 
*/
PMString SelDlgBotonAtrasVPrevObserver::LeerEnWidgetBox(IPanelControlData* panelControlData, const WidgetID& widgetID)
{
	PMString retval="";
	do 
	{	
		//obtengo una interfaz del la caja de vista previa
		InterfacePtr<IControlView>		editBoxView( panelControlData->FindWidget(widgetID), UseDefaultIID() );
		if(editBoxView==nil)
		{
			CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
		//Obtengo un controlador de texto para la caja de vista previa llamado selectChar
		InterfacePtr<ITextControlData>	ControlDETexto( editBoxView, UseDefaultIID());
		if(ControlDETexto==nil)
		{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
		}
		//Extraigo el texto del Widget
		retval=ControlDETexto->GetString();
	}while(false);
	return retval;
}
