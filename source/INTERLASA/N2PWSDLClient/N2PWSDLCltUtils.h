/*
 *  N2PWSDLCltUtils.h
 *  N2PWSDLClient
 *
 *  Created by ADMINISTRADOR on 25/04/11.
 *  Copyright 2011 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef __N2PWSDLCltUtils_H_DEFINED__
#define __N2PWSDLCltUtils_H_DEFINED__

#include"IPMUnknown.h"




class IN2PWSDLCltUtils:public IPMUnknown
{
	public: 
	enum { kDefaultIID = IID_IN2PWSDLCLTUTILSINTERFACE };
	
	
	virtual const bool16 realizaConneccion(PMString usuario,
										   PMString ip, 
										   PMString cliente,
										   PMString aplicacion, 
										   PMString URL)=0;
	
	virtual const  bool16 realizaDesconeccion(PMString usuario,
											  PMString ip, 
											  PMString cliente,
											  PMString aplicacion,
											  PMString URL)=0;
	
	virtual const K2Vector<PMString> MySQLConsulta(PMString query,PMString URL)=0;
	
	virtual const K2Vector<PMString> MySQLConsulta_function(PMString query,PMString URL)=0;


	virtual const bool16 sendPost(PMString URL, PMString publicacion, PMString idsNotas)=0;


};
#endif