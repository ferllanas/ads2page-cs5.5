/*
//	File:	CheckInOutSuite.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"
#include "HelperInterface.h"

#include "InterlasaRegEditUtilities.h"
#include "..\A2P_PREFERENCIAS\A2PPrefID.h"
#include "CAlert.h"
#include <winreg.h>
#include <sys/types.h> 
#include <sys/stat.h> 
/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	@author Juan Fernando Llanas Rdz
*/
class RegEditUtilities : public CPMUnknown<IRegEditUtilities>
{
public:
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	RegEditUtilities (IPMUnknown *boss);

	/*
	*/
	PMString QueryKey(HKEY hKey, LPTSTR lPath, LPCTSTR lSubKey);

	int CreateKey(LPCTSTR lpSubKey, LPTSTR lpClass, LPTSTR lpData);

	int DeleteKey(HKEY hKey, LPTSTR sPsth, LPCTSTR lpSubKey);

	bool16 ExistKey(HKEY hKey, LPTSTR lPath, LPCTSTR lSubKey );

	PMString ProporcionaFechaArchivo(CString Archivo);

	//PMString DescryptSerial(CString CadenaEncriptada);

	//PMString MacAddres2 ();

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(RegEditUtilities, kA2PPrefRegEditUtilitiesImpl)


/* HelloWorld Constructor
*/
RegEditUtilities::RegEditUtilities(IPMUnknown* boss) 
: CPMUnknown<IRegEditUtilities>(boss)
{
}
/**
*/
PMString RegEditUtilities::QueryKey(HKEY hKey, LPTSTR lPath, LPCTSTR lSubKey)
{

	TCHAR lpData[255];
    DWORD lpType, lpcbData =255;
    LPTSTR rtn; 
	PMString Temp="";
	if(RegOpenKeyEx(hKey, lPath, NULL,KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
	{
        if(RegQueryValueEx(hKey, lSubKey, NULL, &lpType, (BYTE*)lpData, &lpcbData) == ERROR_SUCCESS)
		{
			if(lpData==NULL)
				rtn = TEXT("");
			else
				rtn = lpData;
		}
        else rtn = TEXT("");
	}
    else rtn = TEXT("");
    RegCloseKey(hKey); 
	Temp.SetTString(rtn);
	if(Temp.GetTLength()==0)
		Temp="";
    return (Temp.GrabCString());
} 

int RegEditUtilities::CreateKey(LPCTSTR lpSubKey, LPTSTR lpClass, LPTSTR lpData)
{
    HKEY keyHandle;
    long rtn =0;
    DWORD lpdw;
    int aLen = 0; 

    //In order to set the value you must specify the length of the lpData.   
    aLen = lstrlen(lpData) * 2 ; 

    //This will create the key if it does not exist or update it if it does.   
     rtn = RegCreateKeyEx(HKEY_LOCAL_MACHINE,lpSubKey, 0, LPWSTR("Anything"),
            REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, 0, &keyHandle, &lpdw); 

    if(rtn == ERROR_SUCCESS)
	{
		//CAlert::InformationAlert("Error2");
        //This will write the data to the newly created key or update it.
         rtn = RegSetValueEx(keyHandle, lpClass,
                    0, REG_SZ, (LPBYTE)lpData, aLen );
        //Good programming practice, close what you open.
        rtn = RegCloseKey(keyHandle);
    }
    return rtn;
} 

int RegEditUtilities::DeleteKey(HKEY hKey, LPTSTR sPsth, LPCTSTR lpSubKey)
{
	RegDeleteKey(hKey, lpSubKey);
	return(1);
} 

/**
*/
bool16 RegEditUtilities::ExistKey(HKEY hKey, LPTSTR lPath, LPCTSTR lSubKey )
{

	TCHAR lpData[255];
    DWORD lpType, lpcbData =255;
    LPTSTR rtn; 
	bool16 retval;
	if(RegOpenKeyEx(hKey, lPath, NULL,KEY_QUERY_VALUE, &hKey) == ERROR_SUCCESS)
	{
        if(RegQueryValueEx(hKey, lSubKey, NULL, &lpType, (BYTE*)lpData, &lpcbData) == ERROR_SUCCESS)
		{
			rtn = lpData;
			retval=kTrue;
		}
        else 
		{
			retval=kFalse;
		}
	}
    else 
	{
		retval=kFalse;
	}
    RegCloseKey(hKey); 

    return (retval);
} 

PMString RegEditUtilities::ProporcionaFechaArchivo(CString Archivo)
{
	struct stat p;
	stat(Archivo,&p);
	return(ctime(&p.st_atime));
}

/* //NO SE NECESITA CREO YO 
PMString RegEditUtilities::DescryptSerial(CString CadenaEncriptada)
{
	PMString Temp="";
	char *caracter;		//caracter leido
	int ValorAscii=0;	//Valor ascii del caracter
	char *Llave;		//llave para la conversion de de los caracteres
	Llave="INTERLASA";	
	int Tamano;			//tama�o de la cadena a encriptar
	int NumCarLlave;
	int numCarDeCadADesencrip;
	Tamano=strlen(CadenaEncriptada);  //MANDA EL ERROR DE PERDIDA DE INFORMACION
	//char CaracterDesencriptada;		//Caracter desencriptado
	PMString CadenaDesencriptada;			//Cadena completa y desencriptada
	NumCarLlave=0;
	for(numCarDeCadADesencrip=0;numCarDeCadADesencrip<Tamano;numCarDeCadADesencrip++)//hacer hasta el ultimo caracter de la cadena a desencriptar
	{
		ValorAscii=(int)CadenaEncriptada[numCarDeCadADesencrip];//obtiene el codigo ascii del caracter leido de la cadena a desencriptar
		//Temp.AppendNumber(ValorAscii);
		//CAlert::InformationAlert(Temp);
		//Temp.Append(",");
		ValorAscii=(ValorAscii-(int)Llave[NumCarLlave])+19;//codigo asccii del caracter leido de la llave y adicion a valos asscii
		//Temp.AppendNumber(ValorAscii);
		//CAlert::InformationAlert(Temp);
		//Temp.Append(",");
		//CaracterDesencriptada=(char)ValorAscii;//conversion del numero resultante a caracter
		
		CadenaDesencriptada.AppendW(ValorAscii);//adidion a cadena desencriptada
		
		//CAlert::InformationAlert(CadenaDesencriptada);
		caracter=" ";
		ValorAscii=0;
		NumCarLlave++;//siguiente caracter
		if(NumCarLlave>7)//si caracter en llave es mayor de 7 comenzar en 0
			NumCarLlave=0;
	}
//CadenaDesencriptada.SetTranslated();
	//CAlert::InformationAlert(CadenaDesencriptada);
return(CadenaDesencriptada.GrabCString());
}*/

typedef struct _ASTAT_
{
    ADAPTER_STATUS adapt;
    NAME_BUFFER    NameBuff [30];
}ASTAT, * PASTAT;

ASTAT Adapter;

/*PMString RegEditUtilities::MacAddres2 ()
{
	PMString macadres="";
    NCB ncb;
    UCHAR uRetCode;
    //char NetName[50];
	char TempEthernet[25];

    memset( &ncb, 0, sizeof(ncb) );
    ncb.ncb_command = NCBRESET;
    ncb.ncb_lana_num = 0;

    uRetCode = Netbios( &ncb );
    printf( "The NCBRESET return code is: 0x%x \n", uRetCode );

    memset( &ncb, 0, sizeof(ncb) );
    ncb.ncb_command = NCBASTAT;
    ncb.ncb_lana_num = 0;

    strcpy( (char *)ncb.ncb_callname,  "*               " );
    ncb.ncb_buffer = (PUCHAR) &Adapter;
    ncb.ncb_length = sizeof(Adapter);

    uRetCode = Netbios( &ncb );
    //printf( "The NCBASTAT return code is: 0x%x \n", uRetCode );
    if ( uRetCode == 0 )
    {
      sprintf(TempEthernet, "%02i %02i %02i %02i %02i %02i\n",
                Adapter.adapt.adapter_address[0],
                Adapter.adapt.adapter_address[1],
                Adapter.adapt.adapter_address[2],
                Adapter.adapt.adapter_address[3],
                Adapter.adapt.adapter_address[4],
                Adapter.adapt.adapter_address[5] );

		macadres=TempEthernet;
		//CAlert::InformationAlert(macadres);
    }
	return(macadres);
}*/
