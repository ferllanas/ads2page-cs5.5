/*[#text:            **********************************************************************************************]
 [#text:        * Name        : ] upromepsdesign.h
 [#text:      * Design      : ] Designer-C2P-Dummy
 [#text:   * Created By  : ] Sentinel UltraPro v1.0.0
 [#text: * Description : Include File for Toolkit design strategy.]
 [#text:     * Purpose     : This file is required to include in program used to design strategy created by Toolkit.]

 [#text:   * Generated   : ] 07/19/2007 (MM/DD/YYYY)

 [#text:     * WARNING - this file is generated, any edits might be overwritten!]
 [#text:               **********************************************************************************************]*/

#include "upromeps.h"

#define DESIGNID                                                             0x5EC6
#define SP_DESIGNERKEYVALUE_INTEGER                                          0x110008
#define SP_C2PKEYVALUE_INTEGER                                               0x10009
#define SP_DUMMYNATOR_INTEGER                                                0x10008

