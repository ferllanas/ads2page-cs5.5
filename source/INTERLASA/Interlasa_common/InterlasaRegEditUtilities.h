/*
//
//	File:	InterlasaRegEditUtilities.h
//
//
//	Date:	6-Mar-20010
//  
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/
#ifndef __InterlasaRegEditUtilities_h__
#define __InterlasaRegEditUtilities_h__

#include"IPMUnknown.h"
#include"..\A2P_PREFERENCIAS\A2PPrefID.h"
//#include <winreg.h>    //If you are using MS Visual C++ you do not need this

/**
	Encapsulates detail of working with the list-box API. 
	@author Ian Paterson
*/
class IRegEditUtilities:public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IA2PPRegEditUtilities };
		
	virtual PMString QueryKey(HKEY hKey, LPTSTR lPath, LPCTSTR lSubKey)=0;

	virtual int CreateKey(LPCTSTR lpSubKey, LPTSTR lpClass, LPTSTR lpData)=0;

	virtual int DeleteKey(HKEY hKey, LPTSTR sPsth, LPCTSTR lpSubKey)=0;

	virtual bool16 ExistKey(HKEY hKey, LPTSTR lPath, LPCTSTR lSubKey )=0;

	virtual PMString ProporcionaFechaArchivo(CString Archivo)=0;

	//virtual PMString DescryptSerial(CString CadenaEncriptada)=0;

	//virtual PMString MacAddres2 ()=0;
};


#endif // __SDKListBoxHelper_h__

