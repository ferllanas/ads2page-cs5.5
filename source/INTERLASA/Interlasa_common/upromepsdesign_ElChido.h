/*[#text:            **********************************************************************************************]
 [#text:        * Name        : ] upromepsdesign.h
 [#text:      * Design      : ] SwInterlasa
 [#text:   * Created By  : ] Sentinel UltraPro v1.0.0
 [#text: * Description : Include File for Toolkit design strategy.]
 [#text:     * Purpose     : This file is required to include in program used to design strategy created by Toolkit.]

 [#text:   * Generated   : ] 12/26/2007 (MM/DD/YYYY)

 [#text:     * WARNING - this file is generated, any edits might be overwritten!]
 [#text:               **********************************************************************************************]*/

#include "upromeps.h"


//[#text:]
#define DESIGNID                                                             0xBED0
//[#text:] 
#define SP_A_INTEGER                                                         0x20008
//[#text:] 
#define SP_N2PLIMIT_FULLLICENSE                                              0x60014
//[#text:] 
#define SP_N2PLIMIT_FULLLICENSE_USER_LIMIT                                   0x9000A
//[#text:] 
#define SP_N2PLIMIT_FULLLICENSE_TIME_TAMPER                                  0x80012
//[#text:] 
#define SP_N2PLIMIT_FULLLICENSE_EXECUTION_CONTROL                            0x80013
//[#text:] 
#define SP_N2PLIMIT_FULLLICENSE_EXPIRATION_DATE                              0x90011

/*[#text:  Total number of query response: ]    501*/
#define SP_NUM_OF_QR                                                  501

/*[#text:  Length of query data: ]    4*/
#define SP_LEN_OF_QR                                                  4

/*[#text:  This query response is generated for element : ]  N2PLimit */
unsigned char QUERY_SP_N2PLIMIT_FULLLICENSE[SP_NUM_OF_QR]  [SP_LEN_OF_QR] = {
{0x26,0x41,0x6E,0x29},
{0x61,0x34,0x77,0x41},
{0x02,0x32,0x3C,0x7E},
{0x71,0x6D,0x50,0x31},
{0x60,0x4A,0x20,0x7C},
{0x3C,0x7B,0x17,0x4A},
{0x02,0x2F,0x12,0x4C},
{0x0C,0x7C,0x58,0x17},
{0x50,0x4B,0x51,0x20},
{0x40,0x4B,0x0C,0x13},
{0x1E,0x2E,0x78,0x07},
{0x60,0x02,0x71,0x61},
{0x78,0x6F,0x39,0x38},
{0x0E,0x0A,0x22,0x36},
{0x0B,0x69,0x3C,0x33},
{0x5C,0x1F,0x07,0x79},
{0x46,0x42,0x2E,0x4A},
{0x30,0x15,0x23,0x12},
{0x66,0x1A,0x53,0x03},
{0x2B,0x5C,0x38,0x4B},
{0x47,0x41,0x0F,0x20},
{0x0B,0x6F,0x3C,0x76},
{0x79,0x0C,0x4D,0x24},
{0x1E,0x63,0x0A,0x35},
{0x34,0x29,0x50,0x04},
{0x3E,0x0D,0x60,0x72},
{0x08,0x54,0x75,0x03},
{0x43,0x43,0x06,0x3F},
{0x41,0x06,0x35,0x6B},
{0x23,0x52,0x54,0x12},
{0x45,0x60,0x1D,0x5A},
{0x03,0x24,0x1F,0x3C},
{0x72,0x1F,0x7E,0x2A},
{0x57,0x0F,0x06,0x45},
{0x06,0x0B,0x53,0x66},
{0x35,0x4A,0x57,0x2B},
{0x39,0x69,0x6A,0x58},
{0x0B,0x57,0x03,0x03},
{0x7B,0x07,0x78,0x58},
{0x74,0x5A,0x63,0x08},
{0x27,0x13,0x1E,0x33},
{0x14,0x47,0x5B,0x45},
{0x5A,0x6F,0x59,0x05},
{0x47,0x38,0x13,0x02},
{0x34,0x06,0x4E,0x1B},
{0x3E,0x15,0x3D,0x7A},
{0x30,0x49,0x23,0x29},
{0x77,0x63,0x4B,0x07},
{0x44,0x1C,0x7B,0x44},
{0x6F,0x78,0x0D,0x53},
{0x23,0x5F,0x7B,0x41},
{0x15,0x26,0x72,0x5C},
{0x1A,0x34,0x2D,0x01},
{0x25,0x50,0x7D,0x17},
{0x2C,0x72,0x0C,0x42},
{0x10,0x40,0x3A,0x74},
{0x4B,0x2A,0x65,0x1B},
{0x4F,0x43,0x4F,0x3D},
{0x60,0x07,0x15,0x04},
{0x66,0x67,0x5D,0x5B},
{0x1E,0x11,0x06,0x19},
{0x24,0x3D,0x37,0x1D},
{0x7C,0x79,0x07,0x0C},
{0x63,0x55,0x50,0x13},
{0x1A,0x78,0x79,0x59},
{0x5B,0x19,0x0B,0x7D},
{0x40,0x0B,0x75,0x0E},
{0x53,0x27,0x54,0x6B},
{0x3D,0x48,0x32,0x3B},
{0x03,0x09,0x10,0x52},
{0x32,0x5D,0x3E,0x00},
{0x20,0x4A,0x75,0x48},
{0x5F,0x20,0x60,0x3D},
{0x22,0x00,0x15,0x2E},
{0x7D,0x32,0x78,0x36},
{0x59,0x71,0x78,0x74},
{0x42,0x2E,0x5E,0x72},
{0x3A,0x09,0x33,0x08},
{0x34,0x12,0x22,0x1A},
{0x6F,0x3F,0x20,0x17},
{0x68,0x31,0x3A,0x3E},
{0x33,0x77,0x3C,0x33},
{0x61,0x39,0x38,0x0E},
{0x18,0x28,0x76,0x7B},
{0x54,0x04,0x29,0x3A},
{0x64,0x38,0x60,0x50},
{0x1F,0x29,0x13,0x21},
{0x65,0x39,0x38,0x08},
{0x47,0x48,0x22,0x7A},
{0x64,0x67,0x68,0x5F},
{0x59,0x5D,0x03,0x78},
{0x1A,0x43,0x0B,0x66},
{0x50,0x23,0x79,0x25},
{0x21,0x1D,0x51,0x3A},
{0x4A,0x48,0x4A,0x64},
{0x28,0x57,0x2A,0x79},
{0x50,0x1A,0x1D,0x31},
{0x71,0x74,0x5D,0x78},
{0x69,0x3D,0x41,0x61},
{0x42,0x5E,0x3E,0x2B},
{0x57,0x4D,0x73,0x1D},
{0x4D,0x1C,0x70,0x6A},
{0x30,0x7D,0x65,0x62},
{0x04,0x4F,0x67,0x36},
{0x00,0x46,0x00,0x69},
{0x07,0x0A,0x04,0x05},
{0x34,0x06,0x1A,0x42},
{0x1D,0x2F,0x4D,0x3B},
{0x5D,0x53,0x58,0x14},
{0x18,0x00,0x57,0x73},
{0x32,0x30,0x28,0x29},
{0x49,0x3B,0x50,0x28},
{0x65,0x27,0x40,0x2D},
{0x2A,0x29,0x1C,0x3E},
{0x59,0x3E,0x13,0x3C},
{0x12,0x5F,0x6B,0x44},
{0x6F,0x7E,0x49,0x18},
{0x49,0x5C,0x75,0x50},
{0x24,0x64,0x1F,0x5A},
{0x78,0x02,0x79,0x0F},
{0x68,0x45,0x43,0x11},
{0x2D,0x64,0x1C,0x0D},
{0x32,0x1A,0x26,0x2B},
{0x4B,0x03,0x66,0x58},
{0x53,0x34,0x11,0x0C},
{0x01,0x04,0x52,0x11},
{0x54,0x55,0x12,0x58},
{0x32,0x3A,0x31,0x2D},
{0x3A,0x5C,0x73,0x2F},
{0x4B,0x6B,0x0D,0x48},
{0x0E,0x1A,0x4C,0x16},
{0x36,0x5F,0x01,0x13},
{0x7D,0x64,0x5F,0x20},
{0x27,0x17,0x20,0x19},
{0x7C,0x3F,0x1F,0x1F},
{0x49,0x0D,0x06,0x3D},
{0x5A,0x0E,0x50,0x5F},
{0x79,0x79,0x65,0x07},
{0x3B,0x20,0x0B,0x25},
{0x50,0x09,0x56,0x2E},
{0x4B,0x37,0x77,0x02},
{0x6C,0x59,0x7D,0x76},
{0x2D,0x5E,0x3D,0x0C},
{0x2A,0x36,0x3C,0x29},
{0x24,0x25,0x1D,0x0E},
{0x72,0x4A,0x00,0x0F},
{0x39,0x03,0x4E,0x58},
{0x71,0x14,0x22,0x75},
{0x4C,0x11,0x45,0x3A},
{0x3E,0x60,0x6A,0x77},
{0x66,0x5B,0x0E,0x12},
{0x1B,0x79,0x52,0x1F},
{0x70,0x40,0x67,0x4D},
{0x3A,0x23,0x13,0x10},
{0x36,0x28,0x5D,0x28},
{0x6F,0x56,0x40,0x11},
{0x05,0x24,0x74,0x76},
{0x74,0x74,0x56,0x67},
{0x63,0x67,0x42,0x63},
{0x4D,0x77,0x51,0x6E},
{0x5E,0x7D,0x38,0x4C},
{0x6C,0x01,0x16,0x2F},
{0x08,0x04,0x32,0x1A},
{0x43,0x08,0x12,0x55},
{0x48,0x38,0x78,0x49},
{0x13,0x24,0x26,0x4B},
{0x42,0x08,0x14,0x6B},
{0x58,0x50,0x30,0x2A},
{0x14,0x6A,0x61,0x52},
{0x46,0x54,0x74,0x2C},
{0x59,0x72,0x54,0x13},
{0x73,0x1B,0x02,0x4A},
{0x56,0x19,0x0B,0x27},
{0x6F,0x6D,0x17,0x67},
{0x3B,0x39,0x5A,0x09},
{0x6B,0x0F,0x0B,0x3B},
{0x3A,0x7C,0x34,0x71},
{0x41,0x62,0x17,0x27},
{0x09,0x20,0x05,0x4F},
{0x6A,0x6D,0x61,0x47},
{0x34,0x48,0x1F,0x7E},
{0x35,0x2E,0x21,0x5A},
{0x6A,0x7A,0x64,0x0C},
{0x45,0x2B,0x70,0x7E},
{0x49,0x34,0x36,0x3A},
{0x1A,0x29,0x55,0x0E},
{0x60,0x4C,0x7C,0x02},
{0x01,0x11,0x5D,0x24},
{0x08,0x50,0x37,0x0D},
{0x2C,0x44,0x4E,0x44},
{0x3A,0x41,0x24,0x2A},
{0x51,0x12,0x24,0x19},
{0x57,0x16,0x15,0x4D},
{0x32,0x03,0x58,0x43},
{0x62,0x17,0x36,0x6A},
{0x0D,0x0E,0x0F,0x58},
{0x29,0x32,0x42,0x09},
{0x65,0x2C,0x7C,0x4C},
{0x6D,0x0D,0x5A,0x2B},
{0x2B,0x3C,0x23,0x2F},
{0x4D,0x26,0x78,0x52},
{0x2C,0x4A,0x2A,0x34},
{0x15,0x5E,0x4C,0x5B},
{0x78,0x3B,0x0B,0x4C},
{0x68,0x40,0x79,0x6E},
{0x2E,0x0E,0x46,0x18},
{0x1F,0x09,0x1B,0x3C},
{0x47,0x2E,0x4D,0x1C},
{0x2A,0x2C,0x49,0x47},
{0x13,0x70,0x4F,0x30},
{0x2C,0x4E,0x45,0x1F},
{0x6F,0x09,0x4E,0x45},
{0x04,0x40,0x04,0x75},
{0x55,0x04,0x6D,0x6E},
{0x5C,0x08,0x77,0x6E},
{0x4A,0x13,0x1B,0x22},
{0x3C,0x1E,0x50,0x4B},
{0x0B,0x1A,0x57,0x03},
{0x60,0x5A,0x10,0x32},
{0x43,0x53,0x1B,0x76},
{0x07,0x42,0x38,0x04},
{0x66,0x18,0x37,0x18},
{0x04,0x4F,0x5C,0x32},
{0x45,0x3C,0x54,0x75},
{0x69,0x5D,0x1D,0x3A},
{0x00,0x5E,0x6C,0x08},
{0x3C,0x0F,0x61,0x7C},
{0x52,0x3B,0x68,0x45},
{0x52,0x33,0x4A,0x71},
{0x78,0x77,0x74,0x47},
{0x46,0x6A,0x22,0x2C},
{0x40,0x71,0x4A,0x2F},
{0x46,0x2C,0x5C,0x24},
{0x3B,0x59,0x23,0x5D},
{0x2B,0x4F,0x3E,0x4D},
{0x4B,0x14,0x38,0x38},
{0x4C,0x39,0x5C,0x63},
{0x06,0x41,0x17,0x28},
{0x52,0x12,0x74,0x12},
{0x7A,0x51,0x21,0x76},
{0x7C,0x0A,0x45,0x43},
{0x28,0x73,0x76,0x78},
{0x72,0x33,0x56,0x56},
{0x5E,0x20,0x3B,0x4C},
{0x4D,0x78,0x06,0x3F},
{0x12,0x01,0x25,0x2C},
{0x1F,0x39,0x0D,0x2A},
{0x49,0x27,0x3D,0x6E},
{0x69,0x1A,0x3A,0x4A},
{0x6E,0x7D,0x2E,0x6C},
{0x45,0x13,0x60,0x7E},
{0x4C,0x1C,0x29,0x3B},
{0x46,0x51,0x00,0x36},
{0x13,0x19,0x4A,0x6A},
{0x73,0x27,0x3F,0x5F},
{0x03,0x6D,0x07,0x1D},
{0x43,0x0C,0x3A,0x0F},
{0x6C,0x00,0x28,0x75},
{0x05,0x15,0x43,0x21},
{0x0F,0x17,0x75,0x3F},
{0x2F,0x31,0x5F,0x42},
{0x2E,0x56,0x16,0x05},
{0x09,0x32,0x76,0x29},
{0x47,0x13,0x43,0x7D},
{0x77,0x25,0x5D,0x21},
{0x2C,0x57,0x7E,0x05},
{0x08,0x4E,0x1D,0x16},
{0x1C,0x4E,0x40,0x10},
{0x2F,0x6E,0x3A,0x41},
{0x58,0x41,0x09,0x6C},
{0x3C,0x10,0x35,0x02},
{0x56,0x06,0x43,0x6D},
{0x4B,0x6F,0x35,0x31},
{0x47,0x21,0x02,0x50},
{0x57,0x3E,0x27,0x66},
{0x43,0x3F,0x24,0x3D},
{0x12,0x11,0x5D,0x38},
{0x36,0x5B,0x49,0x2F},
{0x46,0x48,0x0C,0x48},
{0x70,0x1B,0x16,0x40},
{0x6F,0x0E,0x37,0x19},
{0x16,0x50,0x1D,0x7B},
{0x3E,0x02,0x05,0x6A},
{0x28,0x07,0x19,0x41},
{0x4B,0x2F,0x37,0x60},
{0x74,0x6B,0x7D,0x57},
{0x15,0x3C,0x4F,0x32},
{0x39,0x12,0x38,0x30},
{0x6B,0x75,0x4B,0x63},
{0x4E,0x18,0x2B,0x4F},
{0x25,0x4E,0x69,0x4E},
{0x1A,0x5F,0x05,0x73},
{0x19,0x4D,0x28,0x1D},
{0x3B,0x2E,0x67,0x6E},
{0x31,0x20,0x36,0x00},
{0x53,0x75,0x1B,0x1E},
{0x21,0x74,0x6F,0x28},
{0x48,0x2C,0x64,0x1A},
{0x5B,0x31,0x7B,0x29},
{0x42,0x26,0x20,0x5B},
{0x5B,0x3A,0x0F,0x35},
{0x37,0x47,0x36,0x6E},
{0x5B,0x78,0x45,0x17},
{0x6F,0x45,0x27,0x35},
{0x03,0x5A,0x10,0x6F},
{0x3C,0x77,0x0E,0x4D},
{0x08,0x18,0x00,0x02},
{0x43,0x3A,0x6F,0x1E},
{0x2B,0x74,0x42,0x44},
{0x2D,0x02,0x0F,0x50},
{0x71,0x2F,0x77,0x7D},
{0x15,0x3C,0x5B,0x25},
{0x42,0x49,0x06,0x25},
{0x53,0x22,0x4A,0x09},
{0x79,0x1C,0x45,0x61},
{0x06,0x38,0x70,0x64},
{0x7A,0x24,0x63,0x23},
{0x43,0x18,0x3B,0x49},
{0x30,0x00,0x78,0x62},
{0x41,0x19,0x1F,0x02},
{0x0B,0x7A,0x64,0x74},
{0x64,0x4A,0x36,0x05},
{0x05,0x03,0x03,0x15},
{0x57,0x5D,0x29,0x34},
{0x60,0x72,0x37,0x08},
{0x23,0x34,0x09,0x5C},
{0x63,0x32,0x37,0x3D},
{0x5E,0x5D,0x40,0x1A},
{0x1C,0x19,0x43,0x6E},
{0x42,0x53,0x1D,0x05},
{0x5F,0x28,0x3F,0x70},
{0x18,0x06,0x2B,0x6C},
{0x31,0x7E,0x0C,0x61},
{0x65,0x11,0x01,0x7D},
{0x48,0x77,0x2E,0x20},
{0x37,0x44,0x41,0x38},
{0x69,0x74,0x29,0x2F},
{0x25,0x62,0x04,0x64},
{0x13,0x6A,0x2C,0x05},
{0x58,0x74,0x6E,0x23},
{0x0E,0x62,0x4C,0x32},
{0x07,0x27,0x22,0x3B},
{0x46,0x3E,0x28,0x41},
{0x46,0x60,0x39,0x61},
{0x69,0x18,0x15,0x57},
{0x0F,0x3D,0x50,0x65},
{0x0E,0x22,0x66,0x6B},
{0x3B,0x36,0x64,0x6B},
{0x37,0x16,0x62,0x26},
{0x4B,0x5C,0x71,0x1E},
{0x18,0x06,0x09,0x62},
{0x10,0x0E,0x79,0x33},
{0x7D,0x5C,0x50,0x5A},
{0x73,0x12,0x0F,0x07},
{0x3E,0x3A,0x17,0x0A},
{0x54,0x44,0x2F,0x27},
{0x24,0x04,0x38,0x2D},
{0x65,0x6F,0x45,0x67},
{0x27,0x47,0x7E,0x48},
{0x0D,0x5D,0x66,0x75},
{0x4D,0x77,0x0F,0x6B},
{0x32,0x20,0x35,0x01},
{0x5A,0x7D,0x57,0x11},
{0x70,0x45,0x0C,0x62},
{0x7C,0x74,0x78,0x09},
{0x1D,0x05,0x0F,0x78},
{0x5F,0x4D,0x38,0x5F},
{0x16,0x4D,0x03,0x4C},
{0x57,0x4A,0x66,0x7D},
{0x61,0x5B,0x41,0x04},
{0x01,0x4B,0x25,0x46},
{0x74,0x68,0x15,0x6A},
{0x7B,0x21,0x6D,0x02},
{0x15,0x01,0x14,0x2E},
{0x16,0x2A,0x6A,0x75},
{0x10,0x41,0x25,0x15},
{0x0A,0x6E,0x73,0x57},
{0x01,0x2B,0x54,0x6F},
{0x5B,0x14,0x03,0x36},
{0x11,0x56,0x5E,0x56},
{0x05,0x3E,0x3A,0x41},
{0x06,0x07,0x48,0x26},
{0x2D,0x0C,0x70,0x2E},
{0x71,0x6F,0x08,0x40},
{0x6A,0x3C,0x50,0x26},
{0x71,0x2B,0x45,0x50},
{0x2F,0x43,0x20,0x7D},
{0x46,0x2A,0x49,0x5C},
{0x06,0x71,0x24,0x6B},
{0x1D,0x73,0x42,0x5F},
{0x10,0x38,0x4A,0x7D},
{0x28,0x3B,0x4F,0x0F},
{0x48,0x55,0x08,0x1B},
{0x3C,0x3D,0x20,0x79},
{0x35,0x09,0x38,0x4A},
{0x62,0x1C,0x23,0x45},
{0x5E,0x1C,0x25,0x20},
{0x10,0x48,0x18,0x42},
{0x6F,0x0A,0x21,0x7B},
{0x18,0x27,0x72,0x36},
{0x2C,0x73,0x6F,0x58},
{0x4D,0x11,0x30,0x2A},
{0x02,0x3C,0x7C,0x10},
{0x1D,0x51,0x0D,0x01},
{0x75,0x19,0x2B,0x70},
{0x2E,0x44,0x39,0x7C},
{0x0E,0x3D,0x1B,0x74},
{0x71,0x59,0x4C,0x6E},
{0x2A,0x71,0x1B,0x6D},
{0x25,0x0A,0x48,0x05},
{0x53,0x05,0x24,0x34},
{0x38,0x0B,0x37,0x46},
{0x1A,0x30,0x0E,0x24},
{0x45,0x6A,0x20,0x6B},
{0x05,0x2F,0x5D,0x3C},
{0x1E,0x00,0x61,0x3D},
{0x64,0x59,0x0B,0x5E},
{0x0C,0x75,0x49,0x77},
{0x2C,0x0A,0x1F,0x09},
{0x54,0x36,0x52,0x03},
{0x58,0x69,0x28,0x6E},
{0x1D,0x71,0x13,0x36},
{0x3F,0x09,0x41,0x5E},
{0x4A,0x48,0x49,0x53},
{0x0D,0x24,0x05,0x0F},
{0x47,0x69,0x64,0x6F},
{0x47,0x78,0x0B,0x46},
{0x27,0x63,0x67,0x4E},
{0x1A,0x6B,0x41,0x12},
{0x15,0x72,0x6E,0x7D},
{0x4C,0x19,0x2E,0x4E},
{0x11,0x78,0x71,0x54},
{0x27,0x05,0x3A,0x04},
{0x5B,0x62,0x67,0x1E},
{0x6D,0x38,0x3B,0x65},
{0x74,0x4E,0x60,0x2C},
{0x71,0x6D,0x26,0x34},
{0x12,0x5F,0x23,0x38},
{0x45,0x01,0x51,0x62},
{0x0C,0x4A,0x59,0x2D},
{0x1C,0x53,0x7D,0x10},
{0x17,0x74,0x66,0x6E},
{0x0A,0x76,0x41,0x46},
{0x02,0x5F,0x49,0x7A},
{0x13,0x0F,0x50,0x6C},
{0x55,0x2D,0x52,0x2A},
{0x20,0x0B,0x52,0x3A},
{0x59,0x22,0x46,0x55},
{0x24,0x08,0x61,0x67},
{0x5D,0x37,0x10,0x71},
{0x0E,0x0D,0x3F,0x3D},
{0x27,0x3A,0x1F,0x63},
{0x33,0x4B,0x7B,0x6E},
{0x25,0x5B,0x69,0x2C},
{0x23,0x09,0x7E,0x1C},
{0x0B,0x5D,0x79,0x71},
{0x0F,0x5D,0x3A,0x20},
{0x71,0x70,0x2B,0x5B},
{0x4B,0x67,0x4F,0x36},
{0x7B,0x2A,0x27,0x56},
{0x0D,0x77,0x2F,0x32},
{0x4A,0x27,0x13,0x23},
{0x7E,0x05,0x42,0x69},
{0x1B,0x16,0x3C,0x39},
{0x4D,0x68,0x2D,0x64},
{0x40,0x46,0x57,0x06},
{0x0B,0x7D,0x69,0x75},
{0x01,0x75,0x08,0x41},
{0x16,0x27,0x4D,0x75},
{0x40,0x17,0x32,0x5D},
{0x68,0x36,0x60,0x17},
{0x59,0x38,0x70,0x6D},
{0x5A,0x22,0x2E,0x68},
{0x7B,0x4A,0x67,0x3A},
{0x4E,0x5D,0x06,0x63},
{0x6A,0x2B,0x7A,0x7D},
{0x17,0x2C,0x75,0x45},
{0x04,0x2C,0x3B,0x22},
{0x6A,0x4E,0x52,0x37},
{0x06,0x55,0x1A,0x1B},
{0x06,0x7D,0x71,0x56},
{0x15,0x35,0x20,0x16},
{0x64,0x75,0x7B,0x53},
{0x05,0x68,0x72,0x14},
{0x10,0x09,0x6B,0x29},
{0x53,0x06,0x17,0x64},
{0x05,0x4C,0x47,0x3A},
{0x24,0x14,0x4F,0x62},
{0x5C,0x18,0x36,0x6E},
{0x47,0x7D,0x63,0x03},
{0x25,0x61,0x06,0x53},
{0x2A,0x5C,0x2C,0x16},
{0x6E,0x11,0x07,0x07},
{0x21,0x2F,0x6E,0x2B},
{0x34,0x75,0x1E,0x45},
{0x50,0x11,0x1D,0x53},
{0x3E,0x5C,0x39,0x5C},
{0x70,0x1D,0x5A,0x71},
{0x06,0x17,0x1E,0x6D},
{0x43,0x03,0x1E,0x30},
{0x1A,0x4E,0x6E,0x11}
};
unsigned char RESPONSE_SP_N2PLIMIT_FULLLICENSE[SP_NUM_OF_QR] [SP_LEN_OF_QR] = {
{0x47,0x20,0x7A,0xD6},
{0xF2,0xC3,0x88,0xD4},
{0xEE,0x87,0xE7,0xE3},
{0xDE,0x3E,0xAA,0x8B},
{0x7A,0x69,0x4E,0x8D},
{0x40,0x70,0x32,0x62},
{0x60,0x9B,0xA3,0x2F},
{0x4A,0x1A,0x4A,0x73},
{0xD0,0x3C,0xF3,0xB4},
{0xEF,0xAE,0x1E,0xA5},
{0xA2,0xD7,0xE9,0xC6},
{0xDC,0xAA,0xDE,0xAE},
{0xA7,0xD3,0x4F,0x67},
{0x2D,0xF8,0x50,0x15},
{0xBB,0xB5,0x7B,0xC1},
{0xCB,0x21,0x60,0x72},
{0xBE,0x62,0xFD,0xA0},
{0xC5,0x32,0xAB,0x4F},
{0xF2,0x45,0x47,0x5F},
{0xE3,0x9E,0x59,0xEC},
{0xFF,0x6B,0x9E,0xED},
{0x0F,0x45,0x64,0x68},
{0x7F,0xC3,0xBF,0x5B},
{0x25,0x89,0x8A,0xAB},
{0xB3,0x2A,0x1B,0x7F},
{0x89,0x44,0x61,0x84},
{0x9A,0x0F,0xC7,0xFC},
{0xF9,0x4C,0x77,0xA5},
{0xC1,0xF7,0x55,0xD2},
{0x15,0x87,0x38,0x15},
{0x3C,0x5D,0xEC,0x49},
{0x05,0x9A,0x1F,0x46},
{0xAF,0xE4,0x61,0x53},
{0x38,0x61,0xB6,0xB9},
{0xD3,0x1D,0x3D,0x86},
{0x6B,0x37,0x62,0xC4},
{0x42,0x03,0x5F,0x1A},
{0x9F,0x24,0x41,0x99},
{0xA3,0xDE,0x01,0x4F},
{0xA5,0x09,0x0A,0xA6},
{0x0B,0x3B,0x88,0x53},
{0x0F,0x78,0xC5,0x0C},
{0xB7,0xE2,0x23,0x2A},
{0xAB,0xD1,0x9B,0xE1},
{0xA5,0x16,0xEA,0x4B},
{0x83,0x36,0x67,0xAD},
{0xAF,0xC1,0xA6,0x0B},
{0x6B,0x7D,0x9C,0x3D},
{0xFC,0x07,0xF8,0xA5},
{0x14,0x31,0xAE,0xBB},
{0xBA,0xBB,0x1B,0xDB},
{0xFF,0xDC,0x4E,0x21},
{0xBC,0x7D,0x3E,0x55},
{0x35,0x32,0xCE,0x56},
{0x48,0xF2,0xAC,0x8E},
{0xAB,0xDE,0x62,0x6A},
{0x65,0x0E,0x97,0x22},
{0x77,0x31,0xBA,0x80},
{0x5D,0x33,0xAF,0xB7},
{0xCD,0x20,0xD1,0x51},
{0xAC,0x44,0xF8,0x67},
{0xFF,0xB3,0xA2,0x90},
{0x3A,0x4D,0xE1,0x56},
{0x64,0xC4,0x7C,0x82},
{0x01,0x47,0x30,0x55},
{0xC0,0x94,0xBC,0x92},
{0xCD,0x18,0x4C,0x29},
{0x11,0x03,0x65,0x6B},
{0x41,0x4A,0xC1,0xA7},
{0x52,0xB5,0xE4,0x9D},
{0xD8,0xF3,0xBB,0x5A},
{0x9E,0x23,0xBB,0x1B},
{0xE1,0xB5,0x0E,0x8C},
{0x7E,0xE4,0x9D,0xB4},
{0x0C,0xCC,0x62,0x1F},
{0xB1,0x8F,0x71,0x49},
{0x42,0x68,0x95,0x24},
{0x24,0x15,0xDB,0x34},
{0x61,0x28,0x74,0x22},
{0x86,0x94,0x36,0xE6},
{0x67,0x53,0x66,0x1A},
{0xC3,0x18,0x50,0x7E},
{0xA6,0x25,0x66,0x4A},
{0x07,0x52,0x4B,0x8B},
{0x48,0xE2,0xFD,0xEC},
{0x31,0x5B,0x68,0x5C},
{0xFC,0x35,0x9B,0x95},
{0x5D,0x23,0x12,0x53},
{0x4C,0xC1,0x75,0x7D},
{0x9B,0x75,0x9F,0xA5},
{0xF5,0x99,0xEA,0xD4},
{0xBE,0x6B,0x7E,0x95},
{0xDB,0x69,0x51,0xEB},
{0xFD,0x74,0xA4,0x52},
{0x73,0x36,0xFA,0x0D},
{0xA7,0x37,0x73,0xEA},
{0x5C,0x64,0x23,0x2E},
{0x19,0x11,0xBB,0x27},
{0x12,0x0B,0x9E,0x3D},
{0x40,0x24,0x28,0x8B},
{0x23,0x02,0x84,0xCA},
{0xC2,0x9F,0xB4,0xC5},
{0xB2,0xDB,0x0A,0xB0},
{0x3B,0xC4,0xBC,0x02},
{0xD7,0x90,0x4F,0x4B},
{0x31,0xC0,0x7B,0x4A},
{0x87,0xFE,0xC6,0x2C},
{0xE9,0xDB,0x77,0x7D},
{0x14,0xCE,0x13,0x5B},
{0x37,0x69,0xC8,0x8F},
{0x28,0xE4,0xD8,0xC1},
{0xFA,0x51,0x7D,0x9A},
{0x2F,0x0B,0x65,0x56},
{0x9A,0x4C,0x09,0x12},
{0xF9,0xA1,0x61,0x66},
{0xAD,0x89,0xA5,0x3C},
{0x3C,0x95,0x7F,0xE5},
{0x52,0x34,0xAC,0xA9},
{0x4C,0x6C,0x84,0xC5},
{0x0B,0xC2,0x36,0x95},
{0x9E,0x4D,0x48,0x1F},
{0x0B,0x73,0x1E,0x53},
{0xE0,0x5E,0x49,0x8C},
{0xD7,0x7B,0x1E,0xF8},
{0xA2,0x54,0x82,0x25},
{0xE5,0xD7,0xC8,0x97},
{0xDB,0x5F,0xBA,0xA5},
{0xEE,0x92,0xCE,0x0D},
{0x8C,0xF8,0x8C,0x5C},
{0xEC,0xA5,0xD5,0xB0},
{0x10,0x28,0x94,0x35},
{0xE8,0x8C,0x3D,0xE8},
{0xE1,0x8A,0xAF,0xC4},
{0xDC,0x1C,0xC0,0x30},
{0x84,0x4C,0x15,0x61},
{0xF9,0x2A,0x7F,0xC1},
{0xE3,0x76,0xB5,0x63},
{0x0A,0x5F,0xEB,0x36},
{0xFA,0x62,0x24,0x0D},
{0x8D,0x57,0x2B,0xBB},
{0x37,0x7E,0x6F,0x6E},
{0x62,0xBD,0x4C,0xC1},
{0x61,0xD0,0x68,0x17},
{0x73,0xB1,0x19,0x52},
{0x4B,0xB2,0x34,0x3A},
{0x3C,0x0D,0x14,0x18},
{0x2B,0x4F,0x27,0x00},
{0xD6,0x80,0x85,0x5E},
{0x13,0x75,0x31,0x0B},
{0x77,0x71,0x16,0x8A},
{0x3B,0xE7,0x55,0xBE},
{0x5C,0xB1,0xDD,0x41},
{0x0A,0xDF,0xD6,0xDB},
{0xE3,0x96,0xC7,0x8D},
{0xC0,0x73,0x9E,0x99},
{0x2A,0xD8,0x1A,0xD1},
{0x97,0xDF,0xAF,0xA7},
{0xC7,0xF7,0x58,0x5A},
{0x93,0xD6,0x25,0x5D},
{0xBD,0x47,0x9F,0x49},
{0x3B,0xA3,0xCE,0x97},
{0xFA,0xEB,0xAB,0x03},
{0x73,0x5F,0xD3,0x74},
{0x55,0x97,0x98,0x28},
{0x77,0x7C,0x96,0x89},
{0x4C,0x58,0x5A,0xB5},
{0xBD,0x56,0xC0,0xEF},
{0x83,0x5E,0x6D,0xCA},
{0x94,0xEC,0xC5,0x46},
{0x2D,0x6A,0x80,0x54},
{0x6C,0xF8,0x82,0xE5},
{0x93,0x48,0x12,0x8B},
{0x19,0x78,0x26,0xBF},
{0xF5,0x28,0x5F,0x84},
{0x48,0xBD,0x5B,0x62},
{0x7A,0xD7,0xD0,0x88},
{0x6B,0x10,0xDC,0xA5},
{0xB5,0x6E,0xD1,0xC2},
{0x9D,0x2E,0xD1,0xB9},
{0x27,0xFD,0x5D,0x39},
{0x06,0x5E,0x55,0x7E},
{0x79,0xE1,0x28,0x98},
{0x8C,0xE1,0x29,0x7C},
{0xB0,0xBA,0xA0,0xAA},
{0x9D,0x23,0x65,0xDB},
{0xD5,0x6F,0x35,0xF8},
{0xF7,0xC7,0x96,0x95},
{0xB1,0xC2,0x59,0xE4},
{0x86,0x61,0xA9,0xCA},
{0x6E,0xC4,0x83,0x96},
{0x76,0xB5,0xB3,0x54},
{0x08,0x06,0x64,0xCE},
{0x71,0x77,0x02,0xFE},
{0x38,0xA3,0x46,0xBC},
{0xFA,0x3F,0x93,0xD5},
{0x01,0x11,0x95,0xE9},
{0xF6,0x59,0xF0,0x92},
{0x89,0x0C,0xCA,0x7B},
{0xB6,0x3A,0x4B,0x18},
{0x64,0x7E,0xC2,0x9B},
{0x4F,0x10,0x92,0xCF},
{0x80,0x1B,0xFC,0x8B},
{0x08,0x91,0xD8,0x56},
{0x11,0xDB,0x08,0xC0},
{0xE9,0x1F,0x40,0x72},
{0x52,0xFC,0xC9,0x1C},
{0xFC,0xBD,0x74,0x40},
{0x3F,0xF2,0xC1,0x68},
{0xEC,0x4D,0x80,0x4E},
{0x32,0x7B,0xD6,0x1A},
{0x04,0xA7,0xAE,0x2C},
{0x46,0x70,0x45,0xBE},
{0xCC,0xE4,0x96,0x0C},
{0x4E,0xE4,0x1C,0x09},
{0x29,0x89,0x91,0xEC},
{0xEB,0x27,0xFC,0x21},
{0x7F,0x9D,0x4E,0x13},
{0x1A,0x95,0xAF,0x1C},
{0xAA,0x20,0x08,0x34},
{0x94,0x86,0xEC,0xE0},
{0xE7,0xF1,0xD8,0xF2},
{0xDC,0x09,0x81,0xB6},
{0xED,0x94,0xDC,0x21},
{0x46,0x0A,0xC1,0x5A},
{0xDF,0x55,0x0D,0x5F},
{0xD0,0xB1,0x8B,0x73},
{0x64,0x05,0xE4,0xAE},
{0xDD,0x5E,0x31,0xFB},
{0x56,0x27,0xBA,0x7A},
{0x7A,0xEE,0x6E,0xC6},
{0xCE,0xEF,0x00,0xAD},
{0xC2,0x1A,0xF8,0xF9},
{0x37,0xAC,0x82,0xEA},
{0xB5,0xE5,0x4D,0x70},
{0x1A,0x86,0x8C,0x44},
{0x0B,0x0B,0x83,0x5C},
{0x09,0x11,0xF3,0x05},
{0x69,0xFD,0x7C,0xBB},
{0x0E,0x50,0xE6,0x1C},
{0x1E,0x0A,0x8E,0x3D},
{0x52,0x56,0x9C,0x74},
{0xBB,0x42,0x4A,0xD0},
{0x34,0x3C,0x66,0x87},
{0x1D,0xCE,0x44,0x35},
{0x50,0xFA,0xA1,0x4A},
{0x03,0x8E,0x90,0x4A},
{0xC9,0x9C,0x6D,0x15},
{0x5D,0xF8,0x30,0xA5},
{0x0E,0xA9,0x04,0xAD},
{0x63,0x15,0x4A,0x76},
{0x85,0x8D,0x04,0x18},
{0x26,0x27,0xFC,0x71},
{0xA7,0x55,0x00,0x69},
{0x60,0xFD,0x05,0x37},
{0x52,0xA6,0x92,0xF1},
{0xB3,0x64,0x75,0x54},
{0xB6,0xFC,0x7A,0x14},
{0x4A,0xFF,0x82,0x39},
{0x1C,0x79,0x40,0x69},
{0x9D,0x0A,0x62,0xBA},
{0xD0,0xB1,0x8E,0xBC},
{0xEF,0x23,0x8D,0x46},
{0x8D,0xC3,0x2E,0x10},
{0x1B,0x1E,0xB3,0x33},
{0x10,0x29,0x34,0x42},
{0xB3,0xF6,0x99,0x94},
{0xE0,0xF2,0x4E,0xCD},
{0xE6,0x84,0xB4,0x26},
{0xE7,0x8A,0x2C,0xA6},
{0x6C,0x97,0xEC,0x8D},
{0xDC,0x21,0x84,0x2D},
{0x2F,0x59,0xE5,0x28},
{0xCC,0xD4,0x5F,0xD3},
{0x23,0xA8,0xB3,0x85},
{0xE3,0x4B,0x90,0x2D},
{0x79,0x8F,0x52,0xBB},
{0x66,0x81,0xE5,0xC0},
{0xEB,0xB0,0x16,0xF9},
{0x5C,0x6E,0x43,0x8A},
{0xA1,0xBB,0xAF,0x30},
{0x2D,0x26,0xA9,0x6F},
{0x9D,0xB6,0x8B,0xEA},
{0x42,0xD4,0xED,0x3E},
{0x6D,0x18,0x2C,0xA4},
{0xCE,0x37,0xCC,0x58},
{0x6D,0x91,0xA9,0x77},
{0x62,0xA1,0x79,0xA3},
{0x0F,0xE6,0x66,0x1F},
{0x98,0x6C,0xEC,0xFA},
{0x95,0x0E,0x6C,0xB8},
{0x91,0xA0,0xC6,0xF6},
{0x1C,0xE0,0x5A,0x92},
{0xF0,0x32,0xCF,0x85},
{0xC8,0x25,0x88,0xB4},
{0x16,0x22,0x6E,0xFA},
{0x70,0xC6,0xE6,0x4D},
{0xA5,0x4D,0x80,0x74},
{0xAC,0x4E,0x9D,0xBE},
{0xDE,0x58,0x1B,0x41},
{0x2C,0x76,0xD3,0x3E},
{0x3D,0x39,0x21,0x2F},
{0x0D,0x3D,0xD7,0xA6},
{0x17,0x87,0xC4,0xF0},
{0xB0,0x03,0x2D,0xCA},
{0x80,0xE1,0xEC,0x4C},
{0x6A,0x0F,0xCF,0xE5},
{0x3E,0x1F,0x89,0xCB},
{0x20,0x43,0xF0,0x34},
{0xBF,0x33,0x78,0xDB},
{0x83,0x46,0xE8,0xEB},
{0x87,0x64,0x97,0xDB},
{0x4A,0x40,0xE9,0x39},
{0xE1,0xD6,0x9F,0xBD},
{0x69,0xFE,0x5F,0x82},
{0x87,0xA8,0x27,0x02},
{0x4D,0x3E,0x6C,0x8C},
{0x61,0x1C,0xBD,0x51},
{0x88,0xF9,0x2D,0x32},
{0x95,0x38,0x76,0x4F},
{0x87,0x7E,0x02,0x5D},
{0x9D,0x57,0x73,0x9E},
{0x38,0x2D,0xB6,0xB4},
{0xE7,0x7D,0x7E,0xC1},
{0x72,0xB1,0x56,0x26},
{0xBB,0xE7,0x2E,0x4F},
{0x7F,0xCA,0xD7,0x40},
{0x35,0x57,0x9C,0x0E},
{0xCB,0x71,0x39,0xAC},
{0x3C,0x85,0xB5,0x95},
{0x34,0xDA,0x9C,0xC4},
{0x12,0x01,0x24,0x94},
{0xBA,0x19,0x6F,0x5D},
{0xAD,0xB1,0xF4,0xCF},
{0xA3,0x29,0x13,0x30},
{0xFF,0x05,0x7D,0xDB},
{0x52,0xEA,0xCC,0x48},
{0xC4,0x5A,0x19,0x50},
{0x74,0xA5,0x9A,0x3F},
{0x73,0x2E,0xDC,0x50},
{0x0D,0x77,0x59,0xE8},
{0x20,0xA2,0xBB,0x1D},
{0xCF,0x68,0xF4,0xB4},
{0xD8,0x81,0x95,0xF9},
{0xAF,0x02,0x2C,0xE1},
{0x1B,0x33,0x0F,0x5D},
{0xCA,0x17,0xAA,0xCA},
{0x0F,0x0C,0xB5,0x9F},
{0xAC,0x1D,0x36,0x05},
{0x84,0x85,0xAF,0x15},
{0x5C,0x01,0xAA,0x4F},
{0x56,0x1A,0x2F,0x7B},
{0x53,0xE4,0x72,0x92},
{0x7D,0xDE,0xC6,0x89},
{0xAC,0xF4,0xCB,0x47},
{0xC6,0x86,0xD9,0xCC},
{0x14,0x12,0x85,0xE2},
{0xE8,0x2C,0x5E,0x44},
{0x6C,0x09,0x28,0xAD},
{0x5E,0x8B,0xB9,0x22},
{0x33,0x1C,0xD1,0x4B},
{0x45,0x3B,0x8C,0xE8},
{0x4E,0xF5,0xBD,0x55},
{0xD9,0x93,0xDD,0xA4},
{0xB9,0x99,0x85,0x36},
{0xA1,0x44,0x50,0x94},
{0x76,0x94,0xFF,0xD6},
{0x2B,0x2E,0x0D,0x4B},
{0x21,0x5D,0x3D,0xC0},
{0xC9,0xDE,0xCC,0x6B},
{0x5E,0x18,0x32,0xEB},
{0x8E,0x6C,0x0B,0x8E},
{0xC5,0x14,0x17,0x41},
{0x6F,0xBC,0x1B,0xFF},
{0xE4,0xAC,0x76,0x35},
{0xE4,0x7E,0x51,0x26},
{0x29,0xD0,0x66,0x92},
{0x10,0x08,0x5A,0x98},
{0x9F,0x5C,0x84,0xEA},
{0x55,0x87,0x17,0xB2},
{0x4E,0x98,0x40,0xE8},
{0xDD,0x20,0x56,0x4F},
{0xA9,0x33,0x18,0xF8},
{0x50,0xCA,0x68,0xEB},
{0x64,0xAD,0xFE,0xCF},
{0x8C,0x49,0x61,0x9A},
{0x8B,0x6E,0x1B,0x07},
{0xFA,0xB6,0xD3,0x9E},
{0x2B,0x0C,0x49,0xC2},
{0x8C,0x7D,0xE7,0xD8},
{0x0F,0x96,0xF2,0xD5},
{0x77,0x97,0xD6,0x40},
{0x25,0x1B,0x84,0x5F},
{0x68,0x4C,0x67,0xCB},
{0x2F,0xDF,0x1B,0xC7},
{0x73,0x8B,0xBC,0x1D},
{0x3A,0xAE,0x03,0x94},
{0x79,0x7B,0xFB,0x2F},
{0xE4,0x0B,0xDD,0xD3},
{0x0E,0x7E,0x00,0x01},
{0x80,0x29,0xE2,0x54},
{0x79,0xED,0x9A,0x45},
{0x5F,0xA9,0x78,0xEC},
{0x7E,0xD4,0x95,0x0B},
{0xBB,0xB9,0xD1,0x23},
{0xC5,0x03,0x82,0x5E},
{0xBC,0xD7,0x24,0xD8},
{0xF0,0x49,0xD4,0x12},
{0xD0,0x81,0x61,0xF1},
{0x0B,0x4A,0xE7,0x81},
{0x91,0xED,0x9F,0xA8},
{0x38,0x76,0x27,0xFC},
{0x89,0xF6,0x22,0xCA},
{0xD4,0xBA,0x55,0x93},
{0xFF,0x89,0x49,0xDA},
{0x57,0x6A,0xCE,0x36},
{0x14,0x9F,0x81,0xA3},
{0xB3,0xC0,0xE6,0xAE},
{0xB6,0x7E,0xB2,0x3A},
{0x04,0x7A,0x44,0x97},
{0xA3,0xDA,0xA3,0x1F},
{0x40,0x4E,0x61,0x26},
{0x52,0xFD,0xDA,0xD7},
{0x45,0x23,0x79,0x49},
{0x89,0xFE,0x93,0x3C},
{0x8B,0xC1,0xE6,0xB2},
{0xCA,0xA9,0xF1,0x8F},
{0x86,0x87,0xC5,0xCB},
{0xEB,0xB1,0x0C,0xA1},
{0xC5,0xDA,0xCE,0xB3},
{0xE8,0xBE,0xBD,0x07},
{0x94,0x85,0xF3,0x40},
{0xA4,0xC4,0x94,0x34},
{0xBB,0x60,0x8D,0x03},
{0x8C,0xA0,0x8E,0x58},
{0x35,0xAC,0xC4,0xAD},
{0x4A,0xA2,0x9A,0xB6},
{0xE8,0xDD,0xF4,0x22},
{0x60,0xBB,0x22,0xDC},
{0xAB,0x11,0x31,0x12},
{0x94,0x78,0x05,0xC3},
{0x16,0x6A,0x79,0x6C},
{0x24,0xD9,0x33,0x41},
{0x83,0xEA,0x20,0x0A},
{0x14,0x9E,0xB7,0xC8},
{0x8A,0x9C,0x13,0x99},
{0x71,0xA3,0x09,0xD2},
{0x66,0x21,0x7D,0x84},
{0xD0,0xA7,0xC6,0xAD},
{0xCD,0x48,0xFB,0x84},
{0x30,0x42,0xEB,0x5F},
{0xD1,0xD4,0x47,0x39},
{0x3B,0x49,0xBD,0x36},
{0x21,0xEE,0x12,0xF4},
{0xB0,0xD5,0x5B,0x64},
{0xA1,0x0E,0xB1,0x22},
{0xE3,0x3A,0xF6,0x52},
{0x36,0xC1,0x15,0x59},
{0x7A,0x00,0x68,0xE8},
{0x1B,0x4B,0xAD,0xD6},
{0x0A,0x59,0x54,0x2B},
{0xB0,0x9C,0x9E,0x0E},
{0xFA,0x4F,0x39,0x78},
{0x4B,0xF6,0xB4,0xAC},
{0xD4,0xD9,0xC9,0xBB},
{0x64,0x8C,0xD3,0xCE},
{0xE4,0xF5,0x5A,0x2C},
{0xAA,0x40,0x7C,0x4C},
{0x19,0x55,0x5A,0xD7},
{0xA1,0xFD,0xB4,0x91},
{0x44,0x7B,0x67,0xEC},
{0x36,0x99,0x9C,0xF2},
{0xDF,0x6B,0x97,0x1A},
{0xBC,0xF1,0x0A,0x46},
{0x96,0x57,0xD0,0xF5},
{0x73,0x15,0x2A,0x7F},
{0x55,0xAC,0x29,0x76},
{0x0F,0xDF,0x28,0x3C},
{0x89,0xA5,0x14,0xD4},
{0xE2,0x83,0x71,0xCE},
{0x64,0x81,0x14,0xAF},
{0xFA,0xB6,0x05,0x76},
{0x6F,0x59,0xD0,0xF8},
{0x61,0xFA,0x17,0xC9},
{0xED,0xE7,0x5A,0xE7},
{0x60,0x2E,0x64,0x6A},
{0x79,0x12,0xCD,0xEF},
{0xDA,0xBB,0x63,0x35},
{0x4E,0xCD,0x58,0x46},
{0xCF,0x12,0x61,0x6E},
{0x6E,0xA6,0x62,0xED},
{0xF1,0x61,0xDF,0xF3},
{0xB5,0x8A,0x94,0x15},
{0xAA,0x1D,0xBB,0x31},
{0xDD,0x1F,0xD9,0x49},
{0xDB,0xDB,0xCF,0xF4},
{0x6E,0xBF,0xDB,0xE9},
{0x0B,0xAC,0x90,0xDF},
{0x63,0x4D,0xC2,0x1A},
{0xE4,0x1E,0xDB,0xF4},
{0xB8,0x03,0x85,0x2A},
{0x9D,0x72,0x3D,0x2B}
};

