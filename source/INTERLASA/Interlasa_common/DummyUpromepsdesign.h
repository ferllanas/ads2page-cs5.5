/***********************************************************************************************]
 [#text:        * Name        : ] upromepsdesign.h
 [#text:      * Design      : ] SwInterlasa
 [#text:   * Created By  : ] Sentinel UltraPro v1.0.0
 [#text: * Description : Include File for Toolkit design strategy.]
 [#text:     * Purpose     : This file is required to include in program used to design strategy created by Toolkit.]

 [#text:   * Generated   : ] 09/20/2006 (MM/DD/YYYY)

 [#text:     * WARNING - this file is generated, any edits might be overwritten!]
 [#text:               ***********************************************************************************************/

#include "upromeps.h"

#define DESIGNID                                                             0xBED0
#define SP_A_INTEGER                                                         0x20008

