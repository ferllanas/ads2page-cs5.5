/*
//	La fucion de este codigo es la de traer las imagenes que no se hayan encontrado durante la funcion  flui
//	de ad2Page 2.0, o aquellas que hayan cambiado.
//	Primero verifica que existan documentos en frente, una vez que los encuentre verifica que 
//	se encuentre una capa update, despues obtiene el texto del textframeUpdate
//	para separar su contenido y guardarlo en la estructura Update.
//	una vez realizado lo anterior verifica los IDs de lo grafics frames si existen verifica la fecha de
//	ultimo acceso para ver si se debe o no fluir denuevo la imagen.
//
*/
#include "VCPlugInHeaders.h"

// IDs:
#include "DocumentID.h"
#include "LayoutID.h"
#include "OpenPlaceID.h"
#include "SpreadID.h"
#include "SplineID.h"
#include "TextID.h"

// Interface includes:

#include "IBoolData.h"
#include "ICommandSequence.h"
#include "ICommand.h"
//#include "ICreateFrameData.h"
#include "IComposeScanner.h"
#include "IDocument.h"
#include "IDocumentLayer.h"
#include "IDatabase.h"
#include "IFrameList.h"
#include "IFrameListComposer.h"
#include "IFrameData.h"
#include "IFrameListComposer.h"
#include "IFrameContentUtils.h"
#include "IGraphicFrameData.h"
#include "IGraphicAttributeUtils.h"
//#include "IGraphicMetaDataUtils.h"
#include "IGraphicStateRenderObjects.h"
#include "IGraphicStateUtils.h"
#include "IGraphicAttrIndeterminateData.h"
#include "IGraphicMetaDataObject.h"
#include "IHierarchy.h"
#include "IHierarchyCmdData.h"
#include "IImportImageCmdData.h"//atributos de texto
#include "IImportManagerOptions.h"
#include "IImportProvider.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IKentenStyle.h"
#include "ILayerList.h"
#include "ILayoutCmdData.h"
#include "ILayoutControlData.h"
#include "INewPageItemCmdData.h"
#include "INewLayerCmdData.h"
#include "IPageItemTypeUtils.h"
//#include "IPageItemSelector.h"
#include "IPlaceGun.h"
#include "IPlaceBehavior.h"
#include "IPathUtils.h"
#include "IPlacePIData.h"
#include "IPMUnknownData.h"
#include "IPersistUIDData.h"
//#include "IPersistUIDRefData.h"
#include "IReplaceCmdData.h"
#include "ISession.h"
//#include "ISelection.h"
//#include "ISpecifier.h"
#include "ISpreadList.h"
#include "ISpread.h"
#include "ISwatchList.h"
#include "ISwatchUtils.h"
#include "IStrokeChangePreference.h"
#include "IScrapItem.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "ITextAttrUtils.h"//atributos de texto
#include "IUIDData.h"
#include "CPMUnknown.h"
#include "IGraphicFrameData.h"


// General includes:
#include "ILayoutUIUtils.h"
#include "CmdUtils.h"
#include "SysFileList.h"
#include "ErrorUtils.h"
#include "PersistUtils.h"
#include "UIDList.h"
#include "TransformUtils.h"
#include "IFrameUtils.h"
//#include "FrameUtils.h"
#include "IImageUtils.h"
#include "PreferenceUtils.h"
#include "WideString.h"
#include "Utils.h"
#include "AttributeBossList.h"
#include "CAlert.h"
//#include "Trace.h" // Debugging.
#include "UIDRef.h"
#include "IPathUtils.h"
#include "StreamUtil.h"//Para convertir file a STream
#include "K2Vector.tpp"
#include "GlobalTime.h"
#include "SDKLayoutHelper.h"
#include "FileUtils.h"
#include "IControlView.h"
#include "IDataLinkHelper.h"
#include "IDataLinkReference.h"
#include "IDataLink.h"
#include "SDKFileHelper.h"
#include "ILinkManager.h"
#include "ILinkUtils.h"
#include "LinkQuery.h"
#include "IDataBase.h"

//Proyect Includes
#include "PnlTrvUtils.h"
#include "A2PImpID.h"
//#include "InterlasaRegEditUtilities.h"

#include "IPanelControlData.h"
#include "Utils.h"
#include "IPalettePanelUtils.h"
#include "ITextControlData.h"
#include "PMString.h"
#include "PMReal.h"
#include "ITextValue.h"

#include "ImportImagenClass.h"
#include "IActualizarFunction.h"
#include "IA2PFluirFunction.h"
#include "A2PImpStructPreferencias.h"

#ifdef MACINTOSH 
	#include "A2PPrefID.h"
	#include "IA2PPreferenciasFunciones.h"
	#include "A2PPreferencias.h"
	#include "InterlasaUtilities.h"
#endif

#ifdef WINDOWS 
	#include "..\A2P_PREFERENCIAS\A2PPrefID.h"
	#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
	#include "..\A2P_PREFERENCIAS\A2PPreferencias.h"
	#include "..\Interlasa_common\InterlasaUtilities.h"
#endif
#include "A2PAvisosHeader.h"




/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	@author Juan Fernando Llanas Rdz
*/
class A2PActualizarFunctionSite : public CPMUnknown<IA2PActualizarFunction>
{
public:
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	A2PActualizarFunctionSite (IPMUnknown *boss);

	/** 
			Destructor.
	*/
	virtual ~A2PActualizarFunctionSite(void);
	
	/**
	*/
	bool16 Actualizar();
	
	bool16 DELETE_AvisosYEtiquetasParaUpdateGeometria();

	/*
	 Metodo que revisa si se debe de actualizar algo de la fluida del A2P
	 */
	virtual bool16 RevisarActualizacionDAvisos();
	virtual bool16 RevisarModificacionParaBlinkAvisoDeCambio(const bool16& act);
	
	virtual bool16 getTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMString& value);
	virtual bool16 setTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMString& value);
	
	virtual bool16 setValueOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMReal& value);
	virtual bool16 getValueOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMReal& value);
	//DECLARE_HELPER_METHODS()
private:
	
	/**
		Funcion que activa la capa con un determinado nombre.
	
		@param NomCapa, variable del tipo PMString que contien el nombre de la capa que se desea activar.
		*/
		void ActivarCapa(PMString NomCapa);

		/**
			Esta funcion no se utiliza actualemente y sera comentada asi como su codigo sera eliminado
			Funcion que obtiene el texto que se encuentra dentro del TextFrameUpdate
			Regresa el contenido del frame
			
		PMString ObtenerTextoUpdate();
		*/
		
		
		/**
			Funcion que obtien el UIDRef de la capa deseada.
			
			  @param NomCapa, variable del tipo PMString que contien el nombre de la capa que se desea activar.
		*/
		UIDRef UIDRefCapa(PMString NomCapa);

		/**
			Funcion que verifica y devuelve si se encuentran abiertos algun documento.
			Devuelve un valor verdadero en caso de que existan documentos.
		*/
		bool16 DocumentosAbiertos();

		/**
			Funcion que regresa la cantidad de avisos a refrescar.
			
			  @param TextoUpdate es la cadena de caracteres que se encontro dentro del texto Update.
		*/
		int32 NumAvisoUpdate(PMString TextoUPdate);

		/**
			Funcion que llena la estructura Update a partir del texto Update(Texto del frame update).
			
			  @param StructUpdate, Estructura del tipo StructCamposUpdate, la cual sera llenada a partir del texto update.
			  @param TextoUpdate, Cadena de caracteres que conmtiene los campos de los avisos a refrescar.
		*/
		void LlenarStructUpdate(StructCamposUpdate *StructUpdate,PMString TextoUpdate);

		/**
			Obtiene la lista de GraficFrame.
			Regresa un UIDList
		*/
		UIDList TomarListaDeRectangulos();

		/**
		*/
		ErrorCode CreateFrame(const UIDRef& contentUIDRef, UIDRef& frameUIDRef);

		/**
			Funcion que ajusta la imagen deacuerdo a preferencias.
			
			  @param contentUIDRef, es la referencia de la imagen.
		*/
		void AjustarImagen(UIDRef& contentUIDRef,PMString Ajuste);

		/**
			Funcion que obtien una estructura de time tm a partir de una cadena de caracteres.
			Esta funcion descompone por campos y los va metiendo en la estructura tm.
			
			  @param timeAviso, cadena de caracteres que contiene una fecha.
		*/
		struct tm convertime(PMString timeAviso);

		/**
			Funcion que compara dos fechas y determina si se debe fluir o no la nueva imagen.
			
			  @param timeAviso, fecha del aviso que se importo por primera vez.
			  @param timeNewAviso, fecha del nuevo aviso que se desea a volver a importar.
		*/
		bool16 comparaFecha(struct tm &timeAviso,struct tm &timeNewAviso);


		/**
					Metodo que busca el archivo correspondiente si no lo encuentra lo buscara por otro tipo de
					extencion.
		*/
		bool16 ExisteArchivoParaLectura(PMString *filePath);

		

		/**
			Cuenta la cantidad de avisos se se encuentran en el archivo de entrada.
		*/
		int32 CantidadAvisos(PMString NomArchivo);

		/**
			separa y almacena de el campo y numero de campo
		*/
		int CampoAGuardar(int32 numCampo,Preferencias &Preferencia);
		
		void AplicarFondoRecImagen(Preferencias &Prefer,bool16 CPublicidad,UIDRef textFrameUIDR);
		
		void AplicarColorMarcoRecImagen(Preferencias &Prefer,bool16 CPublicidad,UIDRef textFrameUIDR);
		
		bool16 UpdateDocumentByUpdateFileImported(IDocument *Document);

	/*///////////////////////////////////////////////////////////////////////*/

		struct tm timeAviso;///structura tiempo de aviso del tipo tm
		struct tm timeNewAviso;//structura del nuevo aviso del tipo tm
		/////////////////////////////////////////////

	
		//PnlTrvDataModel fmodel;//
		Preferencias Preferencia;//ESTRUCTURA PREFERENCIAS
		
		
	bool16 VerificaSiActualizaPorGeometria(IDocument* document);
	
	bool16 VerificaSiActualizaAlgunClasidesplegado(IDocument* document);
	
	bool16 showhidden_widgetOnPalette(const WidgetID& panelWidgetID, const WidgetID& widgetID,const bool16& showpi);
		

};


//DEFINE_HELPER_METHODS(A2PActualizarFunctionSite)

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(A2PActualizarFunctionSite, kA2PImpActualizarFunctionImpl)


/* A2PActualizarFunctionSite Constructor
*/
A2PActualizarFunctionSite::A2PActualizarFunctionSite(IPMUnknown* boss) :CPMUnknown<IA2PActualizarFunction>(boss)
{
}

/* A2PActualizarFunctionSite Destructor
*/
A2PActualizarFunctionSite::~A2PActualizarFunctionSite(void)
{
}


/**
*/
bool16 A2PActualizarFunctionSite::Actualizar()
{
	bool16 retval=kTrue;
	do
	{
	
		//actualizar reseteando la fecha para el blink alert
		if(MarcoDebug == kTrue){CAlert::InformationAlert("this->RevisarModificacionParaBlinkAvisoDeCambio(kTrue);");}
	
		SDKLayoutHelper layoutHelper;
		
		if(!DocumentosAbiertos())//verifica si se encuentra documentos aviertos
		{
			CAlert::ErrorAlert(kA2PImpDontDocumentfrontStringKey);
			retval=kFalse;
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutControlData == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			break;
		}
		
		InterfacePtr<IHierarchy> activeSpreadLayerHierarchy(layoutControlData->QueryActiveLayer());
		ASSERT(activeSpreadLayerHierarchy != nil);
		if (activeSpreadLayerHierarchy == nil) {
			break;
		}
		UIDRef parentUIDRef = ::GetUIDRef(activeSpreadLayerHierarchy);

		// Transform the bounds of the frame from page co-ordinates
		// into the parent co-ordinates, i.e. the spread.
		ASSERT(layoutControlData->GetPage() != kInvalidUID);
		if (layoutControlData->GetPage() == kInvalidUID) {
			break;
		}
		UIDRef pageUIDRef(parentUIDRef.GetDataBase(), layoutControlData->GetPage());
		
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		//
		//
		//
		//
		//UIDRef LayerUpdate=UIDRefCapa("Update");//obtien la referencia de la capa Update
		//if(LayerUpdate==nil)//
		//{
		//	CAlert::ErrorAlert(kA2PImpNoExisteCajaUpdateStringKey);
		//	retval=kFalse;
		//	break;
		//}
		//ActivarCapa("Update");//activa capa Update
		//PMString TextoUpdate=ObtenerTextoUpdate();//obtien el texto que se encuentra sobre el TextframeUpdate
		//////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		//CAlert::InformationAlert("0");
		if(UpdateDocumentByUpdateFileImported(document))
		{
			this->RevisarActualizacionDAvisos();
			CAlert::InformationAlert(kA2PImpActualizarAFinalizadoStringKey);
			break;
		}
		else
		{
			//CAlert::InformationAlert("1");
		
			PMString TextoUpdate="";//ObtenerTextoUpdate();//obtien el texto que se encuentra sobre el TextframeUpdate
		
			//CAlert::InformationAlert("2");
			TextoUpdate = InterlasaUtilities::GetXMPVar("A2PTextUpdate", document);
		    PMString fromPath="";  //añadido para colocar la informacion en la caja
		
			//CAlert::InformationAlert("3");
			if(TextoUpdate.NumUTF16TextChars()<2)
			{
				//CAlert::ErrorAlert(kA2PImpNoExisteCajaUpdateStringKey);
				retval=kFalse;
				break;
			}
		
			//CAlert::InformationAlert("4");
			int32 ContAvisos = NumAvisoUpdate(TextoUpdate);//Determina la cantidad de avisos a fluir
		
	
			//CAlert::InformationAlert("5");
			PMString PathPwd = PnlTrvUtils::CrearFolderPreferencias(); 
		
			//CAlert::InformationAlert("6");
			PathPwd.Append("Default.pfa");
	
			InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
			(
				kA2PPreferenciasFuncionesBoss,	// Object boss/class
				IA2PPreferenciasFunctions::kDefaultIID
			)));
		
		
			if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Preferencia,kFalse)==kFalse)//leer documento de preferencias
			{
				CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
				break;
			}
			//CAlert::InformationAlert("01");
			StructCamposUpdate *StructUpdate=new StructCamposUpdate[ContAvisos+1];//crea la estructura StrucUpdate para guardar despues los campos de cada aviso
			//CAlert::InformationAlert(TextoUpdate);
			LlenarStructUpdate(StructUpdate,TextoUpdate);//llenado la estructura Update
			//CAlert::InformationAlert("02");
			
			TextoUpdate="";
			for(int32 Avisonum=0;Avisonum<=ContAvisos;Avisonum++)//ciclo para compara el rectangulo actual con el UID del rectangulo de los avisos de la Estructura Update
			{	
				//PMString A="";
				//A.AppendNumber(StructUpdate[Avisonum].ID.GetAsNumber());
				//A.Append(",");
				//A.Append(StructUpdate[Avisonum].fecha);
				//CAlert::InformationAlert(A);
				int32 AvisoIDEntero=StructUpdate[Avisonum].ID.GetAsNumber();//ogtengo el UID del aviso de la en EstructuraUpdate[AvisoNum]
						
				UID UIDFrame=AvisoIDEntero;
					
				UIDRef  UIDRefFrame=UIDRef::gNull;
				UIDRefFrame=UIDRef(db,UIDFrame);
			
				InterfacePtr<IGraphicFrameData> graficFrame(UIDRefFrame,UseDefaultIID());
				if(UIDRefFrame!=nil && UIDRefFrame!=UIDRef::gNull && graficFrame!=nil)
				{
			
					bool16 updatePorFecha = kFalse;
				
					InterfacePtr<IHierarchy> graphicFrameHierarchy(UIDRefFrame, UseDefaultIID());
					if(graphicFrameHierarchy!=nil)
					{
						if(graphicFrameHierarchy->GetChildCount()>0)
						{
							for(int32 FrameContentImage=0;FrameContentImage<graphicFrameHierarchy->GetChildCount();FrameContentImage++)
							{
								UIDRef refFrameImagen(db,graphicFrameHierarchy->GetChildUID(FrameContentImage)); 
								InterfacePtr<IHierarchy> graphicContenFrameImageHierarchy(refFrameImagen, UseDefaultIID());
								if(graphicContenFrameImageHierarchy!=nil)
								{
									//CAlert::InformationAlert("update por fecha de imagen<");
									updatePorFecha = kTrue;
									/*if(graphicContenFrameImageHierarchy->GetChildCount()>0)
									{//si tiene la imagen hacer update por la fecha
										//CAlert::InformationAlert("update por fecha de imagen<");
										updatePorFecha = kTrue;
									}
									else
									{//no tiene imagen
										CAlert::InformationAlert("update por que no existe imagen");
										 updatePorFecha = kFalse;
									}*/
								}
								else
								{
									 updatePorFecha = kFalse;
									//CAlert::InformationAlert("update por que no existe imagen");
								}
							
							}
						}
						else
						{//no tiene imagen
							//CAlert::InformationAlert("update por que no existe recuadr de imagen");
							 updatePorFecha = kFalse;
						}
					}
					else
					{
						//CAlert::InformationAlert("no tiene gerarquia el rectangulo principal");
						 updatePorFecha = kFalse;
					}
				
					if( updatePorFecha == kTrue)
					{
						if (StructUpdate[Avisonum].Ruta.IsEmpty() == kFalse)//Verifica si tien ruta este aviso
						{
							
							//CAlert::ErrorAlert("ZZZZ");
						
							//PMString fromPath;	//Direccion o path del aviso declarado arriba
                           	PMString Archivo;	//direccion del Archivo
							fromPath=StructUpdate[Avisonum].Ruta;//copio el path
							#ifdef MACINTOSH
								if(fromPath.Contains("\\\\"))
									SDKUtilities::Replace(fromPath,"\\\\","\\");
									
								SDKUtilities::convertToMacPath(fromPath);

								SDKUtilities::convertToMacPath(fromPath);
							#else // we must have a windows path now...
								if(!(fromPath.Contains(":\\") || fromPath.Contains(":\\\\")))
									SDKUtilities::convertToWinPath(fromPath);
							#endif

							//CAlert::InformationAlert(fromPath);
							Archivo=fromPath.GrabCString();//copio el path
							
							if(!ExisteArchivoParaLectura(&fromPath))
								break;
								
							Archivo=fromPath.GrabCString();
							do 
							{
								if(StructUpdate[Avisonum].fecha.yyy!=0)//si es cero significa que no se encontro la imagen por lo tanto se fluye directamente
								{			
									
									FechaData TimeLast = StructUpdate[Avisonum].fecha;
									FechaData Timenow;
									PnlTrvUtils::ProporcionaStructFechaDataDelArchivo(Archivo,&Timenow);
									
									/*PMString AS="";
									AS.AppendNumber(StructUpdate[Avisonum].fecha.yyy);
									AS.Append("-");
									AS.AppendNumber(StructUpdate[Avisonum].fecha.mm);
									AS.Append("-");
									AS.AppendNumber(StructUpdate[Avisonum].fecha.dd);
									AS.Append("-");
									AS.AppendNumber(StructUpdate[Avisonum].fecha.hr);
									AS.Append("-");
									AS.AppendNumber(StructUpdate[Avisonum].fecha.min);
									AS.Append("-");
									AS.AppendNumber(StructUpdate[Avisonum].fecha.sec);
									AS.Append("\n");
									AS.AppendNumber(Timenow.yyy);
									AS.Append("-");
									AS.AppendNumber(Timenow.mm);
									AS.Append("-");
									AS.AppendNumber(Timenow.dd);
									AS.Append("-");
									AS.AppendNumber(Timenow.hr);
									AS.Append("-");
									AS.AppendNumber(Timenow.min);
									AS.Append("-");
									AS.AppendNumber(Timenow.sec);
									CAlert::InformationAlert(AS);*/
								
									GlobalTime LastTimeGL;
									LastTimeGL.SetTime(TimeLast.yyy,TimeLast.mm,TimeLast.dd,TimeLast.hr,TimeLast.min,TimeLast.sec);
									GlobalTime newTimeGL;
									newTimeGL.SetTime(Timenow.yyy,Timenow.mm,Timenow.dd,Timenow.hr,Timenow.min,Timenow.sec);
									
									if(newTimeGL < LastTimeGL || newTimeGL == LastTimeGL)
									{
										//CAlert::InformationAlert("Segun esto es que son iguales el tiempo");
										break; //sale antes de importar
									}	
								}
								///se ejecuta la importacion.
								//CAlert::ErrorAlert("Z_2");
								UIDRef frameUIDRef=UIDRefFrame;//obtengo el UIDRef del rectangulo donde re defe fluir denuevo la imagen
								
								//CAlert::ErrorAlert("Z_3");
								if(ExisteArchivoParaLectura(&fromPath))
								{
									IDFile	sysFile;
									//convierto la cadena en IDFile
									if (SDKUtilities::AbsolutePathToSysFile(fromPath,sysFile) != kSuccess)
									{
										//CAlert::ErrorAlert("absolute path");
										break;
									}
									
									//CAlert::ErrorAlert("Z_4");
									//Borra las fotos que se encuentren dentro del frame
									/*InterfacePtr<IScrapItem> frameScrap(UIDRefFrame, IID_ISCRAPITEM);
									if(frameScrap!=nil)
									{
											
										// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
										InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
										// Process the DeleteFrameCmd:
										if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
										{		
											break;
										}
									}*/InterfacePtr<IHierarchy> graphicFrameHierarchy(UIDRefFrame, UseDefaultIID());
									if(graphicFrameHierarchy!=nil)
									{
										for(int32 i=0;i<graphicFrameHierarchy->GetChildCount();i++)
										{
											UID Child=graphicFrameHierarchy->GetChildUID(i);
											UIDRef UIDRefChild=UIDRef(db,Child);
	
											InterfacePtr<IScrapItem> frameScrap(UIDRefChild, IID_ISCRAPITEM);
											if(frameScrap!=nil)
											{
											
												// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
												InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
												// Process the DeleteFrameCmd:
												if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
												{		
													break;
												}
											}
										}
									}			

									
									
									
									PMReal left =StructUpdate[Avisonum].LFrameImage  ;
									PMReal top= StructUpdate[Avisonum].TFrameImage  ;
									PMReal right=StructUpdate[Avisonum].RFrameImage  ;
									PMReal bottom=StructUpdate[Avisonum].BFrameImage ;
									PMRect CoordenadasChidas=PMRect(left, top, right, bottom);
		
									//PMRect boundsInParentCoords = layoutHelper.PageToSpread(UIDRefFrame, CoordenadasChidas);
									//UIFlags uiFlags=K2::kMinimalUI;
									
									const UIDRef docUIDRef = ::GetUIDRef(document);
									
									ImportImagenClass ImportImage;
									UIDRef contentUIDRef = ImportImage.ImportImageAndLoadPlaceGun(docUIDRef, fromPath);
									ImportImage.CreateAndProcessPlaceItemInGraphicFrameCmd(docUIDRef,contentUIDRef, UIDRefFrame);
									contentUIDRef=UIDRefFrame;
									//UIDRef contentUIDRef = layoutHelper.PlaceFileInFrame(sysFile,UIDRefFrame,boundsInParentCoords,uiFlags,kFalse,kFalse,kFalse,kInvalidUID,kTrue);
									//IDFile file;
									//FileUtils::PMStringToIDFile(fromPath, file);
									//UIDRef contentUIDRef = IPlaceManager::PlaceDirect(file,UIDRefFrame);
									
									
									UID uidRefFrame=contentUIDRef.GetUID();
									StructUpdate[Avisonum].ID = "";
									StructUpdate[Avisonum].ID.AppendNumber(uidRefFrame.Get());


									AplicarFondoRecImagen(Preferencia,kTrue,frameUIDRef);
									AplicarColorMarcoRecImagen(Preferencia,kTrue,frameUIDRef);
									PMString Ajuste=Preferencia.FitCajaImagenConPublicidad;
									AjustarImagen(contentUIDRef,Ajuste);	
									
									//CAlert::ErrorAlert("Z_6");
									if(Preferencia.BorrarEtiquetaDespuesDeActualizar==1)
									{
										if(StructUpdate[Avisonum].ExistImageFile==kFalse)
										{
											UID Child=UID(StructUpdate[Avisonum].UIDFrameEtiqueta);
											UIDRef UIDRefChild=UIDRef(db,Child);
	
											InterfacePtr<IScrapItem> frameScrap(UIDRefChild, IID_ISCRAPITEM);
											if(frameScrap!=nil)
											{
												// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
												InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
												// Process the DeleteFrameCmd:
												if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
												{		
													break;
												}
												StructUpdate[Avisonum].ExistImageFile = kTrue;
											}
										}
									}
									//CAlert::ErrorAlert("Z_7");
								}
							}while(false);
					
						}
					}
					else
					{
						//CAlert::ErrorAlert("XXXXX");
						if (StructUpdate[Avisonum].Ruta.IsEmpty() == kFalse)//Verifica si tien ruta este aviso
						{
							PMString fromPath;	//Direccion o path del aviso
							PMString Archivo;	//direccion del Archivo
							fromPath=StructUpdate[Avisonum].Ruta;//copio el path
							#ifdef MACINTOSH
								if(fromPath.Contains("\\\\"))
									SDKUtilities::Replace(fromPath,"\\\\","\\");
									
								SDKUtilities::convertToMacPath(fromPath);
							#else // we must have a windows path now...
								if(!(fromPath.Contains(":\\") || fromPath.Contains(":\\\\")))
									SDKUtilities::convertToWinPath(fromPath);
							#endif
							Archivo=fromPath.GrabCString();//copio el path
							do 
							{
								///se ejecuta la importacion.
					
					
							UIDRef frameUIDRef=UIDRefFrame;//obtengo el UIDRef del rectangulo donde re defe fluir denuevo la imagen
							
								if(ExisteArchivoParaLectura(&fromPath))
								{
									IDFile	sysFile;
									//convierto la cadena en IDFile
									if (SDKUtilities::AbsolutePathToSysFile(fromPath,sysFile) != kSuccess)
									{
										//CAlert::ErrorAlert("absolute path");
										break;
									}
									//CAlert::ErrorAlert("Z_4");
									//Borra las fotos que se encuentren dentro del frame
									/*InterfacePtr<IScrapItem> frameScrap(UIDRefFrame, IID_ISCRAPITEM);
									if(frameScrap!=nil)
									{
											
										// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
										InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
										// Process the DeleteFrameCmd:
										if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
										{		
											break;
										}
									}
									*/
									
									
									
									PMReal left =StructUpdate[Avisonum].LFrameImage  ;
									PMReal top= StructUpdate[Avisonum].TFrameImage  ;
									PMReal right=StructUpdate[Avisonum].RFrameImage  ;
									PMReal bottom=StructUpdate[Avisonum].BFrameImage ;
									PMRect CoordenadasChidas=PMRect(left, top, right, bottom);
		
									//PMRect boundsInParentCoords = layoutHelper.PageToSpread(UIDRefFrame, CoordenadasChidas);
									//UIFlags uiFlags=K2::kMinimalUI;
									
									const UIDRef docUIDRef = ::GetUIDRef(document);
									
									ImportImagenClass ImportImage;
									UIDRef contentUIDRef = ImportImage.ImportImageAndLoadPlaceGun(docUIDRef, fromPath);
									ImportImage.CreateAndProcessPlaceItemInGraphicFrameCmd(docUIDRef,contentUIDRef, UIDRefFrame);
									contentUIDRef=UIDRefFrame;
									
									UID uidRefFrame=contentUIDRef.GetUID();
									StructUpdate[Avisonum].ID = "";
									StructUpdate[Avisonum].ID.AppendNumber(uidRefFrame.Get());
									
									AplicarFondoRecImagen(Preferencia,kTrue,frameUIDRef);
									AplicarColorMarcoRecImagen(Preferencia,kTrue,frameUIDRef);
									PMString Ajuste=Preferencia.FitCajaImagenConPublicidad;
									AjustarImagen( contentUIDRef, Ajuste );
									
									if(Preferencia.BorrarEtiquetaDespuesDeActualizar==1)
									{
										if(StructUpdate[Avisonum].ExistImageFile==kFalse)
										{
											UID Child=UID(StructUpdate[Avisonum].UIDFrameEtiqueta);
											UIDRef UIDRefChild=UIDRef(db,Child);

											InterfacePtr<IScrapItem> frameScrap(UIDRefChild, IID_ISCRAPITEM);
											if(frameScrap!=nil)
											{
												// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
												InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
												// Process the DeleteFrameCmd:
												if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
												{		
													break;
												}
										
												StructUpdate[Avisonum].ExistImageFile = kTrue;
											}
										}
									}
								}
							}while(false);
						}
					}
				}
				
				
		StructUpdate[Avisonum].DLImtedImage = A2PPrefeFuntions->ProporcionaFechaArchivo(StructUpdate[Avisonum].Ruta.GrabCString());
				
					TextoUpdate.Append("UIDFrameToImage:");
					TextoUpdate.Append(StructUpdate[Avisonum].ID);
					TextoUpdate.Append(",");
					TextoUpdate.Append("PathToImage:");
					TextoUpdate.Append(StructUpdate[Avisonum].Ruta);
					TextoUpdate.Append(",");
					TextoUpdate.Append("LastTimeImage:");
					TextoUpdate.Append(StructUpdate[Avisonum].DLImtedImage);
								
					TextoUpdate.Append(",");
					TextoUpdate.Append("ExistImageFile:");
					TextoUpdate.AppendNumber(StructUpdate[Avisonum].ExistImageFile);
					TextoUpdate.Append(",");
					TextoUpdate.Append("UIDFrameEtiqueta:");
					TextoUpdate.AppendNumber(StructUpdate[Avisonum].UIDFrameEtiqueta);
					TextoUpdate.Append(",");
								
					TextoUpdate.Append("UIDPageFlowed:");
					TextoUpdate.AppendNumber(StructUpdate[Avisonum].UIDPageFlowed);
					TextoUpdate.Append(",");
								
								
					TextoUpdate.Append("CordenadasFrameToImage:");
					TextoUpdate.AppendNumber(StructUpdate[Avisonum].LFrameImage);
					TextoUpdate.Append("/");
					TextoUpdate.AppendNumber(StructUpdate[Avisonum].TFrameImage);
					TextoUpdate.Append("/");
					TextoUpdate.AppendNumber(StructUpdate[Avisonum].RFrameImage);
					TextoUpdate.Append("/");
					TextoUpdate.AppendNumber(StructUpdate[Avisonum].BFrameImage);
								
					TextoUpdate.Append("\n");

			}
			delete StructUpdate;//libera memoria
			PnlTrvUtils::RemoveXMPVar("A2PTextUpdate");
			PnlTrvUtils::SaveXMPVar("A2PTextUpdate",TextoUpdate);
			this->RevisarActualizacionDAvisos();
			if(MarcoDebug == kTrue){CAlert::InformationAlert("AQUI TERMINA this->RevisarActualizacionDAvisos();");}
			CAlert::InformationAlert(kA2PImpActualizarAFinalizadoStringKey);
		}
	}while(false);
	
	this->RevisarModificacionParaBlinkAvisoDeCambio(kTrue);
	
	return retval;
}

/*
UpdateDocumentByUpdateFileImported*/
bool16 A2PActualizarFunctionSite::UpdateDocumentByUpdateFileImported(IDocument* document)
{
	bool16 retval=kFalse;
	PMString A2PPathLastFileImported = "";
	PMString A2PDateLAstFileImported = "";
	
	A2PPathLastFileImported=InterlasaUtilities::GetXMPVar("A2PPathLastFileImported", document);
	if(A2PPathLastFileImported.NumUTF16TextChars()>0)
	{	

		A2PDateLAstFileImported=InterlasaUtilities::GetXMPVar("A2PDateLAstFileImported", document);
		if(A2PDateLAstFileImported.NumUTF16TextChars()>0)
		{
			//CAlert::InformationAlert(A2PDateLAstFileImported);
			InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
			(
				kA2PPreferenciasFuncionesBoss,	// Object boss/class
				IA2PPreferenciasFunctions::kDefaultIID
			)));
			
			
			FechaData Timelast;
			PnlTrvUtils::ProporcionaStructFechaDataDeCadena(A2PDateLAstFileImported,&Timelast);
			
			FechaData Timenow;
			PnlTrvUtils::ProporcionaStructFechaDataDelArchivo(A2PPathLastFileImported, &Timenow);
			PMString 	UULastModifFileImported(A2PPrefeFuntions->ProporcionaFechaArchivo(A2PPathLastFileImported.GrabCString()));

			GlobalTime LastTimeGL;
			LastTimeGL.SetTime(Timelast.yyy,Timelast.mm,Timelast.dd,Timelast.hr,Timelast.min,Timelast.sec);
			
			GlobalTime newTimeGL;
			newTimeGL.SetTime( Timenow.yyy, Timenow.mm, Timenow.dd, Timenow.hr, Timenow.min, Timenow.sec );
			
			//CAlert::InformationAlert(UULastModifFileImported +"!="+ A2PDateLAstFileImported);
			if(UULastModifFileImported != A2PDateLAstFileImported)
			{
				
				//CAlert::InformationAlert("2");
				InterfacePtr<IA2PAvisos> A2PAVISOS(static_cast<IA2PAvisos*> (CreateObject
				(
					kA2PAvisosBoss,	// Object boss/class
					IA2PAvisos::kDefaultIID
				)));
				
				if(A2PAVISOS!=nil)
					A2PAVISOS->CargarAvisos(A2PPathLastFileImported,kTrue);
			
					
					
				//se realiza el update de todo el documenmto
				InterfacePtr<IA2PFluirFunction> FluirFunction(static_cast<IA2PFluirFunction*> (CreateObject
				(
					kA2PFluirFunctionBoss,	// Object boss/class
					IA2PFluirFunction::kDefaultIID
				)));
				
				if(FluirFunction!=nil)
				{
					
					FluirFunction->ActualizarDocumento();
					retval=kTrue;
					
				}
				else
				{
					retval=kFalse;
				}
			}
		}
	}
	else
	{
		//CAlert::InformationAlert("3");

		//Aqui verifico si es una pagina creada por el plantillero
		PMString FechaPublicacionPag = InterlasaUtilities::GetXMPVar("PlanGen_FechaPubli", document);
		PMString NumPagina = InterlasaUtilities::GetXMPVar("PlanGen_NumPagina", document);
		PMString Publicacion = InterlasaUtilities::GetXMPVar("PlanGen_Publicacion",document);
		PMString Seccion = InterlasaUtilities::GetXMPVar("PlanGen_Seccion",document);
		PMString TipoPag = InterlasaUtilities::GetXMPVar("PlanGen_TipoPag",document);
		
		if(FechaPublicacionPag.NumUTF16TextChars()>0)
		{//si es una pagina nueva
			
			
			InterfacePtr<IA2PFluirFunction> FluirFunction(static_cast<IA2PFluirFunction*> (CreateObject
				(
					kA2PFluirFunctionBoss,	// Object boss/class
					IA2PFluirFunction::kDefaultIID
				)));
				
				if(FluirFunction!=nil)
				{
					FluirFunction->ActualizarDocumentoPlantillero(Publicacion,Seccion,NumPagina,FechaPublicacionPag);
					retval=kTrue;
				}
				else
				{
					retval=kFalse;
				}
		}
		
	}
	
	return(retval);
}

/**
	Funcion que apilica un color de fondo al rectangulo de la imagen dependiendo las preferencias.
			
	  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
	  @param CPublicidad, Variable del tipo bool16 la cual contiene si se encontro o no la imagen que se debio importar.
	  @param frameList, la lista de frames o rectangulos de imagen.
*/
void A2PActualizarFunctionSite::AplicarFondoRecImagen(Preferencias &Prefer,bool16 CPublicidad,UIDRef textFrameUIDR)
{

	do
	{
			
		//UIDList itemList = frameList;
		//UID textFrameUID = kInvalidUID;
		
		//UIDRef textFrameUIDR=itemList.GetRef(itemList.Length() - num_rectangulos);//obtengo el UIDRef del rectangulo que se acaba de crear

		
		UID colorUID;				//UID del color
		if(CPublicidad)///si es una caja con publicidad
		{
			
			if(Prefer.ColorCajaImagenConPublicidad.Contains("Black"))
			{
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					
					break;
				}
			}
			else
			{
				if(Prefer.ColorCajaImagenConPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}
				}
				else
				{
					if(Prefer.ColorCajaImagenConPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==nil)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorCajaImagenConPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorCajaImagenConPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorCajaImagenConPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorCajaImagenConPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//Para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			if(Prefer.ColorCajaImagenSinPublicidad.Contains("Black"))
			{
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					break;
				}
			}
			else
			{
				if(Prefer.ColorCajaImagenSinPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}
				}
				else
				{
					if(Prefer.ColorCajaImagenSinPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==kInvalidUID)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorCajaImagenSinPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorCajaImagenSinPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorCajaImagenSinPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorCajaImagenSinPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//Para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		//obtengo la interfaz IPersistUIDData para alplicar el kGraphicStyleFillRenderingAttrBoss
		InterfacePtr<IPersistUIDData> fillRenderAttr((IPersistUIDData*)::CreateObject(kGraphicStyleFillRenderingAttrBoss,IID_IPERSISTUIDDATA));
		fillRenderAttr->SetUID(colorUID);
				
		//obtengo un comando
		InterfacePtr<ICommand>	gfxApplyCmd(CmdUtils::CreateCommand(kGfxApplyAttrOverrideCmdBoss));//se crea el comado para aplicar color al frame o rectangulo
				
		//pones el UIRef del frame al que se le va a plicar el color
		gfxApplyCmd->SetItemList(UIDList(textFrameUIDR));//

		//se obtiene la interfaz de IPMUnknownData para el comando
		InterfacePtr<IPMUnknownData> pifUnknown(gfxApplyCmd,UseDefaultIID());

		//pones el color
		pifUnknown->SetPMUnknown(fillRenderAttr);

		ErrorCode err = CmdUtils::ProcessCommand(gfxApplyCmd);//poocesamiento de comando para aplicar color al ultimo que se creo	
	}while(false);
}


/**
			Funcion que apilica un color de marco al rectangulo de la imagen dependiendo las preferencias.
			
			  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param CPublicidad, Variable del tipo bool16 la cual contiene si se encontro o no la imagen que se debio importar.
			  @param frameList, la lista de frames o rectangulos de imagen.
*/
void A2PActualizarFunctionSite::AplicarColorMarcoRecImagen(Preferencias &Prefer,bool16 CPublicidad,UIDRef textFrameUIDR)
{

	do
	{
		//////////////////////////////////////
		//    Para poner color a la caja    //
		//////////////////////////////////////
	//	UIDList itemList = frameList;
	//	UID textFrameUID = kInvalidUID;
		
	//	UIDRef textFrameUIDR=itemList.GetRef(itemList.Length() - num_rectangulos);//obtengo el UIDRef del rectangulo que se acaba de crear
		if(textFrameUIDR==nil)//
		{
			break;
		}
		
/*		UID a=textFrameUIDR.GetUID();//OBTENGO EL UID del rectangulo
		uint32 b=a.Get();			//obtengo el valor en exadecimal del rectangulo
		int c=uint32(b);			//Convierto el valor exadecimal a entero
		NumIDFrame.Clear();			//limpio el valor del anterior rectangulo
		NumIDFrame.AppendNumber(c);	//pungo el ID en valor decimal en el string 
		*/
		UID colorUID;//UID del color deseado

		if(CPublicidad)///si es una caja con publicidad
		{
			

			//////////////////////////////////////////////////////////////////////
			if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Black")) 
			{ 
				//lo mismo que el anterior solo que ahora utilizamos el kGraphicStyleStrokeRenderingAttrBoss para aplicar el color al marco
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					break;
				}
			}
			else
			{
				if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}
				}
				else
				{
					if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==nil)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			//////////////////////////////////////////////////////////////////////
			
			if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Black"))
			{
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					break;
				}
			}
			else
			{
				if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}
				}
				else
				{
					if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==kInvalidUID)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		InterfacePtr<IPersistUIDData> strokeRenderAttr((IPersistUIDData*)::CreateObject(kGraphicStyleStrokeRenderingAttrBoss,IID_IPERSISTUIDDATA));
		strokeRenderAttr->SetUID(colorUID);
				
		InterfacePtr<ICommand>	gfxApplyCmd(CmdUtils::CreateCommand(kGfxApplyAttrOverrideCmdBoss));//se crea el comado para aplicar color al frame o rectangulo
				
		gfxApplyCmd->SetItemList(UIDList(textFrameUIDR));//

		InterfacePtr<IPMUnknownData> pifUnknown(gfxApplyCmd,UseDefaultIID());

		pifUnknown->SetPMUnknown(strokeRenderAttr);

		ErrorCode err = CmdUtils::ProcessCommand(gfxApplyCmd);//poocesamiento de comando para aplicar color al ultimo que se creo	

	}while(false);
}


////////////////////////////////////////////////////////////
/**
*/
bool16 A2PActualizarFunctionSite::DocumentosAbiertos()
{
	bool16 ExistDocumento;
	IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();//si no se encuentra algun documento abierto
		if(document == nil)//si se encuentra algun documento abierto
			ExistDocumento=kFalse;
		else
			ExistDocumento=kTrue;
return(ExistDocumento);
}
///////////////////////////////////////////////////////
UIDRef A2PActualizarFunctionSite::UIDRefCapa(PMString NomCapa)
{
	UID LayerUID=nil;
	UIDRef LayerUIDRef;
	// We'll use a do-while(false) to break out on bad pointers:
	do
	{
	
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			break;
		}
		LayerUID=layerList->FindByName(NomCapa);//busqueda de capa por su nombre
		if(LayerUID==nil)
		{
			break;
		}
		LayerUIDRef=UIDRef(db,LayerUID);//obtengo el UIDRef de la capa
	}while(false);
	return(LayerUIDRef);
}

//////////////////////////////////////////////////////
////////////////////////////////////////////////////////
void A2PActualizarFunctionSite::ActivarCapa(PMString NomCapa)
{

	ErrorCode error = kCancel;
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			CAlert::WarningAlert("Error al intentar mostrar documento");
			break;
		}

		const UIDRef docUIDRef = ::GetUIDRef(document);
						
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}

		UID Capa= layerList->FindByName(NomCapa);
		if(Capa==nil)
		{
			break;
		}
		
		int32 NumLayerAActivar=layerList->GetLayerIndex(Capa);
		
		/*InterfacePtr<IDocumentLayer> documentLayer (layerList->QueryLayer(NumLayerAActivar), UseDefaultIID());
-		// Now we've created the new layer, we must set it as the active layer:
		InterfacePtr<ICommand> setActiveLayerCmd(CmdUtils::CreateCommand(kSetActiveLayerCmdBoss));
		
		InterfacePtr<ILayoutCmdData> layoutCmdData(setActiveLayerCmd, UseDefaultIID());
		
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		
		if (setActiveLayerCmd == nil || layoutCmdData == nil || layoutControlData == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd, layoutCmdData, or layoutControlData invalid");
			break;
		}

		setActiveLayerCmd->SetItemList(UIDList(documentLayer));
		
		layoutCmdData->Set(docUIDRef, layoutControlData);
		// Process the command
		error = CmdUtils::ProcessCommand(setActiveLayerCmd);
		*/
		if (error != kSuccess)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd failed");
		}
	}while(false);
}

/////////////////////////////////////////////////////////////
int32 A2PActualizarFunctionSite::NumAvisoUpdate(PMString TextoUpdate)
{	
	int32 i=0;
	int32 numline= 0;
	int32 length = TextoUpdate.NumUTF16TextChars();
	for( i=0 ; i<length ; i++)
	{
		if(TextoUpdate.GetWChar(i)==10 || TextoUpdate.GetWChar(i)==13)
			numline++;
	}
	return(numline-1);
}

void A2PActualizarFunctionSite::LlenarStructUpdate(StructCamposUpdate *StructUpdate,PMString TextoUpdate)
{
	
	int32 numAd=0;
	do
	{
		PMString StringLineaAds = TextoUpdate.Substring(0,(TextoUpdate.IndexOfString("\n")+1))->GrabCString();
		
		//CAlert::InformationAlert(TextoUpdate);
		do
		{
			PMString Dato="";
			if(StringLineaAds.IndexOfString(",") > 0)
			{
				Dato = StringLineaAds.Substring(0,(StringLineaAds.IndexOfString(",")))->GrabCString();
				StringLineaAds.Remove(0,(StringLineaAds.IndexOfString(",")+1));
			}
			else
			{
				Dato = StringLineaAds;
				StringLineaAds.Remove(0,(StringLineaAds.IndexOfString("\n")+1));
			}
			
			//CAlert::InformationAlert(StringLineaAds);
			if(Dato.Contains("UIDFrameToImage:"))
			{
				Dato.Remove(0,16);
				StructUpdate[numAd].ID=Dato;
			}
			else
			{
				if(Dato.Contains("PathToImage:"))
				{
					Dato.Remove(0,12);
					StructUpdate[numAd].Ruta=Dato;
				}
				else
				{
					if(Dato.Contains("LastTimeImage:"))
					{
						Dato.Remove(0,14);
						if(Dato.NumUTF16TextChars()>2)
						{
							PnlTrvUtils::ProporcionaStructFechaDataDeCadena(Dato, &StructUpdate[numAd].fecha);
						}
						else
						{
							StructUpdate[numAd].fecha.yyy = 0;
						}
					}
					else
					{
						if(Dato.Contains("ExistImageFile:"))
						{
							Dato.Remove(0,15);
							int32 num=Dato.GetAsNumber();
							if(num>0)
								StructUpdate[numAd].ExistImageFile=kTrue;
							else	
								StructUpdate[numAd].ExistImageFile=kFalse;
						}
						else
						{
							if(Dato.Contains("UIDFrameEtiqueta:"))
							{
								Dato.Remove(0,17);
								StructUpdate[numAd].UIDFrameEtiqueta = Dato.GetAsNumber();
							}
							else
							{
								if(Dato.Contains("UIDPageFlowed:"))
								{
									Dato.Remove(0,14);
									StructUpdate[numAd].UIDPageFlowed = Dato.GetAsNumber();
								}
								else
								{
									if(Dato.Contains("CordenadasFrameToImage:"))
									{
										Dato.Remove(0,23);
										StructUpdate[numAd].LFrameImage = Dato.GetAsDouble();
										Dato.Remove(0,(Dato.IndexOfString("/")+1));
										
										StructUpdate[numAd].TFrameImage = Dato.GetAsDouble();
										Dato.Remove(0,(Dato.IndexOfString("/")+1));
										
										StructUpdate[numAd].RFrameImage = Dato.GetAsDouble();
										Dato.Remove(0,(Dato.IndexOfString("/")+1));
										
										StructUpdate[numAd].BFrameImage = Dato.GetAsDouble();
										
										
										//Dato.Remove(0,(Dato.IndexOfString("\n")+1));
									}
								}
							}
						}
					}
				}
			}
			
			
		}while(StringLineaAds.NumUTF16TextChars()>0);
		
		TextoUpdate.Remove(0,(TextoUpdate.IndexOfString("\n")+1));
		numAd++;
	}while(TextoUpdate.NumUTF16TextChars()>0);
}


UIDList A2PActualizarFunctionSite::TomarListaDeRectangulos()
{
	UIDList List=nil;
	do
	{


		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
			break;

		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		//Para obtener los frames de mi documento
		// Get the list of spreads in the document
		InterfacePtr<ISpreadList> spreadList(document, UseDefaultIID());
		if (spreadList == nil)
		{
			ASSERT_FAIL("spreadList invalid");
			break;
		}
		
		// Build a list of all the frames in the document
		UIDList frameList(db);
		// Iterate over all the spreads
		int32 spreadCount = spreadList->GetSpreadCount();
		for ( int32 spreadIndex = 0; spreadIndex < spreadCount; spreadIndex++ )
		{
			UIDRef spreadUIDRef(db, spreadList->GetNthSpreadUID(spreadIndex));
			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if (spread == nil)
			{
				ASSERT_FAIL("spread invalid");
				break;
			}
			
			// Iterate over all the pages on the spread.
			int32 numberOfPages = spread->GetNumPages();
			for ( int32 i = 0; i < numberOfPages; i++ )
			{
				UIDList pageItemList(db);
				spread->GetItemsOnPage
				(
					i, 
					&pageItemList, 
					kFalse, // don't include the page object itself
					kTrue // include items that lie on the pasteboard
				);
					
				// Iterate the page items and save off the UIDs of frames.
				int32 pageItemListLength = pageItemList.Length();
				for ( int32 j = 0; j < pageItemListLength; j++ )
				{
					UIDRef pageItemRef = pageItemList.GetRef(j);

					// Graphic frames have an IGraphicFrameData.
					InterfacePtr<IGraphicFrameData> graphicFrameData(pageItemRef, UseDefaultIID());
					if (graphicFrameData != nil)
					{
						if (graphicFrameData->IsGraphicFrame() == kTrue)
							frameList.Append(pageItemRef.GetUID());		
					}
				}
			} // iterate pages in spread		
		} // interate spread
		List=frameList;
	}while (false); // only do once
	return(List);
}



void A2PActualizarFunctionSite::AjustarImagen(UIDRef& GFrameUIDRef,PMString Ajuste)
{
	do
	{
		//CAlert::ErrorAlert(Ajuste);
		UIDRef contentUIDRef=UIDRef::gNull;
		//Verifica si existe la caja de imagen
		if(GFrameUIDRef==nil)//
		{
			//CAlert::ErrorAlert("No encontro frame GFrameUIDRef");
			break;
		}
		
		InterfacePtr<IGraphicFrameData> graficFrame(GFrameUIDRef,UseDefaultIID());
		if(GFrameUIDRef!=nil && GFrameUIDRef!=UIDRef::gNull && graficFrame!=nil)
		{
			
			bool16 updatePorFecha = kFalse;
				
			InterfacePtr<IHierarchy> graphicFrameHierarchy(GFrameUIDRef, UseDefaultIID());
			if(graphicFrameHierarchy!=nil)
			{
				for(int32 i=0;i<graphicFrameHierarchy->GetChildCount() ; i++)
						contentUIDRef= UIDRef(GFrameUIDRef.GetDataBase(), graphicFrameHierarchy->GetChildUID(i));
						//graphicFrameHierarchy->GetChildUID(i);
			}
			else
			{
				//CAlert::ErrorAlert("AAAAAAA");
				break;
			}

				
		}
		else
			{
				//CAlert::ErrorAlert("BBBBBBB");
				break;
			}
		
		if(GFrameUIDRef==nil || GFrameUIDRef==UIDRef::gNull)
		{
			//CAlert::ErrorAlert("No encontro frame GFrameUIDRef");
			break;
		}
			
	if(Ajuste.Contains("kA2PPrefAjusteContenidoAFrameStringKey"))
	{
		// Create a FitContentToFrameCmd:
		InterfacePtr<ICommand>	fitCmd(CmdUtils::CreateCommand(kFitContentToFrameCmdBoss));
		// Set the FitContentToFrameCmd's ItemList:
		fitCmd->SetItemList(UIDList(contentUIDRef));
		// Process the FitContentToFrameCmd:
		
		if (CmdUtils::ProcessCommand(fitCmd) != kSuccess)
		{
			CAlert::ErrorAlert("Error al intentar ajustar imagen");
		}
		
	}
	else
	{	if(Ajuste.Contains("kA2PPrefAjusteCentrarStringKey"))
		{
			// Create a FitContentToFrameCmd:
			InterfacePtr<ICommand>	fitCmd(CmdUtils::CreateCommand(kCenterContentInFrameCmdBoss));
			// Set the FitContentToFrameCmd's ItemList:
			fitCmd->SetItemList(UIDList(contentUIDRef));
			// Process the FitContentToFrameCmd:
			if (CmdUtils::ProcessCommand(fitCmd) != kSuccess)
			{
				CAlert::ErrorAlert("Error al intentar centrar imagen");
			}
			//CAlert::ErrorAlert("Centrar contenido");
		}
		else
		{
			if(Ajuste.Contains("kA2PPrefAjusteProporcionalmenteStringKey"))
			{
				// Create a FitContentToFrameCmd:
				InterfacePtr<ICommand>	fitCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
				// Set the FitContentToFrameCmd's ItemList:
				fitCmd->SetItemList(UIDList(contentUIDRef));
				// Process the FitContentToFrameCmd:
				if (CmdUtils::ProcessCommand(fitCmd) != kSuccess)
				{
					CAlert::ErrorAlert("Error al intentar centrar imagen");
				}
				//CAlert::ErrorAlert("Cont propor");
			}
			else
			{
				//CAlert::ErrorAlert("Sin Ajuste");
				///Sin Ajuste
			}
		}
	}
	}while(false);
}

struct tm A2PActualizarFunctionSite::convertime(PMString timeAviso)
{
	struct tm g;//structura del tipo tm(tiempo)
	int32 numchar;//numero de caracter a ller
	int32 numCampo=1;//numero de campo que se ha leido
	PMString campo=" ";//contenido de este campo
	
	//CAlert::ErrorAlert(timeAviso);
	PMString hd="";
	for(numchar=0;numchar<timeAviso.NumUTF16TextChars();numchar++)//
	{
		if(timeAviso.GetWChar(numchar)!=32 && timeAviso.GetWChar(numchar)!=58)//busqueda hasta que encuentre un : o espacio
		{
			campo.AppendW(timeAviso.GetWChar(numchar));//adiciona a campo el caracter leido
			//CAlert::ErrorAlert("char=");
			//CAlert::ErrorAlert(campo);
		}
		else
		{
			switch(numCampo)
			{
				
				case 2://es el campo del mes
					//compara el nombre del mes y le asigna un numero
						if(campo=="Jan")
						{
							g.tm_mon=0;
							
						}
						if(campo=="Feb")
						{
							g.tm_mon=1;
							
						}
						if(campo=="Mar")
						{
							g.tm_mon=2;
							
						}
						if(campo=="Apr")
						{
							g.tm_mon=3;
							
						}
						if(campo=="May")
						{
							g.tm_mon=4;
							
						}
						if(campo=="Jun")
						{
							g.tm_mon=5;
							
						}
						if(campo=="Jul")
						{
							g.tm_mon=6;
							
						}
						if(campo=="Aug")
						{
							g.tm_mon=7;
							
						}
						if(campo=="Sept")
						{
							g.tm_mon=8;
							
						}
						if(campo=="Oct")
						{
							g.tm_mon=9;
							
						}
						if(campo=="Nov")
						{
							g.tm_mon=10;
							
						}
						if(campo=="Dec")
						{
							g.tm_mon=11;
						}
					break;
				case 3://es el campo del dia
				    //CAlert::InformationAlert(campo);
					g.tm_mday=campo.GetAsNumber();
					
					 //hd="tm_mday=";
					 //hd.AppendNumber(g.tm_mday);
					 //CAlert::InformationAlert(hd);
					break;
				case 4:// es el campo de la hora
				
					g.tm_hour=int(campo.GetAsNumber());
					 //hd="tm_hour=";
					 //hd.AppendNumber(g.tm_hour);
					 //CAlert::InformationAlert(hd);
					break;
				case 5://es el campo de minutos
				
					g.tm_min=int(campo.GetAsNumber());
					 //hd="tm_min=";
					 //hd.AppendNumber(g.tm_min);
					 //CAlert::InformationAlert(hd);
					break;
				case 6://es el campo de segundos
				
					g.tm_sec=int(campo.GetAsNumber());
					 //hd="tm_sec=";
					 //hd.AppendNumber(g.tm_sec);
					 //CAlert::InformationAlert(hd);
					break;
				case 7://es el campo del aÒo
					 campo.Remove(0,2);
					 g.tm_year=int(campo.GetAsNumber());
					// hd="tm_year=";
					 //hd.AppendNumber(g.tm_year);
					 //CAlert::InformationAlert(hd);
					break;
			}//switc
			numCampo++;
			campo.Clear();
			campo="";
		}//else
	}//for
	 campo.Remove(0,2);
	 g.tm_year=int(campo.GetAsNumber());
	// hd="tm_year=";
	// hd.AppendNumber(g.tm_year);
	// CAlert::InformationAlert(hd);
	return(g);//regresa estructura tm
}

bool16 A2PActualizarFunctionSite::comparaFecha(struct tm &timeAviso, struct tm &timeNewAviso)
{
	bool16 value=kFalse;
	do
	{
		PMString sss="Fecha archivoviejo: ";
		sss.AppendNumber(timeAviso.tm_year);
		sss.Append("/");
		sss.AppendNumber(timeAviso.tm_mon);
		sss.Append("/");
		sss.AppendNumber(timeAviso.tm_mday);
		sss.Append("/");
		sss.AppendNumber(timeAviso.tm_hour);
		sss.Append("/");
		sss.AppendNumber(timeAviso.tm_min);
		sss.Append("/");
		sss.AppendNumber(timeAviso.tm_sec);
		sss.Append("\nFecha archivoviejo:");
		sss.AppendNumber(timeNewAviso.tm_year);
		sss.Append("/");
		sss.AppendNumber(timeNewAviso.tm_mon);
		sss.Append("/");
		sss.AppendNumber(timeNewAviso.tm_mday);
		sss.Append("/");
		sss.AppendNumber(timeNewAviso.tm_hour);
		sss.Append("/");
		sss.AppendNumber(timeNewAviso.tm_min);
		sss.Append("/");
		sss.AppendNumber(timeNewAviso.tm_sec);
		//CAlert::InformationAlert(sss);
/*		if(timeNewAviso.tm_year>timeAviso.tm_mon)
		{
			value=kTrue;
			CAlert::InformationAlert("AÒo");
			break;
		}
		else
		{
*/			if(timeNewAviso.tm_year==timeAviso.tm_year)
			{
				if(timeNewAviso.tm_mon>timeAviso.tm_mon)//si el mes de la nueva imagen es menor a la que ya se tiene
				{	
					value=kTrue;
					break;
				}
				else
				{
					if(timeNewAviso.tm_mon==timeAviso.tm_mon)//si el mes de la nueva imagen es menor a la que ya se tiene
					{	
						if(timeNewAviso.tm_mday>timeAviso.tm_mday)//si el dia de la nueva imagen es menor a la que ya se tiene
						{
							value=kTrue;
							break;
						}
						else
						{
							if(timeNewAviso.tm_mday==timeAviso.tm_mday)//si el dia de la nueva imagen es menor a la que ya se tiene
							{
								if(timeNewAviso.tm_hour>timeAviso.tm_hour)//si la hora de la nueva imagen es menor a la que ya se tiene
								{
									value=kTrue;
									break;
								}
								else
								{
									if(timeNewAviso.tm_hour==timeAviso.tm_hour)//si la hora de la nueva imagen es menor a la que ya se tiene
									{
										if(timeNewAviso.tm_min>timeAviso.tm_min)//si el minuto de la nueva imagen es menor a la que ya se tiene
										{
											value=kTrue;
											break;
										}
										else
										{
											if(timeNewAviso.tm_min==timeAviso.tm_min)//si el minuto de la nueva imagen es menor a la que ya se tiene
											{
												if(timeNewAviso.tm_sec>timeAviso.tm_sec)//si el segundo de la nueva imagen es menor a la que ya se tiene
												{
													value=kTrue;
													break;
												}
											}

										}
									}

								}
							}
						}
				
					}

				}
			}
		//}					
	}while(false);
	return(value);//sale y retorna falso
}



/**
			Metodo que busca el archivo correspondiente si no lo encuentra lo buscara por otro tipo de
			extencion.
*/
bool16 A2PActualizarFunctionSite::ExisteArchivoParaLectura(PMString *filePath)
{
	PMString Path;//Variable que almacenara el path original y para poder hacer las preguntas si existen el
					//archivo con determinada extencion
	Path=filePath->GrabCString();
	bool16 value=kFalse;//
	do
	{
		//Busca si existe el archivo con la extencion inicial
		if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
		{
			value=kTrue;
			break;
		}
		else//si no lo encontro
		{
			//Busca si existe el archivo con la extencion jpg
			Path=PnlTrvUtils::TruncateExtencion(Path);
			Path.Append(".jpg");//adiciona la nueva extencion al path o ruta.
			if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
			{
				value=kTrue;
				break;
			}
			else
			{
				//Busca si existe el archivo con la extencion gif
				Path=PnlTrvUtils::TruncateExtencion(Path);
				Path.Append(".gif");
				if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
				{
					value=kTrue;
					break;
				}
				else
				{
					//Busca si existe el archivo con la extencion pdf
					Path=PnlTrvUtils::TruncateExtencion(Path);
					Path.Append(".pdf");
					if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
					{
						value=kTrue;
						break;
					}
					else
					{
						//Busca si existe el archivo con la extencion psd
						Path=PnlTrvUtils::TruncateExtencion(Path);
						Path.Append(".psd");
						if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
						{
							value=kTrue;
							break;
						}
						else
						{
							//Busca si existe el archivo con la extencion eps
							Path=PnlTrvUtils::TruncateExtencion(Path);
							Path.Append(".eps");
							if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
							{
								value=kTrue;
								break;
							}
							else
							{
								//Busca si existe el archivo con la extencion tif
								Path=PnlTrvUtils::TruncateExtencion(Path);
								Path.Append(".tif");
								if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
								{
									value=kTrue;
									break;
								}
								else
								{
									//Busca si existe el archivo con la extencion bmp
									Path=PnlTrvUtils::TruncateExtencion(Path);
									Path.Append(".bmp");
									if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
									{
										value=kTrue;
										break;
									}

									else
									{
										//Busca si existe el archivo con la extencion jepg
										Path=PnlTrvUtils::TruncateExtencion(Path);
										Path.Append(".jpeg");
										if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
										{
											value=kTrue;
											break;
										}
									}
								}
							}
						}
					}
				}
			}			
		}
	}while(false);
filePath->Clear();//limpia la cadena el path original
filePath->Append(Path.GrabCString());
return(value);
}


/**
obtiene la cantidad de avisos dentro de el archivo a importar
*/
/**
obtiene la cantidad de avisos dentro de el archivo a importar
*/
int32 A2PActualizarFunctionSite::CantidadAvisos(PMString NomArchivo)
{
	FILE *stream;
	PMString CSDireccion;
	PlatformChar Caracter;
	PMString CSLinea;
	
	CSDireccion="";
	Caracter=' ';
	CSLinea="";

	CSDireccion=NomArchivo.GrabCString();

	int Linea;
	int ANSCCICaranter;
	Linea=0;

	//do
	//{
		if( (stream  = fopen(CSDireccion.GrabCString(),"r"))==NULL)
			{	
				SDKUtilities::InvokePlugInAboutBox( kA2PImpErrorAlAbrirArchivoStringKey );
				SDKUtilities::InvokePlugInAboutBox(CSDireccion);
				//	break;
			}
		else
			{		
		
				while(!feof(stream))
				{	
					do
					{		
						Caracter.SetFromUnicode(fgetc(stream));
						ANSCCICaranter=Caracter.GetValue();
					}while(ANSCCICaranter!=10 && ANSCCICaranter!=13 && !feof(stream));
					Linea++;
				}
				
				fclose(stream);
			}
	//}while(kFalse);
	//CSLinea=fcvt(Linea,0,&j,&k);
	
	return(Linea);
}


/**
	Borra Avisos y etiquetas para comenzar con la actualizacion de geometria
*/
bool16 A2PActualizarFunctionSite::DELETE_AvisosYEtiquetasParaUpdateGeometria()
{
	
	bool16 retval=kTrue;
	do
	{
		
		if(!DocumentosAbiertos())//verifica si se encuentra documentos aviertos
		{
			CAlert::ErrorAlert(kA2PImpDontDocumentfrontStringKey);
			retval=kFalse;
			break;
		}
		
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		
			//CAlert::InformationAlert("1");
		
			PMString TextoUpdate="";//ObtenerTextoUpdate();//obtien el texto que se encuentra sobre el TextframeUpdate
		
			//CAlert::InformationAlert("2");
			TextoUpdate = InterlasaUtilities::GetXMPVar("A2PTextUpdate", document);
		
		
			//CAlert::InformationAlert("3");
			if(TextoUpdate.NumUTF16TextChars()<2)
			{
				//CAlert::ErrorAlert(kA2PImpNoExisteCajaUpdateStringKey);
				retval=kFalse;
				break;
			}
		
			//CAlert::InformationAlert("4");
			int32 ContAvisos = NumAvisoUpdate(TextoUpdate);//Determina la cantidad de avisos a fluir
		
	
			//CAlert::InformationAlert("5");
			PMString PathPwd = PnlTrvUtils::CrearFolderPreferencias(); 
		
			//CAlert::InformationAlert("6");
			PathPwd.Append("Default.pfa");
	
			InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
			(
				kA2PPreferenciasFuncionesBoss,	// Object boss/class
				IA2PPreferenciasFunctions::kDefaultIID
			)));
		
		
			if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Preferencia,kFalse)==kFalse)//leer documento de preferencias
			{
				CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
				break;
			}
			//CAlert::InformationAlert("01");
			StructCamposUpdate *StructUpdate=new StructCamposUpdate[ContAvisos+1];//crea la estructura StrucUpdate para guardar despues los campos de cada aviso
			//CAlert::InformationAlert(TextoUpdate);
			LlenarStructUpdate(StructUpdate,TextoUpdate);//llenado la estructura Update
			//CAlert::InformationAlert("02");
			for(int32 Avisonum=0;Avisonum<=ContAvisos;Avisonum++)//ciclo para compara el rectangulo actual con el UID del rectangulo de los avisos de la Estructura Update
			{	
				//PMString A="";
				//A.AppendNumber(StructUpdate[Avisonum].ID.GetAsNumber());
				//A.Append(",");
				//A.Append(StructUpdate[Avisonum].fecha);
				//CAlert::InformationAlert(A);
				int32 AvisoIDEntero=StructUpdate[Avisonum].ID.GetAsNumber();//ogtengo el UID del aviso de la en EstructuraUpdate[AvisoNum]
						
				UID UIDFrame=AvisoIDEntero;
					
				UIDRef  UIDRefFrame=UIDRef::gNull;
				UIDRefFrame=UIDRef(db,UIDFrame);
			
				InterfacePtr<IGraphicFrameData> graficFrame(UIDRefFrame,UseDefaultIID());
				if(UIDRefFrame!=nil && UIDRefFrame!=UIDRef::gNull && graficFrame!=nil)
				{
			
					bool16 updatePorFecha = kFalse;
				
					InterfacePtr<IHierarchy> graphicFrameHierarchy(UIDRefFrame, UseDefaultIID());
					if(graphicFrameHierarchy!=nil)
					{
							InterfacePtr<IScrapItem> frameScrap(UIDRefFrame, IID_ISCRAPITEM);
							if(frameScrap!=nil)
							{
								// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
								InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
								// Process the DeleteFrameCmd:
								if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
								{		
									break;
								}
								
							}
					}
				
				}
				
				UID Child=UID(StructUpdate[Avisonum].UIDFrameEtiqueta);
				UIDRef UIDRefChild=UIDRef(db,Child);
	
				InterfacePtr<IScrapItem> frameScrap(UIDRefChild, IID_ISCRAPITEM);
				if(frameScrap!=nil)
				{
					// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
					InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
					// Process the DeleteFrameCmd:
					if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
					{		
						break;
					}
				}
			}
			delete StructUpdate;//libera memoria
			//CAlert::InformationAlert(kA2PImpActualizarAFinalizadoStringKey);
		
	}while(false);	
	return retval;

}

/*
 Metodo que muestra o oculta un widget sobre alguna paleta.
 panelWidgetID	id widget de la paleta donde se encuentra el objeto a mostrar o ocultar
 widgetID	Objeto que sera ocultado o mostrado.
 showpi		Indica si el objeto debe ocultarse o mostrarse.
 
 */
bool16 A2PActualizarFunctionSite::showhidden_widgetOnPalette(const WidgetID& panelWidgetID, const WidgetID& widgetID,const bool16& showpi)
{
	bool16 retval=kFalse;
	do
	{
		IControlView* viewIMageUpdate=PnlTrvUtils::GetWidgetOnPanel(panelWidgetID,widgetID);
		if(viewIMageUpdate==nil)
			break;
		
		if(showpi)
			viewIMageUpdate->Show();
		else
			viewIMageUpdate->Hide();
		
	}while(false);
	return retval;
}


bool16 A2PActualizarFunctionSite::RevisarActualizacionDAvisos()
{
	bool16 retval=kFalse;
	do
	{
		if(!DocumentosAbiertos())//verifica si se encuentra documentos aviertos
		{
			retval=kFalse;
			break;
		}
		
		IDocument* document =  Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		retval = VerificaSiActualizaPorGeometria(document);
		if(!retval)
		{
			retval = VerificaSiActualizaAlgunClasidesplegado(document);
			if(retval)
			{
				//CAlert::InformationAlert("Uptade");
				this->showhidden_widgetOnPalette(kA2PImpPanelWidgetID,kN2PsqlAlertActualizarNotasWidgetID,kTrue);
			}
			else
			{
				//CAlert::InformationAlert("No update");
				this->showhidden_widgetOnPalette(kA2PImpPanelWidgetID,kN2PsqlAlertActualizarNotasWidgetID,kFalse);
			}
			
		}
		else
		{
			this->showhidden_widgetOnPalette(kA2PImpPanelWidgetID,kN2PsqlAlertActualizarNotasWidgetID,kTrue);
		}
		
	}while(false);
	return(retval);
}


/*
 Metodo que verifica si debe actualizarse algun desplegado que ya ha sifdo fluido.
 document documento donde se encuentran fluidos los desplegados a revisar si existe alguna actualizacion
 */
bool16 A2PActualizarFunctionSite::VerificaSiActualizaAlgunClasidesplegado(IDocument* document)
{
	bool16 retval=kFalse;
	do
	{
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		PMString TextoUpdate="";//ObtenerTextoUpdate();//obtien el texto que se encuentra sobre el TextframeUpdate
		
		//CAlert::InformationAlert("2");
		TextoUpdate = InterlasaUtilities::GetXMPVar("A2PTextUpdate", document);
		
		
		//CAlert::InformationAlert("3");
		if(TextoUpdate.NumUTF16TextChars()<2)
		{
			//CAlert::ErrorAlert(kA2PImpNoExisteCajaUpdateStringKey);
			retval=kFalse;
			break;
		}
		
		//CAlert::InformationAlert("4");
		int32 ContAvisos = NumAvisoUpdate(TextoUpdate);//Determina la cantidad de avisos a fluir
		
		
		//CAlert::InformationAlert("5");
		PMString PathPwd = PnlTrvUtils::CrearFolderPreferencias(); 
		
		//CAlert::InformationAlert("6");
		PathPwd.Append("Default.pfa");
		
		InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
																										  (
																										   kA2PPreferenciasFuncionesBoss,	// Object boss/class
																										   IA2PPreferenciasFunctions::kDefaultIID
																										   )));
		
		
		if(A2PPrefeFuntions->leerDocDePreferencia(PathPwd.GrabCString(),Preferencia) == kFalse)//leer documento de preferencias
		{
			CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
			break;
		}
		
		StructCamposUpdate *StructUpdate = new StructCamposUpdate[ContAvisos+1];//crea la estructura StrucUpdate para guardar despues los campos de cada aviso
		LlenarStructUpdate(StructUpdate,TextoUpdate);//llenado la estructura Update
		for( int32 Avisonum = 0; Avisonum<=ContAvisos; Avisonum++ )//ciclo para compara el rectangulo actual con el UID del rectangulo de los avisos de la Estructura Update
		{	
			//Si la foto proviene del servidor local
			
			UID UIDFrameFoto = StructUpdate[Avisonum].ID.GetAsNumber();
			
			PMString RutaElemEnBD="";
			
			
			UIDRef pageItemUIDRef(db,UIDFrameFoto);
			
			
			InterfacePtr<IHierarchy> itemHierarchy(pageItemUIDRef, UseDefaultIID()); 
			ASSERT(itemHierarchy);
			if(!itemHierarchy) 
			{
				continue;
			}
			
			IDataBase* db = pageItemUIDRef.GetDataBase();
			UIDList children(db);
			
			
			itemHierarchy->GetDescendents(&children, IID_IDATALINKREFERENCE);
			if(children.Length()==1)
			{
				//No tiene colocada una imagen
				//Verifica si ya existe un aviso
				if (StructUpdate[Avisonum].Ruta.IsEmpty() == kFalse)//Verifica si tien ruta este aviso
				{
					PMString fromPath;	//Direccion o path del aviso
					fromPath=StructUpdate[Avisonum].Ruta;//copio el path
#ifdef MACINTOSH
					SDKUtilities::convertToMacPath(fromPath);
#else // we must have a windows path now...
					SDKUtilities::convertToWinPath(fromPath);
#endif
					
					if(ExisteArchivoParaLectura(&fromPath))
					{
						retval=kTrue;
						break;
					}
				}
			}
			
			for (int32 c = 0; c < children.Length(); c++)
			{
				
				UIDRef frmecontentUIDREf(::GetDataBase(itemHierarchy), itemHierarchy->GetChildUID(c));
				
				
				InterfacePtr<IHierarchy> iHierarFrameContent(frmecontentUIDREf, UseDefaultIID());
				if(iHierarFrameContent==nil)
				{
					continue;
				} 
				
				UIDRef datachildRef1(::GetDataBase(itemHierarchy), iHierarFrameContent->GetChildUID(0));
				
				
				InterfacePtr<IHierarchy> graphicFrameHierarchy1(datachildRef1, UseDefaultIID());
				if(graphicFrameHierarchy1==nil)
				{
					continue;
				} 
				
				
				IDataBase* iDataBase = ::GetDataBase(graphicFrameHierarchy1);
				
				InterfacePtr<ILinkManager> iLinkManager(iDataBase,iDataBase->GetRootUID(),UseDefaultIID());
				ASSERT_MSG(iLinkManager, "CHMLFiltHelper::addGraphicFrameDescription - No link manager?");
				ILinkManager::QueryResult linkQueryResult;
				
				PMString FullNameOfLinkRef = "";
				
				if (iLinkManager->QueryLinksByObjectUID(::GetUID(graphicFrameHierarchy1), linkQueryResult)>0)
				{//Existen ligas para este frame
					ASSERT_MSG(linkQueryResult.size()==1,"CHMLFiltHelper::addGraphicFrameDescription - Only expecting single link with this object");
					ILinkManager::QueryResult::const_iterator iter = linkQueryResult.begin();
					InterfacePtr<ILink> iLink (iDataBase, *iter,UseDefaultIID());
					if (iLink==nil)
					{
						continue;
					}
					
					if (iLink->GetResourceModificationState() == ILink::kResourceModified )
					{
						retval=kTrue;
						break;
					}
					
				}
			} // loop over kids with IDataLinkReference interface	
		}
	}while(false);
	return retval;
}

/*
 Verifica si se debe actualizar la geometro de los desplegados, observando la fecha de modificacion del archivo de la pauta a fluir.
 */
bool16 A2PActualizarFunctionSite::VerificaSiActualizaPorGeometria(IDocument* document)
{
	bool16 retval=kFalse;
	PMString A2PPathLastFileImported="";
	PMString A2PDateLAstFileImported="";
	
	A2PPathLastFileImported=InterlasaUtilities::GetXMPVar("A2PPathLastFileImported", document);
	if(A2PPathLastFileImported.NumUTF16TextChars()>0)
	{	
		A2PDateLAstFileImported=InterlasaUtilities::GetXMPVar("A2PDateLAstFileImported", document);
		if(A2PDateLAstFileImported.NumUTF16TextChars()>0)
		{
			InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
																											  (
																											   kA2PPreferenciasFuncionesBoss,	// Object boss/class
																											   IA2PPreferenciasFunctions::kDefaultIID
																											   )));
			
			FechaData Timelast;
			PnlTrvUtils::ProporcionaStructFechaDataDeCadena(A2PDateLAstFileImported,&Timelast);
			
			FechaData Timenow;
			PnlTrvUtils::ProporcionaStructFechaDataDelArchivo(A2PPathLastFileImported, &Timenow);
			PMString 	UULastModifFileImported(A2PPrefeFuntions->ProporcionaFechaArchivo(A2PPathLastFileImported.GrabCString()));
			
			GlobalTime LastTimeGL;
			LastTimeGL.SetTime(Timelast.yyy,Timelast.mm,Timelast.dd,Timelast.hr,Timelast.min,Timelast.sec);
			
			GlobalTime newTimeGL;
			newTimeGL.SetTime( Timenow.yyy, Timenow.mm, Timenow.dd, Timenow.hr, Timenow.min, Timenow.sec );
			
			//CAlert::InformationAlert(UULastModifFileImported +"!="+ A2PDateLAstFileImported);
			if(UULastModifFileImported != A2PDateLAstFileImported)
			{
				retval=kTrue;
			}
		}
	}
	else
	{
		//Aqui verifico si es una pagina creada por el plantillero
		PMString FechaPublicacionPag = InterlasaUtilities::GetXMPVar("PlanGen_FechaPubli", document);
		PMString NumPagina = InterlasaUtilities::GetXMPVar("PlanGen_NumPagina", document);
		PMString Publicacion = InterlasaUtilities::GetXMPVar("PlanGen_Publicacion",document);
		PMString Seccion = InterlasaUtilities::GetXMPVar("PlanGen_Seccion",document);
		PMString TipoPag = InterlasaUtilities::GetXMPVar("PlanGen_TipoPag",document);
		
		if(FechaPublicacionPag.NumUTF16TextChars()>0)
		{//si es una pagina nueva
			
			
			InterfacePtr<IA2PFluirFunction> FluirFunction(static_cast<IA2PFluirFunction*> (CreateObject
			(
				kA2PFluirFunctionBoss,	// Object boss/class
				IA2PFluirFunction::kDefaultIID
			)));
			
			if(FluirFunction!=nil)
			{
				retval=kTrue;
			}
		}
		
	}
	
	return(retval);
}

bool16 A2PActualizarFunctionSite::RevisarModificacionParaBlinkAvisoDeCambio(const bool16& act)
{
/*
esta funcion se debe preguntar si el archivo que se entro tiene la misma fecha que cuando entro
solamente si cambio hay que alertar!!
*/
///////////////////////
//SACAR DATOS
///////////////////////
	PMString Tprzr;//declaras
	this->getTextOfWidgetFromPalette( kA2PImpPanelWidgetID, GuardaStringWidgetID, Tprzr );//apuntas
	if( MarcoDebug == kTrue ){CAlert::InformationAlert("Tprzr: "+Tprzr);}
	IDFile IDFTprzr(Tprzr);//convertir string en IDFile //fuegoo!!
	
	PMString alertanum;
	alertanum.AppendFixed(Tprzr.NumUTF16TextChars());
	if(MarcoDebug == kTrue){CAlert::InformationAlert("numero de caracteres "+alertanum);}
	
	PMReal ModDateIntWidget;
	this->getValueOfWidgetFromPalette( kA2PImpPanelWidgetID, GuardaIntWidgetID, ModDateIntWidget );
	if( (ModDateIntWidget <= 0) || (act == kTrue) ){//detectar si ya habia datos en el widget
		
		PMString ModDateIntWidgetString;
		ModDateIntWidgetString.AppendNumber(ModDateIntWidget);
		if(MarcoDebug==kTrue){CAlert::InformationAlert("se va setear el textedit, ModDateIntWidget vale: "+ModDateIntWidgetString);}
		//declarar la variable tipo uint32 para guardar la fecha por un apuntador
		uint32 ModDate;
		FileUtils::GetModificationDate(IDFTprzr, &ModDate); //preguntarle que fecha trae el archivo
		
		GlobalTime time = GlobalTime(ModDate);
		uint64 getime = time.GetTime();
		PMReal Realtime(getime);
		PMString RealtimeStr;
		RealtimeStr.AppendNumber(Realtime);
		PMString TimeStringed;
		time.TimeToString(TimeStringed);
		if(MarcoDebug==kTrue){CAlert::InformationAlert("calar el tiempo " + TimeStringed+" y checar este numerito: "+RealtimeStr);}
		
		uint64 GetTime = time.GetTime();
		PMReal miReal(GetTime);
		PMString estoMetera;
		estoMetera.AppendNumber(miReal);
		if(MarcoDebug==kTrue){CAlert::InformationAlert( "Esto metera: " + estoMetera );}
		this->setValueOfWidgetFromPalette( kA2PImpPanelWidgetID, GuardaIntWidgetID, miReal );
		
	}//ya se guardo en int32!!
	
	if( MarcoDebug == kTrue ){CAlert::InformationAlert("#3 RevisarModificacionParaBlinkAvisoDeCambio()");}//checkpoint !!
	
	//sigue sacar el que se guardo
	PMReal ModDateViejoReal;
	this->getValueOfWidgetFromPalette( kA2PImpPanelWidgetID, GuardaIntWidgetID, ModDateViejoReal );
	PMString AlertaDateViejoReal;
	AlertaDateViejoReal.AppendNumber(ModDateViejoReal);
	if(MarcoDebug==kTrue){CAlert::InformationAlert("DESMARRANEO DE FECHAS: fecha vieja " + AlertaDateViejoReal);}
	
	//sacar la fecha actual
	uint32 ModDateActual;
	FileUtils::GetModificationDate(IDFTprzr, &ModDateActual); //preguntarle que fecha trae el archivo
	GlobalTime time2 = GlobalTime(ModDateActual);
	uint64 GetTime2 = time2.GetTime(); 
	PMReal ModDateActualReal(GetTime2);
	PMString alertaFechaActual;
	alertaFechaActual.AppendNumber(ModDateActualReal);
	if(MarcoDebug==kTrue){CAlert::InformationAlert("DESMARRANEO DE FECHAS: fecha actual "+alertaFechaActual);}
	
	//comparar antigua y actual
	PMString Alerta;
	Alerta.AppendNumber(ModDateViejoReal);
	Alerta.Append(" ");
	Alerta.AppendNumber(ModDateActualReal);
	if(MarcoDebug==kTrue){CAlert::InformationAlert("DESMARRANEO DE FECHAS: comparar "+Alerta);}//(y)
	
///////////////////////
//COMPARAR fechas del documento ".export"
///////////////////////
	PMString alertacomparar;
	alertacomparar = "antigua: ";
	alertacomparar.AppendNumber(ModDateViejoReal);
	alertacomparar.Append(" nueva: ");
	alertacomparar.AppendNumber(ModDateActualReal);
	if(MarcoDebug==kTrue){CAlert::InformationAlert("DESMARRANEO DE FECHAS: comparar de nuevo  "+alertacomparar);}

///////////////////////
////COMPARAR fechas de las imagenes linkeadas
///////////////////////

	//super variable para
	bool16 linkChanged = kFalse;
	
	do{
		//obtiene la db que es el documento entero creo
		IDocument* iDocument = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(!iDocument)
		{ 
			ASSERT_FAIL("no hay documento");
			break;
		}
		
		IDataBase* iDataBase = GetDataBase(iDocument);
		if( ! iDataBase )
		{
			ASSERT_FAIL("no hay database");
			if(MarcoDebug==kTrue){CAlert::InformationAlert(" ! iDataBase ");}
			break;
		}
		
		InterfacePtr<ISpreadList> spreadList( iDocument, UseDefaultIID() );
		if (spreadList == nil)
		{
			ASSERT_FAIL("spreadList invalid");
			break;
		}
		
		UIDList frameList(iDataBase);// Build a list of all the frames in the document
		if (frameList == nil)
		{
			ASSERT_FAIL("framelist fails");
			if(MarcoDebug==kTrue){CAlert::InformationAlert("frameList == nil");}
			break;
		}
		
		int32 spreadCount = spreadList->GetSpreadCount();
		
		// Iterate over all the spreads
		for ( int32 spreadIndex = 0; spreadIndex < spreadCount; spreadIndex++ )
		{
			UIDRef spreadUIDRef(iDataBase, spreadList->GetNthSpreadUID(spreadIndex));
			InterfacePtr<ISpread> spread(spreadUIDRef, UseDefaultIID());
			if (spread == nil)
			{
				ASSERT_FAIL("spread invalid");
				break;
			}
			
			// Iterate over all the pages on the spread.
			int32 numberOfPages = spread->GetNumPages();
			for ( int32 i = 0; i < numberOfPages; i++ )
			{
				UIDList pageItemList(iDataBase);
				spread->GetItemsOnPage
				(
					i, 
					&pageItemList, 
					kFalse, // don't include the page object itself
					kTrue // include items that lie on the pasteboard
				);
					
				// Iterate the page items and save off the UIDs of frames.
				int32 pageItemListLength = pageItemList.Length();
				for ( int32 j = 0; j < pageItemListLength; j++ )
				{
					UIDRef pageItemRef = pageItemList.GetRef(j);

					// Graphic frames have an IGraphicFrameData.
					InterfacePtr<IGraphicFrameData> graphicFrameData(pageItemRef, UseDefaultIID());
					if (graphicFrameData != nil)
					{
						if (graphicFrameData->IsGraphicFrame() == kTrue)
							frameList.Append(pageItemRef.GetUID());		
					}
				}
			}// iterate pages in spread		
		}//interate spread este bucle saca la lista de frames en el documento
		
		
		if(MarcoDebug==kTrue){CAlert::InformationAlert("hasta aqui todo bien");}
		
		//entra otro bucle para irte frame por frame
		for(uint32 frame = 0; frame < frameList.Length(); frame++)
		{
			UIDRef pageItemUIDRef( iDataBase, frameList[frame] );
			if( !pageItemUIDRef )
			{
				if( MarcoDebug==kTrue ){CAlert::InformationAlert("!pageItemUIDRef");}
				break;
			}
		
			InterfacePtr<IHierarchy> itemHierarchy( iDataBase, frameList[frame], UseDefaultIID() );
			ASSERT( itemHierarchy );
			if( !itemHierarchy )
			{
				if( MarcoDebug == kTrue ){CAlert::InformationAlert("!itemHierarchy");}
				break;
			}
		
			UIDList children = frameList;
			itemHierarchy->GetDescendents(&children, IID_IDATALINKREFERENCE);
		
			for (int32 c = 0; c < children.Length(); c++)
			{			
				
				InterfacePtr<IHierarchy> iHierarFrameContent(  itemHierarchy->QueryChild( c ) );
				if( iHierarFrameContent==nil )
				{
					if( MarcoDebug == kTrue ){CAlert::InformationAlert("iHierarFrameContent==nil");}
					break;
				} 
				
				UIDRef datachildRef1(::GetDataBase(itemHierarchy), iHierarFrameContent->GetChildUID(0));
				
				InterfacePtr<IHierarchy> graphicFrameHierarchy1(datachildRef1, UseDefaultIID());
				if(graphicFrameHierarchy1==nil)
				{
					if( MarcoDebug == kTrue ){CAlert::InformationAlert("graphicFrameHierarchy1==nil");}
					break;
				}
				
				IDataBase* iDataBase = ::GetDataBase(graphicFrameHierarchy1);
				
				InterfacePtr<ILinkManager> iLinkManager( iDataBase, iDataBase->GetRootUID(), UseDefaultIID() );
				ASSERT_MSG(iLinkManager, "CHMLFiltHelper::addGraphicFrameDescription - No link manager?");
				ILinkManager::QueryResult linkQueryResult;
		
				PMString FullNameOfLinkRef = "";
					if( MarcoDebug == kTrue ){CAlert::InformationAlert("antes de entrar al if");}
					if (iLinkManager->QueryLinksByObjectUID(::GetUID(graphicFrameHierarchy1), linkQueryResult)>0)
					{//Existen ligas para este frame
						ASSERT_MSG(linkQueryResult.size() == 1,"CHMLFiltHelper::addGraphicFrameDescription - Only expecting single link with this object");
						ILinkManager::QueryResult::const_iterator iter = linkQueryResult.begin();
						InterfacePtr<ILink> iLink (iDataBase, *iter,UseDefaultIID());
							if(iLink==nil)
							{
								if(MarcoDebug==kTrue){CAlert::InformationAlert("!iLink");}
								continue;
							}
							
							if(iLink->GetResourceModificationState() == ILink::kResourceModified)
							{
								if(MarcoDebug==kTrue){CAlert::InformationAlert("Una imagen fue modificada!!!!");}
								linkChanged = kTrue;
								break;
							}							
					}
			
			} // loop over kids with IDataLinkReference interface
		}
		
		if( MarcoDebug == kTrue ){CAlert::InformationAlert("si cumple todo el do");}
	}while(kFalse);

//////////////////////
///////////////DECIDIR
//////////////////////
	if( (ModDateViejoReal < ModDateActualReal) || (linkChanged == kTrue) )
	{
			if(MarcoDebug==kTrue){CAlert::InformationAlert("if( ModDate < ModDateActual )");}
			IControlView* wdgtData = PnlTrvUtils::GetWidgetOnPanel(kA2PImpPanelWidgetID, kA2PImpIconoDeAlertaWidgetID);
			ASSERT(wdgtData);
			if(!wdgtData){
				if(MarcoDebug==kTrue){CAlert::InformationAlert("!wdgtData");}
				//break;
			}else{
				wdgtData->Show();//muestra la alerta
			}
	}else{
			if(MarcoDebug==kTrue){CAlert::InformationAlert("ELSE de if( ModDate < ModDateActual )");}
			IControlView* wdgtData = PnlTrvUtils::GetWidgetOnPanel(kA2PImpPanelWidgetID, kA2PImpIconoDeAlertaWidgetID);
			ASSERT(wdgtData);
			
			if(!wdgtData){
				if(MarcoDebug==kTrue){CAlert::InformationAlert("!wdgtData");}
				//break;
			}else{
				wdgtData->Hide();//la oculta, lo cual ya estaba hecho por default
			}
	}
	
	return kTrue;
	
}


/*
funcion para setear la cadena de texto de un widget textedit 
*/
bool16 A2PActualizarFunctionSite::setTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMString& value)
{
	bool16 retval = kTrue;
	do
	{
		InterfacePtr<IPanelControlData> panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID)) ;
			if(panel==nil)
			{
				if(MarcoDebug==kTrue){CAlert::InformationAlert("#1 InterlasaUtilities::getTextOfWidgetFromPalette  No se encontro el editbox");}
				break;
				}

			InterfacePtr<IControlView> iControView( panel->FindWidget(widgetID), UseDefaultIID() );
			if(iControView==nil)
			{
			CAlert::InformationAlert("TMInterlasaButtonDibujarObserver::getTextOfWidgetFromPalette iControViewList");
			break;
			}
			
			//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
			InterfacePtr<ITextControlData> TextoDeDireccion( iControView, UseDefaultIID());
			if(TextoDeDireccion==nil)
			{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
			}

			TextoDeDireccion->SetString(value);
			retval = kTrue;

			}while(false);
			if(MarcoDebug==kTrue){CAlert::InformationAlert("#2 se agrega el texto al textedit");}
			return(retval);
}

/* setear la cadena de texto de un widget textedit */
bool16 A2PActualizarFunctionSite::setValueOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMReal& value)
{
	bool16 retval = kTrue;
	do
	{
		InterfacePtr<IPanelControlData> panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID));
			if(panel==nil)
			{
				if(MarcoDebug==kTrue){CAlert::InformationAlert("#2 InterlasaUtilities::getTextOfWidgetFromPalette No se encontro el editbox");}
				break;
			}

			InterfacePtr<IControlView> iControView( panel->FindWidget(widgetID), UseDefaultIID() );
			if(iControView==nil)
			{
				CAlert::InformationAlert("TMInterlasaButtonDibujarObserver::getTextOfWidgetFromPalette iControViewList");
				break;
			}
			
			//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
			InterfacePtr<ITextValue> TextoDeDireccion( iControView, UseDefaultIID());
			if(TextoDeDireccion==nil)
			{
				CAlert::InformationAlert("No se encontro el texto de direccion");
				break;
			}

			TextoDeDireccion->SetTextFromValue(value);
			retval = kTrue;

	}while(false);
			if(MarcoDebug==kTrue){CAlert::InformationAlert("#2 se agrega el texto al textedit");}
			return(retval);
}

/*widget tipo get para obtener el valor numerico del intEditBox*/
bool16 A2PActualizarFunctionSite::getValueOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMReal& value)
{
	bool16 retval = kTrue;
	do
	{
		InterfacePtr<IPanelControlData> panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID)) ;
			if(panel==nil)
			{
				if(MarcoDebug==kTrue){CAlert::InformationAlert("#3 InterlasaUtilities::getTextOfWidgetFromPalette  No se encontro el editbox");}
				break;
			}

			InterfacePtr<IControlView> iControView( panel->FindWidget(widgetID), UseDefaultIID() );
			if(iControView==nil)
			{
			CAlert::InformationAlert("TMInterlasaButtonDibujarObserver::getTextOfWidgetFromPalette iControViewList");
			break;
			}
			
			//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
			InterfacePtr<ITextValue> TextoDeDireccion( iControView, UseDefaultIID());
			if(TextoDeDireccion==nil)
			{
				CAlert::InformationAlert("No se encontro el texto de direccion");
				break;
			}

			value = TextoDeDireccion->GetTextAsValue();
			retval = kTrue;

			}while(false);
			
			return(retval);
}

/*widgwt twxt obtain de interlasa*/
bool16 A2PActualizarFunctionSite::getTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMString& value)
{
	bool16 retval = kTrue;
	do
	{
		InterfacePtr<IPanelControlData> panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(PaleteWidgetID)) ;
			if(panel==nil)
			{
				if(MarcoDebug==kTrue){CAlert::InformationAlert("#4 InterlasaUtilities::getTextOfWidgetFromPalette  No se encontro el editbox");}
				break;
				}

			InterfacePtr<IControlView> iControView( panel->FindWidget(widgetID), UseDefaultIID() );
			if(iControView==nil)
			{
			CAlert::InformationAlert("  TMInterlasaButtonDibujarObserver::getTextOfWidgetFromPalette iControViewList");
			break;
			}
			
			//obtiene una interface tel tipo ITextControlData para obtener el texto de esta caja o Widget
			InterfacePtr<ITextControlData> TextoDeDireccion( iControView, UseDefaultIID());
			if(TextoDeDireccion==nil)
			{
			CAlert::InformationAlert("No se encontro el texto de direccion");
			break;
			}

			value = TextoDeDireccion->GetString();
			retval = kTrue;

			}while(false);
			
			return(retval);
}