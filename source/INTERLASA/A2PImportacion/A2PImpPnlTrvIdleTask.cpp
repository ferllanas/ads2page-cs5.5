//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvIdleTask.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// interface includes
#include "IApplication.h"
#include "IMenuManager.h"
#include "IActionManager.h"
#include "IIdleTaskMgr.h"
#include "ITreeViewMgr.h"
#include "IControlView.h"
#include "IBoolData.h"

// General includes:
#include "PnlTrvUtils.h"
#include "CIdleTask.h"
#include "CAlert.h"

// Project includes
#include "A2PImpID.h"

const static int kPnlTrvExecInterval = 5*60*1000;	// Refresh every 5 minutes in case we add new images
// milliseconds between checks

/** Implements IIdleTask, to refresh the view onto the file system.
	Just lets us keep up to date when people add new assets.
	This implementation isn't too respectful; it just clears the tree and calls ChangeRoot.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class PnlTrvIdleTask : public CIdleTask
{
public:

	/**	Constructor
		@param boss boss object on which this interface is aggregated
	*/
	PnlTrvIdleTask(IPMUnknown* boss);

	/**	Destructor
	*/
	virtual ~PnlTrvIdleTask() {}


	/**	Called by the idle task manager in the app core when the task is running
		@param appFlags [IN] specifies set of flags which task can check to see if it should do something
			this time round
		@param timeCheck [IN] specifies how many milliseconds before the next active task becomes overdue.
		@return uint32 giving the interval (msec) that should elapse before calling this back
	 */
	virtual uint32 RunTask(uint32 appFlags, IdleTimer* timeCheck);

	/**	Get name of task
		@return const char* name of task
	 */
	virtual const char* TaskName();
protected:

	/**	Update the treeview.
	 */
	void refresh();
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(PnlTrvIdleTask, kA2PImpTrvIdleTaskImpl)


/* Constructor
*/
PnlTrvIdleTask::PnlTrvIdleTask(IPMUnknown *boss)
	:CIdleTask(boss)
{
}


/* RunTask
*/
uint32 PnlTrvIdleTask::RunTask(
	uint32 appFlags, IdleTimer* timeCheck)
{

	if( appFlags & 
		( IIdleTaskMgr::kMouseTracking 
		| IIdleTaskMgr::kUserActive 
		| IIdleTaskMgr::kInBackground 
		| IIdleTaskMgr::kMenuUp))
	{
		return kOnFlagChange;
	}
	
	this->refresh();

	// Has FS changed?
	return kPnlTrvExecInterval;
}


/* TaskName
*/
const char* PnlTrvIdleTask::TaskName()
{

	return kA2PImpIdleTaskKey;
}


/* refresh
*/
void PnlTrvIdleTask::refresh()
{
	do
	{

		IControlView* treeWidget = 
			PnlTrvUtils::GetWidgetOnPanel(kA2PImpPanelWidgetID, kA2PImpTreeViewWidgetID);
		// Don't assert, fail silently, the tree view panel may be closed.
		if(!treeWidget)
		{
			break;
		}

		InterfacePtr<ITreeViewMgr> 
			iTreeViewMgr(treeWidget, UseDefaultIID());
		ASSERT(iTreeViewMgr);
		if(!iTreeViewMgr)
		{
			break;
		}
		InterfacePtr<IBoolData> 
			refreshStatus(GetExecutionContextSession(), IID_IPnlTrvRefreshStatus);
		ASSERT(refreshStatus);
		if(refreshStatus)
		{
			refreshStatus->Set(kTrue);
			// The model code will clear this flag
			iTreeViewMgr->ClearTree();
			iTreeViewMgr->ChangeRoot(kTrue);
		}
		
	} while(kFalse);
}

//	end, File:	PnlTrvIdleTask.cpp
