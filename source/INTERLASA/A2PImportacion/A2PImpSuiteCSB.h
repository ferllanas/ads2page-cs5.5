//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.com
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

#pragma once
#ifndef __A2PImpSuiteCSB_h__
#define __A2PImpSuiteCSB_h__

#include "IPMUnknown.h"
#include "IA2PImpSuite.h"
#include "A2PImpID.h"

/**  This class can be used to implement code common to all IA2PImpSuite CSBs.
       For example, you may be calling the same code in each CSB, you just find the UIDs to operate on differently.
*/
class A2PImpSuiteCSB : public CPMUnknown<IA2PImpSuite>
{
public:
	/** Constructor.
	@param boss interface ptr from boss object on which this interface is aggregated.
	*/
	A2PImpSuiteCSB(IPMUnknown* boss) : CPMUnknown<IA2PImpSuite>(boss) {};

	/** Destructor. Does nothing.
	*/
	~A2PImpSuiteCSB() {}

	/** @return kTrue if operation X is supported by this CSB.*/
	virtual bool16 CanDoX() const;

	/** Performs operation X returning an ErrorCode.
	@return kSuccess on success, or an appropriate ErrorCode on failure. */
	virtual ErrorCode DoX() ;

	/** @return kTrue if operation Y is supported by this CSB.*/
	virtual bool16 CanDoY() const ;

	/** Performs operation Y returning an ErrorCode. 
	@return kSuccess on success, or an appropriate ErrorCode on failure. */
	virtual ErrorCode DoY() const ;

	/** Gets some data from the current selection.  Instead of returning a value,
	implementations will stuff data into the OUT variable.
	@param vector OUT Data Z for the current selection is inserted into vector.*/
	virtual void GetZ( K2Vector<int32> & vector) ;

	/** Sets some data on the current selection.
	@param z IN Data to set.
	@return kSuccess on success, or an appropriate ErrorCode on failure. */
	virtual ErrorCode SetZ( int32 z ) ;
	

};
#endif // A2PImpSuiteCSB_h__

//  Code generated by DollyXs code generator
