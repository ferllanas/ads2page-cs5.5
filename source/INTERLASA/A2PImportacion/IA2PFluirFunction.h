/*
//	File:	IA2PActualizarFunction.h
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#ifndef _IA2PFluirFunction_
#define _IA2PFluirFunction_

// Interface includes:
#include "IPMUnknown.h"
#include "A2PImpID.h"
#ifdef MACINTOSH
	
	#include "IA2PPreferenciasFunciones.h"
#endif
#ifdef WINDOWS
	
	#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif

//#include "A2PImpStructPreferencias.h"    //If you are using MS Visual C++ you do not need this


/**

	@author Ian Paterson
*/
class IA2PFluirFunction : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IA2PFluirFunction };

	/**

	*/
	virtual bool16 Fluir()=0;
	
	virtual bool16 ActualizarDocumento()=0;
	
	virtual bool16 ActualizarDocumentoPlantillero(const PMString& Publicacion,const PMString& Seccion,const PMString& NumPagina,const PMString& FechaPublicacionPag)=0;
private:
	
};

#endif // _IA2PActualizarFunction_
