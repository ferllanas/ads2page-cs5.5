/*
//	File:	SelectBotonFluir.cpp
//
//	Date:	21-Agosto-2003
//
//	Este codigo es generado para cumplir con la funcion de pegar las imagenes 
//	de en un aviso correspondiente.Es el encargado de generar y construir los 
//	grafics frames los cuales contendran las imagenes del aviso o anuncio, 
//	tambien genera las etiquetas de cada caja de imagen (textFrames).Ademas es encargado de 
//	aplicar las diferentes preferencias (Color de marco, Color fondo, ajuste de imagen,,etc)
//	tambien genera la creacion de capas, asi como la generacion de la capa Update y el TextUpdate,
//	la creacion de colores utilies para Ads2Page 2.0.
//	
//		
//
//
*/
/*		Acontinuacion se declaran las diferentes librerias que se utilizaran para el
		de las funciones que utiliza Fluir.											*/
#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"

// Interface includes
#include "IActionStateList.h"
#include "IApplication.h"

#include "IBoolData.h"

#include "IControlView.h"
#include "IColorData.h"
#include "ICommand.h"
#include "ICommandSequence.h"
//#include "ICreateFrameData.h"

#include "IDatabase.h"
#include "IDialog.h"
#include "IDialogCreator.h"
#include "IDialogController.h"
#include "IDocument.h"
#include "IDocumentUtils.h"
#include "IDocFileHandler.h"
#include "IDocumentLayer.h"
#include "IDocumentList.h"
#include "IDrawingStyle.h"

#include "IFrameData.h"
#include "IFrameContentUtils.h"
#include "IFrameList.h"
#include "IFrameListComposer.h"
#include "IFontMgr.h"
#include "IDocFontMgr.h"


#include "IGraphicAttributeUtils.h"
#include "IGraphicAttrIndeterminateData.h"
#include "IGraphicAttrRealNumber.h"//atributos de rectangulo
#include "IGraphicFrameData.h"
//#include "IGraphicMetaDataUtils.h"
#include "IGraphicMetaDataObject.h"
#include "IGraphicStateRenderObjects.h"
#include "IGraphicStateUtils.h"
#include "IGraphicStyleDescriptor.h"////////para cambiar el tamaÒo del marco del rectangulo////////////
#include "IGeometry.h"

#include "IHierarchy.h"
#include "IHierarchyCmdData.h"

#include "IImportProvider.h"
//#include "IImportFileCmdData.h"
#include "IImportManagerOptions.h"

#include "ILayerList.h"
#include "ILayoutCmdData.h"
#include "ILayoutControlData.h"
#include "ILayoutSelectionSuite.h"


#include "IMargins.h"
#include "IMeasurementSystem.h"
#include "IMeasureUnitsCmdData.h"

#include "INewDocCmdData.h"
#include "INewLayerCmdData.h"
#include "INewPageItemCmdData.h"

#include "IOpenFileDialog.h"
#include "IOpenFileCmdData.h"
#include "IOpenManager.h"

//#include "IPageItemSelector.h"
#include "IPanelControlData.h"
#include "IPathUtils.h"
#include "IPathGeometry.h"
#include "IPlaceGun.h"
#include "IPlaceBehavior.h"
#include "IPlacePIData.h"
#include "IPMStream.h"
#include "IPageItemTypeUtils.h"
#include "IPageColumnsCmdData.h"//Interfaz de los datos para el comando de las columnas del sistema.
#include "IPersistUIDData.h"
//#include "IPersistUIDRefData.h"
#include "IPMUnknownData.h"
#include "IPathSelectionList.h"
#include "IPathSelection.h"


#include "IRenderingObject.h"
#include "IReplaceCmdData.h"

#include "ISession.h"
#include "ISubject.h"
#include "ISysFileData.h"
//#include "ISelection.h"
#include "ISelectableDialogSwitcher.h"
//#include "ISpecifier.h"
#include "IScrapItem.h"		////////////para borra frame////////////////////
#include "ISpreadList.h"
#include "ISpread.h"
#include "IStrokePref.h"//Para ajustar el marco de un graficframe
#include "IStrokeChangePreference.h"
#include "IStyleInfo.h"
#include "IStyleNameTable.h"
#include "ISwatchList.h"
#include "ISwatchUtils.h"
#include "ISelectionUtils.h"
//#include "IScaleItemCmdData.h"


#include "ITextAttrUtils.h"//atributos de texto
#include "ITreeViewHierarchyAdapter.h"
#include "ITreeViewMgr.h"
#include "ITreeNodeIDData.h"
#include "ITreeViewController.h"
#include "ITextModel.h"
#include "ITextAttrAlign.h"
#include "ITextAttrBoolean.h"
#include "ITextAttrFont.h"
#include "ITextAttributes.h"
#include "ITextAttributeSuite.h"
#include "ITextAttrInt16.h"
#include "ITextAttrRealNumber.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "ITextModelCmds.h"
#include "ITextAttrUID.h"
#include "TextAttrID.h"
#include "TextID.h"

#include "IUIDData.h"
#include "IUnitOfMeasure.h"
#include "IUnitOfMeasureSettings.h"

#include "IWidgetParent.h"
#include "IWindowList.h"

#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IKentenStyle.h"

#include "IZeroPointPrefsCmdData.h"//Para ajustar el punto cero del documento
/////////////////////////////
// General includes/////////
///////////////////////////
#include "AttributeBossList.h"
#include "CAlert.h"
#include "CActionComponent.h"
#include "CmdUtils.h"
#include "CObserver.h"
#include "ErrorUtils.h"
//#include "FrameUtils.h"
#include "IImageUtils.h"
#include "ILayoutUIUtils.h"
#include "IPathUtils.h"
#include "PersistUtils.h"
#include "PMString.h"
#include "PMReal.h"
#include "PMRect.h"
#include "PreferenceUtils.h"
#include "UIDList.h"
#include "UIDRef.h"
#include "Utils.h"
#include "StreamUtil.h"
#include "SysFileList.h"
#include "Trace.h" 
#include "TransformUtils.h"
#include "K2Vector.h"
#include "K2Vector.tpp"
#include "WideString.h"
/////////////////////
// includes IDs://///
/////////////////////
#include "DocumentID.h"
#include "LayoutID.h"
#include "OpenPlaceID.h"
#include "A2PImpID.h"
#include "PnlTrvFileNodeID.h"
#include "SpreadID.h"
#include "SplineID.h"
#include "ITextAttrUID.h"
#include "TextID.h"
/////////////////////
//includes Proyect///
/////////////////////
#include "PnlTrvDataModel.h"
#include "PnlTrvUtils.h"
#include "A2PImpStructAvisos.h"
//#include "ProporcionaFechaArchivo.h"//

//#include "InterlasaChecaDoungle.cpp"
#include "ImportImagenClass.h"
//#include "ObtenerMacAddresdeRegistro.cpp"
//#include "InterlasaRegEditUtilities.h"

#include "ITextAttrAlign.h"
#include "ICompositionStyle.h"
#include "AttributeBossList.h"
#include "ITextAttrRealNumber.h"
#include "FileUtils.h"

#include "IA2PFluirFunction.h"

#ifdef MACINTOSH
		#include <types.h> 						
#endif

#include <sys/stat.h> 
#include <time.h>
#include <locale.h> 

#ifdef MACINTOSH
	#include "IA2PPreferenciasFunciones.h"
#endif


#ifdef WINDOWS
	#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif


/***************************************************************************************************/
/**************DECLARACION DE LA CLASE SelBotonFluir DEL TIPO CObserver*****************************/
/***************************************************************************************************/
/** SelDlgIconObserver
	Permite el proceso din·mico de los cambios del estado del botÛn o icono, 
	en este caso el botÛn fluir. 

	@author Fernando Llanas
*/


class SelBotonFluirObserver : public CObserver
{
	public:	

		
		/**
			Constructor.

			@param boss interface ptr from boss object on which interface is aggregated.
		*/
		SelBotonFluirObserver(IPMUnknown *boss) : CObserver(boss) {}

		/** Destructor. */
		//virtual ~SelBotonFluirObserver() {}
		/** 
			Called by the application to allow the observer to attach to the 
			subjects to be observed. In this case the tab dialog's info button widget.
		*/
		virtual void AutoAttach();
		
		/** 
			Called by the application to allow the observer to detach from the 
			subjects being observed. 
		*/
        virtual void AutoDetach();	
		
		/**
			Called by the host when the observed object changes. In this case: 
			the tab dialog's info button is clicked.

			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(SelBotonFluirObserver, kA2PImpBotonFluirObserverImpl)

//SelBotonFluirObserver::SelBotonFluirObserver(IPMUnknown* boss)
	 //: CObserver(boss)
//{
//}

/*************************************************************************************************/
/*************************************************************************************************/
/*************************************************************************************************/
/*************************************************************************************************/
/*************************************************************************************************/

/*	
	Funcion principal que se activa al dar click sobre el boton Fluir
*/
void SelBotonFluirObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID& protocol, 
	void* changedBy
)
{
	NodeIDList ListaNodosSelect;//Declaracion de la Lista de nodos seleccionados
//	int32 NumerodeAviso;//variable util en ciclo					
	PMString Date="";
	InterfacePtr<IControlView> view(theSubject, IID_ICONTROLVIEW);

	if (view != nil)
	{
		// Get the button ID from the view
		WidgetID theSelectedWidget = view->GetWidgetID();

		if (theSelectedWidget == kA2PImpOKButtonFluirWidgetID && theChange == kTrueStateMessage)
		{
			
			do 
			{
			
			
				InterfacePtr<IA2PFluirFunction> FluirFunction(static_cast<IA2PFluirFunction*> (CreateObject
				(
					kA2PFluirFunctionBoss,	// Object boss/class
					IA2PFluirFunction::kDefaultIID
				)));
				
				if(FluirFunction==nil)
				{
					
					break;
				}
					
				InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
				(
					kA2PPreferenciasFuncionesBoss,	// Object boss/class
					IA2PPreferenciasFunctions::kDefaultIID
				)));
				
				if(A2PPrefeFuntions==nil)
				{
					break;
				}
				
				Preferencias Prefer;
				
				if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
				{
					CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
					break;
				}
				
				
				FluirFunction->Fluir();
				
					
			
			
			
			} while(kFalse);
		}
    }
}




/* AutoAttach */
/************AL INICIAR LA FORMA SE CARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR************************************************************************/

void SelBotonFluirObserver::AutoAttach()
{//	//CAlert::WarningAlert("AutoAttach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_IBOOLEANCONTROLDATA);
	}
}

/* AutoDetach */
/************AL TERMINAR LA FORMA SE DESCARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR************************************************************************/

void SelBotonFluirObserver::AutoDetach()
{//	//CAlert::WarningAlert("AutoDetach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		//subject->DetachObserver(this);
	}
}

