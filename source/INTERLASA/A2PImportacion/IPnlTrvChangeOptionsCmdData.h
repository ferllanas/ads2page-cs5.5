//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/IPnlTrvChangeOptionsCmdData.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IPnlTrvCHANGEOPTIONSCMDDATA__
#define __IPnlTrvCHANGEOPTIONSCMDDATA__

#include "A2PImpID.h"


/** Data interface for PnlTrvChangeOptionsCmd, responsible 
for changing options for this plug-in.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class IPnlTrvChangeOptionsCmdData : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IPnlTrvChangeOptionsCmdData };

	/** Method to append option to list of options
		@param newVal [IN] new  value to append to option-list
	*/
	virtual void AddOption(const PMString& newVal)=0; 

	/** Accessor for stored option by index
		@param index [IN] specifies position of interest (zero-based)
		@return the option at this position
	*/
	virtual PMString GetNthOption(const int32 index)=0; 


	/**	Return count of options on this data interface
		@return int32 returned giving the number of options currently cached
	 */
	virtual int32 GetOptionCount()=0;

};

#endif // __IPnlTrvCHANGEOPTIONSCMDDATA__


