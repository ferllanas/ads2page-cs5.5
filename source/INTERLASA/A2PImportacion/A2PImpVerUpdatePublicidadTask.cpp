
//========================================================================================
//  
//  $File: //depot/shuksan/source/sdksamples/paneltreeview/A2PImpVerUpdatePublicidadTask.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: ipaterso $
//  
//  $DateTime: 2005/02/25 04:54:14 $
//  
//  $Revision: #6 $
//  
//  $Change: 320645 $
//  
//  Copyright 1997-2005 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// interface includes
#include "IApplication.h"
#include "IMenuManager.h"
#include "IActionManager.h"
#include "IIdleTaskMgr.h"
#include "ITreeViewMgr.h"
#include "IControlView.h"
#include "IBoolData.h"
#include "CAlert.h"
#include "Utils.h"
#include "IInCopyDocUtils.h"
#include "ILayoutUIUtils.h"
//#include "LayoutUIUtils.h"
#include "IActualizarFunction.h"

// General includes:

#include "CIdleTask.h"

// Project includes
#include "A2PImpID.h"

const static int kkN2PSQLInspeccionaNotasExecInterval = 15*1000;	//15 segundos
// milliseconds between checks

/** Implements IIdleTask, to refresh the view onto the file system.
	Just lets us keep up to date when people add new assets.
	This implementation isn't too respectful; it just clears the tree and calls ChangeRoot.

	
	@ingroup paneltreeview
*/

class A2PImpVerUpdatePublicidadTask : public CIdleTask
{
public:

	/**	Constructor
		@param boss boss object on which this interface is aggregated
	*/
	A2PImpVerUpdatePublicidadTask(IPMUnknown* boss);

	/**	Destructor
	*/
	virtual ~A2PImpVerUpdatePublicidadTask() {}


	/**	Called by the idle task manager in the app core when the task is running
		@param appFlags [IN] specifies set of flags which task can check to see if it should do something
			this time round
		@param timeCheck [IN] specifies how many milliseconds before the next active task becomes overdue.
		@return uint32 giving the interval (msec) that should elapse before calling this back
	 */
	virtual uint32 RunTask(uint32 appFlags, IdleTimer* timeCheck);

	/**	Get name of task
		@return const char* name of task
	 */
	virtual const char* TaskName();
	
	
protected:

	/**	Update the treeview.
	 */
	void refresh();
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(A2PImpVerUpdatePublicidadTask, kA2PImpVerUpdatePublicidadTaskImpl)

/* Constructor
*/
A2PImpVerUpdatePublicidadTask::A2PImpVerUpdatePublicidadTask(IPMUnknown *boss)
	:CIdleTask(boss)
{
}


/* RunTask
*/
uint32 A2PImpVerUpdatePublicidadTask::RunTask(
	uint32 appFlags, IdleTimer* timeCheck)
{
	if( appFlags & 
		( IIdleTaskMgr::kMouseTracking 
		| IIdleTaskMgr::kUserActive 
		| IIdleTaskMgr::kInBackground 
		| IIdleTaskMgr::kMenuUp))
	{
		return kOnFlagChange;
	}
	
	this->refresh();

	//PMString AAS="";
	//AAS.AppendNumber( PrefConections.TimeToCheckUpdateElements);
	//int32  kN2PSQLLogOffExecInterval2 =   (AAS.GetAsNumber() * 60) * 1000;//1 por 60 segundo = 1 minuto
	//CAlert::InformationAlert("Inspect UPDATE");
	// Has FS changed?
	return kkN2PSQLInspeccionaNotasExecInterval;
}


/* TaskName
*/
const char* A2PImpVerUpdatePublicidadTask::TaskName()
{
	return kA2PInspectActualizacionesTaskKey;
}


/* refresh
*/
void A2PImpVerUpdatePublicidadTask::refresh()
{
	do
	{
		InterfacePtr<IA2PActualizarFunction> ActualizarFunction(static_cast<IA2PActualizarFunction*> (CreateObject
				(
					kA2PActualizarFunctionBoss,	// Object boss/class
					IA2PActualizarFunction::kDefaultIID
				)));
				
				if(ActualizarFunction!=nil)
				{	
					ActualizarFunction->RevisarActualizacionDAvisos();					
				}
			
	} while(kFalse);
}
