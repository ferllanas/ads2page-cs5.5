#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"
#include "PMString.h"

#ifndef __A2PImpPreferencias_h__
#define __A2PImpPreferencias_h__
//Declaracion de la estructura de Preferencias

struct FechaData
{
	int32 yyy;
	int32 mm;
	int32 dd;
	int32 hr;
	int32 min;
	int32 sec;
};

struct StructCamposUpdate
{
	PMString ID;  //int32 UIDFrameToImage;		//UIDFrame
	PMString Ruta;	//PMString PathToImage;		//Ruta
	FechaData fecha; //PMString LastTimeImage;
	
	//Adicionales para XMP
	int32 NumPagFlowed;			//numero de pagina fluido
	int32 UIDPageFlowed;		//
	int32 UIDFrameEtiqueta;
	PMString DLImtedImage; //Date Last Imported Image
	bool16 ExistImageFile;	//Si existe el archivo 
	PMReal LFrameImage;		//Left
	PMReal TFrameImage;		//Top
	PMReal RFrameImage;		//Rigth
	PMReal BFrameImage;		//Buttom
	
};



#endif // __SelDlgID_h__