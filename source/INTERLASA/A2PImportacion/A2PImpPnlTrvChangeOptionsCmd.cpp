//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvChangeOptionsCmd.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "ISubject.h"
#include "IWorkspace.h"

// General includes:
#include "CmdUtils.h"
#include "ErrorUtils.h"
#include "Command.h"
#include "CAlert.h"

// Project includes:
#include "A2PImpID.h"
#include "IPnlTrvOptions.h"
#include "IPnlTrvChangeOptionsCmdData.h"


/** Implementation of command to change the preferences persisted to the 
	session workspace through our IPnlTrvOptions interface.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class PnlTrvChangeOptionsCmd : public Command
{
public:
	/** Constructor.
		@param boss interface ptr from boss object on which this interface is aggregated.*/
	PnlTrvChangeOptionsCmd(IPMUnknown* boss);

protected:
	/** Performs notification. */
	virtual void DoNotify();

	/** Implements command. */
	virtual void Do();

	/** Undoes command. */
	virtual void Undo();

	/** Re-implements command. */
	virtual void Redo();

	/** Sets command name. */
	virtual PMString* CreateName();

private:
	K2Vector<PMString> fOldOptionList;
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(PnlTrvChangeOptionsCmd, kA2PImpTrvChangeOptionsCmdImpl)


/* Constructor
*/
PnlTrvChangeOptionsCmd::PnlTrvChangeOptionsCmd(IPMUnknown* boss) :
	Command(boss)
{
}


/* Do
*/
void PnlTrvChangeOptionsCmd::Do()
{
	ErrorCode status = kFailure;
	do						
	{
		
		InterfacePtr<IPnlTrvChangeOptionsCmdData> 
			cmdData (this, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil)
		{
			break;
		}
		
		// Get the ItemList
		const UIDList* itemList = this->GetItemList();
		if (itemList == nil || itemList->IsEmpty())
		{
			break;
		}
		UIDRef uidRef = itemList->GetRef(0);
		InterfacePtr<IPnlTrvOptions> 
			iPnlTrvOptions(uidRef, UseDefaultIID());
		ASSERT(iPnlTrvOptions);
		if (iPnlTrvOptions == nil)
		{
			break;
		}

		ASSERT(cmdData->GetOptionCount()>0);

		// Store current option list for later Undo.
		iPnlTrvOptions->GetOptionListCopy(fOldOptionList);

		for(int32 i=0; i < cmdData->GetOptionCount(); i++)
		{
			iPnlTrvOptions->AddOption(cmdData->GetNthOption(i), i);
		}
		status = kSuccess;

	} while(kFalse);		

	// Handle any errors
	if (status != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(status);
	}
}


/* Undo
*/
void PnlTrvChangeOptionsCmd::Undo()
{
	ErrorCode status = kFailure;
	do						
	{
		
		if(this->fOldOptionList.empty() ) {
			break;
			// No previous option has been stored
		}
		
		const UIDList* itemList = this->GetItemList();
		if (itemList == nil || itemList->IsEmpty())
		{
			break;
		}
		UIDRef uidRef = itemList->GetRef(0);
		InterfacePtr<IPnlTrvOptions> 
			iPnlTrvOptions(uidRef, UseDefaultIID());
		ASSERT(iPnlTrvOptions);
		if (iPnlTrvOptions == nil)
		{
			break;
		}

		// Restore previous option
		for(int32 i=0; i < fOldOptionList.size(); i++) {
			iPnlTrvOptions->AddOption(fOldOptionList[i], i);
		}
		status = kSuccess;

		this->DoNotify();

	} while(kFalse);		

	// Handle any errors
	if (status != kSuccess)
	{
		ErrorUtils::PMSetGlobalErrorCode(status);
	}
}


/* Redo
*/
void PnlTrvChangeOptionsCmd::Redo()
{
	
	this->Do();
	this->DoNotify();
}


/* DoNotify
*/
void PnlTrvChangeOptionsCmd::DoNotify()
{
	
	InterfacePtr<IWorkspace> theWorkSpace(GetExecutionContextSession()-> QueryWorkspace());
	InterfacePtr<ISubject> subject(theWorkSpace, IID_ISUBJECT);
	if (subject)
	{
		subject->Change(kA2PImpPnlTrvChangeOptionsCmdBoss, IID_IPnlTrvOptions, this);
	}
}


/* CreateName
*/
PMString* PnlTrvChangeOptionsCmd::CreateName()
{
	
	return new PMString(kA2PImpChangeOptionsCmdKey);
}
