//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvUtils.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __PnlTrvUtils_H_DEFINED__
#define __PnlTrvUtils_H_DEFINED__

#include "A2PImpStructPreferencias.h"
class IDocument;
class IControlView;

/** A collection of miscellaneous utility methods used by this plug-in.
@ingroup paneltreeview
*/
class PnlTrvUtils
{

public:
	/** Return a version of the supplied path which consists only of the 
		file 'name', removing everything up to and including the last path separator.
		@param fromthis [IN] path of interest
		@return string containing the path-part minus the last segment
	*/
	static PMString TruncatePath(const PMString& fromthis);

	/*
	*/
	static PMString TruncateExtencion(const PMString& fromthis);

	/*
	*/
	static bool16 validPath(const PMString& p);
	
	/** Attempt to import item (which can be any format) from path; loads
		place gun if item can be imported.
		@param docUIDRef [IN] specifies document into which item would be placed
		@param fromPath [IN] specifies the path in the local filesystem of the item to attempt to import
		@return UIDRef of item imported, or UIDRef::gNull otherwise
	*/
	static UIDRef ImportImageAndLoadPlaceGun(
		const UIDRef& docUIDRef, const PMString& fromPath);

	/**	Get a widget from the specified panel by WidgetID, returning nil if it can't be found.
		@param panelWidgetID [IN] specifies the parent widget
		@param widgetID [IN] sought dependent widget
		@return interface ptr to widget found or nil if it can't be found; not a reference incremented one though.
	 */
	static IControlView* GetWidgetOnPanel(
	const WidgetID& panelWidgetID, const WidgetID& widgetID);
	
	static bool16 GetXMPVar(PMString Variable,PMString& value);
	
	static bool16 SaveXMPVar(PMString Variable,PMString value);
	
	static bool16 RemoveXMPVar(PMString Variable);
	
	static PMString CrearFolderPreferencias();
	
	static bool16 ProporcionaStructFechaDataDelArchivo(PMString Archivo, struct FechaData *Date );
	
	static bool16 ProporcionaStructFechaDataDeCadena(PMString Cadena, struct FechaData *Date);

};

#endif // __PnlTrvUtils_H_DEFINED__

