//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvStartupShutdown.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interface includes:
#include "ISession.h"
#include "IIdleTask.h"

// General includes:
#include "CPMUnknown.h"

// Project includes:
#include "A2PImpID.h"
#include "IStartupShutdownService.h"

#include "CAlert.h"

/** Implements IStartupShutdownService; purpose is to install the idle task.

	@author Ian Paterson
	@ingroup paneltreeview
*/
class PnlTrvStartupShutdown : 
	public CPMUnknown<IStartupShutdownService>
{
public:
	/**	Constructor
		@param boss interface ptr from boss object on which this interface is aggregated.
	 */
    PnlTrvStartupShutdown(IPMUnknown* boss );

	/**	Destructor
	 */
	virtual ~PnlTrvStartupShutdown() {}

	/**	Called by the core when app is starting up
	 */
    virtual void Startup();

	/**	Called by the core when app is shutting down
	 */
    virtual void Shutdown();
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(PnlTrvStartupShutdown, kA2PImpTrvStartupShutdownImpl)

/* Constructor
*/
PnlTrvStartupShutdown::PnlTrvStartupShutdown(IPMUnknown* boss) :
    CPMUnknown<IStartupShutdownService>(boss)
{
}


/* Startup
*/
void PnlTrvStartupShutdown::Startup()
{
    do 
	{
	if( MarcoDebug == kTrue ){CAlert::InformationAlert("alert del startupShutdown");}
		InterfacePtr<IIdleTask> task(GetExecutionContextSession(), IID_IPnlTrvIdleTask);
		ASSERT(task);
		if(!task)
		{
			CAlert::InformationAlert("no esta el timer");
			break;
		}
		task->InstallTask(0);
		InterfacePtr<IIdleTask> taskVerUpdate(GetExecutionContextSession(), IID_IA2PImlVerUpdatePublicidadTask);
		ASSERT(taskVerUpdate);
		if(!taskVerUpdate)
		{
			CAlert::InformationAlert("no esta el timer");
			break;
		}
		taskVerUpdate->InstallTask(0);
		
		InterfacePtr<IIdleTask> timer(GetExecutionContextSession(), IID_IA2PImpAlertaDeCambioTask);//temporizador
		ASSERT(timer);
		if(!timer){
			CAlert::InformationAlert("no esta el timer");
			break;
		}
		timer->InstallTask(0);//se supone ke empieza a correr la primera vez el tiempo que le pongas ahi en milisegundos
		
	} while(kFalse);
}


/* Shutdown
*/
void PnlTrvStartupShutdown::Shutdown()
{
	do
	{
		InterfacePtr<IIdleTask> 
			task(GetExecutionContextSession(), IID_IPnlTrvIdleTask);
		ASSERT(task);
		if(!task)
		{
			break;
		}
		task->UninstallTask();
		InterfacePtr<IIdleTask> taskVerUpdate(GetExecutionContextSession(), IID_IA2PImlVerUpdatePublicidadTask);
		ASSERT(taskVerUpdate);
		if(!taskVerUpdate)
		{
			break;
		}
		taskVerUpdate->UninstallTask();
	} while(kFalse);
}

 