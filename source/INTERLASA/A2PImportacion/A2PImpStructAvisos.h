/////////////////////////////////////////////////////////////////////////
//////ESTRUCTURA AVISOS/////////////////////////////////////////////////
/*DENTRO DE ESTA ESTRUCTURA SE GUARDARAN EL NOMBRE DEL CAMPO Y SU CORRESPONDIENTE CONTENIDO 
	ESTA ESTRUCTURA SE UTILIZARA CUANDO SE HAYA SCANEADO EL ARCHIVO QUE SE DESEA IMPORTAR
	Y SE GENERE EL ARBOL DE SELECCION 
*/

#ifndef __StructAvisos_h__
#define __StructAvisos_h__


struct StructAvisos
{
		PMString NomCampo1;
		PMString ContenidoCampo1;
		PMString NomCampo2;
		PMString ContenidoCampo2;
		PMString NomCampo3;
		PMString ContenidoCampo3;
		PMString NomCampo4;
		PMString ContenidoCampo4;
		PMString NomCampo5;
		PMString ContenidoCampo5;
		PMString NomCampo6;
		PMString ContenidoCampo6;
		PMString NomCampo7;
		PMString ContenidoCampo7;
		PMString NomCampo8;
		PMString ContenidoCampo8;
		PMString NomCampo9;
		PMString ContenidoCampo9;
		PMString NomCampo10;
		PMString ContenidoCampo10;
		PMString NomCampo11;
		PMString ContenidoCampo11;
		PMString NomCampo12;
		PMString ContenidoCampo12;
		PMString NomCampo13;
		PMString ContenidoCampo13;
		PMString NomCampo14;
		PMString ContenidoCampo14;
		
		//Adicionales para XMP
		int32 NumPagFlowed;			//numero de pagina fluido
		int32 UIDPageFlowed;		//
		int32 UIDFrameToImage;		//UIDFrame
		int32 UIDFrameEtiqueta;
		PMString PathToImage;		//Ruta
		PMString DLImtedImage; //Date Last Imported Image
		bool16 ExistImageFile;	//Si existe el archivo 
		PMReal LFrameImage;		//Left
		PMReal TFrameImage;		//Top
		PMReal RFrameImage;		//Rigth
		PMReal BFrameImage;		//Buttom
		PMString LastTimeImage;
	
}; 


struct SeccionesUpdate
{
	PMString NombreSeccion;
	int32 UIDPagDeSeccion;
};
//StructAvisos Aviso=new StructAvisos[];
#endif // __SelDlgID_h__