/*
 *  A2PImpBlinkAvisoDeCambio.cpp
 *  A2PImp
 *
 *  Created by marco on 03/06/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */
 
#include "VCPlugInHeaders.h"

//#include "A2PImpBlinkAvisoDeCambio.h"

#include "CAlert.h"
#include "A2PImpID.h"

//#include <IControlView.h>
//#include <ITrackerTimer.h>
#include "IIdleTaskMgr.h"
#include "CIdleTask.h"
#include "IActualizarFunction.h"

#include "PnlTrvUtils.h"


const static int A2PBlinkAvisoDeCambioInterval = 60*1000;//15 SEGUNDOS para calar pero debe ser un minuto
//const static int A2PBlinkAvisoDeCambioInterval = 60*1000;

class A2PImpBlinkAvisoDeCambio : public CIdleTask
{
public: 

	A2PImpBlinkAvisoDeCambio(IPMUnknown *boss);

	virtual ~A2PImpBlinkAvisoDeCambio(){}
	
	virtual uint32 RunTask(uint32 appFlags, IdleTimer* timeCheck);

	/**	Get name of task
		@return const char* name of task
	 */
	virtual const char* TaskName();
		
protected:
	/**	Update the treeview.
	 */
	void refresh();
	/*virtual bool16 Avisar(bool16 avisar, int FechaEntrada, int FechaActual);*/

};

CREATE_PMINTERFACE(A2PImpBlinkAvisoDeCambio, kA2PImpAlertaDeCambioTaskImpl)

A2PImpBlinkAvisoDeCambio::A2PImpBlinkAvisoDeCambio(IPMUnknown *boss): CIdleTask(boss)
{
}

/* RunTask
*/
uint32 A2PImpBlinkAvisoDeCambio::RunTask(
	uint32 appFlags, IdleTimer* timeCheck)
{
	if( appFlags & 
		( IIdleTaskMgr::kMouseTracking 
		| IIdleTaskMgr::kUserActive 
		| IIdleTaskMgr::kInBackground 
		| IIdleTaskMgr::kMenuUp))
	{
		//CAlert::InformationAlert("alerta en el runtask if appflags");
		return kOnFlagChange;
	}
	
	this->refresh();

	//PMString AAS="";
	//AAS.AppendNumber( PrefConections.TimeToCheckUpdateElements);
	//int32  kN2PSQLLogOffExecInterval2 =   (AAS.GetAsNumber() * 60) * 1000;//1 por 60 segundo = 1 minuto
	if(MarcoDebug==kTrue){CAlert::InformationAlert("Inspect UPDATE");}
	//Has FS changed?
	//return kkN2PSQLInspeccionaNotasExecInterval;
	return A2PBlinkAvisoDeCambioInterval; // lo que hace segun es retornar el valor de los segundos con los qe se trabajara ahora
}

/* TaskName
*/
const char* A2PImpBlinkAvisoDeCambio::TaskName()
{
	return kA2PInspectActualizacionesTaskKey;
}

/* refresh
*/
void A2PImpBlinkAvisoDeCambio::refresh()
{
	if( MarcoDebug == kTrue ){CAlert::InformationAlert("yeah");}
	
	do
	{
		InterfacePtr<IA2PActualizarFunction> ActualizarFunction(static_cast<IA2PActualizarFunction*> (CreateObject
				(
					kA2PActualizarFunctionBoss,	// Object boss/class
					IA2PActualizarFunction::kDefaultIID
				)));
				
				if(ActualizarFunction != nil)
				{	
					ActualizarFunction->RevisarModificacionParaBlinkAvisoDeCambio(kFalse);//entra la funcion para blikear que esta en el A2PImpFunctionActualizar.cpp				
				}
			
	} while(kFalse);

}
