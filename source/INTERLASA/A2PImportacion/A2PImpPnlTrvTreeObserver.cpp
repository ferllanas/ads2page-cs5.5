//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvTreeObserver.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes
#include "ISubject.h"
#include "IControlView.h"
#include "ITreeViewMgr.h"
#include "IPanelControlData.h"
#include "ITreeNodeIDData.h"
#include "ITreeViewController.h"
#include "ISysFileData.h"
#include "IBoolData.h"
#include "IPMStream.h"
#include "IWidgetParent.h"
#include "IWorkspace.h"
#include "ITextControlData.h"
#include "IWidgetParent.h"
#include "ILayoutControlData.h"
#include "IComposeScanner.h"
#include "IImportProvider.h"
#include "ITextModel.h"
#include "IDocument.h"
#include "IProgressBarManager.h"
#include "IHierarchy.h"
#include "IPMStream.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IImportProvider.h"
#include "IApplication.h"



// General includes
#include "CObserver.h"
#include "CAlert.h"
#include "K2Vector.tpp" // For NodeIDList to compile
#include "StreamUtil.h"
#include "ILayoutUIUtils.h"
// Project includes
#include "PnlTrvFileNodeID.h"
#include "A2PImpID.h"
//#include "CString_To_Integer.cpp"
#include "IPnlTrvOptions.h"
#include "SDKFileHelper.h"
//#include "PalettePanelUtils.h"
#include "FileUtils.h"

#include "ITriStateControlData.h"
#include "ITreeViewHierarchyAdapter.h"
#include "ITreeViewMgr.h"
#include "NodeID.h"

#ifdef MACINTOSH
	#include "A2PPrefID.h"
	#include "IA2PPreferenciasFunciones.h"
#endif


#ifdef WINDOWS
	#include "..\A2P_PREFERENCIAS\A2PPrefID.h"
	#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif


//#include "N2PCSInputOutputsFILES.h"
//#include "N2PCSStructPreferencias.h"

/** Implements IObserver; initialises the tree when an IObserver::AutoAttach message sent
	and listens for when the node selection changes.
	When the selection changes and there's a non-empty selection list, it takes the first item
	and previews the asset if it can do so (for instance, if it's a GIF, JPEG etc).
	It does this by setting a path on a data interface of the custom-view panel widget and
	invalidating the widget, forcing it to redraw to create a rendering of the new asset.

	 The class is derived from CObserver, and overrides the
	AutoAttach(), AutoDetach(), and Update() methods.
	This class implements the IObserver interface using the CObserver helper class,
	and is listening along the IID_ITREEVIEWCONTROLLER protocol for changes in the tree data model.

	@ingroup paneltreeview
	@author Ian Paterson
*/
class PnlTrvTreeObserver : public CObserver
{
public:

	Preferencias Preferencia;
	/**
		Constructor for WLBListBoxObserver class.
		@param interface ptr from boss object on which this interface is aggregated.
	*/
	PnlTrvTreeObserver(IPMUnknown* boss);

	/**
		Destructor 
	*/	
	~PnlTrvTreeObserver();

	/**
		AutoAttach is only called for registered observers
		of widgets.  This method is called by the application
		core when the widget is shown.
	*/	
	virtual void AutoAttach();

	/**
		AutoDetach is only called for registered observers
		of widgets. Called when widget hidden.
	*/	
	virtual void AutoDetach();

	/**
		Update is called for all registered observers, and is
		the method through which changes are broadcast. 
		@param theChange [IN] this is specified by the agent of change; it can be the class ID of the agent,
		or it may be some specialised message ID.
		@param theSubject [IN] provides a reference to the object which has changed; in this case, the button
		widget boss object that is being observed.
		@param protocol [IN] the protocol along which the change occurred.
		@param changedBy [IN] this can be used to provide additional information about the change or a reference
		to the boss object that caused the change.
	*/	
	virtual void Update(
		const ClassID& theChange, 
		ISubject* theSubject, 
		const PMIID &protocol, 
		void* changedBy);

protected:

	/**	Handles a change in the selection when the end user 
	either clicks on a node or clicks off it
	*/
	void handleSelectionChanged();

	/**	Populates the tree model. 
	 */
	void initTree();

};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(PnlTrvTreeObserver, kA2PImpTrvTreeObserverImpl)


/* Constructor
*/
PnlTrvTreeObserver::PnlTrvTreeObserver(IPMUnknown* boss)
: CObserver(boss)
{
}


/* Destructor
*/
PnlTrvTreeObserver::~PnlTrvTreeObserver()
{
}


/* AutoAttach
*/
void PnlTrvTreeObserver::AutoAttach()
{
	// Show/refresh tree when widget made visible
	initTree();
	InterfacePtr<ISubject> subj(this, UseDefaultIID()); 
	ASSERT(subj);
	subj->AttachObserver(this, IID_ITREEVIEWCONTROLLER);

	InterfacePtr<IWorkspace> ws(GetExecutionContextSession()-> QueryWorkspace()); 
	InterfacePtr<IPnlTrvOptions> ptOptions(ws, UseDefaultIID());
	if (ptOptions)
	{
		InterfacePtr<ISubject> subj(ptOptions, UseDefaultIID());
		if (subj)
		{
			subj->AttachObserver(this, IID_IPnlTrvOptions);
		}
	}
}


/* AutoDetach
*/
void PnlTrvTreeObserver::AutoDetach()
{
	InterfacePtr<ISubject> subj(this, UseDefaultIID()); 
	ASSERT(subj);
	subj->DetachObserver(this, IID_ITREEVIEWCONTROLLER);

	InterfacePtr<IWorkspace> ws(GetExecutionContextSession()-> QueryWorkspace()); 
	InterfacePtr<IPnlTrvOptions> ptOptions(ws, UseDefaultIID());
	if (ptOptions)
	{
		InterfacePtr<ISubject> subj(ptOptions, UseDefaultIID());
		if (subj)
		{
			subj->DetachObserver(this, IID_IPnlTrvOptions);
		}
	}
}


/* Update
*/
void PnlTrvTreeObserver::Update(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy)
{
	switch(theChange.Get())
	{
		case kListSelectionChangedMessage:
		{
			this->handleSelectionChanged();
			break;
		}
		case kA2PImpPnlTrvChangeOptionsCmdBoss:
		{
			this->initTree();
			break;
		}
	}
}


/* initTree
*/
void PnlTrvTreeObserver::initTree()
{
	do
	{
		InterfacePtr<ITreeViewMgr>	
			treeViewMgr(this, UseDefaultIID());
		ASSERT(treeViewMgr);
		if(treeViewMgr == nil)
		{
			break;
		}
		treeViewMgr->ClearTree();
		treeViewMgr->ChangeRoot(kTrue);
	} while(kFalse);
}


/* handleSelectionChanged
*/
void PnlTrvTreeObserver::handleSelectionChanged()
{
	do
	{
			//Se obtiene la interfaz de la jerarquia de arbol
		InterfacePtr<ITreeViewHierarchyAdapter> Herrarquia(this,UseDefaultIID());

		//se obtiene el control de la vista del arbol 
		InterfacePtr<ITreeViewController> controller(this,UseDefaultIID());

		//se obtiene el manejador de la vista del arbol
		InterfacePtr<ITreeViewMgr>	treeViewMgr(this, UseDefaultIID());
		ASSERT(controller);
		if(!controller) {
			break;
		}
		
		//se obtien el NodeID_rv del nodo padre de todos los nodos apartir de la jerarquia del arbol
		NodeID_rv NodoPadre=Herrarquia->GetRootNode();

		//obtengo el NodeID del nodo padre
		NodeID NodePadre=NodoPadre;
		
		//Obtiene la cantidad de nodos seccion o nodos numNodoSeccions
		int32 ContSecciones=Herrarquia->GetNumChildren(NodePadre);
		//numero de nodo seccion
		int32 numNodoSeccion;
		//ciclo para deseleccionar los check box de los nodos
		for(numNodoSeccion=0;numNodoSeccion<ContSecciones;numNodoSeccion++)
		{	
			//obtengo el NodeID_rv del nodo seccion actual
			NodeID_rv NodoSeccionID_rv=Herrarquia->GetNthChild(NodePadre,numNodoSeccion);

			//obtengo el NodeID de la seccion actual
			NodeID NodoSeccionID=NodoSeccionID_rv;

			//obtengo el control de la vista del nodo apartir de el manejador de vista del arbol
			IControlView* ControlNode=treeViewMgr->QueryWidgetFromNode(NodoSeccionID);

			//obtengo la interfaz IPanelControlData a partir dell control de vista del nodo actual
			InterfacePtr<IPanelControlData> iPanelControlData(ControlNode, UseDefaultIID());
			ASSERT(iPanelControlData);
			if(iPanelControlData==nil) 
			{
				break;
			}

			//obtengo el control de vista del check box a partir de su ID
			IControlView * iControlView  = iPanelControlData->FindWidget(kA2PImpTreeNodeCheckBoxWidgetID);
			//Desabilito el check box
			ASSERT(iControlView);
			if(iControlView==nil) 
			{
				break;
			}
			iControlView->Disable(kTrue);

			//obtengo la interfaz ITriStateControlData apartir del conttrrol de vista del check box
			InterfacePtr<ITriStateControlData> iTriStateControlData(iControlView, UseDefaultIID());
			ASSERT(iTriStateControlData);
			if(iTriStateControlData==nil) 
			{
				break;
			}

			//Deselciono todos los checkbox de todos los nodos nodo por nodo
			iTriStateControlData->Deselect(kTrue);
		}

		//ahora selecciono el checbox del nodo seleccionado
		//obtengo la lista de nodos seleccionados
		NodeIDList selectedItems;
		controller->GetSelectedItems(selectedItems);
		// We've got single selection only
		if(selectedItems.size()>0) //si la cantidad de nodos seleccionados esmayor que uno
		{
			TreeNodePtr<PnlTrvFileNodeID> nodeID(selectedItems[0]);//tomo el PnlTrvFileNodeID del primer nodo selecionado
			ASSERT(nodeID);
			if(!nodeID) 
			{
				break;
			}

			//obtengo el path
			PMString pstr(nodeID->GetPath());


			NodeID Nodo=selectedItems[0];
			IControlView* ControlNode=treeViewMgr->QueryWidgetFromNode(Nodo);
			
			InterfacePtr<IPanelControlData> iPanelControlData(ControlNode, UseDefaultIID());
			ASSERT(iPanelControlData);
			if(iPanelControlData==nil) 
			{
				break;
			}

			//Obtiene el conmtro de vista del check box sobre el nodod
			IControlView * iControlView  = iPanelControlData->FindWidget(kA2PImpTreeNodeCheckBoxWidgetID);
			ASSERT(iControlView);
			if(iControlView==nil) 
			{
				break;
			}

			iControlView->Enable(kFalse);//deshabilita el checkbox del nodo seleccionado
			InterfacePtr<ITriStateControlData> iTriStateControlData(iControlView, UseDefaultIID());
			ASSERT(iTriStateControlData);
			if(iTriStateControlData==nil) 
			{
				break;
			}
			iTriStateControlData->Select(kTrue);//Palomea el check box del nodo seleccionado
		}

		
	} while(kFalse);
}

/*void A2PImpActionComponent::handleRefresh()
{
	do
	{
		IControlView* treeWidget = PnlTrvUtils::GetWidgetOnPanel(
			kA2PImpPanelWidgetID, kA2PImpTreeViewWidgetID);
		ASSERT(treeWidget);
		if(!treeWidget)
		{
			break;
		}
		InterfacePtr<IBoolData> 
			refreshStatus(GetExecutionContextSession(), IID_IPnlTrvRefreshStatus);
		ASSERT(refreshStatus);
		if(refreshStatus)
		{
			refreshStatus->Set(kTrue);
			// The model code will clear this flag
			// This flag gives us a way to communicate that the tree model
			// should be updated even if we haven't changed the root
			InterfacePtr<ITreeViewMgr> 
				iTreeViewMgr(treeWidget, UseDefaultIID());
			ASSERT(iTreeViewMgr);
			if(!iTreeViewMgr)
			{
				break;
			}
			iTreeViewMgr->ClearTree();
			iTreeViewMgr->ChangeRoot(kTrue);
		}
	} while(kFalse);
}
*/


