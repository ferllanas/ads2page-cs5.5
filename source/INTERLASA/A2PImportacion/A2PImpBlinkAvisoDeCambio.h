/*
 *  A2PImpBlinkAvisoDeCambio.h
 *  A2PImp
 *
 *  Created by marco on 03/06/10.
 *  Copyright 2010 __MyCompanyName__. All rights reserved.
 *
 */

#ifndef _A2PImpBlinkAvisoDeCambio_
#define _A2PImpBlinkAvisoDeCambio_

//includes:
//poner aqui los includes...

class IA2PImpBlinkAvisoDeCambio : public CIdleTask
{

//virtual bool16 Avisar(bool16 avisar, int FechaEntrada, int FechaActual)=0;

	virtual uint32 RunTask(uint32 appFlags, IdleTimer* timeCheck);

	virtual const char* TaskName();
	
	void refresh();
	

};

#endif