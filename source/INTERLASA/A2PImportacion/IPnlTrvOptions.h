//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/IPnlTrvOptions.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __IPnlTrvOptions_H_DEFINED__
#define __IPnlTrvOptions_H_DEFINED__
#include "A2PImpID.h"


/** Persistent interface that will be added in to the session workspace, holding options
	for this plug-in.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class IPnlTrvOptions : public IPMUnknown
{
public:

	enum { kDefaultIID = IID_IPnlTrvOptions };

	/** Append an option to the persistent list. If the index you specify is outwith the length, then it'll get appended to the list
		@param newVal [IN] new value to add to existing list of options
		@param indexWhere [IN] where you want it in the list of options, zero based
	*/
	virtual void AddOption(
		const PMString& newVal, const int32 indexWhere)=0; 

	/**	Return option at given index in list
		@param index [IN] zero-based index
		@return PMString giving option at specified position
	 */
	virtual PMString GetNthOption(const int32 index) =0; 

	/** Retrieves current list of options
		@param outList OUT contains copy of list of options
	*/
	virtual void GetOptionListCopy(K2Vector<PMString>& outList)=0;
};

#endif // __IPnlTrvOptions_H_DEFINED__


