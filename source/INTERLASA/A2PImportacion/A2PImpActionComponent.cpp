//========================================================================================
//  
//  $File: $
//  
//  Owner: Juan Fernando  Llanas Rdz.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
// Interface includes:
#include "ISession.h"
#include "IApplication.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialogCreator.h"
#include "IPanelControlData.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "IDialog.h"
#include "IScrapItem.h"
#include "ITextModelCmds.h"
#include "ITextModel.h"

#include "IPnlTrvOptions.h"
#include "IWorkspace.h"
#include "IPnlTrvChangeOptionsCmdData.h"
#include "ITreeViewMgr.h"
#include "IControlView.h"
#include "IBoolData.h"

#include "IOpenFileDialog.h"
#include "IActionManager.h"
#include "IApplication.h"
#include "ICellContent.h"
#include "IDocument.h"
#include "IGraphicFrameData.h"
#include "IFrameType.h"
#include "IHierarchy.h"
#include "IIDXMLElement.h"
#include "ISelectionManager.h" // We're querying suite interfaces off IActiveContext::GetContextSelection()
#include "IStringData.h"
#include "ITableModel.h"
//#include "ITextFrame.h"
#include "IXMLAttributeCommands.h"
#include "IXMLCreateElementCmdData.h"
#include "IXMLLoadDTDCmdData.h"
#include "IXMLReferenceData.h"
#include "IXMLStructureSuite.h"
#include "IXMLTagSuite.h"
#include "IXMLUtils.h"
#include "IXMLValidator.h"
#include "IXMLTagList.h"
#include "IWorkspace.h"
#include "XMLReference.h"

// General includes:
#include "XMLContentIterator.h"
#include "K2Vector.h"
#include "K2Vector.tpp"

#include "IDocument.h"
#include "IXMLTagList.h"
#include "IXMLUtils.h"
#include "IHierarchy.h"
// General includes:
#include "CActionComponent.h"
#include "PnlTrvUtils.h"
#include "UIDList.h"
#include "CmdUtils.h"
#include "CAlert.h"
#include "ILayoutUIUtils.h"
#include "IPathUtils.h"
#include "TransformUtils.h"
#include "k2Vector.h"
#include "K2Vector.tpp"
// Project includes:
#include "SDKFileHelper.h"
#include "SysFileList.h"

//#include "SnpXMLHelper.h"

#include "A2PImpID.h"
//#include "InterlasaChecaDoungle.cpp"
//#include "ObtenerMacAddresdeRegistro.cpp"
#include "IActualizarFunction.h"

#ifdef MACINTOSH
#include "IA2PPreferenciasFunciones.h"
#endif
#ifdef WINDOWS
#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif
#include "ImportImagenClass.h"
#include "A2PAvisosHeader.h"

//#include "IPlantillerImpIMP.h"
//#include "PlantillerImpID.h"

/** Implements IActionComponent; performs the actions that are executed when the plug-in's
	menu items are selected.

*/
class A2PImpActionComponent : public CActionComponent
{
public:
/**
 Constructor.
 @param boss interface ptr from boss object on which this interface is aggregated.
 */
		A2PImpActionComponent(IPMUnknown* boss);

		/** The action component should perform the requested action.
			This is where the menu item's action is taken.
			When a menu item is selected, the Menu Manager determines
			which plug-in is responsible for it, and calls its DoAction
			with the ID for the menu item chosen.

			@param actionID identifies the menu item that was selected.
			@param ac active context
			@param mousePoint contains the global mouse location at time of event causing action (e.g. context menus). kInvalidMousePoint if not relevant.
			@param widget contains the widget that invoked this action. May be nil. 
			*/
		virtual void DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget);

	private:
		/** Encapsulates functionality for the about menu item. */
		void handleAbout();

		/** Encapsulates functionality for the Refresh menu item. */
		void handleRefresh();
	
		/** Encapsulates functionality for the SetOptions menu item. */
		void handleSetOptions();
		
		/**Metodo para cerrar la paleta import. */
		void DoPanel();

		/**	Method to retrieve a folder 
			@param pmTitlePtr 
			@return PMString 
		 */
		PMString getSelectedFolderFromDialog(const PMString* pmTitlePtr);


		/**	Exec cmd to change options for this plug-in
			@param options list of options to drive this command with
			@return  kSuccess if we could process it, kFailure otherwise
		 */
		ErrorCode processSetOptionsCommand(const K2Vector<PMString>& options);



};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(A2PImpActionComponent, kA2PImpActionComponentImpl)
									

/* A2PImpActionComponent Constructor
*/
A2PImpActionComponent::A2PImpActionComponent(IPMUnknown* boss)
: CActionComponent(boss)
{
}

/* DoAction
*/
void A2PImpActionComponent::DoAction(IActiveContext* ac, ActionID actionID, GSysPoint mousePoint, IPMUnknown* widget)
{

	Preferencias Prefer;
	switch (actionID.Get())
	{

		case kA2PImpPopupAboutThisActionID:
		case kA2PImpAboutActionID:
		{
			this->handleAbout();//se presiono el menu about del plug in
			/**CUANDO SELECCIONAS EL ABOUT DEL PLUGIN*/
		
			break;
		}

		case kA2PImpRefreshActionID://se prsiono el menu refresh
		{
			//CAlert::InformationAlert("Checar fallo en refresh");
			
			InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
				(
					kA2PPreferenciasFuncionesBoss,	// Object boss/class
					IA2PPreferenciasFunctions::kDefaultIID
				)));
				
				if(A2PPrefeFuntions==nil)
				{
					break;
				}
				
				if(A2PPrefeFuntions->VerificaSeguridadPirateria())
				{
					PMString DefaulPrefer=PnlTrvUtils::CrearFolderPreferencias();
					DefaulPrefer.Append("Default.pfa");
					if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
					{
						CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
						break;
					}
					
					this->handleRefresh();
				}
					
				
			break;
		}


		case kA2PImpSetOptionsActionID://se presiono el boton poner opciones  del menusito importar
		{								//o cambio de rootnode
			//CAlert::InformationAlert("checar fallo  import");
			
			InterfacePtr<IA2PPreferenciasFunctions>A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
				(
					kA2PPreferenciasFuncionesBoss,	// Object boss/class
					IA2PPreferenciasFunctions::kDefaultIID
				)));
				if(A2PPrefeFuntions==nil)
				{CAlert::InformationAlert("A2PPrefeFuntions==nil en importar");	break;}
				
				if(A2PPrefeFuntions->VerificaSeguridadPirateria())
				{
                    //CAlert::InformationAlert(" entro a A2PPrefeFuntions en importar");
					PMString DefaulPrefer=PnlTrvUtils::CrearFolderPreferencias();
					DefaulPrefer.Append("Default.pfa");
					if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
					{
						CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
						break;
					}
					this->handleSetOptions();
				}
			break;
		}

		case kA2PImpPanelImportDialogActionID:// se selecciono el menu importar
		{
			//CAlert::InformationAlert("checar fallo 1");
			
			InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
				(
					kA2PPreferenciasFuncionesBoss,	// Object boss/class
					IA2PPreferenciasFunctions::kDefaultIID
				)));
				if(A2PPrefeFuntions==nil)
				{ CAlert::InformationAlert("A2PPrefeFuntions==nil en importar");
					break;
				}
				
				if(A2PPrefeFuntions->VerificaSeguridadPirateria())
				{
					//CAlert::InformationAlert("checar fallo 2");
					PMString DefaulPrefer=PnlTrvUtils::CrearFolderPreferencias();
					DefaulPrefer.Append("Default.pfa");
					
					//CAlert::InformationAlert("achis achis");
					if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
					{   CAlert::InformationAlert("checar fallo 3");
						CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
						break;
					}
					//CAlert::InformationAlert("checar fallo 4");
					this->handleSetOptions();
				}
			break;
		}

		case kA2PImpPanelUpdateDialogActionID:///click sobre menu Update
		{
			//CAlert::InformationAlert("checar fallo en Update");
			
			InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
				(
					kA2PPreferenciasFuncionesBoss,	// Object boss/class
					IA2PPreferenciasFunctions::kDefaultIID
				)));
				if(A2PPrefeFuntions==nil)
					break;
				
				if(A2PPrefeFuntions->VerificaSeguridadPirateria())
				{
					InterfacePtr<IA2PActualizarFunction> A2PActualizar(static_cast<IA2PActualizarFunction*> (CreateObject
					(
							kA2PActualizarFunctionBoss,	// Object boss/class
						IA2PActualizarFunction::kDefaultIID
					)));
					
					PMString DefaulPrefer=PnlTrvUtils::CrearFolderPreferencias();
					DefaulPrefer.Append("Default.pfa");
					if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
					{
						CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
						break;
					}
				A2PActualizar->Actualizar();
				}
			
			
			break;
		}
		case kA2PImpPanelPreferenciasActionID:///click sobre menu Update
		{	InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
				(	kA2PPreferenciasFuncionesBoss,	// Object boss/class
					IA2PPreferenciasFunctions::kDefaultIID
				)));
				if(A2PPrefeFuntions==nil)
					break;
				
				if(A2PPrefeFuntions->VerificaSeguridadPirateria())
				{
					A2PPrefeFuntions->OpenDialogPassword();
				}
			
			break;
		}
		
		case kA2PImpNewPageFromPlantilleroActionID:
		{
			/*InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
				(
					kA2PPreferenciasFuncionesBoss,	// Object boss/class
					IA2PPreferenciasFunctions::kDefaultIID
				)));
				if(A2PPrefeFuntions==nil)
					break;
				
				PMString DefaulPrefer=PnlTrvUtils::CrearFolderPreferencias();
					DefaulPrefer.Append("Default.pfa");
					if(A2PPrefeFuntions->leerDocDePreferencia(DefaulPrefer.GrabCString(),Prefer)==kFalse)
					{
						CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
						break;
					}
					
				
			InterfacePtr<IPLTROImport> PlantilleroInterface(static_cast<IPLTROImport*> (CreateObject
			(
				kPlantilleroImpImportacionBoss,	// Object boss/class
				IPLTROImport::kDefaultIID
			)));
			if(PlantilleroInterface==nil)
			{
				//Para cuando no se desea abrir las plantillas
				break;
			}
			
			PlantilleroInterface->OpenDialogToNewPage( Prefer.NombreEditorial);*/
			
		}
		
		default:
		{
			break;
		}
	}
}

/* handleAbout
*/
void A2PImpActionComponent::handleAbout()
{
	SDKUtilities::InvokePlugInAboutBox(kA2PImpAboutBoxStringKey);
}

/* handleRefresh
*/
void A2PImpActionComponent::handleRefresh()
{
	do
	{
		IControlView* treeWidget = PnlTrvUtils::GetWidgetOnPanel(
			kA2PImpPanelWidgetID, kA2PImpTreeViewWidgetID);
		ASSERT(treeWidget);
		if(!treeWidget)
		{
			break;
		}
		InterfacePtr<IBoolData> 
			refreshStatus(GetExecutionContextSession(), IID_IPnlTrvRefreshStatus);
		ASSERT(refreshStatus);
		if(refreshStatus)
		{
			refreshStatus->Set(kTrue);
			// The model code will clear this flag
			// This flag gives us a way to communicate that the tree model
			// should be updated even if we haven't changed the root
			InterfacePtr<ITreeViewMgr> 
				iTreeViewMgr(treeWidget, UseDefaultIID());
			ASSERT(iTreeViewMgr);
			if(!iTreeViewMgr)
			{
				break;
			}
			iTreeViewMgr->ClearTree();
			
			iTreeViewMgr->ChangeRoot(kTrue);
			
		}
	} while(kFalse);
}





/*
	busqueda y obtencion de la ruta del archivo a importar
	abre un dialogo para seleccionar archivo.
*/
PMString A2PImpActionComponent::getSelectedFolderFromDialog(const PMString* pmTitlePtr)
{

////////////////////////////////////////////////////
	///tipos de archivos a mostrar
	SysOSType kSysOSType_CSVFileType = 'TEXT';
PMString  kPMString_CSVFileFamily("Texto");
PMString kPMString_CSVFileExtension("txt");

SysOSType kSysOSType_CSVFileTipo = 'EXPO';
PMString  kPMString_CSVFileFamily1("EXPORT");
PMString kPMString_CSVFileExtension1("export");


////////////////////////////////////////////////////
	PMString retval="";//nombre del archivo
	IDFile file;		//archivo
	SysFileList filesToOpen;	//lista de archivos que se muestran
	bool16 success;				//tipo suceso
	InterfacePtr<IOpenFileDialog> iOpenDlg
	(
		static_cast<IOpenFileDialog*>
		(
			CreateObject
			(
				kOpenFileDialogBoss,
				IOpenFileDialog::kDefaultIID
			)
		)
	);//obtencion de la interfaz para IOpenFileDialog

	if (iOpenDlg != nil)//si no se obtuvo la interfaz
	{
		iOpenDlg->AddType(kSysOSType_CSVFileType);	//adiciona el tipo que se desea
		iOpenDlg->AddExtension(&kPMString_CSVFileFamily, &kPMString_CSVFileExtension);

		iOpenDlg->AddType(kSysOSType_CSVFileTipo);	//adiciona el tipo que se desea
		iOpenDlg->AddExtension(&kPMString_CSVFileFamily1, &kPMString_CSVFileExtension1);
	
		if (iOpenDlg->DoDialog(nil, filesToOpen, kFalse) == kTrue)
		{
			file = *filesToOpen.GetNthFile(0);//obtine archivo selecionado
			success = kTrue;//suceso fue vberdadero e indica que se ha seleccionado un archivo
		}
	}


	if(success==kTrue)
	{
		retval=SDKUtilities::SysFileToPMString(&file);//conversion a PMString y obtiene el nombre del archivo		
		//CAlert::InformationAlert(retval);
	}

	 return retval;
}



/* handleSetOptions
*/
void A2PImpActionComponent::handleSetOptions()
{
	do
	{
		const int32 cCountOfOptions = 1;
		K2Vector<PMString> optionsVec(cCountOfOptions);
		PMString assetPathTitle(kA2PImpSetOption1StringKey);
		assetPathTitle.Translate();
		assetPathTitle.SetTranslatable(kFalse);
	
		PMString folder = this->getSelectedFolderFromDialog(&assetPathTitle);
		//CAlert::InformationAlert("0");
		if(folder.IsEmpty() == kTrue)
		{
			break;
		}
		//CAlert::InformationAlert("02");
		
		//CAlert::InformationAlert("01");
		optionsVec.push_back(folder);
		if(folder.IsEmpty() == kFalse)
		{
			//CAlert::InformationAlert("1");
			this->processSetOptionsCommand(optionsVec);
			
			//CAlert::InformationAlert("2");
			InterfacePtr<IA2PAvisos> A2PAVISOS(static_cast<IA2PAvisos*> (CreateObject
			(
				kA2PAvisosBoss,	// Object boss/class
				IA2PAvisos::kDefaultIID
			)));
				
			//CAlert::InformationAlert("3");
			if(A2PAVISOS!=nil)
				A2PAVISOS->CargarAvisos(folder,kTrue);
				
			//CAlert::InformationAlert("4");	
			this->handleRefresh();
			//CAlert::InformationAlert("5");
		}
		
		this->DoPanel();//abre panel
	} while(kFalse);	

}

/* processSetOptionsCommand
*/
ErrorCode A2PImpActionComponent::processSetOptionsCommand(
	const K2Vector<PMString>& options)
{
	ErrorCode retval = kFailure;
	do
	{
		InterfacePtr<IWorkspace>
			iSessionWorkspace(GetExecutionContextSession()-> QueryWorkspace());
		ASSERT(iSessionWorkspace);
		if(iSessionWorkspace == nil)
		{
			break;
		}

		InterfacePtr<IPnlTrvOptions>
			iOptions(iSessionWorkspace, UseDefaultIID());
		ASSERT(iOptions);
		if(!iOptions)
		{
			break;
		}
		InterfacePtr<ICommand>
			modOptionsCmd (CmdUtils::CreateCommand(kA2PImpPnlTrvChangeOptionsCmdBoss));
		ASSERT(modOptionsCmd);
		if(!modOptionsCmd)
		{
			break;
		}	
		modOptionsCmd->SetItemList(UIDList(iSessionWorkspace));

		InterfacePtr<IPnlTrvChangeOptionsCmdData>
			cmdData (modOptionsCmd, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil)
		{
			break;
		}

		K2Vector<PMString>::const_iterator iter;
		for(iter = options.begin(); iter != options.end(); ++iter)
		{
			cmdData->AddOption(*iter);	// template path
		}
		retval = CmdUtils::ProcessCommand(modOptionsCmd);
		ASSERT(retval == kSuccess);	
	
	} while(kFalse);

	return retval;
}

/**
	funcion que busca y habre el panel importar
*/
void A2PImpActionComponent::DoPanel()
{
	bool16 retval=kFalse;
	do {

		IDataBase* db = ::GetDataBase(this);
		ASSERT(db);
		if(!db) {
			break;
		}
	
		InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
		ASSERT(app);
		if(!app) {
			break;
		}
		
		/*InterfacePtr<IPaletteMgr> palMgr(app->QueryPaletteManager());
		ASSERT(palMgr);
		if(!palMgr) {
			break;
		}
		*/
		InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
		ASSERT(panelMgr);
		if(!panelMgr) {
			break;
		}

		//PanelMgrEntryList* ListaPaneles=panelMgr->GetPanelList();
		panelMgr->ShowPanelByWidgetID(kA2PImpPanelWidgetID);
	}while(false);

}




//  Code generated by DollyXS code generator
