//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvDataModel.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interface includes

// Project includes
#include "PnlTrvDataModel.h"
#include "PnlTrvUtils.h"
#include "PnlTrvDataNode.h"
#include "IPanelControlData.h"
#include "IPalettePanelUtils.h"
#include "ITextControlData.h"



#include "A2PImpID.h"
#include "A2PImpStructPreferencias.h"
#include "A2PAvisosHeader.h"

#include "A2PImpStructAvisos.h"


//#include "InterlasaRegEditUtilities.h"

// Other API includes
#include "SDKFileHelper.h"
#include "CAlert.h"

#ifdef MACINTOSH
	#include "IA2PPreferenciasFunciones.h"
	#include "PlatformFileSystemIterator.h"
	#include "InterlasaUtilities.h"
	#include "A2PPrefID.h"
#endif


#ifdef WINDOWS
	#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
	#include "..\Interlasa_common\PlatformFileSystemIterator.h"
	#include "..\Interlasa_common\InterlasaUtilities.h"
	#include "..\A2P_PREFERENCIAS\A2PPrefID.h"
#endif



/**
	@ingroup paneltreeview
*/

/* Constructor
*/
PnlTrvDataModel::PnlTrvDataModel() : 
	fRootNode(nil), 
	fFilter("\\*.*"),
	fRecursionDepthLimiter(0),
	fTreeHashValue(0)
{
}


/* Constructor
*/
PnlTrvDataModel::PnlTrvDataModel(PMString filter) : 
	fRootNode(nil),
	fFilter(filter),
	fRecursionDepthLimiter(0),
	fTreeHashValue(0)
{
}

	
/* Destructor
*/
PnlTrvDataModel::~PnlTrvDataModel()
{
	deleteTree();
}


/* deleteTree
*/
void PnlTrvDataModel::deleteTree() 
{
	// We've flattened the tree in our hashtable anyway so 
	// use this to delete the nodes.
	std::map<PMString, PnlTrvDataNode* >::iterator iter;
	for(iter = fPathNodeMap.begin(); iter != fPathNodeMap.end(); iter++)
	{
		PnlTrvDataNode* node = iter->second;
		ASSERT(node);
		delete node;
	}
	fPathNodeMap.clear();
	fRootNode = nil;
}


/* SetRootPath
*/
void PnlTrvDataModel::SetRootPath(const PMString& path) 
{
	if(fRootNode != nil)
	{
		deleteTree();
	}
	if(path.IsEmpty() == kFalse)
	{
		//CAlert::InformationAlert("S1");
		fRootNode =  new PnlTrvDataNode();
		//CAlert::InformationAlert("S2");
		fRootNode->SetData(path);
		//CAlert::InformationAlert("S3");
		fPathNodeMap.insert( std::pair<PMString, PnlTrvDataNode* >(path, fRootNode));
		//CAlert::InformationAlert("S4");
		fRecursionDepthLimiter = 0;
		//CAlert::InformationAlert("S5");
		this->calcDescendentsRecursively(fRootNode);
		//CAlert::InformationAlert("S6");
	}
}


/* ForciblyUpdate
*/
void PnlTrvDataModel::ForciblyUpdate()
{
	do {
		if(fRootNode == nil)
		{
			break;
		}

		PMString rootPath = fRootNode->GetData();
		if(rootPath.IsEmpty() == kTrue)
		{
			break;
		}
		// Force the model to rebuild itself explicitly
		// A smarter implementation would verify that the profile of the
		// files being watched had changed e.g. through calculating the sum of
		// hash values for the paths before and comparing with the present
		this->SetRootPath(rootPath);
		
	} while(kFalse);
}


/* GetParentPath
*/
PMString  PnlTrvDataModel::GetParentPath(const PMString& path) const
{
	std::map<PMString, PnlTrvDataNode* >::const_iterator result = fPathNodeMap.find(path);
	if(result != fPathNodeMap.end())
	{
		PnlTrvDataNode* node = result->second;
		ASSERT(node);
		ASSERT(fRootNode);
		// Use the comparison we've defined on the node class
		if((*node) == (*fRootNode))
		{
			return PMString();
		}
		return node->GetParent()->GetData();
	}
	return PMString();
}


/* GetChildCount
*/
int32 PnlTrvDataModel::GetChildCount(const PMString& path) const
{
	std::map<PMString, PnlTrvDataNode* >::const_iterator result = fPathNodeMap.find(path);
	if(result != fPathNodeMap.end()) {
		PnlTrvDataNode* node = result->second;
		return node->ChildCount();
	}
	return 0;
}

/* GetNthChildPath
*/
PMString PnlTrvDataModel::GetNthChildPath(
	const PMString& path, int32 nth) const
{
	std::map<PMString, PnlTrvDataNode* >::const_iterator result = fPathNodeMap.find(path);
	if(result != fPathNodeMap.end())
	{
		PnlTrvDataNode* node = result->second;
		return node->GetNthChild(nth).GetData();
	}
	return PMString();
}

/* GetChildIndexFor
*/
int32  PnlTrvDataModel::GetChildIndexFor(
	const PMString& par, const PMString& kid) const
{
	int32 retval=(-1);
	std::map<PMString, PnlTrvDataNode* >::const_iterator result = fPathNodeMap.find(par);
	if(result != fPathNodeMap.end())
	{
		PnlTrvDataNode* node = result->second;

		for(int32 i=0; i < node->ChildCount(); i++)
		{
			PMString kidPath = node->GetNthChild(i).GetData();
			//CAlert::InformationAlert(kidPath +"=" + kid);
			if(kid == kidPath)
			{
				retval=i;
				break;
			}

		}
	}
	return retval;
}


/* GetRootPath
*/
PMString PnlTrvDataModel::GetRootPath() const
{
	if(fRootNode != nil)
	{
		return fRootNode->GetData();
	}
	return PMString();
}


/* calcDescendentsRecursively
*/
void PnlTrvDataModel::calcDescendentsRecursively(PnlTrvDataNode* parentNode) 
{

	//variable utilizadas en el escaneo u obtencion de los avisos
	//FILE *stream;	//para el 
	PMString DireccionPMString=parentNode->GetData();
	//obtencion de la direccion o path del archivo a importar contenido en el padre nodo
	
	
	//Colocar nombre de Archivo Fluido sobre la paleta Requerimiento para  A2P3.0 para CS2
	InterfacePtr<IPanelControlData>	panel(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(kA2PImpPanelWidgetID)) ;
	if(panel!=nil)
	{
		PMString StringToShow(kA2PImpNameDocumentImportedStringKey);
		StringToShow.Translate();
		StringToShow.SetTranslatable(kTrue);
		
		
		//StringToShow.Append();
		IControlView *iControlView = panel->FindWidget(kA2PImpFileImportedNameWidgetID);
		InterfacePtr<ITextControlData> TextControl(iControlView,ITextControlData::kDefaultIID);
		if (TextControl!= nil)
		{
			TextControl->SetString(StringToShow + PnlTrvUtils::TruncatePath(DireccionPMString));
		}
	}
	//
	
	int NumAvisos=1;//
	//int Linea;			//linea en que se encuentra la lectura cada linea leida es un aviso.
	int NumCaracter=0;	//numero de caracter leido
	//int ANSCCICaranter;	//codigo anscci del caracter leido
	//int NumCampoAGuardar;	
	PlatformChar Caracter;		//caracter leido
	int PosTab=0;		//posicion del primer tabulador en la linea o aviso leido
	//int numcampoLeido;	//num campo leido
	int salir=0;		//bandera para indicar si se esta leyendo el ultimo campo en la cadena aviso
	PMString CadenaAviso;	//Cadena que contien el aviso completo(todos los campos)
	
	PMString CadenaCampo;	//Cadena que contien el campo encontrado
	
	///// variables utilizadas en el arbol
	PMString NomNodoPadre=parentNode->GetData();
	PMString NomAviso;
	PMString NombredeSeccion="";
	PMString NombredeSeccion2="";
	PMString PathAviso="";
	int32 i,j;
	int32 NumerodelCampoSeccion=0;
	int32 NumerodelCampoAviso=0;
	int32 NumerodelRuta=0;

	do
	{
		//DireccionPMString= InterlasaUtilities::MacToUnix(DireccionPMString);
		
		//CAlert::InformationAlert("A");
		//OBTIENE PEREFERNCIAS
		InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
			(
				kA2PPreferenciasFuncionesBoss,	// Object boss/class
				IA2PPreferenciasFunctions::kDefaultIID
			)));
		
		if(A2PPrefeFuntions==nil)
		{
			CAlert::InformationAlert("Salio por eeror=ERRORSOTE de A2PPrefeFuntions");
			break;
		}
		//CAlert::InformationAlert("B");
		
		if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Pref,kFalse)==kFalse)
		{
			CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
			break;
		}
		
		//CAlert::InformationAlert("C");
		InterfacePtr<IA2PAvisos> A2PAVISOS(static_cast<IA2PAvisos*> (CreateObject
		(
			kA2PAvisosBoss,	// Object boss/class
			IA2PAvisos::kDefaultIID
		)));
		
		
		
		if(A2PAVISOS==nil)
		{
			CAlert::InformationAlert("Salio por eeror de conf");
			break;
		}
		
		PMString PathDAvisos=A2PAVISOS->ArchivoDAvisos();
		
		
		//CAlert::InformationAlert("E");
		
		if(PathDAvisos.NumUTF16TextChars()<=0)
		{		
			//CAlert::InformationAlert("No tiene avisos1");
			if(DireccionPMString.NumUTF16TextChars()<=0)
			{
				break;
			}
			else
			{
				//CAlert::InformationAlert("F");
				if(!A2PAVISOS->CargarAvisos(DireccionPMString,kFalse))
				{
					
					break;
				}
				else
				{
					
					NumAvisos=A2PAVISOS->CantidadDAvisos();
					
				}
			}
		}
		else
		{
			//CAlert::InformationAlert("H");
			NumAvisos=A2PAVISOS->CantidadDAvisos();
			if(NumAvisos<=0)
			{
				//CAlert::InformationAlert("No tiene avisos");
				if(DireccionPMString.NumUTF16TextChars()<=0)
				{
					break;
				}
				else
				{
					if(!A2PAVISOS->CargarAvisos(DireccionPMString,kFalse))
					{
						
						break;
					}
					else
						NumAvisos=A2PAVISOS->CantidadDAvisos();
				}
			}
		}
		
		//CAlert::InformationAlert("J");
		/*PMString CantidadAviso="";
		CantidadAviso.AppendNumber(NumAvisos);
		CAlert::InformationAlert("Avisos tamaño estructura: "+CantidadAviso);
	*/	
		struct StructAvisos *Avisos=new StructAvisos[NumAvisos];
		
		A2PAVISOS->CopiasAvisosCargados(Avisos);
		
		
		//CAlert::InformationAlert("K");
		
	
		
	//es la siguiente parte se obtiene el numero de campo en que se encuentra 
	//el campo con el Nombre Pagina o Seccion y se almacena en NumerodelCapoSeccion
	do
	{
		if(Avisos[0].NomCampo1=="Ruta" || Avisos[0].NomCampo1=="Path")
		{
			NumerodelRuta=1;
			break;
		}

		if(Avisos[0].NomCampo2=="Ruta" || Avisos[0].NomCampo2=="Path")
		{
			NumerodelRuta=2;
			break;
		}

		if(Avisos[0].NomCampo3=="Ruta" || Avisos[0].NomCampo3=="Path")
		{
			NumerodelRuta=3;
			break;
		}
		if(Avisos[0].NomCampo4=="Ruta" || Avisos[0].NomCampo4=="Path")
		{
			NumerodelRuta=4;
			break;
		}

		if(Avisos[0].NomCampo5=="Ruta" || Avisos[0].NomCampo5=="Path")
		{
			NumerodelRuta=5;
			break;
		}
		if(Avisos[0].NomCampo6=="Ruta" || Avisos[0].NomCampo6=="Path")
		{
			NumerodelRuta=6;
			break;
		}
		if(Avisos[0].NomCampo7=="Ruta" || Avisos[0].NomCampo7=="Path")
		{
			NumerodelRuta=7;
			break;
		}
		if(Avisos[0].NomCampo8=="Ruta" || Avisos[0].NomCampo8=="Path")
		{
			NumerodelRuta=8;
			break;
		}
		if(Avisos[0].NomCampo9=="Ruta" || Avisos[0].NomCampo9=="Path")
		{
			NumerodelRuta=9;
			break;
		}
		if(Avisos[0].NomCampo10=="Ruta" || Avisos[0].NomCampo10=="Path")
		{
			NumerodelRuta=10;
			break;
		}
		if(Avisos[0].NomCampo11=="Ruta" || Avisos[0].NomCampo11=="Path")
		{
			NumerodelRuta=11;
			break;
		}
		if(Avisos[0].NomCampo12=="Ruta" || Avisos[0].NomCampo12=="Path")
		{
			NumerodelRuta=12;
			break;
		}
		if(Avisos[0].NomCampo13=="Ruta" || Avisos[0].NomCampo13=="Path")
		{
			NumerodelRuta=13;
			break;
		}
		if(Avisos[0].NomCampo14=="Ruta" || Avisos[0].NomCampo14=="Path")
		{
			NumerodelRuta=14;
			break;
		}
	}while(false);
	
	//CAlert::InformationAlert("L");
/*	PMString A="";
	Avisos[0].NomCampo4.StripWhiteSpace(PMString::kAllWhiteSpace);
	A.Append(Avisos[0].NomCampo4);
	A.Append(",");
	A.AppendNumber(Avisos[0].NomCampo4.NumUTF16TextChars());
*/
	
	do
	{
		if(Avisos[0].NomCampo1=="Pagina o seccion" || Avisos[0].NomCampo1=="Page or section")
		{
			NumerodelCampoSeccion=1;
			break;
		}

		if(Avisos[0].NomCampo2=="Pagina o seccion" || Avisos[0].NomCampo2=="Page or section")
		{
			NumerodelCampoSeccion=2;
			break;
		}

		if(Avisos[0].NomCampo3=="Pagina o seccion" || Avisos[0].NomCampo3=="Page or section")
		{
			NumerodelCampoSeccion=3;
			break;
		}
		if(Avisos[0].NomCampo4=="Pagina o seccion" || Avisos[0].NomCampo4=="Page or section")
		{
			//CAlert::InformationAlert("Se encontro");
			NumerodelCampoSeccion=4;
			//CAlert::InformationAlert("valor de 4");
			break;
		}

		if(Avisos[0].NomCampo5=="Pagina o seccion" || Avisos[0].NomCampo5=="Page or section")
		{
			NumerodelCampoSeccion=5;
			break;
		}
		if(Avisos[0].NomCampo6=="Pagina o seccion" || Avisos[0].NomCampo6=="Page or section")
		{
			NumerodelCampoSeccion=6;
			break;
		}
		if(Avisos[0].NomCampo7=="Pagina o seccion" || Avisos[0].NomCampo7=="Page or section")
		{
			NumerodelCampoSeccion=7;
			break;
		}
		if(Avisos[0].NomCampo8=="Pagina o seccion" || Avisos[0].NomCampo8=="Page or section")
		{
			NumerodelCampoSeccion=8;
			break;
		}
		if(Avisos[0].NomCampo9=="Pagina o seccion" || Avisos[0].NomCampo9=="Page or section")
		{
			NumerodelCampoSeccion=9;
			break;
		}
		if(Avisos[0].NomCampo10=="Pagina o seccion" || Avisos[0].NomCampo10=="Page or section")
		{
			NumerodelCampoSeccion=10;
			break;
		}
		if(Avisos[0].NomCampo11=="Pagina o seccion" || Avisos[0].NomCampo11=="Page or section")
		{
			NumerodelCampoSeccion=11;
			break;
		}
		if(Avisos[0].NomCampo12=="Pagina o seccion" || Avisos[0].NomCampo12=="Page or section")
		{
			NumerodelCampoSeccion=12;
			break;
		}
		if(Avisos[0].NomCampo13=="Pagina o seccion" || Avisos[0].NomCampo13=="Page or section")
		{
			NumerodelCampoSeccion=13;
			break;
		}
		if(Avisos[0].NomCampo14=="Pagina o seccion" || Avisos[0].NomCampo14=="Page or section")
		{
			NumerodelCampoSeccion=14;
			break;
		}
	}while(false);
	
	//CAlert::InformationAlert("M");
	//En la siguiente parte se obtiene el numero de campo o la variable ne donde se encuentra
	//el campo con el nombre de kA2PImpCampoNombreStringKey
	do
	{
		if(Avisos[0].NomCampo1=="Nombre" || Avisos[0].NomCampo1=="Name")
		{
			NumerodelCampoAviso=1;
			break;
		}

		if(Avisos[0].NomCampo2=="Nombre" || Avisos[0].NomCampo2=="Name")
		{
			NumerodelCampoAviso=2;
			break;
		}

		if(Avisos[0].NomCampo3=="Nombre" || Avisos[0].NomCampo3=="Name")
		{
			NumerodelCampoAviso=3;
			break;
		}

		if(Avisos[0].NomCampo4=="Nombre" || Avisos[0].NomCampo4=="Name")
		{
			NumerodelCampoAviso=4;
			break;
		}

		if(Avisos[0].NomCampo5=="Nombre" || Avisos[0].NomCampo5=="Name")
		{
			NumerodelCampoAviso=5;
			break;
		}

		if(Avisos[0].NomCampo6=="Nombre" || Avisos[0].NomCampo6=="Name")
		{
			NumerodelCampoAviso=6;
			break;
		}

		if(Avisos[0].NomCampo7=="Nombre" || Avisos[0].NomCampo7=="Name")
		{
			NumerodelCampoAviso=7;
			break;
		}

		if(Avisos[0].NomCampo8=="Nombre" || Avisos[0].NomCampo8=="Name")
		{
			NumerodelCampoAviso=8;
			break;
		}

		if(Avisos[0].NomCampo9=="Nombre" || Avisos[0].NomCampo9=="Name")
		{
			NumerodelCampoAviso=9;
			break;
		}

		if(Avisos[0].NomCampo10=="Nombre" || Avisos[0].NomCampo10=="Name")
		{
			NumerodelCampoAviso=10;
			break;
		}

		if(Avisos[0].NomCampo11=="Nombre" || Avisos[0].NomCampo11=="Name")
		{
			NumerodelCampoAviso=11;
			break;
		}

		if(Avisos[0].NomCampo12=="Nombre" || Avisos[0].NomCampo12=="Name")
		{
			NumerodelCampoAviso=12;
			break;
		}

		if(Avisos[0].NomCampo13=="Nombre" || Avisos[0].NomCampo13=="Name")
		{
			NumerodelCampoAviso=13;
			break;
		}
		if(Avisos[0].NomCampo14=="Nombre" || Avisos[0].NomCampo14=="Name")
		{
			NumerodelCampoAviso=14;
			break;
		}
	}while(false);
	///CAlert::InformationAlert("N");
	//////////////////////////////////////////////////////////////////
	//ciclo para buscar y obtener las secciones y los avisos de cada//
	//seccion.														//
	//////////////////////////////////////////////////////////////////
/*	PMString A="";
	A.Append("Nemero del campo de la seccion:");
	A.AppendNumber(NumerodelCampoSeccion);
	CAlert::InformationAlert(A);*/
	for(i=0;i<NumAvisos;i++)
	{	
		//se obtien el nombre de la seccion del aviso i dependiendo en el numero de campo que se encuentre
		switch(NumerodelCampoSeccion)
		{
		case 1:
			NombredeSeccion=Avisos[i].ContenidoCampo1;
			break;
		case 2:
			NombredeSeccion=Avisos[i].ContenidoCampo2;
			break;
		case 3:
			NombredeSeccion=Avisos[i].ContenidoCampo3;
			break;
		case 4:
			NombredeSeccion=Avisos[i].ContenidoCampo4;
			break;
		case 5:
			NombredeSeccion=Avisos[i].ContenidoCampo5;
			break;
		case 6:
			NombredeSeccion=Avisos[i].ContenidoCampo6;
			break;
		case 7:
			NombredeSeccion=Avisos[i].ContenidoCampo7;
			break;
		case 8:
			NombredeSeccion=Avisos[i].ContenidoCampo8;
			break;
		case 9:
			NombredeSeccion=Avisos[i].ContenidoCampo9;
			break;
		case 10:
			NombredeSeccion=Avisos[i].ContenidoCampo10;
			break;
		case 11:
			NombredeSeccion=Avisos[i].ContenidoCampo11;
			break;
		case 12:
			NombredeSeccion=Avisos[i].ContenidoCampo12;
			break;
		case 13:
			NombredeSeccion=Avisos[i].ContenidoCampo13;
			break;
		case 14:
			NombredeSeccion=Avisos[i].ContenidoCampo14;
			break;
		}
		
		
		
		//CAlert::InformationAlert("NombredeSeccion");
/*		PMString  nuemrosecc="";
		nuemrosecc.AppendNumber(NumerodelCampoSeccion);
		CAlert::InformationAlert(nuemrosecc + " NombredeSeccion:"+NombredeSeccion);
*/		//que sea diferente a una cadena nula o que no contenga un caracter escribible
		//NombredeSeccion.StripWhiteSpace(PMString::kTrailingWhiteSpace );
		//CAlert::InformationAlert(NombredeSeccion);

		if((!NombredeSeccion.Contains("0") || NombredeSeccion.GetAsNumber()>0 
			|| NombredeSeccion.IndexOfString("0")!=0) && NombredeSeccion.NumUTF16TextChars()>0 && NombredeSeccion!=" ")//mientras existe o tenga una seccion y sea diferente de 0
		{
//			CAlert::InformationAlert("ok");
			////buscar que no se repita el nombre de la seccion o pagina
			if(BuscaNodo(parentNode,NombredeSeccion)==kFalse)///no existe otro nodo con este mismo nombre de seccion
			{
				//CAlert::InformationAlert("no existe otro nodo con el mismo nombre de seccion");
				//se crea un nuevo nodo para la seccion
				//CAlert::InformationAlert("se crea un nuevo nodo para la seccion");
				PnlTrvDataNode* NodoSeccion = new PnlTrvDataNode();
				//se adiciona el nombre de la seccion al nodo
				//CAlert::InformationAlert("se adiciona el nombre de la seccion al nodo");
				//CAlert::InformationAlert(NombredeSeccion);
				NodoSeccion->SetData(NombredeSeccion);
				//el nuevo nodo(nodo seccion) lo pone como hijo del node padre o rootnod
				//CAlert::InformationAlert("el nuevo nodo(nodo seccion) lo pone como hijo del node padre o rootnod");
				NodoSeccion->SetParent(parentNode);
				//CAlert::InformationAlert("//adiciona el nuevo nodo(nodo seccion) como hijo del nodo padre(rootnode)");
				//adiciona el nuevo nodo(nodo seccion) como hijo del nodo padre(rootnode)
				parentNode->AddChild(static_cast<const PnlTrvDataNode& >(*NodoSeccion));
				//CAlert::InformationAlert("//se inserta el path del nodo hijo adicionando el nombre de la seccion");
				//se inserta el path del nodo hijo adicionando el nombre de la seccion
				fPathNodeMap.insert( std::pair<PMString, PnlTrvDataNode* >(NombredeSeccion, NodoSeccion));
				////////////////////////////////////////////////////////////
				//Busqueda de los avisos que se van dentro de esta seccion//
				//														  //
				////////////////////////////////////////////////////////////
				//CAlert::InformationAlert("Busqueda de los avisos que se van dentro de esta seccion");
				int32 contAvisoDeSeccion=0;
				for(j=0;j<NumAvisos;j++)
				{
					//se obtitne el nombre de seccion y nombre de aviso
					//del aviso j
					switch(NumerodelCampoSeccion)
					{
					case 1:
						NombredeSeccion2=Avisos[j].ContenidoCampo1;
						break;
					case 2:
						NombredeSeccion2=Avisos[j].ContenidoCampo2;
						break;
					case 3:
						NombredeSeccion2=Avisos[j].ContenidoCampo3;
						break;
					case 4:
						NombredeSeccion2=Avisos[j].ContenidoCampo4;
						break;
					case 5:
						NombredeSeccion2=Avisos[j].ContenidoCampo5;
						break;
					case 6:
						NombredeSeccion2=Avisos[j].ContenidoCampo6;
						break;
					case 7:
						NombredeSeccion2=Avisos[j].ContenidoCampo7;
						break;
					case 8:
						NombredeSeccion2=Avisos[j].ContenidoCampo8;
						break;
					case 9:
						NombredeSeccion2=Avisos[j].ContenidoCampo9;
						break;
					case 10:
						NombredeSeccion2=Avisos[j].ContenidoCampo10;
						break;
					case 11:
						NombredeSeccion2=Avisos[j].ContenidoCampo11;
						break;
					case 12:
						NombredeSeccion2=Avisos[j].ContenidoCampo12;
						break;
					case 13:
						NombredeSeccion2=Avisos[j].ContenidoCampo13;
						break;
					case 14:
						NombredeSeccion2=Avisos[j].ContenidoCampo14;
						break;
					}

					switch(NumerodelCampoAviso)
					{
					case 1:
						NomAviso=Avisos[j].ContenidoCampo1;
						break;
					case 2:
						NomAviso=Avisos[j].ContenidoCampo2;
						break;
					case 3:
						NomAviso=Avisos[j].ContenidoCampo3;
						break;
					case 4:
						NomAviso=Avisos[j].ContenidoCampo4;
						break;
					case 5:
						NomAviso=Avisos[j].ContenidoCampo5;
						break;
					case 6:
						NomAviso=Avisos[j].ContenidoCampo6;
						break;
					case 7:
						NomAviso=Avisos[j].ContenidoCampo7;
						break;
					case 8:
						NomAviso=Avisos[j].ContenidoCampo8;
						break;
					case 9:
						NomAviso=Avisos[j].ContenidoCampo9;
						break;
					case 10:
						NomAviso=Avisos[j].ContenidoCampo10;
						break;
					case 11:
						NomAviso=Avisos[j].ContenidoCampo11;
						break;
					case 12:
						NomAviso=Avisos[j].ContenidoCampo12;
						break;
					case 13:
						NomAviso=Avisos[j].ContenidoCampo13;
						break;
					case 14:
						NomAviso=Avisos[j].ContenidoCampo14;
						break;
					}
					
					switch(NumerodelRuta)
					{
					case 1:
						PathAviso=Avisos[j].ContenidoCampo1;
						break;
					case 2:
						PathAviso=Avisos[j].ContenidoCampo2;
						break;
					case 3:
						PathAviso=Avisos[j].ContenidoCampo3;
						break;
					case 4:
						PathAviso=Avisos[j].ContenidoCampo4;
						break;
					case 5:
						PathAviso=Avisos[j].ContenidoCampo5;
						break;
					case 6:
						PathAviso=Avisos[j].ContenidoCampo6;
						break;
					case 7:
						PathAviso=Avisos[j].ContenidoCampo7;
						break;
					case 8:
						PathAviso=Avisos[j].ContenidoCampo8;
						break;
					case 9:
						PathAviso=Avisos[j].ContenidoCampo9;
						break;
					case 10:
						PathAviso=Avisos[j].ContenidoCampo10;
						break;
					case 11:
						PathAviso=Avisos[j].ContenidoCampo11;
						break;
					case 12:
						PathAviso=Avisos[j].ContenidoCampo12;
						break;
					case 13:
						PathAviso=Avisos[j].ContenidoCampo13;
						break;
					case 14:
						PathAviso=Avisos[j].ContenidoCampo14;
						break;
					}
					
					#ifdef MACINTOSH
						if(PathAviso.Contains("\\"))
						SDKUtilities::convertToMacPath(PathAviso);
					#endif
					
					PathAviso=PnlTrvUtils::TruncatePath(PathAviso);
					
					///se compara el nombre de seccion de la seccion actual(Aviso[i])
					//con el nombre del (Aviso j)
					if(NombredeSeccion==NombredeSeccion2)
					{
						PMString NombreYNumeroAviso="";
						NombreYNumeroAviso.Append(NomAviso);
						NombreYNumeroAviso.Append(", ");
						NombreYNumeroAviso.Append(PathAviso);
						if(BuscaNodo(NodoSeccion,NombreYNumeroAviso)==kFalse)///no existe otro nodo con este mismo nombre de seccion
						{
							contAvisoDeSeccion++;
							//Adiciona al nodo Seccion el nodo aviso
							PnlTrvDataNode* NodoAviso = new PnlTrvDataNode();
							//pone el nombre del nuevo nodo con el nombre del aviso
							NodoAviso->SetData(NombreYNumeroAviso);
							//pone como padre del nodo aviso al nodo seccion
							NodoAviso->SetParent(NodoSeccion);
							//se adiciona al nodo seccion el nodo aviso
							NodoSeccion->AddChild(static_cast<const PnlTrvDataNode& >(*NodoAviso));
							//pone el nombre del nodo aviso
							fPathNodeMap.insert( std::pair<PMString, PnlTrvDataNode* >(NombreYNumeroAviso, NodoAviso));
						}
					}
				}
			}
		}	
	}
	//CAlert::InformationAlert("Y");
	delete  Avisos;//borra la estructura aviso
	//CAlert::InformationAlert("Z");


	}while(kFalse);
//	CAlert::InformationAlert("Arbol Llenado");
/*	if(fRecursionDepthLimiter > ePnlTrvMaxRecursionDepth)
	{
		ASSERT(kFalse);
		// Check to avoid overflowing stack if things go wrong
		return;
	}

	do
	{
		ASSERT(parentNode);
		if(!parentNode)
		{
			break;
		}
		PMString rootPath = parentNode->GetData();
		if(rootPath.IsEmpty() == kTrue)
		{
			break;
		}

		SDKFileHelper rootFileHelper(rootPath);
		IDFile rootSysFile = rootFileHelper.GetSysFile();
		PlatformFileSystemIterator iter;
		if(!iter.IsDirectory(rootSysFile))
		{
			return;
		}
	#ifdef WINDOWS
		// Windows dir iteration a little diff to Mac
		rootSysFile.Append(fFilter);
	#endif
		iter.SetStartingPath(rootSysFile);
		
		IDFile sysFile;
		bool16 hasNext= iter.FindFirstFile(sysFile);

		while(hasNext)
		{
			SDKFileHelper fileHelper(sysFile);
			PMString truncP = PnlTrvUtils::TruncatePath(fileHelper.GetPath());
			if(validPath(truncP)
				&& (truncP.Contains(".doc") || truncP.Contains(".DOC") ||
				   truncP.Contains(".TXT") || truncP.Contains(".txt") ||
				   truncP.Contains(".xy3") || truncP.Contains(".XY3") ||
				   truncP.Contains(".JPG") || truncP.Contains(".jpg") ||
				   truncP.Contains(".EPS") || truncP.Contains(".eps") ||
				   truncP.Contains(".BMP") || truncP.Contains(".bmp") ||
				   truncP.Contains(".PSD") || truncP.Contains(".psd") ||
				   truncP.Contains(".PDF") || truncP.Contains(".pdf") ||
				   truncP.Contains(".incd") || truncP.Contains(".incd") ||
				   truncP.Contains(".TIF") || truncP.Contains(".tif")))
			{
				PnlTrvDataNode* newNode = new PnlTrvDataNode();
				newNode->SetData(fileHelper.GetPath());
				newNode->SetParent(parentNode);
				parentNode->AddChild(static_cast<const PnlTrvDataNode& >(*newNode));
				fPathNodeMap.insert( std::pair<PMString, PnlTrvDataNode* >(fileHelper.GetPath(), newNode));
				fRecursionDepthLimiter++;
				this->calcDescendentsRecursively(newNode);
				fRecursionDepthLimiter--;
			}
			hasNext= iter.FindNextFile(sysFile);
		}
	} while(kFalse);*/
	//CAlert::InformationAlert("12");
}

/* validPath
*/
bool16 PnlTrvDataModel::validPath(const PMString& p)
{
	const PMString thisDir(".");
	const PMString parentDir("..");
	return p != thisDir && p != parentDir;
}


/* calcCurrentTreeHashValue
*/
int32 PnlTrvDataModel::calcCurrentTreeHashValue()
{
	int32 retval = 0;
	std::map<PMString, PnlTrvDataNode* >::iterator iter;
	for(iter = fPathNodeMap.begin(); iter != fPathNodeMap.end(); iter++)
	{
		PMString key = iter->first;
		retval += key.Hash();
	}
	return retval;
}

bool16 PnlTrvDataModel::BuscaNodo(PnlTrvDataNode* parentNode,PMString NomNodo)
{
	int32 NumNodo=0;
	PMString NomPadre=parentNode->GetData();
	NumNodo=this->GetChildIndexFor(NomPadre,NomNodo);
	if(NumNodo==-1)
	{
		//CAlert::InformationAlert("No existe");
		return(kFalse);
	}
	return(kTrue);
}

/**
	Esta funcion es encargada de buscar en campo de la estructura preferencia se encuentra el numero
	de campo en donde se desea guardar una cadena
*/
int PnlTrvDataModel::CampoAGuardar(int32 numCampo,Preferencias &Pref)
{	
	int retval;
	
	//		CString CSNumCampo;//Numero String
		int32 IntNumCampo;//Numero entero
		//se realizara una conversion de caracter a entero para hacer las comparaciones
		IntNumCampo=Pref.PosCampos1;
		if(IntNumCampo==numCampo)
			retval=1;
		else
		{
			IntNumCampo=Pref.PosCampos2;
			if(IntNumCampo==numCampo)
				retval=2;
			else
			{
				IntNumCampo=Pref.PosCampos3;
                if(IntNumCampo==numCampo)
                    retval=3;
				else
				{
                    IntNumCampo=Pref.PosCampos4;
                    if(IntNumCampo==numCampo)
						retval=4;
					else
					{
                        IntNumCampo=Pref.PosCampos5;
                        if(IntNumCampo==numCampo)
							retval=5;
						else
						{
                            IntNumCampo=Pref.PosCampos6;
                            if(IntNumCampo==numCampo)
								retval=6;
							else
							{
								IntNumCampo=Pref.PosCampos7;
                                if(IntNumCampo==numCampo)
									retval=7;
								else
								{
                                    IntNumCampo=Pref.PosCampos8;
									if(IntNumCampo==numCampo)
										retval=8;
									else
									{
                                        IntNumCampo=Pref.PosCampos9;
										if(IntNumCampo==numCampo)
                                            retval=9;
										else
										{
											IntNumCampo=Pref.PosCampos10;
											if(IntNumCampo==numCampo)
												retval=10;
											else
											{
												IntNumCampo=Pref.PosCampos11;
                                                if(IntNumCampo==numCampo)
													retval=11;
												else
												{
													IntNumCampo=Pref.PosCampos12;
													if(IntNumCampo==numCampo)
														retval=12;
													else
													{
                                                        IntNumCampo=Pref.PosCampos13;
														if(IntNumCampo==numCampo)
															retval=13;
														else
														{
															IntNumCampo=Pref.PosCampos14;
                                                            if(IntNumCampo==numCampo)
                                                                retval=14;
															else
																retval=0;
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	return(retval);
}

/**
obtiene la cantidad de avisos dentro de el archivo a importar
*/
int32 PnlTrvDataModel::CantidadAvisos(PMString NomArchivo)
{
	FILE *stream;
	PMString CSDireccion;
	
	PlatformChar Caracter;
	PMString CSLinea;
	
	CSDireccion="";
	Caracter=' ';
	CSLinea="";

	CSDireccion=NomArchivo.GrabCString();

	int Linea;
	int ANSCCICaranter;
	Linea=0;

	//do
	//{
		if( (stream  = fopen(CSDireccion.GrabCString(),"r"))==NULL)
			{	
				SDKUtilities::InvokePlugInAboutBox( kA2PImpErrorAlAbrirArchivoStringKey );
				//break;
			}
		else
			{		
		
				while(!feof(stream))
				{	
					do
					{		
						Caracter.SetFromUnicode(fgetc(stream));
						ANSCCICaranter=Caracter.GetValue();
					}while(ANSCCICaranter!=10 && ANSCCICaranter!=13 && !feof(stream));
					Linea++;
				}
				
				fclose(stream);//exes_sa@hotmail. com
			}
	//}while(kFalse);
	//CSLinea=fcvt(Linea,0,&j,&k);
	
	return(Linea);
}

//	end, File: PnlTrvDataModel.cpp
