/*
//	File:	A2PImpCerrarPnObserver.cpp
//
//	Este archivo es utilizado pa ejecutar instrucciones las cuales cierren el panel,}
//	todo esto al dar click sobre el boton cerrar de la paleta Importar
*/
#include "VCPlugInHeaders.h"
// Interface includes:

#include "IApplication.h"
#include "ISession.h"
#include "IPnlTrvOptions.h"
#include "IWorkspace.h"
#include "IPnlTrvChangeOptionsCmdData.h"
#include "ITreeViewMgr.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "IBoolData.h"
#include "CObserver.h"
#include "ISubject.h"
// General includes:
#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "PnlTrvUtils.h"

#include "UIDList.h"
#include "CmdUtils.h"
#include "SysFileList.h"
#include "CAlert.h"
#include "IOpenFileDialog.h"

// Project includes:
#include "IPalettePanelUtils.h"
//#include "IPaletteMgr.h"
#include "IPanelMgr.h"
#include "IApplication.h"
#include "A2PImpID.h"
#include "IActualizarFunction.h"

#ifdef MACINTOSH
		#include "IA2PPreferenciasFunciones.h"
#endif
#ifdef WINDOWS
		#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif





/** SelDlgIconObserver
	Allows dynamic processing of icon button widget state changes, in this case
	the tab dialog's info button. 

	Implements IObserver based on the partial implementation CObserver. 

	@author Lee Huang
*/
class SelBotonCerraPnObserver : public CObserver
{
	public:	
		//StructAvisos Avisos;
		/**
			Constructor.

			@param boss interface ptr from boss object on which interface is aggregated.
		*/
		SelBotonCerraPnObserver(IPMUnknown *boss); //: CObserver(boss) {}

		/** Destructor. */
		virtual ~SelBotonCerraPnObserver() {}

		virtual void AutoAttach();

        virtual void AutoDetach();		
		/**
			Called by the host when the observed object changes. In this case: 
			the tab dialog's info button is clicked.

			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(SelBotonCerraPnObserver, kA2PImpBotonCerrarObserverImpl)

SelBotonCerraPnObserver::SelBotonCerraPnObserver(IPMUnknown* boss)
	: CObserver(boss)
{
}

/*	
	Update
	es llamada cuando das clic sobre el boton cerra del panel Fluir.
	su funcion principal es la de cerrar el panel Fluir.
*/
void SelBotonCerraPnObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID& protocol, 
	void* changedBy
)
{
	InterfacePtr<IControlView> view(theSubject, IID_ICONTROLVIEW);

	if (view != nil)
	{
		// Get the button ID from the view
		WidgetID theSelectedWidget = view->GetWidgetID();

		if (theSelectedWidget == kA2PImpOKButtonCerrarWidgetID && theChange == kTrueStateMessage)
		{
			
			do {
				
				//se obtien la base de datos del Panel
				IDataBase* db = ::GetDataBase(this);
				ASSERT(db);
				if(!db) {
					break;
				}
			
				//Se obtien la interfaz de la plicion
				InterfacePtr<IApplication> app(GetExecutionContextSession()->QueryApplication());
				ASSERT(app);
				if(!app) {
					break;
				}

				/*//se obtien la interfaz de manegador de la paleta apartir de la interfaz de la aplicacion
				InterfacePtr<IPaletteMgr> palMgr(app->QueryPaletteManager());
				ASSERT(palMgr);
				if(!palMgr) {
					break;
				}
				*/
				//Se obtiene la interfaz manegjadora del panel a partir del manejador de la paleta
				InterfacePtr<IPanelMgr> panelMgr(app->QueryPanelManager());
				ASSERT(panelMgr);
				if(!panelMgr) {
					break;
				}

				//se obtiene la lista de la lista de paneles a partir del manejador de paneles
				//PanelMgrEntryList* ListaPaneles=panelMgr->GetPanelList();
				//se le indica el panel que se debe cerrar por medio del ID del panel deseado.
				panelMgr->HidePanelByWidgetID(kA2PImpPanelWidgetID);
			}while(false);		
		}
		else
		{
			if (theSelectedWidget == kA2PImpButtonUpdateWidgetID && theChange == kTrueStateMessage)
			{
				do
				{
					//if(Obtener_MacAddressReg_Y_ComparaCMacAddress())//if(LlaveCorrecta())////
					//{	
					InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
					(
						kA2PPreferenciasFuncionesBoss,	// Object boss/class
						IA2PPreferenciasFunctions::kDefaultIID
					)));
					if(A2PPrefeFuntions==nil)
						break;
				
					if(A2PPrefeFuntions->VerificaSeguridadPirateria())
					{
						InterfacePtr<IA2PActualizarFunction> A2PActualizar(static_cast<IA2PActualizarFunction*> (CreateObject
						(
								kA2PActualizarFunctionBoss,	// Object boss/class
							IA2PActualizarFunction::kDefaultIID
						)));
						
				
						Preferencias Prefer;
						
						PMString DefaulPrefer=PnlTrvUtils::CrearFolderPreferencias();
						DefaulPrefer.Append("Default.pfa");
						if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
						{
							CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
							break;
						}
						
						A2PActualizar->Actualizar();
					}
			
				
				}while(false);
				
				
			}
			
		}
    }	
}//end Funcion




/* AutoAttach */
/************AL INICIAR LA FORMA SE CARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR************************************************************************/

void SelBotonCerraPnObserver::AutoAttach()
{//	CAlert::WarningAlert("AutoAttach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_IBOOLEANCONTROLDATA);
	}
}

/* AutoDetach */
/************AL TERMINAR LA FORMA SE DESCARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR************************************************************************/

void SelBotonCerraPnObserver::AutoDetach()
{//	CAlert::WarningAlert("AutoDetach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		//subject->DetachObserver(this);
	}
}

