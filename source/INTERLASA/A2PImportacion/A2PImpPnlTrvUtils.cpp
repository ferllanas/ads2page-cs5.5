//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvUtils.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPluginHeaders.h"

// Interface includes
//#include "IImportFileCmdData.h"
#include "IDocument.h"
#include "IPlaceGun.h"
#include "IPanelControlData.h"
#include "IMetaDataAccess.h"
#include "ICoreFilename.h"
#include "IUIDData.h"
#include "IImportResourceCmdData.h"
#include "IURIUtils.h"

// Other API includes
#include "K2SmartPtr.h"
#include "SDKFileHelper.h"
#include "IPalettePanelUtils.h"
#include "TextChar.h"
#include "ILayoutUIUtils.h"
#include "SDKUtilities.h"
#include "FileUtils.h"
#include "CAlert.h"

// Project includes
#include "PnlTrvUtils.h"


#ifdef WINDOWS
#define PLATFORM_PATH_DELIMITER kTextChar_ReverseSolidus
#endif
#ifdef MACINTOSH
#define PLATFORM_PATH_DELIMITER kTextChar_Colon
#endif


/* TruncatePath
*/
PMString PnlTrvUtils::TruncatePath(const PMString& fromthis)
{
	if(fromthis.IsEmpty() == kTrue) return PMString("Empty!");

	PMString retval = fromthis;
	int32 lastpos = (-1);
	for (int32 i = 0 ; i < fromthis.CharCount();i++)
	{
		bool16 predicate = (fromthis[i] == PLATFORM_PATH_DELIMITER);
		if (predicate)
		{
			lastpos = i;
		}
	}

	if(lastpos >= 0)
	{
		// Suppose we have ../X/Y.gif
		// Then, lastpos would be 4, and we'd want 5 chars from 5 onwards
		int32 countChars = fromthis.CharCount() - (lastpos+1);
		int32 startIndex = lastpos+1;
		int32 endIndex = (startIndex + countChars);
		if((endIndex > startIndex) && (endIndex <= fromthis.CharCount()))
		{
			K2::scoped_ptr<PMString> ptrRightStr(fromthis.Substring(startIndex, countChars));
			if(ptrRightStr)
			{
				retval = *ptrRightStr;
			}
		}
	} 
	return retval;
}


/* ImportImageAndLoadPlaceGun
*/
UIDRef PnlTrvUtils::ImportImageAndLoadPlaceGun(
	const UIDRef& docUIDRef, const PMString& fromPath)
{
	// Precondition: active document
	// Precondition: file exists
	// 
	UIDRef retval = UIDRef::gNull;
	do
	{
		IDFile idFile;
		FileUtils::PMStringToIDFile(fromPath,idFile);
		SDKFileHelper fileHelper(idFile);
		bool16 fileExists = fileHelper.IsExisting();
		// Fail quietly... ASSERT(fileExists);
		if(!fileExists)
		{
			break;
		}
		InterfacePtr<IDocument> 
		iDocument(docUIDRef, UseDefaultIID());
		ASSERT(iDocument);
		if(!iDocument)
		{
			break;
		}
		
		// Wrap the import in a command sequence.
		//PMString seqName("Import");
		//CmdUtils::SequenceContext seq(&seqName);
		//seq=kFailure;
		
		// If the place gun is already loaded abort it so we can re-load it. 
		InterfacePtr<IPlaceGun> placeGun(iDocument, UseDefaultIID());
		ASSERT(placeGun);
		if (!placeGun) 
		{
			break;
		}
		if (placeGun->IsLoaded())
		{
			InterfacePtr<ICommand> abortPlaceGunCmd( CmdUtils::CreateCommand(kAbortPlaceGunCmdBoss));
			ASSERT(abortPlaceGunCmd);
			if (!abortPlaceGunCmd)
			{
				break;
			}
			InterfacePtr<IUIDData> uidData(abortPlaceGunCmd, UseDefaultIID());
			ASSERT(uidData);
			if (!uidData) 
			{
				break;
			}
			uidData->Set(placeGun);
			if (CmdUtils::ProcessCommand(abortPlaceGunCmd) != kSuccess) 
			{
				ASSERT_FAIL("kAbortPlaceGunCmdBoss failed");
				break;
			}
		}
		
		IDataBase* db = docUIDRef.GetDataBase();
		ASSERT(db);
		if(!db)
		{
			break;
		}
		
		// Now import the selected file and load it's UID into the place gun.
		InterfacePtr<ICommand> importCmd(CmdUtils::CreateCommand(kImportAndLoadPlaceGunCmdBoss));
		ASSERT(importCmd);
		if (!importCmd) {
			break;
		}
		
		InterfacePtr<IImportResourceCmdData> data(importCmd, IID_IIMPORTRESOURCECMDDATA);
		if (data == nil)
			break;
		
		//IDFile myfile(fromPath);
		URI fileURI;
		Utils<IURIUtils>()->IDFileToURI(idFile, fileURI);
		
		data->Set(db, fileURI, K2::kSuppressUI);  
		
		ErrorCode err = CmdUtils::ProcessCommand(importCmd);
		
		ASSERT(err == kSuccess);
		if(err != kSuccess)
		{
			break;
		}	
		// Get the contents of the place gun as our return value
		UIDRef placedItem(db, placeGun->GetFirstPlaceGunItemUID());
		retval = placedItem; 
		//seq.SetState(kSuccess);
	} while(kFalse);
	
	return retval;
}


/* GetWidgetOnPanel
*/
IControlView* PnlTrvUtils::GetWidgetOnPanel(
	const WidgetID& panelWidgetID, const WidgetID& widgetID)
{
	IControlView* controlView=nil;
	do
	{
		InterfacePtr<IPanelControlData> 
			panelData(Utils<IPalettePanelUtils>()->QueryPanelByWidgetID(panelWidgetID));	
		// Don't assert, fail silently, the tree view panel may be closed.
		if(panelData == nil)
		{
			break;
		}
		controlView = panelData->FindWidget(widgetID);
		ASSERT(controlView);
		if(controlView == nil)
		{
			break;
		}
	} while(0);
	
	return controlView;
}

/* TruncatePath
*/
PMString PnlTrvUtils::TruncateExtencion(const PMString& fromthis)
{
	if(fromthis.IsEmpty() == kTrue) return PMString("Empty!");

	PMString retval = fromthis;
	int32 lastpos = (-1);
	lastpos=fromthis.IndexOfString("."); 

	if(lastpos >= 0)
	{
		// Suppose we have ../X/Y.gif
		// Then, lastpos would be 4, and we'd want 5 chars from 5 onwards
		int32 countChars = lastpos;
		int32 startIndex = 0;
		int32 endIndex = (startIndex + countChars);
		if((endIndex > startIndex) && (endIndex <= fromthis.CharCount()))
		{
			K2::scoped_ptr<PMString> ptrRightStr(fromthis.Substring(startIndex, countChars));
			if(ptrRightStr)
			{
				retval = *ptrRightStr;
			}
		}
	} 
	return retval;
}


bool16 PnlTrvUtils::validPath(const PMString& p)
{
	const PMString thisDir(".");
	const PMString parentDir("..");
	return p != thisDir && p != parentDir;
}


bool16 PnlTrvUtils::SaveXMPVar(PMString Variable,PMString value)
{
	bool16 retval=kFalse;
	do 
	{
		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		
		IDataBase* db = ::GetDataBase(doc);
		
		db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;

		metaDataAccess->Set("http://ns.adobe.com/xap/1.0/", Variable, value);
		db->EndTransaction();
		retval=kTrue;
	} while(false);
	return(retval);
}


bool16 PnlTrvUtils::RemoveXMPVar(PMString Variable)
{
	do 
	{
		
		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		IDataBase* db = ::GetDataBase(doc);
		
		db->BeginTransaction();
		
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			CAlert::ErrorAlert("metaDataAccess is nil!");
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;

		metaDataAccess->Remove("http://ns.adobe.com/xap/1.0/", Variable);
		db->EndTransaction();
	} while(false);
	return(kTrue);
}

bool16 PnlTrvUtils::GetXMPVar(PMString Variable,PMString& value)
{
	bool16 retval=kFalse;
	do 
	{
		IDocument* doc = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (doc == nil) 
		{
			ASSERT_FAIL("You do not have a document open.  This snippet requires a front document.");
			break;
		}
		IDataBase* db = ::GetDataBase(doc);
		
		db->BeginTransaction();
		
		InterfacePtr<IMetaDataAccess> metaDataAccess(doc, UseDefaultIID());
		if (metaDataAccess == nil)
		{
			ASSERT_FAIL("metaDataAccess is nil!");
			break;
		}

		int32 nodesVisited = 0;
		//MetaDataFeatures metaDataFeatures;

		if(metaDataAccess->Get("http://ns.adobe.com/xap/1.0/", Variable, value))
			retval=kTrue;
		db->EndTransaction();
	} while(false);
	return(retval);
}

PMString PnlTrvUtils::CrearFolderPreferencias()
{
	
	PMString Archivo = "";
	SDKUtilities::GetApplicationFolder(Archivo);
	Archivo.Append(":asalretni:");
	IDFile fileFolder;
		
		FileUtils::PMStringToIDFile(Archivo,fileFolder);
		
		if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
		{//Fue creado  
			Archivo.Append("Software:");
			FileUtils::PMStringToIDFile(Archivo,fileFolder);
			if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
			{//fue creado
				Archivo.Append("ADS2PAGE30:");
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias:");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias:");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}

			}
			else
			{
				Archivo.Append("ADS2PAGE30:");
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias:");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias:");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
		}
		else//ya habia sido creado
		{
			Archivo.Append("Software:");
		//	Archivo.Append("ADS2PAGE30:");
		//	Archivo.Append("Preferencias:");
			FileUtils::PMStringToIDFile(Archivo,fileFolder);
			if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
			{//fue creado
				Archivo.Append("ADS2PAGE30:");
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias:");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias:");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
			else
			{
				Archivo.Append("ADS2PAGE30:");
				FileUtils::PMStringToIDFile(Archivo,fileFolder);
				if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
				{//fue creado
					Archivo.Append("Preferencias:");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
				}
				else
				{
					Archivo.Append("Preferencias:");
					FileUtils::PMStringToIDFile(Archivo,fileFolder);
					if(FileUtils::CreateFolderIfNeeded(fileFolder)<0)
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
					else
					{//fue Creado
						//Archivo.Append("C2PPref.pfa");
					}
				}
			}
		}
	return(Archivo);
}


bool16 PnlTrvUtils::ProporcionaStructFechaDataDelArchivo(PMString Archivo,struct FechaData *date)
{
	
	
	uint64 size=0;
	uint64 time=0;

	PMString FilePath(Archivo);
	InterfacePtr<ICoreFilename> cfn((ICoreFilename*)::CreateObject(kCoreFilenameBoss,IID_ICOREFILENAME));
	IDFile file=SDKUtilities::PMStringToSysFile(&FilePath);
	if(cfn->Initialize(&file)==0)
	{
		
		IDataLink *link=nil;
		cfn->GetFileInfo(&size,&time,link);
	
		GlobalTime  GlTimeFile(time);
		
		
					
		GlTimeFile.GetTime(&date->yyy , &date->mm, &date->dd, &date->hr, &date->min, &date->sec);
		
	}
	else
	{
		date->yyy=0;
		date->mm=0;
		date->dd=0;
		date->hr=0;
		date->min=0;
		date->sec=0;
	}
	return(kTrue);

}


bool16 PnlTrvUtils::ProporcionaStructFechaDataDeCadena(PMString Cadena,struct FechaData *date)
{
		date->yyy = Cadena.GetAsNumber();
		if(date->yyy>0)
		{
			int32 index = Cadena.IndexOfString("/",0);
			Cadena.Remove(0,index+1);
		
		
			date->mm=Cadena.GetAsNumber();
			index=Cadena.IndexOfString("/",0);
			Cadena.Remove(0,index+1);
	
		
			date->dd=Cadena.GetAsNumber();
			index=Cadena.IndexOfString("/",0);
			Cadena.Remove(0,index+1);
		
		
			date->hr=Cadena.GetAsNumber();
			index=Cadena.IndexOfString("/",0);
			Cadena.Remove(0,index+1);
	
		
			date->min=Cadena.GetAsNumber();
			index=Cadena.IndexOfString("/",0);
			Cadena.Remove(0,index+1);
	
		
			date->sec=Cadena.GetAsNumber();
	
		}
		else
		{
			//CAlert::InformationAlert("no existe fecha");
			date->mm=0;
			date->dd=0;
			date->hr=0;
			date->min=0;
			date->sec=0;
		}
		
		
		return(kTrue);

}
//	end, File: PnlTrvUtils.cpp
