/*
//	File:	SelectBotonFluir.cpp
//
//	Date:	21-Agosto-2003
//
//	Este codigo es generado para cumplir con la funcion de pegar las imagenes 
//	de en un aviso correspondiente.Es el encargado de generar y construir los 
//	grafics frames los cuales contendran las imagenes del aviso o anuncio, 
//	tambien genera las etiquetas de cada caja de imagen (textFrames).Ademas es encargado de 
//	aplicar las diferentes preferencias (Color de marco, Color fondo, ajuste de imagen,,etc)
//	tambien genera la creacion de capas, asi como la generacion de la capa Update y el TextUpdate,
//	la creacion de colores utilies para Ads2Page 2.0.
//	
//		
//
//
*/
/*		Acontinuacion se declaran las diferentes librerias que se utilizaran para el
		de las funciones que utiliza Fluir.											*/
#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"

// Interface includes
#include "IActionStateList.h"
#include "IApplication.h"

#include "IBoolData.h"

#include "IControlView.h"
#include "IColorData.h"
#include "ICommand.h"
#include "ICommandSequence.h"
#include "ICreateMCFrameData.h"
#include "IDocumentLayer.h"
#include "IPageList.h"
#include "IPageSetupPrefs.h"
#include "IDialogMgr.h"

// Dialog-specific resource includes:
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"

#include "IDatabase.h"
#include "IDialog.h"
#include "IDialogCreator.h"
#include "IDialogController.h"
#include "IDocument.h"
#include "IDocumentUtils.h"
#include "IDocFileHandler.h"
#include "IDocumentLayer.h"
#include "IDocumentList.h"
#include "IDrawingStyle.h"
#include "IDocSetupCmdData.h"

#include "IFrameData.h"
#include "IFrameContentUtils.h"
#include "IFrameList.h"
#include "IFrameListComposer.h"
#include "IFontMgr.h"
#include "IDocFontMgr.h"
#include "ILayoutControlData.h"


#include "IGraphicAttributeUtils.h"
#include "IGraphicAttrIndeterminateData.h"
#include "IGraphicAttrRealNumber.h"//atributos de rectangulo
#include "IGraphicFrameData.h"
//#include "IGraphicMetaDataUtils.h"
#include "IGraphicMetaDataObject.h"
#include "IGraphicStateRenderObjects.h"
#include "IGraphicStateUtils.h"
#include "IGraphicStyleDescriptor.h"////////para cambiar el tama‚àö√≠o del marco del rectangulo////////////
#include "IGeometry.h"
#include "IStyleGroupManager.h"

#include "IHierarchy.h"
#include "IHierarchyCmdData.h"

#include "IImportProvider.h"
//#include "IImportFileCmdData.h"
#include "IImportManagerOptions.h"

#include "ILayerList.h"
#include "ILayoutCmdData.h"
#include "ILayoutControlData.h"
#include "ILayoutSelectionSuite.h"


#include "IMargins.h"
#include "IMeasurementSystem.h"
#include "IMeasureUnitsCmdData.h"

#include "INewDocCmdData.h"
#include "INewLayerCmdData.h"
#include "INewPageItemCmdData.h"

#include "IOpenFileDialog.h"
#include "IOpenFileCmdData.h"
#include "IOpenManager.h"

//#include "IPageItemSelector.h"
#include "IPanelControlData.h"
#include "IPathUtils.h"
#include "IPathGeometry.h"
#include "IPlaceGun.h"
#include "IPlaceBehavior.h"
#include "IPlacePIData.h"
#include "IPMStream.h"
#include "IPageItemTypeUtils.h"
#include "IPageColumnsCmdData.h"//Interfaz de los datos para el comando de las columnas del sistema.
#include "IPersistUIDData.h"
//#include "IPersistUIDRefData.h"
#include "IPMUnknownData.h"
#include "IPathSelectionList.h"
#include "IPathSelection.h"


#include "IRenderingObject.h"
#include "IReplaceCmdData.h"

#include "ISession.h"
#include "ISubject.h"
#include "ISysFileData.h"
//#include "ISelection.h"
#include "ISelectableDialogSwitcher.h"
//#include "ISpecifier.h"
#include "IScrapItem.h"		////////////para borra frame////////////////////
#include "ISpreadList.h"
#include "ISpread.h"
#include "IStrokePref.h"//Para ajustar el marco de un graficframe
#include "IStrokeChangePreference.h"
#include "IStyleInfo.h"
#include "ISwatchList.h"
#include "ISwatchUtils.h"
#include "ISelectionUtils.h"
//#include "IScaleItemCmdData.h"
#include "ISpreadLayer.h"
#include "IStyleNameTable.h"


#include "ITextAttrUtils.h"//atributos de texto
#include "ITreeViewHierarchyAdapter.h"
#include "ITreeViewMgr.h"
#include "ITreeNodeIDData.h"
#include "ITreeViewController.h"
#include "ITextModel.h"
#include "ITextAttrAlign.h"
#include "ITextAttrBoolean.h"
#include "ITextAttrFont.h"
#include "ITextAttributes.h"
#include "ITextAttributeSuite.h"
#include "ITextAttrInt16.h"
#include "ITextAttrRealNumber.h"
//#include "ITextFrame.h"
#include "ITextModel.h"
#include "ITextModelCmds.h"
#include "ITextAttrUID.h"
#include "TextAttrID.h"
#include "TextID.h"

#include "IUIDData.h"
#include "IUnitOfMeasure.h"
#include "IUnitOfMeasureSettings.h"

#include "IWidgetParent.h"
#include "IWindowList.h"
#include "IWorkspace.h"

#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IKentenStyle.h"

#include "IZeroPointPrefsCmdData.h"//Para ajustar el punto cero del documento
/////////////////////////////
// General includes/////////
///////////////////////////
#include "AttributeBossList.h"
#include "CAlert.h"
#include "CActionComponent.h"
#include "CmdUtils.h"
#include "CObserver.h"
#include "ErrorUtils.h"
//#include "FrameUtils.h"
#include "IImageUtils.h"
#include "ILayoutUIUtils.h"
#include "IPathUtils.h"
#include "PersistUtils.h"
#include "PMString.h"
#include "PMReal.h"
#include "PMRect.h"
#include "PreferenceUtils.h"
#include "UIDList.h"
#include "UIDRef.h"
#include "Utils.h"
#include "StreamUtil.h"
#include "SysFileList.h"
#include "Trace.h" 
#include "TransformUtils.h"
#include "K2Vector.h"
#include "K2Vector.tpp"
#include "WideString.h"
/////////////////////
// includes IDs://///
/////////////////////
#include "DocumentID.h"
#include "LayoutID.h"
#include "OpenPlaceID.h"
#include "A2PImpID.h"
#include "PnlTrvFileNodeID.h"
#include "SpreadID.h"
#include "SplineID.h"
#include "ITextAttrUID.h"
#include "TextID.h"
/////////////////////
//includes Proyect///
/////////////////////
#include "ITextAttrAlign.h"
#include "ICompositionStyle.h"
#include "AttributeBossList.h"
#include "ITextAttrRealNumber.h"
#include "FileUtils.h"
#include "ILayoutUIUtils.h"
#include "SDKLayoutHelper.h"
#include "ILayerUtils.h"


#ifdef MACINTOSH
	#include <types.h> 
#endif

#include <sys/stat.h> 
#include <time.h>
#include <locale.h> 
#include <Math.h> 


#include "IDocumentCommands.h"
#include "IOpenLayoutCmdData.h"
#include "IWindow.h"

#include "PnlTrvDataModel.h"
#include "PnlTrvUtils.h"
#include "A2PImpStructAvisos.h"
#include "IA2PFluirFunction.h"
#include "ImportImagenClass.h"

#include "ICompositeFont.h"
//#include "IPlantillerImpIMP.h"
//#include "PlantillerImpID.h"
#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
	#include "IA2PPreferenciasFunciones.h"
#endif
#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"
	#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif

#include "IActualizarFunction.h"
#include "A2PAvisosHeader.h"


//#include "InterPLGDemDocWchUtils.h"


/**
	Integrator ICheckInOutSuite implementation. Uses templates
	provided by the API to delegate calls to ICheckInOutSuite
	implementations on underlying concrete selection boss
	classes.

	@author Juan Fernando Llanas Rdz
*/
class A2PFluirFunction : public CPMUnknown<IA2PFluirFunction>
{
public:
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	A2PFluirFunction (IPMUnknown *boss);

	/** 
			Destructor.
	*/
	virtual ~A2PFluirFunction(void);
	
	/**
	*/
	bool16 Fluir();
	
	bool16 ActualizarDocumento();
	
	
	bool16 ActualizarDocumentoPlantillero(const PMString& Publicacion,const PMString& Seccion,const PMString& NumPagina,const PMString& FechaPublicacionPag);

private:
	/*
			Valida que se haya seleccionada unicamente una seccion del arbol. Regresa una variable del tipo boolean
			
			  @parametro NodosSeleccionados especifica los nodos se encuentran seleccionados en el arbol. Es del tipo NodoIDList que es la lista de todos los nodos seleccionados, pero en este caso unicamente contendra un nodo
					el nodo de la seccion o pagina.
		*/
		bool16 SoloUnaSeccion(NodeIDList& NodosSeleccionados);

		/**
			llamada para obtener el nombre de la pagina o seccion seleccionado.Regrea una variable del Tipo PMString
			
			  @parametro	NodosSeleccionados especifica los nodos se encuentran seleccionados en el arbol. Es del tipo NodoIDList que es la lista de todos los nodos seleccionados, pero en este caso unicamente contendra un nodo
					el nodo de la seccion o pagina.

		*/
		PMString ObtenerNombreSeccionAFluir(NodeIDList& NodosSeleccionados);

		/**	
			Esta funcion obtiene el nombre del nodo padre o raiz el cual debe 
			contener la direccion del archivo	que se debe de importar.Regresa una variable del tipo PMString.
		*/
		PMString DireccionArchivo();

		/**	
			Es Llamada para llenar la estructura de Avisos lee el archivo refenciado:
			
			  @param Avisos, Es la estructura a llenar. del Tipo StructAvisos(ver StructAvisos.h). Esta estructura es previamente declarada de un tama‚àö√≠o determinado por la cantidad de avisos del archivo a importar.
			  @param DireccionPMString, Variable de entrada del tipoPMString que contiene la direccion(path) del archivo a importar.
			  @param NumAvisos, Variable de entrada del tipo int32 (entero) la cual contiene la cantidad de Avisos en el Archivo a importar.
			  @param Prefer, Estructura del Tipo Preferencias (ver PnlTrvDataModel.h), Utilizada para saber la forma de lectura de los campos de los avisos.
		 */
		void LlenarStructAvisos(StructAvisos *Avisos,PMString DireccionPMString,int32 NumAvisos,Preferencias &Prefer);							
		
		/**		
			Es llamada para obtener los avisos de la seccion o nodo seleccionado.
			
			  @param StructAvisos *AvisoCompletos, Estructura que contiene todos los avisos del archivo que se importo.
			  @param StructAvisos *AvisoAFluir, Es la estructura que se llena unicamente con los avisos del nodo que fue seleccionado.
			  @param PMString NomSeccion, nombre de la seccion o pagina que fue seleccionada.
			  @param int32 NumAvisos, Es la cantidad de nodoa hijos del nodo seleccionado, en otras palabras la cantidad de avisos a fluir en el documento.
		*/
		void ObtenerAvisosDeSeccion(StructAvisos *AvisoCompletos,StructAvisos *AvisoAFluir,PMString NomSeccion,int32 NumAvisos,int32 NumAvisoSec);
		/**
			Esta funcion tiene como entrada lista de nodos seleccionados, y su salida sera la cantidad de nodos hijos
				del nodo seleccionado(cantidad de avisos del la pagina o seccion seleccionada).
				
				  @param NodosSeleccionados especifica los nodos se encuentran seleccionados en el arbol. Es del tipo NodoIDList que es la lista de todos los nodos seleccionados, pero en este caso unicamente contendra un nodo
					el nodo de la seccion o pagina.
		*/
		int32 NumeroHijos(NodeIDList& NodosSeleccionados);
						
			/**
			Funcion para convertir a puntos cada uno de las coordenadas dependiendo 
			de la unidades en que esten especificadas.
			
			  @param Cantidad, cantidad a convertir a puntos.
			  @param Unidades, Texto que especifica las unidades en que se encuentra esta cantidad.

		*/
		PMReal ConvertirCoorAPuntos(PMReal Cantidad, PMString Unidades);
				
		/**
			Funcion que crea nuevo documento.
			
			  @param Pref, Estructura del tipo Preferencias, utilizado para aplicar las preferncias determinadas al documento.
		*/
		bool16 CrearDocumento(Preferencias &Pref);
		/**
			Funcion que manda llamar la creacion de cada una de las capas dependiendo las preferencias.
			
			  @param Pref, Estructura del tipo Preferencias, utilizado para aplicar las preferncias determinadas al documento.
		*/
		bool16 CreaciondeCapas(Preferencias &Prefer);
		/**
			Funcion que crea una determinada capa.
			
			  @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa con que se debe crear.
			  @param CapaBloqueada,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de bloqueo de la capa a crear;0 para no bloqueada, 1 para bloqueada.
			  @param CapaVisible,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de muestra de la capa a crear; 0 para no visible, 1 para visible
		*/
		void CrearCapa(PMString NombredeCapa,int32 CapaBloqueada,int32 CapaVisible);
		/**
			Funcion para bloquear una determinada capa.
			
			  @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa con que se debe crear.
			  @param CapaBloqueada,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de bloqueo de la capa a crear;0 para no bloqueada, 1 para bloqueada.
		*/
		void BloquearCapa(PMString NombreCapa,bool16 Bloqueo);
		/**
			Funcion para mostra o ocultar una determinada capa.
			
			  @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa con que se debe crear.
			  @param CapaVisible,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de muestra de la capa a crear; 0 para no visible, 1 para visible
		*/
		void MostrarCapa(PMString NombreCapa,bool16 Mostrar);

		
		/**
			Activa una capa dependiendo su nombre NomCapa
			
			  @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa que se desea activar.
		*/
		bool16 ActivarCapa(PMString NomCapa);

		/**
			Convierte la direccion contenida en una variable PMString, en una direccion valida para InDesign.
			//Adiciona "/" despues de uno como este.
			
			  @param filePath, Variable del tipo PMString que contiene el path o direccion normal de una imagen.
		*/
		PMString ConvertAFileAceptada(PMString filePath);

		/**
			Obtiene y separa las coordenadas de la cadena Coordenadas para convertirlas en PMReal (double).
			
			  @param leftMargin, variable del tipo PMReal la cual contendra la coordenada y1 
			  @param topMargin, variable del tipo PMReal la cual contendra x1.
			  @param rightMargin,variable del tipo PMReal la cual contendra y2.
			  @param bottomMargin, variable del tipo PMReal la cual contendra x2.
			  @param Coordenadas, Variable del Tipo PMString la cual contendra la cadena o las coordenadas en forma string o char.
		*/			
		void CoordenadasDeImagen(Preferencias &Prefer, PMReal &leftMargin,PMReal &topMargin,PMReal &rightMargin,PMReal &bottomMargin,PMString Coordenadas);

	

		/**
			Funcion que apilica un color de fondo al rectangulo de la imagen dependiendo las preferencias.
			
			  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param CPublicidad, Variable del tipo bool16 la cual contiene si se encontro o no la imagen que se debio importar.
			  @param frameList, la lista de frames o rectangulos de imagen.
		*/
		void AplicarFondoRecImagen(Preferencias &Prefer,bool16 CPublicidad,UIDRef textFrameUIDR);
		
		/**
			Funcion que apilica un color de marco al rectangulo de la imagen dependiendo las preferencias.
			
			  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param CPublicidad, Variable del tipo bool16 la cual contiene si se encontro o no la imagen que se debio importar.
			  @param frameList, la lista de frames o rectangulos de imagen.
		*/
		void AplicarColorMarcoRecImagen(Preferencias &Prefer,bool16 CPublicidad,UIDRef textFrameUIDR);

		/**
			Funcion que apilica el ajuste de la imagen en rectangulo dependiendo las preferencias.
			
			  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param frameList, la lista de frames o rectangulos de imagen.
		*/
		void AplicarAjusteRecImagen(Preferencias &Prefer,UIDRef textFrameUIDR);
		/**
			Funcion que copia los campos del aviso que se fluye y deben ponerse en la etiqueta dependiendo preferencias, en esta funcion se llama a la funcion CrearRectanguloEtiqueta.
			
			  @param Prefer, Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param AvisoAFluir, Structura de avisos.
			  @param numAviso, numero del aviso que se fluyo.
			  @param CPublicidad, si se encontro  o no la imagen.
			  @param leftMargin, coordenadas y1.
			  @param topMargin, coordenadas x1.
			  @param rightMargin, coordenadas y2.
			  @param bottomMargin, coordenadas x2.
		*/
		int32 TextodeEtiqueta(Preferencias &Prefer,//Funcion que crea un textFrame y llena este frame con los campos del aviso que se fluyo
							StructAvisos *AvisoAFluir,//Structura de avisos
							int32 numAviso,//numero de aviso que se fluyo
							bool16 CPublicidad,//si se encontro  o no la imagen
							PMReal leftMargin,//coordenadas del frame text:
							PMReal topMargin,
							PMReal rightMargin,
							PMReal bottomMargin);

		/**
			Crea el rectangulo de etiquetas
		*/
		int32 CrearRectanguloEtiqueta(Preferencias &Prefer,PMString NumCampodePref,
									bool16 CPublicidad,
									PMReal leftMargin,
									PMReal topMargin,
									PMReal rightMargin,
									PMReal bottomMargin);
		/**
			Funcion que manda llamar la crecion de los colores (llama a la funcion BuscarOcrearColor).
		*/
		void crearColores();

		/**
			Funcion que busca un determinado color por su nombre si no existe lo crea y si existe regresa el UID de este color.
			
			  @param swatchName, nombre del color a buscar.
			  @param rCyan, prioporcion del color cyan que debe tener en caso de no encontrar el color deseado.
			  @param rMagenta, proporcion del color magent que debe tener en caso de no encontrar el color deseado.
			  @param rAmarillo, proporcion del color Yellow que debe tener en caso de no encontrar el color deseado.
			  @param rNegro, proporcion del color Black que debe tener en caso de no encontrar el color deseado.
		*/
		UID BuscarOcrearColor(const PMString& swatchName, 
								  const PMReal rCyan, 
								  const PMReal rMagenta, 
								  const PMReal rAmarillo,
								  const PMReal rNegro);


		
		
		/**
			Funcion que se utiliza a la hora de fluir sobre un documento previamente creado.
			para borra el textframe que contiene los datos utilizxados por la funcion Update.
			Esta funcion busca el ultimo textframe el cual es el TextUpdate puesto que este es
			el ultimo que se crea.
		*/
		void BorraTextoUpdate();
		
		/**
			Funcion que apilica un color de fondo al rectangulo de la etiqueta dependiendo las preferencias.
			
			  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param CPublicidad, Variable del tipo bool16 la cual contiene si se encontro o no la imagen que se debio importar.
			  @param frame, Variable del tipo UIRef que contiene la referencia del rectangulo a cambiar el color del fondo
		*/
		void AplicarFondoTextFrame(Preferencias &Prefer,bool16 CPublicidad,const UIDRef &frame);

		/**
			Funcion que apilica un color de tecto al rectangulo de la etiqueta dependiendo las preferencias.
			
			  @param textModel, Referenc‚àö√•a el texto que se encuantra dentro de la etiqueta.
			  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param CPublicidad, Variable del tipo bool16 la cual contiene si se encontro o no la imagen que se debio importar.
		*/
		void CambiarColorTexto(InterfacePtr<ITextModel> textModel,Preferencias &Prefer,bool16 Cpublicidad);

		/**
			Funcion que apilica un tama‚àö√≠o de marco al rectangulo de la etiqueta dependiendo las preferencias.
			
			  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param textFrameUIDR, Variable del tipo UIRef que contiene la referencia del rectangulo a cambiar el color del fondo
			  @param CPublicidad, Variable del tipo bool16 la cual contiene si se encontro o no la imagen que se debio importar.
		*/
		void AplicarTamanoMarcoEtiqueta(Preferencias &Prefer,UIDRef &textFrameUIDR,bool16 CPublicidad);

		/**
			Funcion que vcompara el numero de columnas en el documento actual con las determinadas en preferencias.
			  
				@param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
		*/
		bool16 ValidaColumnas(Preferencias &Prefer);
		
		/**
			Funcion que compara los margenes en  el documento actual con las determinadas en preferencias.
			  
				@param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
		*/
		bool16 ValidaMargenes(Preferencias &Prefer);

		/**
			Funcion que compara el tama‚àö√≠o del medianil el documento actual con las determinadas en preferencias.
			  
				@param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
		*/
		bool16 ValidaMedianil(Preferencias &Prefer);
		
		/**
			Funcion que obtien el tama‚àö√≠o del documento actual con las determinadas en preferencias.
			  
		*/
		PMRect obtenerTamPagina();

		/**
			Funcion que ajusta el punto cero de el documento actual a 0.0.
			  
		*/
		void AjustarPuntoZero();

		/**
			Altiva la capa dependiendo su gerarquia.
			  
		*/
		void AplicaPreferenciasACapas();

		/**	
			Para convertir un item a una Frame

		*/
		void CreateAndProcessConvertItemToFrameCmd(const UIDRef& itemToConvert);
		/**
			convierte un item a un texto
		*/
		void CreateAndProcessConvertItemToTextCmd(const UIDRef& selectedItem);

		/**
			Para obtener las coordenas dependiendo el orden especificado en TLBR
				
		*/
		void ObtenerCoordenadasDeTLBR(PMString SecuenciaCoord,
										int32 NumeroDeCoord,
										PMReal &top,
										PMReal &left,
										PMReal &bottom,
										PMReal &right,
										PMString Valor);

		/**
			obtiene las coordenadas dependiedo el oprden que se haya seleccionado
		*/
		void ObtenerCoordenadasDeXY(PMString SecuenciaCoord,int32 NumeroDeCoord,PMReal &top,PMReal &left,PMReal &bottom,PMReal &right,PMString Valor);

		/**
			Metodo que busca el archivo correspondiente si no lo encuentra lo buscara por otro tipo de
			extencion.
				@ filePath, variable de cadena tipo PMString que contiene la ruta del archivo oroginal.
						Si no se encuentra con la extencion orinal este cambiara a determinada extencion.
				regresa, si existe el archivo o no para lectura.
		*/
		bool16 ExisteArchivoParaLectura(PMString *filePath);
		
		int32 ObtenerCantAvisosDeSeccion(StructAvisos *AvisoCompletos,PMString NomSeccion,int32 NumAvisos);
		
		void BorrarFrontLayer();
		void BorrarCapas();
		
		/* 
			Se posiciona sobre la pagina que contiene el UID		
		*/
		
		bool16 ShowThePageByUIDPage(int32 UIDPage);
		
		bool16 ShowThePageByNumberPage(int32 which) ;
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		void CrearRectanguloImagen(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso);

		bool16 Abrir_DlgGetFechaPag(PMString& FechaEdicion,PMString& PaginaFluida, int32 &NumeroDePagina, PMString& FechaStandart,const bool16& Plantillero);

		void Aplica_Fecha_Pagina(Preferencias &Prefer,PMString& FechaEdicion,PMString& PaginaFluida);

		PMRect CrearRectanguloFechaOPagina(Preferencias &Prefer,PMString& CadenaAImprimir,PMReal &leftMargin,PMReal &topMargin,PMReal &rightMargin,PMReal &bottomMargin, int32 Alineacion);

		bool16 obtener_TipodePagina(PMString& PaginaFluida);
		
		bool16 ObtenerFechaPublicacionDeAvisos(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso, PMString &DateIssue);

		PMString FechaYHora(PMString& FechaStandart);

		PMString ArmaFecha(PMString fecha);
		void printError();
		int validateDate(int dd, int mm, int yyyy);
		PMString DayWek(int dd,int mm,int yyyy);
		void nameInStr(char daysInWord[], int days);
		int dayInYear(int dd, int mm);
		int calcDay_Dec31(int yyyy);

		bool16 GetPMStringCoordinatesIssueOdAds(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso, PMString& Coordenadas);

		bool16 GetPathIssueOdAds(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso, PMString& PathAds);

		bool16 ObtenerTipoPaginaDeAvisos(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso, PMString &TipoPagina);
		
		
		
		bool16 DELETE_AvisosYEtiquetasParaUpdateGeometria();

		PnlTrvDataModel FModel;//Modelo para utilizar las funciones de lectura de archivos de preferencias asi como de Avisos
		PMString NomSeccionAFluir;//Nombre de la seccion a Fluir
		int32 NumAvisos;//Numero de Avisos Totales a fluir
		int32 NumAvisoSec;//numero de avisos de la seccion a fluir
		PMString DireccionPMString;//Direccion del archivo a Importar o que se importo
		CString  DireccionCString;//Direccion del archivo a Importar o que se importo
		FILE *stream;			//puntero a estructura File
		int32 PosIni;			//
		int32 PosFin;
		int32 Linea;
		char Caracter;
		PMString CadenaAviso;
		CString CSCadenaAviso;
		int32 NumCaracter;
		int32 ANSCCICaranter;
		int32 numCampo;
		int32 NumCampoAGuardar;
		char *CadenaCampo;
		UIDRef fPageUIDRef;		// Remembers which page we were on
		unsigned char fStep;	// Indicates how many frames we've created this round.

			
		PMString NumIDFrame;	//Numero de rectangulo
		PMString Ruta;	//Coordenas
		PMString TimeCreacionArchivo;//Tiempo de creacion del archivo

		PMString FechaEdicion;
		PMString PaginaFluida;
		
		int32 num_rectangulos;
		
		/** Collection of import providers that can handle a format.
		*/
		Preferencias Prefer;
};


//DEFINE_HELPER_METHODS(A2PFluirFunction)

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(A2PFluirFunction, kA2PImpFluirFunctionImpl)


/* A2PFluirFunction Constructor
*/
A2PFluirFunction::A2PFluirFunction(IPMUnknown* boss) :CPMUnknown<IA2PFluirFunction>(boss)
{
}

/* A2PFluirFunction Destructor
*/
A2PFluirFunction::~A2PFluirFunction(void)
{
}


bool16 A2PFluirFunction::Fluir()
{
	NodeIDList ListaNodosSelect;//Declaracion de la Lista de nodos seleccionados
	int32 NumerodeAviso;//variable util en ciclo					
	PMString Date="";
	PMString TextoUpdate="";;
	int32 NumberPagina=0;
	PMString FechaStandart="";
	do 
	{
				
				//if(!Obtener_MacAddressReg_Y_ComparaCMacAddress()) //if(!LlaveCorrecta())//
				InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
				(
					kA2PPreferenciasFuncionesBoss,	// Object boss/class
					IA2PPreferenciasFunctions::kDefaultIID
				)));
				if(A2PPrefeFuntions==nil)
					break;
				
				
				//
				if(!A2PPrefeFuntions->VerificaSeguridadPirateria())
					break;
			
				
				////Busqueda del ControlView del arbol

				num_rectangulos=0;
				IControlView* treeWidget = PnlTrvUtils::GetWidgetOnPanel(kA2PImpPanelWidgetID,kA2PImpTreeViewWidgetID);
				ASSERT(treeWidget);
				if(!treeWidget)
				{
					//CAlert::InformationAlert("No se encontro el ControlView del arbol");
					break;
				}
				
				InterfacePtr<ITreeViewController> controller(treeWidget,UseDefaultIID());
				ASSERT(controller);
				if(!controller) 
				{
					//CAlert::InformationAlert("No se encontro el arbol");
					break;
				}
				
				controller->GetSelectedItems(ListaNodosSelect);//obtencion de la lista de los nodos seleccionados
		
				if(ListaNodosSelect.size()>0) ///si la cantidad de noods seleccionados es mayor a cero
				{	
					if(SoloUnaSeccion(ListaNodosSelect)==kTrue)//si solo se selecciono un nodos o una seccion
					{
						
						//obtencion de la direccion de o path del archivo a importar
						DireccionPMString=DireccionArchivo();

						//limpia la variable el nombre de la seccion a fluir
						NomSeccionAFluir.Clear();

						///obtengo el nombre de la seccion a fluir
						NomSeccionAFluir=ObtenerNombreSeccionAFluir(ListaNodosSelect);

						
						//llenado de estructura preferencias
					/*	InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
						(
							kRegEditUtilitiesBoss,	// Object boss/class
							IRegEditUtilities::kDefaultIID
						)));


					*/
						
						InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
						(
							kA2PPreferenciasFuncionesBoss,	// Object boss/class
							IA2PPreferenciasFunctions::kDefaultIID
						)));

						if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
						{
							CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
							break;
						}
						
						Prefer.SepEnCoordenadas.Remove(0,1);
						
						///fModel se utiliza para usar las funciones de lectura y escritura de los archivos de preferencias
						//NumAvisos=FModel.CantidadAvisos(DireccionPMString);
						

						//StructAvisos Avisos llenad de todos los avisos en la estructura
						InterfacePtr<IA2PAvisos> A2PAVISOS(static_cast<IA2PAvisos*> (CreateObject
						(
							kA2PAvisosBoss,	// Object boss/class
							IA2PAvisos::kDefaultIID
						)));
		
		
		
						if(A2PAVISOS==nil)
						{
							CAlert::InformationAlert("Salio por eeror de conf");
							break;
						}
		
						PMString PathDAvisos=A2PAVISOS->ArchivoDAvisos();
		
		
		
		
						if(PathDAvisos.NumUTF16TextChars()<=0)
						{		
							//CAlert::InformationAlert("No tiene avisos1");
							if(DireccionPMString.NumUTF16TextChars()<=0)
							{
								break;
							}
							else
							{
								if(!A2PAVISOS->CargarAvisos(DireccionPMString,kFalse))
								{
						
									break;
								}
								else
								{
									NumAvisos=A2PAVISOS->CantidadDAvisos();
					
								}
							}
						}
						else
						{
							NumAvisos=A2PAVISOS->CantidadDAvisos();
							if(NumAvisos<=0)
							{
								//CAlert::InformationAlert("No tiene avisos");
								if(DireccionPMString.NumUTF16TextChars()<=0)
								{
									break;
								}
								else
								{
									if(!A2PAVISOS->CargarAvisos(DireccionPMString,kFalse))
									{
						
										break;
									}
									else
										NumAvisos=A2PAVISOS->CantidadDAvisos();
								}
							}
						}
					
					struct StructAvisos *Avisos=new StructAvisos[NumAvisos];
		
					A2PAVISOS->CopiasAvisosCargados(Avisos);
					
					//StructAvisos *Avisos=new StructAvisos[NumAvisos];
					//LlenarStructAvisos(Avisos,DireccionPMString,NumAvisos,Prefer);							

						
					//OBTENCION DE AVISOS DE LA SECCION//
					//numero de avisos a fluir//
					NumAvisoSec = NumeroHijos(ListaNodosSelect);				
					StructAvisos *AvisosAFluir = new StructAvisos[NumAvisoSec];
						

					//obtencion de los avisos a fluir//
					ObtenerAvisosDeSeccion(Avisos,AvisosAFluir,NomSeccionAFluir,NumAvisos, NumAvisoSec);
						

					/*//	Ads2Page y News2Page
						//Obtiene la fecha del regEdit si existe entonces se establece esta para la fluida
						//si no se obtiene una nueva
						PMString FechaEnRegEdit="";
						FechaEnRegEdit.Append(ConsultaRegEdit-> QueryKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows NT\\Drives32\\IDCSLogInLogOut"), TEXT("Date")));
						if(FechaEnRegEdit.WCharLength()<3)
						{//no existe fecha se establece una nueva
							FechaEdicion=FechaYHora();
							FechaEdicion.Remove(0,FechaEdicion.IndexOfWChar(44)+2);
						}
						else
						{//se establece para la fluida
							FechaEdicion=FechaEnRegEdit;
						}*/
						
						
						
					
						
						
						//FechaEdicion.Remove(0,FechaEdicion.IndexOfWChar(44)+2);

						//Ads2Page y News2Page
						//obtiene el nombre de la pagina del regedit si existe se establece para la fluida
						//si no se coloca como nombre de la pagina el nombre de la seccion que se esta fluyendo
						/*PMString PagInRegEdit="";
						PagInRegEdit.Append(ConsultaRegEdit-> QueryKey(HKEY_LOCAL_MACHINE, TEXT("SOFTWARE\\Microsoft\\Windows NT\\Drives32\\IDCSLogInLogOut"), TEXT("Pagina")));
						if(PagInRegEdit.NumUTF16TextChars()<1)
						{//no existe se establece el nombre que se esta fluyendo
							PaginaFluida=NomSeccionAFluir;
						}
						else
						{//se establkece el nombre de la pagina del reg Edit
							PaginaFluida=PagInRegEdit;
						}*/
						
					/////////////////////////////////////////////////// 
					//	Ads2Page 2.0.12 conjunto de News2Page
					////////////////////////////////////////////////////	
						

						//FechaEdicion=ArmaFecha(FechaEdicion);
						PMString DateIssue="";
						PMString TipoPagina="";
						this->ObtenerFechaPublicacionDeAvisos(Prefer,AvisosAFluir,0,DateIssue);	//Obtiene la fecha de la publicacion del aviso
						if(this->ObtenerTipoPaginaDeAvisos(Prefer,AvisosAFluir,0,TipoPagina)==kFalse)		//Obtiene el tipo de pagina en que se fluira el aviso
							TipoPagina="";		//Obtiene el tipo de pagina en que se fluira el aviso
						
						if(DateIssue.NumUTF16TextChars()<=0)
						{
							DateIssue = FechaYHora(FechaStandart);
						}
						else
						{
							FechaStandart = DateIssue;
						}
						
						
						
						PaginaFluida=NomSeccionAFluir;
						/*if(Prefer.AbrirDialogoIssue==kTrue)
						{
							
							//this->ShowThePageByNumberPage(NumberPagina);
							this->Abrir_DlgGetFechaPag(FechaEdicion,PaginaFluida,NumberPagina, FechaStandart);
						}*/
						
						
						
						
						
						
						
						////////////////////////////////////////////////////////// 
						//////////////////////////////////////////////////////////
						//////////////////////////////////////////////////////////
						
						bool16 UtilizoPlantillaBD=kFalse;
						
						
						
						/*if(Prefer.UtilizaPlantilleroPref==kTrue)
						{
							//Para Utilizar el plantillero
						
							InterfacePtr<IPLTROImport> PlantilleroInterface(static_cast<IPLTROImport*> (CreateObject
							(
								kPlantilleroImpImportacionBoss,	// Object boss/class
								IPLTROImport::kDefaultIID
							)));
							if(PlantilleroInterface==nil)
							{
								//Para cuando no se desea abrir las plantillas
								//Puede ser que no se encuentre instalado el plugin plantillero
								CAlert::InformationAlert(kA2PImpInstallPnPlantilleroStringKey);
							}
							else
							{
								
								
								
								if(!PlantilleroInterface->VerificaSeguridadPirateria())
								{
									break;
								}
								

								this->Abrir_DlgGetFechaPag(FechaEdicion,PaginaFluida,NumberPagina, FechaStandart,kTrue);
								
								if(FechaEdicion=="?" && PaginaFluida=="?")
									break;
								
								//sacar el mayor rigth para determinar si se debe abrir una doble pagina o una normal
								K2Vector<PMReal> vectorRightCoor;
								PMString Coordenadas="";
							
							
							
								for(NumerodeAviso=0;NumerodeAviso<NumAvisoSec;NumerodeAviso++)
								{
							
									this->GetPMStringCoordinatesIssueOdAds(Prefer,AvisosAFluir, NumerodeAviso, Coordenadas);
									//determina las coordenadas del rectangulo
									PMReal left,top,right,bottom;
									this->CoordenadasDeImagen(Prefer,left,top,right,bottom,Coordenadas);//izquierda,tope, derecha,bajo
	
									left=ConvertirCoorAPuntos(left,Prefer.UnidadesEnLeft);
									top=ConvertirCoorAPuntos(top,Prefer.UnidadesEnTop);
									right=ConvertirCoorAPuntos(right,Prefer.UnidadesEnRight);
									bottom=ConvertirCoorAPuntos(bottom,Prefer.UnidadesEnButtom);
									
									vectorRightCoor.Append(right);
								}
							
							
							
								std::sort(vectorRightCoor.begin(),vectorRightCoor.end());
							
							
		
								PMReal MasAlaDerecha = vectorRightCoor.Last();
		
		
								FechaEdicion =DateIssue;
								//FechaEdicion = ArmaFecha(DateIssue);//FechaStandart);//
							
		
								//if(Prefer.NombreEditorial.NumUTF16TextChars()>0)
								//	FechaEdicion=Prefer.NombreEditorial+" "+FechaEdicion;
							
							
		
								//abrir plantillas
								UtilizoPlantillaBD=PlantilleroInterface->ProcesarPlantilla(FechaStandart,PaginaFluida,MasAlaDerecha,TipoPagina,Prefer.NombreEditorial);
		
							}
						}*/
						
						
						
							
						if(!UtilizoPlantillaBD)
						{
							if(!CrearDocumento(Prefer))
							{
								break;
							}
						}
							
						if(!UtilizoPlantillaBD)
						{
							if(Prefer.AbrirDialogoIssue==kTrue)
							{
								if(this->Abrir_DlgGetFechaPag(FechaEdicion,PaginaFluida,NumberPagina, FechaStandart,kFalse)==kTrue)
									this->ShowThePageByNumberPage(NumberPagina);
							}
						}
						
						
							
							
						/////funcion para la creacion del documento
						//{
							//Observador del documento que se encuentra en frente para borra capas cuando se guarde la pagina
							//InterDemoDocWatchUtils::StartDocResponderMode();
							
							
/*							PnlTrvUtils::GetXMPVar("N2PDate",FechaStandart);
				
							if(FechaStandart.NumUTF16TextChars()<=0)
								FechaEdicion = FechaYHora(FechaStandart);
							else
								//Convertir a cadena
								FechaEdicion=ArmaFecha(FechaStandart);
								
							PnlTrvUtils::GetXMPVar("N2PPagina",PaginaFluida);
							if(PaginaFluida.NumUTF16TextChars()<=0)
								PaginaFluida=NomSeccionAFluir;
						
							
							
							
							
							Date=FechaEdicion;
							//para salir de la funcion sin fluir
*/							
							
							
							
							
							
							if(!CreaciondeCapas(Prefer))
							{
								CAlert::InformationAlert(kA2PImpA2PImpNoCapasSobrePrefStringKey);
								break;
							}
							
							
							if(!UtilizoPlantillaBD)
							{
								if(Prefer.AbrirDialogoIssue==kTrue)
								{
									PaginaFluida.Append(" ");
									PaginaFluida.AppendNumber(NumberPagina);
									PMString dd="";
									PMString mm="";
									PMString aaaa="";
		
									if(FechaStandart.NumUTF16TextChars()>0)
										FechaStandart =InterlasaUtilities::ArmaFecha(FechaStandart,"Spanish",aaaa,mm,dd);
									this->Aplica_Fecha_Pagina(Prefer,FechaStandart,PaginaFluida);
								}
							}
							
								
							
							///////////////////////////////////////////////////////////////
							UIDRef refDoc(::GetUIDRef(Utils<ILayoutUIUtils>()->GetFrontDocument()));
							InterfacePtr<IDocument> myDoc(refDoc,IID_IDOCUMENT);
							IDataBase *db=refDoc.GetDataBase();
							
							//crea colores que utiliza Ads2Page 2.0
							crearColores();
							

							//crea las capas dependiendo preferencias
							
							
							//Toma el texto generado por la o las fluidas anteriores
							PnlTrvUtils::GetXMPVar("A2PTextUpdate",TextoUpdate);
							
							//ciclo para fluir los nodos hijos
							for(NumerodeAviso=0;NumerodeAviso<NumAvisoSec;NumerodeAviso++)
							{
								//si no se existe la capa imagen entonces se fluye sobre la capa update
							
								
								if(!ActivarCapa("Imagen"))
									if(!ActivarCapa("Editorial"))
										if(!ActivarCapa("Etiquetas C/Publicidad"))
											ActivarCapa("Etiquetas S/Publicidad");
											
								//crea rectangulo de imagen. Dentro de esta imagen se hacen los diferentes
								//llamadas a otras funciones para la creacion de etiquetas
								
								
								num_rectangulos=1;
								
								
								CrearRectanguloImagen(Prefer,AvisosAFluir,NumerodeAviso);
								
								
							//	TextoUpdate.Append(NumIDFrame);			//Adiciona el Numero de rectangulo
							//	TextoUpdate.Append(",");
							//	TextoUpdate.Append(Ruta);				//Adiciona la ruta
							//	TextoUpdate.Append(",");
							//	TextoUpdate.Append(TimeCreacionArchivo);//Tiempo de creacion del archivo
							//	TextoUpdate.Append("\n");
								
								
								TextoUpdate.Append("UIDFrameToImage:");
								TextoUpdate.AppendNumber(AvisosAFluir[NumerodeAviso].UIDFrameToImage);
								TextoUpdate.Append(",");
								TextoUpdate.Append("PathToImage:");
								TextoUpdate.Append(AvisosAFluir[NumerodeAviso].PathToImage);
								TextoUpdate.Append(",");
								TextoUpdate.Append("LastTimeImage:");
								TextoUpdate.Append(AvisosAFluir[NumerodeAviso].LastTimeImage);
								
								TextoUpdate.Append(",");
								TextoUpdate.Append("ExistImageFile:");
								TextoUpdate.AppendNumber(AvisosAFluir[NumerodeAviso].ExistImageFile);
								TextoUpdate.Append(",");
								TextoUpdate.Append("UIDFrameEtiqueta:");
								TextoUpdate.AppendNumber(AvisosAFluir[NumerodeAviso].UIDFrameEtiqueta);
								TextoUpdate.Append(",");
								
								TextoUpdate.Append("UIDPageFlowed:");
								TextoUpdate.AppendNumber(AvisosAFluir[NumerodeAviso].UIDPageFlowed);
								TextoUpdate.Append(",");
								
								
								TextoUpdate.Append("CordenadasFrameToImage:");
								TextoUpdate.AppendNumber(AvisosAFluir[NumerodeAviso].LFrameImage);
								TextoUpdate.Append("/");
								TextoUpdate.AppendNumber(AvisosAFluir[NumerodeAviso].TFrameImage);
								TextoUpdate.Append("/");
								TextoUpdate.AppendNumber(AvisosAFluir[NumerodeAviso].RFrameImage);
								TextoUpdate.Append("/");
								TextoUpdate.AppendNumber(AvisosAFluir[NumerodeAviso].BFrameImage);	
								
								TextoUpdate.Append("\n");		
							}

							
							//creacion del textupdate
							
							PnlTrvUtils::SaveXMPVar("A2PTextUpdate",TextoUpdate);
							TextoUpdate.Clear();//

						
							
						
							///Bloqueo de Capas	
							this->AplicaPreferenciasACapas();
							
							
							
							//adiciona el UID donde fue fluida la seccion
							PMString TextUpdateOnXMP="";
							PnlTrvUtils::GetXMPVar("A2PPathLastSectionImported",TextUpdateOnXMP);
							
							
							PMString NewPagUIDAndSecction="";
							NewPagUIDAndSecction.AppendNumber(AvisosAFluir[NumerodeAviso-1].UIDPageFlowed);
							NewPagUIDAndSecction.Append(",");
							NewPagUIDAndSecction.Append(NomSeccionAFluir);
							
							
							if(TextUpdateOnXMP.NumUTF16TextChars()>0)
							{
								bool16 YaSehaFluyoEstaSeccion=kFalse;
								int32 numItem=1;
					
								PMString *Item=TextUpdateOnXMP.GetItem("\n",numItem);
								while(Item!=nil && YaSehaFluyoEstaSeccion==kFalse)
								{
							
									PMString Luna(Item->GrabCString());
							
									if(Luna==NewPagUIDAndSecction)
									{
									
										YaSehaFluyoEstaSeccion=kTrue;
									}
									numItem++;
									Item=TextUpdateOnXMP.GetItem("\n",numItem);
								}
								
								
								if(YaSehaFluyoEstaSeccion==kFalse)
								{
									NewPagUIDAndSecction.Append("\n");
									TextUpdateOnXMP.Append(NewPagUIDAndSecction);
								}
							}
							else
							{
								NewPagUIDAndSecction.Append("\n");
								TextUpdateOnXMP.Append(NewPagUIDAndSecction);
							}
							
							
							
							
							
							
							
							
							PnlTrvUtils::SaveXMPVar("A2PPathLastFileImported",DireccionPMString);
							PnlTrvUtils::SaveXMPVar("A2PPathLastSectionImported",TextUpdateOnXMP);
						
						
				PMString 	LastModifFileImported(A2PPrefeFuntions->ProporcionaFechaArchivo(DireccionPMString.GrabCString()));
						
							PnlTrvUtils::SaveXMPVar("A2PDateLAstFileImported", LastModifFileImported);
							
							
							
							if(NumberPagina==1)
							{
								PnlTrvUtils::SaveXMPVar("A2PFolioDPagina",PaginaFluida);
								PnlTrvUtils::SaveXMPVar("A2PFechaEdicionDPagina",FechaStandart);
							}
							
							//libera memoria 
							delete AvisosAFluir;	
							delete Avisos;		

						//}
						CAlert::InformationAlert(kProcesoFinalizadoStringKey);
					}
				}
				else
				{
					//en caso de que se encuentre ningun nodo seleccionado
					CAlert::InformationAlert(kSeleccionarPaginaStringKey);
				}
			} while(kFalse);
	return(kTrue);
}

void A2PFluirFunction::BorrarCapas()
{
	if(ActivarCapa("Imagen"))
	{
		BorrarFrontLayer();
	}
	/*if(ActivarCapa("Editorial"))
	{
		BorrarFrontLayer();
	}*/
	if(ActivarCapa("Etiquetas C/Publicidad"))
	{
		BorrarFrontLayer();
	}
	if(ActivarCapa("Etiquetas S/Publicidad"))
	{
		BorrarFrontLayer();
	}
}

void A2PFluirFunction::BorrarFrontLayer()
{
	do
	{
			// We want to delete the active layer, if it's not the page layer:
		InterfacePtr<ILayoutControlData>	layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutControlData == nil)
		{
			ASSERT_FAIL("DelLayerButtonWidgetObserver::Update: Cannot get layoutControlData");
			
		}
		else
		{
			// Get the active layer from the ILayoutControlData interface:
			InterfacePtr<IHierarchy>	layerHier(layoutControlData->QueryActiveLayer());
			
			// Now we need the spread layer:
			InterfacePtr<ISpreadLayer> spreadLayer(::GetUIDRef(layerHier), UseDefaultIID());
			if (spreadLayer == nil)
			{
				ASSERT_FAIL("DelLayerButtonWidgetObserver::Update: spreadLayer is invalid");
				
			}
			else
			{
				// From the spread layer we can get the document layer:
				InterfacePtr<IDocumentLayer> docLayer(spreadLayer->QueryDocLayer());

				// Create the UIDList for the command:
				UIDRef docLayerRef = (::GetUIDRef(docLayer));
				UIDList layerList(docLayer);
				
				// Create the deleteLayerCmd, set the item list with the active layer, process:
				InterfacePtr<ICommand>	deleteLayerCmd(CmdUtils::CreateCommand(kDeleteLayerCmdBoss));
				if (deleteLayerCmd == nil)
				{
					ASSERT_FAIL("DelLayerButtonWidgetObserver::Update: deleteLayerCmd is invalid");
					
				}
				else
				{
					deleteLayerCmd->SetItemList(layerList);
					
					// Process command and report error if it fails:
					ErrorCode error = CmdUtils::ProcessCommand(deleteLayerCmd);
					if (error != kSuccess)
					{
						ASSERT_FAIL("DelLayerButtonWidgetObserver::Update: deleteLayerCmd is invalid");
					}
				}
			}
		}
	}while(false);
}


bool16 A2PFluirFunction::ActualizarDocumento()
{
	
	int32 NumerodeAviso;			//variable util en ciclo	
	PMString Date="";
	PMString FechaStandart="";
	PMString DireccionPMString="";
	PMString NomSeccionAFluir="";		//Nombre de la seccion que se va a fluir
	PMString LastModifFileImported="";
	PMString CadenaSecionesXPagina="";
	int32 UIDNumberPage=0;					//UID de Pagida donde sera fluido la seccion
	PMString TextoUpdate="";
	PnlTrvUtils::GetXMPVar("A2PPathLastFileImported",DireccionPMString);
	PnlTrvUtils::GetXMPVar("A2PPathLastSectionImported",CadenaSecionesXPagina);
	PnlTrvUtils::GetXMPVar("A2PDateLAstFileImported",LastModifFileImported);

	PnlTrvUtils::SaveXMPVar("A2PPathLastFileImported","");
	PnlTrvUtils::SaveXMPVar("A2PPathLastSectionImported","");
	PnlTrvUtils::SaveXMPVar("A2PDateLAstFileImported","");
	
	
	do
	{
		
		
				
		InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
		(
			kA2PPreferenciasFuncionesBoss,	// Object boss/class
			IA2PPreferenciasFunctions::kDefaultIID
		)));
		
		if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
		{
			CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
			break;
		}
		
		Prefer.SepEnCoordenadas.Remove(0,1);
		
		///fModel se utiliza para usar las funciones de lectura y escritura de los archivos de preferencias
		
		InterfacePtr<IA2PAvisos> A2PAVISOS(static_cast<IA2PAvisos*> (CreateObject
		(
			kA2PAvisosBoss,	// Object boss/class
			IA2PAvisos::kDefaultIID
		)));
		
		
		if(A2PAVISOS==nil)
		{
			CAlert::InformationAlert("Salio por eeror de conf");
			break;
		}
						
		NumAvisos=A2PAVISOS->CantidadDAvisos();
		if(NumAvisos<=0)
		{
			if(!A2PAVISOS->CargarAvisos(DireccionPMString,kFalse))
			{
				break;
			}
			else
				NumAvisos=A2PAVISOS->CantidadDAvisos();
		}
		
		struct StructAvisos *Avisos=new StructAvisos[NumAvisos];
		
		A2PAVISOS->CopiasAvisosCargados(Avisos);
		
		//NumAvisos =   FModel.CantidadAvisos(DireccionPMString);
	
	
	
		//StructAvisos Avisos llenad de todos los avisos en la estructura
		//StructAvisos *Avisos=new StructAvisos[NumAvisos];
		//LlenarStructAvisos(Avisos,DireccionPMString,NumAvisos,Prefer);							
		
		this->DELETE_AvisosYEtiquetasParaUpdateGeometria();
		
		
		PnlTrvUtils::RemoveXMPVar("A2PTextUpdate");
		//OBTENCION DE AVISOS DE LA SECCION//
		//numero de avisos a fluir//
		do
		{	
			
			UIDNumberPage = CadenaSecionesXPagina.GetAsNumber();
			CadenaSecionesXPagina.Remove( 0 , (CadenaSecionesXPagina.IndexOfString(",")+1) );
			
			
			NomSeccionAFluir = CadenaSecionesXPagina.Substring(0,(CadenaSecionesXPagina.IndexOfString("\n")))->GrabCString();
			CadenaSecionesXPagina.Remove( 0 , (CadenaSecionesXPagina.IndexOfString("\n")+1) );
		//plantillero
			PMString StrSeccion="";
			PMString StrNumPage="";
			
			PMString *TokensOnElemnt = NomSeccionAFluir.GetItem(" ",1);
			PMString strSubString = NomSeccionAFluir;
			int32 nCountTags = 0;
			bool16 sale = kFalse;
			if(TokensOnElemnt)
			{
				StrSeccion= *TokensOnElemnt;
				if(strSubString.CharCount() > StrSeccion.CharCount()+1)
				{
					PMString *pSubstring = strSubString.Substring(StrSeccion.CharCount()+1);
					StrNumPage = *pSubstring;
				}
				
			}
			
			
		//Termina fincion de plantillero	
		
			if(ShowThePageByUIDPage(UIDNumberPage))
			{
				
				
				
				
				
				
				/////////////////////////////////////////
				NumAvisoSec = this->ObtenerCantAvisosDeSeccion(Avisos, NomSeccionAFluir, NumAvisos);	
				
				StructAvisos *AvisoAFluir=new StructAvisos[NumAvisoSec];
				
				//obtencion de los avisos a fluir//
				ObtenerAvisosDeSeccion(Avisos,AvisoAFluir,NomSeccionAFluir,NumAvisos, NumAvisoSec);
				
				///////////////////////////////////////////
				
				PMString DateIssue="";
				PMString TipoPagina="";
				this->ObtenerFechaPublicacionDeAvisos(Prefer,AvisoAFluir,0,DateIssue);	//Obtiene la fecha de la publicacion del aviso
				if(this->ObtenerTipoPaginaDeAvisos(Prefer,AvisoAFluir,0,TipoPagina)==kFalse)		//Obtiene el tipo de pagina en que se fluira el aviso
					TipoPagina="";		//Obtiene el tipo de pagina en que se fluira el aviso
				FechaEdicion=DateIssue;
				
				bool16 UtilizoPlantillaBD=kFalse;
				//Para Utilizar el plantillero
						
				/*InterfacePtr<IPLTROImport> PlantilleroInterface(static_cast<IPLTROImport*> (CreateObject
				(
					kPlantilleroImpImportacionBoss,	// Object boss/class
					IPLTROImport::kDefaultIID
				)));
			
				if(PlantilleroInterface==nil)
				{
					//Para cuando no se desea abrir las plantillas
							
				}
				else
				{
					PlantilleroInterface->FluirElementosPlantillero( FechaEdicion, StrSeccion, StrNumPage ,TipoPagina, Prefer.NombreEditorial);
				}*/
				
				/////////////////////////////////////////////
				
				PnlTrvUtils::GetXMPVar("N2PDate",FechaEdicion);
				
				if(FechaEdicion.NumUTF16TextChars()<=0)
					FechaEdicion = FechaYHora(FechaStandart);
				
				PaginaFluida=NomSeccionAFluir;
			
				
				
				Date=FechaEdicion;
				//para salir de la funcion sin fluir
				if(FechaEdicion=="?" && PaginaFluida=="?")
					break;
				//FechaEdicion=ArmaFecha(FechaEdicion);
						
				
				///////////////////////////////////////////////////////////////
				UIDRef refDoc(::GetUIDRef(Utils<ILayoutUIUtils>()->GetFrontDocument()));
				
				
				InterfacePtr<IDocument> myDoc(refDoc,IID_IDOCUMENT);
				
				
				IDataBase *db=refDoc.GetDataBase();
			
				
			
				//crea colores que utiliza Ads2Page 2.0
				this->crearColores();
			
				
				//crea las capas dependiendo preferencias
				if(!CreaciondeCapas(Prefer))
				{
					CAlert::InformationAlert("Los anuncios no fueron colocados por que se selecciono ninguna capa en preferencias");
					break;
				}
			
				PnlTrvUtils::GetXMPVar("A2PTextUpdate",TextoUpdate);
				
				//ciclo para fluir los nodos hijos
				for(NumerodeAviso=0;NumerodeAviso<NumAvisoSec;NumerodeAviso++)
				{
					//si no se existe la capa imagen entonces se fluye sobre la capa update
					//	CAlert::WarningAlert("13");

					if(!ActivarCapa("Imagen"))
						if(!ActivarCapa("Editorial"))
							if(!ActivarCapa("Etiquetas C/Publicidad"))
								ActivarCapa("Etiquetas S/Publicidad");
											
					//crea rectangulo de imagen. Dentro de esta imagen se hacen los diferentes
					//llamadas a otras funciones para la creacion de etiquetas
						
								
					num_rectangulos=1;
					
					CrearRectanguloImagen(Prefer,AvisoAFluir,NumerodeAviso);
											
					
								
								
					TextoUpdate.Append("UIDFrameToImage:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].UIDFrameToImage);
					TextoUpdate.Append(",");
					TextoUpdate.Append("PathToImage:");
					TextoUpdate.Append(AvisoAFluir[NumerodeAviso].PathToImage);
					TextoUpdate.Append(",");
					TextoUpdate.Append("LastTimeImage:");
					TextoUpdate.Append(AvisoAFluir[NumerodeAviso].LastTimeImage);
								
					TextoUpdate.Append(",");
					TextoUpdate.Append("ExistImageFile:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].ExistImageFile);
					TextoUpdate.Append(",");
					TextoUpdate.Append("UIDFrameEtiqueta:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].UIDFrameEtiqueta);
					TextoUpdate.Append(",");
								
					TextoUpdate.Append("UIDPageFlowed:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].UIDPageFlowed);
					TextoUpdate.Append(",");
								
								
					TextoUpdate.Append("CordenadasFrameToImage:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].LFrameImage);
					TextoUpdate.Append("/");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].TFrameImage);
					TextoUpdate.Append("/");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].RFrameImage);
					TextoUpdate.Append("/");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].BFrameImage);	
								
					TextoUpdate.Append("\n");	
				}
				//libera memoria 
			
			
				//creacion del textupdate
					
				PnlTrvUtils::SaveXMPVar("A2PTextUpdate",TextoUpdate);
				TextoUpdate.Clear();//

						
				
						
				///Bloqueo de Capas	
				this->AplicaPreferenciasACapas();
							
							
				
				//adiciona el UID donde fue fluida la seccion
				
				PMString TextUpdateOnXMP="";
				PnlTrvUtils::GetXMPVar("A2PPathLastSectionImported",TextUpdateOnXMP);
							
				PMString NewPagUIDAndSecction="";
				NewPagUIDAndSecction.AppendNumber(AvisoAFluir[NumerodeAviso-1].UIDPageFlowed);
				NewPagUIDAndSecction.Append(",");
				NewPagUIDAndSecction.Append(NomSeccionAFluir);
				
							
				if(TextUpdateOnXMP.NumUTF16TextChars()>0)
				{
					bool16 YaSehaFluyoEstaSeccion=kFalse;
					int32 numItem=1;
					
					PMString *Item=TextUpdateOnXMP.GetItem("\n",numItem);
					while(Item!=nil && YaSehaFluyoEstaSeccion==kFalse)
					{
						
						PMString Luna(Item->GrabCString());
							
						if(Luna==NewPagUIDAndSecction)
						{
								
							YaSehaFluyoEstaSeccion=kTrue;
						}
						numItem++;
						Item=TextUpdateOnXMP.GetItem("\n",numItem);
					}
								
								
					if(YaSehaFluyoEstaSeccion==kFalse)
					{
						NewPagUIDAndSecction.Append("\n");
						TextUpdateOnXMP.Append(NewPagUIDAndSecction);
					}
				}
				else
				{
					NewPagUIDAndSecction.Append("\n");
					TextUpdateOnXMP.Append(NewPagUIDAndSecction);
				}
							
				
				PnlTrvUtils::SaveXMPVar("A2PPathLastFileImported",DireccionPMString);
				PnlTrvUtils::SaveXMPVar("A2PPathLastSectionImported",TextUpdateOnXMP);
				PMString 	LastModifFileImported(A2PPrefeFuntions->ProporcionaFechaArchivo(DireccionPMString.GrabCString()));

				PnlTrvUtils::SaveXMPVar("A2PDateLAstFileImported", LastModifFileImported);
				
			
				//libera memoria 
				//delete AvisoAFluir;	
			
				//delete Avisos;		
				
			
													
			}
			else
			{
				CAlert::ErrorAlert("Se ha borrado la Pagina donde se fluyo la seccion.");
			}
			
		}while(CadenaSecionesXPagina.NumUTF16TextChars()>0);//la cadena de Secciones por pagina tenga caracteres
	}while(false);
	return(kTrue);
}



/*
			Valida que se haya seleccionada unicamente una seccion del arbol. Regresa una variable del tipo boolean
			
			  @parametro NodosSeleccionados especifica los nodos se encuentran seleccionados en el arbol. Es del tipo NodoIDList que es la lista de todos los nodos seleccionados, pero en este caso unicamente contendra un nodo
					el nodo de la seccion o pagina.
*/
bool16 A2PFluirFunction::SoloUnaSeccion(NodeIDList& ListaNodosSelect)
{
	int32 NumeroDeNodosSeleccionados=ListaNodosSelect.size();//cantidad de nodos seleccionados 
	int32 Nodo;//util en ciclo
	int32 ContadorDeNodosSeccion=0;//la catidad de nodos seccion seleccionados
	do
	{
		IControlView* treeWidget = PnlTrvUtils::GetWidgetOnPanel(kA2PImpPanelWidgetID,kA2PImpTreeViewWidgetID);
		ASSERT(treeWidget);
		if(!treeWidget)
		{
			//CAlert::InformationAlert("No se encontro el ControlView del arbol");
			break;
		}
					
		InterfacePtr<ITreeViewHierarchyAdapter>	iTreeViewHry(treeWidget,UseDefaultIID());
		// Be sure we get the right panel control data; we don't want the one on the
		// tree but on it's parent, a panel
		ASSERT(iTreeViewHry);
		if(!iTreeViewHry) 
		{
			//CAlert::InformationAlert("5");
			break;
		}
		for(Nodo=0;Nodo<NumeroDeNodosSeleccionados;Nodo++)
		{	
			int32 NumHijos=iTreeViewHry->GetNumChildren(ListaNodosSelect[Nodo]);
			if(NumHijos>0)
			{//Es un nodo que contiene el nombre de la seccion o nodo padre
				ContadorDeNodosSeccion++;		
				if(ContadorDeNodosSeccion>1)//si la cantinas de nodos seccion es mayor a un 
				{
					return(kFalse);
				}
			}
			else
			{//Es un nodo que no contiene hijos por o tanto no es una seccion
				CAlert::ErrorAlert(kA2PImpMsgErrorAlertNoSeccionNodoStringKey);
				return(kFalse);
			}
		}
	}while(false);
	return(kTrue);
}

/**
	llamada para obtener el nombre de la pagina o seccion seleccionado.Regrea una variable del Tipo PMString
			
	  @parametro	NodosSeleccionados especifica los nodos se encuentran seleccionados en el arbol. Es del tipo NodoIDList que es la lista de todos los nodos seleccionados, pero en este caso unicamente contendra un nodo
		el nodo de la seccion o pagina.

*/

PMString A2PFluirFunction::ObtenerNombreSeccionAFluir(NodeIDList& ListaNodosSelect)
{
	PMString NomSeccion="";//Nombre de Seccion
	int32 NumeroDeNodosSeleccionados=ListaNodosSelect.size();//Cantidad de nodos seleccionados
	int32 NumNodo;//util en el ciclo
	int32 NumNodosHijos;
	do
	{
		
		IControlView* treeWidget = PnlTrvUtils::GetWidgetOnPanel(kA2PImpPanelWidgetID,kA2PImpTreeViewWidgetID);
		ASSERT(treeWidget);
		if(!treeWidget)
		{
			//CAlert::InformationAlert("No se encontro el ControlView del arbol");
			break;
		}
		
		InterfacePtr<ITreeViewHierarchyAdapter>	iTreeViewHry(treeWidget,UseDefaultIID());
		// Be sure we get the right panel control data; we don't want the one on the
		// tree but on it's parent, a panel
		ASSERT(iTreeViewHry);
		if(!iTreeViewHry) 
		{
			break;
		}
		for(NumNodo=0;NumNodo<NumeroDeNodosSeleccionados;NumNodo++)
		{	

			NumNodosHijos=iTreeViewHry->GetNumChildren(ListaNodosSelect[NumNodo]);
			if(NumNodosHijos>0)
			{//Es el nodo Seccion que se fluira
				TreeNodePtr<PnlTrvFileNodeID> nodeID(ListaNodosSelect[NumNodo]);
				ASSERT(nodeID);
				if(!nodeID) 
				{
					return("");
				}
				NomSeccion=nodeID->GetPath();			
			}
		}
	}while(false);
	return(NomSeccion);

}

/**	
	Esta funcion obtiene el nombre del nodo padre o raiz el cual debe 
	contener la direccion del archivo	que se debe de importar.Regresa una variable del tipo PMString.
*/
PMString A2PFluirFunction::DireccionArchivo()
{
	PMString PathArchivo="";
	do
	{
		IControlView* treeWidget = PnlTrvUtils::GetWidgetOnPanel(kA2PImpPanelWidgetID,kA2PImpTreeViewWidgetID);
		ASSERT(treeWidget);
		if(!treeWidget)
		{
			//CAlert::InformationAlert("No se encontro el ControlView del arbol");
			break;
		}
					
		
		InterfacePtr<ITreeViewHierarchyAdapter>	iTreeViewHry(treeWidget,UseDefaultIID());
		// Be sure we get the right panel control data; we don't want the one on the
		// tree but on it's parent, a panel
		ASSERT(iTreeViewHry);
		if(!iTreeViewHry) 
		{
			//CAlert::InformationAlert("5");
			break;
		}
		//obtiene el ID del root
		TreeNodePtr<PnlTrvFileNodeID> nodeID(iTreeViewHry->GetRootNode());
		ASSERT(nodeID);
		if(!nodeID) 
		{
			return("");
		}
		PathArchivo=nodeID->GetPath();	
	}while(false);
	return(PathArchivo);
}


/**
	Esta funcion tiene como entrada lista de nodos seleccionados, y su salida sera la cantidad de nodos hijos
	del nodo seleccionado(cantidad de avisos del la pagina o seccion seleccionada).
				
	  @param NodosSeleccionados especifica los nodos se encuentran seleccionados en el arbol. Es del tipo NodoIDList que es la lista de todos los nodos seleccionados, pero en este caso unicamente contendra un nodo
		el nodo de la seccion o pagina.
*/
int32 A2PFluirFunction::NumeroHijos(NodeIDList& ListaNodosSelect)
{
	int32 numHijos=0;//numero de hijos del nodo seleccionado
	int32 NumeroDeNodosSeleccionados=ListaNodosSelect.size();//cantidad de noods padres seleccionados
	int32 Nodo;//
	do
	{
		
		IControlView* treeWidget = PnlTrvUtils::GetWidgetOnPanel(kA2PImpPanelWidgetID,kA2PImpTreeViewWidgetID);
		ASSERT(treeWidget);
		if(!treeWidget)
		{
			//CAlert::InformationAlert("No se encontro el ControlView del arbol");
			break;
		}
					
		
		InterfacePtr<ITreeViewHierarchyAdapter>	iTreeViewHry(treeWidget,UseDefaultIID());
		// Be sure we get the right panel control data; we don't want the one on the
		// tree but on it's parent, a panel
		ASSERT(iTreeViewHry);
		if(!iTreeViewHry) 
		{
			//CAlert::InformationAlert("5");
			break;
		}
		for(Nodo=0;Nodo<NumeroDeNodosSeleccionados;Nodo++)
		{	
			////CAlert::ErrorAlert("fer 1");
			numHijos=iTreeViewHry->GetNumChildren(ListaNodosSelect[Nodo]);
			////CAlert::ErrorAlert("fer 2");
			if(numHijos>0)
			{//Es el nodo Seccion que se fluira
				
				return(numHijos);
				
			}
		}
	}while(false);
	return(numHijos);
}

/**		
	Es llamada para obtener los avisos de la seccion o nodo seleccionado.
			
*/
int32 A2PFluirFunction::ObtenerCantAvisosDeSeccion(StructAvisos *AvisoCompletos,PMString NomSeccion,int32 NumAvisos)
{
	//Buscar Cual Campo contiene el El Nombre de Seccion o pagina
	
	PMString ContenidoCampo;
	ContenidoCampo.Clear();
	int32 NumerodelCampoSeccion=0;
	int32 AvisoCompletoNum=0;
	int32 AvisoFluirNum=0;
	do
	{

		if(AvisoCompletos[0].NomCampo1=="Pagina o seccion" || AvisoCompletos[0].NomCampo1=="Page or section")
		{
			//CAlert::ErrorAlert("0");
			NumerodelCampoSeccion=1;
			break;
		}

		if(AvisoCompletos[0].NomCampo2=="Pagina o seccion" || AvisoCompletos[0].NomCampo2=="Page or section")
		{
			//CAlert::ErrorAlert("1");
			NumerodelCampoSeccion=2;
			break;
		}

		if(AvisoCompletos[0].NomCampo3=="Pagina o seccion" || AvisoCompletos[0].NomCampo3=="Page or section")
		{
			//CAlert::ErrorAlert("2");
			NumerodelCampoSeccion=3;
			break;
		}
		if(AvisoCompletos[0].NomCampo4=="Pagina o seccion" || AvisoCompletos[0].NomCampo4=="Page or section")
		{
			//CAlert::ErrorAlert("3");
			NumerodelCampoSeccion=4;
			break;
		}

		if(AvisoCompletos[0].NomCampo5=="Pagina o seccion" || AvisoCompletos[0].NomCampo5=="Page or section")
		{
			//CAlert::ErrorAlert("4");
			NumerodelCampoSeccion=5;
			break;
		}
		if(AvisoCompletos[0].NomCampo6=="Pagina o seccion" || AvisoCompletos[0].NomCampo6=="Page or section")
		{
			//CAlert::ErrorAlert("5");
			NumerodelCampoSeccion=6;
			break;
		}
		if(AvisoCompletos[0].NomCampo7=="Pagina o seccion" || AvisoCompletos[0].NomCampo7=="Page or section")
		{
			//CAlert::ErrorAlert("6");
			NumerodelCampoSeccion=7;
			break;
		}
		if(AvisoCompletos[0].NomCampo8=="Pagina o seccion" || AvisoCompletos[0].NomCampo8=="Page or section")
		{
			//CAlert::ErrorAlert("7");
			NumerodelCampoSeccion=8;
			break;
		}
		if(AvisoCompletos[0].NomCampo9=="Pagina o seccion" || AvisoCompletos[0].NomCampo9=="Page or section")
		{
			//CAlert::ErrorAlert("8");
			NumerodelCampoSeccion=9;
			break;
		}
		if(AvisoCompletos[0].NomCampo10=="Pagina o seccion" || AvisoCompletos[0].NomCampo10=="Page or section")
		{
			//CAlert::ErrorAlert("9");
			NumerodelCampoSeccion=10;
			break;
		}
		if(AvisoCompletos[0].NomCampo11=="Pagina o seccion" || AvisoCompletos[0].NomCampo11=="Page or section")
		{
			//CAlert::ErrorAlert("10");
			NumerodelCampoSeccion=11;
			break;
		}
		if(AvisoCompletos[0].NomCampo12=="Pagina o seccion" || AvisoCompletos[0].NomCampo12=="Page or section")
		{
			//CAlert::ErrorAlert("11");
			NumerodelCampoSeccion=12;
			break;
		}
		if(AvisoCompletos[0].NomCampo13=="Pagina o seccion" || AvisoCompletos[0].NomCampo13=="Page or section")
		{
			////CAlert::ErrorAlert(AvisoCompletos[0].NomCampo13);
			NumerodelCampoSeccion=13;
			break;
		}
		if(AvisoCompletos[0].NomCampo14=="Pagina o seccion" || AvisoCompletos[0].NomCampo14=="Page or section")
		{
			////CAlert::ErrorAlert(AvisoCompletos[0].NomCampo14);
			
			break;
		}
	}while(false);
	

	/*PMString j="NumAvisos= ";
	j.AppendNumber(NumAvisos);
	CAlert::ErrorAlert(j);
	j="Numero de opcion o campo= ";
	j.AppendNumber(NumerodelCampoSeccion);
	*/
	//CAlert::ErrorAlert(NomSeccion);
	
	
	/*NomSeccion.Shrink(kTrue);
	
	
	PMString j;

	
	for(int i=0;i<NomSeccion.Length()-1;i++)
	{
		j.AppendW(NomSeccion.GetWChar(i));
	}
	
	
	NomSeccion=j;
	*/

	for(AvisoCompletoNum=0;AvisoCompletoNum<NumAvisos ;AvisoCompletoNum++)
	{
		switch(NumerodelCampoSeccion)
		{
		case 1:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo1;
			break;
		case 2:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo2;
			break;
		case 3:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo3;
			break;
		case 4:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo4;
			break;
		case 5:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo5;
			break;
		case 6:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo6;
			break;
		case 7:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo7;
			break;
		case 8:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo8;
			break;
		case 9:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo9;
			break;
		case 10:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo10;
			break;
		case 11:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo11;
			break;
		case 12:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo12;
			break;
		case 13:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo13;
			break;
		case 14:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo14;
			break;
		}
		

	//	CAlert::InformationAlert(ContenidoCampo);
	///Compara la seccion de cada aviso 

	/*	ContenidoCampo.Shrink(kTrue);
		j.Clear();
		for(i=0;i<ContenidoCampo.Length()-1;i++)
		{
			j.AppendW(ContenidoCampo.GetWChar(i));
		}
		ContenidoCampo=j;*/
		
		if(ContenidoCampo.Compare(kTrue,NomSeccion)==0 || ContenidoCampo==NomSeccion)
		{
		
			AvisoFluirNum++;
		}
		ContenidoCampo.Clear();
	
	}
	return(AvisoFluirNum);
}

/**		
	Es llamada para obtener los avisos de la seccion o nodo seleccionado.
			
			  @param StructAvisos *AvisoCompletos, Estructura que contiene todos los avisos del archivo que se importo.
			  @param StructAvisos *AvisoAFluir, Es la estructura que se llena unicamente con los avisos del nodo que fue seleccionado.
			  @param PMString NomSeccion, nombre de la seccion o pagina que fue seleccionada.
			  @param int32 NumAvisos, Es la cantidad de nodoa hijos del nodo seleccionado, en otras palabras la cantidad de avisos a fluir en el documento.
*/
void A2PFluirFunction::ObtenerAvisosDeSeccion(StructAvisos *AvisoCompletos,StructAvisos *AvisoAFluir, PMString NomSeccion,int32 NumAvisos,int32 NumAvisoSec)
{
	//Buscar Cual Campo contiene el El Nombre de Seccion o pagina
	PMString ContenidoCampo;
	ContenidoCampo.Clear();
	int32 NumerodelCampoSeccion=0;
	int32 AvisoCompletoNum=0;
	int32 AvisoFluirNum=0;
	do
	{

		if(AvisoCompletos[0].NomCampo1=="Pagina o seccion" || AvisoCompletos[0].NomCampo1=="Page or section")
		{
			//CAlert::ErrorAlert("0");
			NumerodelCampoSeccion=1;
			break;
		}

		if(AvisoCompletos[0].NomCampo2=="Pagina o seccion" || AvisoCompletos[0].NomCampo2=="Page or section")
		{
			//CAlert::ErrorAlert("1");
			NumerodelCampoSeccion=2;
			break;
		}

		if(AvisoCompletos[0].NomCampo3=="Pagina o seccion" || AvisoCompletos[0].NomCampo3=="Page or section")
		{
			//CAlert::ErrorAlert("2");
			NumerodelCampoSeccion=3;
			break;
		}
		if(AvisoCompletos[0].NomCampo4=="Pagina o seccion" || AvisoCompletos[0].NomCampo4=="Page or section")
		{
			//CAlert::ErrorAlert("3");
			NumerodelCampoSeccion=4;
			break;
		}

		if(AvisoCompletos[0].NomCampo5=="Pagina o seccion" || AvisoCompletos[0].NomCampo5=="Page or section")
		{
			//CAlert::ErrorAlert("4");
			NumerodelCampoSeccion=5;
			break;
		}
		if(AvisoCompletos[0].NomCampo6=="Pagina o seccion" || AvisoCompletos[0].NomCampo6=="Page or section")
		{
			//CAlert::ErrorAlert("5");
			NumerodelCampoSeccion=6;
			break;
		}
		if(AvisoCompletos[0].NomCampo7=="Pagina o seccion" || AvisoCompletos[0].NomCampo7=="Page or section")
		{
			//CAlert::ErrorAlert("6");
			NumerodelCampoSeccion=7;
			break;
		}
		if(AvisoCompletos[0].NomCampo8=="Pagina o seccion" || AvisoCompletos[0].NomCampo8=="Page or section")
		{
			//CAlert::ErrorAlert("7");
			NumerodelCampoSeccion=8;
			break;
		}
		if(AvisoCompletos[0].NomCampo9=="Pagina o seccion" || AvisoCompletos[0].NomCampo9=="Page or section")
		{
			//CAlert::ErrorAlert("8");
			NumerodelCampoSeccion=9;
			break;
		}
		if(AvisoCompletos[0].NomCampo10=="Pagina o seccion" || AvisoCompletos[0].NomCampo10=="Page or section")
		{
			//CAlert::ErrorAlert("9");
			NumerodelCampoSeccion=10;
			break;
		}
		if(AvisoCompletos[0].NomCampo11=="Pagina o seccion" || AvisoCompletos[0].NomCampo11=="Page or section")
		{
			//CAlert::ErrorAlert("10");
			NumerodelCampoSeccion=11;
			break;
		}
		if(AvisoCompletos[0].NomCampo12=="Pagina o seccion" || AvisoCompletos[0].NomCampo12=="Page or section")
		{
			//CAlert::ErrorAlert("11");
			NumerodelCampoSeccion=12;
			break;
		}
		if(AvisoCompletos[0].NomCampo13=="Pagina o seccion" || AvisoCompletos[0].NomCampo13=="Page or section")
		{
			////CAlert::ErrorAlert(AvisoCompletos[0].NomCampo13);
			NumerodelCampoSeccion=13;
			break;
		}
		if(AvisoCompletos[0].NomCampo14=="Pagina o seccion" || AvisoCompletos[0].NomCampo14=="Page or section")
		{
			////CAlert::ErrorAlert(AvisoCompletos[0].NomCampo14);
			
			break;
		}
	}while(false);
	

	/*PMString j="NumAvisos= ";
	j.AppendNumber(NumAvisos);
	CAlert::ErrorAlert(j);
	j="Numero de opcion o campo= ";
	j.AppendNumber(NumerodelCampoSeccion);
	*/
	//CAlert::ErrorAlert(NomSeccion);
	
	
	/*NomSeccion.Shrink(kTrue);
	
	
	PMString j;

	
	for(int i=0;i<NomSeccion.Length()-1;i++)
	{
		j.AppendW(NomSeccion.GetWChar(i));
	}
	
	
	NomSeccion=j;
	*/

	for(AvisoCompletoNum=0;AvisoCompletoNum<NumAvisos ;AvisoCompletoNum++)
	{
		switch(NumerodelCampoSeccion)
		{
		case 1:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo1;
			break;
		case 2:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo2;
			break;
		case 3:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo3;
			break;
		case 4:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo4;
			break;
		case 5:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo5;
			break;
		case 6:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo6;
			break;
		case 7:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo7;
			break;
		case 8:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo8;
			break;
		case 9:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo9;
			break;
		case 10:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo10;
			break;
		case 11:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo11;
			break;
		case 12:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo12;
			break;
		case 13:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo13;
			break;
		case 14:
			ContenidoCampo=AvisoCompletos[AvisoCompletoNum].ContenidoCampo14;
			break;
		}
		

	//	CAlert::InformationAlert(ContenidoCampo);
	///Compara la seccion de cada aviso 

	/*	ContenidoCampo.Shrink(kTrue);
		j.Clear();
		for(i=0;i<ContenidoCampo.Length()-1;i++)
		{
			j.AppendW(ContenidoCampo.GetWChar(i));
		}
		ContenidoCampo=j;*/
		
		if((ContenidoCampo.Compare(kTrue,NomSeccion)==0 || ContenidoCampo==NomSeccion) && AvisoFluirNum<NumAvisoSec)
		{
		//	CAlert::InformationAlert(NomSeccion);
		//	CAlert::InformationAlert(ContenidoCampo);
			
			AvisoAFluir[AvisoFluirNum].NomCampo1      =AvisoCompletos[AvisoCompletoNum].NomCampo1;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo1=AvisoCompletos[AvisoCompletoNum].ContenidoCampo1;
			AvisoAFluir[AvisoFluirNum].NomCampo2      =AvisoCompletos[AvisoCompletoNum].NomCampo2;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo2=AvisoCompletos[AvisoCompletoNum].ContenidoCampo2;
			AvisoAFluir[AvisoFluirNum].NomCampo3      =AvisoCompletos[AvisoCompletoNum].NomCampo3;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo3=AvisoCompletos[AvisoCompletoNum].ContenidoCampo3;
			AvisoAFluir[AvisoFluirNum].NomCampo4      =AvisoCompletos[AvisoCompletoNum].NomCampo4;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo4=AvisoCompletos[AvisoCompletoNum].ContenidoCampo4;
			AvisoAFluir[AvisoFluirNum].NomCampo5      =AvisoCompletos[AvisoCompletoNum].NomCampo5;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo5=AvisoCompletos[AvisoCompletoNum].ContenidoCampo5;
			AvisoAFluir[AvisoFluirNum].NomCampo6      =AvisoCompletos[AvisoCompletoNum].NomCampo6;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo6=AvisoCompletos[AvisoCompletoNum].ContenidoCampo6;
			AvisoAFluir[AvisoFluirNum].NomCampo7      =AvisoCompletos[AvisoCompletoNum].NomCampo7;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo7=AvisoCompletos[AvisoCompletoNum].ContenidoCampo7;
			AvisoAFluir[AvisoFluirNum].NomCampo8      =AvisoCompletos[AvisoCompletoNum].NomCampo8;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo8=AvisoCompletos[AvisoCompletoNum].ContenidoCampo8;
			AvisoAFluir[AvisoFluirNum].NomCampo9      =AvisoCompletos[AvisoCompletoNum].NomCampo9;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo9=AvisoCompletos[AvisoCompletoNum].ContenidoCampo9;
			AvisoAFluir[AvisoFluirNum].NomCampo10     =AvisoCompletos[AvisoCompletoNum].NomCampo10;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo10=AvisoCompletos[AvisoCompletoNum].ContenidoCampo10;
			AvisoAFluir[AvisoFluirNum].NomCampo11      =AvisoCompletos[AvisoCompletoNum].NomCampo11;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo11=AvisoCompletos[AvisoCompletoNum].ContenidoCampo11;
			AvisoAFluir[AvisoFluirNum].NomCampo12      =AvisoCompletos[AvisoCompletoNum].NomCampo12;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo12=AvisoCompletos[AvisoCompletoNum].ContenidoCampo12;
			AvisoAFluir[AvisoFluirNum].NomCampo13      =AvisoCompletos[AvisoCompletoNum].NomCampo13;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo13=AvisoCompletos[AvisoCompletoNum].ContenidoCampo13;
			AvisoAFluir[AvisoFluirNum].NomCampo14      =AvisoCompletos[AvisoCompletoNum].NomCampo14;
			AvisoAFluir[AvisoFluirNum].ContenidoCampo14=AvisoCompletos[AvisoCompletoNum].ContenidoCampo14;		
			
		/*	CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].NomCampo1);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].ContenidoCampo1);
			
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].NomCampo2);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].ContenidoCampo2);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].NomCampo3);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].ContenidoCampo3);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].NomCampo4);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].ContenidoCampo4);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].NomCampo5);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].ContenidoCampo5);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].NomCampo6);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].ContenidoCampo6);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].NomCampo7);
			CAlert::ErrorAlert(AvisoAFluir[AvisoFluirNum].ContenidoCampo7);
			*/
			AvisoFluirNum++;
		}
		ContenidoCampo.Clear();
	//	CAlert::ErrorAlert("limpia");
	}
//	CAlert::ErrorAlert("18");
}

/**
		Funcion que crea nuevo documento.
			
			  @param Pref, Estructura del tipo Preferencias, utilizado para aplicar las preferncias determinadas al documento.
*/
bool16 A2PFluirFunction::CrearDocumento(Preferencias &Pref)
{
	bool16 seCreoNuevo=kFalse;
	do
	{	
		IDocument* document =Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
				//	obtencion de un ICommand utilizando kNewDocCmdBoss
				/*InterfacePtr<ICommand> newDocCmd(CmdUtils::CreateCommand(kNewDocCmdBoss));
				if (newDocCmd == nil)
				{
					ASSERT_FAIL("FilActActionComponent::DoNew: create newDocCmd failed");
					break;
				}*/
				const UIFlags 	uiFlags = K2::kFullUI;
				 InterfacePtr<ICommand> newDocCmd(Utils<IDocumentCommands>()->CreateNewCommand(uiFlags));
				ASSERT(newDocCmd);
				if (newDocCmd == nil) {
					break;
				}

				
				//	adeciona la interfaz INewDocCmdData al Commando 
				InterfacePtr<INewDocCmdData> newDocCmdData(newDocCmd, UseDefaultIID());
				if (newDocCmdData == nil)
				{
					// Issue assert and generic user warning about failure:
					ASSERT_FAIL("FilActActionComponent::DoNew: newDocCmdData invalid");
					break;
				}
				
				// Set some parameters (although the defaults are fine for us):
				newDocCmdData->SetCreateBasicDocument(kFalse); // Create a complex document (default = kFalse).
				
				//obtencion del alto y ancho de la pagina
				PMReal Alto=Prefer.AltoPag;
				PMReal Ancho=Prefer.AnchoPagina;
				//PMRect a(0,0,Ancho,Alto);
				PMRect pageSize(0,0,Ancho, Alto );
				//pone el tama‚àö√≠o de la pagina
				newDocCmdData->SetPageSizePref(pageSize);//SetNewDocumentPageSize(pageSize);

				//pone la orientacion de la pagina
				newDocCmdData->SetWideOrientation(kFalse);

				//obtencion de los margenes de la pagina
				PMReal MIzq=Prefer.MargenInterior;
				PMReal MDer=Prefer.MargenExterior;
				PMReal MSup=Prefer.MargenSuperior;
				PMReal MInf=Prefer.MargenInferior;
				
				// pone los margenes 
				newDocCmdData->SetMargins(MIzq,MSup,MDer,MInf);
					
				//obtiene el numero de columnas
				int32 NCol=Prefer.NumColumas;

				//obtiene el tama‚àö√≠o del medianil
				PMReal Medi=Prefer.Medianil;
				
				//pone el numero de columnas, el medianil y la orientacion de las columnas
				newDocCmdData->SetColumns(NCol,Medi,kFalse);//Numero de columnas, Medianil,Orintacion
				
				if ( CmdUtils::ProcessCommand(newDocCmd) != kSuccess)//procesa el comando
				{
					//CAlert::ErrorAlert("No se pudo crear o ejecutar el comando");
					ErrorUtils::PMSetGlobalErrorCode(kFailure);
					break;
				}

				 // Pass the UIDRef of the new document back to the caller.
				const UIDList& newDocCmdUIDList = newDocCmd->GetItemListReference();
				UIDRef documentUIDRef = newDocCmdUIDList.GetRef(0);
				if (documentUIDRef == UIDRef::gNull) {
					ASSERT_FAIL("newDocCmd returned invalid document UIDRef!");
					break;
				}

 // Create the command.
				InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(kOpenLayoutCmdBoss));
				ASSERT(cmd);
				if (cmd == nil){
					break;
				}
 
				// Pass the command the UIDRef of the document.
				cmd->SetItemList(UIDList(documentUIDRef));
         
				// You could override the command's default options if necessary...
				InterfacePtr<IOpenLayoutPresentationCmdData> winData(cmd, IID_IOPENLAYOUTCMDDATA);
				ASSERT(winData);
				if (winData == nil) {
					break;
				}
				// ...we just take the defaults.
 
				// Open a window on the document.
				if (CmdUtils::ProcessCommand(cmd) != kSuccess) {
					ASSERT_FAIL("kOpenLayoutCmdBoss failed");
					break;
				}
 
				// Validate the the newly opened window is returned.
				InterfacePtr<IWindow> window(winData->GetResultingPresentation(), UseDefaultIID());
				ASSERT(window);
				if (window == nil) {
					// If we couldn't get an IWindow the postconditions won't be met
					break;
				}

				/*// Create a layout window for the new doc:
				InterfacePtr<ICommand> newWinCmd(CmdUtils::CreateCommand(kOpenLayoutWinCmdBoss));
				if (newWinCmd == nil)
				{
					ASSERT_FAIL("FilActActionComponent::DoNew: create openLayoutWinCmd failed");
					break;
				}
				
				// newDocCmd returns a UID of the new document
				newWinCmd->SetItemList(UIDList(*newDocCmd->GetItemList()));
				
				if ( CmdUtils::ProcessCommand(newWinCmd) != kSuccess)
				{
					//CAlert::ErrorAlert("Error al crear capa");
					ErrorUtils::PMSetGlobalErrorCode(kFailure);
				}*/
				
				seCreoNuevo=kTrue;
		}
		else
		{	///ya existe un documento al frente
			const int ActionAgreedValue=1;//numero de accion

			
			int32 result=CAlert::ModalAlert(kSobreDocActualStringKey,
												kYesString, //boton si
												kNoString,  //boton no
												kNullString, 
												ActionAgreedValue,//la numero de accion que devuelve
												CAlert::eQuestionIcon);//tipo de mensaje

			if(ActionAgreedValue==result)//si oprimio aceptar 
			{
				//Hay que borra el texto update de la capa update para
				//crea uno nuevo
					//if(ActivarCapa("Update"))
					//	BorraTextoUpdate();
					
					
					///validacion para verificar el tama‚àö√≠o de la pagina actual
					PMReal Alto=Prefer.AltoPag;
					PMReal Ancho=Prefer.AnchoPagina;
					
					PMRect rectPref(0,0,Ancho,Alto);
					PMRect rect=obtenerTamPagina();
					/*CAlert::InformationAlert("cdsdfdf");
					PMString asa="";
					asa.AppendNumber(rectPref.Height());
					asa.Append(",");
					asa.AppendNumber(rectPref.Width());
					asa.Append("\n");
					asa.AppendNumber(rect.Height());
					asa.Append(",");
					asa.AppendNumber(rect.Width());
					asa.Append("\n");
					asa.AppendNumber(rectPref.Height()-rect.Height());
					asa.Append(",");
					asa.AppendNumber(rectPref.Width()-rect.Width());
					
					CAlert::InformationAlert(asa);*/
					//if( (rectPref.OutputHeight()-rect.OutputHeight())>2 || (rectPref.OutputWidth()-rect.OutputWidth())>2 || (rectPref.OutputWidth()-rect.OutputWidth())<-2 || (rectPref.OutputHeight()-rect.OutputHeight())<-2 )
					if( (rectPref.Height()-rect.Height())>2 || (rectPref.Width()-rect.Width())>2 || (rectPref.Width()-rect.Width())<-2 || (rectPref.Height()-rect.Height())<-2 )

					{	
						result=CAlert::ModalAlert(kTamanoIncorrectoStringKey,
													kYesString, 
													kNoString,
													kNullString,
													ActionAgreedValue,CAlert::eQuestionIcon);
						if(ActionAgreedValue!=result)
							return(kFalse);
					}

					////validar numero de columnas
					if(!ValidaColumnas(Prefer))
					{
						result=CAlert::ModalAlert(kNumColumnasIncorrectoStringKey,
													kYesString, 
													kNoString,
													kNullString,
													ActionAgreedValue,CAlert::eQuestionIcon);
						if(ActionAgreedValue!=result)
							return(kFalse);

					}
					
					////validar margenes
					if(!ValidaMargenes(Prefer))
					{
						result=CAlert::ModalAlert(kTamMargenIncorrectoStringKey,
													kYesString, 
													kNoString,
													kNullString,
													ActionAgreedValue,CAlert::eQuestionIcon);
						if(ActionAgreedValue!=result)
							return(kFalse);

					}

					////validar tama‚àö√≠o de medianil
					if(!ValidaMedianil(Prefer))
					{
						result=CAlert::ModalAlert(kTamMedianilIncorrectoStringKey,
													kYesString, 
													kNoString,
													kNullString,
													ActionAgreedValue,CAlert::eQuestionIcon);
						if(ActionAgreedValue!=result)
							return(kFalse);

					}
					

					//ajusta el punto cero
					AjustarPuntoZero();
					
					seCreoNuevo=kTrue;
			}
			else//si no hay documento en frente
			{
				//	obtencion de un ICommand utilizando kNewDocCmdBoss
				/*InterfacePtr<ICommand> newDocCmd(CmdUtils::CreateCommand(kNewDocCmdBoss));
				if (newDocCmd == nil)
				{
					ASSERT_FAIL("FilActActionComponent::DoNew: create newDocCmd failed");
					break;
				}*/
				const UIFlags 	uiFlags = K2::kFullUI;
				 InterfacePtr<ICommand> newDocCmd(Utils<IDocumentCommands>()->CreateNewCommand(uiFlags));
				ASSERT(newDocCmd);
				if (newDocCmd == nil) {
					break;
				}

				
				//	adeciona la interfaz INewDocCmdData al Commando 
				InterfacePtr<INewDocCmdData> newDocCmdData(newDocCmd, UseDefaultIID());
				if (newDocCmdData == nil)
				{
					// Issue assert and generic user warning about failure:
					ASSERT_FAIL("FilActActionComponent::DoNew: newDocCmdData invalid");
					break;
				}
				
				// Set some parameters (although the defaults are fine for us):
				newDocCmdData->SetCreateBasicDocument(kFalse); // Create a complex document (default = kFalse).
				
				//obtencion del alto y ancho de la pagina
				PMReal Alto=Prefer.AltoPag;
				PMReal Ancho=Prefer.AnchoPagina;
				//PMRect a(0,0,Ancho,Alto);
				PMRect pageSize(0,0,Ancho, Alto );
				//pone el tama‚àö√≠o de la pagina
				newDocCmdData->SetPageSizePref(pageSize);
				
				//pone la orientacion de la pagina
				newDocCmdData->SetWideOrientation(kFalse);

				//obtencion de los margenes de la pagina
				PMReal MIzq=Prefer.MargenInterior;
				PMReal MDer=Prefer.MargenExterior;
				PMReal MSup=Prefer.MargenSuperior;
				PMReal MInf=Prefer.MargenInferior;
				
				// pone los margenes 
				newDocCmdData->SetMargins(MIzq,MSup,MDer,MInf);
					
				//obtiene el numero de columnas
				int32 NCol=Prefer.NumColumas;

				//obtiene el tama‚àö√≠o del medianil
				PMReal Medi=Prefer.Medianil;
				
				//pone el numero de columnas, el medianil y la orientacion de las columnas
				newDocCmdData->SetColumns(NCol,Medi,kFalse);//Numero de columnas, Medianil,Orintacion
				
				if ( CmdUtils::ProcessCommand(newDocCmd) != kSuccess)//procesa el comando
				{
					//CAlert::ErrorAlert("No se pudo crear o ejecutar el comando");
					ErrorUtils::PMSetGlobalErrorCode(kFailure);
					break;
				}

				 // Pass the UIDRef of the new document back to the caller.
				const UIDList& newDocCmdUIDList = newDocCmd->GetItemListReference();
				UIDRef documentUIDRef = newDocCmdUIDList.GetRef(0);
				if (documentUIDRef == UIDRef::gNull) {
					ASSERT_FAIL("newDocCmd returned invalid document UIDRef!");
					break;
				}

 // Create the command.
				InterfacePtr<ICommand> cmd(CmdUtils::CreateCommand(kOpenLayoutCmdBoss));
				ASSERT(cmd);
				if (cmd == nil){
					break;
				}
 
				// Pass the command the UIDRef of the document.
				cmd->SetItemList(UIDList(documentUIDRef));
         
				// You could override the command's default options if necessary...
				InterfacePtr<IOpenLayoutPresentationCmdData> winData(cmd, IID_IOPENLAYOUTCMDDATA);
				ASSERT(winData);
				if (winData == nil) {
					break;
				}
				// ...we just take the defaults.
 
				// Open a window on the document.
				if (CmdUtils::ProcessCommand(cmd) != kSuccess) {
					ASSERT_FAIL("kOpenLayoutCmdBoss failed");
					break;
				}
 
				// Validate the the newly opened window is returned.
				InterfacePtr<IWindow> window(winData->GetResultingPresentation(), UseDefaultIID());
				ASSERT(window);
				if (window == nil) {
					// If we couldn't get an IWindow the postconditions won't be met
					break;
				}

				/*// Create a layout window for the new doc:
				InterfacePtr<ICommand> newWinCmd(CmdUtils::CreateCommand(kOpenLayoutWinCmdBoss));
				if (newWinCmd == nil)
				{
					ASSERT_FAIL("FilActActionComponent::DoNew: create openLayoutWinCmd failed");
					break;
				}
				
				// newDocCmd returns a UID of the new document
				newWinCmd->SetItemList(UIDList(*newDocCmd->GetItemList()));
				
				if ( CmdUtils::ProcessCommand(newWinCmd) != kSuccess)
				{
					//CAlert::ErrorAlert("Error al crear capa");
					ErrorUtils::PMSetGlobalErrorCode(kFailure);
				}*/
				
				seCreoNuevo=kTrue;
		}
		}
	} while (false); 
	return(seCreoNuevo);
}





/**
	Funcion para convertir a puntos cada uno de las coordenadas dependiendo 
	de la unidades en que esten especificadas.
*/
PMReal A2PFluirFunction::ConvertirCoorAPuntos(PMReal Cantidad, PMString Unidades)
{	
	int16 unitIndex;//index de unidades
	do
	{
		if(Unidades.Contains("Centimeters") || Unidades.Contains("Centimetros"))
			unitIndex=kCentimetersBoss;//numero de identificacion para las unidades en centimetros
		else
			if(Unidades.Contains("Milimeters") || Unidades.Contains("Milimetros"))
				unitIndex=kMillimetersBoss;//numero de identificacion para las unidades en Milimetros
			else
				if(Unidades.Contains("Inches") || Unidades.Contains("Pulgadas"))
				{
					unitIndex=kInchesBoss;//numero de identificacion para las unidades en pulgadas
				}
				else
					if(Unidades.Contains("C‚àö√•ceros")|| Unidades.Contains("Ciceros"))
						unitIndex=kCicerosBoss;//numero de identificacion para las unidades en ciceros
					else
						if(Unidades.Contains("Points") || Unidades.Contains("Puntos"))
							unitIndex=kPointsBoss;//numero de identificacion para las unidades en puntos
						else
							if(Unidades.Contains("Picas"))
								unitIndex=kPicasBoss;//numero de identificacion para las unidades en picas
							else
							{
								break; //Sin converciones
							}

		//obtengo la interfaz  IMeasurementSystem de la sesion actual
		InterfacePtr<IMeasurementSystem>	iMeasureSystem(GetExecutionContextSession(), UseDefaultIID());
		if (iMeasureSystem == nil)
		{
			break;
		}

		InterfacePtr<IUnitOfMeasure> iUOM(iMeasureSystem->QueryUnitOfMeasure(iMeasureSystem->Location(unitIndex)));

		// Use the index to get the unit of measurement
		//InterfacePtr<IUnitOfMeasure> iUOM(iMeasureSystem->QueryUnitOfMeasure(unitIndex));
		if (iUOM == nil)
		{
			break;
		}
		// Converting 1 unit of this unit of measurement to points
		PMReal pointValue = iUOM->UnitsToPoints(Cantidad);
		PMString SSD="";
		SSD.AppendNumber(Cantidad);
		SSD.Append(",");
		SSD.AppendNumber(pointValue);
		//CAlert::InformationAlert(SSD);
		return(pointValue);
	}while(false);
	return(0);
}
/**
		Funcion que manda llamar la creacion de cada una de las capas dependiendo las preferencias.
			
			  @param Pref, Estructura del tipo Preferencias, utilizado para aplicar las preferncias determinadas al documento.
*/
bool16 A2PFluirFunction::CreaciondeCapas(Preferencias &Prefer)
{
	bool16 retval=kFalse;
	//CrearCapa("Update",0,0);
	if(Prefer.CapaEditorial==1)
	{
		CrearCapa("Editorial",Prefer.CapaEditorialBlocked,Prefer.CapaEditorialVisible);
		retval=kTrue;
	}
	if(Prefer.CapaImagen==1)
	{
		CrearCapa("Imagen",Prefer.CapaImagenBlocked,Prefer.CapaImagenVisible);
		retval=kTrue;
	}
	if(Prefer.CapaEtiquetaConPublicidad==1)
	{
		CrearCapa("Etiquetas C/Publicidad",Prefer.CapaEtiquetaConPublicidadBlocked,Prefer.CapaEtiquetaConPublicidadVisible);
		retval=kTrue;
	}
	if(Prefer.CapaEtiquetaSinPublicidad==1)
	{
		CrearCapa("Etiquetas S/Publicidad",Prefer.CapaEtiquetaSinPublicidadBlocked,Prefer.CapaEtiquetaSinPublicidadVisible);
		retval=kTrue;
	}
	
	CrearCapa("A2PDate&Page",kFalse,kTrue);
	
	
	return (retval);
}

/**
			Funcion que crea una determinada capa.
			
			  @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa con que se debe crear.
			  @param CapaBloqueada,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de bloqueo de la capa a crear;0 para no bloqueada, 1 para bloqueada.
			  @param CapaVisible,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de muestra de la capa a crear; 0 para no visible, 1 para visible
*/
void A2PFluirFunction::CrearCapa(PMString NombredeCapa,int32 CapaBloqueada,int32 CapaVisible)
{
	ErrorCode error = kCancel;
	
	// We'll access this later, so we must create it here:
	IAbortableCmdSeq* newLayerCmdSequence = nil;
	 
	UID LayerUID=nil;
	// We'll use a do-while(false) to break out on bad pointers:
	do
	{
	
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			//CAlert::WarningAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			//CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		//Busca de una capa por nombre.
		LayerUID=layerList->FindByName(NombredeCapa);
		if(LayerUID!=nil)//salir en caso de que exista esta capa
			break;
			
		// To create a new layer requires the new layer command followed by the
		// set active layer command, so we'll do it as a sequence:
		newLayerCmdSequence = CmdUtils::BeginAbortableCmdSeq();
		if (newLayerCmdSequence == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: newLayerCmdSequence invalid");
			break;
		}
		
		error = kFailure; // Indicates we successfully started command sequence.

		//pone el nombre de la capa
		newLayerCmdSequence->SetName(NombredeCapa);
		////CAlert::ErrorAlert("2");
		
		// Create a new layer in the frontmost document:
		InterfacePtr<ICommand> newLayerCmd(CmdUtils::CreateCommand(kNewLayerCmdBoss));
		////CAlert::ErrorAlert("3");
		InterfacePtr<INewLayerCmdData> newLayerCmdData(newLayerCmd, IID_INEWLAYERCMDDATA);
		////CAlert::ErrorAlert("4");
		if (newLayerCmd == nil || newLayerCmdData == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: newLayerCmdData invalid");
			////CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}

		// Now set the name of this layer:
		//SetLayerName(NombredeCapa);
		////CAlert::ErrorAlert("5");
		
		// Create the document UID Ref to use here and in the active layer cmd:
		const UIDRef docUIDRef = ::GetUIDRef(document);
		////CAlert::ErrorAlert("6");
		
		// Create it in the front most document and set the name:
		newLayerCmdData->Set(docUIDRef, &NombredeCapa);
		////CAlert::ErrorAlert("7");
		// Process the command
		error = CmdUtils::ProcessCommand(newLayerCmd);
		////CAlert::ErrorAlert("8");
		// Check for error, and alert the user if so:
		if (error != kSuccess)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: newLayerCmdData failed");
			////CAlert::WarningAlert("Error al intentar Crear nueva Capa");
			break;
		}

		// We've created a new layer.  Let's get its UID.  First we get the pointer to the last layer:
		IDocumentLayer* documentLayer = layerList->QueryLayer(layerList->GetCount()-1);
		////CAlert::ErrorAlert("9");
		// Now we've created the new layer, we must set it as the active layer:
		InterfacePtr<ICommand> setActiveLayerCmd(CmdUtils::CreateCommand(kSetActiveLayerCmdBoss));
		////CAlert::ErrorAlert("10");
		InterfacePtr<ILayoutCmdData> layoutCmdData(setActiveLayerCmd, UseDefaultIID());
		////CAlert::ErrorAlert("11");
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		////CAlert::ErrorAlert("12");
		if (setActiveLayerCmd == nil || layoutCmdData == nil || layoutControlData == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd, layoutCmdData, or layoutControlData invalid");
			////CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}

		setActiveLayerCmd->SetItemList(UIDList(documentLayer));
		////CAlert::ErrorAlert("13");
		layoutCmdData->Set(docUIDRef, layoutControlData);
		////CAlert::ErrorAlert("14");
		// Process the command
		error = CmdUtils::ProcessCommand(setActiveLayerCmd);
		////CAlert::ErrorAlert("15");
		// Check for error, and alert the user if so:
		if (error != kSuccess)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd failed");
			////CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
		}
		
	} while (false); // Only do once.
	
	if (newLayerCmdSequence != nil)
	{
		// End command sequence, either aborting if we had an error or
		// ending it if we're successful:
		if (error == kFailure)
		{
			CmdUtils::AbortCommandSequence(newLayerCmdSequence);
		}
		else if (error == kSuccess)
		{
			// We were successful.
			CmdUtils::EndCommandSequence(newLayerCmdSequence);
		}
	}
	// Otherwise we never were able to start the sequence.
}

/**
			Funcion para bloquear una determinada capa.
			
			  @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa con que se debe crear.
			  @param CapaBloqueada,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de bloqueo de la capa a crear;0 para no bloqueada, 1 para bloqueada.
*/
void A2PFluirFunction::BloquearCapa(PMString NombreCapa,bool16 Bloqueo)
{
	ErrorCode success = kFailure;
	UID LayerUID=nil; //ID de la capa deseada
	UIDRef LayerUIDR;//Referencia a la capa deseada
	// We'll use a do-while(false) to break out on bad pointers:
	do
	{
		
		//obtiene un interfaz IDocument del documento de enfrente
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			//CAlert::WarningAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		//obtiene un interfaz IDataBase del documento de enfrente
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		//obtiene un interfaz ILayerList del documento de enfrente
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			//CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		//Busqueda de la capa por su nombre
		LayerUID=layerList->FindByName(NombreCapa);

		//obtencion del UIDRef de la capa por medio de la base de de datos del documento
		LayerUIDR=UIDRef(db,LayerUID);
		
	
		// Lock or unlock the layer: Creacion del comando a partir del kLockLayerCmdBoss
		InterfacePtr<ICommand>	lockLayerCmd(CmdUtils::CreateCommand(kLockLayerCmdBoss));
		if (lockLayerCmd == nil)
		{
			ASSERT_FAIL("LayersApplyButtonWidgetObserver::DoLock: lockLayerCmd is invalid");
			//CAlert::WarningAlert("Error");
			break;
		}

		//pone la la referencia de la capa a ocultar
		lockLayerCmd->SetItemList(UIDList(LayerUIDR));
		
		//obtencion de la interfaz IBoolData a partr del comando lockLayerCmd 
		InterfacePtr<IBoolData> lockLayerCmdData(lockLayerCmd, UseDefaultIID());
		if (lockLayerCmdData == nil)
		{			
			ASSERT_FAIL("LayersApplyButtonWidgetObserver::DoLock: lockLayerCmdData is invalid");
			//CAlert::WarningAlert("Error");
			break;
		}
		
		//bloque o desbloque la capa dependiendo la variable booleana bloqueo
		lockLayerCmdData->Set(Bloqueo);
		
		//ejecuta comando
		success = CmdUtils::ProcessCommand(lockLayerCmd);
		if (success != kSuccess)
		{
			//CAlert::WarningAlert("Error");
		}
		
	} while (false); // Only do once.
}


/**
	Funcion para mostra o ocultar una determinada capa.
			
			  @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa con que se debe crear.
			  @param CapaVisible,	Variable entero que indicara un valor p¬¨‚àëra determinar el estado de muestra de la capa a crear; 0 para no visible, 1 para visible
*/		
void A2PFluirFunction::MostrarCapa(PMString NombreCapa,bool16 Mostrar)
{
	ErrorCode success = kFailure;
		UID LayerUID=nil;
	// We'll use a do-while(false) to break out on bad pointers:
	do
	{
	
			//obtiene un interfaz IDocument del documento de enfrente
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			//CAlert::WarningAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		//obtiene un interfaz IDataBase del documento de enfrente
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		//obtiene un interfaz ILayerList del documento de enfrente
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			//CAlert::ErrorAlert("Error al intentar Crear nueva Capa");
			break;
		}
		
		//Busqueda de la capa por su nombre
		LayerUID=layerList->FindByName(NombreCapa);

		//obtencion del UIDRef de la capa por medio de la base de de datos del documento
		UIDRef parent=UIDRef(db,LayerUID);
		// Lock or unlock the layer: creacion de un comando a partir del jefe kShowLayerCmdBoss
		InterfacePtr<ICommand>	showLayerCmd(CmdUtils::CreateCommand(kShowLayerCmdBoss));
		if (showLayerCmd == nil)
		{
			ASSERT_FAIL("LayersApplyButtonWidgetObserver::DoLock: showLayerCmd is invalid");
			//CAlert::WarningAlert("Error");
			break;
		}

		//pone la la referencia de la capa a ocultar
		showLayerCmd->SetItemList(UIDList(parent));
		
		InterfacePtr<IBoolData> showLayerCmdData(showLayerCmd, UseDefaultIID());
		if (showLayerCmdData == nil)
		{			
			ASSERT_FAIL("LayersApplyButtonWidgetObserver::DoLock: showLayerCmdData is invalid");
			//CAlert::WarningAlert("Error");
			break;
		}
		//popne la accion que se debe realizar si ocultarla o mostrar dependiendo la variable booleana Mostrar
		showLayerCmdData->Set(Mostrar);
		
		//ejecuta comando
		success = CmdUtils::ProcessCommand(showLayerCmd);//
		if (success != kSuccess)
		{
			//CAlert::WarningAlert("Error");
		}
		
	} while (false); // Only do once.
}




/**PMString Ruta; //Declaracion de cadena paqa la ruta
Ruta="Macintosh: ....."; llenado de cadena

if (!ExisteArchivoParaLectura(&Ruta))// como mandar llamar la funcion 


			Metodo que busca el archivo correspondiente si no lo encuentra lo buscara por otro tipo de
			extencion.
-Devuelve verdadero al encontrar una ruta con cualquier extencion especificada en este codigo.
-Devuelve el path ("filePath") de el archivo que se encontro con la extencion indicada o con la extencion que se indica en esta funcion.
*/
bool16 A2PFluirFunction::ExisteArchivoParaLectura(PMString *filePath)
{
	PMString Path;//Variable que almacenara el path original y para poder hacer las preguntas si existen el
					//archivo con determinada extencion
	Path=filePath->GrabCString();
	bool16 value=kFalse;//
	
	do
	{
		//Busca si existe el archivo con la extencion inicial
		if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
		{
			value=kTrue;
			break;
		}
		else//si no lo encontro
		{
			//Busca si existe el archivo con la extencion jpg
			Path=PnlTrvUtils::TruncateExtencion(Path);
			Path.Append(".jpg");//adiciona la nueva extencion al path o ruta.
			if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
			{
				value=kTrue;
				break;
			}
			else
			{
				//Busca si existe el archivo con la extencion gif
				Path=PnlTrvUtils::TruncateExtencion(Path);
				Path.Append(".gif");
				if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
				{
					value=kTrue;
					break;
				}
				else
				{
					//Busca si existe el archivo con la extencion pdf
					Path=PnlTrvUtils::TruncateExtencion(Path);
					Path.Append(".pdf");
					if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
					{
						value=kTrue;
						break;
					}
					else
					{
						//Busca si existe el archivo con la extencion psd
						Path=PnlTrvUtils::TruncateExtencion(Path);
						Path.Append(".psd");
						if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
						{
							value=kTrue;
							break;
						}
						else
						{
							//Busca si existe el archivo con la extencion eps
							Path=PnlTrvUtils::TruncateExtencion(Path);
							Path.Append(".eps");
							if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
							{
								value=kTrue;
								break;
							}
							else
							{
								//Busca si existe el archivo con la extencion tif
								Path=PnlTrvUtils::TruncateExtencion(Path);
								Path.Append(".tif");
								if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
								{
									value=kTrue;
									break;
								}
								else
								{
									//Busca si existe el archivo con la extencion bmp
									Path=PnlTrvUtils::TruncateExtencion(Path);
									Path.Append(".bmp");
									if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
									{
										value=kTrue;
										break;
									}

									else
									{
										//Busca si existe el archivo con la extencion jepg
										Path=PnlTrvUtils::TruncateExtencion(Path);
										Path.Append(".jpeg");
										if(SDKUtilities::FileExistsForRead(Path) == kSuccess)
										{
											value=kTrue;
											break;
										}
									}
								}
							}
						}
					}
				}
			}			
		}
	}while(false);
	filePath->Clear();//limpia la cadena el path original
	filePath->Append(Path.GrabCString());
	return(value);
}

/**
			Activa una capa dependiendo su nombre NomCapa
			
			  @param NombredeCapa, Variable del tipo PMString que contiene el nombre de la capa que se desea activar.
*/
bool16 A2PFluirFunction::ActivarCapa(PMString NomCapa)
{
	

	bool16 retval = kFalse;
	ErrorCode error=kCancel;
	do
	{
		//obtiene la interfaz IDocument del documento actual
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			break;
		}
		
		//obtiene el UIDRef del documento
		const UIDRef docUIDRef = ::GetUIDRef(document);
						
		//obtien la interfaz ILayerList o lista de de capas del documento actual
		InterfacePtr<ILayerList> layerList(document, IID_ILAYERLIST);
		if (layerList == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: layerList invalid");
			break;
		}

		//Busqueda de la capa por su nombre obtienendo su UID
		UID Capa= layerList->FindByName(NomCapa);
		if(Capa==nil)
		{
			break;
		}
		
		//obtiene el numero de capa que se desea activar a partir de su UID
		int32 NumLayerAActivar=layerList->GetLayerIndex(Capa);
		
		//se obtien la interfaz IDocumentLayer a partir del numero de capa
		IDocumentLayer* documentLayer = layerList->QueryLayer(NumLayerAActivar);
		
		// Now we've created the new layer, we must set it as the active layer:
		InterfacePtr<ICommand> setActiveLayerCmd(CmdUtils::CreateCommand(kSetActiveLayerCmdBoss));
		
		InterfacePtr<ILayoutCmdData> layoutCmdData(setActiveLayerCmd, UseDefaultIID());
		
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		
		if (setActiveLayerCmd == nil || layoutCmdData == nil || layoutControlData == nil)
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd, layoutCmdData, or layoutControlData invalid");
			break;
		}

		setActiveLayerCmd->SetItemList(UIDList(documentLayer));
		
		layoutCmdData->Set(docUIDRef, layoutControlData);
		
		// Process the command
		error = CmdUtils::ProcessCommand(setActiveLayerCmd);

		if (error != kSuccess)//si no se efectuo satisfactoriamente el comando
		{
			ASSERT_FAIL("AddLayerButtonWidgetObserver::Update: setActiveLayerCmd failed");
			break;
		}
		retval = kTrue;
	}while(false);
	return(retval);
}

/**
			Convierte la direccion contenida en una variable PMString, en una direccion valida para InDesign.
			//Adiciona "/" despues de uno como este.
			
			  @param filePath, Variable del tipo PMString que contiene el path o direccion normal de una imagen.
*/
PMString A2PFluirFunction::ConvertAFileAceptada(PMString filePath)
{
	PMString Char;
	Char.Clear();
	int32 Caracter;
	int32 Ascii=0;
	for(Caracter=0;Caracter<filePath.NumUTF16TextChars();Caracter++)
	{
		if(filePath.GetWChar(Caracter)==92)
		{
			filePath.InsertW(92,Caracter+1);
			Caracter=Caracter+2;
		}
	}
	return(filePath);
}

/**
			Obtiene y separa las coordenadas de la cadena Coordenadas para convertirlas en PMReal (double).
			
			  @param leftMargin, variable del tipo PMReal la cual contendra la coordenada y1 
			  @param topMargin, variable del tipo PMReal la cual contendra x1.
			  @param rightMargin,variable del tipo PMReal la cual contendra y2.
			  @param bottomMargin, variable del tipo PMReal la cual contendra x2.
			  @param Coordenadas, Variable del Tipo PMString la cual contendra la cadena o las coordenadas en forma string o char.
*/	
void A2PFluirFunction::CoordenadasDeImagen(Preferencias &Prefer, PMReal &left,PMReal &top,PMReal &right,PMReal &bottom,PMString Coordenadas)
{	
	bool16 CoordenadaValida=kFalse;
	PMString Coordenada="";
	PMString Cadena="";//cadena que almacena los digitos de cada coordenada
	int32 NumCaracter=0;//numero de caracter
	int32 NumCoor=1;//Numero de coordenada
//	int32 Ascci;//valor ascii del caracter leido

//	CAlert::InformationAlert(Prefer.SepEnCoordenadas);
	int32 indexCar=Prefer.SepEnCoordenadas.LastIndexOfWChar(94);

	PMString Cadena_A_Buscar="";
	Cadena_A_Buscar.Clear();

	if(indexCar==0)
	{
		PMString* B=Prefer.SepEnCoordenadas.Substring(indexCar+1,Prefer.SepEnCoordenadas.NumUTF16TextChars()-indexCar);
		Cadena_A_Buscar.AppendW(B->GetAsNumber());
		//CAlert::InformationAlert(Cadena_A_Buscar);
	}
	else
	{
		PMString* A=Prefer.SepEnCoordenadas.Substring(0,indexCar);
		PMString* B=Prefer.SepEnCoordenadas.Substring(indexCar+1,Prefer.SepEnCoordenadas.NumUTF16TextChars()-indexCar);
		Cadena_A_Buscar.Append(A->GrabCString());
		Cadena_A_Buscar.AppendW(B->GetAsNumber());
		//CAlert::InformationAlert(Cadena_A_Buscar);
	}
	
	
/*	if(Cadena_A_Buscar.Contains()==1 && Cadena_A_Buscar.GetWChar(0)==44)
	{
		//Escanear de atras hacia delante
		NumCaracter=Coordenadas.Length()-1;
		for(NumCoor=4;NumCoor>0 && NumCaracter>=0;NumCoor--)//ciclo que controla la coordenada actual
		{
			Cadena.Clear();
			do
			{
				Cadena.AppendW(Coordenadas.GetWChar(NumCaracter));
				if(strstr(Cadena.GrabCString(),Cadena_A_Buscar.GrabCString()))
				{
					Coordenada.Clear();
					if(Cadena.Length()>3)
					{
						for(int i=0;i<Cadena.Length();i++)
						{
							if((Cadena.GetWChar(i)>=48 && Cadena.GetWChar(i)<=58)||Cadena.GetWChar(i)==46)
								Coordenada.AppendW(Cadena.GetWChar(i));
						}
						
					}
					CoordenadaValida=kTrue;
				}
				else
				{
					
					NumCaracter--;
				}
				
			}while(CoordenadaValida==kFalse && NumCaracter>=0);
		}
	}
	else
	{*/
		for(NumCoor=1;NumCoor<=4 && NumCaracter<Coordenadas.NumUTF16TextChars();NumCoor++)//ciclo que controla la coordenada actual
		{
			//Escanear de delante hacia atras
			Cadena.Clear();//limpia cadena
			do
			{
				
				if(NumCoor==4)
				{
					Coordenada.Clear();
					do
					{
						Cadena.AppendW(Coordenadas.GetWChar(NumCaracter));
						NumCaracter++;
					}while(NumCaracter<Coordenadas.NumUTF16TextChars());

					for(int i=0;i<Cadena.NumUTF16TextChars();i++)
						{
							if((Cadena.GetWChar(i)>=48 && Cadena.GetWChar(i)<=58)||Cadena.GetWChar(i)==46||Cadena.GetWChar(i)==45)
								Coordenada.AppendW(Cadena.GetWChar(i));
						}
				}
				else
				{
					Cadena.AppendW(Coordenadas.GetWChar(NumCaracter));
					if(strstr(Cadena.GrabCString(),Cadena_A_Buscar.GrabCString()))
					{
						Coordenada.Clear();
						for(int i=0;i<Cadena.NumUTF16TextChars();i++)
						{
							if((Cadena.GetWChar(i)>=48 && Cadena.GetWChar(i)<=58)||Cadena.GetWChar(i)==46||Cadena.GetWChar(i)==45)
								Coordenada.AppendW(Cadena.GetWChar(i));
						}
					
						CoordenadaValida=kTrue;
					}
					else
					{
						//NumCaracter++;
						NumCaracter++;
					}
				}
			}while(CoordenadaValida==kFalse && NumCaracter<Coordenadas.NumUTF16TextChars());

			//incrementa el numero de caracter para leer la siguiente coordenada
			//NumCaracter=++;
			NumCaracter++;
			CoordenadaValida=kFalse;

			//CAlert::InformationAlert(Coordenada);

			switch(Prefer.TipoLecturaCoor.GetAsNumber())
			{
				case 1://Forma TLBR
					
						ObtenerCoordenadasDeTLBR(Prefer.OrdendeLectura,NumCoor,top,left,bottom,right,Coordenada);	
					break;

				case 2://Forma (X,Y) (X,Y)
					
						ObtenerCoordenadasDeXY(Prefer.OrdendeLectura,NumCoor,top,left,bottom,right,Coordenada);
					break;
				case 3://Forma Tradicional T,L,B,R
					
						switch(NumCoor)
						{
							case 1:top=Coordenada.GetAsDouble();break;//y1
							case 2:left=Coordenada.GetAsDouble();break;//x1
							case 3:bottom=Coordenada.GetAsDouble();break;//y2
							case 4:right=Coordenada.GetAsDouble();break;//x2
						}
				break;
			}

		}
//	}
}


/**
*/
void A2PFluirFunction::ObtenerCoordenadasDeTLBR(PMString SecuenciaCoord,int32 NumeroDeCoord,PMReal &top,PMReal &left,PMReal &bottom,PMReal &right,PMString Valor)
{

	
		do
		{
			if(SecuenciaCoord.Contains("T,L,B,R"))
			{
				switch(NumeroDeCoord)
				{
					case 1:top=Valor.GetAsDouble();break;
					case 2:left=Valor.GetAsDouble();break;
					case 3:bottom=Valor.GetAsDouble();break;
					case 4:right=Valor.GetAsDouble();break;
				}
				break;
			}
			if(SecuenciaCoord.Contains("T,L,R,B"))
			{	
				switch(NumeroDeCoord)
				{
					case 1:top=Valor.GetAsDouble();break;
					case 2:left=Valor.GetAsDouble();break;
					case 3:right=Valor.GetAsDouble();break;
					case 4:bottom=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("T,R,L,B"))
			{
				switch(NumeroDeCoord)
				{
					case 1:top=Valor.GetAsDouble();break;
					case 2:right=Valor.GetAsDouble();break;
					case 3:left=Valor.GetAsDouble();break;
					case 4:bottom=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("T,R,B,L"))
			{	
				switch(NumeroDeCoord)
				{
					case 1:top=Valor.GetAsDouble();break;
					case 2:right=Valor.GetAsDouble();break;
					case 3:bottom=Valor.GetAsDouble();break;
					case 4:left=Valor.GetAsDouble();break;
				}	
				break;
			}
			if(SecuenciaCoord.Contains("T,B,L,R"))
			{	
				switch(NumeroDeCoord)
				{
					case 1:top=Valor.GetAsDouble();break;
					case 2:bottom=Valor.GetAsDouble();break;
					case 3:left=Valor.GetAsDouble();break;
					case 4:right=Valor.GetAsDouble();break;
				}	
				break;
			}
			if(SecuenciaCoord.Contains("T,B,R,L"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:top=Valor.GetAsDouble();break;
					case 2:bottom=Valor.GetAsDouble();break;
					case 3:right=Valor.GetAsDouble();break;
					case 4:left=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("B,L,T,R"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:bottom=Valor.GetAsDouble();break;
					case 2:left=Valor.GetAsDouble();break;
					case 3:top=Valor.GetAsDouble();break;
					case 4:right=Valor.GetAsDouble();break;
				}	
				break;
			}
			if(SecuenciaCoord.Contains("B,L,R,T"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:bottom=Valor.GetAsDouble();break;
					case 2:left=Valor.GetAsDouble();break;
					case 4:top=Valor.GetAsDouble();break;
					case 3:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("B,R,L,T"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:bottom=Valor.GetAsDouble();break;
					case 3:left=Valor.GetAsDouble();break;
					case 4:top=Valor.GetAsDouble();break;
					case 2:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("B,R,T,L"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:bottom=Valor.GetAsDouble();break;
					case 4:left=Valor.GetAsDouble();break;
					case 3:top=Valor.GetAsDouble();break;
					case 2:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("B,T,R,L"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:bottom=Valor.GetAsDouble();break;
					case 4:left=Valor.GetAsDouble();break;
					case 2:top=Valor.GetAsDouble();break;
					case 3:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("B,T,L,R"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:bottom=Valor.GetAsDouble();break;
					case 3:left=Valor.GetAsDouble();break;
					case 2:top=Valor.GetAsDouble();break;
					case 4:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("L,T,B,R"))
			{		
				switch(NumeroDeCoord)
				{
					case 3:bottom=Valor.GetAsDouble();break;
					case 1:left=Valor.GetAsDouble();break;
					case 2:top=Valor.GetAsDouble();break;
					case 4:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("L,T,R,B"))
			{		
				switch(NumeroDeCoord)
				{
					case 4:bottom=Valor.GetAsDouble();break;
					case 1:left=Valor.GetAsDouble();break;
					case 2:top=Valor.GetAsDouble();break;
					case 3:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("L,B,T,R"))
			{		
				switch(NumeroDeCoord)
				{
					case 2:bottom=Valor.GetAsDouble();break;
					case 1:left=Valor.GetAsDouble();break;
					case 3:top=Valor.GetAsDouble();break;
					case 4:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("L,B,R,T"))
			{		
				switch(NumeroDeCoord)
				{
					case 2:bottom=Valor.GetAsDouble();break;
					case 1:left=Valor.GetAsDouble();break;
					case 4:top=Valor.GetAsDouble();break;
					case 3:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("L,R,B,T"))
			{		
				switch(NumeroDeCoord)
				{
					case 3:bottom=Valor.GetAsDouble();break;
					case 1:left=Valor.GetAsDouble();break;
					case 4:top=Valor.GetAsDouble();break;
					case 2:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("L,R,T,B"))
			{		
				switch(NumeroDeCoord)
				{
					case 4:bottom=Valor.GetAsDouble();break;
					case 1:left=Valor.GetAsDouble();break;
					case 3:top=Valor.GetAsDouble();break;
					case 2:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("R,T,B,L"))
			{		
				switch(NumeroDeCoord)
				{
					case 3:bottom=Valor.GetAsDouble();break;
					case 4:left=Valor.GetAsDouble();break;
					case 2:top=Valor.GetAsDouble();break;
					case 1:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("R,T,L,B"))
			{		
				switch(NumeroDeCoord)
				{
					case 4:bottom=Valor.GetAsDouble();break;
					case 3:left=Valor.GetAsDouble();break;
					case 2:top=Valor.GetAsDouble();break;
					case 1:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("R,L,T,B"))
			{		
				switch(NumeroDeCoord)
				{
					case 4:bottom=Valor.GetAsDouble();break;
					case 2:left=Valor.GetAsDouble();break;
					case 3:top=Valor.GetAsDouble();break;
					case 1:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("R,L,B,T"))
			{		
				switch(NumeroDeCoord)
				{
					case 3:bottom=Valor.GetAsDouble();break;
					case 2:left=Valor.GetAsDouble();break;
					case 4:top=Valor.GetAsDouble();break;
					case 1:right=Valor.GetAsDouble();break;
				}	
				break;
			}
			if(SecuenciaCoord.Contains("R,B,L,T"))
			{		
				switch(NumeroDeCoord)
				{
					case 2:bottom=Valor.GetAsDouble();break;
					case 3:left=Valor.GetAsDouble();break;
					case 4:top=Valor.GetAsDouble();break;
					case 1:right=Valor.GetAsDouble();break;
				}	

				break;
			}
			if(SecuenciaCoord.Contains("R,B,T,L"))
			{		
				switch(NumeroDeCoord)
				{
					case 2:bottom=Valor.GetAsDouble();break;
					case 4:left=Valor.GetAsDouble();break;
					case 3:top=Valor.GetAsDouble();break;
					case 1:right=Valor.GetAsDouble();break;
				}	

				break;
			}
		}while(false);	
	
}

/**
*/
void A2PFluirFunction::ObtenerCoordenadasDeXY(PMString SecuenciaCoord,int32 NumeroDeCoord,PMReal &Y1,PMReal &X1,PMReal &Y2,PMReal &X2,PMString Valor)
{
		
		do
		{
			if(SecuenciaCoord.Contains("(X1,X2) (Y1,Y2)"))
			{
				switch(NumeroDeCoord)
				{
					case 3:Y1=Valor.GetAsDouble();break;
					case 1:X1=Valor.GetAsDouble();break;
					case 2:Y2=Valor.GetAsDouble();break;
					case 4:X2=Valor.GetAsDouble();break;
				}
				break;
			}
			if(SecuenciaCoord.Contains("(X1,X2) (Y2,Y1)"))
			{	
				switch(NumeroDeCoord)
				{
					case 4:Y1=Valor.GetAsDouble();break;
					case 1:X1=Valor.GetAsDouble();break;
					case 2:Y2=Valor.GetAsDouble();break;
					case 3:X2=Valor.GetAsDouble();break;
				}
				break;
			}
			if(SecuenciaCoord.Contains("(X1,Y1) (X2,Y2)"))
			{
				switch(NumeroDeCoord)
				{
					case 2:Y1=Valor.GetAsDouble();break;
					case 1:X1=Valor.GetAsDouble();break;
					case 4:Y2=Valor.GetAsDouble();break;
					case 3:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(X1,Y1) (Y2,X2)"))
			{	
				switch(NumeroDeCoord)
				{
					case 2:Y1=Valor.GetAsDouble();break;
					case 1:X1=Valor.GetAsDouble();break;
					case 3:Y2=Valor.GetAsDouble();break;
					case 4:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(X1,Y2) (X2,Y1)"))
			{	
				switch(NumeroDeCoord)
				{
					case 4:Y1=Valor.GetAsDouble();break;
					case 1:X1=Valor.GetAsDouble();break;
					case 2:Y2=Valor.GetAsDouble();break;
					case 3:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(X1,Y2) (Y1,X2)"))
			{		
				switch(NumeroDeCoord)
				{
					case 3:Y1=Valor.GetAsDouble();break;
					case 1:X1=Valor.GetAsDouble();break;
					case 2:Y2=Valor.GetAsDouble();break;
					case 4:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(X2,X1) (Y1,Y2)"))
			{		
				switch(NumeroDeCoord)
				{
					case 3:Y1=Valor.GetAsDouble();break;
					case 2:X1=Valor.GetAsDouble();break;
					case 4:Y2=Valor.GetAsDouble();break;
					case 1:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(X2,X1) (Y2,Y1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 4:Y1=Valor.GetAsDouble();break;
					case 2:X1=Valor.GetAsDouble();break;
					case 3:Y2=Valor.GetAsDouble();break;
					case 1:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(X2,Y1) (X1,Y2)"))
			{		
				switch(NumeroDeCoord)
				{
					case 2:Y1=Valor.GetAsDouble();break;
					case 3:X1=Valor.GetAsDouble();break;
					case 4:Y2=Valor.GetAsDouble();break;
					case 1:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(X2,Y1) (Y2,X1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 2:Y1=Valor.GetAsDouble();break;
					case 4:X1=Valor.GetAsDouble();break;
					case 3:Y2=Valor.GetAsDouble();break;
					case 1:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(X2,Y2) (X1,Y1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 4:Y1=Valor.GetAsDouble();break;
					case 3:X1=Valor.GetAsDouble();break;
					case 2:Y2=Valor.GetAsDouble();break;
					case 1:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(X2,Y2) (Y1,X1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 3:Y1=Valor.GetAsDouble();break;
					case 4:X1=Valor.GetAsDouble();break;
					case 2:Y2=Valor.GetAsDouble();break;
					case 1:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y1,X1) (X2,Y2)"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:Y1=Valor.GetAsDouble();break;
					case 2:X1=Valor.GetAsDouble();break;
					case 4:Y2=Valor.GetAsDouble();break;
					case 3:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y1,X1) (Y2,X2)"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:Y1=Valor.GetAsDouble();break;
					case 2:X1=Valor.GetAsDouble();break;
					case 3:Y2=Valor.GetAsDouble();break;
					case 4:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y1,Y2) (X1,X2)"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:Y1=Valor.GetAsDouble();break;
					case 3:X1=Valor.GetAsDouble();break;
					case 2:Y2=Valor.GetAsDouble();break;
					case 4:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y1,Y2) (X2,X1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:Y1=Valor.GetAsDouble();break;
					case 4:X1=Valor.GetAsDouble();break;
					case 2:Y2=Valor.GetAsDouble();break;
					case 3:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y1,X2) (Y2,X1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:Y1=Valor.GetAsDouble();break;
					case 4:X1=Valor.GetAsDouble();break;
					case 3:Y2=Valor.GetAsDouble();break;
					case 2:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y1,X2) (X1,Y2)"))
			{		
				switch(NumeroDeCoord)
				{
					case 1:Y1=Valor.GetAsDouble();break;
					case 3:X1=Valor.GetAsDouble();break;
					case 4:Y2=Valor.GetAsDouble();break;
					case 2:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y2,X1) (Y1,X2)"))
			{		
				switch(NumeroDeCoord)
				{
					case 3:Y1=Valor.GetAsDouble();break;
					case 2:X1=Valor.GetAsDouble();break;
					case 1:Y2=Valor.GetAsDouble();break;
					case 4:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y2,X1) (X2,Y1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 4:Y1=Valor.GetAsDouble();break;
					case 2:X1=Valor.GetAsDouble();break;
					case 1:Y2=Valor.GetAsDouble();break;
					case 3:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y2,Y1) (X1,X2)"))
			{		
				switch(NumeroDeCoord)
				{
					case 2:Y1=Valor.GetAsDouble();break;
					case 3:X1=Valor.GetAsDouble();break;
					case 1:Y2=Valor.GetAsDouble();break;
					case 4:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y2,Y1) (X2,X1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 2:Y1=Valor.GetAsDouble();break;
					case 4:X1=Valor.GetAsDouble();break;
					case 1:Y2=Valor.GetAsDouble();break;
					case 3:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y2,X2) (Y1,X1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 3:Y1=Valor.GetAsDouble();break;
					case 4:X1=Valor.GetAsDouble();break;
					case 1:Y2=Valor.GetAsDouble();break;
					case 2:X2=Valor.GetAsDouble();break;
				}

				break;
			}
			if(SecuenciaCoord.Contains("(Y2,X2) (X1,Y1)"))
			{		
				switch(NumeroDeCoord)
				{
					case 4:Y1=Valor.GetAsDouble();break;
					case 3:X1=Valor.GetAsDouble();break;
					case 1:Y2=Valor.GetAsDouble();break;
					case 2:X2=Valor.GetAsDouble();break;
				}

				break;
			}
		}while(false);	
}


/**
	Funcion que apilica un color de fondo al rectangulo de la imagen dependiendo las preferencias.
			
	  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
	  @param CPublicidad, Variable del tipo bool16 la cual contiene si se encontro o no la imagen que se debio importar.
	  @param frameList, la lista de frames o rectangulos de imagen.
*/
void A2PFluirFunction::AplicarFondoRecImagen(Preferencias &Prefer,bool16 CPublicidad,UIDRef textFrameUIDR)
{

	do
	{
			
		//UIDList itemList = frameList;
		//UID textFrameUID = kInvalidUID;
		
		//UIDRef textFrameUIDR=itemList.GetRef(itemList.Length() - num_rectangulos);//obtengo el UIDRef del rectangulo que se acaba de crear

		
		UID colorUID;				//UID del color
		if(CPublicidad)///si es una caja con publicidad
		{
			
			if(Prefer.ColorCajaImagenConPublicidad.Contains("Black"))
			{
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					
					break;
				}
			}
			else
			{
				if(Prefer.ColorCajaImagenConPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}
				}
				else
				{
					if(Prefer.ColorCajaImagenConPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==nil)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorCajaImagenConPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorCajaImagenConPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorCajaImagenConPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorCajaImagenConPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//Para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			if(Prefer.ColorCajaImagenSinPublicidad.Contains("Black"))
			{
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					break;
				}
			}
			else
			{
				if(Prefer.ColorCajaImagenSinPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}
				}
				else
				{
					if(Prefer.ColorCajaImagenSinPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==kInvalidUID)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorCajaImagenSinPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorCajaImagenSinPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorCajaImagenSinPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorCajaImagenSinPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//Para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		//obtengo la interfaz IPersistUIDData para alplicar el kGraphicStyleFillRenderingAttrBoss
		InterfacePtr<IPersistUIDData> fillRenderAttr((IPersistUIDData*)::CreateObject(kGraphicStyleFillRenderingAttrBoss,IID_IPERSISTUIDDATA));
		fillRenderAttr->SetUID(colorUID);
				
		//obtengo un comando
		InterfacePtr<ICommand>	gfxApplyCmd(CmdUtils::CreateCommand(kGfxApplyAttrOverrideCmdBoss));//se crea el comado para aplicar color al frame o rectangulo
				
		//pones el UIRef del frame al que se le va a plicar el color
		gfxApplyCmd->SetItemList(UIDList(textFrameUIDR));//

		//se obtiene la interfaz de IPMUnknownData para el comando
		InterfacePtr<IPMUnknownData> pifUnknown(gfxApplyCmd,UseDefaultIID());

		//pones el color
		pifUnknown->SetPMUnknown(fillRenderAttr);

		ErrorCode err = CmdUtils::ProcessCommand(gfxApplyCmd);//poocesamiento de comando para aplicar color al ultimo que se creo	
	}while(false);
}

/**
			Funcion que apilica un color de marco al rectangulo de la imagen dependiendo las preferencias.
			
			  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param CPublicidad, Variable del tipo bool16 la cual contiene si se encontro o no la imagen que se debio importar.
			  @param frameList, la lista de frames o rectangulos de imagen.
*/
void A2PFluirFunction::AplicarColorMarcoRecImagen(Preferencias &Prefer,bool16 CPublicidad,UIDRef textFrameUIDR)
{

	do
	{
		//////////////////////////////////////
		//    Para poner color a la caja    //
		//////////////////////////////////////
	//	UIDList itemList = frameList;
	//	UID textFrameUID = kInvalidUID;
		
	//	UIDRef textFrameUIDR=itemList.GetRef(itemList.Length() - num_rectangulos);//obtengo el UIDRef del rectangulo que se acaba de crear
		if(textFrameUIDR==nil)//
		{
			break;
		}
		
/*		UID a=textFrameUIDR.GetUID();//OBTENGO EL UID del rectangulo
		uint32 b=a.Get();			//obtengo el valor en exadecimal del rectangulo
		int c=uint32(b);			//Convierto el valor exadecimal a entero
		NumIDFrame.Clear();			//limpio el valor del anterior rectangulo
		NumIDFrame.AppendNumber(c);	//pungo el ID en valor decimal en el string 
		*/
		UID colorUID;//UID del color deseado

		if(CPublicidad)///si es una caja con publicidad
		{
			

			//////////////////////////////////////////////////////////////////////
			if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Black")) 
			{ 
				//lo mismo que el anterior solo que ahora utilizamos el kGraphicStyleStrokeRenderingAttrBoss para aplicar el color al marco
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					break;
				}
			}
			else
			{
				if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}
				}
				else
				{
					if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==nil)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorMargenCajaImagenConPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			//////////////////////////////////////////////////////////////////////
			
			if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Black"))
			{
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					break;
				}
			}
			else
			{
				if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}
				}
				else
				{
					if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==kInvalidUID)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorMargenCajaImagenSinPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		InterfacePtr<IPersistUIDData> strokeRenderAttr((IPersistUIDData*)::CreateObject(kGraphicStyleStrokeRenderingAttrBoss,IID_IPERSISTUIDDATA));
		strokeRenderAttr->SetUID(colorUID);
				
		InterfacePtr<ICommand>	gfxApplyCmd(CmdUtils::CreateCommand(kGfxApplyAttrOverrideCmdBoss));//se crea el comado para aplicar color al frame o rectangulo
				
		gfxApplyCmd->SetItemList(UIDList(textFrameUIDR));//

		InterfacePtr<IPMUnknownData> pifUnknown(gfxApplyCmd,UseDefaultIID());

		pifUnknown->SetPMUnknown(strokeRenderAttr);

		ErrorCode err = CmdUtils::ProcessCommand(gfxApplyCmd);//poocesamiento de comando para aplicar color al ultimo que se creo	

	}while(false);
}

/**
			Funcion que apilica el ajuste de la imagen en rectangulo dependiendo las preferencias.
			
			  @param Prefer,Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param frameList, la lista de frames o rectangulos de imagen.
*/
void A2PFluirFunction::AplicarAjusteRecImagen(Preferencias &Prefer,UIDRef GFrameUIDRef)
{
	do
	{
		UIDRef contentUIDRef=UIDRef::gNull;
		//Verifica si existe la caja de imagen
		if(GFrameUIDRef==nil)//
		{
			//CAlert::ErrorAlert("No encontro frame");
			break;
		}
		
		InterfacePtr<IGraphicFrameData> graficFrame(GFrameUIDRef,UseDefaultIID());
		if(GFrameUIDRef!=nil && GFrameUIDRef!=UIDRef::gNull && graficFrame!=nil)
		{
			
			bool16 updatePorFecha = kFalse;
				
			InterfacePtr<IHierarchy> graphicFrameHierarchy(GFrameUIDRef, UseDefaultIID());
			if(graphicFrameHierarchy!=nil)
			{
				for(int32 i=0;i<graphicFrameHierarchy->GetChildCount() ; i++)
						contentUIDRef= UIDRef(GFrameUIDRef.GetDataBase(), graphicFrameHierarchy->GetChildUID(i));
						//graphicFrameHierarchy->GetChildUID(i);
			}
			else
				break;
				
		}
		else
			break;
		
		if(GFrameUIDRef==nil || GFrameUIDRef==UIDRef::gNull)
			break;

		if(Prefer.FitCajaImagenConPublicidad.Contains("kA2PPrefAjusteContenidoAFrameStringKey"))
		{
			// Create a FitContentToFrameCmd:
			InterfacePtr<ICommand>	fitCmd(CmdUtils::CreateCommand(kFitContentToFrameCmdBoss));
			// Set the FitContentToFrameCmd's ItemList:
			fitCmd->SetItemList(UIDList(contentUIDRef));
			// Process the FitContentToFrameCmd:
			
			if (CmdUtils::ProcessCommand(fitCmd) != kSuccess)
			{
				CAlert::ErrorAlert("Error al intentar ajustar imagen");
			}
			//CAlert::ErrorAlert("Contenido a frame");
		}
		else
		{	if(Prefer.FitCajaImagenConPublicidad.Contains("kA2PPrefAjusteCentrarStringKey"))
			{
				// Create a FitContentToFrameCmd:
				InterfacePtr<ICommand>	fitCmd(CmdUtils::CreateCommand(kCenterContentInFrameCmdBoss));
				// Set the FitContentToFrameCmd's ItemList:
				fitCmd->SetItemList(UIDList(contentUIDRef));
				// Process the FitContentToFrameCmd:
				if (CmdUtils::ProcessCommand(fitCmd) != kSuccess)
				{
					CAlert::ErrorAlert("Error al intentar centrar imagen");
				}
				//CAlert::ErrorAlert("Centrar contenido");
			}
			else
			{
				if(Prefer.FitCajaImagenConPublicidad.Contains("kA2PPrefAjusteProporcionalmenteStringKey"))
				{
					// Create a FitContentToFrameCmd:
					InterfacePtr<ICommand>	fitCmd(CmdUtils::CreateCommand(kFitContentPropCmdBoss));
					// Set the FitContentToFrameCmd's ItemList:
					fitCmd->SetItemList(UIDList(contentUIDRef));
					// Process the FitContentToFrameCmd:
					if (CmdUtils::ProcessCommand(fitCmd) != kSuccess)
					{
						CAlert::ErrorAlert("Error al intentar centrar imagen");
					}
					//CAlert::ErrorAlert("Cont propor");
				}
				else
				{
					//CAlert::ErrorAlert("Sin Ajuste");
					///Sin Ajuste
				}
			}
		}
	}while(false);
}

/**
			Funcion que copia los campos del aviso que se fluye y deben ponerse en la etiqueta dependiendo preferencias, en esta funcion se llama a la funcion CrearRectanguloEtiqueta.
			
			  @param Prefer, Structura del tipo Preferencias, conrtiene las preferencias a aplicar en el rectangulo.
			  @param AvisoAFluir, Structura de avisos.
			  @param numAviso, numero del aviso que se fluyo.
			  @param CPublicidad, si se encontro  o no la imagen.
			  @param leftMargin, coordenadas y1.
			  @param topMargin, coordenadas x1.
			  @param rightMargin, coordenadas y2.
			  @param bottomMargin, coordenadas x2.
*/
int32 A2PFluirFunction::TextodeEtiqueta(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso,bool16 CPublicidad,PMReal leftMargin,PMReal topMargin,PMReal rightMargin,PMReal bottomMargin)
{
	int32 numCampo;			//numero de campo que controla el ciclo
	int32 NumCampodePref;	//numero de campo que se obtiene de cada campo de preferencias
	PMString NomCampoPref;	//nombre de campo que corresponde a este numero de campo(NumCampodePref)
	PMString CadenaAEnviar;	//cadena con la lista de campos a enviar
	CadenaAEnviar.Clear();	//limpia cadena a enviar

	for(numCampo=1;numCampo<15;numCampo++)//ciclo para obterer el nombre del campo deependiendo su numero
										//he ir adicionandolo a la variable que contendra todo el texto que debera ponerse en la etiqueta
	{
		if(CPublicidad)
		{
			do
			{
				NomCampoPref.Clear();

				NumCampodePref=Prefer.PosCamposEtiCon1.GetAsNumber();//busqueta del numero de campo y comparacion con el numero de campo actual
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon1;//si es el numero de campo que correponde  a preferencias
					break;								//se obtiene el nombre de campo
				}
				NumCampodePref=Prefer.PosCamposEtiCon2.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon2;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon3.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon3;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon4.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon4;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon5.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon5;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon6.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon6;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon7.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon7;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon8.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon8;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon9.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon9;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon10.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon10;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon11.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon11;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon12.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon12;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon13.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon13;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiCon14.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiCon14;
					break;
				}
			}while(false);
		}
		else
		{
			do
			{
				NomCampoPref.Clear();
				NumCampodePref=Prefer.PosCamposEtiSin1.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin1;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin2.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin2;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin3.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin3;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin4.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin4;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin5.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin5;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin6.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin6;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin7.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin7;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin8.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin8;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin9.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin9;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin10.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin10;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin11.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin11;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin12.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin12;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin13.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin13;
					break;
				}
				NumCampodePref=Prefer.PosCamposEtiSin14.GetAsNumber();
				if(NumCampodePref==numCampo)
				{
					NomCampoPref=Prefer.NomCamposEtiSin14;
					break;
				}
			}while(false);
		}////en else
		

		//una vez obtenido el nombre del campo se hace la busqueda en avisos a fluir y se compara con cada uno de los campo
		//al encontrar este se adiciona cadena a enviar
		do
		{
			if(NomCampoPref.NumUTF16TextChars()==0)//si no tiene un nombre de campo no se adiciona el texto
				break;
		
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo1)//si el nombre del campo corresponde con el nombre de campo de alguno de los campos del aviso
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo1);//se agreaga el contenido a la cadena a enviar
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo2)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo2);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo3)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo3);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo4)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo4);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo5)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo5);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo6)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo6);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo7)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo7);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo8)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo8);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo9)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo9);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo10)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo10);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo11)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo11);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo12)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo12);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo13)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo13);
				CadenaAEnviar.Append(" || ");
				break;
			}
			if(NomCampoPref==AvisoAFluir[numAviso].NomCampo14)
			{
				CadenaAEnviar.Append(AvisoAFluir[numAviso].ContenidoCampo14);
				CadenaAEnviar.Append(" || ");
				break;
			}
		}while(false);
		
			
	}//fin for
	
	//CAlert::InformationAlert(CadenaAEnviar);
	return(CrearRectanguloEtiqueta(Prefer,CadenaAEnviar,CPublicidad,leftMargin,topMargin,rightMargin,bottomMargin));
}

/**
*/
int32 A2PFluirFunction::CrearRectanguloEtiqueta(Preferencias &Prefer,PMString CadenaAImprimir,bool16 CPublicidad,PMReal leftMargin,PMReal topMargin,PMReal rightMargin,PMReal bottomMargin)
{	
	int32 UIDIntegerFrameEtiqueta=0;
	do
	{
		if(CPublicidad)//activa la capa determinada depencdiendo si existen
		{
			if(Prefer.CreaEtiquetaConPublicidad==1)
				ActivarCapa("Etiquetas C/Publicidad");
			else
				break;

		}
		else
		{
			if(Prefer.CreaEtiquetaSinPublicidad==1)
				ActivarCapa("Etiquetas S/Publicidad");
			else
				break;
		}
		
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutControlData == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			break;
		}
		
		InterfacePtr<IHierarchy> activeSpreadLayerHierarchy(layoutControlData->QueryActiveLayer());
		ASSERT(activeSpreadLayerHierarchy != nil);
		if (activeSpreadLayerHierarchy == nil) {
			break;
		}
		UIDRef parentUIDRef = ::GetUIDRef(activeSpreadLayerHierarchy);

		// Transform the bounds of the frame from page co-ordinates
		// into the parent co-ordinates, i.e. the spread.
		ASSERT(layoutControlData->GetPage() != kInvalidUID);
		if (layoutControlData->GetPage() == kInvalidUID) {
			break;
		}
		UIDRef pageUIDRef(parentUIDRef.GetDataBase(), layoutControlData->GetPage());
		
						
		
		
		PMRect CoordenadasChidas=PMRect(leftMargin, topMargin, rightMargin ,topMargin+((bottomMargin-topMargin)/2));
		
		SDKLayoutHelper helperlayout;
		UIDRef outFrameUIDRef=helperlayout.CreateTextFrame (parentUIDRef, CoordenadasChidas);
		
		InterfacePtr<IGraphicFrameData> graphicFrameData(outFrameUIDRef,UseDefaultIID());
		if(graphicFrameData==nil)
			break;
			
		UIDRef 	UidRefTextModel=helperlayout.GetTextModelRef(graphicFrameData);
			
		UID UIDFrameEtiqueta = outFrameUIDRef.GetUID();
		UIDIntegerFrameEtiqueta = UIDFrameEtiqueta.Get();
			
		//aplica el tama‚àö√≠o de marco
		//AplicarTamanoMarcoEtiqueta(Prefer,outFrameUIDRef,CPublicidad);

		//aplica color de fondo al frame
		AplicarFondoTextFrame(Prefer,CPublicidad,outFrameUIDRef);
			

		fStep++; // Increment how many frames we've created.

		//serie de conversiones del texto a insertar
		PMString textToInsert(CadenaAImprimir);
		textToInsert.Translate(); // Look up our string and replace.
				
		//K2::shared_ptr<WideString> myText(new WideString(textToInsert));
		boost::shared_ptr<WideString> myText(new WideString(textToInsert));		
		//obtiene el Textmodel del frame 
		InterfacePtr<ITextModel> textModel(UidRefTextModel, ITextModel::kDefaultIID);
		if (textModel == nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			break;
		}

		InterfacePtr<ITextModelCmds> iTextModelCmd(textModel,UseDefaultIID());
		if(textModel==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			break;
		}

		//crea un comando para insertar el textmodel y el nuevo texto en el frame
		InterfacePtr<ICommand> insertTextCommand (iTextModelCmd->InsertCmd(0, myText, kFalse));
		if (insertTextCommand == nil)
		{
			ASSERT_FAIL("ICommand pointer nil");
			break;
		}

		//procesa comando
		if(CmdUtils::ProcessCommand(insertTextCommand) != kSuccess)
		{
			CAlert::ModalAlert
			(
			   CadenaAImprimir,			// Alert string
			   kOKString,	// OK button
			   kNullString,	// No second button
			   kNullString,	// No third button
			   1,									// Set OK button to default
			   CAlert::eWarningIcon					// Warning icon.
			);			
		}

		//cambia el color de texto
		CambiarColorTexto(textModel,Prefer,CPublicidad);
	
	} while(kFalse);
	
	return(UIDIntegerFrameEtiqueta);
}

void A2PFluirFunction::AplicarFondoTextFrame(Preferencias &Prefer,bool16 CPublicidad,const UIDRef &textFrameUIDR)
{

	do
	{
		//////////////////////////////////////
		//    Para poner color a la caja    //
		//////////////////////////////////////

		
		if(textFrameUIDR==nil)//
		{ 
			CAlert::InformationAlert("sin UIDRef text frame");
			break;
		}
		UID colorUID;
		if(CPublicidad)///si es una caja con publicidad
		{
		
			if(Prefer.ColorFondoEtiquetaConPublicidad.Contains("Black"))
			{
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					break;
				}
			}
			else
			{
				if(Prefer.ColorFondoEtiquetaConPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}
				}
				else
				{
					if(Prefer.ColorFondoEtiquetaConPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==nil)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorFondoEtiquetaConPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorFondoEtiquetaConPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorFondoEtiquetaConPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorFondoEtiquetaConPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//Para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		else
		{
			if(Prefer.ColorFondoEtiquetaSinPublicidad.Contains("Black"))
			{
				InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
				colorUID=iSwatchList->GetBlackSwatchUID();//obtengo el UID del color black
				if(colorUID==kInvalidUID)
				{
					break;
				}
			}
			else
			{
				if(Prefer.ColorFondoEtiquetaSinPublicidad.Contains("White"))
				{
					InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
					colorUID=iSwatchList->GetPaperSwatchUID();//obtengo el UID del color black
					if(colorUID==kInvalidUID)
					{
						break;
					}	
				}
				else
				{
					if(Prefer.ColorFondoEtiquetaSinPublicidad.Contains("Red"))
					{
						InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
						UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Red");//obtengo el UID del color black
						colorUID=colorUIDR.GetUID();
						if(colorUID==kInvalidUID)
						{
							break;
						}
					}
					else
					{
						if(Prefer.ColorFondoEtiquetaSinPublicidad.Contains("Blue"))
						{
							InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
							UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Blue");//obtengo el UID del color black
							colorUID=colorUIDR.GetUID();
							if(colorUID==kInvalidUID)
							{
								break;
							}
						}
						else
						{
							if(Prefer.ColorFondoEtiquetaSinPublicidad.Contains("Yellow"))
							{
								InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
								UIDRef colorUIDR=iSwatchList->FindSwatch("A2P Yellow");//obtengo el UID del color black
								colorUID=colorUIDR.GetUID();
								if(colorUID==kInvalidUID)
								{
									break;
								}
							}
							else
							{
								if(Prefer.ColorFondoEtiquetaSinPublicidad.Contains("None"))
								{
									InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
									colorUID=iSwatchList->GetNoneSwatchUID();//obtengo el UID del color black
									
									if(colorUID==kInvalidUID)
									{
										break;
									}
								}
								else
								{
									if(Prefer.ColorFondoEtiquetaSinPublicidad.Contains("Register"))
									{
										InterfacePtr<ISwatchList> iSwatchList(Utils<ISwatchUtils> ()->QueryActiveSwatchList());//obtengo la lista de muestras que se encuentra en nuestro documento
										colorUID=iSwatchList->GetRegistrationSwatchUID();//obtengo el UID del color black
										if(colorUID==kInvalidUID)
										{
											break;
										}
									}
									else
									{
										break;	//Para no aplicar color
									}
								}
							}
						}
					}
				}
			}
		}
		InterfacePtr<IPersistUIDData> fillRenderAttr((IPersistUIDData*)::CreateObject(kGraphicStyleFillRenderingAttrBoss,IID_IPERSISTUIDDATA));
		fillRenderAttr->SetUID(colorUID);
				
		InterfacePtr<ICommand>	gfxApplyCmd(CmdUtils::CreateCommand(kGfxApplyAttrOverrideCmdBoss));//se crea el comado para aplicar color al frame o rectangulo
				
		gfxApplyCmd->SetItemList(UIDList(textFrameUIDR));//

		InterfacePtr<IPMUnknownData> pifUnknown(gfxApplyCmd,UseDefaultIID());

		pifUnknown->SetPMUnknown(fillRenderAttr);

		ErrorCode err = CmdUtils::ProcessCommand(gfxApplyCmd);//poocesamiento de comando para aplicar color al ultimo que se creo	
	}while(false);
}

void A2PFluirFunction::AplicarTamanoMarcoEtiqueta(Preferencias &Prefer,UIDRef &textFrameUIDR,bool16 CPublicidad)
{
/*	//EN ESTE MOMENTO NO TRABAJA ESTA FUNCION ESPERANDO A UN UPDATE
	//PROBLEMA: NO SE HAN ENCONTRADO ALGUNA INSTRUCCION PARA APLICAR O CAMBIAR EL TAMA‚Äö√Ñ√ÆO DE TEXTO A UN TEXTFRAME
	//SOLO SE HAN ENCONTRADO ESTO PARA LOS GRAFICS FRAMES.

	//conversion de textframe a grafic frame
	this->CreateAndProcessConvertItemToFrameCmd(textFrameUIDR);
	CAlert::InformationAlert("Cambio?");

	UIDList newSplineItemList(textFrameUIDR);
	ErrorCode seqStatus = kSuccess;

	do
	{
		////////////primero aplico el tama‚àö√≠o del marco del graficframe///////
		if(CPublicidad)
		{
			InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->CreateStrokeWeightCommand(30.0,&newSplineItemList,kTrue,kTrue));
			if(strokeSplineCmd==nil)
			{
				seqStatus = kFailure;
				CAlert::InformationAlert("cannot create the command to apply stroke");
				break;
			}
			seqStatus = CmdUtils::ProcessCommand(strokeSplineCmd);
			if (seqStatus ==kFailure)
			{
				CAlert::InformationAlert("error when processing command to apply stroke");
				break;
			}
		}
		else
		{
			InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->CreateStrokeWeightCommand(PMReal(1.0),&newSplineItemList,kTrue,kTrue));
			if(strokeSplineCmd==nil)
			{
				seqStatus = kFailure;
				CAlert::InformationAlert("cannot create the command to apply stroke");
				break;
			}
			seqStatus = CmdUtils::ProcessCommand(strokeSplineCmd);
			if (seqStatus ==kFailure)
			{
				CAlert::InformationAlert("error when processing command to apply stroke");
				break;
			}
		}
	}while(false);


	this->CreateAndProcessConvertItemToTextCmd(textFrameUIDR);
	//conversion de grafic frame a textframe
	CAlert::InformationAlert("Cambio2?");
*/
/////////////////SETAS FUNCIONES SOLO TRABAJAN CON GRAFICFRAMES///////////////////////////

/*	do
	{

		InterfacePtr<IPMUnknown> iStrokeimpnown( textFrameUIDR,IID_IUNKNOWN);
		if( iStrokeimpnown == nil )
		{
			CAlert::InformationAlert("Error 1");
			break;
		}
		

		CAlert::InformationAlert("Error 2");
		InterfacePtr<IStrokePref> iStroke(iStrokeimpnown,IID_ISTROKECHANGEPREFERENCE);
		CAlert::InformationAlert("Error 3");
		if( iStroke == nil )
		{
			CAlert::InformationAlert("Error 4");
			break;
		}
		CAlert::InformationAlert("Error 5");
		iStroke->SetWeight(20);
		/*PMString p;
		p.AppendNumber(j);
		CAlert::InformationAlert(p);
		CAlert::InformationAlert("Error 6");
	}while(false);*/

	//	SOLO CON GRAFICFRAME
/*		UIDList newSplineItemList(textFrameUIDR);
		ErrorCode seqStatus = kSuccess;

		InterfacePtr<IGraphicStyleDescriptor> desc (textFrameUIDR,UseDefaultIID());
		//stroke weight
		InterfacePtr<IGraphicAttrRealNumber> strWeightAttr (desc->QueryAttribute(kGraphicStyleStrokeWeightAttrBoss), IID_IGRAPHICATTR_REALNUMBER);
		PMReal TamMarco=Prefer.TamMargenEtiquetaConPublicidad.GetAsDouble();
		strWeightAttr->SetRealNumber(15);
*/		
		//UIDList newSplineItemList(textFrameUIDR);

/*	
		InterfacePtr<ICommand> setWeightCmd(Utils<IGraphicAttributeUtils>()->CreateStrokeWeightCommand (15, &UIDList(textFrameUIDR), kTrue, kTrue));
		  if (setWeightCmd != nil)
		  {
			 if (CmdUtils::ProcessCommand(setWeightCmd) != kSuccess)
			 {
				 CAlert::InformationAlert("cannot create the command to apply stroke");
			 }
		  } 
		  
*/

	/*do
	{
		////////////primero aplico el tama‚àö√≠o del marco del graficframe///////
		if(CPublicidad)
		{
			InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->CreateStrokeWeightCommand(30.0,&newSplineItemList,kTrue,kTrue));
			if(strokeSplineCmd==nil)
			{
				seqStatus = kFailure;
				CAlert::InformationAlert("cannot create the command to apply stroke");
				break;
			}
			seqStatus = CmdUtils::ProcessCommand(strokeSplineCmd);
			if (seqStatus ==kFailure)
			{
				CAlert::InformationAlert("error when processing command to apply stroke");
				break;
			}
		}
		else
		{
			InterfacePtr<ICommand> strokeSplineCmd (Utils<IGraphicAttributeUtils>()->CreateStrokeWeightCommand(PMReal(1.0),&newSplineItemList,kTrue,kTrue));
			if(strokeSplineCmd==nil)
			{
				seqStatus = kFailure;
				CAlert::InformationAlert("cannot create the command to apply stroke");
				break;
			}
			seqStatus = CmdUtils::ProcessCommand(strokeSplineCmd);
			if (seqStatus ==kFailure)
			{
				CAlert::InformationAlert("error when processing command to apply stroke");
				break;
			}
		}
	}while(false);*/

}

/*ConvertItemToFrameCmd*/
void A2PFluirFunction::CreateAndProcessConvertItemToFrameCmd(const UIDRef& itemToConvert)
{
	// Create a ConvertItemToFrameCmd:
	InterfacePtr<ICommand>
	convertCmd(CmdUtils::CreateCommand(kConvertItemToFrameCmdBoss));
	// Set the ConvertItemToFrameCmd's ItemList:
	convertCmd->SetItemList(UIDList(itemToConvert));
	// Process the ConvertItemToFrameCmd:
	if (CmdUtils::ProcessCommand(convertCmd) != kSuccess)
	{
		ASSERT_FAIL("Can't process ConvertItemToFrameCmd");
		//CAlert::ErrorAlert("Can't process ConvertItemToFrameCmd");
	}
}


/*ConvertItemToTextCmd*/
void A2PFluirFunction::CreateAndProcessConvertItemToTextCmd(const UIDRef& selectedItem)
{
	// Create a ConvertItemToTextCmd:
	InterfacePtr<ICommand>
	convertITTCmd(CmdUtils::CreateCommand(kConvertItemToTextCmdBoss));
	// Set the ConvertItemToTextCmd's ItemList:
	convertITTCmd->SetItemList(UIDList(selectedItem));
	// Process the ConvertItemToTextCmd:
	if (CmdUtils::ProcessCommand(convertITTCmd) != kSuccess)
	{
		ASSERT_FAIL("Can't process ConvertItemToTextCmd");
		//CAlert::ErrorAlert("Can't process ConvertItemToTextCmd");
	}
}
/**
	funcion que manda llamar a la creacion de los colores necesarios para ads2page
*/
void A2PFluirFunction::crearColores()
{
	//llamada a la funcion buscaOcreaColor mandando el nombre del color y la mescla de los colores para generar elm deseado
	BuscarOcrearColor("A2P Red",0,100,100,0);
	BuscarOcrearColor("A2P Blue",100,0,0,0);
	BuscarOcrearColor("A2P Yellow",0,0,100,0);
}

/**
	ESTA BUNCION SU FUNCION PRINCIPAL ES LA BUSQUEDA DE UN COLOR POR SU NOMBRE EN CASO
	DE QUE NO EXISTA SE DEBERA CREAR USANDO LA MEZCLA DE COLORES QUE SE LE MANDA
*/
UID A2PFluirFunction::BuscarOcrearColor(const PMString& swatchName, 
								  const PMReal rCyan, 
								  const PMReal rMagenta, 
								  const PMReal rAmarillo,
								  const PMReal rNegro)
{

	//para indicar el tipo de color CMYK
	//Trace("ResolveCMYKColorSwatch\n"); // Make debugging easier.

	//UID del color
	UID colorUID = kInvalidUID;
	do
	{
		//Documento
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
		{
			// How did we POSSIBLY get here when our menu item should have been disabled if we
			// have no document and have no selection?
			ASSERT_FAIL("TSCore::ResolveCMYKColorSwatch: document invalid");
			break;
		}	

		//obtien la lista de colores en el documento actual
		InterfacePtr<ISwatchList> swatchList(document->GetDocWorkSpace(), IID_ISWATCHLIST);
		if (swatchList == nil)
		{
			ASSERT_FAIL("TSCore::ResolveRGBColorSwatch: swatchList invalid");
			break;
		}

		
		PMString NombreColor=swatchName;
		NombreColor.Translate();
		NombreColor.SetTranslatable(kTrue);
		//busqueda del color por nombre y regresaa el UIDRef
		UIDRef colorUIDRef = swatchList->FindSwatch(NombreColor);
		
		if (colorUIDRef != nil)//en caso de que se encontro
		{
			colorUID = colorUIDRef.GetUID();//obtenge el UID del color
		}
		else//en caso de que no se haya encontrado se crea
		{
			// Must create the color.  Didn't find it in swatch list.
			
			// First create our command and get at the data:
		    InterfacePtr<ICommand> newColorCmd(CmdUtils::CreateCommand(kNewColorCmdBoss));

			// We'll be adding to the swatch list:
			InterfacePtr<IUIDData> swatchListData(newColorCmd, IID_IUIDDATA);
			if (swatchListData == nil)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: swatchListData invalid");
				break;
			}		

			//se adiciona la lista de colores al comando
			swatchListData->Set(swatchList);

			// Create the rendering object:
		    InterfacePtr<IRenderingObject> renderingObject(newColorCmd, IID_IRENDERINGOBJECT);
		    if (renderingObject == nil)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: renderingObject invalid");
				break;
			}		
			
		    // Fill out the name:
			renderingObject->SetSwatchName(NombreColor);

		    // Supply the actual color via the colorData interface:
		    InterfacePtr<IColorData> colorData(newColorCmd, IID_ICOLORDATA);
		    
		    const int32 colorSpace = (kPMCsCalCMYK);	//CMYK color space.

		    // Create the color array:
			ColorArray rgbColor;
	
			rgbColor.push_back(rCyan); // Cyan (0.0...1.0)
			rgbColor.push_back(rMagenta); // MAgenta (0.0...1.0)
			rgbColor.push_back(rAmarillo); // Yellow (0.0...1.0)
			rgbColor.push_back(rNegro); // Black (0.0...1.0)
			
			//pone el array del colore
		    colorData->SetColorData(colorSpace, rgbColor);

			//procesa el comando
			ErrorCode error = CmdUtils::ProcessCommand(newColorCmd);
			if (error != kSuccess)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: newColorCmd failed");
				break;
			}

			// Now get our UID out of the list and add it to the swatch:
		    const UIDList* colorUIDList = newColorCmd->GetItemList();
		    if (colorUIDList == nil)
		    {
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: UIDList invalid");
				break;
			}
		    
		    // We're only going to add the very first color here, not the whole list:
		    colorUID = colorUIDList->First();

			// As of build 325 we have to manually add to the Swatch list,
			// it's no longer part of the NewColorCmd:
			InterfacePtr<ICommand> addSwatchesCmd(CmdUtils::CreateCommand(kAddSwatchesCmdBoss));
			InterfacePtr<IUIDData> addSwatchesCmdListData(addSwatchesCmd, IID_IUIDDATA);
			
			if (addSwatchesCmdListData == nil || addSwatchesCmd == nil)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: addSwatches invalid");
				break;
			}
			
			addSwatchesCmd->SetName(kA2PImpStringAddSwatchCommandKey);				
			
			addSwatchesCmdListData->Set(swatchList);
			addSwatchesCmd->SetItemList(UIDList(colorUIDList->GetDataBase(), colorUID));

			// Finally, add the color to the swatch list via this command:
			error = CmdUtils::ProcessCommand(addSwatchesCmd);
			if (error != kSuccess)
			{
				ASSERT_FAIL("TSCore::CreateRGBColorSwatch: addSwatchesCmd failed");
				break;
			}

			// Now let's try to find the color from the swatch list:
			colorUIDRef = swatchList->FindSwatch(NombreColor);

			if (colorUIDRef == nil)
			{
				ASSERT_FAIL("TSCore::CreateStyle: even after new color, cannot find swatch");
				break;
			}
			
			colorUID = colorUIDRef.GetUID();
		}
	} while (false);
	return colorUID;
}


	
/**
	Su funcion es la de borrar el frame del textUpdate esto sucede cuando se fluye
	sobre un documento previamente creado por ads2Page 2.0
*/
void A2PFluirFunction::BorraTextoUpdate()
{
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
			break;

		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		ILayoutControlData* layout=Utils<ILayoutUIUtils>()->QueryFrontLayoutData();
		if (layout == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			break;
		}
		//Para obtener los frames de mi documento
		// Get the list of spreads in the document
		IHierarchy* Herraquia=layout->QueryActiveLayer();
		int32 Cont=Herraquia->GetChildCount();
		UID RefUPdate=Herraquia->GetChildUID(Cont-1);
		UIDRef Ref(db,RefUPdate);

		if(Utils<IPageItemTypeUtils>()->IsTextFrame(Ref) == kTrue)
		{
			InterfacePtr<IScrapItem> frameScrap(Ref, IID_ISCRAPITEM);
			// Use an IScrapItem's GetDeleteCmd() to create a DeleteFrameCmd:
			InterfacePtr<ICommand> deleteFrame(frameScrap->GetDeleteCmd());
			// Process the DeleteFrameCmd:
			if (CmdUtils::ProcessCommand(deleteFrame) != kSuccess)
			{		
				break;
			}
		}
		else
		{break;}
	}while (false); // only do once
}

/**
	funcion que cambia el color de texto de una etiqueta dependiento preferencias
*/
void A2PFluirFunction::CambiarColorTexto(InterfacePtr<ITextModel> textModel,Preferencias &Prefer,bool16 CPublicidad)
{
	do
	{
			///aplicar sin color a texto update
			if (textModel == nil)
			{
				ASSERT_FAIL("ITextModel pointer nil");
				break;
			}

			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				// How did we POSSIBLY get here when our menu item should have been disabled if we
				// have no document and have no selection?
				ASSERT_FAIL("TSCore::ResolveRGBColorSwatch: document invalid");
				break;
			}	

			ITextModel* textmodel=textModel;
			InterfacePtr<ISwatchList> swatchList(document->GetDocWorkSpace(), IID_ISWATCHLIST);
			if (swatchList == nil)
			{
				ASSERT_FAIL("TSCore::ResolveRGBColorSwatch: swatchList invalid");
				break;
			}
			UIDRef colorUIDR;
			UID colorUID ;
			/*
				virtual UID		GetBlackSwatchUID () const = 0;
				virtual UID		GetPaperSwatchUID () const = 0;
				virtual UID		GetRegistrationSwatchUID () const = 0;
				virtual UID		GetNoneSwatchUID () const = 0;
				*/
			if(CPublicidad)
			{
				if(Prefer.ColorTextEtiquetaConPublicidad.Contains("Black"))
				{
					colorUID =  swatchList->GetBlackSwatchUID();
					if(colorUID==nil)
					{	
						//CAlert::InformationAlert("no encontro UID del color");	
						break;
					}
				}
				else
				{
					if(Prefer.ColorTextEtiquetaConPublicidad.Contains("White"))
					{
						colorUID =  swatchList->GetPaperSwatchUID();
						if(colorUID==nil)
						{	
							//CAlert::InformationAlert("no encontro UID del color");	
							break;
						}
					}
					else
					{
						if(Prefer.ColorTextEtiquetaConPublicidad.Contains("Register"))
						{
							colorUID =  swatchList->GetRegistrationSwatchUID();
							if(colorUID==nil)
							{	
								//CAlert::InformationAlert("no encontro UID del color");	
								break;
							}
						}
						else
						{
							if(Prefer.ColorTextEtiquetaConPublicidad.Contains("None"))
							{
								colorUID =  swatchList->GetNoneSwatchUID();
								if(colorUID==nil)
								{	
									//CAlert::InformationAlert("no encontro UID del color");	
									break;
								}
							}
							else
							{
								if(Prefer.ColorTextEtiquetaConPublicidad.Contains("Red"))
								{
									colorUIDR =  swatchList->FindSwatch("A2P Red");
									colorUID=colorUIDR.GetUID();
									if(colorUID==nil)
									{	
										//CAlert::InformationAlert("no encontro UID del color");	
										break;
									}
								}
								else
								{
									if(Prefer.ColorTextEtiquetaConPublicidad.Contains("Blue"))
									{
										colorUIDR =  swatchList->FindSwatch("A2P Blue");
										colorUID=colorUIDR.GetUID();
										if(colorUID==nil)
										{	
											//CAlert::InformationAlert("no encontro UID del color");	
											break;
										}
									}
									else
									{
										if(Prefer.ColorTextEtiquetaConPublicidad.Contains("Yellow"))
										{
											colorUIDR =  swatchList->FindSwatch("A2P Yellow");
											colorUID=colorUIDR.GetUID();
											if(colorUID==nil)
											{	
												//CAlert::InformationAlert("no encontro UID del color");	
												break;
											}
										}
										else
										{
											break; //Texto sin color
										}
									}
								}
							}
						}
					}
				}
			}
			else
			{
				if(Prefer.ColorTextEtiquetaSinPublicidad.Contains("Black"))
				{
					colorUID =  swatchList->GetBlackSwatchUID();
					if(colorUID==nil)
					{	
						//CAlert::InformationAlert("no encontro UID del color");	
						break;
					}
				}
				else
				{
					if(Prefer.ColorTextEtiquetaSinPublicidad.Contains("None"))
					{
						colorUID =  swatchList->GetNoneSwatchUID();
						if(colorUID==nil)
						{	
							//CAlert::InformationAlert("no encontro UID del color");	
							break;
						}
					}
					else
					{
						if(Prefer.ColorTextEtiquetaSinPublicidad.Contains("White"))
						{
							colorUID =  swatchList->GetPaperSwatchUID();
							if(colorUID==nil)
							{	
								//CAlert::InformationAlert("no encontro UID del color");	
								break;
							}
						}
						else
						{
							if(Prefer.ColorTextEtiquetaSinPublicidad.Contains("Register"))
							{
								colorUID =  swatchList->GetRegistrationSwatchUID();
								if(colorUID==nil)
								{	
									//CAlert::InformationAlert("no encontro UID del color");	
									break;
								}
							}
							else
							{
								if(Prefer.ColorTextEtiquetaSinPublicidad.Contains("Red"))
								{
									colorUIDR =  swatchList->FindSwatch("A2P Red");
									colorUID=colorUIDR.GetUID();
									if(colorUID==nil)
									{	
										//CAlert::InformationAlert("no encontro UID del color");	
										break;
									}
								}
								else
								{
									if(Prefer.ColorTextEtiquetaSinPublicidad.Contains("Blue"))
									{
										colorUIDR =  swatchList->FindSwatch("A2P Blue");
										colorUID=colorUIDR.GetUID();
										if(colorUID==nil)
										{	
											//CAlert::InformationAlert("no encontro UID del color");	
											break;
										}
									}
									else
									{
										if(Prefer.ColorTextEtiquetaSinPublicidad.Contains("Yellow"))
										{
											colorUIDR =  swatchList->FindSwatch("A2P Yellow");
											colorUID=colorUIDR.GetUID();
											if(colorUID==nil)
											{	
												//CAlert::InformationAlert("no encontro UID del color");	
												break;
											}
										}
										else
										{
											break; //Texto sin color
										}
									}
								}
							}
						}
					}
				}
			}

			//crea la interfaz ITextAttrUID con el jefe kTextAttrColorBoss para cambiar el color de texto
			InterfacePtr<ITextAttrUID>textAttrUID(::CreateObject2<ITextAttrUID>(kTextAttrColorBoss));
			if(textAttrUID==nil)
			{	
				break;
			}
			//pone el UID del color a aplicar
			textAttrUID->Set(colorUID);
			InterfacePtr<ICommand> applyCmd(Utils<ITextAttrUtils>()->BuildApplyTextAttrCmd(textmodel,0,textmodel->TotalLength(),textAttrUID,kCharAttrStrandBoss) );
			if(applyCmd==nil)
			{	
				break;
			}
			//procesa comando
			ErrorCode err = CmdUtils::ProcessCommand(applyCmd);

	}while(false);

}

/**
	funcion que llena lka estructura de Avisos. lee el Archivo a importar, para descomponerlo segun las preferencias, y acomodar
	los campo en la estructura Avisos.
*/
void A2PFluirFunction::LlenarStructAvisos(StructAvisos *Avisos,PMString DireccionPMString,int32 NumAvisos,Preferencias &Prefer)
{
	
	PMString  CAceptada="";
	PMString CEncontrada;
	int salir=0;
	long numcampo=1;
	int PosTab=0;
	int NumCampoAGuardar;
	PMString DireccionCString=" ";
	int PosIni;
	int PosFin;
	int Linea;
	
	//DireccionPMString= InterlasaUtilities::MacToUnix(DireccionPMString);
	
	DireccionCString=DireccionPMString.GrabCString();
	//CAlert::InformationAlert(DireccionPMString);
	//////////////LLENADO DE AVISOS//////////
	
	if( (stream  = fopen(DireccionCString.GrabCString(),"r"))==NULL)
	{	
		////CAlert::InformationAlert("No entro");
	}
	else//cuando si se pudo leer el archivo
	{
		////CAlert::InformationAlert("Si entro");
		PosIni=0;	//Posicion inicial de una linea
		PosFin=0;	//Posicion final de una linea
		Linea=1;	//Numero de linea se inicializa en uno
		while(Linea<=NumAvisos)//Hacer mientras el numero de linea sea menor al tama‚àö√≠o de avisos o numero de avisos
		{	PosIni=PosFin;
			do
			{		
				PMString SASW="";
				PlatformChar Caracter;
				SASW.Append(fgetc(stream));//Caracter.SetFromUnicode();//obtiene siguente caracter de
				CadenaAviso.Append(SASW);
				Caracter=SASW.GetChar(0);
				NumCaracter++;
				ANSCCICaranter=Caracter.GetValue();
			/*	PlatformChar Caracter;
				Caracter.SetFromUnicode(fgetc(stream));//obtiene siguente caracter de
				CadenaAviso.Append(Caracter);
				NumCaracter++;
				ANSCCICaranter=Caracter.GetValue();
				/////Esto ya no es
				if(ANSCCICaranter==10 || ANSCCICaranter==13 || !feof(stream))
				{//remuevo el ltimo caracter en caso de que sea un brinco de linea o retorno de carro
					CadenaAviso.Remove(CadenaAviso.Length()-2,CadenaAviso.Length()-1);
				}
			*/}while(ANSCCICaranter!=10 && ANSCCICaranter!=13 && !feof(stream));//Hacer mientras que no sea fin de linea(10) retorno de carro(13) o fin de archivo(EOF)


			CAceptada=CadenaAviso;
			if(feof(stream))
			CAceptada.Remove(CAceptada.NumUTF16TextChars()-1,1);
			salir=0;///para salir y leer una nueva linea de aviso
			numcampo=1;//
			do///ciclo para acomodar los campos dependiendo preferencias en la estructura avisos
			{
				
					//obtiene la posicion del tabulador
					/*PMString a;
				a.Clear();
				a.AppendNumber(Prefer.SepEnCampos.GetAsNumber());
				CAlert::ErrorAlert(a);
				*/
				PosTab=CAceptada.IndexOfWChar(Prefer.SepEnCampos.GetAsNumber());
				
				//si la posicion del tabulador es -1 significa que se llego al final del aviso
				if(PosTab==-1)
				{
				/*	if(ANSCCICaranter==10 || ANSCCICaranter==13)
						PosTab==CAceptada.Length()-1;
					else
				*/		PosTab=CAceptada.NumUTF16TextChars();//posicion del tabulador es igual al tama‚àö√≠o de la cadena
					salir=1;//bandear salir activada para salir al terminar la seguientes instrcciones y llegar a la pregunta del ciclo
				}
				CEncontrada.Clear();//limpia CEncontrada
				CEncontrada.Insert(CAceptada,0,PosTab);//inserta en CadenaEncontrada la cadena que se acepto
				
				CAceptada.Remove(0,PosTab+1);//se remueve el primer campo de la Cadena Aceptada o hasta donde se encuentre la posicion del tabulador
				
			//	CAlert::InformationAlert(CAceptada);
				
				NumCampoAGuardar=FModel.CampoAGuardar(numcampo,Prefer);//se optiene el numero del campo que toca de guardar
				switch(NumCampoAGuardar)
				{
					case 0:
							//CAlert::InformationAlert(kA2PImpVerificarPreferenciasStringKey);
							break;
					case 1:
						//CAlert::InformationAlert("1");
						Avisos[Linea-1].NomCampo1=Prefer.NomCampos1;
						Avisos[Linea-1].ContenidoCampo1=CEncontrada;
						break;


					case 2:
						//CAlert::InformationAlert("2");
						Avisos[Linea-1].NomCampo2=Prefer.NomCampos2;
						Avisos[Linea-1].ContenidoCampo2=CEncontrada;
						break;

					case 3:
						//CAlert::InformationAlert("3");
						Avisos[Linea-1].NomCampo3=Prefer.NomCampos3;
						Avisos[Linea-1].ContenidoCampo3=CEncontrada;
						break;

					case 4:
						//CAlert::InformationAlert("4");
						Avisos[Linea-1].NomCampo4=Prefer.NomCampos4;
						Avisos[Linea-1].ContenidoCampo4=CEncontrada;
						break;
					
					case 5:
						//CAlert::InformationAlert("5");
						Avisos[Linea-1].NomCampo5=Prefer.NomCampos5;
						Avisos[Linea-1].ContenidoCampo5=CEncontrada;
						break;

					case 6:
						//CAlert::InformationAlert("6");
						Avisos[Linea-1].NomCampo6=Prefer.NomCampos6;
						Avisos[Linea-1].ContenidoCampo6=CEncontrada;
						break;

					case 7:
						//CAlert::InformationAlert("7");
						Avisos[Linea-1].NomCampo7=Prefer.NomCampos7;
						Avisos[Linea-1].ContenidoCampo7=CEncontrada;
						break;

					case 8:
						//CAlert::InformationAlert("8");
						Avisos[Linea-1].NomCampo8=Prefer.NomCampos8;
						Avisos[Linea-1].ContenidoCampo8=CEncontrada;
						break;

					case 9:
						//CAlert::InformationAlert("9");
						Avisos[Linea-1].NomCampo9=Prefer.NomCampos9;
						Avisos[Linea-1].ContenidoCampo9=CEncontrada;
						break;

					case 10:
						//CAlert::InformationAlert("10");
						Avisos[Linea-1].NomCampo10=Prefer.NomCampos10;
						Avisos[Linea-1].ContenidoCampo10=CEncontrada;
						break;

					case 11:
						//CAlert::InformationAlert("11");
						Avisos[Linea-1].NomCampo11=Prefer.NomCampos11;
						Avisos[Linea-1].ContenidoCampo11=CEncontrada;
						break;

					case 12:
						//CAlert::InformationAlert("12");
						Avisos[Linea-1].NomCampo12=Prefer.NomCampos12;
						Avisos[Linea-1].ContenidoCampo12=CEncontrada;
						break;

					case 13:
						//CAlert::InformationAlert("13");
						Avisos[Linea-1].NomCampo13=Prefer.NomCampos13;
						Avisos[Linea-1].ContenidoCampo13=CEncontrada;
						break;

					case 14:
						//CAlert::InformationAlert("14");
						Avisos[Linea-1].NomCampo14=Prefer.NomCampos14;
						Avisos[Linea-1].ContenidoCampo14=CEncontrada;
						break;
				}	
				numcampo++;//incremento del numero de campo a guardar
				CEncontrada.Clear();//limpia cadena Encontrada
			}while(salir!=1);//hacer mientras no se encuentre el fin del aviso
			CadenaAviso.Clear();//limpiar cadena aviso
			CAceptada.Clear();//limpiar cadena aceptada o campo
			NumCaracter=0;//numero de caracter en 0
			Linea++;//numero de linea o aviso se incrementa
			
		}//end while
		fclose(stream);
	}//end Else
}//////////////FIN DE LLENADO DE AVISOS//////////


/**
	funcion que compara el numero de columnas del documento actual con el de las preferencias
	regresa u valor booleano, v si son niguales, f si son diferentes
*/
bool16 A2PFluirFunction::ValidaColumnas(Preferencias &Prefer)
{
	bool16 retval=kFalse;
	do
	{

		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			break;
		}
		
		InterfacePtr<IHierarchy> layerHier(layout->QueryActiveLayer());
		if (layerHier == nil)
		{
			ASSERT_FAIL("IHierarchy pointer nil");
			//CAlert::InformationAlert("IHierarchy pointer nil");
			break;
		}

		// Get the UIDRef of the layer:
		UIDRef layerRef(::GetUIDRef(layerHier));
		// Get the bounding box for the current page
		IDataBase* db = layerRef.GetDataBase();
		// ovtiene el UIRef del la pagina actual
		UIDRef pageRef = UIDRef(db,layout->GetPage());
		
		InterfacePtr<IColumns> pageColumnsCmdData( pageRef, IID_ICOLUMNS );
		if( pageColumnsCmdData == nil )
		{
			ASSERT_FAIL("IColumns pointer nil");
			//CAlert::InformationAlert("Error");
			break;
		}
		int32 colums = pageColumnsCmdData->GetNumberColumns(kTrue);
		if(colums==Prefer.NumColumas)
		{
			//CAlert::InformationAlert("1");
			retval=kTrue;
		}
	}while(false);
	return(retval);
}

/**
	Funcion que compara los margenes del documento actual con las de la preferencias
	regresa un valor booleano, V en caso de ser iguales.
*/
bool16 A2PFluirFunction::ValidaMargenes(Preferencias &Prefer)
{
	bool16 retval=kTrue;
	PMReal leftPref;
	PMReal topPref;
	PMReal rightPref;
	PMReal bottomPref;

	leftPref=Prefer.MargenInterior;
	topPref=Prefer.MargenSuperior;
	rightPref=Prefer.MargenExterior;
	bottomPref=Prefer.MargenInferior;
	
	
	PMReal leftDoc;
	PMReal topDoc;
	PMReal rightDoc;
	PMReal bottomDoc;

	do
	{

		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			break;
		}
		
		InterfacePtr<IHierarchy> layerHier(layout->QueryActiveLayer());
		if (layerHier == nil)
		{
			ASSERT_FAIL("IHierarchy pointer nil");
			break;
		}

		// Get the UIDRef of the layer:
		UIDRef layerRef(::GetUIDRef(layerHier));
		// Get the bounding box for the current page
		IDataBase* db = layerRef.GetDataBase();
		// ovtiene el UIRef del la pagina actual
		UIDRef pageRef = UIDRef(db,layout->GetPage());
		
		InterfacePtr<IMargins> pageMargins( pageRef, IID_IMARGINS );
		if( pageMargins == nil )
		{
			ASSERT_FAIL("IMargins pointer nil");
			break;
		}
		pageMargins->GetMargins(&leftDoc,&topDoc,&rightDoc,&bottomDoc,kTrue);
		
		PMString ToRound="";
		ToRound.AppendNumber(leftDoc,2,kTrue,kTrue);
		leftDoc=ToRound.GetAsDouble();
		
		ToRound="";
		ToRound.AppendNumber(topDoc,2,kTrue,kTrue);
		topDoc=ToRound.GetAsDouble();
		
		ToRound="";
		ToRound.AppendNumber(rightDoc,2,kTrue,kTrue);
		rightDoc=ToRound.GetAsDouble();
		
		ToRound="";
		ToRound.AppendNumber(bottomDoc,2,kTrue,kTrue);
		bottomDoc=ToRound.GetAsDouble();
		
		
		if(  leftPref <=  (leftDoc - 0.01) && leftPref >= (leftDoc + 0.01) )
		{
			retval=kFalse;
			break;
		}
		
		if( topPref <=  (topDoc - 0.01) && topPref  >=(topDoc + 0.01))
		{
			retval=kFalse;
			break;
		}
			
		if( rightPref <=  (rightDoc - 0.01) && rightPref >= (rightDoc + 0.01) )
		{
			retval=kFalse;
			break;
		}
				
		if( bottomPref <=  (bottomDoc - 0.01) && bottomPref	 >= (bottomDoc + 0.01))
		{
			retval=kFalse;
			break;
		}
		retval=kTrue;
	}while(false);
	return(retval);
}

/**
	funcion que compara el tama‚àö√≠o del medianil de un documento actual y las preferencias
	regresa un  valor boleano, V si son iguales
*/
bool16 A2PFluirFunction::ValidaMedianil(Preferencias &Prefer)
{
	bool16 retval=kFalse;
	PMReal MedianilPref=Prefer.Medianil;
	do
	{

		InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layout == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			break;
		}
		
		InterfacePtr<IHierarchy> layerHier(layout->QueryActiveLayer());
		if (layerHier == nil)
		{
			//CAlert::InformationAlert("IHierarchy pointer nil");
			ASSERT_FAIL("IHierarchy pointer nil");
			break;
		}

		// Get the UIDRef of the layer:
		UIDRef layerRef(::GetUIDRef(layerHier));
		// Get the bounding box for the current page
		IDataBase* db = layerRef.GetDataBase();
		// ovtiene el UIRef del la pagina actual
		UIDRef pageRef = UIDRef(db,layout->GetPage());
		
		InterfacePtr<IColumns> pageColumnsCmdData( pageRef, IID_ICOLUMNS );
		if( pageColumnsCmdData == nil )
		{
			ASSERT_FAIL("IColumns pointer nil");
			//CAlert::InformationAlert("Error");
			break;
		}
		
		PMString ToRound="";
		ToRound.AppendNumber(pageColumnsCmdData->GetGutter(kTrue),2,kTrue,kTrue);
		//CAlert::InformationAlert(ToRound);
		PMReal MedianilDoc = ToRound.GetAsDouble();
		
		if(MedianilPref >= (MedianilDoc - 0.1) && MedianilPref <= (MedianilDoc + 0.1))
		{
			retval=kTrue;
		}
	}while(false);
	return(retval);
}


/**
	funcionn que ajusta el punto cero de unn documento esta funcion es llamda cuando 
	se fluye un documneto ya creada
*/
void A2PFluirFunction::AjustarPuntoZero()
{
	IDocument* document =Utils<ILayoutUIUtils>()->GetFrontDocument();
	const UIDRef doc = ::GetUIDRef(document);
	PMPoint zeroPoint(0,0);
	// Create a SetZeroPointPrefCmd:
	InterfacePtr<ICommand>	setZPPCmd(CmdUtils::CreateCommand(kSetZeroPointPrefCmdBoss));
	// Get an IZeroPointPrefsCmdData Interface for the SetZeroPointPrefCmd:
	InterfacePtr<IZeroPointPrefsCmdData> cmdData(setZPPCmd,	IID_IZEROPOINTPREFSCMDDATA);
	// Set the IZeroPointPrefsCmdData Interface's data:
	cmdData->Set(doc, zeroPoint, kFalse);
	// Process the SetZeroPointPrefCmd:
	if (CmdUtils::ProcessCommand(setZPPCmd) != kSuccess)
	{
		ASSERT_FAIL("No se encontro el Punto cero");
		//CAlert::WarningAlert("No se encontro el Punto cero");
	}
}
/**
	obtien el tamal‚àö√≠o de una pagina previamente creada
*/
PMRect A2PFluirFunction::obtenerTamPagina()
{
	PMRect rect(0,0,0,0);//variable que contendra y devolvera el tama‚àö√≠o de la pagina
	do
	{
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
			break;
		// Get the database for the document.
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}
		
		InterfacePtr<IPageSetupPrefs> iPageSetupPrefs(static_cast<IPageSetupPrefs *>(::QueryPreferences(IID_IPAGEPREFERENCES, document)));
		if(iPageSetupPrefs==nil)
		{
			break;
		}
		
		rect= iPageSetupPrefs->GetPageSizePref();
		
		//rect
		/*InterfacePtr<ILayoutControlData> layout(Utils<ILayoutUIUtils>()->QueryFrontLayoutData()); 

		UID pageUID = layout->GetPage(); 
		UIDRef ref = UIDRef(db,pageUID); 

		InterfacePtr<IGeometry> iGeometry(ref, IID_IGEOMETRY); 

		rect = iGeometry->GetPathBoundingBox(); //iGeometry->GetStrokeBoundingBox(); //*/
	}while(false);
	return(rect);
}

/**
	esta funcion es llamada al terminar la funcion de Fluir y realiza el acomodo y las preferencias de las 
	capas creadas(Oculta,mostrar, bloquear), ademas es la encargada te poner activa la una capa dependiendo
	su jerarquia.
*/
void A2PFluirFunction::AplicaPreferenciasACapas()
{
	/*if(ActivarCapa("Update"))
	{
		BloquearCapa("Update",kTrue);
		MostrarCapa("Update",kFalse);
	}*/
	if(ActivarCapa("Editorial"))
	{
		if(Prefer.CapaEditorialBlocked)
			BloquearCapa("Editorial",kTrue);
		else
			BloquearCapa("Editorial",kFalse);
		if(Prefer.CapaEditorialVisible)
			MostrarCapa("Editorial",kTrue);
		else
			MostrarCapa("Editorial",kFalse);
	}
	if(ActivarCapa("Etiquetas C/Publicidad"))
	{
		if(Prefer.CapaEtiquetaConPublicidadBlocked)
			BloquearCapa("Etiquetas C/Publicidad",kTrue);
		else
			BloquearCapa("Etiquetas C/Publicidad",kFalse);
		if(Prefer.CapaEtiquetaConPublicidadVisible)
			MostrarCapa("Etiquetas C/Publicidad",kTrue);
		else
			MostrarCapa("Etiquetas C/Publicidad",kFalse);
	}
	if(ActivarCapa("Etiquetas S/Publicidad"))
	{
		if(Prefer.CapaEtiquetaSinPublicidadBlocked)
			BloquearCapa("Etiquetas S/Publicidad",kTrue);
		else
			BloquearCapa("Etiquetas S/Publicidad",kFalse);
		if(Prefer.CapaEtiquetaSinPublicidadVisible)
			MostrarCapa("Etiquetas S/Publicidad",kTrue);
		else
			MostrarCapa("Etiquetas S/Publicidad",kFalse);
	}
	if(ActivarCapa("Imagen"))
	{
		if(Prefer.CapaImagenBlocked)
			BloquearCapa("Imagen",kTrue);
		else
			BloquearCapa("Imagen",kFalse);
		if(Prefer.CapaImagenVisible)
			MostrarCapa("Imagen",kTrue);
		else
			MostrarCapa("Imagen",kFalse);
	}
	//Activar Capa
	if(!ActivarCapa("Editorial"))
	{
		if(!ActivarCapa("Imagen"))
		{
			if(!ActivarCapa("Etiquetas C/Publicidad"))
			{
				if(!ActivarCapa("Etiquetas S/Publicidad"))
				{	
					//ActivarCapa("Update");
				}
			}	
		}
	}
}



void A2PFluirFunction::CrearRectanguloImagen(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso)
{
	SDKLayoutHelper layoutHelper;
	PMString filePath;
	filePath.Clear();
	PMString Coordenadas;
	Coordenadas.Clear();
	bool16 ExisteArchivoImagen=kTrue;
	IDFile	file;
	ErrorCode status = kFailure;
	
	this->GetPathIssueOdAds(Prefer,AvisoAFluir,numAviso, filePath);
	
	this->GetPMStringCoordinatesIssueOdAds(Prefer,AvisoAFluir, numAviso, Coordenadas);

	//////////////////////////////////////////////////////////
	//	Ver la forma de que unicamente	por que no puede importar utilizando unicamente la direccion
	//	Nota si se puede con una cadena que yo le de por directamente en la funcion "AbsolutePathToSysFile"
	//////////////////////////////////////////////////////////

	//CAlert::InformationAlert(Coordenadas);
	//Conversion del archivo a un archivo aceptable por InDesign
	//	if(Obtener_Version_Windows())
	//		filePath=ConvertAFileAceptada(filePath);
	
	PlatformChar car=filePath.GetChar(filePath.NumUTF16TextChars()-1);
	if( 10==car.GetValue() || 13==car.GetValue()
		|| 9==car.GetValue() || 20==car.GetValue())
	{
		filePath.Remove(filePath.NumUTF16TextChars()-1,filePath.NumUTF16TextChars());
	}

	Ruta=filePath; 
	Ruta.Translate();
	Ruta.SetTranslatable(kTrue);
	#ifdef MACINTOSH
	if(Ruta.Contains("\\\\"))
		SDKUtilities::Replace(Ruta,"\\\\","/");
		//CAlert::InformationAlert(Ruta);
		SDKUtilities::convertToMacPath(Ruta);
		/*CharCounter IndexDosPuntos = Ruta.IndexOfWChar(58);
		CharCounter IndexSlach = Ruta.IndexOfWChar(92);
		PMString SASA="";
		SASA.AppendNumber(IndexSlach-IndexDosPuntos);
		CAlert::InformationAlert(SASA);
		if((IndexSlach-IndexDosPuntos)==1)	//si es un windows
		{
			PMString target="";
			target.InsertW(92);
			PMString replace="";
			target.InsertW(58);
			SDKUtilities::Replace(Ruta,target, replace);
		}*/
		
	#else // we must have a windows path now...
	if(!(Ruta.Contains(":\\") || Ruta.Contains(":\\\\")))
		SDKUtilities::convertToWinPath(Ruta);
	#endif
	
	//CAlert::InformationAlert("Y por que no aparece esto sera por alguna falla o que rollo");
	//CAlert::InformationAlert(Ruta);
	do
	{
		// Import into the frontmost document. Stop if there isn't one.
		IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
		if (document == nil)
			break;
		// Get the database for the document.
		IDataBase* db = ::GetDataBase(document);
		if (db == nil)
		{
			ASSERT_FAIL("db is invalid");
			break;
		}

		//si el contenido de la variable filePath es vacio no se fluye	
		if (Ruta.IsEmpty())
		{	
			//crea un rectangulo (GraficFrame) vacio
			ExisteArchivoImagen=kFalse;
			//limpio el tiempo de acceso del ultimo archivo importado
			TimeCreacionArchivo.Clear();
			//agrega 0 puesto que no existe el archivo(imagen) que se desea importar
			TimeCreacionArchivo="0";
			//agrega un brinco de linea
			//TimeCreacionArchivo.AppendW(10);
		}
		else//si no es vacio el contenido de la variable
		{	

	
			//Verifica no existe el archivo(imagen) para lectura
			if (!ExisteArchivoParaLectura(&Ruta))
			{

				//crea un rectangulo (GraficFrame) vacio
				ExisteArchivoImagen=kFalse;

				//limpio el tiempo de acceso del ultimo archivo importado
				TimeCreacionArchivo.Clear();
				//agrega 0 puesto que no existe el archivo(imagen) que se desea importar
				TimeCreacionArchivo="0";
				//agrega un brinco de linea
				//TimeCreacionArchivo.AppendW(10);
			}
			else
			{
				//conversion de la cadena a un Sysfile
				if (SDKUtilities::AbsolutePathToSysFile(Ruta,file) != kSuccess)
				{
					ASSERT_FAIL("AbsolutePathToSysFile is false");
					break;
				}
				
				//si existe el archivo de la imagen
				//limpio el tiempo de acceso del ultimo archivo importado
			/*	TimeCreacionArchivo.Clear();

				InterfacePtr<IRegEditUtilities> ConsultaRegEdit(static_cast<IRegEditUtilities*> (CreateObject
				(
					kRegEditUtilitiesBoss,	// Object boss/class
					IRegEditUtilities::kDefaultIID
				)));
				//agrega la fecha del ultimo acceso del archivo a variable tiempo de creacion
				TimeCreacionArchivo=ConsultaRegEdit->ProporcionaFechaArchivo(filePath.GrabCString());	
			*/
			}
		}


/***********************************************************************************************/		

		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutControlData == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			break;
		}
		
		InterfacePtr<IHierarchy> activeSpreadLayerHierarchy(layoutControlData->QueryActiveLayer());
		ASSERT(activeSpreadLayerHierarchy != nil);
		if (activeSpreadLayerHierarchy == nil) {
			break;
		}
		UIDRef parentUIDRef = ::GetUIDRef(activeSpreadLayerHierarchy);

		// Transform the bounds of the frame from page co-ordinates
		// into the parent co-ordinates, i.e. the spread.
		ASSERT(layoutControlData->GetPage() != kInvalidUID);
		if (layoutControlData->GetPage() == kInvalidUID) {
			break;
		}
		UIDRef pageUIDRef(parentUIDRef.GetDataBase(), layoutControlData->GetPage());
		
						
		//determina las coordenadas del rectangulo
		PMReal left,top,right,bottom;
		this->CoordenadasDeImagen(Prefer,left,top,right,bottom,Coordenadas);//izquierda,tope, derecha,bajo
		
		PMString Coordenadas="Top:";
		Coordenadas.AppendNumber(top);
		Coordenadas.Append(" left:");
		Coordenadas.AppendNumber(left);
		Coordenadas.Append(" bottom:");
		Coordenadas.AppendNumber(bottom);
		Coordenadas.Append(" right:");
		Coordenadas.AppendNumber(right);
		//CAlert::InformationAlert(Coordenadas);
		
		left=ConvertirCoorAPuntos(left,Prefer.UnidadesEnLeft);
		top=ConvertirCoorAPuntos(top,Prefer.UnidadesEnTop);
		right=ConvertirCoorAPuntos(right,Prefer.UnidadesEnRight);
		bottom=ConvertirCoorAPuntos(bottom,Prefer.UnidadesEnButtom);
		
		PMRect CoordenadasChidas=PMRect(left, top, right, bottom);
		
		//PMRect boundsInParentCoords = layoutHelper.PageToSpread(pageUIDRef, CoordenadasChidas);
		
		////////
		 InterfacePtr<IDocumentLayer> activeLayer(Utils<ILayerUtils>()->QueryDocumentActiveLayer(document));
		 
		InterfacePtr<ISpread> spread(layoutControlData->GetSpreadRef(), IID_ISPREAD);
		if (spread==nil) 
		{
			ASSERT_FAIL("nil ISpread* or IHierarchy*");
			break;
		}


		InterfacePtr<ISpreadLayer> spreadLayer(spread->QueryLayer(activeLayer));
		UIDRef layerRef = ::GetUIDRef(spreadLayer);

		// Get visible page of this spread
        InterfacePtr<IGeometry> pageGeo(layerRef.GetDataBase(), layoutControlData->GetPage(), UseDefaultIID());
		if (pageGeo == nil )
		{
			ASSERT_FAIL("nil IGeometry* for page");
			break;
		}

		// Create the geometry for the new page item.
		// The geometry is first specified in inner (page item) coordinates.
		PMPoint startPoint(left, top);		// Upper Left in Page Coords
		PMPoint endPoint(right-left , bottom-top);		// Size of rectangle
		endPoint+=startPoint;

		// Now transform the description from inner coordinates to pasteboard coordinates.  (Pasteboard
		// coords are the same as the spread.)
		::TransformInnerPointToPasteboard(pageGeo,&endPoint);
		::TransformInnerPointToPasteboard(pageGeo,&startPoint);
		PMRect boundsInParentCoords(startPoint, endPoint);

		//PMPoint leftTop(left,top );
		//PMPoint rightBottom( right, bottom);
		//::InnerToPasteboard(pageGeometry,&leftTop);
		//::InnerToPasteboard(pageGeometry,&rightBottom);

		// Create the bounding box
		//PMRect bbox(leftTop, rightBottom);
		
		
		//crea el rectangulo
		//UIDRef newSplineItemRef = PathUtils::CreateRectangleSpline( pageUIDRef, boundsInParentCoords, INewPageItemCmdData::kGraphicFrameAttributes ); 
		
	//	UIDRef newSplineItemRef = layoutHelper.CreateRectangleGraphic(parentUIDRef, boundsInParentCoords);
	//	if (newSplineItemRef == UIDRef::gNull) {
	//		break;
	//	}
		
		UIDRef newSplineItemRef;
		if(ExisteArchivoImagen)
		{// Process an ImportAndPlaceCmd.
			//::SnipLayoutImportStream(file,newSplineItemRef);
			//SnipLayoutImportStream fer(file,newSplineItemRef);
			//fer.ImportIntoPageItemInFrame(file,newSplineItemRef);
			//CAlert::WarningAlert("E");
			//ImportImagenClass ImportImage;
			//newSplineItemRef=ImportImage.ImportIntoPageItemInFrame(file,parentUIDRef);
			//CAlert::WarningAlert("F");
			//CAlert::InformationAlert("p");
			//
			//CAlert::WarningAlert("G");
			//CAlert::InformationAlert("pp");
			//UIFlags uiFlags=K2::kMinimalUI;
			//newSplineItemRef = layoutHelper.PlaceFileInFrame(file,layerRef,boundsInParentCoords,uiFlags,kFalse,kFalse,kFalse,kInvalidUID);//,kFalse
			//CAlert::InformationAlert("ASASSASAAS");

			newSplineItemRef= Utils<IPathUtils>()->CreateRectangleSpline(	layerRef, 
														boundsInParentCoords, 
														INewPageItemCmdData::kGraphicFrameAttributes);

			const UIDRef docUIDRef = ::GetUIDRef(document);
			
			ImportImagenClass ImportImage;
			UIDRef contentUIDRef = ImportImage.ImportImageAndLoadPlaceGun(docUIDRef, Ruta);
			ImportImage.CreateAndProcessPlaceItemInGraphicFrameCmd(docUIDRef,contentUIDRef, newSplineItemRef);
			
			AplicarAjusteRecImagen( Prefer , newSplineItemRef);
		}
		else
		{
			//newSplineItemRef = layoutHelper.CreateRectangleGraphic(layerRef, boundsInParentCoords);
			newSplineItemRef= Utils<IPathUtils>()->CreateRectangleSpline(	layerRef, 
														boundsInParentCoords, 
														INewPageItemCmdData::kGraphicFrameAttributes);
		}
		
		
		UID a=newSplineItemRef.GetUID();//OBTENGO EL UID del rectangulo
		uint32 b=a.Get();			//obtengo el valor en exadecimal del rectangulo
		int c=uint32(b);			//Convierto el valor exadecimal a entero
		NumIDFrame.Clear();			//limpio el valor del anterior rectangulo
		NumIDFrame.AppendNumber(c);	//pungo el ID en valor decimal en el string 
		//CAlert::WarningAlert("B");
		AplicarFondoRecImagen(Prefer,ExisteArchivoImagen,newSplineItemRef);
		//CAlert::WarningAlert("C");
		//Aplica color de Marco del rectangulo
		AplicarColorMarcoRecImagen(Prefer,ExisteArchivoImagen,newSplineItemRef);
		
		
		//parent donde se va a insertar el frame, coordenadas, Comando y atributos del frame
		//obtiene el UIRef del frame para aplizar preferencias

		
		
		InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
		(
			kA2PPreferenciasFuncionesBoss,	// Object boss/class
			IA2PPreferenciasFunctions::kDefaultIID
		)));
		//////////////////
		AvisoAFluir[numAviso].LFrameImage = left;
		AvisoAFluir[numAviso].TFrameImage = top;
		AvisoAFluir[numAviso].RFrameImage = right;
		AvisoAFluir[numAviso].BFrameImage = bottom;
		AvisoAFluir[numAviso].PathToImage = filePath;
		AvisoAFluir[numAviso].UIDFrameToImage = c;
		AvisoAFluir[numAviso].ExistImageFile = ExisteArchivoImagen;
		AvisoAFluir[numAviso].UIDFrameEtiqueta = TextodeEtiqueta(Prefer,AvisoAFluir,numAviso,ExisteArchivoImagen,boundsInParentCoords.Left(),boundsInParentCoords.Top(),boundsInParentCoords.Right(),boundsInParentCoords.Bottom() ); //crea etiqueta 
		AvisoAFluir[numAviso].UIDPageFlowed =layoutControlData->GetPage().Get();
		AvisoAFluir[numAviso].LastTimeImage = A2PPrefeFuntions->ProporcionaFechaArchivo(Ruta.GrabCString());
		//////////////////
	}while(false);
}

bool16 A2PFluirFunction::Abrir_DlgGetFechaPag(PMString& FechaEdicion,PMString& PaginaFluida,int32 &NumeroDePagina, PMString& FechaStandart,const bool16& Plantillero)
{
	bool16 retval=kFalse;
	do
	{
	
		// Get the application interface and the DialogMgr.	
		InterfacePtr<IApplication> application(GetExecutionContextSession()->QueryApplication());
		ASSERT(application);
		if (application == nil) {	
			CAlert::InformationAlert("application");
			break;
		}
		InterfacePtr<IDialogMgr> dialogMgr(application, UseDefaultIID());
		ASSERT(dialogMgr);
		if (dialogMgr == nil) {
			CAlert::InformationAlert("dialogMgr");
			break;
		}

		// Load the plug-in's resource.
		PMLocaleId nLocale = LocaleSetting::GetLocale();
		RsrcSpec dialogSpecCopia;
		if(Plantillero)
		{
			RsrcSpec dialogSpec
			(
				nLocale,					// Locale index from PMLocaleIDs.h. 
				kA2PImpPluginID,			// Our Plug-in ID  
				kViewRsrcType,				// This is the kViewRsrcType.
					kA2PImpDlgGetFechaYPag_PlantilleroResourceID,	// Resource ID for our dialog.
				kTrue						// Initially visible.
			);
			dialogSpecCopia=dialogSpec;
		}
		else
		{
			RsrcSpec dialogSpec
			(
				nLocale,					// Locale index from PMLocaleIDs.h. 
				kA2PImpPluginID,			// Our Plug-in ID  
				kViewRsrcType,				// This is the kViewRsrcType.
				kA2PImpDlgGetFechaYPag_ToA2P_ResourceID,
				kTrue						// Initially visible.
			);
			
			dialogSpecCopia=dialogSpec;
		}
		

		// CreateNewDialog takes the dialogSpec created above, and also
		// the type of dialog being created (kMovableModal).
		IDialog* dialog = dialogMgr->CreateNewDialog(dialogSpecCopia, IDialog::kMovableModal);
		ASSERT(dialog);
		if (dialog == nil) {
			CAlert::InformationAlert("dialog");
			break;
		}

		// Open the dialog.
		dialog->Open(); 
		
		IControlView *CVDialogGetDate=dialog->GetDialogPanel();
				
		InterfacePtr<IDialogController> dialogController(CVDialogGetDate,IID_IDIALOGCONTROLLER);
		if (dialogController == nil)
		{	
			CAlert::InformationAlert("dialogController");
			ASSERT_FAIL("SelDlgActionComponent::DoSelectableDialog: panelData invalid");
			break;
		}
	
		
		dialogController->SetTextControlData(kA2PImpTextFechaWidgetID,"");
		
		dialogController->SetTextControlData(kA2PImpTextPaginaWidgetID,"");
		
		dialogController->SetTextControlData(kA2PImpTextFechaWidgetID,FechaEdicion);
		dialogController->SetTextControlData(kA2PImpTextPaginaWidgetID,PaginaFluida);
		
		dialogController->SetTextControlData(kA2PImpTextFechaStandartWidgetID,FechaStandart);
		
		dialog->WaitForDialog();
		FechaEdicion=dialogController->GetTextControlData(kA2PImpTextFechaWidgetID);
		PaginaFluida=dialogController->GetTextControlData(kA2PImpTextPaginaWidgetID);
		
		FechaStandart=dialogController->GetTextControlData(kA2PImpTextFechaStandartWidgetID);
		
		PMString sS=dialogController->GetTextControlData(kA2PImpNumberDePaginaAFluirWidgetID);
		if(sS.NumUTF16TextChars()>0)
			NumeroDePagina = sS.GetAsNumber();
		else
			NumeroDePagina=1;
			
		if(FechaEdicion!="?")
			 retval=kTrue;
	}while(false);	
	return retval;
}

void A2PFluirFunction::Aplica_Fecha_Pagina(Preferencias &Prefer,PMString& FechaEdicion,PMString& PaginaFluida)
{
	
do
	{
		//Verifica si existe estilo para aplicar
		InterfacePtr<IWorkspace> theWS(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
		if(theWS==nil)
		{
			break;
		}
		
		IDocument* document =Utils<ILayoutUIUtils>()->GetFrontDocument();
		if(document==nil)
			break;
			
		InterfacePtr<IStyleGroupManager> styleNameTable_para(document->GetDocWorkSpace(), IID_IPARASTYLEGROUPMANAGER);
		if (styleNameTable_para == nil)
		{
			ASSERT_FAIL("styleNameTable_para invalid");
			break;
		}
		
		UID myStyleUID = kInvalidUID;
		
		//CAlert::InformationAlert("Sorra"+Prefer.NombreEstiloIssue+"Sorra");
		
		
		// first, assume styleName is a full path to a style
		myStyleUID = styleNameTable_para->FindByName(Prefer.NombreEstiloIssue);
		///////
/*
		InterfacePtr<IStyleNameTable> ParaStyleNameTable(theWS, IID_ICOMPOSITEFONTLIST);//,IID_IPARASTYLENAMETABLE
		if(ParaStyleNameTable==nil)
		{
			break;
		}
	
		Prefer.NombreEstiloIssue.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
		UID myStyleUID=ParaStyleNameTable->FindByName(Prefer.NombreEstiloIssue);
*/
		if( myStyleUID==kInvalidUID )
		{
			CAlert::InformationAlert(kA2PImpA2PImpNoExisteHojaEstiloStringKey);
		}
		else
		{
			ActivarCapa("A2PDate&Page");
			//Prefer.FuenteDeIssue.Remove(0,1);
			//PMString AnchoPag=Prefer.AnchoPagina;
			//if(!ActivarCapa("Editorial"))
			//	if(!ActivarCapa("Imagen"))
			//		if(ActivarCapa("Etiquetas C/Publisidad"))
			//			ActivarCapa("Etiquetas S/Publisidad");
			//En caso de que fuera par el numero de la pagina
			PMReal TextLeftNumPag=	0;
			PMReal TextRigthNumPag=	0;
			PMReal TextTopNumPag=	0;
			PMReal TextButtomNumPag=0;
			if(obtener_TipodePagina(PaginaFluida))
			{	
			
				//En caso de que fuera par el numero de la pagina
				//Para texto del numero de pagina
				TextLeftNumPag=Prefer.DesplazamientoHorIssue;
				TextRigthNumPag=1000;
				TextTopNumPag=Prefer.DesplazamientoVerIssue;
				TextButtomNumPag=900;
				if(PaginaFluida.NumUTF16TextChars()>0)
				{
					
					PMRect coor=	CrearRectanguloFechaOPagina(Prefer,PaginaFluida,TextLeftNumPag,TextTopNumPag,TextRigthNumPag,TextButtomNumPag,2);
					//PMReal &leftMargin,PMReal &topMargin,PMReal &rightMargin,PMReal &bottomMargin
			
					//Para texto de la fecha
					TextLeftNumPag=  Prefer.DesplazamientoHorIssue + ( coor.Right()-coor.Left() );
					TextRigthNumPag= TextLeftNumPag+1000;
					TextTopNumPag=   Prefer.DesplazamientoVerIssue;
					TextButtomNumPag=TextTopNumPag+900;
					PMString Temp=   "";
					Temp=            Prefer.NombreEditorial;
					Temp.Append(FechaEdicion);
					//FechaEdicion.Insert(Prefer.NombreEditorial,14,0);
					CrearRectanguloFechaOPagina(Prefer,Temp,TextLeftNumPag,TextTopNumPag,TextRigthNumPag,TextButtomNumPag,2);
				}
				else
				{
					
					PMString Temp=	"";
					Temp=			Prefer.NombreEditorial;
					Temp.Append(FechaEdicion);
					CrearRectanguloFechaOPagina(Prefer,Temp,TextLeftNumPag,TextTopNumPag,TextRigthNumPag,TextButtomNumPag,2);
				}
			}
			else
			{
				
				//Para texto del numero de pagina
				TextRigthNumPag=	Prefer.AnchoPagina-Prefer.DesplazamientoHorIssue;
				TextLeftNumPag=		TextRigthNumPag-1000;
				TextTopNumPag=Prefer.DesplazamientoVerIssue;
				TextButtomNumPag=TextTopNumPag+900;
				if(PaginaFluida.NumUTF16TextChars()>0)
				{
					
					PMRect coor=CrearRectanguloFechaOPagina(Prefer,PaginaFluida,TextLeftNumPag,TextTopNumPag,TextRigthNumPag,TextButtomNumPag,1);
			
					//CAlert::InformationAlert(f);

					//Para texto de la fecha
					TextRigthNumPag=(Prefer.AnchoPagina-Prefer.DesplazamientoHorIssue )-( coor.Right()-coor.Left() );
					TextLeftNumPag=TextRigthNumPag-1000;
					TextTopNumPag=Prefer.DesplazamientoVerIssue;
					TextButtomNumPag=TextTopNumPag+900;
					/*	PMString f="Ri:";
						f.AppendNumber(TextRigthNumPag);
						f.Append(",iz:");
						f.AppendNumber(TextLeftNumPag);
						f.Append(",top:");
						f.AppendNumber(TextTopNumPag);
						f.Append(",bot:");
						f.AppendNumber(TextButtomNumPag);
						CAlert::InformationAlert(f);*/
					
					FechaEdicion.Append(Prefer.NombreEditorial);
					
					
					CrearRectanguloFechaOPagina(Prefer,FechaEdicion,TextLeftNumPag,TextTopNumPag,TextRigthNumPag,TextButtomNumPag,1);
				}
				else
				{
					
					FechaEdicion.Append(Prefer.NombreEditorial);
					CrearRectanguloFechaOPagina(Prefer,FechaEdicion,TextLeftNumPag,TextTopNumPag,TextRigthNumPag,TextButtomNumPag,1);
				}
			}
		}
	}while(false);
	
	
	
}


/**
*/
PMRect A2PFluirFunction::CrearRectanguloFechaOPagina(Preferencias &Prefer,PMString& CadenaAImprimir,PMReal &leftMargin,PMReal &topMargin,PMReal &rightMargin,PMReal &bottomMargin, int32 Alineacion)
{	
	PMRect coor;
	do
	{
				
		InterfacePtr<ILayoutControlData> layoutControlData(Utils<ILayoutUIUtils>()->QueryFrontLayoutData());
		if (layoutControlData == nil)
		{
			ASSERT_FAIL("ILayoutControlData pointer nil");
			break;
		}
		
		InterfacePtr<IHierarchy> activeSpreadLayerHierarchy(layoutControlData->QueryActiveLayer());
		ASSERT(activeSpreadLayerHierarchy != nil);
		if (activeSpreadLayerHierarchy == nil) {
			break;
		}
		UIDRef parentUIDRef = ::GetUIDRef(activeSpreadLayerHierarchy);

		// Transform the bounds of the frame from page co-ordinates
		// into the parent co-ordinates, i.e. the spread.
		ASSERT(layoutControlData->GetPage() != kInvalidUID);
		if (layoutControlData->GetPage() == kInvalidUID) {
			break;
		}
		UIDRef pageUIDRef(parentUIDRef.GetDataBase(), layoutControlData->GetPage());
		
		SDKLayoutHelper layoutHelper;
		
		
		PMRect CoordenadasChidas=PMRect(leftMargin, topMargin, rightMargin, bottomMargin);
		PMRect boundsInParentCoords = layoutHelper.PageToSpread(pageUIDRef, CoordenadasChidas);
			
		UIDRef outFrameUIDRef=layoutHelper.CreateTextFrame (parentUIDRef, boundsInParentCoords);
		
		
		InterfacePtr<IGraphicFrameData> graphicFrameData(outFrameUIDRef,UseDefaultIID());
		if(graphicFrameData==nil)
			break;
			
		UIDRef 	UidRefTextModel=layoutHelper.GetTextModelRef(graphicFrameData);
			
		UID UIDFrameEtiqueta = outFrameUIDRef.GetUID();
		//UIDIntegerFrameEtiqueta = UIDFrameEtiqueta.Get();

		//////////////////////
		
	
		
		//serie de conversiones del texto a insertarç
		
		PMString textToInsert(CadenaAImprimir);
		textToInsert.Translate(); // Look up our string and replace.
				
		//WideString* myText = new WideString(textToInsert);
		//K2::shared_ptr<WideString> myText(new WideString(textToInsert));
		boost::shared_ptr<WideString> myText(new WideString(textToInsert));
		//obtiene el Textmodel del frame 
		InterfacePtr<ITextModel> textModel(UidRefTextModel, ITextModel::kDefaultIID);
		if (textModel == nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			break;
		}

		InterfacePtr<ITextModelCmds> iTextModelCmd(textModel,UseDefaultIID());
		if(iTextModelCmd==nil)
		{
			ASSERT_FAIL("ITextModel pointer nil");
			break;
		}

		//crea un comando para insertar el textmodel y el nuevo texto en el frame
		InterfacePtr<ICommand> insertTextCommand (iTextModelCmd->InsertCmd(0, myText, kFalse));
		if (insertTextCommand == nil)
		{
			ASSERT_FAIL("ICommand pointer nil");
			break;
		}

		//procesa comando
		if(CmdUtils::ProcessCommand(insertTextCommand) != kSuccess)
		{
			CAlert::ModalAlert
			(
			   CadenaAImprimir,			// Alert string
			   kOKString,	// OK button
			   kNullString,	// No second button
			   kNullString,	// No third button
			   1,									// Set OK button to default
			   CAlert::eWarningIcon					// Warning icon.
			);			
		}

		//Aplicar estilo a Rectangulo
		InterfacePtr<IWorkspace> theWS(Utils<ILayoutUIUtils>()->QueryActiveWorkspace());
		if(theWS==nil)
		{
			break;
		}

	
		InterfacePtr<IStyleNameTable> ParaStyleNameTable(theWS,IID_ICOMPOSITEFONTLIST);//,IID_IPARASTYLENAMETABLE
		if(ParaStyleNameTable==nil)
		{
			break;
		}
	
		Prefer.NombreEstiloIssue.StripWhiteSpace(PMString::kLeadingAndTrailingWhiteSpace);
		UID myStyleUID=ParaStyleNameTable->FindByName(Prefer.NombreEstiloIssue);
		if(myStyleUID!=nil)
		{
			InterfacePtr<ICommand> applyCmd(iTextModelCmd->ApplyStyleCmd(0,textModel->TotalLength(), myStyleUID,kParaAttrStrandBoss));
			if(applyCmd==nil)
			{
				break;
			}

			if(CmdUtils::ProcessCommand(applyCmd)!=kSuccess)
			{
				break;
			}
		}
		else
		{
			//CAlert::InformationAlert(kA2PImpA2PImpNoExisteHojaEstiloStringKey);
		}

			
			
			
		if(Alineacion==1)
		{//Alinear a la derecha
			//CAlert::InformationAlert("alineacion A la Derecha");
			
			//**Establece la alineacuon
			InterfacePtr<ITextAttrAlign> alignAttr1(::CreateObject2<ITextAttrAlign>(kTextAttrAlignmentBoss));//kTextAttrAlignBodyBoss//Para 2.x
			InterfacePtr<ITextAttrAlign> alignAttr2(::CreateObject2<ITextAttrAlign>(kTextAttrAlignmentBoss));//kTextAttrAlignLastBoss//Para 2.x
			if(alignAttr1==nil || alignAttr2==nil)
			{
				break;
			}

			alignAttr1->SetAlignment(ICompositionStyle::kTextAlignRight);
			alignAttr2->SetAlignment(ICompositionStyle::kTextAlignRight);

				
				
			//K2::shared_ptr< AttributeBossList > attr(new AttributeBossList());
			boost::shared_ptr<AttributeBossList> attr(new AttributeBossList());
			//AttributeBossList *attr=new AttributeBossList();

			attr->ApplyAttribute(alignAttr1);
			attr->ApplyAttribute(alignAttr2);
			//	attr->ApplyAttribute(iTextAttrSize);
			//	attr->ApplyAttribute(attrFont);
			//	attr->ApplyAttribute(attrFont2);
				
				
				

			InterfacePtr<ICommand> pApplyAttrCmd(iTextModelCmd->ApplyCmd(0,textToInsert.NumUTF16TextChars(),attr,kParaAttrStrandBoss));
			if(pApplyAttrCmd==nil)
			{	
				break;
			}

			if(CmdUtils::ProcessCommand(pApplyAttrCmd)!=kSuccess)
			{
				break;
			}
		}

		if(Alineacion==2)
		{//Alinear a la derecha
			InterfacePtr<ITextAttrAlign> alignAttr1(::CreateObject2<ITextAttrAlign>(kTextAttrAlignmentBoss));
			InterfacePtr<ITextAttrAlign> alignAttr2(::CreateObject2<ITextAttrAlign>(kTextAttrAlignmentBoss));
			if(alignAttr1==nil || alignAttr2==nil)
			{
				break;
			}

			//InterfacePtr<ITextAttrRealNumber> iTextAttrSize((ITextAttrRealNumber*) ::CreateObject(kTextAttrPointSizeBoss, IID_ITEXTATTRREALNUMBER));
			//if(iTextAttrSize==nil)
			//{
				
			//	break;
			//}

			//iTextAttrSize->Set(Prefer.TamFuenteIssue.GetAsDouble());

			//InterfacePtr<ITextAttrFont>	attrFont(::CreateObject2<ITextAttrFont>(kTextAttrFontStyleBoss));
			//if (attrFont == nil) 
			//{
			//	CAlert::InformationAlert("Salio 2");
			//	break;
			//}
			//InterfacePtr<ITextAttrUID>	attrFont2(::CreateObject2<ITextAttrUID>(kTextAttrFontUIDBoss));
			//if(attrFont2==nil)
			//{
			//	CAlert::InformationAlert("Salio");
			//	break;
			//}


			// set the font style.
			//CAlert::InformationAlert(Prefer.FuenteDeIssue);
			//attrFont->SetFontName("Regular");
				
			//***********************************
			//InterfacePtr<IFontMgr> iFontMgr(GetExecutionContextSession(), UseDefaultIID()); 
			//if (iFontMgr == nil) 
			//{ 
			//	CAlert::InformationAlert("iFontMgr");
			//	break;
			//}
			
			//IDocument *iDocument=Utils<ILayoutUIUtils>()->GetFrontDocument(); 

			//InterfacePtr<IDocFontMgr> iDocFontMgr(iDocument->GetDocWorkSpace(), UseDefaultIID()); 
			//if (iDocFontMgr == nil) 
			//{ 
			//CAlert::InformationAlert("Document Font Manager Failed"); 
			//} 

			//UID fontUID = iDocFontMgr->GetFontGroupUID(Prefer.FuenteDeIssue, kTrue);

			//attrFont2->SetUIDData(fontUID);
			
			//************************************
				
			alignAttr1->SetAlignment(ICompositionStyle::kTextAlignLeft );
			alignAttr2->SetAlignment(ICompositionStyle::kTextAlignLeft );

			//AttributeBossList *attr=new AttributeBossList();//PARA 2.X
			//K2::shared_ptr< AttributeBossList > attr(new AttributeBossList());//Para CS
			boost::shared_ptr<AttributeBossList> attr(new AttributeBossList());
			attr->ApplyAttribute(alignAttr1);
			attr->ApplyAttribute(alignAttr2);
			//attr->ApplyAttribute(iTextAttrSize);
			//attr->ApplyAttribute(attrFont);
			//attr->ApplyAttribute(attrFont2);
				
				
				

			InterfacePtr<ICommand> pApplyAttrCmd(iTextModelCmd->ApplyCmd(0,textToInsert.NumUTF16TextChars(),attr,kParaAttrStrandBoss));
			if(pApplyAttrCmd==nil)
			{	
				break;
			}

			if(CmdUtils::ProcessCommand(pApplyAttrCmd)!=kSuccess)
			{
				break;
			}
		}
		if(Alineacion==2)
		{//Alinear a la izquierda
			//InterfacePtr<ITextAttrRealNumber> iTextAttrAlign((ITextAttrRealNumber*) ::CreateObject(kTextAttrPointSizeBoss, IID_ITEXTATTRREALNUMBER));
			//if(iTextAttrAlign==nil)
			//{
			//	CAlert::InformationAlert("1");
			//	break;
			//}
				
			//iTextAttrAlign->Set(14.0);
			//K2::shared_ptr< AttributeBossList > attr(new AttributeBossList());
			//AttributeBossList *attr=new AttributeBossList();

			//attr->ApplyAttribute(iTextAttrAlign);

			//InterfacePtr<ICommand> pApplyAttrCmd(iTextModelCmd->ApplyCmd(0,textToInsert.NumUTF16TextChars(),attr,kCharAttrStrandBoss));
			//if(pApplyAttrCmd==nil)
			//{
			//	CAlert::InformationAlert("2");
			//	break;
			//}

			//if(CmdUtils::ProcessCommand(pApplyAttrCmd)!=kSuccess)
			//{
			//	CAlert::InformationAlert("3");
			//	break;
			//}

		}
		// Create a FitFrameToContentCmd:
		///****************************************************************************
		do
		{
				
			InterfacePtr<ICommand> fitCmd(CmdUtils::CreateCommand(kFitFrameToContentCmdBoss));
			if(fitCmd==nil)
				CAlert::InformationAlert("11");

			// Set the FitFrameToContentCmd's ItemList:
			fitCmd->SetItemList(UIDList(outFrameUIDRef));
			// Process the FitFrameToContentCmd:
			if (CmdUtils::ProcessCommand(fitCmd) != kSuccess)
			{
				CAlert::InformationAlert("Can‚àö‚â†t process FitFrameToContentCmd");
			}

			InterfacePtr<IGeometry> geometry(outFrameUIDRef,IID_IGEOMETRY);
			if(geometry==nil)
			{ 
				CAlert::InformationAlert("Coordenadas de frame");
				break;
			}
			coor=geometry->GetStrokeBoundingBox();

				
			//PMReal &leftMargin,PMReal &topMargin,PMReal &rightMargin,PMReal &bottomMargin
			leftMargin=coor.Left();
			topMargin=coor.Top();
			rightMargin=coor.Right();
			bottomMargin=coor.Bottom();
				
		}while(false);
			//cambia el color de texto
	} while(kFalse);
	return(coor);
}



/**
*/
bool16 A2PFluirFunction::obtener_TipodePagina(PMString& PaginaFluida)
{
	bool16 retval;
	PMString UltimoCaracter=" ";
	UltimoCaracter.Clear();
	UltimoCaracter.AppendW(PaginaFluida.GetWChar(PaginaFluida.NumUTF16TextChars()-1));
	int32 numero=UltimoCaracter.GetAsNumber();
	if(fmod(numero,2.0)==0)
		retval=kTrue;
	else
		retval=kFalse;
	return retval;
}





PMString A2PFluirFunction::FechaYHora(PMString& FechaStandart)
{
  time_t tiempo;
  char cad[80];
  struct tm *tmPtr;

  tiempo = time(NULL);
  tiempo=tiempo+86400;
  
  
 // setlocale(LC_ALL,"C");

  tmPtr = localtime(&tiempo);
  
  
  
  PMString Idioma(kA2PImpA2PImplIdiomaStringKey);
  Idioma.Translate();
  Idioma.SetTranslatable(kTrue);
  
// CAlert::InformationAlert("1");  
  if(Idioma=="English")
  {
  	
  	strftime( cad, 80, "%A, %B %d, %Y", tmPtr ); 

  }
  else
  {
  	if(Idioma=="Spanish")
  	{
  		 strftime( cad, 80, "%A %d de %B de %Y", tmPtr );
  	}
  }
 
  PMString Fecha(cad);
  Fecha.Translate();
  Fecha.SetTranslatable(kTrue);
  
   strftime( cad, 80, "%Y/%m/%d", tmPtr );
   FechaStandart=PMString(cad);
  return Fecha.GrabCString();
}

/**
	funcion que cambia el color de texto de una etiqueta dependiento preferencias
*/
/*void A2PFluirFunction::CambiaAlineacionTexto(InterfacePtr<ITextModel> textModel,PMString Alineacion)
{
	do
	{
			///aplicar sin color a texto update
			if (textModel == nil)
			{
				ASSERT_FAIL("ITextModel pointer nil");
				break;
			}

			IDocument* document = Utils<ILayoutUIUtils>()->GetFrontDocument();
			if (document == nil)
			{
				// How did we POSSIBLY get here when our menu item should have been disabled if we
				// have no document and have no selection?
				ASSERT_FAIL("TSCore::ResolveRGBColorSwatch: document invalid");
				break;
			}	

			ITextModel* textmodel=textModel;
			
			if(Alineacion=="Izq")
			{

			}

			//crea la interfaz ITextAttrUID con el jefe kTextAttrColorBoss para cambiar el color de texto
			InterfacePtr<ITextAttrUID>textAttrUID(::CreateObject2<ITextAttrUID>(kTextAttrAlignBodyBoss));
			if(textAttrUID==nil)
			{	
				break;
			}
			//pone el UID del color a aplicar
			textAttrUID->Set(colorUID);
			InterfacePtr<ICommand> applyCmd(Utils<ITextAttrUtils>()->BuildApplyTextAttrCmd(textmodel,0,textmodel->TotalLength(),textAttrUID,kCharAttrStrandBoss) );
			if(applyCmd==nil)
			{	
				break;
			}
			//procesa comando
			ErrorCode err = CmdUtils::ProcessCommand(applyCmd);

	}while(false);

}*/

PMString A2PFluirFunction::DayWek(int dd,int mm,int yyyy)
{
    //int dd, mm, yyyy;
    int days;
    char daysInWord[11];
    
    /* Read a date and validate the date */


       /* do{
        printf("Enter a date(dd/mm/yyyy) :");
        scanf("%d / %d / %d", &dd, &mm ,&yyyy);
        fflush(stdin);
        }
        */
        do
        {
        	
        }
        while(validateDate(dd, mm, yyyy));
        
        /* Calculate the day for Dec 31 of the previous year */
        days = calcDay_Dec31(yyyy);
        /* Calculate the day for the given date */
        days = (dayInYear(dd, mm) + days) % 7;
        /* Add one day if the year is leap year and desired date is after February */
        if ((!(yyyy % 4) && (yyyy % 100) || !(yyyy % 400)) && mm > 2)
        days++;
        nameInStr(daysInWord, days);
        /* Print the day of the desired date */
        //printf("The day for date %d/%d/%d is %s\n\n", dd, mm, yyyy, daysInWord);
		return(daysInWord);
    } /* main */

int A2PFluirFunction::validateDate(int dd, int mm, int yyyy)
{
	int i = 0, j = 0;
    int a[7] = {1, 3, 5, 7, 8, 10, 12};
    int b[4] = {4, 6, 9, 11};
    int error = 0;
    if (mm < 1 || mm > 12)
    error = 1;
    if (mm == 2)
    {
		if (!(yyyy % 4) && (yyyy % 100) || !(yyyy % 400))
        {
			if (dd < 1 || dd > 29)
				error = 1;
                
        }
        else if (dd < 1 || dd >28)
            error = 1;
    }
                
    for (i=0;i<6;i+=1)
    {
      if (mm == a[i])
      {
		if (dd < 1 || dd > 31)
			error = 1;
      }
    }
	for (j=0;j<4;j+=1)
    {
		if (mm == b[j])
        {
			if (dd < 1 || dd > 30)
				error = 1;
        }
    }
    if (error == 1)
		printError();
    return error;
}

/**
*/
void A2PFluirFunction::printError()
{
	printf("Invalid Input!\n\n");
}

/**
*/
int A2PFluirFunction::calcDay_Dec31(int yyyy)
{
	int dayCode = 0;
    dayCode = ((yyyy-1)*365 + (yyyy-1)/4 - (yyyy-1)/100 + (yyyy-1)/400) % 7;
    return dayCode;
} /* calcDay_Dec31 */

/**
*/
int A2PFluirFunction::dayInYear(int dd, int mm)
{
	switch(mm)
	{
		case 12:dd += 30;
        case 11:dd += 31;
        case 10:dd += 30;
        case 9:dd += 31;
        case 8:dd += 31;
        case 7:dd += 30;
        case 6:dd += 31;
        case 5:dd += 30;
        case 4:dd += 31;
        case 3:dd += 28;
        case 2:dd += 31;
    }
    return dd;
} /* dayInYear */

/**
*/
void A2PFluirFunction::nameInStr(char daysInWord[], int days)
{
	switch(days)
    {
		case 7:strcpy(daysInWord, "Domingo");break;
        case 1:strcpy(daysInWord, "Lunes");break;
        case 2:strcpy(daysInWord, "Martes");break;
        case 3:strcpy(daysInWord, "Miercoles");break;
        case 4:strcpy(daysInWord, "Jueves");break;
        case 5:strcpy(daysInWord, "Viernes");break;
        case 6:strcpy(daysInWord, "Sabado");break;
    }
} /* nameInStr */

PMString A2PFluirFunction::ArmaFecha(PMString fecha)
{
	
	//CAlert::InformationAlert(fecha);
	PMString* mes=fecha.Substring(0,fecha.IndexOfWChar(47));
	fecha.Remove(0,fecha.IndexOfWChar(47)+1);
	
	PMString* Dia=fecha.Substring(0,fecha.IndexOfWChar(47));
	fecha.Remove(0,fecha.IndexOfWChar(47)+1);

	//CAlert::InformationAlert("Dia="+Dia->GrabCString());
	
	
	
	//CAlert::InformationAlert("mes="+mes->GrabCString());

	PMString* ano=fecha.Substring(0,fecha.NumUTF16TextChars());
	
	//CAlert::InformationAlert("a‚àö¬±o="+ano->GrabCString());

//	time_t tiempo;
//	char cad[80];
	//struct tm *tmPtr;
	
	//tiempo = time(NULL);
	//setlocale(LC_TIME,"es_ES");

	//tmPtr = localtime(&tiempo);

	//	PMString me_s;
		
		
	
	  fecha=DayWek(Dia->GetAsNumber(), mes->GetAsNumber(), ano->GetAsNumber());

		fecha.Append(" ");

		fecha.Append(Dia->GrabCString());
		fecha.Append(" de ");
		switch(mes->GetAsNumber())
		{
			case 1:
					fecha.Append("Enero");
					break;
			case 2:
					fecha.Append("Febrero");
					break;
			case 3:
					fecha.Append("Marzo");
					break;
					
			case 4:
					fecha.Append("Abril");
					break;
			case 5:
					fecha.Append("Mayo");
					break;
					
			case 6:
					fecha.Append("Junio");
					break;
			case  7:
					fecha.Append("Julio");
					break;
					
			case 8:
					fecha.Append("Agosto");
					break;
			case 9:
					fecha.Append("Septiembre");
					break;
			case 10:
					fecha.Append("Octubre");
					break;
			case 11:
					fecha.Append("Noviembre");
					break;
			case 12:
					fecha.Append("Diciembre");
					break;
		}
	  
	  fecha.Append(" de ");
	  fecha.Append(ano->GrabCString());
	  /*tmPtr->tm_mon=mes->GetAsNumber();

	  tmPtr->tm_mday= Dia->GetAsNumber();

	  tmPtr->tm_year=ano->GetAsNumber();
	  
	  me_s.AppendNumber( tmPtr->tm_mon);
	 CAlert::InformationAlert(me_s);

	  fecha.Append(" ");
	  mktime(tmPtr);
	  strftime( cad, 80,"%d de %B de 20%y",  Dia->GetAsNumber(),Dia->GetAsNumber(),ano->GetAsNumber());*/

	 // fecha.Append(cad);
	  return(fecha);
}


bool16 A2PFluirFunction::ShowThePageByUIDPage(int32 which) 
{ 
	bool16 retval=kTrue;
	do
	{
		UID PageUID; 
		UID DocUID; 

		UIDRef PageRef; 
		UIDRef DocRef; 
		IDocument *DocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		IDataBase *theDB = GetDataBase(DocPtr); 

		InterfacePtr<ICommand> SetPgCmd(CmdUtils::CreateCommand(kSetPageCmdBoss)); 
		InterfacePtr<IControlView> view( Utils<ILayoutUIUtils>()->QueryFrontView()); 
		InterfacePtr<ILayoutControlData> LCD(view, UseDefaultIID()); 

		//InterfacePtr<IPageList> PageList(DocPtr,IID_IPAGELIST); 
		//PMString PageString; 
		//PageString.AppendNumber(which); 
		PageUID = UID(which);//PageList->PageStringToUID(PageString); 
		if(PageUID == kInvalidUID)
		{ 
			//PostAlert("failed to move to the page--1"); 
			break;
			retval=kFalse; 
		} 

		//check here 
		PageRef = UIDRef(theDB,PageUID); 
		if (PageRef == UIDRef(nil, kInvalidUID))
		{ 
			//PostAlert("failed to move to the page--2"); 
			break;
			retval=kFalse; 
		} 
		InterfacePtr<IHierarchy> SpreadNode(PageRef,IID_IHIERARCHY); 
		SetPgCmd->SetItemList(UIDList(theDB,SpreadNode->GetSpreadUID())); 

		InterfacePtr<ILayoutCmdData> layoutData(SetPgCmd,IID_ILAYOUTCMDDATA); 
		DocUID = theDB->GetRootUID(); 
		DocRef = UIDRef(theDB,DocUID); 

		layoutData->Set(DocRef,LCD); 
		InterfacePtr<IBoolData> boolData(SetPgCmd,IID_IBOOLDATA); 
		boolData->Set(kFalse); 

		InterfacePtr<IUIDData> uidData(SetPgCmd,IID_IUIDDATA); 
		InterfacePtr<IGeometry> pageGeometry(PageRef, UseDefaultIID()); 
		uidData->Set(pageGeometry); 

		// Process the SetPageCmd: 
		if(CmdUtils::ProcessCommand(SetPgCmd) != kSuccess)
		{ 
			retval=kFalse; 	
		}
		else
			retval=kTrue; 
	}while(false);
	return(retval);
}


bool16 A2PFluirFunction::ShowThePageByNumberPage(int32 which) 
{ 
	bool16 retval=kTrue;
	do
	{
		UID PageUID; 
		UID DocUID; 

		UIDRef PageRef; 
		UIDRef DocRef; 
		IDocument *DocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		IDataBase *theDB = GetDataBase(DocPtr); 

		InterfacePtr<ICommand> SetPgCmd(CmdUtils::CreateCommand(kSetPageCmdBoss)); 
		InterfacePtr<IControlView> view(Utils<ILayoutUIUtils>()->QueryFrontView()); 
		InterfacePtr<ILayoutControlData> LCD(view, UseDefaultIID()); 

		InterfacePtr<IPageList> PageList(DocPtr,IID_IPAGELIST); 
		PMString PageString; 
		PageString.AppendNumber(which); 
		PageUID = PageList->PageStringToUID(PageString); 
		if(PageUID == kInvalidUID)
		{ 
			//PostAlert("failed to move to the page--1"); 
			retval=kFalse; 
		} 

		//check here 
		PageRef = UIDRef(theDB,PageUID); 
		if (PageRef == UIDRef(nil, kInvalidUID))
		{ 
			//PostAlert("failed to move to the page--2"); 
			retval=kFalse; 
		} 
		InterfacePtr<IHierarchy> SpreadNode(PageRef,IID_IHIERARCHY); 
		SetPgCmd->SetItemList(UIDList(theDB,SpreadNode->GetSpreadUID())); 

		InterfacePtr<ILayoutCmdData> layoutData(SetPgCmd,IID_ILAYOUTCMDDATA); 
		DocUID = theDB->GetRootUID(); 
		DocRef = UIDRef(theDB,DocUID); 

		layoutData->Set(DocRef,LCD); 
		InterfacePtr<IBoolData> boolData(SetPgCmd,IID_IBOOLDATA); 
		boolData->Set(kFalse); 

		InterfacePtr<IUIDData> uidData(SetPgCmd,IID_IUIDDATA); 
		InterfacePtr<IGeometry> pageGeometry(PageRef, UseDefaultIID()); 
		uidData->Set(pageGeometry); 

		// Process the SetPageCmd: 
		if(CmdUtils::ProcessCommand(SetPgCmd) != kSuccess)
		{ 
			retval=kFalse; 	
		}
		else
			retval=kTrue; 
	}while(false);
	return(retval);
}





bool16 A2PFluirFunction::ObtenerFechaPublicacionDeAvisos(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso, PMString &DateIssue)
{
	bool16 retval=kFalse;
	do
	{	
	
	//Busqueda del campo ruta y almacenamiento del contenido en variable filePath
		if(AvisoAFluir[0].NomCampo1=="Fecha" || AvisoAFluir[0].NomCampo1=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo1;
			break;
		}
		if(AvisoAFluir[0].NomCampo2=="Fecha" || AvisoAFluir[0].NomCampo2=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo2;
			break;
		}
		if(AvisoAFluir[0].NomCampo3=="Fecha" || AvisoAFluir[0].NomCampo3=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo3;
			break;
		}
		if(AvisoAFluir[0].NomCampo4=="Fecha" || AvisoAFluir[0].NomCampo4=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo4;
			break;
		}
		if(AvisoAFluir[0].NomCampo5=="Fecha" || AvisoAFluir[0].NomCampo5=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo5;
			break;
		}
		if(AvisoAFluir[0].NomCampo6=="Fecha" || AvisoAFluir[0].NomCampo6=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo6;
			break;
		}
		if(AvisoAFluir[0].NomCampo7=="Fecha" || AvisoAFluir[0].NomCampo7=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo7;
			break;
		}
		if(AvisoAFluir[0].NomCampo8=="Fecha" || AvisoAFluir[0].NomCampo8=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo8;
			break;
		}
		if(AvisoAFluir[0].NomCampo9=="Fecha" || AvisoAFluir[0].NomCampo9=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo9;
			break;
		}
		if(AvisoAFluir[0].NomCampo10=="Fecha" || AvisoAFluir[0].NomCampo10=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo10;
			break;
		}
		if(AvisoAFluir[0].NomCampo11=="Fecha" || AvisoAFluir[0].NomCampo11=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo11;
			break;
		}
		if(AvisoAFluir[0].NomCampo12=="Fecha" || AvisoAFluir[0].NomCampo12=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo12;
			break;
		}
		if(AvisoAFluir[0].NomCampo13=="Fecha" || AvisoAFluir[0].NomCampo13=="Date")
		{
			retval=kTrue;
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo13;
			break;
		}
		if(AvisoAFluir[0].NomCampo14=="Fecha" || AvisoAFluir[0].NomCampo14=="Date")
		{
			DateIssue=AvisoAFluir[numAviso].ContenidoCampo14;
			retval=kTrue;
			break;
		}
	
	}while(false);
	return retval;
}

bool16 A2PFluirFunction::ObtenerTipoPaginaDeAvisos(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso, PMString &TipoPagina)
{
	bool16 retval=kFalse;
	do
	{	
	
	//Busqueda del campo ruta y almacenamiento del contenido en variable filePath
		if(AvisoAFluir[0].NomCampo1=="Tipo pagina" || AvisoAFluir[0].NomCampo1=="Page type" || AvisoAFluir[0].NomCampo1=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo1;
			break;
		}
		if(AvisoAFluir[0].NomCampo2=="Tipo pagina" || AvisoAFluir[0].NomCampo2=="Page type" || AvisoAFluir[0].NomCampo2=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo2;
			break;
		}
		if(AvisoAFluir[0].NomCampo3=="Tipo pagina" || AvisoAFluir[0].NomCampo3=="Page type" || AvisoAFluir[0].NomCampo3=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo3;
			break;
		}
		if(AvisoAFluir[0].NomCampo4=="Tipo pagina" || AvisoAFluir[0].NomCampo4=="Page type" || AvisoAFluir[0].NomCampo4=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo4;
			break;
		}
		if(AvisoAFluir[0].NomCampo5=="Tipo pagina" || AvisoAFluir[0].NomCampo5=="Page type" || AvisoAFluir[0].NomCampo5=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo5;
			break;
		}
		if(AvisoAFluir[0].NomCampo6=="Tipo pagina" || AvisoAFluir[0].NomCampo6=="Page type" || AvisoAFluir[0].NomCampo6=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo6;
			break;
		}
		if(AvisoAFluir[0].NomCampo7=="Tipo pagina" || AvisoAFluir[0].NomCampo7=="Page type" || AvisoAFluir[0].NomCampo7=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo7;
			break;
		}
		if(AvisoAFluir[0].NomCampo8=="Tipo pagina" || AvisoAFluir[0].NomCampo8=="Page type" || AvisoAFluir[0].NomCampo8=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo8;
			break;
		}
		if(AvisoAFluir[0].NomCampo9=="Tipo pagina" || AvisoAFluir[0].NomCampo9=="Page type" || AvisoAFluir[0].NomCampo9=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo9;
			break;
		}
		if(AvisoAFluir[0].NomCampo10=="Tipo pagina" || AvisoAFluir[0].NomCampo10=="Page type" || AvisoAFluir[0].NomCampo10=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo10;
			break;
		}
		if(AvisoAFluir[0].NomCampo11=="Tipo pagina" || AvisoAFluir[0].NomCampo11=="Page type" || AvisoAFluir[0].NomCampo11=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo11;
			break;
		}
		if(AvisoAFluir[0].NomCampo12=="Tipo pagina" || AvisoAFluir[0].NomCampo12=="Page type" || AvisoAFluir[0].NomCampo12=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo12;
			break;
		}
		if(AvisoAFluir[0].NomCampo13=="Tipo pagina" || AvisoAFluir[0].NomCampo13=="Page type" || AvisoAFluir[0].NomCampo13=="Tipo p‚àö¬∞gina")
		{
			retval=kTrue;
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo13;
			break;
		}
		if(AvisoAFluir[0].NomCampo14=="Tipo pagina" || AvisoAFluir[0].NomCampo14=="Page type" || AvisoAFluir[0].NomCampo14=="Tipo p‚àö¬∞gina")
		{
			TipoPagina=AvisoAFluir[numAviso].ContenidoCampo14;
			retval=kTrue;
			break;
		}
	
	}while(false);
	return retval;
}



bool16 A2PFluirFunction::GetPathIssueOdAds(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso, PMString& filePath)
{
	bool16 retval=kFalse;
	do
	{	
	
	//Busqueda del campo ruta y almacenamiento del contenido en variable filePath
		if(AvisoAFluir[0].NomCampo1=="Ruta" || AvisoAFluir[0].NomCampo1=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo1;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo2=="Ruta" || AvisoAFluir[0].NomCampo2=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo2;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo3=="Ruta" || AvisoAFluir[0].NomCampo3=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo3;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo4=="Ruta" || AvisoAFluir[0].NomCampo4=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo4;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo5=="Ruta" || AvisoAFluir[0].NomCampo5=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo5;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo6=="Ruta" || AvisoAFluir[0].NomCampo6=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo6;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo7=="Ruta" || AvisoAFluir[0].NomCampo7=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo7;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo8=="Ruta" || AvisoAFluir[0].NomCampo8=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo8;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo9=="Ruta" || AvisoAFluir[0].NomCampo9=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo9;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo10=="Ruta" || AvisoAFluir[0].NomCampo10=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo10;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo11=="Ruta" || AvisoAFluir[0].NomCampo11=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo11;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo12=="Ruta" || AvisoAFluir[0].NomCampo12=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo12;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo13=="Ruta" || AvisoAFluir[0].NomCampo13=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo13;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo14=="Ruta" || AvisoAFluir[0].NomCampo14=="Path")
		{
			filePath=AvisoAFluir[numAviso].ContenidoCampo14;
			retval=kTrue;
			break;
		}
	}while(false);
	return retval;
}



bool16 A2PFluirFunction::GetPMStringCoordinatesIssueOdAds(Preferencias &Prefer,StructAvisos *AvisoAFluir,int32 numAviso, PMString& Coordenadas)
{
	bool16 retval=kFalse;
	do
	{
		
		//PMString sss="";
		//sss.AppendNumber(AvisoAFluir[0].NomCampo1.NumUTF16TextChars());
		//CAlert::InformationAlert(AvisoAFluir[0].NomCampo1+","+sss);
		if(AvisoAFluir[0].NomCampo1=="Coordenadas" || AvisoAFluir[0].NomCampo1=="Coordinates" || AvisoAFluir[0].NomCampo1.Contains("Coordinates") || AvisoAFluir[0].NomCampo1.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo1;
			retval=kTrue;
			break;
		}
		
		//sss="";
		//sss.AppendNumber(AvisoAFluir[0].NomCampo2.NumUTF16TextChars());
		//CAlert::InformationAlert(AvisoAFluir[0].NomCampo2+","+sss);
		if(AvisoAFluir[0].NomCampo2=="Coordenadas" || AvisoAFluir[0].NomCampo2=="Coordinates" || AvisoAFluir[0].NomCampo2.Contains("Coordinates") || AvisoAFluir[0].NomCampo2.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo2;
			retval=kTrue;
			break;
		}
		
		//sss="";
		//sss.AppendNumber(AvisoAFluir[0].NomCampo3.NumUTF16TextChars());
		//CAlert::InformationAlert(AvisoAFluir[0].NomCampo3+","+sss);
		if(AvisoAFluir[0].NomCampo3=="Coordenadas" || AvisoAFluir[0].NomCampo3=="Coordinates" || AvisoAFluir[0].NomCampo3.Contains("Coordinates") || AvisoAFluir[0].NomCampo3.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo3;
			retval=kTrue;
			break;
		}
		
		//sss="";
		//sss.AppendNumber(AvisoAFluir[0].NomCampo4.NumUTF16TextChars());
		//CAlert::InformationAlert(AvisoAFluir[0].NomCampo4+","+sss);
		if(AvisoAFluir[0].NomCampo4=="Coordenadas" || AvisoAFluir[0].NomCampo4=="Coordinates" || AvisoAFluir[0].NomCampo4.Contains("Coordinates") || AvisoAFluir[0].NomCampo4.Contains("Coordenadas"))
		{		
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo4;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo5=="Coordenadas" || AvisoAFluir[0].NomCampo5=="Coordinates" || AvisoAFluir[0].NomCampo5.Contains("Coordinates") || AvisoAFluir[0].NomCampo5.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo5;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo6=="Coordenadas" || AvisoAFluir[0].NomCampo6=="Coordinates"  || AvisoAFluir[0].NomCampo6.Contains("Coordinates") || AvisoAFluir[0].NomCampo6.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo6;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo7=="Coordenadas" || AvisoAFluir[0].NomCampo7=="Coordinates" || AvisoAFluir[0].NomCampo7.Contains("Coordinates") || AvisoAFluir[0].NomCampo7.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo7;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo8=="Coordenadas" || AvisoAFluir[0].NomCampo8=="Coordinates" || AvisoAFluir[0].NomCampo8.Contains("Coordinates") || AvisoAFluir[0].NomCampo8.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo8;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo9=="Coordenadas" || AvisoAFluir[0].NomCampo9=="Coordinates" || AvisoAFluir[0].NomCampo9.Contains("Coordinates") || AvisoAFluir[0].NomCampo9.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo9;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo10=="Coordenadas" || AvisoAFluir[0].NomCampo10=="Coordinates" || AvisoAFluir[0].NomCampo10.Contains("Coordinates") || AvisoAFluir[0].NomCampo10.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo10;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo11=="Coordenadas" || AvisoAFluir[0].NomCampo11=="Coordinates" || AvisoAFluir[0].NomCampo11.Contains("Coordinates") || AvisoAFluir[0].NomCampo11.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo11;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo12=="Coordenadas" || AvisoAFluir[0].NomCampo12=="Coordinates" || AvisoAFluir[0].NomCampo12.Contains("Coordinates") || AvisoAFluir[0].NomCampo12.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo12;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo13=="Coordenadas" || AvisoAFluir[0].NomCampo13=="Coordinates" || AvisoAFluir[0].NomCampo13.Contains("Coordinates") || AvisoAFluir[0].NomCampo13.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo13;
			retval=kTrue;
			break;
		}
		if(AvisoAFluir[0].NomCampo14=="Coordenadas" || AvisoAFluir[0].NomCampo14=="Coordinates" || AvisoAFluir[0].NomCampo14.Contains("Coordinates") || AvisoAFluir[0].NomCampo14.Contains("Coordenadas"))
		{
			Coordenadas=AvisoAFluir[numAviso].ContenidoCampo14;
			retval=kTrue;
			break;
		}
	}while(false);
	return retval;
}



bool16 A2PFluirFunction::ActualizarDocumentoPlantillero(const PMString& Publicacion,const PMString& Seccion,const PMString& NumPagina,const PMString& FechaPublicacionPag)
{
	
	
	int32 NumerodeAviso;			//variable util en ciclo	
	PMString Date="";
	PMString FechaStandart="";
	PMString DireccionPMString = DireccionArchivo();;
	PMString NomSeccionAFluir=Seccion+" "+NumPagina;;		//Nombre de la seccion que se va a fluir
	PMString LastModifFileImported="";
	PMString CadenaSecionesXPagina=Seccion+" "+NumPagina;
	int32 UIDNumberPage=0;					//UID de Pagida donde sera fluido la seccion
	PMString TextoUpdate="";
	//PnlTrvUtils::GetXMPVar("A2PPathLastFileImported",DireccionPMString);
	PnlTrvUtils::GetXMPVar("A2PPathLastSectionImported",CadenaSecionesXPagina);
	//PnlTrvUtils::GetXMPVar("A2PDateLAstFileImported",LastModifFileImported);

	PnlTrvUtils::SaveXMPVar("A2PPathLastFileImported","");
	PnlTrvUtils::SaveXMPVar("A2PPathLastSectionImported","");
	PnlTrvUtils::SaveXMPVar("A2PDateLAstFileImported","");
	
	do
	{
		
		
		InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
		(
			kA2PPreferenciasFuncionesBoss,	// Object boss/class
			IA2PPreferenciasFunctions::kDefaultIID
		)));
		
		if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
		{
			CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
			break;
		}
		
		
		
		Prefer.SepEnCoordenadas.Remove(0,1);
		
		
		///fModel se utiliza para usar las funciones de lectura y escritura de los archivos de preferencias
		NumAvisos =   FModel.CantidadAvisos(DireccionPMString);
	
		
		//StructAvisos Avisos llenad de todos los avisos en la estructura
		StructAvisos *Avisos=new StructAvisos[NumAvisos];
		LlenarStructAvisos(Avisos,DireccionPMString,NumAvisos,Prefer);							
		
		
		this->DELETE_AvisosYEtiquetasParaUpdateGeometria();
		
		PnlTrvUtils::RemoveXMPVar("A2PTextUpdate");
		//OBTENCION DE AVISOS DE LA SECCION//
		//numero de avisos a fluir//
		do
		{	
			
			UIDNumberPage = CadenaSecionesXPagina.GetAsNumber();
			CadenaSecionesXPagina.Remove( 0 , (CadenaSecionesXPagina.IndexOfString(",")+1) );
			
			
			
			NomSeccionAFluir = CadenaSecionesXPagina.Substring(0,(CadenaSecionesXPagina.IndexOfString("\n")))->GrabCString();
			CadenaSecionesXPagina.Remove( 0 , (CadenaSecionesXPagina.IndexOfString("\n")+1) );
			
			PMString StrSeccion="";
			PMString StrNumPage="";
			
			PMString *TokensOnElemnt = NomSeccionAFluir.GetItem(" ",1);
			PMString strSubString = NomSeccionAFluir;
			int32 nCountTags = 0;
			bool16 sale = kFalse;
			if(TokensOnElemnt)
			{
				StrSeccion= *TokensOnElemnt;
				if(strSubString.CharCount() > StrSeccion.CharCount()+1)
				{
					PMString *pSubstring = strSubString.Substring(StrSeccion.CharCount()+1);
					StrNumPage = *pSubstring;
				}
				
			}
			
			bool16 UtilizoPlantillaBD=kFalse;
			//Para Utilizar el plantillero
						
			
			
			if(ShowThePageByUIDPage(UIDNumberPage))
			{
				
				
				NumAvisoSec = this->ObtenerCantAvisosDeSeccion(Avisos, NomSeccionAFluir, NumAvisos);	
				
				
				
				StructAvisos *AvisoAFluir=new StructAvisos[NumAvisoSec];
				
				
				//obtencion de los avisos a fluir//
				ObtenerAvisosDeSeccion(Avisos,AvisoAFluir,NomSeccionAFluir,NumAvisos, NumAvisoSec);
				
				
				///////////////////////////////////////////
				
				PMString DateIssue="";
				PMString TipoPagina="";
				this->ObtenerFechaPublicacionDeAvisos(Prefer,AvisoAFluir,0,DateIssue);	//Obtiene la fecha de la publicacion del aviso
				if(this->ObtenerTipoPaginaDeAvisos(Prefer,AvisoAFluir,0,TipoPagina)==kFalse)		//Obtiene el tipo de pagina en que se fluira el aviso
					TipoPagina="";
				FechaEdicion=DateIssue;
				
				bool16 UtilizoPlantillaBD=kFalse;
				//Para Utilizar el plantillero
						
				/*InterfacePtr<IPLTROImport> PlantilleroInterface(static_cast<IPLTROImport*> (CreateObject
				(
					kPlantilleroImpImportacionBoss,	// Object boss/class
					IPLTROImport::kDefaultIID
				)));
			
				if(PlantilleroInterface==nil)
				{
					//Para cuando no se desea abrir las plantillas
							
				}
				else
				{
					PlantilleroInterface->FluirElementosPlantillero( FechaEdicion, StrSeccion, StrNumPage ,TipoPagina, Prefer.NombreEditorial);
				}*/
				
				/////////////////////////////////////////////
				
/*				

				PnlTrvUtils::GetXMPVar("N2PDate",FechaEdicion);
				
				if(FechaEdicion.NumUTF16TextChars()<=0)
					FechaEdicion = FechaYHora(FechaStandart);
				
				PaginaFluida=NomSeccionAFluir;
			
				
				
				Date=FechaEdicion;
				//para salir de la funcion sin fluir
				if(FechaEdicion=="?" && PaginaFluida=="?")
					break;
				//FechaEdicion=ArmaFecha(FechaEdicion);
*/
						
				
				///////////////////////////////////////////////////////////////
				UIDRef refDoc(::GetUIDRef(Utils<ILayoutUIUtils>()->GetFrontDocument()));
				
				
				InterfacePtr<IDocument> myDoc(refDoc,IID_IDOCUMENT);
				
				
				IDataBase *db=refDoc.GetDataBase();
			
				
			
				//crea colores que utiliza Ads2Page 2.0
				this->crearColores();
			
				
				//crea las capas dependiendo preferencias
				if(!CreaciondeCapas(Prefer))
				{
					CAlert::InformationAlert("Los anuncios no fueron colocados por que se selecciono ninguna capa en preferencias");
					break;
				}
				
				
			
				PnlTrvUtils::GetXMPVar("A2PTextUpdate",TextoUpdate);
				
				//ciclo para fluir los nodos hijos
				for(NumerodeAviso=0;NumerodeAviso<NumAvisoSec;NumerodeAviso++)
				{
					//si no se existe la capa imagen entonces se fluye sobre la capa update
					//	CAlert::WarningAlert("13");

					if(!ActivarCapa("Imagen"))
						if(!ActivarCapa("Editorial"))
							if(!ActivarCapa("Etiquetas C/Publicidad"))
								ActivarCapa("Etiquetas S/Publicidad");
											
					//crea rectangulo de imagen. Dentro de esta imagen se hacen los diferentes
					//llamadas a otras funciones para la creacion de etiquetas
						
								
					num_rectangulos=1;
					
					CrearRectanguloImagen(Prefer,AvisoAFluir,NumerodeAviso);
											
					
								
								
					TextoUpdate.Append("UIDFrameToImage:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].UIDFrameToImage);
					TextoUpdate.Append(",");
					TextoUpdate.Append("PathToImage:");
					TextoUpdate.Append(AvisoAFluir[NumerodeAviso].PathToImage);
					TextoUpdate.Append(",");
					TextoUpdate.Append("LastTimeImage:");
					TextoUpdate.Append(AvisoAFluir[NumerodeAviso].LastTimeImage);
								
					TextoUpdate.Append(",");
					TextoUpdate.Append("ExistImageFile:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].ExistImageFile);
					TextoUpdate.Append(",");
					TextoUpdate.Append("UIDFrameEtiqueta:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].UIDFrameEtiqueta);
					TextoUpdate.Append(",");
								
					TextoUpdate.Append("UIDPageFlowed:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].UIDPageFlowed);
					TextoUpdate.Append(",");
								
								
					TextoUpdate.Append("CordenadasFrameToImage:");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].LFrameImage);
					TextoUpdate.Append("/");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].TFrameImage);
					TextoUpdate.Append("/");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].RFrameImage);
					TextoUpdate.Append("/");
					TextoUpdate.AppendNumber(AvisoAFluir[NumerodeAviso].BFrameImage);	
								
					TextoUpdate.Append("\n");	
				}
				
				
				
				
				//libera memoria 
			
			
				//creacion del textupdate
					
				PnlTrvUtils::SaveXMPVar("A2PTextUpdate",TextoUpdate);
				TextoUpdate.Clear();//

						
				
						
				///Bloqueo de Capas	
				this->AplicaPreferenciasACapas();
							
				
				
				//adiciona el UID donde fue fluida la seccion
				
				
				PMString TextUpdateOnXMP="";
				PnlTrvUtils::GetXMPVar("A2PPathLastSectionImported",TextUpdateOnXMP);
							
				PMString NewPagUIDAndSecction="";
				NewPagUIDAndSecction.AppendNumber(AvisoAFluir[NumerodeAviso-1].UIDPageFlowed);
				NewPagUIDAndSecction.Append(",");
				NewPagUIDAndSecction.Append(NomSeccionAFluir);
				
							
				if(TextUpdateOnXMP.NumUTF16TextChars()>0)
				{
					bool16 YaSehaFluyoEstaSeccion=kFalse;
					int32 numItem=1;
					
					PMString *Item=TextUpdateOnXMP.GetItem("\n",numItem);
					while(Item!=nil && YaSehaFluyoEstaSeccion==kFalse)
					{
						
						PMString Luna(Item->GrabCString());
							
						if(Luna==NewPagUIDAndSecction)
						{
								
							YaSehaFluyoEstaSeccion=kTrue;
						}
						numItem++;
						Item=TextUpdateOnXMP.GetItem("\n",numItem);
					}
								
								
					if(YaSehaFluyoEstaSeccion==kFalse)
					{
						NewPagUIDAndSecction.Append("\n");
						TextUpdateOnXMP.Append(NewPagUIDAndSecction);
					}
				}
				else
				{
					NewPagUIDAndSecction.Append("\n");
					TextUpdateOnXMP.Append(NewPagUIDAndSecction);
				}
				
				
				
				PnlTrvUtils::SaveXMPVar("A2PPathLastFileImported",DireccionPMString);
				PnlTrvUtils::SaveXMPVar("A2PPathLastSectionImported",TextUpdateOnXMP);
				PMString 	LastModifFileImported(A2PPrefeFuntions->ProporcionaFechaArchivo(DireccionPMString.GrabCString()));
				PnlTrvUtils::SaveXMPVar("A2PDateLAstFileImported", LastModifFileImported);
				
				
				//libera memoria 
				//delete AvisoAFluir;	
			
				//delete Avisos;		
				
			
													
			}
			else
			{
				CAlert::ErrorAlert("Se ha borrado la Pagina donde se fluyo la seccion.");
			}
			
		}while(CadenaSecionesXPagina.NumUTF16TextChars()>0);//la cadena de Secciones por pagina tenga caracteres
	}while(false);
	return(kTrue);

}



/**
	Borra Avisos y etiquetas para comenzar con la actualizacion de geometria
*/
bool16 A2PFluirFunction::DELETE_AvisosYEtiquetasParaUpdateGeometria()
{
	
	bool16 retval=kTrue;
	do
	{
		InterfacePtr<IA2PActualizarFunction> ActualizarFunction(static_cast<IA2PActualizarFunction*> (CreateObject
				(
					kA2PActualizarFunctionBoss,	// Object boss/class
					IA2PActualizarFunction::kDefaultIID
				)));
				
				if(ActualizarFunction!=nil)
				{
					
					ActualizarFunction->DELETE_AvisosYEtiquetasParaUpdateGeometria();
					retval=kTrue;
					
				}
				else
				{
					retval=kFalse;
				}
	}while(false);	
	return retval;

}
