//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.com S.A. de C.V.
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2008 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================


#ifndef __A2PImpID_h__
#define __A2PImpID_h__

#include "SDKDef.h"

// Company:
#define kA2PImpCompanyKey	"Interlasa"		// Company name used internally for menu paths and the like. Must be globally unique, only A-Z, 0-9, space and "_".
#define kA2PImpCompanyValue	"Interlasa"	// Company name displayed externally.

// Plug-in:
#define kA2PImpPluginName	"Ads2Page"			// Name of this plug-in.
#define kA2PImpPrefixNumber	0x73000 		// Unique prefix number for this plug-in(*Must* be obtained from Adobe Developer Support).
#define kA2PImpVersion		"v4.7.8M CS5"						// Version of this plug-in (for the About Box).
#define kA2PImpAuthor		"Interlasa.com S.A. de C.V."					// Author of this plug-in (for the About Box).

// Plug-in Prefix: (please change kA2PImpPrefixNumber above to modify the prefix.)
#define kA2PImpPrefix		RezLong(kA2PImpPrefixNumber)				// The unique numeric prefix for all object model IDs for this plug-in.
#define kA2PImpStringPrefix	SDK_DEF_STRINGIZE(kA2PImpPrefixNumber)	// The string equivalent of the unique prefix number for  this plug-in.

// Missing plug-in: (see ExtraPluginInfo resource)
#define kA2PImpMissingPluginURLValue		kSDKDefPartnersStandardValue_enUS // URL displayed in Missing Plug-in dialog
#define kA2PImpMissingPluginAlertValue	kSDKDefMissingPluginAlertValue // Message displayed in Missing Plug-in dialog - provide a string that instructs user how to solve their missing plug-in problem

// PluginID:
DECLARE_PMID(kPlugInIDSpace, kA2PImpPluginID, kA2PImpPrefix + 0)

// ClassIDs:
DECLARE_PMID(kClassIDSpace, kA2PImpActionComponentBoss,	kA2PImpPrefix + 0)
DECLARE_PMID(kClassIDSpace, kA2PImpPTVStringRegisterBoss,	kA2PImpPrefix + 1)
DECLARE_PMID(kClassIDSpace, kA2PImpPnlTrvMenuRegisterBoss,	kA2PImpPrefix + 2)
DECLARE_PMID(kClassIDSpace, kA2PImpPnlTrvActionRegisterBoss,	kA2PImpPrefix + 3)
DECLARE_PMID(kClassIDSpace, kA2PImpPnlTrvPanelWidgetBoss,		kA2PImpPrefix + 4)
DECLARE_PMID(kClassIDSpace, kA2PImpPnlTrvPanelRegisterBoss,	kA2PImpPrefix + 5)
DECLARE_PMID(kClassIDSpace, kA2PImpPnlTrvTreeViewWidgetBoss,	kA2PImpPrefix + 6)
DECLARE_PMID(kClassIDSpace, kA2PImpPnlTrvNodeWidgetBoss,		kA2PImpPrefix + 7)
DECLARE_PMID(kClassIDSpace, kA2PImpPnlTrvChangeOptionsCmdBoss,kA2PImpPrefix + 8)
DECLARE_PMID(kClassIDSpace, kA2PImpPnlTrvStartupShutdownBoss,	kA2PImpPrefix + 9)
DECLARE_PMID(kClassIDSpace, kA2PImpPnlTrvCViewPanelWidgetBoss,	kA2PImpPrefix + 10)
DECLARE_PMID(kClassIDSpace, kA2PImpBotonImportarSuiteWidgetBoss, kA2PImpPrefix + 11)
DECLARE_PMID(kClassIDSpace, kA2PImpBotonFluirSuiteWidgetBoss, kA2PImpPrefix + 12)
DECLARE_PMID(kClassIDSpace, kA2PImpSelDlgCheckBoxSuiteWidgetBoss, kA2PImpPrefix + 13)
DECLARE_PMID(kClassIDSpace, kA2PImpBotonCerrarSuiteWidgetBoss, kA2PImpPrefix + 14)
DECLARE_PMID(kClassIDSpace, kA2PImpDlgGetFechaPagDialogBoss, kA2PImpPrefix + 15)

DECLARE_PMID(kClassIDSpace, kA2PActualizarFunctionBoss, kA2PImpPrefix + 16)
DECLARE_PMID(kClassIDSpace, kA2PFluirFunctionBoss, 		kA2PImpPrefix + 17)
DECLARE_PMID(kClassIDSpace, kA2PImpRollOverIconCalButtonObserverBoss, 		kA2PImpPrefix + 18)
DECLARE_PMID(kClassIDSpace, kA2PAvisosBoss, 		kA2PImpPrefix + 19)



// InterfaceIDs:
DECLARE_PMID(kInterfaceIDSpace, IID_IPNLTRVSHADOWEVENTHANDLER,	kA2PImpPrefix + 0)
DECLARE_PMID(kInterfaceIDSpace, IID_IPnlTrvOptions,				kA2PImpPrefix + 1)
DECLARE_PMID(kInterfaceIDSpace, IID_IPnlTrvChangeOptionsCmdData,kA2PImpPrefix + 2)
DECLARE_PMID(kInterfaceIDSpace, IID_IPnlTrvIdleTask,			kA2PImpPrefix + 3)
DECLARE_PMID(kInterfaceIDSpace, IID_IPnlTrvRefreshStatus,		kA2PImpPrefix + 4)
DECLARE_PMID(kInterfaceIDSpace, IID_IA2PActualizarFunction,		kA2PImpPrefix + 5)
DECLARE_PMID(kInterfaceIDSpace, IID_IA2PFluirFunction,			kA2PImpPrefix + 6)
DECLARE_PMID(kInterfaceIDSpace, IID_IA2PAVISOSINTERFACE,		kA2PImpPrefix + 7)
DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPSUITE,               kA2PImpPrefix + 8)//aÒadi mary
DECLARE_PMID(kInterfaceIDSpace, IID_IA2PImlVerUpdatePublicidadTask,				kA2PImpPrefix + 9)
DECLARE_PMID(kInterfaceIDSpace, IID_IA2PImpAlertaDeCambioTask,	kA2PImpPrefix + 10)//task AlertaDeCambio


// ImplementationIDs:
DECLARE_PMID(kImplementationIDSpace, kA2PImpActionComponentImpl,			kA2PImpPrefix + 0)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvTVWidgetMgrImpl,				kA2PImpPrefix + 1)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvTreeObserverImpl,				kA2PImpPrefix + 2)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvTVHierarchyAdapterImpl,		kA2PImpPrefix + 3)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvNodeEHImpl,					kA2PImpPrefix + 4)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvChangeOptionsCmdImpl,		kA2PImpPrefix + 5)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvOptionsImpl,					kA2PImpPrefix + 6)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvChangeOptionsCmdDataImpl,	kA2PImpPrefix + 7)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvNodeObserverImpl,			kA2PImpPrefix + 8)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvIdleTaskImpl,				kA2PImpPrefix + 9)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvStartupShutdownImpl,		kA2PImpPrefix + 10)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvCustomViewImpl, kA2PImpPrefix + 11)
DECLARE_PMID(kImplementationIDSpace, kA2PImpTrvDragDropSourceImpl, kA2PImpPrefix + 12)

DECLARE_PMID(kImplementationIDSpace, kA2PImpBotonImportarObserverImpl,  kA2PImpPrefix+ 13)
DECLARE_PMID(kImplementationIDSpace, kA2PImpBotonFluirObserverImpl, kA2PImpPrefix + 14)
DECLARE_PMID(kImplementationIDSpace, kA2PImpCheckBoxObserverImpl, kA2PImpPrefix + 15)
DECLARE_PMID(kImplementationIDSpace, kA2PImpBotonCerrarObserverImpl,  kA2PImpPrefix+ 16)
DECLARE_PMID(kImplementationIDSpace, kA2PImpDlgGetFechaPagCreatorImpl,  kA2PImpPrefix+ 17)
DECLARE_PMID(kImplementationIDSpace, kA2PImpDlgGetFechaPagControllerImpl,  kA2PImpPrefix+ 18)

DECLARE_PMID(kImplementationIDSpace, kA2PImpActualizarFunctionImpl,  kA2PImpPrefix+ 19)
DECLARE_PMID(kImplementationIDSpace, kA2PImpFluirFunctionImpl,  kA2PImpPrefix+ 20)
DECLARE_PMID(kImplementationIDSpace, kA2PImpRollOverCalButtonObserverImpl,  kA2PImpPrefix+ 21)
DECLARE_PMID(kImplementationIDSpace, kA2PImpDlgGetFechaPagObserverImpl,  kA2PImpPrefix+ 22)
DECLARE_PMID(kImplementationIDSpace, kA2PImpAvisosImpl,  kA2PImpPrefix+ 23)
DECLARE_PMID(kImplementationIDSpace, kA2PImpVerUpdatePublicidadTaskImpl,  kA2PImpPrefix+ 24)

DECLARE_PMID(kImplementationIDSpace, A2PImpBlinkAvisoDeCambioImpl, kA2PImpPrefix + 25)
DECLARE_PMID(kImplementationIDSpace, kA2PImpAlertaDeCambioTaskImpl, kA2PImpPrefix + 26)//task AlertaDeCambio



// ActionIDs:
DECLARE_PMID(kActionIDSpace, kA2PImpAboutActionID,				kA2PImpPrefix + 0)
DECLARE_PMID(kActionIDSpace, kA2PImpPanelWidgetActionID,		kA2PImpPrefix + 1)
DECLARE_PMID(kActionIDSpace, kA2PImpSeparator1ActionID,			kA2PImpPrefix + 2)
DECLARE_PMID(kActionIDSpace, kA2PImpPopupAboutThisActionID,		kA2PImpPrefix + 3)
DECLARE_PMID(kActionIDSpace, kA2PImpPanelImportDialogActionID, kA2PImpPrefix + 4)
DECLARE_PMID(kActionIDSpace, kA2PImpPanelUpdateDialogActionID, kA2PImpPrefix + 5)
DECLARE_PMID(kActionIDSpace, kA2PImpPanelPreferenciasActionID, kA2PImpPrefix + 6)
DECLARE_PMID(kActionIDSpace, kA2PImpNewPageFromPlantilleroActionID, kA2PImpPrefix + 7)
DECLARE_PMID(kActionIDSpace, kA2PImpRefreshActionID,			kA2PImpPrefix + 11)
DECLARE_PMID(kActionIDSpace, kA2PImpSetOptionsActionID,			kA2PImpPrefix + 12)

// WidgetIDs:
DECLARE_PMID(kWidgetIDSpace, kA2PImpPanelWidgetID,					kA2PImpPrefix + 0) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpTreeViewWidgetID,				kA2PImpPrefix + 1) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpNodeWidgetId,					kA2PImpPrefix + 2) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpNodeNameWidgetID,				kA2PImpPrefix + 3) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpElementWidgetId,				kA2PImpPrefix + 4) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpIconWidgetId,					kA2PImpPrefix + 5) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpCustomPanelViewWidgetID,		kA2PImpPrefix + 6)
DECLARE_PMID(kWidgetIDSpace, kA2PImpTextMessageWidgetID,			kA2PImpPrefix + 7)
DECLARE_PMID(kWidgetIDSpace, kA2PImpTreeNodeCheckBoxWidgetID,			kA2PImpPrefix + 8) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpOKButtonImportarWidgetID,				kA2PImpPrefix + 9) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpOKButtonFluirWidgetID,				kA2PImpPrefix + 10) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpOKButtonCerrarWidgetID,				kA2PImpPrefix + 11) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpDlgGetFechaYPag_PlantilleroWidgetID,				kA2PImpPrefix + 12) 
DECLARE_PMID(kWidgetIDSpace, kA2PImpTextFechaWidgetID,				kA2PImpPrefix + 13)
DECLARE_PMID(kWidgetIDSpace, kA2PImpTextPaginaWidgetID,				kA2PImpPrefix + 14)
DECLARE_PMID(kWidgetIDSpace, kA2PImpLogoWidgetID,				kA2PImpPrefix + 15)
DECLARE_PMID(kWidgetIDSpace, kA2PImpCalRollOverIconButtonWidgetID,				kA2PImpPrefix + 16)
DECLARE_PMID(kWidgetIDSpace, kA2PImpFileImportedNameWidgetID,				kA2PImpPrefix + 17)
DECLARE_PMID(kWidgetIDSpace, kA2PImpButtonUpdateWidgetID,				kA2PImpPrefix + 18)
DECLARE_PMID(kWidgetIDSpace, kA2PImpNumberDePaginaAFluirWidgetID,				kA2PImpPrefix + 19)
DECLARE_PMID(kWidgetIDSpace, kA2PImpTextFechaStandartWidgetID,				kA2PImpPrefix + 20)
DECLARE_PMID(kWidgetIDSpace, kN2PsqlAlertActualizarNotasWidgetID,				kA2PImpPrefix + 21)
DECLARE_PMID(kWidgetIDSpace, kA2PImpIconoDeAlertaWidgetID,				kA2PImpPrefix + 22)
DECLARE_PMID(kWidgetIDSpace, GuardaStringWidgetID,	kA2PImpPrefix + 23)
DECLARE_PMID(kWidgetIDSpace, GuardaStringFechaWidgetID,	kA2PImpPrefix + 24)
DECLARE_PMID(kWidgetIDSpace, GuardaIntWidgetID,	kA2PImpPrefix + 25)

// Service IDs
//IDs de Servicios
DECLARE_PMID(kServiceIDSpace, kA2PImpSelDlgService, kA2PImpPrefix + 0)

// "About Plug-ins" sub-menu:
#define kA2PImpAboutMenuKey			kA2PImpStringPrefix "kA2PImpAboutMenuKey"
#define kA2PImpAboutMenuPath		kSDKDefStandardAboutMenuPath kA2PImpCompanyKey

// "Plug-ins" sub-menu:
#define kA2PImpPluginsMenuKey 		kA2PImpStringPrefix "kA2PImpPluginsMenuKey"
#define kA2PImpPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kA2PImpCompanyKey kSDKDefDelimitMenuPath kA2PImpPluginsMenuKey
//#define kA2PImpPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kA2PImpCompanyKey
// Menu item keys:
#define kA2PImpRefreshMenuItemKey				kA2PImpStringPrefix "kA2PImpRefreshMenuItemKey"
#define kA2PImpSetOptionsMenuItemKey			kA2PImpStringPrefix "kA2PImpSetOptionsMenuItemKey"

#define kA2PImpBotonFluirItemKey						kA2PImpStringPrefix "kA2PImpBotonFluirItemKey"
#define kA2PImpBotonImportarItemKey						kA2PImpStringPrefix "kA2PImpBotonImportarItemKey"
#define kA2PImpBotonCerraItemKey						kA2PImpStringPrefix "kA2PImpBotonCerraItemKey"

// Other StringKeys:
#define kA2PImpAboutBoxStringKey				kA2PImpStringPrefix "kA2PImpAboutBoxStringKey"
#define kA2PImpRefreshStringKey					kA2PImpStringPrefix "kA2PImpRefreshStringKey"
#define kA2PImpPanelTitleKey					kA2PImpStringPrefix	"kA2PImpPanelTitleKey"
#define kA2PImpStaticTextKey					kA2PImpStringPrefix	"kA2PImpStaticTextKey"
#define kA2PImpInternalPopupMenuNameKey			kA2PImpStringPrefix	"kA2PImpInternalPopupMenuNameKey"
#define kA2PImpTargetMenuPath kA2PImpInternalPopupMenuNameKey
#define kA2PImpSetOption1StringKey kA2PImpStringPrefix "kA2PImpSetOption1StringKey"
#define kA2PImpChangeOptionsCmdKey kA2PImpStringPrefix "kA2PImpChangeOptionsCmdKey"
#define kA2PImpIdleTaskKey						kA2PImpStringPrefix "kA2PImpIdleTaskKey"

//Mensajes de errores
#define kProcesoFinalizadoStringKey				kA2PImpStringPrefix "kProcesoFinalizadoStringKey"
#define kSeleccionarPaginaStringKey				kA2PImpStringPrefix "kSeleccionarPaginaStringKey"
#define kSobreDocActualStringKey				kA2PImpStringPrefix "kSobreDocActualStringKey"
#define kTamanoIncorrectoStringKey				kA2PImpStringPrefix "kTamanoIncorrectoStringKey"
#define kNumColumnasIncorrectoStringKey			kA2PImpStringPrefix "kNumColumnasIncorrectoStringKey"
#define kTamMargenIncorrectoStringKey			kA2PImpStringPrefix "kTamMargenIncorrectoStringKey"
#define kTamMedianilIncorrectoStringKey			kA2PImpStringPrefix "kTamMedianilIncorrectoStringKey"

#define kA2PImpDontDocumentfrontStringKey			kA2PImpStringPrefix "kA2PImpDontDocumentfrontStringKey"
#define kA2PImpNoExisteCajaUpdateStringKey			kA2PImpStringPrefix "kA2PImpNoExisteCajaUpdateStringKey"
#define kA2PImpActualizarAFinalizadoStringKey			kA2PImpStringPrefix "kA2PImpActualizarAFinalizadoStringKey"

#define kA2PImpNoSeEncontroLlaveStringKey			kA2PImpStringPrefix "kA2PImpNoSeEncontroLlaveStringKey"
#define kA2PImpLlaveIncorrectaStringKey			kA2PImpStringPrefix "kA2PImpLlaveIncorrectaStringKey"

#define kA2PImpCampoNombreStringKey				kA2PImpStringPrefix "kA2PImpCampoNombreStringKey"
#define kA2PImpCampoCoordenadasStringKey			kA2PImpStringPrefix "kA2PImpCampoCoordenadasStringKey"
#define kA2PImpCampoPathStringKey					kA2PImpStringPrefix "kA2PImpCampoPathStringKey"
#define kA2PImpPageOSectionStringKey				kA2PImpStringPrefix "kA2PImpPageOSectionStringKey"

#define kA2PImpComunicDistribuidorStringKey				kA2PImpStringPrefix "kA2PImpComunicDistribuidorStringKey"
#define kA2PImpCopiadeA2PStringKey				kA2PImpStringPrefix "kA2PImpCopiadeA2PStringKey"
#define kA2PImpErrorAlAbrirArchivoStringKey				kA2PImpStringPrefix "kA2PImpErrorAlAbrirArchivoStringKey"
#define kA2PImpVerificarPreferenciasStringKey				kA2PImpStringPrefix "kA2PImpVerificarPreferenciasStringKey"


#define kA2PImpStringLayerCmdSequenceKey				kA2PImpStringPrefix "kA2PImpStringLayerCmdSequenceKey"
#define kA2PImpStringNewLayerCmdSequenceKey			kA2PImpStringPrefix "kA2PImpStringNewLayerCmdSequenceKey"


#define kA2PImpSelPanelPreferenciasMenuItemKey		kA2PImpStringPrefix "kA2PImpSelPanelPreferenciasMenuItemKey"
#define kA2PImpSelPanelImportDialogMenuItemKey		kA2PImpStringPrefix "kA2PImpSelPanelImportDialogMenuItemKey"
#define kA2PImpSelPanelUpdateDialogMenuItemKey		kA2PImpStringPrefix "kA2PImpSelPanelUpdateDialogMenuItemKey"
#define kA2PImpStringAddSwatchCommandKey				kA2PImpStringPrefix "kA2PImpStringAddSwatchCommandKey"




#define kA2PImpAjusteImagenStringKey				kA2PImpStringPrefix "kA2PImpAjusteImagenStringKey"
#define kA2PImpAjusteContenidoAFrameStringKey		kA2PImpStringPrefix "kA2PImpAjusteContenidoAFrameStringKey"
#define kA2PImpAjusteCentrarStringKey				kA2PImpStringPrefix "kA2PImpAjusteCentrarStringKey"
#define kA2PImpAjusteProporcionalmenteStringKey	kA2PImpStringPrefix "kA2PImpAjusteProporcionalmenteStringKey"
#define kA2PImpNingunoStringKey					kA2PImpStringPrefix "kA2PImpNingunoStringKey"
#define kA2PImpLabelFechaStringKey				kA2PImpStringPrefix "kA2PImpLabelFechaStringKey"
#define kA2PImpLabelPaginaStringKey				kA2PImpStringPrefix "kA2PImpLabelPaginaStringKey"

#define kA2PImpLabelGetDateDialogStringKey				kA2PImpStringPrefix "kA2PImpLabelGetDateDialogStringKey"
#define kA2PImpMsgErrorAlertNoSeccionNodoStringKey		kA2PImpStringPrefix "kA2PImpMsgErrorAlertNoSeccionNodoStringKey"
#define kA2PImpErrorEnCoordenadasStringKey		kA2PImpStringPrefix "kA2PImpErrorEnCoordenadasStringKey"
#define kA2PImpA2PImplIdiomaStringKey		kA2PImpStringPrefix "kA2PImpA2PImplIdiomaStringKey"
#define kA2PImpA2PImpNoExistenPreferenciasStringKey		kA2PImpStringPrefix "kA2PImpA2PImpNoExistenPreferenciasStringKey"	
#define kA2PImpA2PImpNoExisteHojaEstiloStringKey		kA2PImpStringPrefix "kA2PImpA2PImpNoExisteHojaEstiloStringKey"				
#define kA2PImpA2PImpNoCapasSobrePrefStringKey		kA2PImpStringPrefix "kA2PImpNoCapasSobrePrefStringKeygKey"
#define kA2PImpNumberDePaginaAFluirStringKey		kA2PImpStringPrefix "kA2PImpNumberDePaginaAFluirStringKey"
#define kA2PImpNPagInvalidoStringKey		kA2PImpStringPrefix "kA2PImpNPagInvalidoStringKey"
#define kA2PImpNameDocumentImportedStringKey		kA2PImpStringPrefix "kA2PImpNameDocumentImportedStringKey"
#define kA2PImpNomPagInvalidoStringKey		kA2PImpStringPrefix "kA2PImpNomPagInvalidoStringKey"
#define kA2PImpNewPageFromPlantilleroMenuItemKey		kA2PImpStringPrefix "kA2PImpNewPageFromPlantilleroMenuItemKey"
#define kA2PImpInstallPnPlantilleroStringKey		kA2PImpStringPrefix "kA2PImpInstallPnPlantilleroStringKey"

#define kA2PInspectActualizacionesTaskKey		kA2PImpStringPrefix "kA2PInspectActualizacionesTaskKey"

// Menu item positions:
#define kA2PImpSetOptionsMenuItemPosition				1.0
#define kA2PImpSelPanelImportDialogMenuItemPosition		2.0
#define kA2PImpSelPanelUpdateDialogMenuItemPosition		3.0
#define kA2PImpNewPageFromPlantilleroMenuItemPosition	4.0
#define kA2PImpRefreshMenuItemPosition					5.0
#define kA2PImpSelPanelPreferenciasMenuItemPosition		6.0

#define	kA2PImpSeparator1MenuItemPosition			10.0
#define kA2PImpAboutThisMenuItemPosition			11.0

#define kA2PImpDialogTitleKey kA2PImpStringPrefix "kA2PImpDialogTitleKey"
// "Plug-ins" sub-menu item key for dialog:
#define kA2PImpDialogMenuItemKey kA2PImpStringPrefix "kA2PImpDialogMenuItemKey"

// See. the platform resource file where these are used!
#define kA2PImpEyeBallIconRsrcID			1510
#define kA2PImpLockPenIconRsrcID			1512
#define kA2PImpPenIconRsrcID				1516
#define kA2PImpLogoResourceID				128 //recurso del logo
#define kN2PsqlAlertActualizarNotasIconResourceID	129
#define kA2PImpIcoCalResourceID				150
#define kA2PImpCalPNGIconRsrcID				229
#define kA2PImpIconoDeAlertaRsrcID			1313



#define kA2PImpNodeWidgetRsrcID 	1200
#define kA2PImpDlgGetFechaYPag_PlantilleroResourceID			1520
#define kA2PImpDlgGetFechaYPag_ToA2P_ResourceID			1540

// Initial data format version numbers
#define kA2PImpFirstMajorFormatNumber  RezLong(1)
#define kA2PImpFirstMinorFormatNumber  RezLong(0)

// Data format version numbers for the PluginVersion resource 
#define kA2PImpCurrentMajorFormatNumber kA2PImpFirstMajorFormatNumber
#define kA2PImpCurrentMinorFormatNumber kA2PImpFirstMinorFormatNumber

//para debug de marco
#define MarcoDebug kFalse

//PMString Tprzr "";

/*// ClassIDs:
 DECLARE_PMID(kClassIDSpace, kA2PImpActionComponentBoss, kA2PImpPrefix + 0)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 3)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 4)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 5)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 6)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 7)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 8)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 9)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 10)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 11)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 12)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 13)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 14)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 15)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 16)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 17)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 18)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 19)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 20)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 21)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 22)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 23)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 24)
 //DECLARE_PMID(kClassIDSpace, kA2PImpBoss, kA2PImpPrefix + 25)
 
 
 // InterfaceIDs:
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 0)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 1)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 2)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 3)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 4)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 5)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 6)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 7)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 8)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 9)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 10)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 11)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 12)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 13)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 14)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 15)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 16)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 17)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 18)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 19)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 20)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 21)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 22)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 23)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 24)
 //DECLARE_PMID(kInterfaceIDSpace, IID_IA2PIMPINTERFACE, kA2PImpPrefix + 25)
 
 
 // ImplementationIDs:
 DECLARE_PMID(kImplementationIDSpace, kA2PImpActionComponentImpl, kA2PImpPrefix + 0 )
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 1)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 2)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 3)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 4)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 5)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 6)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 7)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 8)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 9)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 10)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 11)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 12)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 13)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 14)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 15)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 16)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 17)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 18)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 19)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 20)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 21)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 22)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 23)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 24)
 //DECLARE_PMID(kImplementationIDSpace, kA2PImpImpl, kA2PImpPrefix + 25)
 
 
 // ActionIDs:
 DECLARE_PMID(kActionIDSpace, kA2PImpAboutActionID, kA2PImpPrefix + 0)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 5)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 6)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 7)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 8)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 9)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 10)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 11)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 12)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 13)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 14)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 15)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 16)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 17)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 18)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 19)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 20)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 21)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 22)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 23)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 24)
 //DECLARE_PMID(kActionIDSpace, kA2PImpActionID, kA2PImpPrefix + 25)
 
 
 // WidgetIDs:
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 2)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 3)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 4)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 5)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 6)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 7)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 8)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 9)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 10)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 11)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 12)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 13)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 14)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 15)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 16)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 17)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 18)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 19)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 20)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 21)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 22)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 23)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 24)
 //DECLARE_PMID(kWidgetIDSpace, kA2PImpWidgetID, kA2PImpPrefix + 25)
 
 
 // "About Plug-ins" sub-menu:
 #define kA2PImpAboutMenuKey			kA2PImpStringPrefix "kA2PImpAboutMenuKey"
 #define kA2PImpAboutMenuPath		kSDKDefStandardAboutMenuPath kA2PImpCompanyKey
 
 // "Plug-ins" sub-menu:
 #define kA2PImpPluginsMenuKey 		kA2PImpStringPrefix "kA2PImpPluginsMenuKey"
 #define kA2PImpPluginsMenuPath		kSDKDefPlugInsStandardMenuPath kA2PImpCompanyKey kSDKDefDelimitMenuPath kA2PImpPluginsMenuKey
 
 // Menu item keys:
 
 // Other StringKeys:
 #define kA2PImpAboutBoxStringKey	kA2PImpStringPrefix "kA2PImpAboutBoxStringKey"
 #define kA2PImpTargetMenuPath kA2PImpPluginsMenuPath
 
 // Menu item positions:
 
 
 // Initial data format version numbers
 #define kA2PImpFirstMajorFormatNumber  RezLong(1)
 #define kA2PImpFirstMinorFormatNumber  RezLong(0)
 
 // Data format version numbers for the PluginVersion resource 
 #define kA2PImpCurrentMajorFormatNumber kA2PImpFirstMajorFormatNumber
 #define kA2PImpCurrentMinorFormatNumber kA2PImpFirstMinorFormatNumber
 */
#endif // __A2PImpID_h__

//  Code generated by DollyXs code generator
