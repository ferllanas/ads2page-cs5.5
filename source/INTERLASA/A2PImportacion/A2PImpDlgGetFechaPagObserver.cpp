/*
//	File:	A2PImpDlgGetFechaPagObserver.cpp
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Interlasa S.A. Todos los derechos reservados.
//	
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ISubject.h"
#include "ITextControlData.h"
#include "IControlView.h"
#include "IStringListControlData.h"
#include "IDropDownListController.h"
#include "IDialogController.h"

// General includes:
#include "CDialogObserver.h"
#include "SDKUtilities.h"
#include "SystemUtils.h"
#include "CAlert.h"

// Project includes:
#include "A2PImpID.h"





/** A2PImpDlgGetFechaPagObserver
	Allows dynamic processing of dialog widget changes, in this case
	the dialog's info button. 
  
	Implements IObserver based on the partial implementation CDialogObserver. 
	@author Juan Fernando Llanas Rdz
*/
class A2PImpDlgGetFechaPagObserver : public CDialogObserver
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		A2PImpDlgGetFechaPagObserver(IPMUnknown* boss) : CDialogObserver(boss) {}

		/** Destructor. */
		virtual ~A2PImpDlgGetFechaPagObserver() {}

		/** 
			Called by the application to allow the observer to attach to the subjects 
			to be observed, in this case the dialog's info button widget. If you want 
			to observe other widgets on the dialog you could add them here. 
		*/
		virtual void AutoAttach();

		/** Called by the application to allow the observer to detach from the subjects being observed. */
		virtual void AutoDetach();

		/**
			Called by the host when the observed object changes, in this case when
			the dialog's info button is clicked.
			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
		
	private:
		
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(A2PImpDlgGetFechaPagObserver, kA2PImpDlgGetFechaPagObserverImpl)

/* AutoAttach
*/
void A2PImpDlgGetFechaPagObserver::AutoAttach()
{
	// Call the base class AutoAttach() function so that default behavior
	// will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoAttach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("A2PImpDlgGetFechaPagObserver::AutoAttach() panelControlData invalid");
			break;
		}
		
		// Now attach to N2PCheckInOut's info button widget.
		//AttachToWidget(kPlanGenPublicComboBoxWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		
		
		
		// Attach to other widgets you want to handle dynamically here.

	} while (false);
}

/* AutoDetach
*/
void A2PImpDlgGetFechaPagObserver::AutoDetach()
{
	// Call base class AutoDetach() so that default behavior will occur (OK and Cancel buttons, etc.).
	CDialogObserver::AutoDetach();

	do
	{
		// Get the IPanelControlData interface for the dialog:
		InterfacePtr<IPanelControlData> panelControlData(this, UseDefaultIID());
		if (panelControlData == nil)
		{
			ASSERT_FAIL("A2PImpDlgGetFechaPagObserver::AutoDetach() panelControlData invalid");
			break;
		}
		
		// Now we detach from N2PCheckInOut's info button widget.
		//DetachFromWidget(kCheckInOutIconSuiteWidgetID, IID_ITRISTATECONTROLDATA, panelControlData);
		//DetachFromWidget(kPlanGenPublicComboBoxWidgetID, IID_ISTRINGLISTCONTROLDATA, panelControlData);
		
		// Detach from other widgets you handle dynamically here.
		
	} while (false);
}

/* Update
*/
void A2PImpDlgGetFechaPagObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID &protocol, 
	void* changedBy
)
{
	// Call the base class Update function so that default behavior will still occur (OK and Cancel buttons, etc.).
	CDialogObserver::Update(theChange, theSubject, protocol, changedBy);

	do
	{
		InterfacePtr<IControlView> controlView(theSubject, UseDefaultIID());
		if (controlView == nil)
		{
			ASSERT_FAIL("A2PImpDlgGetFechaPagObserver::Update() controlView invalid");
			break;
		}

		// Get the button ID from the view.
		WidgetID theSelectedWidget = controlView->GetWidgetID();
		
		
		/*if (theSelectedWidget == kPlanGenPublicComboBoxWidgetID && theChange == kPopupChangeStateMessage)
		{
			
			// Bring up the About box.
			this->LLenar_Combo_Seccion();
		}*/

	} while (false);
}






//  Generated by Dolly build 17: template "Dialog".
// End, A2PImpDlgGetFechaPagObserver.cpp.


