//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvCustomView.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IBoolData.h"
#include "IFormatImageStreamData.h"
#include "IGraphicsPort.h"
#include "IImageAttributes.h"
#include "IImageFormatManager.h"
#include "IImageReadFormat.h"
#include "IImageReadFormatInfo.h"
#include "IImageStream.h"
#include "IImageStreamManager.h"
#include "IPMStream.h"
#include "ISession.h"
#include "ISysFileData.h"
#include "IDFile.h"
#include "IWorkspace.h"
#include "ICoreFilename.h"

#include "IImportPreview.h"
#include "ILayoutUIUtils.h"
#include "IImportProvider.h"
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"


// General includes:
#include "AGMGraphicsContext.h"
#include "CreateObject.h"
#include "ImageID.h"
#include "ImageTypes.h"
#include "StreamUtil.h"
#include "SysControlIds.h"
#include "GraphicsExternal.h"	// _t_AGM...
#include "CControlView.h"
#include "CAlert.h"
#include "FileUtils.h"

// Project includes:
#include "A2PImpID.h"

  
/** Color space family from AGM, subsetted to meet our needs here.
	@ingroup paneltreeview
*/
enum SubsettedAGMColorSpaceFamily
{
	/** */
	kDontCare,
	/** */
	kAGMCsRGB
	/** */
};


/** Implements IControlView; provides custom control view for a widget.

	@author Ian Paterson
	@ingroup paneltreeview
*/

class PnlTrvCustomView : public CControlView
{
public:
	/** Constructor
	@param boss interface on boss object on which interface is being aggregated
	*/
	PnlTrvCustomView(IPMUnknown* boss);

	/** Destructor
	*/
	virtual ~PnlTrvCustomView();	

	/** Called when widget is being initialised.
	@param widgetId [IN] specifies WidgetID to associate with this widget
	@param frame [IN] specifies bounding box for the control
	@param rsrcID [IN] specifies resource to associate with this widget
	*/
	virtual void Init(
		const WidgetID& widgetId, const PMRect& frame, RsrcID rsrcID);

	/** Called when widget should draw.
	@param viewPort [IN] specifies viewport
	@param updateRgn [IN] specifies update region
	*/
	virtual void Draw(IViewPort* viewPort, SysRgn updateRgn);

private:
	void deleteBuffers();
	ErrorCode createPreview(
		const IDFile& previewFile, uint32 nWidthWidget, uint32 nHeightWidget, uint8 backGrey);

	AGMImageRecord *fpCurAGMImage;
	IDFile fCurImageSysFile;
};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PERSIST_PMINTERFACE(PnlTrvCustomView, kA2PImpTrvCustomViewImpl)


/* Constructor
*/
PnlTrvCustomView::PnlTrvCustomView(IPMUnknown* boss)
    : CControlView(boss),
      fpCurAGMImage(nil)
{
}


/* Destructor
*/
PnlTrvCustomView::~PnlTrvCustomView()
{

    deleteBuffers();
    
}

void PnlTrvCustomView::deleteBuffers()
{

	if(fpCurAGMImage) {
		delete (uint8 *)fpCurAGMImage->baseAddr;
		delete fpCurAGMImage;
		fpCurAGMImage = nil;
	}

	
}

/* Init
*/
void PnlTrvCustomView::Init(
	const WidgetID& widgetId, const PMRect& frame, RsrcID rsrcID)
{

	CControlView::Init(widgetId, frame, rsrcID);
	do {
		InterfacePtr<IBoolData> iIsFileValid(this, UseDefaultIID());
		ASSERT(iIsFileValid);
		if(!iIsFileValid) {
			break;
		}
		iIsFileValid->Set(kFalse);
	} while(kFalse);
}


/* Draw
*/
void PnlTrvCustomView::Draw(IViewPort* viewPort, SysRgn updateRgn)
{
	

	AGMGraphicsContext gc(viewPort, this, updateRgn);
	InterfacePtr<IGraphicsPort>
		gPort(gc.GetViewPort(), UseDefaultIID());	// IID_IGRAPHICSPORT);
	ASSERT(gPort);
	if(!gPort) {
		return;
	}

	gPort->gsave();

	do {
		ErrorCode resultOfCreatingPreview = kFailure;

		InterfacePtr<IInterfaceColors>
			iInterfaceColors(GetExecutionContextSession(), IID_IINTERFACECOLORS);
		ASSERT(iInterfaceColors);
		if(!iInterfaceColors){
			break;
		}
		RealAGMColor whiteFill;
		iInterfaceColors->GetRealAGMColor(kInterfaceWhite, whiteFill);
		RealAGMColor defaultGreyFill;
		iInterfaceColors->GetRealAGMColor(kInterfacePaletteFill, defaultGreyFill); 
		
		PMRect frame = GetFrame();
		frame.MoveTo(0, 0);             // get into local coordinates

		uint32 nWidthWidget=1;
		uint32 nHeightWidget=1;
	

		// Dropped the 3d border effect
		nWidthWidget = ToInt32(frame.Width());  
		nHeightWidget = ToInt32(frame.Height());
		// The bool-data state tells this code whether the image is valid 
		// to display (or attempt to display)
		InterfacePtr<IBoolData> iIsFileValid(this, UseDefaultIID());
		ASSERT(iIsFileValid);
		if(!iIsFileValid) {
			break;
		}
		// get filespec currently associated with widget
		InterfacePtr<ISysFileData> iImageSysFile(this, IID_ISYSFILEDATA);
		ASSERT(iImageSysFile);
		if(!iImageSysFile)
		{
			break;
		}
		IDFile previewFile = iImageSysFile->GetSysFile(); 
		if(iIsFileValid->GetBool()) {
			if(fCurImageSysFile != previewFile) {
				// Take average greyvalue of what we're filling background to
				uint8 backgroundGrey = ToInt32(Round(
					255.0* (whiteFill.red +  whiteFill.green + whiteFill.blue)/3.0));
				// Create another preview, if we need to
				resultOfCreatingPreview = this->createPreview(previewFile, nWidthWidget, nHeightWidget, backgroundGrey);
				
			} else {
				// That's OK- assume safe to use the old one
				resultOfCreatingPreview = kSuccess;
			}
		}
	
		// if we got an image, then display it
		// otherwise, draw a red diagonal line through the widget
		if (resultOfCreatingPreview == kSuccess) {
			// Fill the background with white
			gPort->setrgbcolor(whiteFill.red, whiteFill.green, whiteFill.blue);
			gPort->rectpath(frame);
			gPort->fill();
			PMReal imageWidth = (fpCurAGMImage->bounds.xMax - fpCurAGMImage->bounds.xMin);
			PMReal imageHeight = (fpCurAGMImage->bounds.yMax - fpCurAGMImage->bounds.yMin);
			PMReal xOffset = frame.GetHCenter() - imageWidth/2;
			PMReal yOffset = frame.GetVCenter() - imageHeight/2;
			// Centered
			gPort->translate(xOffset, yOffset);
			PMMatrix theMatrix;	// No transform 
			ASSERT(fpCurAGMImage);
			gPort->image(fpCurAGMImage, theMatrix, 0);
		}
		else {
			// Fill with the palette default background fill
			gPort->setrgbcolor(defaultGreyFill.red, defaultGreyFill.green, defaultGreyFill.blue);
			gPort->rectpath(frame);
			gPort->fill();
			// Render a red stroke over the background
			gPort->setrgbcolor(PMReal(1), PMReal(0), PMReal(0));
			gPort->setlinewidth(PMReal(2));
			gPort->setmiterlimit(PMReal(2));  // SPAM: what units does this use?
			gPort->moveto(frame.Left()+2, frame.Bottom()-2);
			gPort->lineto(frame.Right()-2, frame.Top()+2);
			gPort->stroke();
		}	
	} while(kFalse);

	gPort->grestore();
}
/*
*/
ErrorCode PnlTrvCustomView::createPreview(
	const IDFile& previewFile, 
	uint32 nWidthWidget, 
	uint32 nHeightWidget, 
	uint8 backGrey
	)
{

	do
	{
		// get source stream (image file to preview)
		InterfacePtr<IPMStream>
			fileStream(StreamUtil::CreateFileStreamRead(previewFile));

		if(fileStream == nil) {
			break;
		}
		InterfacePtr<IK2ServiceRegistry>	serviceRegistry(GetExecutionContextSession(), UseDefaultIID());
		ASSERT(serviceRegistry);
		if(!serviceRegistry) {
			break;
		}
		int	numHandlers = serviceRegistry->GetServiceProviderCount(kImportProviderService);
		for (int i = 0; i < numHandlers; i++) {

			InterfacePtr<IK2ServiceProvider> provider(
				serviceRegistry->QueryNthServiceProvider(kImportProviderService, i));
			InterfacePtr<IImportProvider> importProvider(provider, IID_IIMPORTPROVIDER);
			
			if (importProvider && 
				importProvider->CanImportThisStream(fileStream) == IImportProvider::kFullImport) {
			
				InterfacePtr<IImportPreview> preview(importProvider, IID_IIMPORTPREVIEW);
				if(preview) {
					// Trash any existing storage
					this->deleteBuffers();
					
					fpCurAGMImage = new AGMImageRecord;
					memset (fpCurAGMImage, 0, sizeof(AGMImageRecord));

					//create the preview		
					int32 width = nWidthWidget;
					int32 height = nHeightWidget;
					fpCurAGMImage->bounds.xMin 			= 0;
					fpCurAGMImage->bounds.yMin 			= 0;
					fpCurAGMImage->bounds.xMax 			= width;
					fpCurAGMImage->bounds.yMax 			= height;
					fpCurAGMImage->byteWidth 			= BYTES2ROWBYTES(3*width);
					fpCurAGMImage->colorSpace 			= kAGMCsRGB;
					fpCurAGMImage->bitsPerPixel 		= 24;
					fpCurAGMImage->decodeArray 			= 0;
					fpCurAGMImage->colorTab.numColors 	= 0;
					fpCurAGMImage->colorTab.theColors 	= nil;

					uint32 bufSizeInBytes = (fpCurAGMImage->byteWidth) * height;
					fpCurAGMImage->baseAddr = 
						new uint8[bufSizeInBytes];
					//set the background to be grey
					::memset(fpCurAGMImage->baseAddr, backGrey,bufSizeInBytes);
			
					if (fpCurAGMImage->baseAddr) {
						
						preview->Create24bitRGBPreview( (uint8*)fpCurAGMImage->baseAddr,
							width, height, fileStream, kTrue );
						fCurImageSysFile = previewFile;
						return kSuccess;
					}
				}
			}
			// Be sure to reset the stream
			fileStream->Seek(0,kSeekFromStart);
		} // i loop ove handlers

		

	} while(false);
	
	return kFailure;
}

//	end, File:	PnlTrvCustomView.cpp
