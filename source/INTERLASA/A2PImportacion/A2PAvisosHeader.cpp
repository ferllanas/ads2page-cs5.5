/*
 *  A2PAvisosHeader.cpp
 *  A2PImp
 *
 *  Created by Desarrollo2 on 4/8/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#include "VCPlugInHeaders.h"

#include "CAlert.h"
#include "FileUtils.h"

#include "A2PImpID.h"
#include "A2PAvisosHeader.h"
#include "A2PImpStructAvisos.h"

#ifdef MACINTOSH
	#include "InterlasaUtilities.h"
	#include "IA2PPreferenciasFunciones.h"
#endif
#ifdef WINDOWS
	#include "..\Interlasa_common\InterlasaUtilities.h"
	#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif

StructAvisos AvisosCargados[1000];
int32 cantidadAvisos;
PMString RutaArchivoDAvisos;

class A2PAvisos : public CPMUnknown<IA2PAvisos>
{
public:
	/** Constructor.
		@param boss boss object on which this interface is aggregated.
	 */
	A2PAvisos (IPMUnknown *boss);

	/** 
			Destructor.
	*/
	virtual ~A2PAvisos(void);
	
	/**
	*/
	bool16 CargarAvisos(PMString Path,bool16 RefreshAvisos);
	
	int32 CantidadDAvisos();
	
	PMString ArchivoDAvisos();
	
	bool16 CopiasAvisosCargados(StructAvisos Avisos[]);

private:
	bool16 LeerAvisosDesdeArchivo(PMString Path);

	

};


/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its
 ImplementationID making the C++ code callable by the
 application.
*/
CREATE_PMINTERFACE(A2PAvisos, kA2PImpAvisosImpl)

/* A2PFluirFunction Constructor
*/
A2PAvisos::A2PAvisos(IPMUnknown* boss) :CPMUnknown<IA2PAvisos>(boss)
{
}

/* A2PFluirFunction Destructor
*/
A2PAvisos::~A2PAvisos(void)
{
}



bool16 A2PAvisos::CargarAvisos(PMString Path,bool16 RefreshAvisos)// archivo pasa por aqui
{
	//CAlert::InformationAlert("Aviso PatZZZ: "+ Path);
	if(!this->LeerAvisosDesdeArchivo(Path))//luego el archivo pasa por aca (sigue siendo string)
		return kFalse;
	/*if(RefreshAvisos==kTrue)
	{//Carga de nuevo los Avisos desde el archivo
		CAlert::InformationAlert("Aviso PatsXXX: "+ Path);
		this->LeerAvisosDesdeArchivo(Path);
			
	}
	else
	{	
		if(cantidadAvisos<0)
		{//si no existen avisos cargados
			CAlert::InformationAlert("Aviso PatZZZ: "+ Path);
			this->LeerAvisosDesdeArchivo(Path);
			//	this->CopiasAvisosCargados();
		}
		else
		{//si existen avisos cargados
	
		}
	}	*/
	
	if(MarcoDebug == kTrue){CAlert::InformationAlert("archivo - luego el archivo pasa por aca (sigue siendo string)");}
	return kTrue;
}


bool16 A2PAvisos::LeerAvisosDesdeArchivo(PMString Path)
{
	if(MarcoDebug == kTrue){CAlert::InformationAlert("LeerAvisosDesdeArchivo(PMString Path)");}
	FILE *stream;
	int32 numLinea=0;
	int32 linecount=1;

	
	PMString salto("\n");
	//salto.Append(10);
	RutaArchivoDAvisos = Path;//InterlasaUtilities::MacToUnix(Path); //despues pasa por aca el archivo y se le asigna a el otro string RutaArchivoDAvisos
	
	if(MarcoDebug == kTrue){CAlert::InformationAlert("despues pasa por aca el archivo y se le asigna a el otro string RutaArchivoDAvisos");}
	
	Preferencias Pref;
	
	InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
	(
		kA2PPreferenciasFuncionesBoss,	// Object boss/class
		IA2PPreferenciasFunctions::kDefaultIID
	)));
		
	if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Pref,kFalse)==kFalse)
	{
		CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
		return(kFalse);
	}
		
	//FileUtils::NormalizeUnComposedUnicodeChars(RutaArchivoDAvisos);
	
	#if defined(MACINTOSH)
		if( (stream  =  FileUtils::OpenFile(RutaArchivoDAvisos.GrabCString(),"r"))==NULL) // el archivo pasa por aca y se abre el stream (ya es archivo)
	#elif defined(WINDOWS)
		if( (stream  =  fopen(RutaArchivoDAvisos.GrabCString(),"r"))==NULL)
	#endif
	//fopen(RutaArchivoDAvisos.GrabCString(),"r"))==NULL)//
	{	
		CAlert::InformationAlert(kA2PImpErrorAlAbrirArchivoStringKey);
		
		return kFalse;
	}
	else
	{
	if(MarcoDebug == kTrue){CAlert::InformationAlert("el archivo pasa por aca y se abre el stream ya es archivo");}
		PMString ArchivoFul="";
		PlatformChar Caracter;		//caracter leido
		PMString myS="";
		do
		{		
			Caracter.SetFromUnicode(fgetc(stream));//obtiene siguente caracter de
			
			PMString ASD="";
			ASD.Append(Caracter);
			ASD.Append(",");
			ASD.Append(Caracter.GetAsHexString()->GrabCString());
			//CAlert::InformationAlert(ASD);
			//->GetAsNumber()
			switch (Caracter.GetAsOneByteChar()) {
				case 0x87:
					//CAlert::InformationAlert("a");
					ArchivoFul.Append("a");
					break;
				case 0x8e:
					ArchivoFul.Append("e");
					break;
				case 0x92:
					ArchivoFul.Append("i");
					break;
				case 0x97:
					ArchivoFul.Append("o");
					break;
				case 0x9c:
					ArchivoFul.Append("u");
					break;
				case 0xe7:
					ArchivoFul.Append("A");
					break;
				case 0x83:
					ArchivoFul.Append("E");
					break;
				case 0xea:
					ArchivoFul.Append("I");
					break;
				case 0xee:
					ArchivoFul.Append("O");
					break;
				case 0xf2:
					ArchivoFul.Append("U");
					break;
				case 0x96:
					ArchivoFul.Append("n");
					break;
				case 0x84:
					ArchivoFul.Append("N");
					break;
				default:
					ArchivoFul.Append(Caracter);
					break;
			}
			
			//adiciona el caracter a la cadena aviso
			myS="";
			myS.Append(Caracter);
			if(myS==salto)
				linecount++;			
		}while(!feof(stream));//Hacer mientras que no sea fin de linea(10) retorno de carro(13) o fin de archivo(EOF)
		ArchivoFul.Remove(ArchivoFul.NumUTF16TextChars()-1, 1);
		ArchivoFul.Remove(ArchivoFul.NumUTF16TextChars()-1, 1);
		fclose(stream);	
		//linecount++;
		ArchivoFul.Append("\n");
		
		
		//CAlert::InformationAlert("MAMA:"+ArchivoFul);
		////*Acontinuacion se inicia el llenado de la estructura avisos*////
		numLinea=1;	//Numero de linea se inicializa en uno
		PMString *LineaAviso;
		
		PMString TokenXLinea="";
		TokenXLinea.Append("\n");
		
		//char* str1 = (char *)ArchivoFul.GetItem(TokenXLinea,numLinea)->GetPlatformString().c_str();
		//CAlert::InformationAlert(str1);
		LineaAviso = ArchivoFul.GetItem(TokenXLinea,numLinea);//(PMString *)str1;//char* str1 = (char *)str.GetPlatformString().c_str();
		
		if(LineaAviso->NumUTF16TextChars()<=0 || LineaAviso==nil)
		{
			//CAlert::InformationAlert("XXX1");
			TokenXLinea="";
			TokenXLinea.Append(10);
			LineaAviso = ArchivoFul.GetItem(TokenXLinea,numLinea);
			
			if(LineaAviso->NumUTF16TextChars()<0 || LineaAviso==nil)
			{	
				//CAlert::InformationAlert("XXX2");
				TokenXLinea="";
				TokenXLinea.AppendW(13);  //
				LineaAviso = ArchivoFul.GetItem(TokenXLinea,numLinea);
				if(LineaAviso->NumUTF16TextChars()<0 || LineaAviso==nil)
				{
					return(kFalse);
				}
				//CAlert::InformationAlert("XXX3");
			}
		}
		else
		{
			/*PMString CantidadDar="CadA=";
			CantidadDar.AppendNumber(ArchivoFul.NumUTF16TextChars());
			CantidadDar.Append("\nCadB");
			CantidadDar.AppendNumber(LineaAviso->NumUTF16TextChars());*/
			//CAlert::InformationAlert("ZZZZ1");
			if(ArchivoFul.NumUTF16TextChars()==LineaAviso->NumUTF16TextChars())
			{//Quiere decir o que ess una sola linea o que es un archivo con 0xd
				
				//CAlert::InformationAlert("ZZZZ2");
				if(ArchivoFul.IndexOfWChar(13)<ArchivoFul.NumUTF16TextChars() || ArchivoFul.IndexOfWChar(10)<ArchivoFul.NumUTF16TextChars())
				{//Quiere decir que es un archivo del tipo 0xd
					
					TokenXLinea="";
					TokenXLinea.Append(10);  //
					LineaAviso = ArchivoFul.GetItem(TokenXLinea,numLinea);
					//CAlert::InformationAlert("ZZZZ3");
					if( ArchivoFul.NumUTF16TextChars()==LineaAviso->NumUTF16TextChars() && (ArchivoFul.IndexOfWChar(13)<ArchivoFul.NumUTF16TextChars() || ArchivoFul.IndexOfWChar(10)<ArchivoFul.NumUTF16TextChars()))
					{
						
							//CAlert::InformationAlert("ZZZZ4");
							TokenXLinea="";
							TokenXLinea.AppendW(13);  //
							LineaAviso = ArchivoFul.GetItem(TokenXLinea,numLinea);
							if(LineaAviso->NumUTF16TextChars()<0 || LineaAviso==nil)
							{
								return(kFalse);
							}
							//CAlert::InformationAlert("ZZZZ5");
						
					}
				}
			}
		}
			
		PMString TokenX="";
		TokenX.AppendW(Pref.SepEnCampos.GetAsNumber());  //
		PMString *ResultToken;
	
			
		while(LineaAviso->NumUTF16TextChars()>0 && LineaAviso!=nil && numLinea <= linecount)//Hacer mientras el numero de linea sea menor al tamaÒo de avisos o numero de avisos
		{
			PMString ASQA="";
			ASQA.AppendNumber(numLinea);
			ASQA.Append(", ");
			ASQA.AppendNumber(linecount);
			
			//CAlert::InformationAlert("Datos de aviso "+ASQA+" : "+PMString(LineaAviso->GrabCString()));

			//CadenaAviso=LineaAviso;//hace una copia de la cadena aviso
			//salir=0;//bandera a 0
			//numcampoLeido=1;//numero de campo inicializa en 1
			
		//	CAlert::InformationAlert("A1");
			ResultToken = LineaAviso->GetItem( TokenX, Pref.PosCampos1 );
			
			if(ResultToken->NumUTF16TextChars()<=0)
				break;
			
			//PMString CVD="PosCampos1: ";
			//CVD.AppendNumber(Pref.PosCampos1);
			//CAlert::InformationAlert(CVD);
				
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				//CAlert::InformationAlert("ASZ");
				AvisosCargados[numLinea-1].NomCampo1=Pref.NomCampos1;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo1=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			else
			{
				//CAlert::InformationAlert("debe salir");
				break;
			}
			
			//CAlert::InformationAlert("A2");
			//CAlert::InformationAlert("NomCampo1 "+AvisosCargados[numLinea-1].NomCampo1);
			//CAlert::InformationAlert("ContenidoCampo1 "+AvisosCargados[numLinea-1].ContenidoCampo1);
			
			ResultToken = LineaAviso->GetItem(TokenX, Pref.PosCampos2);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo2=Pref.NomCampos2;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo2=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos3);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo3=Pref.NomCampos3;//se obtiene el nombre con que debe guardarse el campo
				
				//CAlert::InformationAlert(ResultToken->GrabCString());
				
				AvisosCargados[numLinea-1].ContenidoCampo3=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
		
			}
				
				
			/*	CVD="PosCampos4: ";
				CVD.AppendNumber(Pref.PosCampos4);
				CAlert::InformationAlert(CVD);
			*/	
			//CAlert::InformationAlert("A4");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos4);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo4=Pref.NomCampos4;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo4=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			/*	CAlert::InformationAlert("NomCampo4 "+AvisosCargados[numLinea-1].NomCampo4);
				CAlert::InformationAlert("ContenidoCampo4 "+AvisosCargados[numLinea-1].ContenidoCampo4);
			*/	
			
			//CAlert::InformationAlert("A5");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos5);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo5=Pref.NomCampos5;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo5=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			
			//CAlert::InformationAlert("A6");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos6);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo6=Pref.NomCampos6;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo6=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			
			//CAlert::InformationAlert("A7");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos7);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo7=Pref.NomCampos7;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo7=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			
			//CAlert::InformationAlert("A8");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos8);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo8=Pref.NomCampos8;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo8=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			
			//CAlert::InformationAlert("A9");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos9);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo9=Pref.NomCampos9;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo9=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
				
			//CAlert::InformationAlert("A10");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos10);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo10=Pref.NomCampos10;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo10=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			
			//CAlert::InformationAlert("A11");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos11);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo11=Pref.NomCampos11;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo11=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			
			//CAlert::InformationAlert("A12");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos12);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo12=Pref.NomCampos12;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo12=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			
			//CAlert::InformationAlert("A13");	
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos13);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo13=Pref.NomCampos13;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo13=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
				
			//CAlert::InformationAlert("A14");
			ResultToken = LineaAviso->GetItem(TokenX,Pref.PosCampos14);
			if(ResultToken!=nil && ResultToken->NumUTF16TextChars())
			{
				AvisosCargados[numLinea-1].NomCampo14=Pref.NomCampos14;//se obtiene el nombre con que debe guardarse el campo
				AvisosCargados[numLinea-1].ContenidoCampo14=ResultToken->GrabCString();//se almacena en contenido la cadena encontrada
			}
			
			
			
		//	CAlert::InformationAlert("A15");
			numLinea++;//siguiente numLinea
			ASQA="";
			ASQA.AppendNumber(numLinea);
			//CAlert::InformationAlert(ASQA);
			
			LineaAviso = ArchivoFul.GetItem(TokenXLinea,numLinea);
		//	CAlert::InformationAlert("A16");
			if(LineaAviso==nil || LineaAviso->NumUTF16TextChars()<=0 || numLinea > linecount)
			{
				//CAlert::InformationAlert("Salio A chingar a su mauser");
				break;
			}
			
			//CAlert::InformationAlert("Datos de aviso "+ASQA+" : "+PMString(LineaAviso->GrabCString()));
		}//end while(numLinea<=NumAvisos)
		cantidadAvisos=numLinea-1;
	}
	
	return kTrue;

}

bool16 A2PAvisos::CopiasAvisosCargados(StructAvisos Avisos[])
{
	for(int32 i=0;i<cantidadAvisos;i++)
	{//Copia los avisos ya existentes
		
		Avisos[i].NomCampo1=AvisosCargados[i].NomCampo1;
		Avisos[i].ContenidoCampo1=AvisosCargados[i].ContenidoCampo1;
		Avisos[i].NomCampo2=AvisosCargados[i].NomCampo2;
		Avisos[i].ContenidoCampo2=AvisosCargados[i].ContenidoCampo2;
		Avisos[i].NomCampo3=AvisosCargados[i].NomCampo3;
		Avisos[i].ContenidoCampo3=AvisosCargados[i].ContenidoCampo3;
		Avisos[i].NomCampo4=AvisosCargados[i].NomCampo4;
		Avisos[i].ContenidoCampo4=AvisosCargados[i].ContenidoCampo4;
		Avisos[i].NomCampo5=AvisosCargados[i].NomCampo5;
		Avisos[i].ContenidoCampo5=AvisosCargados[i].ContenidoCampo5;
		Avisos[i].NomCampo6=AvisosCargados[i].NomCampo6;
		Avisos[i].ContenidoCampo6=AvisosCargados[i].ContenidoCampo6;
		Avisos[i].NomCampo7=AvisosCargados[i].NomCampo7;
		Avisos[i].ContenidoCampo7=AvisosCargados[i].ContenidoCampo7;
		Avisos[i].NomCampo8=AvisosCargados[i].NomCampo8;
		Avisos[i].ContenidoCampo8=AvisosCargados[i].ContenidoCampo8;
		Avisos[i].NomCampo9=AvisosCargados[i].NomCampo9;
		Avisos[i].ContenidoCampo9=AvisosCargados[i].ContenidoCampo9;
		Avisos[i].NomCampo10=AvisosCargados[i].NomCampo10;
		Avisos[i].ContenidoCampo10=AvisosCargados[i].ContenidoCampo10;
		Avisos[i].NomCampo11=AvisosCargados[i].NomCampo11;
		Avisos[i].ContenidoCampo11=AvisosCargados[i].ContenidoCampo11;
		Avisos[i].NomCampo12=AvisosCargados[i].NomCampo12;
		Avisos[i].ContenidoCampo12=AvisosCargados[i].ContenidoCampo12;
		Avisos[i].NomCampo13=AvisosCargados[i].NomCampo13;
		Avisos[i].ContenidoCampo13=AvisosCargados[i].ContenidoCampo13;
		Avisos[i].NomCampo14=AvisosCargados[i].NomCampo14;
		Avisos[i].ContenidoCampo14=AvisosCargados[i].ContenidoCampo14;
				
		Avisos[i].NumPagFlowed=AvisosCargados[i].NumPagFlowed;
		Avisos[i].UIDPageFlowed=AvisosCargados[i].UIDPageFlowed;
		Avisos[i].UIDFrameToImage=AvisosCargados[i].UIDFrameToImage;
		Avisos[i].UIDFrameEtiqueta=AvisosCargados[i].UIDFrameEtiqueta;
		Avisos[i].PathToImage=AvisosCargados[i].PathToImage;
		Avisos[i].DLImtedImage=AvisosCargados[i].DLImtedImage;
		Avisos[i].ExistImageFile=AvisosCargados[i].ExistImageFile;
		Avisos[i].LFrameImage=AvisosCargados[i].LFrameImage;
		Avisos[i].TFrameImage=AvisosCargados[i].TFrameImage;
		Avisos[i].RFrameImage=AvisosCargados[i].RFrameImage;
		Avisos[i].BFrameImage=AvisosCargados[i].BFrameImage;
		Avisos[i].LastTimeImage=AvisosCargados[i].LastTimeImage;
	}
	
	return kTrue;
}



int32 A2PAvisos::CantidadDAvisos()
{
	return cantidadAvisos;
}
	
PMString A2PAvisos::ArchivoDAvisos()
{
	return RutaArchivoDAvisos;
}