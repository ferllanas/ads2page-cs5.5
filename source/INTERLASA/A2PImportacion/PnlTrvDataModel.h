//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvDataModel.h $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#ifndef __PnlTrvDataModel_H_DEFINED__
#define __PnlTrvDataModel_H_DEFINED__

#include <map>
//#include "A2PPreferencias.h"
#ifdef MACINTOSH
	
	#include "IA2PPreferenciasFunciones.h"
#endif
#ifdef WINDOWS
	
	#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif


class PnlTrvDataNode;

/**  Class representing a folder hierarchy. A smarter implementation would build the hierarchy lazily, i.e. as needed, rather than eagerly (i.e. the whole thing first time of asking) as here.
	@ingroup paneltreeview
*/
class PnlTrvDataModel
{
public:
	/**	Constructor
		@param filter [IN] specifies a filter on the files that are represented by this
	 */
	PnlTrvDataModel(PMString filter);

	/** Default constructor */
	PnlTrvDataModel();

	/**	Destructor
	 */
	virtual ~PnlTrvDataModel();

	/**	Specify root path for the view onto the file system
		@param rootPath [IN] specify the path for the root node, and force
			a recalculation of the folder hierarchy represented in this model
	 */
	void SetRootPath(const PMString& rootPath) ;

	/**	Accessor for the root path
		@return PMString giving the path associated with the root node
	 */
	PMString GetRootPath() const;

	/**	Accessor for the root node
		@return PnlTrvDataNode* root node reference
	 */
	PnlTrvDataNode* GetRootNode() const { return fRootNode;}

	/**	Given a path, discover the path of its parent
		@param path [IN] specifies given path
		@return Returns path as string
	 */
	PMString GetParentPath(const PMString& path) const;

	/**	Determine the number of children given a path
		@param path [IN] specifies given path
		@return Returns number of children
	 */
	int32 GetChildCount(const PMString& path) const;

	/**	Get the path associated with the specified child
		@param path [IN] specifies given path
		@param nth [IN] specifies which child
		@return Returns path as string
	 */
	PMString GetNthChildPath(const PMString& path, int32 nth) const;

	/**	Determine the index in the parent's list of kids for 
		a particular child
		@param par [IN] specifies parent
		@param kid [IN] specifies kid
		@return Returns index 
	 */
	int32 GetChildIndexFor(const PMString& par, const PMString& kid) const;


	/**	Call when you want to force a recalculation
	 */
	void ForciblyUpdate();

	/**
		funcion que busca un nodo a partir de su nombre y devuelve el nodo parent
		tambien retorna un valor booleano, v en caso de que lo encontro
	*/
	bool16 BuscaNodo(PnlTrvDataNode* parentNode,PMString NomNodo);

	/**
	*/
	int CampoAGuardar(int32 numCampo,Preferencias &Pref);

	/**
		funcion que obtiene la cantidad de avisos dentro del archivo a importar
	*/
	int32 CantidadAvisos(PMString NomArchivo);

protected:

	/**	Calculate the subtree below the given node 
		@param node [IN] node to start from
	 */
	void calcDescendentsRecursively(PnlTrvDataNode* node) ;

	/**	Determine if this is a path that we want to put in the data model
		@param p [IN] specifies path of interest
		@return bool16 if the path is a file system path of interest
	 */
	bool16 validPath(const PMString& p);

	/**	Destroy the tree represented in this 
	 */
	void deleteTree() ;

	/** Get the sum of hash values for this tree
	*/
	int32 calcCurrentTreeHashValue();

private:
	enum {ePnlTrvMaxRecursionDepth = 256};

	std::map<PMString, PnlTrvDataNode* > fPathNodeMap;
	PnlTrvDataNode* fRootNode;
	PMString fFilter;
	int32 fRecursionDepthLimiter;
	int32 fTreeHashValue;
	Preferencias Pref;
};


#endif // __PnlTrvDataModel_H_DEFINED__

