//========================================================================================
//  
//  $File: $
//  
//  Owner: Interlasa.com
//  
//  $Author: $
//  
//  $DateTime: $
//  
//  $Revision: $
//  
//  $Change: $
//  
//  Copyright 1997-2007 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#pragma once
#ifndef __IA2PImpSuite_h__
#define __IA2PImpSuite_h__

#include "IPMUnknown.h"
#include "A2PImpID.h"

/** IA2PImpSuite defines the interface for this plug-ins selection suite.

    
    @ingroup a2pimp
*/
class IA2PImpSuite : public IPMUnknown
{
public:
	enum {kDefaultIID = IID_IA2PIMPSUITE };

	/** Selection suites will have methods that return whether or not the current selection supports an operation.  The
	actual ASB and CSB implementations may answer this question differently, so we demonstrate two different types. 
	The first will return kTrue when any CSBs support this operation.  The later will return kTrue when all CSBs support
	this operation.  This is actually an ASB detail, but it's explained here for demonstration purposes.
	@return kTrue if operation X is supported on the current selection.*/
	virtual bool16 CanDoX() const = 0;

	/** Performs operation X returning an ErrorCode.
	@return kSuccess on success, or an appropriate ErrorCode on failure. */
	virtual ErrorCode DoX() = 0;

	/** Is operation Y supported by the current selection.  In this case, the ASB
	will dictate that all CSBs must support the selection.
	@return kTrue if operation Y is supported on the current selection.*/
	virtual bool16 CanDoY() const = 0;

	/** Performs operation Y returning an ErrorCode. 
	@return kSuccess on success, or an appropriate ErrorCode on failure. */
	virtual ErrorCode DoY() const = 0;

	/** Gets some data from the current selection.  Instead of returning a value,
	implementations will stuff data into the OUT variable.
	@param vector OUT Data Z for the current selection is inserted into vector.*/
	virtual void GetZ( K2Vector<int32> & vector) = 0;

	/** Sets some data on the current selection.
	@param z IN Data to set.
	@return kSuccess on success, or an appropriate ErrorCode on failure.  */
	virtual ErrorCode SetZ( int32 z ) = 0;

};
#endif // __IA2PImpSuite_h__

//  Code generated by DollyXs code generator
