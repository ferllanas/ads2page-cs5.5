/*
//	File:	A2P20ControlladorDlgPreferencias.cpp
//
//	Este archivo ferifica el nuevo passwor y lo guarda en el documento pwd.pw
//
*/

#include "VCPlugInHeaders.h"
#include "SDKUtilities.h"
// Interface includes:
#include "IDocument.h"
#include "ILayoutUIUtils.h"
#include "ILayoutUIUtils.h"
#include "IDataBase.h"
#include "IControlView.h"
#include "ILayoutControlData.h"
#include "IWidgetParent.h"
#include "IPageList.h"
#include "IPanelControlData.h"

// none.

// General includes:
#include "CDialogController.h"

// Project includes:

#include "A2PImpID.h"
#include "PMString.h"
#include <stdio.h>
#include <string.h>
//#include <io.h>
//#include "fstream.h"
//#include <iomanip.h>
#include "CAlert.h"
//#include "SelDlgActionComponent.cpp"
//////////////////////////
#include "IK2ServiceRegistry.h"
#include "IK2ServiceProvider.h"
#include "IDialog.h"
#include "IDialogCreator.h"
#include "IPanelControlData.h"
#include "ISelectableDialogSwitcher.h"
#include "ISession.h"
#include "CAlert.h"
#include "SysFileList.h"
#include "IOpenFileDialog.h"
#include "ITextControlData.h"
// General includes:
#include "SDKUtilities.h"
#include "CActionComponent.h"
//#include  "Encrypt.h"
////////////////////////////////


/** GetFechaPagDialogController
	Methods allow for the initialization, validation, and application of dialog widget values.
  
	Implements IDialogController based on the partial implementation CDialogController. 
	@author Rodney Cook
*/
class GetFechaPagDialogController : public CDialogController
{
	public:
		/**
			Constructor.
			@param boss interface ptr from boss object on which this interface is aggregated.
		*/
		GetFechaPagDialogController(IPMUnknown* boss) : CDialogController(boss) {}

		/** 
			Destructor.
		*/
		virtual ~GetFechaPagDialogController() {}

		/**
			Initialize each widget in the dialog with its default value.
			Called when the dialog is opened.
		*/
		virtual void InitializeDialogFields(IActiveContext *myContext);

		/**
			Validate the values in the widgets. 
			By default, the widget with ID kOKButtonWidgetID causes 
			ValidateDialogFields to be called. When all widgets are valid, 
			ApplyDialogFields will be called.			
			@return kDefaultWidgetId if all widget values are valid, WidgetID of the widget to select otherwise.

		*/
		virtual WidgetID ValidateDialogFields(IActiveContext *myContext);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId);

		/**
			Retrieve the values from the widgets and act on them.
			@param widgetId identifies the widget on which to act.
		*/
		virtual void UserCancelled();

		private:
			int32 result;
			/**
			*/
	
		
};

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its 
 ImplementationID making the C++ code callable by the 
 application.
*/
CREATE_PMINTERFACE(GetFechaPagDialogController, kA2PImpDlgGetFechaPagControllerImpl)
/* InitializeDialogFields
*/
void GetFechaPagDialogController::InitializeDialogFields(IActiveContext *myContext) 
{	/***************AL ABRIR LA FORMA DE LA LISTA*********/
	//Llenado de combo

	SetTextControlData(kA2PImpTextFechaWidgetID,"");	
	SetTextControlData(kA2PImpTextPaginaWidgetID,"");	
	SelectDialogWidget(kA2PImpTextFechaWidgetID);
	
	
	do
	{
		InterfacePtr<IControlView> DlgPrefView(this, UseDefaultIID());
		if(DlgPrefView==nil)
		{
			CAlert::ErrorAlert("No existe");
			break;
		}
		
		InterfacePtr<IWidgetParent>		myParent(DlgPrefView, UseDefaultIID());				
		if(myParent==nil)
		{
			
			break;
		}
				
		InterfacePtr<IPanelControlData>	panel((IPanelControlData*)myParent->QueryParentFor(IID_IPANELCONTROLDATA)) ;
		if(panel==nil)
		{
			
			break;
		}
		
		InterfacePtr<IControlView>		CVNumberPage(panel->FindWidget(kA2PImpNumberDePaginaAFluirWidgetID), UseDefaultIID() );
		if(CVNumberPage==nil)
		{
			//CAlert::InformationAlert("No se encontro el editbox");
			break;
		}
			
		IDocument *DocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
		IDataBase *theDB = GetDataBase(DocPtr); 

	
		InterfacePtr<IControlView> view(Utils<ILayoutUIUtils>()->QueryFrontView()); 
		if(view==nil)
			break;
		
		InterfacePtr<ILayoutControlData> LCD(view, UseDefaultIID()); 
		if(LCD==nil)
			break;
		
		InterfacePtr<IPageList> PageList(DocPtr,IID_IPAGELIST); 
		if(PageList==nil)
			break;
		
		if(PageList->GetPageCount()>1)
		{
			SetTextValue(kA2PImpNumberDePaginaAFluirWidgetID,1);
			CVNumberPage->Enable(kTrue);
		}
		else
		{	
			SetTextValue(kA2PImpNumberDePaginaAFluirWidgetID,1);
			CVNumberPage->Disable(kTrue);
		}
		
	}while(false);
	
	
}

/* ValidateDialogFields
*/
/************ACOMODA O PONE LAS PREFERENCIAS SOBRE LAS CAJAS ETC....********/
WidgetID GetFechaPagDialogController::ValidateDialogFields(IActiveContext *myContext) 
{
	WidgetID widget=kInvalidWidgetID;
	do
	{
	
		PMString dates=GetTextControlData(kA2PImpTextFechaStandartWidgetID);	
		if(dates.NumUTF16TextChars()<=0)
		{
			widget=kA2PImpTextFechaStandartWidgetID;
		}
		
		PMString namepage=this->GetTextControlData(kA2PImpTextPaginaWidgetID);
	
		if(namepage.NumUTF16TextChars()<=0)
		{
			widget=kA2PImpTextPaginaWidgetID;
			CAlert::InformationAlert(kA2PImpNomPagInvalidoStringKey);
		
		}
		else
		{
			/*if((GetTextControlData(kA2PImpNumberDePaginaAFluirWidgetID).GetAsNumber())<1)
			{
				widget=kA2PImpNumberDePaginaAFluirWidgetID;
				CAlert::InformationAlert(kA2PImpNPagInvalidoStringKey);
			}
			else
			{*/
				IDocument *DocPtr = Utils<ILayoutUIUtils>()->GetFrontDocument(); 
				if(DocPtr==nil)
					break;
				IDataBase *theDB = GetDataBase(DocPtr); 
				if(theDB==nil)
					break;
	
				InterfacePtr<IControlView> view(Utils<ILayoutUIUtils>()->QueryFrontView()); 
				if(view==nil)
					break;
				InterfacePtr<ILayoutControlData> LCD(view, UseDefaultIID()); 
				if(LCD==nil)
					break;
				InterfacePtr<IPageList> PageList(DocPtr,IID_IPAGELIST);
				if(PageList==nil)
					break; 
				if((GetTextControlData(kA2PImpNumberDePaginaAFluirWidgetID).GetAsNumber())>PageList->GetPageCount())
				{
					widget=kA2PImpNumberDePaginaAFluirWidgetID;
					CAlert::InformationAlert(kA2PImpNPagInvalidoStringKey);
				}
			//}
		
	
		}
	
	}while(false);
	
	return(widget);
}

/* ApplyDialogFields
*/
void GetFechaPagDialogController::ApplyDialogFields(IActiveContext *myContext,const WidgetID& widgetId) 
{	
	SystemBeep();  
}

void GetFechaPagDialogController::UserCancelled()
{
	SetTextControlData(kA2PImpTextFechaWidgetID,"?");	
	SetTextControlData(kA2PImpTextPaginaWidgetID,"?");	
}

// End, GetFechaPagDialogController.cpp.