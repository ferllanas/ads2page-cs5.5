/*
//	File:	SelectBotonImport.cpp
//
//	Date:	22-Aug-2003
//
//	Este codigo es utilizado para observar el momento que seleccionas el boton importar.
//	al realizar esto se habrira una ventana para seleccionar el archivo que se desea importar.
//	llenando deacuerdo a prefrencias el arbol del la paleta importar.
//
*/
#include "VCPlugInHeaders.h"

// Interface includes:
#include "IApplication.h"
#include "ISession.h"
#include "IPnlTrvOptions.h"
#include "IWorkspace.h"
#include "IPnlTrvChangeOptionsCmdData.h"
#include "ITreeViewMgr.h"
#include "IControlView.h"
#include "IPanelControlData.h"
#include "IBoolData.h"
#include "CObserver.h"
#include "ISubject.h"

#include "IIdleTaskMgr.h"
#include "IIdleTaskthread.h"
#include "IIdleTask.h"
#include "FileUtils.h"
#include "IControlView.h"

//#include "ICallbackTimer.h"
//#include "MetaDataTypes.h"
//#include <IDateTimeData.h>
//#include "A2PImpBlinkAvisoDeCambio.h"

// General includes:
#include "SDKUtilities.h"
#include "CActionComponent.h"
#include "PnlTrvUtils.h"
#include "UIDList.h"
#include "CmdUtils.h"
#include "SysFileList.h"
#include "CAlert.h"
#include "IOpenFileDialog.h"

//para obtener el texto del widget de la paleta
#include "ITextControlData.h"
#include "Utils.h"
#include "IPalettePanelUtils.h"

#ifdef MACINTOSH
		#include "IA2PPreferenciasFunciones.h"
#endif
#ifdef WINDOWS
		#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif
#include "A2PAvisosHeader.h"
//#include "InterDemoDocWatchUtils.h"

// Project includes:
#include "A2PImpID.h"

#include "IActualizarFunction.h"

/** SelDlgIconObserver
	Allows dynamic processing of icon button widget state changes, in this case
	the tab dialog's info button. 

	Implements IObserver based on the partial implementation CObserver. 

	@author Lee Huang
*/
class SelBotonImportObserver : public CObserver
{
	public:	
		//StructAvisos Avisos;
		/**
			Constructor.

			@param boss interface ptr from boss object on which interface is aggregated.
		*/
		SelBotonImportObserver(IPMUnknown *boss); //: CObserver(boss) {}

		/** Destructor. */
		virtual ~SelBotonImportObserver() {}

		virtual void AutoAttach();

        virtual void AutoDetach();		
		/**
			Called by the host when the observed object changes. In this case: 
			the tab dialog's info button is clicked.

			@param theChange specifies the class ID of the change to the subject. Frequently this is a command ID.
			@param theSubject points to the ISubject interface for the subject that has changed.
			@param protocol specifies the ID of the changed interface on the subject boss.
			@param changedBy points to additional data about the change. Often this pointer indicates the class ID of the command that has caused the change.
		*/
		
		virtual void Update
		(
			const ClassID& theChange, 
			ISubject* theSubject, 
			const PMIID& protocol, 
			void* changedBy
		);
	private:
		
		/** Encapsulates functionality for the SetOptions menu item. */
		void handleSetOptions();
		
		/** Encapsulates functionality for the Refresh menu item. */
		void handleRefresh();
		
		
		void Temporizar(PMString &Tprzr);
		
		
		/**	Method to retrieve a folder 
			@param pmTitlePtr 
			@return PMString 
		 */
		PMString getSelectedFolderFromDialog();

		/**	Exec cmd to change options for this plug-in
			@param options list of options to drive this command with
			@return  kSuccess if we could process it, kFailure otherwise
		 */
		ErrorCode processSetOptionsCommand(const K2Vector<PMString>& options);
};








/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(SelBotonImportObserver, kA2PImpBotonImportarObserverImpl)






SelBotonImportObserver::SelBotonImportObserver(IPMUnknown* boss)
	: CObserver(boss)
{
}



/*	Update
*/
void SelBotonImportObserver::Update
(
	const ClassID& theChange, 
	ISubject* theSubject, 
	const PMIID& protocol, 
	void* changedBy
)
{
	InterfacePtr<IControlView> view(theSubject, IID_ICONTROLVIEW);

	if (view != nil)
	{
		// Get the button ID from the view
		WidgetID theSelectedWidget = view->GetWidgetID();

		if (theSelectedWidget == kA2PImpOKButtonImportarWidgetID && theChange == kTrueStateMessage)
		{//cuando se selecciono el boton importar
			
			InterfacePtr<IA2PPreferenciasFunctions> A2PPrefeFuntions(static_cast<IA2PPreferenciasFunctions*> (CreateObject
			(
				kA2PPreferenciasFuncionesBoss,	// Object boss/class
				IA2PPreferenciasFunctions::kDefaultIID
			)));
			
			Preferencias Prefer;
			
			if(A2PPrefeFuntions!=nil)
			{
				PMString DefaulPrefer=PnlTrvUtils::CrearFolderPreferencias();
				DefaulPrefer.Append("Default.pfa");
				if(A2PPrefeFuntions->GetDefaultPreferencesConnection(Prefer,kFalse)==kFalse)
				{
					CAlert::ErrorAlert(kA2PImpA2PImpNoExistenPreferenciasStringKey);
				}
				else
				{
					this->handleSetOptions();//para obtener el nuevo arbol
					//this->Temporizar();
				}
			}
		}
    }	
}//end Funcion




/* AutoAttach */
/************AL INICIAR LA FORMA SE CARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR************************************************************************/

void SelBotonImportObserver::AutoAttach()
{//	CAlert::WarningAlert("AutoAttach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		subject->AttachObserver(this, IID_IBOOLEANCONTROLDATA);
	}
}

/* AutoDetach */
/************AL TERMINAR LA FORMA SE DESCARGA EL OBSERVER EL CUAL ES ENCARGADO DE QUE CUANDO OCURRA UN EVENTO 
			SOBRE EL BOTON APLICAR************************************************************************/

void SelBotonImportObserver::AutoDetach()
{//	CAlert::WarningAlert("AutoDetach 2");
	InterfacePtr<ISubject> subject(this, UseDefaultIID());
	if (subject != nil)
	{
		//subject->DetachObserver(this);
	}
}

/*
	funcion que habre el dialogo para seleccionar el archivo deseado
	regresa el path del archivo seleccionado
*/
PMString SelBotonImportObserver::getSelectedFolderFromDialog()
{
//Se establecen las opciones de vista de archivos para el dialogo abrir archivo
SysOSType kSysOSType_CSVFileType = 'TEXT';
PMString  kPMString_CSVFileFamily("Texto");
PMString kPMString_CSVFileExtension("txt");

SysOSType kSysOSType_CSVFileTipo = 'EXPO';//no debe sobrepasar de cuatro letras
PMString  kPMString_CSVFileFamily1("EXPORT");
PMString kPMString_CSVFileExtension1("export");



	PMString retval="";//la cadena que contien el path a regresar
	IDFile file;		//variabl tipo IDFile
	SysFileList filesToOpen;//variable Lista de archivo que se muestran
	bool16 success;			//suceso

	InterfacePtr<IOpenFileDialog> iOpenDlg	//interfaz para abrir el dialogo
	(
		static_cast<IOpenFileDialog*>
		(
			CreateObject
			(
				kOpenFileDialogBoss,
				IOpenFileDialog::kDefaultIID
			)
		)
	);

	if(iOpenDlg != nil)//si no se habre
	{
		iOpenDlg->AddType(kSysOSType_CSVFileType);
		iOpenDlg->AddExtension(&kPMString_CSVFileFamily, &kPMString_CSVFileExtension);
		iOpenDlg->AddType(kSysOSType_CSVFileTipo);
		iOpenDlg->AddExtension(&kPMString_CSVFileFamily1, &kPMString_CSVFileExtension1);
	
		if(iOpenDlg->DoDialog(nil, filesToOpen, kFalse) == kTrue)
		{
			file = *filesToOpen.GetNthFile(0);//muestra los archivos a seleccionar
			success = kTrue;
		}
	}


	if(success==kTrue)// se no se seleciono un archivo o se dio cancelar sin seleccionar el archivo
	{
		retval=SDKUtilities::SysFileToPMString( &file );		
	}
	
	if(MarcoDebug == kTrue){CAlert::InformationAlert("getSelectedFolderFromDialog()");}
	 return retval;
}



/*
	funcion principal
*/
void SelBotonImportObserver::handleSetOptions()
{

	do{
		const int32 cCountOfOptions = 1;
		K2Vector<PMString> optionsVec(cCountOfOptions);
		PMString assetPathTitle(kA2PImpSetOption1StringKey);
		assetPathTitle.Translate();
		assetPathTitle.SetTranslatable(kFalse);
	
		PMString folder = this->getSelectedFolderFromDialog();
		
		if(MarcoDebug == kTrue){CAlert::InformationAlert("archivo - PMString folder = this->getSelectedFolderFromDialog();");}
		
		if(folder.IsEmpty() == kTrue)
		{
			break;
		}
		
		optionsVec.push_back(folder);
		if(folder.IsEmpty() == kFalse)
		{
			//CAlert::InformationAlert("1");
			this->processSetOptionsCommand(optionsVec);
			
			//CAlert::InformationAlert("2");
			InterfacePtr<IA2PAvisos> A2PAVISOS(static_cast<IA2PAvisos*> (CreateObject
			(
				kA2PAvisosBoss,	// Object boss/class
				IA2PAvisos::kDefaultIID
			)));
				
			//CAlert::InformationAlert("3");
			if(A2PAVISOS!=nil){
				A2PAVISOS->CargarAvisos(folder,kTrue);// archivo desde aqui
				this->Temporizar(folder);// magic starts here !!!
			}
			
			//CAlert::InformationAlert("4");
			this->handleRefresh();
			//CAlert::InformationAlert("5");
		}
		
	if(MarcoDebug == kTrue){CAlert::InformationAlert("handleSetOptions() ended");}

	}while(kFalse);
}
/*
	lo que se realiza en esta funcion es cambiar el rootnode del arbol.
	por el nuevo rootnode que viene siendo el path del archivo a importar
	una vez que se realiza esto se hace la llamada de handle refresh 
	al realizar esto se limpia el arbol y se hace un refres ahora con el refresh
	se cambial el noderoot por el nuevo path imprtando y poneindo los nodos de la seccion.
*/
ErrorCode SelBotonImportObserver::processSetOptionsCommand(const K2Vector<PMString>& options)
{
	
	ErrorCode retval = kFailure;
	do {
	
		InterfacePtr<IWorkspace> iSessionWorkspace(GetExecutionContextSession()-> QueryWorkspace());
		ASSERT(iSessionWorkspace);
		if(iSessionWorkspace == nil) {
			break;
		}

		InterfacePtr<IPnlTrvOptions> iOptions(iSessionWorkspace, UseDefaultIID());
		ASSERT(iOptions);
		if(!iOptions) {
			break;
		}
		InterfacePtr<ICommand>	modOptionsCmd (CmdUtils::CreateCommand(kA2PImpPnlTrvChangeOptionsCmdBoss));
		ASSERT(modOptionsCmd);
		if(!modOptionsCmd) {
			break;
		}	
		modOptionsCmd->SetItemList(UIDList(iSessionWorkspace));
		InterfacePtr<IPnlTrvChangeOptionsCmdData> cmdData (modOptionsCmd, UseDefaultIID());
		ASSERT(cmdData);
		if (cmdData == nil){
			break;
		}
		K2Vector<PMString>::const_iterator iter;
		for(iter = options.begin(); iter != options.end(); ++iter) 
		{
			cmdData->AddOption(*iter);	// template path
		}
		retval = CmdUtils::ProcessCommand(modOptionsCmd);
		ASSERT(retval == kSuccess);	
	
	if(MarcoDebug == kTrue){CAlert::InformationAlert("processSetOptionsCommand(const K2Vector<PMString>& options)");}
	
	} while(kFalse);
	return retval;
}

//  Generated by Dolly build 17: template "IfPanelMenu".
// End, PnlTrvActionComponent.cpp
void SelBotonImportObserver::handleRefresh()
{
	do 
	{
		IControlView* treeWidget = PnlTrvUtils::GetWidgetOnPanel(kA2PImpPanelWidgetID,kA2PImpTreeViewWidgetID);
		ASSERT(treeWidget);
		
		if(!treeWidget) {
			break;
		}
		
		InterfacePtr<IBoolData> refreshStatus(GetExecutionContextSession(),IID_IPnlTrvRefreshStatus);
		ASSERT(refreshStatus);
		if(refreshStatus) {
			refreshStatus->Set(kTrue);
			// The model code will clear this flag
			// This flag gives us a way to communicate that the tree model should
			// be updated even if we haven't changed the root
			InterfacePtr<ITreeViewMgr> iTreeViewMgr(treeWidget,UseDefaultIID());
			ASSERT(iTreeViewMgr);
			
			if(!iTreeViewMgr) {
				break;
			}
			
			iTreeViewMgr->ClearTree(kTrue);//limpia el arbol
			iTreeViewMgr->ChangeRoot();//esta funcion es la que en realidad ejecuta la llamada a llenar el arbol
			// se ejecuta imediatamente PnlTrvTVHierarchyAdapter::GetRootNode
		}
		
		if(MarcoDebug == kTrue){CAlert::InformationAlert("handleRefresh()");}
		
	} while(kFalse);
}


/*
Temporizar
*/
void SelBotonImportObserver::Temporizar(PMString &Tprzr){
	if(MarcoDebug == kTrue){CAlert::InformationAlert("#1  Temporizar(PMString &Tprzr): "+Tprzr);}
	//do{
		InterfacePtr<IA2PActualizarFunction> ActualizarFunction(static_cast<IA2PActualizarFunction*> (CreateObject
				(
					kA2PActualizarFunctionBoss,	// Object boss/class
					IA2PActualizarFunction::kDefaultIID
				)));
				
				if(ActualizarFunction!=nil)
				{	
					ActualizarFunction->setTextOfWidgetFromPalette( kA2PImpPanelWidgetID, GuardaStringWidgetID, Tprzr );//entra la primer fecha al momento de clikear importar			
				}
			
	//} while(kFalse);
	//aqui ya viene ese string
}