/*
 *  A2PAvisosHeader.h
 *  A2PImp
 *
 *  Created by Desarrollo2 on 4/8/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
 
#ifndef _IA2PAvisos_
#define _IA2PAvisos_

// Interface includes:
#include "IPMUnknown.h"
#include "A2PImpID.h"
#include "A2PImpStructAvisos.h"


 class IA2PAvisos : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IA2PAVISOSINTERFACE };

	/**

	*/
	virtual bool16 CargarAvisos(PMString Path,bool16 RefreshAvisos)=0;
	
	virtual int32 CantidadDAvisos()=0;
	
	virtual PMString ArchivoDAvisos()=0;
	
	virtual bool16 CopiasAvisosCargados(StructAvisos Avisos[])=0;
};


#endif // _IAvisos_