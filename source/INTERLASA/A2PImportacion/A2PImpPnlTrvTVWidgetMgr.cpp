//========================================================================================
//  
//  $File: //depot/indesign_3.0/gm/source/sdksamples/paneltreeview/PnlTrvTVWidgetMgr.cpp $
//  
//  Owner: Adobe Developer Technologies
//  
//  $Author: pmbuilder $
//  
//  $DateTime: 2003/09/30 15:41:37 $
//  
//  $Revision: #1 $
//  
//  $Change: 223184 $
//  
//  Copyright 1997-2003 Adobe Systems Incorporated. All rights reserved.
//  
//  NOTICE:  Adobe permits you to use, modify, and distribute this file in accordance 
//  with the terms of the Adobe license agreement accompanying it.  If you have received
//  this file from a source other than Adobe, then your use, modification, or 
//  distribution of it requires the prior written permission of Adobe.
//  
//========================================================================================

#include "VCPlugInHeaders.h"

// Interface includes:
#include "IControlView.h"
#include "IPanelControlData.h"
#include "ITextControlData.h"
#include "ITreeViewHierarchyAdapter.h"
#include "ITriStateControlData.h"

// General includes:
#include "CTreeViewWidgetMgr.h"
#include "CreateObject.h"
#include "CoreResTypes.h"
#include "LocaleSetting.h"
#include "RsrcSpec.h"
#include "SysControlIds.h"
#include "PnlTrvUtils.h"
#include "CAlert.h"
// Project includes:
#include "A2PImpID.h"
#include "PnlTrvFileNodeID.h"

/**
	Implements ITreeViewWidgetMgr, providing methods to create and describe the widgets for 
	nodes in the tree. This interface is similar to a widget factory in that this is where 
	you will create widgets for the nodes in the tree.
	
	For further reading on ITreeViewWidgetMgr interface, see the Widgets chapter
	of the User Interface technote.

	@author Ian Paterson
	@ingroup paneltreeview

*/

class PnlTrvTVWidgetMgr: public CTreeViewWidgetMgr
{
public:
	/**
		Constructor
		@param boss object on which this interface is being aggregated
	*/	
	PnlTrvTVWidgetMgr(IPMUnknown* boss);

	/**
		Destructor
	*/	
	virtual ~PnlTrvTVWidgetMgr() {}

	/** Create instance of the widget that represents given node in the tree.
		@param node [IN] specifies node of interest
		@return return reference to a newly instantiated object that is suitable for this node
	*/	
	virtual	IControlView* CreateWidgetForNode(
		const NodeID& node) const;
	/**
		Retrieve ID of a widget that has the right appearance for the node
		that you're trying to display. Here we use the same widget types for all the nodes,
		but vary the appearance by showing or hiding the expander widget depending on the number
		of children associated with a node.
		@param node [IN] specifies node of interest
		@return the ID of a widget that has the correct appearance for the given node type
	*/	
	virtual	WidgetID GetWidgetTypeForNode(const NodeID& node) const;

	/** Determine how to render the given node to the specified control.
		Figure out how you're going to render it based on properties of the node
		such as whether is has children or not (don't show expander if no children, for instance).
		@param node [IN] refers to the node that we're trying to render
		@param widget [IN] the control into which this node's appearance is going to be rendered
		@param message [IN] specifies ???
		@return kTrue if the node has been rendered successfully, kFalse otherwise
	*/	
	virtual	bool16 ApplyNodeIDToWidget(
		const NodeID& node, 
		IControlView* widget, 
		int32 message = 0) const;

	/** Given a particular node, how far should it be indented? This method
		returns a value in pixels that is used to indent the displayed node 
		from the left edge of the tree-view control.
		@param node [IN] specifies node of interest
		@return an indent for this particular node from the left edge of tree-view
	*/
	virtual PMReal GetIndentForNode(const NodeID& node) const;

private:
	void indent(
		const NodeID& node, IControlView* widget, IControlView* staticTextWidget) const;

	void setIconForContent(
		IControlView* widget, const PMString& path) const;
};	

/* CREATE_PMINTERFACE
 Binds the C++ implementation class onto its ImplementationID 
 making the C++ code callable by the application.
*/
CREATE_PMINTERFACE(PnlTrvTVWidgetMgr, kA2PImpTrvTVWidgetMgrImpl)

/* Constructor
*/
PnlTrvTVWidgetMgr::PnlTrvTVWidgetMgr(IPMUnknown* boss) :
	CTreeViewWidgetMgr(boss)
{
	//CAlert::InformationAlert("A000");
}


/* CreateWidgetForNode
*/
IControlView* PnlTrvTVWidgetMgr::CreateWidgetForNode(
	const NodeID& node) const
{
	IControlView* retval= (IControlView*) ::CreateObject(
		::GetDataBase(this),
		RsrcSpec(LocaleSetting::GetLocale(), 
		kA2PImpPluginID, 
		kViewRsrcType, 
		kA2PImpNodeWidgetRsrcID),
		IID_ICONTROLVIEW);
	ASSERT(retval);
	return retval;
}



/* GetWidgetTypeForNode
*/
WidgetID PnlTrvTVWidgetMgr::GetWidgetTypeForNode(
	const NodeID& node) const
{
	return kA2PImpNodeWidgetId;
}


/* ApplyNodeIDToWidget
*/
bool16 PnlTrvTVWidgetMgr::ApplyNodeIDToWidget(
	const NodeID& node, IControlView* widget, int32 message) const
{
	CTreeViewWidgetMgr::ApplyNodeIDToWidget(node, widget);
	do
	{
		// If this node doesn't have any children, don't show expander Widget
		InterfacePtr<IPanelControlData> 
			panelControlData(widget, UseDefaultIID());
		ASSERT(panelControlData);
		if(panelControlData==nil)
		{
			break;
		}
		// Set expander widget to hidden state if this node has no kids
		IControlView* expanderWidget = panelControlData->FindWidget(kTreeNodeExpanderWidgetID);
		ASSERT(expanderWidget);
		if(expanderWidget == nil)
		{
			break;
		}

		IControlView*   CheckboxWidget = panelControlData->FindWidget(kA2PImpTreeNodeCheckBoxWidgetID);

		InterfacePtr<const ITreeViewHierarchyAdapter> adapter(this, UseDefaultIID());
		ASSERT(adapter);
		if(adapter == nil)
		{
			break;
		}
		if (expanderWidget)
		{
			if (adapter->GetNumChildren(node) <= 0)
			{
				expanderWidget->Hide();
				CheckboxWidget->Hide();//no muestra el checbox
				widget->Disable();	   //no abilita los nodos avisos
			}
			else
			{
				expanderWidget->Show();
				expanderWidget->Show();//se puestra el checbox
				widget->Enable();	   //se habilita el nodo padre
			}
		}
		IControlView* iconView = 
			panelControlData->FindWidget(kA2PImpIconWidgetId);
		ASSERT(iconView);
		if (!iconView)
		{
			break;
		}

		TreeNodePtr<PnlTrvFileNodeID> nodeID(node);
		ASSERT(nodeID);
		if(nodeID == nil)
		{
			break;
		}	
		PMString nodePath =  nodeID->GetPath();
		this->setIconForContent(iconView, nodePath);

		IControlView* displayStringView = 
			panelControlData->FindWidget( kA2PImpNodeNameWidgetID );
		ASSERT(displayStringView);
		if(displayStringView == nil)
		{
			break;
		}
		InterfacePtr<ITextControlData>
			textControlData( displayStringView, UseDefaultIID() );
		ASSERT(textControlData);
		if(textControlData== nil)
		{
			break;
		}
		
		PMString stringToDisplay(PnlTrvUtils::TruncatePath(nodePath));
		if(stringToDisplay.IsEmpty() == kTrue)
		{
			stringToDisplay="Empty?";	
		}
		stringToDisplay.SetTranslatable( kFalse );
		textControlData->SetString(stringToDisplay);
		// Adjust indenting- this may be temporary
		this->indent( node, widget, displayStringView );
		//////////////////////
		IControlView* CheckBoxMostrar = panelControlData->FindWidget( kA2PImpTreeNodeCheckBoxWidgetID );
		ASSERT(CheckBoxMostrar);
		if(CheckBoxMostrar == nil) 
		{
			break;
		}
		InterfacePtr<ITriStateControlData>  ControlDataCheckBox( CheckBoxMostrar, UseDefaultIID() );
		ASSERT(ControlDataCheckBox);
		if(ControlDataCheckBox== nil) 
		{
			break;
		}
		ControlDataCheckBox->Deselect(kFalse);
	} while(kFalse);

	return kTrue;
}


/* GetIndentForNode
*/
PMReal PnlTrvTVWidgetMgr::GetIndentForNode(const NodeID& node) const
{
	/*do
	{
		TreeNodePtr<PnlTrvFileNodeID>  nodeID(node);
		ASSERT(nodeID);
		if(nodeID == nil)
		{
			break;
		}	
		PMString path = nodeID->GetPath();
		// Figure out the indent by counting the platform path separators in the path?
		// Not sure how apt this would be on Mac if we have aliases
		int32 indents = PnlTrvUtils::GetPathSeparatorCount(path);
		const PMReal cIndentInterval = 2.0;
		return indents * cIndentInterval;

	} while(kFalse);*/
	return 10.0;
}


/* indent
*/
void PnlTrvTVWidgetMgr::indent(
	const NodeID& node, 
	IControlView* widget, 
	IControlView* staticTextWidget) const
{	
	do
	{
		ASSERT(widget);
		ASSERT(staticTextWidget);
		if(widget == nil || staticTextWidget == nil)
		{
			break;
		}
		const PMReal indent = this->GetIndent(node);	
		// adjust the size to fit the text 
		PMRect widgetFrame = widget->GetFrame();
		widgetFrame.Left() = indent;
		widget->SetFrame( widgetFrame );
		// Call window changed to force FittedStaticText to resize
		staticTextWidget->WindowChanged();
		PMRect staticTextFrame = staticTextWidget->GetFrame();
		staticTextFrame.Right( widgetFrame.Right() );
		// Don't at present take account of scroll bar dimension?
		staticTextWidget->SetFrame( staticTextFrame );
	} while(kFalse);
}


/* setIconForContent
*/
void PnlTrvTVWidgetMgr::setIconForContent(
	IControlView* iconView, const PMString& path) const
{
	PMString lcPath = path;
	lcPath.ToLower();
	// If it's some kind of graphic; set to be an eyeball?
	// Quick hack for demo purposes
	// It's a bit misleading; we can preview a PSD for instance,
	// but although we can place PDF the image preview code doesn't render PDF
	// So this is just approximately what we can preview
	if (lcPath.Contains(PMString(".gif")) || 
		lcPath.Contains(PMString(".jpg")) || 
		lcPath.Contains(PMString(".jpeg")) || 
		lcPath.Contains(PMString(".eps")) ||
		lcPath.Contains(PMString(".tif")) ||
		lcPath.Contains(PMString(".psd")) ||
		lcPath.Contains(PMString(".pdf")))
	{
		iconView->SetRsrcPluginID(kA2PImpPluginID);
		iconView->SetRsrcID(kA2PImpEyeBallIconRsrcID);
	}
	else
	{
		// Since the node widgets are recycled, be sure to explicitly set
		// the icon back to our default
		// kPMRsrcID_None = 0,0
		iconView->SetRsrcPluginID(0);
		iconView->SetRsrcID(0);
	}
}

//	end, File: PnlTrvTVWidgetMgr.cpp
