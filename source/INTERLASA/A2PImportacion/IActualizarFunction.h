/*
//	File:	IA2PActualizarFunction.h
//
//	Date:	23-Feb-2004
//
//	ADOBE SYSTEMS INCORPORATED
//	Copyright 2001 Adobe Systems Incorporated. All Rights Reserved.
//
//	NOTICE: Adobe permits you to use, modify, and distribute this file in
//	accordance with the terms of the Adobe license agreement accompanying it.
//	If you have received this file from a source other than Adobe, then your
//	use, modification, or distribution of it requires the prior written
//	permission of Adobe.
//
*/

#ifndef _A2PActualizarFunction_
#define _A2PActualizarFunction_

// Interface includes:
#include "IPMUnknown.h"
#include "A2PImpID.h"

#ifdef MACINTOSH
	#include "A2PPreferencias.h"
#endif
#ifdef WINDOWS
	#include "..\A2P_PREFERENCIAS\IA2PPreferenciasFunciones.h"
#endif
//#include "A2PImpStructPreferencias.h"    //If you are using MS Visual C++ you do not need this


/**

	@author Ian Paterson
*/
class IA2PActualizarFunction : public IPMUnknown
{
public:
	enum { kDefaultIID = IID_IA2PActualizarFunction };

	/**

	*/
	virtual bool16 Actualizar()=0;
	virtual bool16 DELETE_AvisosYEtiquetasParaUpdateGeometria()=0;
	/*
	 Metodo que revisa si se debe de actualizar algo de la fluida del A2P
	 */
	virtual bool16 RevisarActualizacionDAvisos()=0;
	
	/*
	metodo que es llamado paradecidir si se actualiza o no tiene que regresar true o false
	*/
	virtual bool16 RevisarModificacionParaBlinkAvisoDeCambio(const bool16& act)=0;
	
	virtual bool16 getTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMString& value)=0;
	virtual bool16 setTextOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMString& value)=0;
	virtual bool16 setValueOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMReal& value)=0;
	virtual bool16 getValueOfWidgetFromPalette(const WidgetID& PaleteWidgetID, const WidgetID& widgetID, PMReal& value)=0;
};

#endif // _IA2PActualizarFunction_
